import { Component, ViewContainerRef, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>`,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = 'app';

  constructor(public viewContainerRef: ViewContainerRef) {}
}
