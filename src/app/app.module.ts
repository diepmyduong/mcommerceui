import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from 'app/app.component';
import { AppRoutingModule } from 'app/app.routing';
import { ServicesModule } from 'app/services';
import { MatDialogModule, MatInputModule, MatButtonModule, MatFormFieldModule } from '@angular/material';

import * as Raven from 'raven-js';
import { AlertDialog } from 'app/modals/alert/alert.component';
import { DynamicComponentLoaderModule } from 'app/services/dynamic-component-loader/dynamic-component-loader.module';
import { DYNAMIC_MANIFEST } from 'app/dynamic-manifest';
import { DynamicComponentLoaderService } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { environment } from '@environments';
import { DeviceDetectorModule } from 'ngx-device-detector';

// Raven
// .config('https://d51de4611e3242609a20dc06a360947c@sentry.io/279093', { autoBreadcrumbs: {
//   'console': false,  // console logging
// }})
// .install();

// export class RavenErrorHandler implements ErrorHandler {
//   handleError(err:any) : void {
//     if (err.message && err.message.indexOf('ViewDestroyedError: Attempt to use a destroyed view: detectChanges') >= 0) {
//       console.warn('ViewDestroyedError')
//     } else {
//       if(environment.production) {
//         Raven.captureException(err)
//       } else {
//           console.error(err)
//       }
//     }
//   }
// }

@NgModule({
  declarations: [
    AppComponent,
    AlertDialog
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ServicesModule,
    FormsModule, 
    ReactiveFormsModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    DynamicComponentLoaderModule.forRoot(DYNAMIC_MANIFEST),
    DeviceDetectorModule.forRoot()
  ],
  entryComponents: [
    AlertDialog
  ],
  providers: [ 
    // { provide: ErrorHandler, useClass: RavenErrorHandler }, 
    DynamicComponentLoaderService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
