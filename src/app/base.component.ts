import * as Console from 'console-prefix'
import { IProgressBarOption } from 'app/chatgut-app/portals/portal-container/base-portal';
import { Subscription } from 'rxjs';
export class BaseComponent {
    constructor(
        public name: string
    ) {
    }
    get log() { 
        return Console(`[${this.name}]`).log
    }
    subscriptions: Subscription[] = []
    progressBar: IProgressBarOption = {
        show: false,
        color: 'primary',
        mode: 'query',
        value: 0,
        buffer: 0
    }

    showLoading() {
        setTimeout(() => { this.progressBar.show = true })
    }

    hideLoading() {
        setTimeout(() => { this.progressBar.show = false })
    }
    async ngOnDestroy() {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subscriptions.forEach(s => {
            s.unsubscribe()
        })
    }
}