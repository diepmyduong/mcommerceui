import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NotificationComponent } from './notification.component';
import { MatProgressSpinnerModule, MatTooltipModule } from '@angular/material';
import { SafePipeModule } from 'app/shared/pipes/safe-pipe/safe-pipe.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatProgressSpinnerModule,
    SafePipeModule
  ],
  declarations: [NotificationComponent],
  entryComponents: [NotificationComponent],
  exports: [NotificationComponent]
})
export class NotificationModule { }
