import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ChatbotApiService, iTask, iBroadcast } from 'app/services/chatbot';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
@Component({
  selector: 'app-notification',
  styleUrls: ['./notification.component.scss'],
  templateUrl: './notification.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificationComponent {

  loadingTasks: boolean
  taskPage = 1
  pendingTasks: iTask[] = []
  completedTasks: iTask[] = []
  taskSubscription: Subscription
  show: boolean = false
  manuallyShow: boolean = false
  closeTimeout
  intervalList = []
  noConnectionInterval
  noConnectionClosed: boolean = false

  constructor(
    private changeRef: ChangeDetectorRef,
    private chatbotApi: ChatbotApiService
  ) {
    this.initTasks()
    this.checkOnline()
  }

  async ngOnDestroy() {
    if (this.taskSubscription) this.taskSubscription.unsubscribe()
    window.removeEventListener('offline', this.onOffline);
    window.removeEventListener('online', this.onOnline);
    // if (this.noConnectionInterval) clearInterval(this.noConnectionInterval)
  }

  onOffline = () => {     
    if (!this.noConnectionClosed) {
      document.getElementById('no-connection').style.display = 'flex'
    }
  }

  onOnline = () => { 
    document.getElementById('no-connection').style.display = 'none'
    this.noConnectionClosed = false
  }

  async checkOnline() {
    window.addEventListener('offline', this.onOffline);
    window.addEventListener('online', this.onOnline);
    // this.noConnectionInterval = setInterval(() => {
    //   if(navigator.onLine){
    //     document.getElementById('no-connection').style.display = 'none'
    //     this.noConnectionClosed = false
    //   } else {
    //     if (!this.noConnectionClosed) {
    //       document.getElementById('no-connection').style.display = 'flex'
    //     }
    //   }
    // }, 5000)
  }

  async closeNoConnection() {
    document.getElementById('no-connection').style.display = 'none'
    this.noConnectionClosed = true
  }

  async loadTask(local: boolean = true) {
    this.loadingTasks = true
    const limit = 20;
    return await this.chatbotApi.task.getList({
      query: {
        page: this.taskPage,
        limit: limit,
        order: "-updatedAt",
        // filter: {
        //   processState: { $eq: "running" }
        // }
      }
    }).then(tasks => {
      this.completedTasks = [...this.completedTasks, ...tasks]
      this.taskPage += 1
      // console.log('tasks', tasks)
      return tasks
    }).catch(err => {
      console.error(err)
      this.loadingTasks = false
    })
  }
  
  async initTasks() {

    this.pendingTasks = []
    this.completedTasks = []

    // Task Service
    await this.loadTask()
    this.taskSubscription = this.chatbotApi.socket.onTaskChange.subscribe(async (task: iTask) => {
      // console.log(task)
      console.log(JSON.stringify(task, null, 2))
      if (task.type == 'test') {
        await this.checkTestStoryTask(task.testResult)
      } else if (task.type == 'send_story') {
        await this.checkSendStoryTask(task)
      } else if (task.type == 'broadcast') {
        let intervalTime = 10000
        task.broadcastResult.interval = setInterval(async () => {
          let result = await this.chatbotApi.broadcast.getItem(task.broadcastResult._id)
          // console.log('broadcast result', result)
          await this.checkBroadcastTask(result)
          this.changeRef.detectChanges()
        }, intervalTime)
        await this.checkBroadcastTask(task.broadcastResult)
      }
      this.changeRef.detectChanges()
    })
  }

  async checkTestStoryTask(testResult) {
    let testTask = testResult as iTask
    testTask.type = 'test'
    testTask.value = 100
    testTask.createdAt = moment().toISOString()
    testTask.subtitle = `<span ${testResult.success?`style="color: mediumseagreen"`:''}><i class="fas fa-check-circle"></i> Thành công: ${testResult.success}</span> 
    <span ${testResult.failed?`style="color: tomato"`:''}><i class="fas fa-exclamation-triangle"></i> Thất bại: ${testResult.failed?'<b>' + testResult.failed + '</b>':testResult.failed}</span>`
    if (testResult.success > 0) {
      testResult.processState = 'complete'
      testTask.title = 'Test câu chuyện thành công'
    } else {
      testResult.processState = 'failed'
      testTask.title = 'Test câu chuyện thất bại'
      testTask.error = testResult.lastFailedReason
    }

    this.pendingTasks.splice(0, 0, testTask)
    this.checkShowing()
  }

  async checkBroadcastTask(broadcastResult: iBroadcast) {
    let broadcastTask = broadcastResult as iTask
    broadcastTask.type = 'broadcast'

    let hasError = false, hasInProgress = false
    let reached
    for (let broadcast of broadcastResult.broadcasts) {
      if (broadcast.status == 'ERROR') {
        hasError = true
      } else if (broadcast.status == 'IN_PROGRESS') {
        hasInProgress = true
      }
      if (!reached || reached > broadcast.reached) { reached = broadcast.reached }
    }

    broadcastTask.value = 0
    if (hasError) broadcastTask.processState = 'failed'
    else if (hasInProgress) broadcastTask.processState = 'running'
    else { 
      broadcastTask.processState = 'complete'
      broadcastTask.value = 100
    }

    broadcastTask.subtitle = `<span>Số lượng thẻ: ${broadcastResult.broadcasts.length}</span> <span>Đã gửi: ${reached}</span>`
    if (broadcastTask.processState == 'running') {
      broadcastTask.title = 'Đang gửi broadcast'
    } else if (broadcastTask.processState == 'complete') {
      broadcastTask.title = 'Gửi xong broadcast'
      clearInterval(broadcastTask.interval)
    } else if (broadcastTask.processState == 'failed') {
      broadcastTask.title = 'Lỗi khi gửi broadcast'
      clearInterval(broadcastTask.interval)
    }

    let index = this.pendingTasks.findIndex(x => x._id == broadcastTask._id && x.type == 'broadcast')
    if (index > -1) {
      this.pendingTasks[index] = broadcastTask
    } else {
      this.pendingTasks.splice(0, 0, broadcastTask)
    }

    this.changeRef.detectChanges()
    this.checkShowing()
  }

  async checkSendStoryTask(task: iTask) {
    let index = this.pendingTasks.findIndex(x => x._id == task._id)
    if (index == -1) {
      index = this.completedTasks.findIndex(x => x._id == task._id)
      // console.log('not found finding index', index)
      if (index > -1) {
        this.pendingTasks.splice(0, 0, this.completedTasks[index])
        index = 0
      } else {
        index = -1
      }
    }

    if (index > -1) {
      if (this.pendingTasks[index].completeProcess > task.completeProcess)
        task.completeProcess = this.pendingTasks[index].completeProcess
      if (this.pendingTasks[index].failedProcess > task.failedProcess)
      task.failedProcess = this.pendingTasks[index].failedProcess
    }

    if (task.completeProcess + task.failedProcess < task.totalProcess) {
      task.processState = 'running'
    }

    task.subtitle = `<span ${task.completeProcess?`style="color: mediumseagreen"`:''}><i class="fas fa-check-circle"></i> ${task.completeProcess}</span> 
    <span ${task.failedProcess?`style="color: tomato"`:''}><i class="fas fa-exclamation-triangle"></i> ${task.failedProcess?'<b>' + task.failedProcess + '</b>':task.failedProcess}</span> 
    <span>Tổng: ${task.totalProcess}</span>
    <span>Đã xem: ${task.readCount}</span>`
    if (task.processState == 'running') {
      task.title = 'Đang gửi câu chuyện'
      if (task.payload && task.payload.lastFailedReason) task.error = task.payload.lastFailedReason
      task.value = (task.completeProcess + task.failedProcess) / task.totalProcess * 100
    } else if (task.processState == 'complete') {
      task.title = 'Gửi xong câu chuyện'
      if (task.payload && task.payload.lastFailedReason) task.error = task.payload.lastFailedReason
      task.value = 100
    } else if (task.processState == 'failed') {
      task.title = 'Gửi câu chuyện thất bại'
      if (task.payload && task.payload.lastFailedReason) task.error = task.payload.lastFailedReason
      task.value = 100
    }

    if (index > -1) { //if pending tasks already found
      this.pendingTasks[index] = task
    } else {
      this.pendingTasks.splice(0, 0, task)
    }
    this.checkShowing()
  }

  checkShowing() {
    if (this.manuallyShow) return
    let hasRunning = false, hasTest = false
    for (let task of this.pendingTasks) {
      if (task.type == 'test') hasTest = true
      if (task.processState === 'running') hasRunning = true
    }
    if (hasRunning) {
      this.show = true
      this.changeRef.detectChanges()
    } else {
      if (hasTest) this.show = true
      clearTimeout(this.closeTimeout)
      this.closeTimeout = setTimeout(() => {
        this.show = false
        this.closeTimeout = null
        this.changeRef.detectChanges()
      }, 10000)
    }
  }

  toggleShow() {
    this.show = !this.show
    this.manuallyShow = !this.manuallyShow
    this.changeRef.detectChanges()
  }

  notificationHovered() {
    if (this.closeTimeout) {
      clearTimeout(this.closeTimeout)
      this.closeTimeout = setTimeout(() => {
        this.show = false
        this.closeTimeout = null
        this.changeRef.detectChanges()
      }, 10000)
    }
  }

  movePendingTask(id) {
    let index = this.pendingTasks.findIndex(x => x._id == id)
    if (index > -1) {
      if (this.pendingTasks[index].timeout) clearTimeout(this.pendingTasks[index].timeout)
      this.completedTasks.splice(0, 0, this.pendingTasks.splice(index, 1))
      this.changeRef.detectChanges()
    }
    if (!this.pendingTasks.length) {
      this.show = false
    }
  }

  trackByFn(index, item) {
    return item._id
  }
}