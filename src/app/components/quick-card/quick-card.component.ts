import { Component, ChangeDetectionStrategy, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';
import { ChatbotApiService } from 'app/services/chatbot';
@Component({
  selector: 'app-quick-card',
  styleUrls: ['./quick-card.component.scss'],
  templateUrl: './quick-card.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuickCardComponent {

  @Input() cards: any[]
  @Input() disabled: boolean
  @Input() action: Function
  @Input() index: number
  @Output() onEditQuickCard = new EventEmitter<number>();
  constructor(
    private changeRef: ChangeDetectorRef,
    private chatbotApi: ChatbotApiService
  ) {
  }

  async ngOnDestroy() {
  }
  ngOnInit(){
  }
  // editQuickCard(event,index){
  //   event.stopPropagation()
  //   console.log( 'edit component')
  //   this.onEditQuickCard.emit(index)
  // }

}