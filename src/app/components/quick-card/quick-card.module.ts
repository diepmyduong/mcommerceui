import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { QuickCardComponent } from './quick-card.component';
import { MatProgressSpinnerModule, MatTooltipModule } from '@angular/material';
import { SafePipeModule } from 'app/shared/pipes/safe-pipe/safe-pipe.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatProgressSpinnerModule,
    SafePipeModule,
    MatTooltipModule,
  ],
  declarations: [QuickCardComponent],
  entryComponents: [QuickCardComponent],
  exports: [QuickCardComponent]
})
export class QuickCardModule { }
