import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatProgressSpinnerModule, MatTooltipModule } from '@angular/material';
import { SubscriberSelectionComponent } from './subscriber-selection.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatTooltipModule
  ],
  declarations: [SubscriberSelectionComponent],
  entryComponents: [SubscriberSelectionComponent],
  exports: [SubscriberSelectionComponent]
})
export class SubscriberSelectionModule { }
