import { Component, ChangeDetectionStrategy, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';
import { iSubscriber } from 'app/services/chatbot';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { PopoverService } from 'app/shared/popover-form/popover.service';
@Component({
  selector: 'app-subscriber-selection',
  styleUrls: ['./subscriber-selection.component.scss'],
  templateUrl: './subscriber-selection.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SubscriberSelectionComponent {

  @Input() name: string = 'Câu chuyện'
  @Input() subscribers: iSubscriber[]
  @Input() disabled: boolean
  @Output() change = new EventEmitter()

  constructor(
    public changeRef: ChangeDetectorRef,
    private popoverService: PopoverService
  ) {
  }

  async ngOnInit() {
  }

  async addSubscribers(event) {
    if (!this.subscribers) return
    
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      target: '.subscriber-selection-parent',
      targetElement: event.target,
      width: '96%',
      confirm: 'Cập nhật danh sách thông báo',
      horizontalAlign: 'center',
      verticalAlign: 'top',
      fields: [
        {
          type: 'subscribers',
          placeholder: 'Danh sách thông báo',
          property: 'subscribers',
          current: this.subscribers?[...this.subscribers]:[]
        }
      ],      
      onSubmit: async (data) => {
        if (!data.subscribers) return

        this.subscribers = data.subscribers
        this.change.emit(this.subscribers)
        this.changeRef.detectChanges()
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }
}