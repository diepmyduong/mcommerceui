import { Component, ChangeDetectionStrategy, ChangeDetectorRef, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { ChatbotApiService, iStory, iTopic } from 'app/services/chatbot';
import { PortalService } from 'app/services/portal.service';
import { Subscription } from 'rxjs';
import { StoriesPortalComponent } from 'app/chatgut-app/portals/stories-portal/stories-portal.component';
@Component({
  selector: 'app-story-selection',
  styleUrls: ['./story-selection.component.scss'],
  templateUrl: './story-selection.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StorySelectionComponent {

  @ViewChild('selectionWrapper', {read: ElementRef}) selectionWrapper: ElementRef
  @Input() title: string = 'Câu chuyện'
  @Input() multiple: boolean = false
  @Input() mode: 'story' | 'topic' = 'story'
  @Input() ids: string[]
  @Input() error: string = ''
  @Input() removable: boolean = false
  @Output() change = new EventEmitter()
  @Output() active = new EventEmitter(false)

  selected: iStory[] | iTopic[]
  topics: iTopic[]
  stories: iStory[]
  loadDone: boolean = false

  isNextOpened: boolean = false
  savedSubscription: Subscription
  closedSubscription: Subscription
  refreshSubscription: Subscription

  constructor(
    public changeRef: ChangeDetectorRef,
    private chatbotApi: ChatbotApiService,
    private portalService: PortalService
  ) {
  }

  async ngOnInit() {
    await this.reload()
  }

  async ngOnDestroy() {
    if (this.savedSubscription) this.savedSubscription.unsubscribe()
    if (this.closedSubscription) this.closedSubscription.unsubscribe()
    if (this.refreshSubscription) this.refreshSubscription.unsubscribe()
  }

  async reload(local = true) {
    try {
      this.loadDone = false
      if (this.mode == 'story') {
        this.stories = await this.chatbotApi.story.getList({
          local,
          query: {
            limit: 0,
            fields: ["_id", "name"],
            order: { 'createdAt': 1 },
            filter: { mode: 'normal' }
          }
        })
        this.selected = this.stories.filter(x => this.ids.includes(x._id))
      } else {
        this.topics = await this.chatbotApi.topic.getList({
          local,
          query: {
            limit: 0,
            fields: ["_id", "name"],
            order: { 'createdAt': 1 }
          }
        })
        this.selected = this.topics.filter(x => this.ids.includes(x._id))
      }

    } catch (err) {

    } finally {
      this.loadDone = true
      this.changeRef.detectChanges()
    }
  }

  async select() {
    if (this.mode == 'story') {
      this.ids = await this.getStory(this.ids)
      this.selected = this.stories.filter(x => this.ids.includes(x._id))
    } else {
      this.ids = await this.getTopic(this.ids)
      this.selected = this.topics.filter(x => this.ids.includes(x._id))
    }
    this.change.emit(this.ids)
    this.changeRef.detectChanges()
  }

  public async changeId(ids) {
    this.ids = ids
    if (this.mode == 'story') {
      this.selected = this.stories.filter(x => this.ids.includes(x._id))
    } else {
      this.selected = this.topics.filter(x => this.ids.includes(x._id))
    }
    this.changeRef.detectChanges()
  }

  removeItem(index) {
    console.log('here', index)
    this.selected.splice(index, 1)
    this.ids = (this.selected as any[]).map(x => x._id)
    this.change.emit(this.ids)
    this.changeRef.detectChanges()
  }
  
  getStory(ids: string[]) {
    return new Promise<string[]>(async (resolve, reject) => {
      let portalContentBlock = this.selectionWrapper.nativeElement.closest('.portal-content-block')
      let index, componentRef
      if (portalContentBlock && portalContentBlock.id) {
        index = this.portalService.container.portals.findIndex(x => x.portalId == portalContentBlock.id)
        componentRef = await this.portalService.pushPortalAt(index + 1, "StoriesPortalComponent", {
          select: true, multi: this.multiple, selectedStories: ids
        })
      } else {
        componentRef = await this.portalService.pushPortal("StoriesPortalComponent", {
          select: true, multi: this.multiple, selectedStories: ids
        })
      }
      this.isNextOpened = true;
      this.changeRef.detectChanges()
      this.active.emit(true)

      if (!componentRef) return;
      const component = componentRef.instance as StoriesPortalComponent

      this.refreshSubscription = component.onStoryRefresh.subscribe(result => {
        this.reload(result)
      })
      this.savedSubscription = component.onStorySaved.subscribe(selectedStory => {
        resolve(selectedStory)
        component.hideSave();
        component.close();
      })
      this.closedSubscription = component.onStoryClosed.subscribe(() => {
        this.isNextOpened = false;
        this.changeRef.detectChanges()
        this.savedSubscription.unsubscribe()
        this.closedSubscription.unsubscribe()
        this.refreshSubscription.unsubscribe()
        setTimeout(() => {
          this.active.emit(false)
        }, 500)
      })
    })
  }
  
  getTopic(ids: string[]) {
    return new Promise<string[]>(async (resolve, reject) => {
      let portalContentBlock = this.selectionWrapper.nativeElement.closest('.portal-content-block')
      let index = this.portalService.container.portals.findIndex(x => x.portalId == portalContentBlock.id)
      const componentRef = await this.portalService.pushPortalAt(index + 1, "TopicsPortalComponent", {
        select: true, multi: this.multiple, selectedTopics: ids
      })
      this.isNextOpened = true;
      this.changeRef.detectChanges()
      this.active.emit(true)

      if (!componentRef) return;
      const component = componentRef.instance as any

      this.refreshSubscription = component.onTopicRefresh.subscribe(result => {
        this.reload(result)
      })
      this.savedSubscription = component.onTopicSaved.subscribe(selectedTopic => {
        resolve(selectedTopic)
        component.hideSave();
        component.close();
      })
      this.closedSubscription = component.onTopicClosed.subscribe(() => {
        this.isNextOpened = false;
        this.changeRef.detectChanges()
        this.savedSubscription.unsubscribe()
        this.closedSubscription.unsubscribe()
        this.refreshSubscription.unsubscribe()
        setTimeout(() => {
          this.active.emit(false)
        }, 500)
      })
    })
  }
}