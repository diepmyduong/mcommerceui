import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material';
import { StorySelectionComponent } from './story-selection.component';
import { NameFilterPipeModule } from 'app/shared/pipes/name-filter-pipe/name-filter-pipe.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatProgressSpinnerModule,
    NameFilterPipeModule
  ],
  declarations: [StorySelectionComponent],
  entryComponents: [StorySelectionComponent],
  exports: [StorySelectionComponent]
})
export class StorySelectionModule { }
