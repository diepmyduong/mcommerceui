import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { EmojiComponent } from 'app/components/ngx-emoji-mart/emoji/emoji.component';
import { EmojiService } from 'app/components/ngx-emoji-mart/emoji/emoji.service';

@NgModule({
  imports: [CommonModule],
  exports: [EmojiComponent],
  declarations: [EmojiComponent],
  providers: [EmojiService],
})
export class EmojiModule {}
