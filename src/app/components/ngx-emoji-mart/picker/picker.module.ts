import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { EmojiModule } from 'app/components/ngx-emoji-mart/emoji/public_api';
import { AnchorsComponent } from 'app/components/ngx-emoji-mart/picker/anchors.component';
import { CategoryComponent } from 'app/components/ngx-emoji-mart/picker/category.component';
import { EmojiFrequentlyService } from 'app/components/ngx-emoji-mart/picker/emoji-frequently.service';
import { EmojiSearch } from 'app/components/ngx-emoji-mart/picker/emoji-search.service';
import { PickerComponent } from 'app/components/ngx-emoji-mart/picker/picker.component';
import { PreviewComponent } from 'app/components/ngx-emoji-mart/picker/preview.component';
import { SearchComponent } from 'app/components/ngx-emoji-mart/picker/search.component';
import { SkinComponent } from 'app/components/ngx-emoji-mart/picker/skins.component';

const components: any[] = [
  PickerComponent,
  AnchorsComponent,
  CategoryComponent,
  SearchComponent,
  PreviewComponent,
  SkinComponent,
];

@NgModule({
  imports: [CommonModule, FormsModule, EmojiModule],
  exports: components,
  declarations: components,
  providers: [EmojiSearch, EmojiFrequentlyService],
})
export class PickerModule {}
