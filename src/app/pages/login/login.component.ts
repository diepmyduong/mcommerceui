import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ChatbotApiService } from 'app/services/chatbot';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import * as Console from 'console-prefix';
import { AlertService } from 'app/services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;
  submitting: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  date: Date = new Date()
  year: any

  constructor(
    public chatbotApi: ChatbotApiService,
    public router: Router,
    public alert: AlertService
  ) {
    this.log = Console('[Login Page]').log
    this.year = this.date.getFullYear()
    
  }
  log: any

  async ngOnInit() {
    document.title = 'Login - MCOM Chatbot'
    if (await this.chatbotApi.chatbotAuth.authenticated) {
      this.log('already login success', 'firebase token', await this.chatbotApi.chatbotAuth.firebaseToken)
      this.toDashboard()
    } else {
      this.log('user not login')
    }
  }

  async login(form: NgForm) {

    if (this.submitting.getValue()) {
      return;
    }

    this.submitting.next(true);

    if (form.valid) {
      try {
        let { email, password } = this;
        let user = await this.chatbotApi.chatbotAuth.loginEmailAndPassword(email, password)
        // await this.checkEmailVerified()
        this.log('login success', user)
        // let user = await this.auth.loginWithEmailAndPassword(email,password);
      } catch (err) {
        this.log(err.code)
        switch (err.code) {
          case "auth/argument-error":
            this.alertAuthError("Vui lòng nhập đầy đủ dữ liệu")
            break;
          case "auth/operation-not-allowed":
            this.alertAuthError("Phương thức đăng nhập này chưa được cho phép")
            break;
          case "auth/user-disabled":
            this.alertAuthError("Tài khoản này đã bị khoá")
            break;
          case "auth/invalid-email":
            this.alertAuthError("Định dạng Email không đúng")
            break;
          case "auth/user-not-found":
            this.alertAuthError("Tài khoản không tồn tại")
            break;
          case "auth/wrong-password":
            this.alertAuthError("Mật khẩu không đúng")
            break;
          default:
            this.log(err)
            this.alertAuthError("Đăng nhập không thành công")

        }
        this.submitting.next(false)
      }
    } else {
      this.alertFormNotValid();
      this.submitting.next(false);
    }
  }

  async loginFacebook() {
    try {
      await this.chatbotApi.chatbotAuth.loginFacebook()
      this.toDashboard()
    } catch (err) {
      // console.error(err)
      this.alert.handleError(err);
      // this.alert.error('Lỗi khi đăng nhập', err.message)
    }
  }

  async loginGoogle() {
    try {
      await this.chatbotApi.chatbotAuth.loginGoogle()
      this.toDashboard()
    } catch (err) {
      // console.error(err)
      this.alert.handleError(err);
      // this.alert.error('Lỗi khi đăng nhập', err.message)
    }
  }

  logout() {
    this.chatbotApi.chatbotAuth.logout()
  }

  toDashboard() {
    try {
      const stateSnapshot = JSON.parse(sessionStorage.getItem('relogin_router_state_url'))
      if (stateSnapshot && stateSnapshot.path && stateSnapshot.queryParams) {
        this.router.navigate([stateSnapshot.path], { queryParams: stateSnapshot.queryParams })
        sessionStorage.removeItem('relogin_router_state_url')
      } else {
        this.router.navigate(["apps"])
      }
    } catch (err) {
      this.alert.handleError(err);
      // this.alert.error("Lỗi", err.message)
    }

  }

  async alertFormNotValid(message = "") {
    this.alert.info('Chú ý', 'Nội dung nhập không hợp lệ')
  }

  async alertAuthError(message = "") {
    this.alert.info('Chú ý', message)
  }

  async confirmResendVerifyEmail() {
    this.alert.info('Email chưa được xác thực', "Vui lòng kiểm tra lại họp thư")
  }

  keyDownFunction(event, form: NgForm) {
    if (event.keyCode == 13) {
      this.login(form)
    }
  }

  // checkEmailVerified() {
  //   return new Promise((resolve, reject) => {
  //     const firebaseUser = this.chatbotApi.chatbotAuth.firebaseUser
  //     if (!firebaseUser.emailVerified) {
  //       this.alert.warning('Email chưa được xác thực', "Vui lòng kiểm tra lại họp thư", 'Tôi chưa nhận được email')
  //       .then(async () => {
  //         await this.chatbotApi.chatbotAuth.sendVerifyEmail()
  //         this.alert.info('Email đã được gửi', `Email đã được gửi đén hộp thư ${firebaseUser.email}. Vui lòng kiểm tra lại hộp thư`)
  //       })
  //     } else {
  //       resolve(true)
  //     }
  //   })
  // }

}
