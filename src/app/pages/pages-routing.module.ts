import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from 'app/pages/login/login.component'
import { AppInviteComponent } from 'app/pages/app-invite/app-invite.component';
import { FirebaseAuthGuard } from 'app/services/chatbot';
import { QrcodeGeneratorComponent } from 'app/pages/qrcode-generator/qrcode-generator.component';

 
const routes: Routes = [{
  path: '',
  redirectTo: 'login',
  pathMatch: 'full',
}, {
  path: 'login',
  component: LoginComponent
}, {
  path: 'acceptInvite',
  component: AppInviteComponent,
  canActivate: [FirebaseAuthGuard],
}, {
  path: 'acceptInviteLink',
  component: AppInviteComponent,
  canActivate: [FirebaseAuthGuard],
}, {
  path: 'qrcodeGenerator',
  component: QrcodeGeneratorComponent,
  canActivate: [],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
