import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from 'app/base.component';
import { ChatbotApiService } from 'app/services/chatbot';
import { AlertService } from 'app/services/alert.service';

@Component({
  selector: 'app-app-invite',
  templateUrl: './app-invite.component.html',
  styleUrls: ['./app-invite.component.scss']
})
export class AppInviteComponent extends BaseComponent implements OnInit {

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public chatbotApi: ChatbotApiService,
    public alert: AlertService
  ) { 
    super("AppInviteComponent")
  }
  inviteToken: string

  async ngOnInit() {
    this.inviteToken = this.route.snapshot.queryParams["payload"]
    try {
      const payload = this.inviteToken.split("|")
      if(payload.length != 2) {
        this.toDashboard()
        this.log('error', 'bad invite payload')
        return
      }
      const appId = payload[0]
      const appToken = payload[1]
      let res
      if(location.pathname.includes('acceptInviteLink')){
        res = await this.chatbotApi.app.acceptInviteLink({ appId, appToken })
      }else{
        res = await this.chatbotApi.app.acceptInvite({ appId, appToken })
      }
      this.router.navigate(["apps", appId])
    } catch(err) {
      // this.log('accept invite error', err)
      // this.alert.error("Không thể thêm người dùng", err.message)
      this.alert.handleError(err, "Lỗi khi thêm người dùng");
      this.toDashboard()
    }

  }

  toDashboard() {
    this.router.navigate(["apps"])
  }

}
