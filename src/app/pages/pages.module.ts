import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'app/shared'
import { FlexLayoutModule } from '@angular/flex-layout';
import { ClipboardModule } from 'ngx-clipboard'
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { PagesRoutingModule } from 'app/pages/pages-routing.module';
import { LoginComponent } from 'app/pages/login/login.component';
import { AppInviteComponent } from 'app/pages/app-invite/app-invite.component';
import { QrcodeGeneratorComponent } from 'app/pages/qrcode-generator/qrcode-generator.component'


@NgModule({
  imports: [
    CommonModule,
    PagesRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    ClipboardModule,
  ],
  declarations: [LoginComponent, AppInviteComponent, QrcodeGeneratorComponent]
})
export class PagesModule { }
