import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { merge } from 'lodash-es'
import * as guid from 'guid'
import { iReferral } from 'app/services/chatbot';
import { ChatbotApiService } from 'app/services/chatbot';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IProgressBarOption } from 'app/chatgut-app/portals/portal-container/base-portal';
import { iEditInfoDialogData, EditInfoDialog } from 'app/modals/edit-info/edit-info.component';
import { MatDialog } from '@angular/material';
import { AlertService } from 'app/services/alert.service';
declare var Jimp, AwesomeQRCode: any
@Component({
  selector: 'app-qrcode-generator',
  templateUrl: './qrcode-generator.component.html',
  styleUrls: ['./qrcode-generator.component.scss']
})
export class QrcodeGeneratorComponent extends BaseComponent implements OnInit {

  constructor(
    public route: ActivatedRoute,
    public chatbotApi: ChatbotApiService,
    public dialog: MatDialog,
    public alert: AlertService
  ) {
    super("Qrcode Generator")
  }
  referral: iReferral
  qrCode: any = {
    id: guid.raw(),
    mode: 'mscode',
    src: "",
    option: {
      size: 500,
      margin: 0,
      dotScale: 1,
      whiteMargin: true,
      colorDark: "#000000",
      colorLight: "#ffffff",
      autoColor: false,
      maskedDots: false,
      backgroundDimming: 'rgba(0,0,0,0)',
      logoScale: 0.2,
      logoMargin: 6,
      logoCornerRadius: 500 * 0.2 / 2,
    }
  }
  progressBar: IProgressBarOption = {
    show: false,
    color: 'primary',
    mode: 'query',
    value: 0,
    buffer: 0
  }

  async ngOnInit() {
    const query = this.route.snapshot.queryParams
    if (!query.link || !query.mscode) {

    } else {
      this.referral = {
        link: query.link,
        messengerCode: query.mscode
      }
      this.qrCode.mode = 'mscode'
      this.qrCode.src = this.referral.messengerCode
      // const mscode = await Jimp.read(this.referral.messengerCode);
      if (this.referral.customQRCode && this.referral.customQRCode.logo) {
        await this.createMSCodeWithLogo(this.referral.customQRCode.logo)
      }
    }
  }

  getJimpBase64(jimp, mime: string = 'image/png') {
    return new Promise<string>((resolve, reject) => {
      jimp.getBase64(mime, (err, data) => {
        if (err) {
          reject(err)
          return
        }
        resolve(data)
      })
    })
  }

  getAwesomeQRData(setting: any) {
    return new Promise((resolve, reject) => {
      const awesomeQR = new AwesomeQRCode()
      if (setting.logoType === 'circle' || setting.logoCornerRadius > (setting.size * setting.logoScale / 2)) {
        setting.logoCornerRadius = setting.size * setting.logoScale / 2
      }
      setting.callback = (data) => {
        if (data) resolve(data)
        else reject()
      }
      awesomeQR.create(setting);
    })
  }

  getBufferFromSrc(src: string) {
    return new Promise(async (resolve, reject) => {
      const jimp = await Jimp.read(src)
      jimp.getBuffer('image/png', (err, buff) => {
        if (err) {
          reject(err)
          return
        } else {
          resolve(buff)
        }
      })
    })
  }

  getBlobFromURL(url: string) {
    return new Promise<Blob>((resolve, reject) => {
      var blob = null;
      var xhr = new XMLHttpRequest();
      xhr.open("GET", url);
      xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
      xhr.onload = () => {
        blob = xhr.response;//xhr.response is now a blob object
        resolve(blob)
      }
      xhr.send();
    })
  }

  async createMSCodeWithLogo(logoUrl: string) {
    const mscode = await Jimp.read(this.referral.messengerCode);
    const logo = await Jimp.read(logoUrl)
    const mask = await Jimp.read("assets/img/mscode-mask.png")
    logo.cover(500, 500)
    mscode.resize(500, 500)
    logo.mask(mask, 0, 0)
    mscode.composite(logo, 0, 0)
    this.qrCode.mode = 'mscodewithlogo'
    this.qrCode.src = await this.getJimpBase64(mscode)
  }

  async createLogoCode(src: string, option: any) {
    var image = new Image()
    image.crossOrigin = "Anonymous";
    this.showLoading()
    image.onload = async () => {
      try {
        const setting = merge({}, option)
        setting.text = this.referral.link
        setting.logoImage = image
        setting.backgroundImage = undefined
        setting.gifBackground = undefined
        setting.bindElement = this.qrCode.id
        this.qrCode.src = await this.getAwesomeQRData(setting)
        // const jimp = await Jimp.read(await this.getAwesomeQRData(setting))
        // this.qrCode.src = await this.getJimpBase64(jimp)
        this.qrCode.mode = "logo"
      } catch(err){
        this.alert.handleError(err, "Lỗi khi tạo mới");
        // this.alert.error("Lỗi", err.message)
      }finally {
        this.hideLoading()
      }
    }
    image.src = await this.chatbotApi.deeplink.getImageBufferLink({ src })
  }

  async createBackgroudCode(src: string, option: any) {
    var image = new Image();
    image.crossOrigin = "Anonymous";
    this.showLoading()
    image.onload = async () => {
      try {
        const setting = merge({}, option)
        setting.text = this.referral.link
        setting.logoImage = undefined
        setting.gifBackground = undefined
        setting.backgroundImage = image
        // const jimp = await Jimp.read(await this.getAwesomeQRData(setting))
        this.qrCode.src = await this.getAwesomeQRData(setting)
        this.qrCode.mode = "backgroud"
      } catch(err){
        this.alert.handleError(err);
        // this.alert.error("Lỗi", err.message)
      } finally {
        this.hideLoading()
      }
    }
    image.src = await this.chatbotApi.deeplink.getImageBufferLink({ src })
  }

  async createGifCode(src: string, option: any) {
    const r = new FileReader();
    r.onload = async (e: any) => {
      // get the ArrayBuffer

      try {
        const setting = merge({}, option)
        setting.text = this.referral.link
        setting.logoImage = undefined
        setting.backgroundImage = undefined
        setting.gifBackground = e.target.result
        // const jimp = await Jimp.read(await this.getAwesomeQRData(setting))
        this.qrCode.src = await this.getAwesomeQRData(setting)
        this.qrCode.mode = "gif"
      } catch(err){
        this.alert.handleError(err, "Tạo thất bại");
        // this.alert.error("Lỗi", err.message)
      } finally {
        this.hideLoading()
      }
    };
    // read as ArrayBuffer
    r.readAsArrayBuffer(await this.getBlobFromURL(await this.chatbotApi.deeplink.getImageBufferLink({ src })));
  }

  async onCreateMSCodeWithLogo() {
    if (!this.qrCode.option) this.qrCode.option = {}
    if (!this.qrCode.option.logoUrl) this.qrCode.option.logoUrl = await this.getURLDialog()
    if (this.qrCode.option.logoUrl) {
      try {
        this.showLoading()
        const src = await this.chatbotApi.deeplink.getImageBufferLink({ src: this.qrCode.option.logoUrl })
        await this.createMSCodeWithLogo(src)
      } catch (err) {
        this.alert.handleError(err, "Tạo thất bại");
        // this.alert.error('Không thể tạo QR Code', err.message)
      } finally {
        this.hideLoading()
      }
    }
  }

  async onCreateMSCode() {
    this.qrCode.mode = 'mscode'
    this.qrCode.src = this.referral.messengerCode
  }

  async onCreateSimpleCode() {
    const setting = merge({}, this.qrCode.option)
    setting.text = this.referral.link
    // const jimp = await Jimp.read(await this.getAwesomeQRData(setting))
    // this.qrCode.src = await this.getJimpBase64(jimp)
    this.qrCode.src = await this.getAwesomeQRData(setting)
    this.qrCode.mode = "simple"
  }

  async onCreateLogoCode() {
    if (!this.qrCode.option) this.qrCode.option = {}
    if (!this.qrCode.option.logoUrl) this.qrCode.option.logoUrl = await this.getURLDialog()
    if (this.qrCode.option.logoUrl) await this.createLogoCode(this.qrCode.option.logoUrl, this.qrCode.option)
  }

  async onCreateBackgroudCode() {
    if (!this.qrCode.option) this.qrCode.option = {}
    if (!this.qrCode.option.backgroudUrl) this.qrCode.option.backgroudUrl = await this.getURLDialog()
    if (this.qrCode.option.backgroudUrl) await this.createBackgroudCode(this.qrCode.option.backgroudUrl, this.qrCode.option)
  }

  async onCreateGifCode(reloadTime: number = 0) {
    if (reloadTime === 3) {
      this.hideLoading()
      return;
    }
    if (!this.qrCode.option) this.qrCode.option = {}
    if (!this.qrCode.option.gifUrl) this.qrCode.option.gifUrl = await this.getURLDialog()
    if (this.qrCode.option.gifUrl) await this.createGifCode(this.qrCode.option.gifUrl, this.qrCode.option)

  }
  
  async getURLDialog() {
    let data: iEditInfoDialogData = {
      title: "Nhập link ảnh",
      confirmButtonText: "Xác nhận",
      inputs: [{
        title: "URL",
        property: "url",
        current: "",
        type: "text",
        required: true,
      }]
    };

    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    });

    return (await dialogRef.afterClosed().toPromise()).url
  }


  async applyQRCode(frmCtrl: NgForm) {
    this.showLoading()
    try {
      switch (this.qrCode.mode) {
        case 'mscodewithlogo':
          await this.createMSCodeWithLogo(frmCtrl.controls['logo'].value)
          break
        case 'simple':
          await this.onCreateSimpleCode()
          break
        case 'logo':
          await this.createLogoCode(frmCtrl.controls['logo'].value, this.qrCode.option)
          break
        case 'backgroud':
          await this.createBackgroudCode(frmCtrl.controls['backgroud'].value, this.qrCode.option)
          break
        case 'gif':
          await this.createGifCode(frmCtrl.controls['gif'].value, this.qrCode.option)
          break
      }
    } catch(err){
      this.alert.handleError(err, "Tạo thất bại");
      // this.alert.error("Lỗi", err.message)
    } finally {
      this.hideLoading()
    }

  }

  showLoading() {
    setTimeout(() => { this.progressBar.show = true })
  }

  hideLoading() {
    setTimeout(() => { this.progressBar.show = false })
  }

}
