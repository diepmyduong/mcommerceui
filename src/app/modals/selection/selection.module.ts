import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectionDialog } from 'app/modals/selection/selection.component';
import { FormsModule } from '@angular/forms';
import { MatGridListModule, MatButtonModule, MatRadioModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatDialogModule } from '@angular/material';
import { MatRippleModule } from '@angular/material/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatRadioModule,
    MatCheckboxModule,
    MatGridListModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatDialogModule
  ],
  declarations: [SelectionDialog],
  exports: [SelectionDialog],
  entryComponents: [SelectionDialog]
})
export class SelectionModule { }
