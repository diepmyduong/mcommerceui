import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { merge } from 'lodash-es'
@Component({
  selector: 'selection-dialog',
  styleUrls: ['./selection.component.scss'],
  templateUrl: './selection.component.html'
})
export class SelectionDialog implements OnInit {

  info = {};
  errorNum = 0;
  selection: any;
  // otherOptions: any = {
  //   radio: '',
  //   checkbox: {},
  //   date:{}
  // }
  colNum: number[] = []
  overlay: Element
  // scheduleDate: moment.Moment
  // scheduleTime: ITime
  constructor(
    public dialogRef: MatDialogRef<SelectionDialog>,
    @Inject(MAT_DIALOG_DATA) public data: iSelectionData) {
    this.data = merge({
      confirmButtonText: 'Đồng ý',
      cancelButtonText: 'Huỷ'
    }, data)

    this.overlay = document.getElementsByClassName('cdk-global-overlay-wrapper').item(0)
    this.overlay.classList.add('overflow')
    this.overlay.addEventListener('click', event => this.overlayClickEvent(event) )

    // for (let cat of this.data.categories) {
    //   for (let opt of cat.options) {
    //     if (!opt.disabled) {
    //       this.selection = opt.code
    //       break;
    //     }
    //   }
    //   if (this.selection) break;
    // }

    // if (this.data.otherOptions) {
    //   if (this.data.otherOptions.radio) {
    //     for (let opt of this.data.otherOptions.radio) {
    //       if (!opt.disabled) {
    //         this.otherOptions.radio = opt.code
    //         break;
    //       }
    //     }
    //   }
    //   if (this.data.otherOptions.checkbox) {
    //     for (let opt of this.data.otherOptions.checkbox) {
    //       if (opt.checked) {
    //         this.otherOptions.checkbox[opt.code] = true
    //       } else {
    //         this.otherOptions.checkbox[opt.code] = false
    //       }
    //     }
    //   }
    //   if (this.data.otherOptions.date) {
    //     this.otherOptions.date.hasSchedule = this.data.otherOptions.date.hasSchedule
    //   }
    // }

    // this.selection = this.data.categories[0].options[0].code;

    let maxLength = 0;
    let cat;
    for (let i = 0; i < this.data.categories.length; i++) {
      cat = this.data.categories[i];
      let expanding = localStorage.getItem(`selection-${this.data.title}-${i}`)
      if (expanding && expanding == 'true') cat.defaultToggle = true
      maxLength = 0;
      for (let opt of cat.options) {
        if(opt.image) {
          break;
        } else {
          if (opt.display.length > maxLength) {
            maxLength = opt.display.length;
          }
        }
      }
      if (window.innerWidth < 414) {
        this.colNum[i] = 1;
      } else {
        if (maxLength > 20) {
          if (maxLength > 28) {
            this.colNum[i] = 1;
          } else {
            this.colNum[i] = 2;
          }
        } else {
          this.colNum[i] = 3;
        }
      }

    }
  }

  ngOnInit() {
   
    // this.scheduleDate = moment()

    // this.scheduleTime = {
    //   hour: moment().add(30,'minutes').get('hour'),
    //   minute: moment().add(30,'minutes').get('minute'),
    // } 
    // console.log( 'schedule',this.scheduleTime)
  }

  ngAfterViewInit() {
    // this.selection = {};
    // this.selection.source = document.getElementById('mat-button-toggle-0');
    // this.selection.value = this.selection.source.value;
  }

  ngOnDestroy() {
    this.overlay.removeEventListener('click', event => this.overlayClickEvent(event) )
  }

  selectionToggled(code) {
    this.selection = code
    this.onYesClick()
  }

  onYesClick() {
    let errorFlag = false;

    // for (let i = 0; i < this.data.inputs.length; i++) {
    //   let input = this.data.inputs[i];
    //   if (input.required && !this.info[input.property]) {
    //     input.error = "Bắt buộc nhập";
    //     this.errorNum++;
    //     errorFlag = true;
    //     continue;
    //   }
    // }
    if (errorFlag) {
      return;
    }

    // if (this.data.otherOptions) {
    //   if(this.otherOptions.date.hasSchedule){
    //     let scheduleTime = moment(this.scheduleDate)
    //     scheduleTime.hour(this.scheduleTime.hour)
    //     scheduleTime.minutes(this.scheduleTime.minute)
    //     this.otherOptions.date.value = scheduleTime.format()
    //   }
    //   this.dialogRef.close({
    //     category: this.selection,
    //     otherOptions: this.otherOptions
    //   });
    // } else {
      this.dialogRef.close(this.selection);
    // }
  }

  overlayClickEvent(e) {
    if (e.srcElement && e.srcElement.classList.contains('cdk-global-overlay-wrapper')) {
      this.onNoClick()
    }
  }

  onNoClick(): void {
    this.dialogRef.close()
  }
  onExpandClick(index) {
    this.data.categories[index].defaultToggle = !this.data.categories[index].defaultToggle
    localStorage.setItem(`selection-${this.data.title}-${index}`, this.data.categories[index].defaultToggle?'true':'false')
  }
}

export interface iSelectionData {
  title: string
  confirmButtonText?: string
  cancelButtonText?: string
  // confirmButtonClass?: string
  // cancelButtonClass?: string
  // otherOptions?: {
  //   radio?: {
  //     code: string,
  //     display: string,
  //     disabled?: boolean
  //   }[],
  //   checkbox?: {
  //     code: string,
  //     display: string,
  //     checked?: boolean
  //   }[],
  //   date?:{
  //     code:string,
  //     date?:Date,
  //     display:string,
  //     hasSchedule?:boolean
  //   }
  // }
  categories: {
    name: string,
    canToggle?: boolean,
    defaultToggle?: boolean,
    options: {  
      code: string,
      display: string,
      icon: string,
      disabled?: boolean
    }[]
  }[]
}