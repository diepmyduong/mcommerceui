import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { AlertService } from 'app/services/alert.service';
import { ChatbotApiService, iApp, iCard, iInteractPost, iStory } from 'app/services/chatbot';
import { iLuckyWheel } from 'app/services/chatbot/api/crud/luckyWheel';
import { iStoryGroup } from 'app/services/chatbot/api/crud/storyGroup';
import { cloneDeep } from 'lodash-es';
import * as moment from 'moment';
import { BehaviorSubject } from 'rxjs';
@Component({
  selector: 'app-copy-dialog',
  styleUrls: ['./copy.component.scss'],
  templateUrl: './copy.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CopyDialog implements OnInit {
  apps: iApp[]
  storyGroups: iStoryGroup[] = []
  stories: iStory[] = []
  selectedStory: iStory
  selectedApp: iApp
  result: any
  modeDisplay = {
    card: 'thẻ',
    story: 'câu chuyện',
    storyGroup: 'nhóm câu chuyện',
    hashtag: 'hashtag',
    wheel: 'vòng quay'
  }
  appSearchText: string
  searchText: string
  success: boolean = false
  copying: boolean = false
  loadDone: boolean = true

  targetStoryGroupId: string = ''
  targetStoryGroups$ = new BehaviorSubject<iStoryGroup[]>([])

  constructor(
    public dialogRef: MatDialogRef<CopyDialog>,
    public chatbotApi: ChatbotApiService,
    @Inject(MAT_DIALOG_DATA) public data: iCopyData,
    private alert: AlertService,
    private changeRef: ChangeDetectorRef,
    private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.reload()
  }

  async reload(local = true) {
    try {
        let tasks = []
        this.loadDone = false
        this.changeRef.detectChanges()
        switch (this.data.mode) {
          case 'story': case 'storyGroup': case 'hashtag': case 'wheel': {
            tasks.push(this.chatbotApi.app.getList({
              local: true,
              query: {
                order: {
                  createdAt: -1
                },
                populates: ["activePage", "license"],
              }
            }).then(result => {
              this.apps = result
              console.log(result)
              this.apps.forEach(x => {
                x.disabled = x.whiteListed?false:this.isExpired(x.license.expireDate)
              })
              if (this.data.app) {
                this.selectedApp = this.data.app
                setTimeout(() => {
                  let items = document.getElementsByClassName('copy-dialog-wrapper').item(0).getElementsByClassName('btn-app')
                  for (let i = 0; i < items.length; i++) {
                    let item = items.item(i)
                    if (item.classList.contains('active')) {
                      item.scrollIntoView()
                      break
                    }
                  }
                })
              } else {
                this.selectedApp = this.apps[0]
              }
              this.getStoryGroupsList()
            }))
            await Promise.all(tasks)
            break;
          }
          case 'card': {
            let stories = []
            tasks.push(this.chatbotApi.storyGroup.getList({ local, query: {
                limit: 0,
                fields: ["_id", "name", "type", "stories", "sequenceEvents"],
                filter: { type: this.data.story.mode }, 
                order: { 'createdAt': 1 } 
            } }).then(result => {
              this.storyGroups = cloneDeep(result)
            }))

            tasks.push(this.chatbotApi.story.getList({ local, query: {
                limit: 0,
                fields: ["_id", "name"],
                order: { 'createdAt': 1 },
                filter: { mode: this.data.story.mode }
            } })
            .then(result => {
                this.stories = cloneDeep(result)
            }))
            await Promise.all(tasks)
            
            for (let group of this.storyGroups) {
              for (let i = 0; i < group.stories.length; i++) {
                if (!group.stories[i]) {
                    group.stories.splice(i, 1)
                    i--
                    return
                }
                let index = this.stories.findIndex(x => x._id == group.stories[i])
                if (index >= 0) {
                    group.stories[i] = this.stories[index]
                    this.stories.splice(index, 1)
                } else {
                    group.stories.splice(i, 1)
                    i--
                }
              }
            }

            let generalStoryGroup: iStoryGroup = {
                _id: "0",
                name: 'Mặc định',
                type: 'normal',
                stories: [],
                sequenceEvents: [],
                isDefault: true,
                expanded: true,
            }
            if (this.storyGroups[0] && this.storyGroups[0].isDefault) {
                this.storyGroups[0].stories = [...this.stories]
            } else {
                generalStoryGroup.stories = [...this.stories]
                this.storyGroups.splice(0, 0, generalStoryGroup)
            }

            this.selectedStory = null
            if (this.data.story) {
              this.selectedStory = this.data.story
              setTimeout(() => {
                let items = document.getElementsByClassName('copy-dialog-wrapper').item(0).getElementsByClassName('story-item')
                for (let i = 0; i < items.length; i++) {
                  let item = items.item(i)
                  if (item.classList.contains('active')) {
                    item.scrollIntoView()
                    break
                  }
                }
              })
            } else {
              for (let group of this.storyGroups) {
                if (group.stories.length) {
                  this.selectedStory = group.stories[0] as iStory
                  break
                }
              }
            }
            break
          }
        }
    } catch (err) {
        // console.error(err)
        this.dialogRef.close()
        this.alert.handleError(err);
        // this.alert.error('Lỗi xảy ra', err.message)
    } finally {
        this.loadDone = true
        this.changeRef.detectChanges()
        setTimeout(() => {
          if (this.data.mode != 'card') {
            document.getElementById('app-filter').focus()
          }
        }, 300)
    }
  }
  
  openStory() {
    switch (this.data.mode) {
      case 'card':
      this.dialogRef.close(this.selectedStory)
      break;
      case 'story':
      this.dialogRef.close({ story: this.result, app: this.selectedApp })
      break;
      case 'storyGroup':
      this.dialogRef.close(this.selectedApp)
      break;
      case 'hashtag':
      this.dialogRef.close({ interactPost: this.result, app: this.selectedApp })
      break;
      case 'wheel':
      this.dialogRef.close({ wheel: this.result, app: this.selectedApp })
      break;
    }
  }

  getStoryGroupsList() {
    this.targetStoryGroupId = ''
    this.targetStoryGroups$.next([])
    this.chatbotApi.storyGroup.getList({
      local: false,
      query: {
        limit: 1000,
        fields: ['_id', 'name']
      },
      headers: {
        'x-api-key': `A|${this.selectedApp._id}|${this.selectedApp.accessToken}`
      }
    }).then(res => {
      this.targetStoryGroups$.next(res)
    })
  }

  onYesClick() {

  }

  onNoClick(): void {
    this.dialogRef.close()
  }

  isExpired(date) {
    if (!date) return false
    let expiredMoment = moment(date);
    let expiredMomentUTC = expiredMoment.utc()
    let nowUTC = moment.utc()
    let diffDuration = moment.duration(expiredMomentUTC.diff(nowUTC))
    let minDiff = diffDuration.asMinutes();
    if (minDiff > 0) {
      return false
    } else {
      return true
    }
  }

  async copy() {
    try {
      this.copying = true
      this.changeRef.detectChanges()
      let app = this.chatbotApi.chatbotAuth.app
      switch(this.data.mode) {
        case 'card':
        this.result = await this.chatbotApi.card.copy(app._id, app.accessToken, { destinationStoryId: this.selectedStory._id, cardId: this.data.card._id })
        break;
        case 'story':
        this.result = await this.chatbotApi.story.copy(this.selectedApp._id,this.selectedApp.accessToken,
          { name: this.data.story.name, storyId: this.data.story._id, storyGroupId: this.targetStoryGroupId })
        break;
        case 'storyGroup':
        this.result = await this.chatbotApi.storyGroup.copy(this.selectedApp._id,this.selectedApp.accessToken, this.data.storyGroup._id)
        break;
        case 'hashtag':
        this.result = await this.chatbotApi.interactPost.copy(this.selectedApp._id,this.selectedApp.accessToken, this.data.interactPost._id)
        break;
        case 'wheel':
        this.result = await this.chatbotApi.luckyWheel.copy(this.selectedApp._id,this.selectedApp.accessToken, this.data.wheel._id)
        break;
      }
      this.success = true
      this.snackBar.open('✔️ Sao chép thành công', '', {
          duration: 2500
      });
    } catch (err) {
      // console.error(err)
      this.alert.handleError(err, "Lỗi khi sao chép");
      // this.alert.error("Lỗi khi sao chép", err.message)
    } finally {
      this.copying = false
      this.changeRef.detectChanges()
    }
  }
}

export interface iCopyData {
  mode: 'card' | 'story' | 'storyGroup' | 'hashtag' | 'wheel'
  name: string
  type: string
  card?: iCard
  story?: iStory
  storyGroup?: iStoryGroup
  app?: iApp
  interactPost?: iInteractPost
  wheel?: iLuckyWheel
}

export interface iCopyStoryData {
  title: string
  confirmButtonText?: string
  cancelButtonText?: string
  confirmButtonClass?: string
  cancelButtonClass?: string
  inputs?: {
    title?: string,
    property?: string,
    type?: string,
    required?: true,
    className?: string,
    [key: string]: any
  }[],
  options?: {
    selectTitle?: string,
    defaultValue?: string,
    [key: string]: any
  }[],
  [key: string]: any
}