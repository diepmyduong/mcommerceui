import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatProgressSpinnerModule, MatSnackBarModule, MatRadioModule, MatCheckboxModule, MatGridListModule, MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule, MatDialogModule } from '@angular/material';
import { CopyDialog } from './copy.component';
import { NameFilterPipeModule } from 'app/shared/pipes/name-filter-pipe/name-filter-pipe.module';
import { MaterialTimeControlModule } from 'app/shared';
import { NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { AppFilterPipeModule } from 'app/shared/pipes/app-filter-pipe/app-filter-pipe.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatDialogModule,
    NameFilterPipeModule,
    AppFilterPipeModule
  ],
  declarations: [CopyDialog],
  entryComponents: [CopyDialog],
  exports: [CopyDialog]
})
export class CopyModule { }
