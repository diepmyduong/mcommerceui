import { Component, Inject, ChangeDetectionStrategy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { iApp, ChatbotApiService } from 'app/services/chatbot';
import { FormGroup, FormBuilder } from '@angular/forms';
import { debounceTime, tap, switchMap, filter, takeUntil } from 'rxjs/operators';
import { BehaviorSubject, Subject } from 'rxjs';
import { iCustomer } from 'app/services/chatbot/api/crud/customer';
@Component({
  selector: 'app-invite-customer-dialog',
  styleUrls: ['./app-invite-customer.component.scss'],
  templateUrl: './app-invite-customer.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppInviteCustomerDialog {

  app: iApp
  overlay: HTMLElement
  customerForm: FormGroup = this.fb.group({
    customerInput: null
  })
  loading$ = new BehaviorSubject<boolean>(false)
  customers$ = new BehaviorSubject<iCustomer[]>(undefined)

  destroy$ = new Subject()
  customerSearch: any
  customer$ = new BehaviorSubject<iCustomer>(undefined)

  role: string = 'admin'
  userRoles:any[] = [
    { code:'admin',display:"Admin" },
    { code:'write',display:"Write" },
    { code:'read',display:"Read" },
    { code:'chatter',display:"Chat" },
  ]

  submitting$ = new BehaviorSubject<boolean>(false)
  success$ = new BehaviorSubject<boolean>(false)

  constructor(
    public dialogRef: MatDialogRef<AppInviteCustomerDialog>,
    @Inject(MAT_DIALOG_DATA) public data: iApp,
    private chatbotApi: ChatbotApiService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar) {
    this.app = data

    this.overlay = document.getElementsByClassName('cdk-global-overlay-wrapper').item(0) as HTMLElement
    this.overlay.classList.add('overflow')
    this.overlay.addEventListener('click', event => this.overlayClickEvent(event) )
  }

  ngAfterViewInit() {

    let adminToken = this.chatbotApi.chatbotAuth.systemUser.token
    this.customerForm.get('customerInput')
    .valueChanges
    .pipe(
      takeUntil(this.destroy$),
      filter(Boolean),
      debounceTime(300),
      tap(() => this.loading$.next(true)),
      switchMap(value => this.chatbotApi.customer.getList({
        headers: {
          'x-api-key': adminToken
        },
        local: false, query: {
          limit: 10,
          filter: { $or: [
            { fullName: { $regex: value } },
            { email: { $regex: value } },
          ] }
        }
      })
      .then(res => {
        this.customers$.next(res)
        this.loading$.next(false)
      })
      .catch(() => this.loading$.next(false))
      )
    )
    .subscribe();
  }

  customerChanged(event) {
    if (typeof(event)=='object' && event._id) {
      this.customer$.next(event)
    } else {
      this.customer$.next(undefined)
    }
  }

  removeCustomer() {
    this.customerSearch = ''
    this.customer$.next(undefined)
  }

  displayFn(customer: iCustomer) {
    if (customer) { return customer.fullName + ' - ' + customer.email }
  }

  ngOnDestroy() {
    this.destroy$.next()
    this.overlay.removeEventListener('click', event => this.overlayClickEvent(event) )
  }

  async inviteCustomer() {
    if (!this.customer$.getValue()) return
    try {
      this.submitting$.next(true)
      await this.chatbotApi.app.inviteToApp(this.app._id, this.customer$.getValue()._id, this.role)
      await this.chatbotApi.app.getItem(this.app._id, { local: false }) as iApp
      this.snackBar.open(`✔️ Thêm khách hàng thành công`, '', { duration: 3000 });
      this.success$.next(true)
      setTimeout(() => {
        this.dialogRef.close(true)
      }, 1000)
    } catch (err) {
      console.error(err)
      this.snackBar.open(`❌ Thêm khách hàng thất bại`, err.message, { duration: 3000 })
    } finally {
      this.submitting$.next(false)
    }
  }

  overlayClickEvent(e) {
    if (e.srcElement && e.srcElement.classList.contains('cdk-global-overlay-wrapper')) {
      this.onNoClick()
    }
  }

  onNoClick(): void {
    this.dialogRef.close()
  }
}
