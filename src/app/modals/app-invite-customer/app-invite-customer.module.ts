import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppInviteCustomerDialog } from 'app/modals/app-invite-customer/app-invite-customer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatFormFieldModule, MatInputModule, MatAutocompleteModule, MatProgressSpinnerModule, MatSelectModule, MatSnackBarModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatFormFieldModule, 
    MatInputModule,
    MatSelectModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule
  ],
  declarations: [AppInviteCustomerDialog],
  exports: [AppInviteCustomerDialog],
  entryComponents: [AppInviteCustomerDialog]
})
export class AppInviteCustomerModule { }
