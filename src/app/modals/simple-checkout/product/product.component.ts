import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { BaseComponent } from 'app/base.component';


@Component({
  selector: 'simple-checkout-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class SimpleCheckoutProductComponent extends BaseComponent implements OnInit {

  @Input() product: any
  @Output() onChange = new EventEmitter<any>()
  @Output() remove = new EventEmitter<any>()
  @ViewChild('myFrm') myFrm;
  constructor() { 
    super("SimpleCheckoutProductComponent")
  }
  toppings = {}
  ngOnInit() {
  }
  ngAfterViewInit() {
    for(let t of this.product.product.toppings) {
      if(t.topping.is_select_multiple) {
        this.toppings[t.topping.id] = []
        setTimeout(() => {
          this.toppings[t.topping.id] = t.topping.values.filter(value => this.product.topping_value_ids.indexOf(value.id) != -1).map(v => v.id)
        })
      } else {
        setTimeout(() => {
          const value = t.topping.values.find(value => this.product.topping_value_ids.indexOf(value.id) != -1)
          this.toppings[t.topping.id] = value?value.id:undefined
        })
      }
    } 
    setTimeout(() => {
      this.subscriptions.push(this.myFrm.valueChanges.debounceTime(500).subscribe(data => {
        this.product.topping_value_ids = []
        for(let id of Object.keys(this.toppings)) {
          if(this.toppings[id] === undefined) continue;
          if(typeof this.toppings[id] === "string") {
            this.product.topping_value_ids.push(this.toppings[id])
          } else {
            this.product.topping_value_ids = this.product.topping_value_ids.concat(this.toppings[id])
          }
        }
        this.onChange.emit(this.product)
      }));
    }, 500)
    
  }
  async removeProduct() {
    this.remove.emit(this.product)
  }
  selectToppingChange() {

  }

}
