import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleCheckoutDialog } from 'app/modals/simple-checkout/simple-checkout.component';
import { SimpleCheckoutProductComponent } from 'app/modals/simple-checkout/product/product.component';
import { MatExpansionModule, MatIconModule, MatTooltipModule, MatSelectModule, MatFormFieldModule, MatInputModule, MatDividerModule, MatAutocompleteModule, MatButtonModule, MatDialogModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatExpansionModule,
    MatIconModule,
    MatTooltipModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule,
    MatAutocompleteModule,
    MatButtonModule,
    FlexLayoutModule,
    FormsModule,
    MatDialogModule
  ],
  declarations: [SimpleCheckoutDialog, SimpleCheckoutProductComponent],
  exports: [SimpleCheckoutDialog],
  entryComponents: [SimpleCheckoutDialog]
})
export class SimpleCheckoutModule { }
