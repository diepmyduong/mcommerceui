import { cloneDeep, merge } from 'lodash-es'
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatAutocompleteSelectedEvent } from '@angular/material';
import { NgForm, NgModel } from '@angular/forms';
import { ChatbotApiService } from 'app/services/chatbot';
import { BehaviorSubject } from 'rxjs';
import { ServicesInjector } from 'app/services/serviceInjector';

@Component({
  selector: 'app-simple-checkout',
  templateUrl: './simple-checkout.component.html',
  styleUrls: ['./simple-checkout.component.scss']
})
export class SimpleCheckoutDialog implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<SimpleCheckoutDialog>,
    @Inject(MAT_DIALOG_DATA) public button: any) {
  }
  products: any[] = []
  payload: any = {}
  mappingProducts: any[] = []
  autocompleteFilter = new BehaviorSubject<any[]>([])
  productInput: string = ""
  async ngOnInit() {
    const api = ServicesInjector.get(ChatbotApiService)
    const { items: products } = await api.app.getInnowayProducts()
    this.products = products
    const url = new URL(this.button.url)
    this.payload = JSON.parse(url.searchParams.get('p'))
    this.mapProducts()
  }
  mapProducts() {
    this.mappingProducts = this.payload.products.map(item => {
      item.product = cloneDeep(this.products.find(p => p.id == item.product_id));
      return item
    })
  }
  onSave(form: NgForm) {
    if(form.valid) {
      const url = new URL(this.button.url)
      let copy = merge({}, this.payload)
      for(let i = 0; i < copy.products.length; i++) {
        delete copy.products[i].product
      }
      url.searchParams.set('p',JSON.stringify(copy))
      this.button.url = url.href
      // console.log('this.button', this.button);
      this.dialogRef.close(this.button);
    }
  }
  async addProduct(event: MatAutocompleteSelectedEvent) {
    const product = event.option.value
    setTimeout(() => {
      this.productInput = ""
    }, 200)
    for(let i = 0; i < this.payload.products.length; i++) {
      if(this.payload.products[i].product_id == product.id && this.payload.products[i].topping_value_ids.length == 0) {
        this.payload.products[i].amount += 1
        this.mapProducts()
        return;
      }
    }
    this.payload.products.push({
      product_id: product.id,
      amount: 1,
      topping_value_ids: []
    })
    this.mapProducts()
  }
  async onProductChange(product, index) {
    this.payload.products[index] = product
    console.log('this.payload', this.payload);
  }
  async onProductRemove(product, index) {
    this.payload.products.splice(index, 1);
    this.mapProducts()
  }
  async inputChanged(model: NgModel) {
    this.autocompleteFilter.next(this.products.filter(p => new RegExp(model.value.toLowerCase()).test(p.name.toLowerCase())).slice(0, 5))
  }

}
