import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { JsonEditorOptions, JsonEditorComponent } from 'ang-jsoneditor';

@Component({
    selector: 'json-dialog',
    styleUrls: ['./json.component.scss'],
    templateUrl: './json.component.html',
})
export class JsonDialog implements OnInit {

    hasError: boolean = false
    content: JSON

    @ViewChild(JsonEditorComponent) editor: JsonEditorComponent;
    jsonEditorConfig:JsonEditorOptions = new JsonEditorOptions()
    constructor(
    public dialogRef: MatDialogRef<JsonDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
        if (!this.data) {
            this.data = {}
        }
        let debounce;
        this.jsonEditorConfig.onChange = () => {
            if(debounce) clearTimeout(debounce);
            debounce = setTimeout(this.onJSONChange.bind(this), 250);
        }
    }

    async ngAfterViewInit() {   
        console.log('focus')     
        setTimeout(() => {
            this.editor.focus()
        }, 300)
    }

    onJSONChange() {
        try {
            this.content = this.editor.get()
            this.hasError = false
        } catch (err) {
            this.hasError = true
            return;
        }
    }

    ngOnInit() {
        this.jsonEditorConfig.mode = 'code';
    }

    onYesClick() {
        this.dialogRef.close(this.content);
    }

    close(): void {
        this.dialogRef.close();
    }

}