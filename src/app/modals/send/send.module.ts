import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SendDialog } from './send.component';
import { MatProgressSpinnerModule, MatTooltipModule, MatSnackBarModule, MatDialogModule, MatDatepickerModule, MatNativeDateModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { NameFilterPipeModule } from 'app/shared/pipes/name-filter-pipe/name-filter-pipe.module';
import { TextFieldModule } from '@angular/cdk/text-field';
import { LazyLoadImagesModule } from 'ngx-lazy-load-images'
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatTooltipModule,
    LazyLoadImagesModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatDialogModule,
    NameFilterPipeModule,
    TextFieldModule,
    MatInputModule,
    MatNativeDateModule, 
    MatDatepickerModule
  ],
  declarations: [SendDialog],
  exports: [SendDialog],
  entryComponents: [SendDialog]
})
export class SendModule { }
