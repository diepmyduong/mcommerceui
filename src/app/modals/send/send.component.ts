import {Component, Inject, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar} from '@angular/material';
import { ChatbotApiService, iSubscriber, iGroup, iLabel, iPage, iSendData, iStory } from 'app/services/chatbot';
import { AlertService } from 'app/services/alert.service';
import { iStoryGroup } from 'app/services/chatbot/api/crud/storyGroup';
import { cloneDeep } from 'lodash-es'
import * as converter from 'json-2-csv'

declare var window: any
@Component({
    selector: 'send-dialog',
    styleUrls: ['./send.component.scss'],
    templateUrl: './send.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SendDialog implements OnInit {

    target: boolean
    content: boolean
    targetCategory: any
    contentCategory: any
    targetCategories = [
        { code: 'group', display: 'Nhóm' },
        { code: 'label', display: 'Nhãn' },
        { code: 'subscriber', display: 'Cá nhân' },
        { code: 'psid', display: 'PSID' },
        { code: 'file', display: 'CSV' }
    ]
    contentCategories = [
        { code: 'normal', display: 'Câu chuyện' },
        { code: 'broadcast', display: 'Broadcast' },
        { code: 'sequence', display: 'Luồng' },
    ]
    selectedSubscribers: iSubscriber[] = []
    searchedSubscribers: iSubscriber[] = []
    subscribers: iSubscriber[] = []
    selectedGroup: iGroup
    groups: iGroup[] = []
    selectedLabel: iLabel
    labels: iLabel[] = []
    selectedStory: iGroup
    stories: iStory[] = []
    storyGroups: iStoryGroup[] = []
    file: File
    csvText: string = ''

    allChecked: boolean = false
    currentPage: number = 1
    searchPage: number = 1
    totalSearchSubscribers: number = 0
    totalSubscribers: number

    searchText: string
    searching: boolean = false

    psidText: string
    psidList: string[]
    csvList: any[]
    columnList: any[]

    maxSubscriber: number
    random: boolean = false

    hasSchedule: boolean = false
    scheduledDate: Date = new Date()
    scheduledTime: string = "06:00"
    minDate = new Date()

    loadingSubscribers: boolean = false
    loadingMore: boolean = false
    loadDone: boolean = false

    sending: boolean = false
    exceptReceived: boolean = false
    success: boolean = false

    constructor(
    public dialogRef: MatDialogRef<SendDialog>,
    @Inject(MAT_DIALOG_DATA) public data: iSendContent,
    public chatbotApi: ChatbotApiService,
    private alert: AlertService,
    private changeRef: ChangeDetectorRef,
    private snackBar: MatSnackBar
    ) {
        if (data.targetId) {
            this.target = true
            this.targetCategory = this.targetCategories.find(x => x.code == data.targetMode)
            switch(this.targetCategory.code) {
                case 'group': {
                    this.selectedGroup = { _id: data.targetId, name: data.targetName, isAll: data.targetId==='0'?true:false }
                    break
                }
                case 'subscriber': {
                    this.selectedSubscribers = data.targetId
                }
            }
            this.contentCategory = this.contentCategories[0]
        } else {
            this.content = true
            this.selectedStory = { _id: data.contentId, name: data.contentName }
            this.contentCategory = this.contentCategories.find(x => x.code == data.contentMode)
            if (data.contentMode == 'broadcast') {
                this.targetCategories[0] = { code: 'all', display: 'Tất cả' }
            }
            this.targetCategory = this.targetCategories[0]
        }
    }

    async ngOnInit() {
        this.reload()
    }

    async reload(local = true) {
        try {
            let tasks = []
            this.loadDone = false
            this.changeRef.detectChanges()
            if (this.content) {
                switch (this.targetCategory.code) {
                    case 'group': {
                        tasks.push(this.chatbotApi.group.getList({ local, query: {
                            limit: 0,
                            fields: ["_id", "name"],
                            order: { createdAt: 1 }
                        }}).then(result => {
                            this.groups = [{
                                _id: '0',
                                name: 'Tất cả khách hàng',
                                isAll: true
                            },...result]
                            this.selectedGroup = this.groups[0]
                            console.log('group', result)
                        }))
                        break;
                    }
                    case 'label': {
                        tasks.push(this.chatbotApi.page.getLabels({
                            local, query: {
                                limit: 0
                            }
                        }).then(result => {
                            console.log('labels', result)
                            this.labels = result
                            this.selectedLabel = this.labels[0]
                        }))
                        break;
                    }
                    case 'subscriber': {
                        this.currentPage = 1
                        this.selectedSubscribers = []
                        tasks.push(this.chatbotApi.subscriber.getList({ local, query: {
                            fields: ["_id", "messengerProfile"],
                            limit: 15,
                            page: this.currentPage
                        } }).then(result => {
                            this.subscribers = result.filter(x => x.messengerProfile)
                            this.totalSubscribers = this.chatbotApi.subscriber.pagination.totalItems
                            console.log('subscribers', this.subscribers)
                        }))
                        break;    
                    }       
                    case 'psid': {
                        this.currentPage = 1
                        this.psidList = []
                        this.psidText = ""
                        this.selectedSubscribers = []
                        break
                    }
                }
                await Promise.all(tasks)
            } else {
                let currentCategory = this.contentCategory.code
                let stories = []
                tasks.push(this.chatbotApi.storyGroup.getList({ local, query: {
                    limit: 0,
                    fields: ["_id", "name", "type", "stories", "sequenceEvents"],
                    filter: { type: this.contentCategory.code }, 
                    order: { 'createdAt': 1 } 
                } })
                .then(result => {
                    this.storyGroups = cloneDeep(result)
                }))
                tasks.push(this.chatbotApi.story.getList({ local, query: {
                    limit: 0,
                    fields: ["_id", "name"],
                    order: { 'createdAt': 1 },
                    filter: { mode: this.contentCategory.code }
                } })
                .then(result => {
                    this.stories = cloneDeep(result)
                }))
                await Promise.all(tasks)
                if (this.contentCategory.code != currentCategory) return
                
                for (let group of this.storyGroups) {
                    for (let i = 0; i < group.stories.length; i++) {
                        if (!group.stories[i]) {
                            group.stories.splice(i, 1)
                            i--
                            return
                        }
                        let index = this.stories.findIndex(x => x._id == group.stories[i])
                        if (index >= 0) {
                            group.stories[i] = this.stories[index]
                            this.stories.splice(index, 1)
                        } else {
                            group.stories.splice(i, 1)
                            i--
                        }
                    }
                }

                let generalStoryGroup: iStoryGroup = {
                    _id: "0",
                    name: 'Mặc định',
                    type: 'normal',
                    stories: [],
                    sequenceEvents: [],
                    isDefault: true,
                    expanded: true,
                }
                if (this.storyGroups[0] && this.storyGroups[0].isDefault) {
                    this.storyGroups[0].stories = [...this.stories]
                } else {
                    generalStoryGroup.stories = [...this.stories]
                    this.storyGroups.splice(0, 0, generalStoryGroup)
                }

                this.selectedStory = null
                for (let group of this.storyGroups) {
                    if (group.stories.length) {
                        this.selectedStory = group.stories[0] as iStory
                        break
                    }
                }
            }
        } catch (err) {
            // console.error(err)
            this.dialogRef.close()
            this.alert.handleError(err);
            // this.alert.error('Lỗi xảy ra', err.message)
        } finally {
            this.loadDone = true
            this.changeRef.detectChanges()

            switch (this.targetCategory.code) {                
                case 'psid': {
                    setTimeout(() => { document.getElementById('psid-text').focus() })
                    break
                }
                case 'file': {
                    this.resetCSV()
                    setTimeout(() => { document.getElementById('csv-input').focus() })
                    break
                }
                default: break
            }
        }
    }

    setTargetCategory(cat) {
        if (this.targetCategory != cat) {
            this.targetCategory = cat
            this.reload()
        }
    }

    setContentCategory(cat) {
        if (this.contentCategory != cat) {
            this.contentCategory = cat
            this.reload()
        }
    }

    addSubscriber(sub) {
        if (!this.selectedSubscribers.includes(sub)) {
            this.selectedSubscribers.push(sub)
            this.changeRef.detectChanges()
        }
    }

    async loadMore() {
        try {
            this.loadingMore = true
            this.changeRef.detectChanges()
            this.currentPage += 1
            let tasks = []
            tasks.push(this.chatbotApi.subscriber.getList({ local: true, query: {
                limit: 15,
                page: this.currentPage
            } }).then(result => {
                this.subscribers = [...this.subscribers, ...result]
            }))
            await Promise.all(tasks)
        } catch (err) {
            // console.error(err)
            // this.snackBar.open('❌ Lỗi xảy ra khi lấy thêm người dùng', '', {
            //     duration: 2500,
            // });
            this.alert.handleError(err, "Lỗi khi lấy thêm dữ liệu");
            // this.alert.error("Lỗi khi lấy thêm người dùng", err.message)
        } finally {
            this.loadingMore = false
            this.changeRef.detectChanges()
        }
    }

    async search() {
        try {
            this.searching = true
            this.searchPage = 1
            this.changeRef.detectChanges()
            let tasks = []
            tasks.push(this.chatbotApi.subscriber.filter({
                local: true, query: {
                    fields: ["_id", "messengerProfile"],
                    limit: 15,
                    page: this.searchPage,
                    order: { "snippetUpdatedAtText": -1 }
                }
            }, {search: this.searchText}).then(result => {
                this.searchedSubscribers = result
                this.totalSearchSubscribers = this.chatbotApi.subscriber.pagination.totalItems
            }))
            await Promise.all(tasks)
        } catch (err) {
            // console.error(err)
            this.alert.handleError(err);
            // this.alert.error('Lỗi xảy ra', err.message)
        } finally {
            this.searching = false
            this.changeRef.detectChanges()
        }
    }

    async searchMore() {
        try {
            this.loadingMore = true
            this.searchPage += 1
            this.changeRef.detectChanges()
            let tasks = []
            tasks.push(this.chatbotApi.subscriber.filter({
                local: true, query: {
                    fields: ["_id", "psid", "messengerProfile"],
                    limit: 15,
                    page: this.searchPage,
                    order: { "snippetUpdatedAtText": -1 }
                }
            }, {search: this.searchText}).then(result => {
                this.searchedSubscribers = [...this.searchedSubscribers, ...result]
            }))
            await Promise.all(tasks)
        } catch (err) {
            this.alert.handleError(err);
        } finally {
            this.loadingMore = false
            this.changeRef.detectChanges()
        }
    }

    removeSubscriber(sub) {
        let index = this.selectedSubscribers.findIndex(x => x._id == sub._id)
        if (index >= 0) {
            this.selectedSubscribers.splice(sub, 1)
            this.changeRef.detectChanges()
        }
    }

    async onPsidTextChanged() {
        this.psidList = []
        this.psidText = this.psidText.trim().replace('\'', '').replace('\"', '')
        let separator = '\n'
        let separatorList = ['\n', ',', '|', '.', '/', '&']
        for (let sep of separatorList) {
            if (this.psidText.includes(sep)) {
                separator = sep
                break
            }
        }
        this.psidList = this.psidText.split(separator)
        console.log(this.psidList)

        this.loadSubscriberByPSID()
    }

    async loadSubscriberByPSID(local = true, all = false) {
        try {
            this.loadingSubscribers = true
            this.changeRef.detectChanges()
            if (all) {
                this.selectedSubscribers = await this.chatbotApi.subscriber.query({ local, query: {
                    fields: ["_id", "messengerProfile", "psid"],
                    limit: 0,
                } }, { filter: { "psid": { "$in": this.psidList } } })
            } else {
                this.selectedSubscribers = await this.chatbotApi.subscriber.query({ local, query: {
                    fields: ["_id", "messengerProfile", "psid"],
                    limit: 10,
                    page: this.currentPage,
                } }, { filter: { "psid": { "$in": this.psidList } } })
            }
            this.totalSubscribers = this.chatbotApi.subscriber.pagination.totalItems
        } catch (err) {
            this.alert.handleError(err, "Lỗi khi lấy dữ liệu người dùng");
        } finally {
            this.loadingSubscribers = false
            this.changeRef.detectChanges()
        }
    }
    async loadMorePSID() {
        try {
            this.loadingMore = true
            this.changeRef.detectChanges()
            this.currentPage += 1
            let tasks = []
            tasks.push(this.chatbotApi.subscriber.query({ local: true, query: {
                fields: ["_id", "messengerProfile", "psid"],
                limit: 10,
                page: this.currentPage,
            } }, { filter: { "psid": { "$in": this.psidList } } }).then(result => {
                this.selectedSubscribers = [...this.selectedSubscribers, ...result]
            }))
            await Promise.all(tasks)
        } catch (err) {
            this.alert.handleError(err, "Lỗi khi lấy thêm dữ liệu");
        } finally {
            this.loadingMore = false
            this.changeRef.detectChanges()
        }
    }

    async onCSVTextChanged(event: ClipboardEvent) { 
        let clipboardData = event.clipboardData || window.clipboardData;
        this.csvText = clipboardData.getData('text')
        let delimiter = ','
        if (this.csvText.includes('\t')) delimiter = '\t'
        converter.csv2jsonAsync(this.csvText, {
            delimiter: { field: delimiter, wrap: '"', eol: '\n'  } 
        }).then(async result => this.processCSVText(result))
    }

    async resetCSV() {
        this.file = null
        this.csvText=''
        this.psidList = []
        this.csvList = []
        this.columnList = []
        this.selectedSubscribers = [];
        (document.getElementById('file-input') as HTMLInputElement).value = ''
    }

    async fileChanged(event) {
        this.file = event.target.files[0]
        let reader = new FileReader()

        reader.onload = () => {
            let csvText = reader.result.toString()
            console.log(csvText)
            converter.csv2jsonAsync(csvText).then(async result => this.processCSVText(result))
        }

        reader.readAsText(this.file)
        this.changeRef.detectChanges()
    }

    async processCSVText(result) {
        for (let i = 0; i < result.length; i++) {
            if (!(result[i] as any).psid && !(result[i] as any).PSID && !(result[i] as any).Psid) {
                result.splice(i, 1)
                i--
            } else {
                for (let key in result[i] as any) {
                    if (!key || key === "" || /[^ -~]+/g.test(key)) {
                        delete (result[i] as any)[key]
                    }
                }
            }
        }

        if (result.length > 2000) {
            this.resetCSV()
            this.alert.info('Quá giới hạn', 'Mỗi file chỉ được chứa tối đa 2000 khách hàng.')
            return
        }

        this.psidList = []
        this.csvList = []
        this.columnList = []

        for (let key in result[0] as any) {
            this.columnList.push({ name: key, check: true })
        }
        
        for (let sub of result as any[]) {
            let { psid, PSID, Psid, ...context } = sub
            console.log(psid, PSID, Psid, psid || PSID || Psid)
            this.psidList.push((psid || PSID || Psid).toString())
            this.csvList.push({ psid: (psid || PSID || Psid).toString(), ...context })
        }

        await this.loadSubscriberByPSID(true, true)

        for (let sub of this.selectedSubscribers) {
            let index = this.csvList.findIndex(x => x.psid == sub.psid)
            if (index != -1) this.csvList[index] = { _id: sub._id, ...this.csvList[index] }
        }
        console.log(this.csvList)
    }

    async send() {
        let scheduledAt = this.scheduledDate
        if (this.hasSchedule) {
            if (this.scheduledTime && this.scheduledTime.split(':').length == 2) {
                let hour, min
                hour = Number(this.scheduledTime.split(':')[0])
                min = Number(this.scheduledTime.split(':')[1])
                if (hour != NaN && min != NaN) {
                    scheduledAt.setHours(hour, min, 0)
                    console.log(scheduledAt)
                } else {
                    this.alert.info('Nhập sai định dạng', 'Vui lòng nhập thời gian lên lịch theo định dang HH:mm (24h)')
                    return
                }
            } else {
                this.alert.info('Nhập sai định dạng', 'Vui lòng nhập thời gian lên lịch theo định dang HH:mm (24h)')
                return
            }
        }

        if (this.targetCategory.code == 'all' && !this.allChecked) return
        if (!await this.alert.question(`Xác nhận ${this.hasSchedule?'lên lịch gửi':'gửi'}`, `Bạn có chắc chắn muốn ${this.hasSchedule?'lên lịch gửi':'gửi'} 
        câu chuyện ${this.selectedStory.name} đến ${this.targetCategory.code=='all'?'tất cả khách hàng':
        this.targetCategory.code == 'group'?('nhóm ' + this.selectedGroup.name):
        this.targetCategory.code=='label'?('nhãn ' + this.selectedLabel.name):
        this.targetCategory.code=='psid'?(this.psidList.length + ' khách hàng'):
        (this.selectedSubscribers.length >= 2?this.selectedSubscribers.length + ' khách hàng':this.selectedSubscribers[0].messengerProfile.name)} không?`)) return

        try {
            this.sending = true
            this.changeRef.detectChanges()
            let sendBy, sendTo: any
            let context
            switch (this.targetCategory.code) {
                case 'all': {
                    sendBy = 'all'
                    sendTo = undefined
                    break;
                }
                case 'group': {
                    if (this.selectedGroup.isAll) {
                        sendBy = 'all'
                        sendTo = undefined
                    } else {
                        sendBy = this.targetCategory.code
                        sendTo = [this.selectedGroup._id]
                    }
                    break
                }
                case 'label': {
                    if (this.contentCategory.code == "broadcast") { 
                        sendBy = 'label_id'
                        sendTo = this.selectedLabel.id
                    } else {
                        sendBy = 'facebook_label_id'
                        sendTo = [this.selectedLabel.default_label_id]
                    }
                    break
                }
                case 'subscriber': {
                    sendBy = this.targetCategory.code
                    sendTo = this.selectedSubscribers.map(sub => sub._id)
                    break
                }
                case 'psid': {
                    sendBy = this.targetCategory.code
                    sendTo = this.psidList
                    break
                }
                case 'file': {
                    sendBy = 'psid'
                    sendTo = this.psidList

                    for (let col of this.columnList) {
                        if (!col.check) { this.csvList.forEach(x => delete x[col.name]) } 
                    }
                    context = this.csvList
                }
            }
            let sendData: iSendData = {
                type: "story",
                story: this.selectedStory._id,
                sendBy: sendBy,
                sendTo: sendTo,
                exceptReceived: this.exceptReceived
            }
            if (this.maxSubscriber) {
                let maxSubscriber = Number(this.maxSubscriber)
                if (!isNaN(maxSubscriber)) {
                    sendData.maxSubscriber = maxSubscriber
                    sendData.random = this.random
                }
            }
            if (this.hasSchedule) sendData.scheduleAt = scheduledAt
            if (context) sendData.context = context
            if(this.contentCategory.code == "broadcast" && this.targetCategory.code != "subscriber") {
                delete sendData.exceptReceived
                let broadcastResult = await this.chatbotApi.send.sendBroadcast(sendData)
                this.chatbotApi.socket.onTaskChange.emit({
                    type: 'broadcast',
                    broadcastResult
                })
            } else {
                await this.chatbotApi.send.sendMessage(sendData)
            }
            this.snackBar.open(this.hasSchedule?'📅 Lên lịch thành công':'✔️ Gửi thành công', '', {
                duration: 2500
            });
            this.success = true
        } catch (err) {
            // console.error(err)
            // this.snackBar.open('❌ Lỗi xảy ra khi gửi', '', {
            //     duration: 2500,
            // });
            this.alert.handleError(err, "Lỗi khi gửi");
            // this.alert.error("Lỗi khi gửi", err.message)
        } finally {
            this.sending = false
            this.changeRef.detectChanges()
        }
    }

    close(): void {
        this.dialogRef.close();
    }

    scheduledTimeChanged() {
        if (this.scheduledTime.length >= 3 && this.scheduledTime.indexOf(':') < 0) {
            this.scheduledTime = this.scheduledTime.slice(0, 2) + ':' + this.scheduledTime.slice(2)
            this.changeRef.detectChanges()
        }
    }

}

export class iSendContent {
    targetId: any
    targetMode: string
    targetName: string
    contentId: string
    contentName: string
    contentMode: string
}