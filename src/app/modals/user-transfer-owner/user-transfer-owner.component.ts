import { Component, Inject, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from "@angular/material";
import { ChatbotApiService, iUser, iApp } from "app/services/chatbot";
import { AlertService } from "app/services/alert.service";

@Component({
  selector: 'app-user-transfer-owner',
  templateUrl: './user-transfer-owner.component.html',
  styleUrls: ['./user-transfer-owner.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserTransferOwnerDialog {

  users: iUser[]
  app: iApp
  owner: iUser
  transferedUser: any
  transfering: boolean = false
  confirmedUID: string
  error: boolean = false

  constructor(
    public dialogRef: MatDialogRef<UserTransferOwnerDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private chatbotApi: ChatbotApiService,
    private changeRef: ChangeDetectorRef,
    private snackBar: MatSnackBar,
    private alert: AlertService
  ) {
    this.users = data.users,
    this.app = data.app
    this.owner = data.owner
  }

  async ngOnInit() {
  }

  async inputChanged() {
    this.error = false
  }

  async transferOwnerPermission() {

    if (!this.confirmedUID || this.confirmedUID != this.transferedUser.uid) { 
      this.error = true
      return
    }

    try {
      this.transfering = true
      this.changeRef.detectChanges()
      if (this.chatbotApi.chatbotAuth.systemUser.rule == 'system_admin') {
        await this.chatbotApi.app.updateOwner(this.app._id, this.transferedUser._id)
      } else {
        await this.chatbotApi.app.transferOwner(this.app._id, this.app.accessToken, this.transferedUser.uid)
      }
      await this.alert.success('Chuyển quyền thành công', 
      `${(this.transferedUser as any).displayName || (this.transferedUser as any).name} đã trở thành Owner mới của app!`)
      this.dialogRef.close(true)
    } catch (err) {
      this.alert.handleError(err, 'Không thể chuyển quyền sở hữu')
    } finally {
      this.transfering = false
      this.changeRef.detectChanges()
    }
  }

  copyUID(){
    let input = document.createElement('input') as HTMLInputElement
    let parent = document.getElementById('uid-content')
    parent.appendChild(input)
    input.value = this.transferedUser.uid
    input.select();
    document.execCommand('copy',false);
    parent.removeChild(input)
    this.snackBar.open('✔️ Đã copy vào clipboard', '', {
      duration: 2000,
    });
    this.changeRef.detectChanges()
  }
}