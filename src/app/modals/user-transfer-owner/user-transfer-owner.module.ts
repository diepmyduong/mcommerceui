import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatProgressSpinnerModule, MatSelectModule, MatSnackBarModule, MatTooltipModule } from '@angular/material';
import { UserTransferOwnerDialog } from './user-transfer-owner.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatSelectModule,
    MatIconModule,
    MatDialogModule,
    MatInputModule,
    MatTooltipModule,
    MatProgressSpinnerModule
  ],
  declarations: [UserTransferOwnerDialog],
  entryComponents: [UserTransferOwnerDialog]
})
export class UserTransferOwnerDialogModule { }
