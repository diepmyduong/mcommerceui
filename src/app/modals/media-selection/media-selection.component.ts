import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MatButtonToggle, MAT_DIALOG_DATA } from '@angular/material';
import { merge } from 'lodash-es'
import { ChatbotApiService, iVideo, iPhoto, iAlbum } from 'app/services/chatbot';
@Component({
  selector: 'media-selection-dialog',
  styleUrls: ['./media-selection.component.scss'],
  templateUrl: './media-selection.component.html',
})
export class MediaSelectionDialog implements OnInit {

  info = {};
  errorNum = 0;
  selection: any;
  selectionItem: any;
  colNum: number[] = [];
  loadDone: boolean = false

  constructor(
    public dialogRef: MatDialogRef<MediaSelectionDialog>,
    @Inject(MAT_DIALOG_DATA) public data: iMediaSelectionData,
    public chatbotApi: ChatbotApiService) {
    this.data = merge({
      confirmButtonText: 'Đồng ý',
      categories: []
    }, data)

  }

  async ngOnInit() {

    if (this.data.isImage) {
      await this.getImage();
    } else {
      if (this.data.isVideo) {
        await this.getVideo();
      }
    }
    this.loadDone = true;
  }

  async getImage() {
    const albums = await this.chatbotApi.page.getFanpageAlbums() as iAlbum[]

    albums.forEach(async album => {
      const photos = await this.chatbotApi.page.getFanpageAlbumPhotos(album.id) as iPhoto[]
      let options = []
      photos.forEach(photo => {
        options.push({
          code: photo.id,
          preview: photo.images.find(image => image.width <= 500).source,
          item: photo
        })
      })
      this.data.categories.push({
        name: album.name,
        options: options
      })
    
      if (this.data.current) {
        let photo = photos.find(x => x.link == this.data.current)
        if (photo) {
          this.selection = photo.id;
          this.selectionItem = photo
        }
      }
    })

  }

  async getVideo() {
    const videos = await this.chatbotApi.page.getFanpageVideos() as iVideo[]

    if (this.data.current) {
      let index = videos.findIndex(x => x.id == this.data.current)
      if (index != -1) {
        this.selection = this.data.current;
        this.selectionItem = videos[index]
      }
    }

    videos.forEach(video => {

      const option = {
        code: video.id,
        item: video,
        preview: video.picture
      }

      let index = this.data.categories.findIndex(x => x.name == video.content_category)
      if (index == -1) {
        this.data.categories.push({
          name: video.content_category,
          options: [option]
        })
      } else {
        this.data.categories[index].options.push(option)
      }
    })
  }

  ngAfterViewInit() {
  }

  selectionToggled(code, item) {
    this.selection = code;
    this.selectionItem = item;
  }

  onYesClick() {
    let errorFlag = false;
    if (errorFlag) {
      return;
    }

    this.dialogRef.close(this.selectionItem);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

export interface iMediaSelectionData {
  title: string
  confirmButtonText?: string
  confirmButtonClass?: string
  current?: any
  isImage?: boolean
  isVideo?: boolean
  categories?: {
    name: string,
    options: {
      code: string,
      preview: string
      item: any,
    }[]
  }[]
}