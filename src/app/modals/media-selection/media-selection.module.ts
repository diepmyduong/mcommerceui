import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MediaSelectionDialog } from 'app/modals/media-selection/media-selection.component';
import { MatGridListModule, MatProgressSpinnerModule, MatIconModule, MatButtonModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatButtonModule
  ],
  declarations: [MediaSelectionDialog],
  entryComponents: [MediaSelectionDialog],
  exports: [MediaSelectionDialog]
})
export class MediaSelectionModule { }
