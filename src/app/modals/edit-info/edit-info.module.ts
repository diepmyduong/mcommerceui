import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditInfoDialog } from 'app/modals/edit-info/edit-info.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { HightlightTextareaModule } from 'app/directives/hightlight-textarea/hightlight-textarea.module';

@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    HightlightTextareaModule
  ],
  declarations: [EditInfoDialog],
  entryComponents: [EditInfoDialog],
  exports: [EditInfoDialog, HightlightTextareaModule]
})
export class EditInfoModule { }
