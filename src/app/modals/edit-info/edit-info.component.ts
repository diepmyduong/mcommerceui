import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, ErrorStateMatcher } from '@angular/material';
import { merge } from 'lodash-es'
// import * as Ajv from 'ajv'
import { FormControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
@Component({
  selector: 'edit-info',
  styleUrls: ['./edit-info.component.scss'],
  templateUrl: './edit-info.component.html',
})
export class EditInfoDialog implements OnInit {

  info = {};
  isError = false;
  confirmButtonClicked = false;
  multiSelect = new FormControl();
  constructor(
    public dialogRef: MatDialogRef<EditInfoDialog>,
    @Inject(MAT_DIALOG_DATA) public data: iEditInfoDialogData) {
    this.data = merge({
      confirmButtonText: 'Đồng ý',
      cancelButtonText: 'Huỷ'
    }, data)
  }

  ngOnInit() {
    this.data.inputs.forEach(e => {
      e.error = '';
      e.errorState = new ErrorStateMatcher();
      e.errorState = {
        isErrorState: (control: FormControl) => {
          return e.error != "";
        }
      };
      this.info[e.property] = e.current ? e.current : '';
      if (e.disableOtherProperty) {
        if ((e.disableOtherProperty.value && this.info[e.property] == e.disableOtherProperty.value) ||
        e.disableOtherProperty.hasValue && this.info[e.property]) {
          this.info[e.disableOtherProperty.property] = null
          this.data.inputs.find(x => x.property == e.disableOtherProperty.property).disabled = true
        } else {
          this.data.inputs.find(x => x.property == e.disableOtherProperty.property).disabled = false
        }
      }
    });
    this.multiSelect.value

  }

  inputChanged(item) {
    if (item.disableOtherProperty) {
      if ((item.disableOtherProperty.value && this.info[item.property] == item.disableOtherProperty.value) ||
      item.disableOtherProperty.hasValue && this.info[item.property]) {
        this.info[item.disableOtherProperty.property] = null
        this.data.inputs.find(x => x.property == item.disableOtherProperty.property).disabled = true
      } else {
        this.data.inputs.find(x => x.property == item.disableOtherProperty.property).disabled = false
      }
    }

    if (item.type == 'text' && (item.autocomplete && item.src)) {
      const value = this.info[item.property]
      if (!item.autocompleteFilter) item.autocompleteFilter = new BehaviorSubject<any[]>([])
      item.autocompleteFilter.next(item.src.filter((i => new RegExp(value).test(i.display))).take(5).value())
    }
    item.error = '';
    this.isError = false;
  }

  onYesClick() {
    this.isError = false;
    this.confirmButtonClicked = true;
    let value = "";

    for (let input of this.data.inputs) {
      value = this.info[input.property] as string;

      if (input.required && !value) {
        input.error = 'Bắt buộc nhập';
        this.isError = true;
        continue;
      }
      // if (input.isURL) {
      //   const ajv = new Ajv()
      //   let valid = ajv.validate({ type: "string", format: 'url' }, value);
      //   if (!valid) {
      //     input.error = 'Đường dẫn URL không hợp lệ';
      //     this.isError = true;
      //     continue;
      //   }
      // }

      if (input.isLocation) {
        if (value.split(',').length != 2) {
          input.error = 'Tọa độ không hợp lệ';
          this.isError = true;
          continue;
        }
      }

      if (input.validation) {
        const errMessage = input.validation(value, this.info)
        if (errMessage) {
          input.error = errMessage;
          this.isError = true;
          continue;
        }
      }
    }

    if (this.isError) {
      return;
    }

    // for (let i = 0; i < this.data.inputs.length; i++) {
    //   let input = this.data.inputs[i];
    //   if (input.required && !this.info[input.property]) {
    //     input.error = "Bắt buộc nhập";
    //     this.errorNum++;
    //     errorFlag = true;
    //     continue;
    //   }
    // }
    // if (errorFlag) {
    //   return;
    // }

    this.dialogRef.close(this.info);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getDisplayByCode(code,index){
    let res = this.data.inputs[index].options.find(x => x.code === code)
    if(res){
      return res.display
    }else{
      return ''
    }
  }
}

export interface iEditInfoDialogData {
  title: string
  confirmButtonText?: string
  cancelButtonText?: string
  confirmButtonClass?: string
  cancelButtonClass?: string
  className?: string
  inputs: iDialogTextInput[]
}

export interface iDialogTextInput {
  title: string
  property: string
  type: string
  value?: string
  required?: boolean
  isURL?: boolean
  className?: string
  validation?: { (value: any, info?: any): string }
  [key: string]: any
  options?: any[]
  autocomplete?: boolean
  src?: any[]
  autocompleteFilter?: any
  disableOtherProperty?: { property?: string, value?: any, hasValue?: boolean }
  disabled?: boolean
}
