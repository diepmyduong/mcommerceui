import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
@Component({
  selector: 'image-popup',
  styleUrls: ['./image-popup.component.scss'],
  templateUrl: './image-popup.component.html',
})
export class ImagePopupDialog implements OnInit {

  imageSrc: string
  videoEmbedHTML: any

  constructor(
    public dialogRef: MatDialogRef<ImagePopupDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public sanitizer: DomSanitizer ) {
    if (this.data.image) {
      this.imageSrc = this.data.image
    }
    if (this.data.video) {
      this.videoEmbedHTML = this.sanitizer.bypassSecurityTrustHtml(this.data.video)
    }
  }

  ngOnInit() {
  }
  
  close() {
    this.dialogRef.close();
  }

}