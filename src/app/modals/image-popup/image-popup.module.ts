import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImagePopupDialog } from 'app/modals/image-popup/image-popup.component';
import { MatFormFieldModule, MatInputModule, MatAutocompleteModule, MatSelectModule, MatDatepickerModule, MatNativeDateModule, MatButtonModule, MatDialogModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule
  ],
  declarations: [ImagePopupDialog],
  entryComponents: [ImagePopupDialog],
  exports: [ImagePopupDialog]
})
export class ImagePopupModule { }
