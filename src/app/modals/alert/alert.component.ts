import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
    selector: 'alert-dialog',
    styleUrls: ['./alert.component.scss'],
    templateUrl: './alert.component.html',
})
export class AlertDialog implements OnInit {

    type: 'success' | 'warning' | 'info' | 'error' | 'question'
    title: string
    subtitle: string
    okText: string
    confirmText: string
    cancelText: string
    confirmFn: any

    constructor(
    public dialogRef: MatDialogRef<AlertDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
        this.type = data.type
        this.title = data.title
        this.subtitle = data.subtitle
        this.okText = data.okText
        this.confirmText = data.confirmText
        this.cancelText = data.cancelText
        this.confirmFn = data.confirmFn
    }

    ngOnInit() {
    }

    async onYesClick() {
        if (this.confirmFn) {
            await this.confirmFn()
        }
        this.dialogRef.close('confirmed');
    }

    close(): void {
        this.dialogRef.close();
    }

}