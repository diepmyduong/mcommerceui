import { Component, OnInit, ViewChild, HostListener, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MatSidenav, MatTabChangeEvent, MatListOption, MatSlideToggleChange } from '@angular/material'
import { ActivatedRoute, Router } from '@angular/router'
import * as Console from 'console-prefix'
import * as moment from 'moment'
import { Subscription } from 'rxjs';
import { AlertService } from 'app/services/alert.service';
import { PortalService } from 'app/services/portal.service';
import { PortalContainerComponent } from 'app/chatgut-app/portals/portal-container/portal-container.component';
import { CardService } from 'app/services/card.service';
import { iApp } from 'app/services/chatbot/api/crud/app';
import { iPage } from 'app/services/chatbot/api/crud/page';
import { iTask } from 'app/services/chatbot/api/crud/task';
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service';
import { DynamicComponentLoaderService } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { StoriesPortalComponent } from './portals/stories-portal/stories-portal.component';
import { iStory, iInteractPost } from 'app/services/chatbot';
import { HashTagPortalComponent } from './portals/hash-tag-portal/hash-tag-portal.component';
import { isObject } from 'lodash-es'
import { environment } from '@environments';
@Component({
  selector: 'app-chatgut-app',
  templateUrl: './chatgut-app.component.html',
  styleUrls: ['./chatgut-app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatgutAppComponent implements OnInit {

  app: iApp
  page: iPage
  template: any
  appStatus: boolean

  tasks: iTask[] = []
  taskProgress: number[] = []
  notifyState = {
    nonReadedNotify: 0,
    hasError: false,
    isRunning: false,
    mode: 'determinate',
    value: 0
  }
  subscriptions: Subscription[] = []

  leftSidebarExpanded = false;
  toolsExpanded = false
  togglingApp = false;

  loadingTasks = false
  taskPaging = 1;
  
  liveChatUrl: string
  mode: any;
  menuList = [
    { 
      code: 'stories', 
      display: 'Câu chuyện', 
      icon: 'fas fa-book', 
      component: 'StoriesPortalComponent', 
      extra: { mode: 'normal' } 
    }, { 
      code: 'broadcast', 
      display: 'Broadcast', 
      icon: 'fas fa-broadcast-tower', 
      component: 'StoriesPortalComponent', 
      extra: { mode: 'broadcast' } 
    }, { 
      code: 'sequence', 
      display: 'Luồng câu chuyện', 
      icon: 'fas fa-stream',
      component: 'StoriesPortalComponent', 
      extra: { mode: 'sequence' } 
    }, { 
      code: 'groups', 
      display: 'Nhóm khách hàng', 
      icon: 'fas fa-users', 
      component: 'CustomersPortalComponent',
      readPermission: true
    }, { 
      code: 'livechat', 
      display: 'Live chat', 
      icon: 'fas fa-comments',
      component: 'SubscribersPortalComponent'
    // }, { 
    //   code: 'topics', 
    //   display: 'Chủ đề', 
    //   icon: 'fas fa-calendar-week',
    //   component: 'TopicsPortalComponent'
    }, { 
      code: 'hashtag', 
      display: 'Hashtag', 
      icon: 'fas fa-hashtag', 
      class: 'menu-hashtag', 
      component: 'HashTagPortalComponent' 
    }, { 
      code: 'overview', 
      display: 'Tổng quan câu chuyện', 
      icon: 'fas fa-book-reader', 
      component: 'MenuPortalComponent', 
      extra: { type: 'OVERVIEW' } 
    }, { 
      code: 'statistics', 
      display: 'Thống kê', 
      icon: 'fas fa-chart-line', 
      component: 'MenuPortalComponent', 
      extra: { type: 'STATISTICS' },
      readPermission: true  
    }, { 
      code: 'marketing', 
      display: 'Bán hàng', 
      icon: 'fas fa-funnel-dollar', 
      component: 'MenuPortalComponent', 
      extra: { type: 'MARKETING' }
    }, { 
      code: 'settings', 
      display: 'Thiết lập', 
      icon: 'fas fa-cog',
      component: 'MenuPortalComponent', 
      extra: { type: 'SETTINGS' } 
    }
  ]

  value = 0
  openNotificationMenu = false

  @ViewChild("portalContainer") portalContainer: PortalContainerComponent
  @ViewChild("leftSidebar") leftSidebar: MatSidenav
  @ViewChild('menuSidebar') menuSidebar: MatSidenav
  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public chatbotApi: ChatbotApiService,
    public alert: AlertService,
    public portalService: PortalService,
    public cardService: CardService,
    private changeRef: ChangeDetectorRef,
    private dynamicLoader: DynamicComponentLoaderService
  ) {
    this.liveChatUrl = `${environment.chatbot.livechatHost}?token=${this.chatbotApi.chatbotAuth.appToken}`
    this.chatbotApi.clear();
  }

  async ngOnInit() {
    this.app = this.chatbotApi.chatbotAuth.app
    this.chatbotApi.setTitle(this.app.name)
    
    if (!this.app.whiteListed) {
      let expiredMoment = moment(this.app.license.expireDate)
      let minDiff = moment.duration(expiredMoment.utc().diff(moment.utc())).asMinutes()
      if (minDiff < 0) {
        await this.alert.info("App hết hạn", "App đã hết hạn sử dụng. Xin mời đăng ký lại app để được sử dụng tiếp.")
        this.router.navigate(['apps'])
      }
    }

    if (isObject(this.app.activePage)) {
      this.page = this.app.activePage as iPage
      this.chatbotApi.page.getFanpageInfo()
    } else {
      this.page = await this.chatbotApi.page.getItem(this.app.activePage as string,
        {
          local: true,
          reload: false,
        })
      this.chatbotApi.page.getFanpageInfo()
    }

    // this.initTask()
    // this.checkState()

    this.appStatus = (this.page.status == 'active')
    //await this.portalContainer.pushPortalAt(1, StoryInfoPortalComponent, { storyId: "5a698c44082929009fd1061c" })
    this.portalService.regisContainer(this.portalContainer)
    // this.openPostsPortal();
    
    // this.openStoriesPortal();
    if (this.chatbotApi.chatbotAuth.user.rule == 'read'){
      this.selectMenu('groups')
    } else {      
      let mode = this.route.snapshot.queryParamMap.get("mode")
      let item = JSON.parse(this.route.snapshot.queryParamMap.get("item"))
      if (mode) {
        switch (mode) {
          case 'story':
            await this.dynamicLoader.getComponentFactory('StoriesPortalComponent')            
            await this.selectMenu('stories')
            let storiesPortal = this.portalContainer.portals[0] as StoriesPortalComponent
            storiesPortal.openStory(item as iStory)
          break;
          case 'hashtag':
            await this.dynamicLoader.getComponentFactory('HashTagPortalComponent')  
            await this.selectMenu('hashtag')
            let hashtagPortal = this.portalContainer.portals[0] as HashTagPortalComponent
            hashtagPortal.openInteractPost(item as iInteractPost)
          break;
          case 'shop':
            await this.dynamicLoader.getComponentFactory('MenuPortalComponent')
            await this.selectMenu('marketing')
            await this.portalService.pushPortal("ShopPortalComponent")
          break;
          case 'wheel':
            await this.dynamicLoader.getComponentFactory('MenuPortalComponent')
            await this.selectMenu('marketing')
            await this.portalService.pushPortal("WheelPortalComponent")
          break;
        }
      } else {
        await this.dynamicLoader.getComponentFactory('StoriesPortalComponent')
        await this.selectMenu('stories')

        // this.portalService.pushPortal("StatisticsResultPortalComponent", { mode: "overview", portalName: "Tổng quan Fanpage" })
        // this.portalService.pushPortal("EvoucherPortalComponent")
      }
    }
    
    await this.dynamicLoader.getComponentFactory('ButtonComponent')
    await this.dynamicLoader.getComponentFactory('CustomersPortalComponent')
    await this.dynamicLoader.getComponentFactory('MenuPortalComponent')

    this.chatbotApi.chatbotAuth.setStatus("online")
  }

  async ngAfterViewInit() {
    // preload global variables for later use
    const { app } = this.chatbotApi.chatbotAuth as iApp
    let environment
    if (app.environment && typeof app.environment == "string") {
      environment = await this.chatbotApi.environment.getItem(app.environment, { local: false });
    } else {
      environment = await this.chatbotApi.environment.getItem(app.environment._id, { local: false });
    }
  }
  
  async openDrawer() {
    this.leftSidebarExpanded = !this.leftSidebarExpanded;
    this.leftSidebar.toggle();
  }

  async ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe())
  }

  checkNotifyState() {
    for (let t of this.tasks) {
      if (t.processState === 'running') {
        this.notifyState.isRunning = true
        break
      } else {
        this.notifyState.isRunning = false
      }
    }
  }

  toggleNotify(notifySidebar) {
    notifySidebar.toggle()
    this.notifyState.nonReadedNotify = 0
  }

  async loadTask(local: boolean = true) {
    this.loadingTasks = true
    const limit = 10;
    return await this.chatbotApi.task.getList({
      query: {
        page: this.taskPaging,
        limit: limit,
        order: "-updatedAt",
        filter: {
          processState: { $ne: "running" }
        }
      }
    }).then(tasks => {
      this.tasks = [...this.tasks, ...tasks]
      for (let i = 0; i < tasks.length; i++) {
        this.taskProgress.push(0)
      }
      this.checkNotifyState()
      this.taskPaging += 1
      this.loadingTasks = false
      return tasks
    }).catch(err => {
      console.error(err)
      this.loadingTasks = false
    })
  }
  
  openChatpluginPortal() {
    if (this.portalService.getLastPortalName() != 'ChatpluginPortalComponent') {
      this.portalService.pushPortal('ChatpluginPortalComponent')
      this.changeRef.detectChanges()
    }
  }

  async publishToggled() {
    if (!this.appStatus) {
      if (!await this.alert.question('Bật app', 'App sẽ hoạt động ở chế độ công khai và tương tác với người dùng thật', 'Bật app'))
        return
      try {
        this.togglingApp = true
        await this.chatbotApi.page.activePage()
        this.appStatus = true
        this.alert.success("App bật thành công", "App đang ở chế độ công khai")
      } catch (err) {
        // console.error(err)
        this.alert.handleError(err, "Lỗi xảy ra khi bật app");
        // this.alert.error("Lỗi xảy ra khi bật app",err.message)
      } finally {
        this.togglingApp = false
        this.changeRef.detectChanges()
      }
    } else {
      if (!await this.alert.question('Tắt app', 'App sẽ hoạt động ở chế độ riêng tư và chỉ tương tác với những người quản lý trang', 'Tắt app'))
        return
      try {
        this.togglingApp = true
        await this.chatbotApi.page.deactivePage()
        this.appStatus = false
        this.alert.success("App tắt thành công", "App đang ở chế độ riêng tư")
      } catch (err) {
        console.error(err)
        this.alert.handleError(err, "Lỗi xảy ra khi tắt app");
        // this.alert.error("Lỗi xảy ra khi tắt app",err.message)
      } finally {
        this.togglingApp = false
        this.changeRef.detectChanges()
      }
    }
  }

  get log() { return Console('[ChatgutApp]').log }

  get activePortalIndex() {
    return this.portalContainer.portals.findIndex(p => p.portalId == this.portalContainer.activePortal)
  }

  async selectMenu(code) {
    let mode = this.menuList.find(x => x.code == code)
    if (await this.portalService.pushPortalAt(0, mode.component, mode.extra?mode.extra:null)) {
      this.mode = code
      this.changeRef.detectChanges()
    }
  }

  cachePortal() {
    if (this.mode)
      sessionStorage.setItem(this.mode, JSON.stringify(this.portalContainer));
  }

  getCache() {
    return JSON.parse(sessionStorage.getItem(this.mode));
  }

  selectPortal(change: MatTabChangeEvent) {
    const { index } = change
    this.portalContainer.portals[index].focus()
  }

  async resetTestData() {
    if (!await this.alert.question('Reset dữ liệu tester', 'Reset dữ liệu tester này về trạng thái ban đầu')) return

    try {
      await this.chatbotApi.app.resetTester()
      this.alert.success('Thành công', 'Reset dữ liệu tester thành công')
    } catch (err) {
      this.alert.handleError(err, "Lỗi xảy ra khi reset dữ liệu");
      // this.alert.error('Lỗi xảy ra', 'Có lỗi khi reset dữ liệu tester, có thể do bạn chưa đăng ký là tester.' + err.message)
    }
  }
}
