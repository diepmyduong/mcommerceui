import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { ChatgutAppComponent } from 'app/chatgut-app/chatgut-app.component'
import { AppAuthGuard } from 'app/services/chatbot'
const routes: Routes = [
  {
    path: ':id',
    data: {
      title: 'ChatGut'
    },
    component: ChatgutAppComponent,
    canActivate: [AppAuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatgutAppRoutingModule { }
