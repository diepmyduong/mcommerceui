// Library
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// Module
import { ChatgutAppRoutingModule } from 'app/chatgut-app/chatgut-app-routing.module';
// Compoment
import { ChatgutAppComponent } from 'app/chatgut-app/chatgut-app.component';
import { PortalContainerComponent } from 'app/chatgut-app/portals/portal-container/portal-container.component';

import { SelectionModule } from 'app/modals/selection/selection.module';
import { SwiperModule } from 'angular2-useful-swiper';
import { MaterialModule } from 'app/shared';
import { FormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { NotificationModule } from 'app/components/notification/notification.module';

@NgModule({
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ChatgutAppComponent }
  ],
  imports: [
    CommonModule,
    ChatgutAppRoutingModule,
    SelectionModule,
    MaterialModule,
    SwiperModule,
    FormsModule,
    DragDropModule,
    NotificationModule
  ],
  declarations: [
    ChatgutAppComponent,
    PortalContainerComponent
  ],
  entryComponents: [
    PortalContainerComponent
  ]
})

export class ChatgutAppModule {
  constructor() {
  }
}