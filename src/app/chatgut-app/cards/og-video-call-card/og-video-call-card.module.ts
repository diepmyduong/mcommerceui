import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OgVideoCallCardComponent } from 'app/chatgut-app/cards/og-video-call-card/og-video-call-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { CardImageModule } from 'app/shared/cards/card-image/card-image.module';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';

@NgModule({
  imports: [
    CommonModule,
    CardImageModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    CardComponentsModule
  ],
  declarations: [OgVideoCallCardComponent],
  entryComponents: [OgVideoCallCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: OgVideoCallCardComponent }
  ]
})
export class OgVideoCallCardModule { }
