import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { NgForm } from '@angular/forms';
import { iCard } from 'app/services/chatbot';
import { VALID_BUTTONS_3 } from 'app/shared/buttons/button-container/validButtons';
import { merge } from 'lodash-es'

@Component({
  selector: 'app-og-video-call-card',
  templateUrl: './og-video-call-card.component.html',
  styleUrls: ['./og-video-call-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OgVideoCallCardComponent extends BaseCard implements OnInit {

	componentName = "OgVideoCallCardComponent";

  constructor(
    public changeRef: ChangeDetectorRef
  ) {
     super()
  }
  validButtons = VALID_BUTTONS_3
  cardName = 'Gọi Video'
  
  resetForm(formCtrl: NgForm, card: iCard) {
    formCtrl.resetForm(merge(super.resetForm(formCtrl, card), {
      image: card.option.image,
      title: card.option.title,
      description: card.option.description
    }))
  }
  cardValid(): string {
    if (this.isUploading) {
      return `Thẻ thứ ${this.index + 1} (${this.cardName}) đang tải ảnh.`
    } else {
      return super.cardValid()
    }
  }

  async onImageSourceChanged(event) {
    this.card.option.image = event
    this.canBeSaved.next(true)
  }

  onButtonsChange(buttons: any[]) {
    this.card.option.buttons = buttons
    this.canBeSaved.next(true)
  }
}
