import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { NgForm } from '@angular/forms';
import { iCard, iSubscriber } from 'app/services/chatbot';
import { environment } from '@environments';
import { merge } from 'lodash-es'
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { SubscriberSelectionComponent } from 'app/components/subscriber-selection/subscriber-selection.component';

@Component({
  selector: 'app-notify-action-card',
  templateUrl: './notify-action-card.component.html',
  styleUrls: ['./notify-action-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotifyActionCardComponent extends BaseCard implements OnInit {

	componentName = "NotifyActionCardComponent";
  @ViewChild(SubscriberSelectionComponent) subscriberSelection: SubscriberSelectionComponent
  constructor(
    public changeRef: ChangeDetectorRef,
    private popoverService: PopoverService
  ) { 
    super()
  }
  cardName = 'Thông báo';
  subscribers: iSubscriber[]
  // allowNotify = undefined;

  async ngOnInit() {
    super.ngOnInit()
    await this.reload(false)
  }

  async reload(local: boolean = true) {
    if (this.card.option.psids && this.card.option.psids.length) {
      this.subscribers = await this.chatbotApi.subscriber.getList({ local, query: {
        fields: ["psid", "_id", "messengerProfile"],
        limit: 0,
        filter: { psid: { $in: this.card.option.psids }}
      }})
    } else {
      this.subscribers = []
    }
    this.detectChanges()
    this.subscriberSelection.changeRef.detectChanges()
  }

  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card),{
      text: card.option.text,
      option: {
        psids: card.option.psids
      }
    }))
  }

  async onChanged(event) {
    this.subscribers = event
    this.card.option.psids = this.subscribers.map(x => x.psid)
    this.canBeSaved.next(true)
    this.detectChanges()
  }

  // regisNotify() {
  //   window.open(`https://m.me/${environment.chatbot.notifyPageId}?ref=user-regis-notify=${this.chatbotApi.chatbotAuth.app._id}=${this.chatbotApi.chatbotAuth.app.accessToken}`, '_blank')
  //   setTimeout(() => {
  //     this.reload(false);
  //   }, 5000)
  // }
}

