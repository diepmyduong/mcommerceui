import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotifyActionCardComponent } from 'app/chatgut-app/cards/notify-action-card/notify-action-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule, MatTooltipModule, MatProgressSpinnerModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { TextFieldModule } from '@angular/cdk/text-field';
import { SubscriberSelectionModule } from 'app/components/subscriber-selection/subscriber-selection.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatInputModule,
    SubscriberSelectionModule,
    FormsModule,
    TextFieldModule,
    CardComponentsModule
  ],
  declarations: [NotifyActionCardComponent],
  entryComponents: [NotifyActionCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: NotifyActionCardComponent }
  ]
})
export class NotifyActionCardModule { }
