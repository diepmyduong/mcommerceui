import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MediaActivityCardComponent } from 'app/chatgut-app/cards/media-activity-card/media-activity-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    TextFieldModule,
    CardComponentsModule
  ],
  declarations: [MediaActivityCardComponent],
  entryComponents: [MediaActivityCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: MediaActivityCardComponent }
  ]
})
export class MediaActivityCardModule { }
