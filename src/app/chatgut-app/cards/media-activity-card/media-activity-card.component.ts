import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { NgForm } from '@angular/forms';
import { iCard } from 'app/services/chatbot/api/crud/card';
import { merge } from 'lodash-es'

@Component({
  selector: 'app-media-activity-card',
  templateUrl: './media-activity-card.component.html',
  styleUrls: ['./media-activity-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MediaActivityCardComponent extends BaseCard implements OnInit {

	componentName = "MediaActivityCardComponent";

  constructor(
     public changeRef: ChangeDetectorRef
  ) { 
     super()
  }
  cardName = 'Gửi File'

  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card),{
      text: card.option.text,
      key: card.option.key,
      fallback: card.option.fallback
    }))
  }

  ngAfterViewInit() {
  }
}
