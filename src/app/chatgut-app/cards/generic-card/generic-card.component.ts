import { Component, OnInit, ViewChild, ViewChildren, QueryList, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { NgForm } from '@angular/forms';
import { iCard } from 'app/services/chatbot';
import { VALID_BUTTONS_3 } from 'app/shared/buttons/button-container/validButtons';
import { ButtonContainerComponent } from 'app/shared/buttons/button-container/button-container.component';
import { ButtonService } from 'app/services/button.service';
import { SwiperComponent } from 'angular2-useful-swiper';
import { merge, differenceBy, cloneDeep } from 'lodash-es'

@Component({
  selector: 'app-generic-card',
  templateUrl: './generic-card.component.html',
  styleUrls: ['./generic-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GenericCardComponent extends BaseCard implements OnInit {

	componentName = "GenericCardComponent";
  @ViewChildren("btnContainer", { read: ButtonContainerComponent }) btnContainersRef: QueryList<ButtonContainerComponent>;
  @ViewChild(SwiperComponent) swiperComp: SwiperComponent
  constructor(
    private buttonSrv: ButtonService,
    public changeRef: ChangeDetectorRef
  ) {
    super()
  }
  cardName = 'Hình và nút'
  swiperOptions:any = {
    pagination: {
      el: '.element-pagination',
      type: 'bullets',
      clickable: true,
      dynamicBullets: true,
    },
    slidesPerView: 1,
    centeredSlides: true,
    navigation: {
      nextEl: '.element-button-next',
      prevEl: '.element-button-prev',
    },
    spaceBetween: 10,
    simulateTouch: false
  }
  validButtons = VALID_BUTTONS_3
  sortMode: boolean = false
  btnContainers: ButtonContainerComponent[] = [];

  ngAfterViewInit() {
    this.regisBtnContainers();
    this.btnContainersRef.changes.subscribe(this.regisBtnContainers.bind(this));
    console.log('generics', this.card.option.attachment.payload.elements)
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  regisBtnContainers() {
    const containers = this.btnContainersRef.toArray();
    differenceBy(this.btnContainers, containers, 'containerId').forEach(c => this.buttonSrv.clearContainer(c));
    this.btnContainers = containers;
  }

  addElement() {
    if(this.card.option.attachment.payload.elements.length >= 10) {
      this.alert.info("Không thể thêm hình", "Chỉ được phép thêm 10 hình")
      return
    }
    const element = {
      title: "Tiêu đề",
      subtitle: "Mô tả ngắn",
      image_url: this.chatbotApi.cardBuilder.defaultImage,
      buttons: [{
        type: "web_url",
        title: "Nút Web URL",
        url: "https://google.com",
        messenger_extensions: false 
      }]
    }
    this.card.option.attachment.payload.elements.push(element)
    this.canBeSaved.next(true)
    setTimeout(() => {
      this.swiper.slideTo(this.card.option.attachment.payload.elements.length - 1)
    }, 250)
    
  }

  copyElement() {
    if(this.card.option.attachment.payload.elements.length >= 10) {
      this.alert.info("Không thể thêm hình", "Chỉ được phép thêm 10 hình")
      return
    }
    const element = cloneDeep(this.card.option.attachment.payload.elements[this.swiperComp.swiper.activeIndex])
    this.card.option.attachment.payload.elements.push(element)
    this.canBeSaved.next(true)
    setTimeout(() => {
      this.swiper.slideTo(this.card.option.attachment.payload.elements.length - 1)
    }, 250)
    
  }

  cardValid(): string {
    if (this.isUploading) {
      return `Thẻ thứ ${this.index + 1} (${this.cardName}) đang tải ảnh.`
    } 
    // for(let e of this.card.option.attachment.payload.elements) {
    //   if(e.buttons.length == 0) return `Thẻ thứ ${this.index + 1} (${this.cardName}) bị lỗi. Yêu cầu ít nhất 1 nút cho từng ảnh.`
    // }
    return super.cardValid();
  }

  get swiper(){
    return this.swiperComp.swiper
  }

  removeElement(){
    if(this.card.option.attachment.payload.elements.length == 1) {
      this.alert.info("Không thể xoá hình", "Yêu cầu ít nhất phải có 1 hình")
      return
    }
    const elementIndex = this.swiper.activeIndex;
    this.card.option.attachment.payload.elements.splice(elementIndex, 1)
    this.canBeSaved.next(true)
  }

  resetForm(formCtrl: NgForm,card: iCard) {
    let resetData = {
      sharable: card.option.attachment.payload.sharable,
    }
    resetData['imageAspectRatio' + this.cardRefId] = card.option.attachment.payload.image_aspect_ratio
    card.option.attachment.payload.elements.forEach((element,index) => {
      resetData[`title-${index}`] = element.title
      resetData[`subtitle-${index}`] = element.subtitle
    })
    formCtrl.resetForm(merge(super.resetForm(formCtrl, card),resetData))
  }

  onButtonsChange(elementIndex:number,buttons: any[]) {
    this.card.option.attachment.payload.elements[elementIndex].buttons = buttons
    this.canBeSaved.next(true)
    this.container.change.emit();
  }

  async onImageSourceChanged(event, index) {
    let element = this.card.option.attachment.payload.elements[index]
    element.image_url = event
    this.card.option.attachment.payload.elements[index] = element
    this.canBeSaved.next(true)
  }

  async toggleAspectRatio() {
    if (this.card.option.attachment.payload.image_aspect_ratio == 'square') {
      this.card.option.attachment.payload.image_aspect_ratio = 'horizontal'
    }
    else {
      this.card.option.attachment.payload.image_aspect_ratio = 'square'
    }
    this.canBeSaved.next(true)
  }

  async toggleSharable() {
    this.card.option.attachment.payload.sharable = !this.card.option.attachment.payload.sharable
    this.canBeSaved.next(true)
  }

  toggleSort() {
    this.sortMode = !this.sortMode;
  }

  moveLeft(i) {
    if (i == 0) return;
    // [this.card.option.attachment.payload.elements[i - 1], this.card.option.attachment.payload.elements[i]] = [this.card.option.attachment.payload.elements[i], this.card.option.attachment.payload.elements[i - 1]]
    let temp = {...this.card.option.attachment.payload.elements[i - 1]}
    this.card.option.attachment.payload.elements[i - 1] = {...this.card.option.attachment.payload.elements[i]}
    this.card.option.attachment.payload.elements[i] = temp;
    this.canBeSaved.next(true)
  }

  moveRight(i) {
    if (i == this.card.option.attachment.payload.elements.length - 1) return;
    // [this.card.option.attachment.payload.elements[i], this.card.option.attachment.payload.elements[i + 1]] = [this.card.option.attachment.payload.elements[i + 1], this.card.option.attachment.payload.elements[i]]
    let temp = {...this.card.option.attachment.payload.elements[i + 1]}
    this.card.option.attachment.payload.elements[i + 1] = {...this.card.option.attachment.payload.elements[i]}
    this.card.option.attachment.payload.elements[i] = temp;
    this.canBeSaved.next(true)
  }

}
