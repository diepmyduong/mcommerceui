import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenericCardComponent } from 'app/chatgut-app/cards/generic-card/generic-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatButtonModule, MatIconModule, MatTooltipModule, MatGridListModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CardImageModule } from 'app/shared/cards/card-image/card-image.module';
import { ButtonContainerModule } from 'app/shared/buttons/button-container/button-container.module';
import { SwiperModule } from 'angular2-useful-swiper';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    MatGridListModule,
    MatFormFieldModule,
    MatInputModule,
    TextFieldModule,
    FormsModule,
    CardImageModule,
    ButtonContainerModule,
    SwiperModule,
    CardComponentsModule
  ],
  declarations: [GenericCardComponent],
  entryComponents: [GenericCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: GenericCardComponent }
  ]
})
export class GenericCardModule { }
