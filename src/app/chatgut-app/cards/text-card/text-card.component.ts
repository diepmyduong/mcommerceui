import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { iCard } from 'app/services/chatbot'
import { NgForm } from '@angular/forms'
import { merge } from 'lodash-es'

@Component({
  selector: 'app-text-card',
  templateUrl: './text-card.component.html',
  styleUrls: ['./text-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextCardComponent extends BaseCard implements OnInit {

	componentName = "TextCardComponent";

  constructor(
    public changeRef: ChangeDetectorRef
  ) { 
     super()
  }
  cardName = 'Tin nhắn'

  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card),{
      text: card.option.text
    }))
  }

  ngAfterViewInit() {
    console.log('card', this.card)
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
  }
}
