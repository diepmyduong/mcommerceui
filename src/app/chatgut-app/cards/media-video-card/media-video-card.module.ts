import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MediaVideoCardComponent } from 'app/chatgut-app/cards/media-video-card/media-video-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatButtonModule, MatIconModule } from '@angular/material';
import { MediaSelectionModule } from 'app/modals/media-selection/media-selection.module';
import { ButtonContainerModule } from 'app/shared/buttons/button-container/button-container.module';
import { ImagePopupModule } from 'app/modals/image-popup/image-popup.module';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MediaSelectionModule,
    ButtonContainerModule,
    ImagePopupModule,
    FormsModule,
    CardComponentsModule
  ],
  declarations: [MediaVideoCardComponent],
  entryComponents: [MediaVideoCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: MediaVideoCardComponent }
  ]
})
export class MediaVideoCardModule { }
