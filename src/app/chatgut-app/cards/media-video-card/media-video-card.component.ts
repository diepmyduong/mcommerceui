import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { NgForm } from '@angular/forms'
import { MatDialog } from '@angular/material'
import { iMediaSelectionData, MediaSelectionDialog } from 'app/modals/media-selection/media-selection.component';
import { ImagePopupDialog } from 'app/modals/image-popup/image-popup.component';
import { iCard } from 'app/services/chatbot/api/crud/card';
import { VALID_BUTTONS_3 } from 'app/shared/buttons/button-container/validButtons';
import { ButtonContainerComponent } from 'app/shared/buttons/button-container/button-container.component';
import { ButtonService } from 'app/services/button.service';
import { merge } from 'lodash-es'
@Component({
  selector: 'app-media-video-card',
  templateUrl: './media-video-card.component.html',
  styleUrls: ['./media-video-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MediaVideoCardComponent extends BaseCard implements OnInit {

	componentName = "MediaVideoCardComponent";
  @ViewChild("btnContainer", { read: ButtonContainerComponent }) btnContainer: ButtonContainerComponent
  @ViewChild("swiper") swiperComp: any
  constructor(
    public dialog: MatDialog,
    private buttonSrv: ButtonService,
    public changeRef: ChangeDetectorRef
  ) {
     super();
  }
  cardName = 'Mẫu video'
  swiperOptions: any = {
    pagination: { el: '.element-pagination', type: 'bullets', clickable: true, dynamicBullets: true, },
    slidesPerView: 1,
    centeredSlides: true,
    navigation: { nextEl: '.element-button-next', prevEl: '.element-button-prev', },
    spaceBetween: 10,
    simulateTouch: false
  }
  validButtons = VALID_BUTTONS_3;

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  setVideo(video) {
    let element = this.card.option.attachment.payload.elements[0]
    if (this.card.option.preview.id != video.id) {
      element.url = `https://facebook.com/${video.from.id}/videos/${video.id}`
      this.card.option.preview = video
      this.card.option.attachment.payload.elements[0] = element
      this.canBeSaved.next(true)
    }
  }

  resetForm(formCtrl: NgForm, card: iCard) {
    formCtrl.resetForm(merge(super.resetForm(formCtrl, card),{}))
  }

  onButtonsChange(elementIndex: number, buttons: any[]) {
    this.card.option.attachment.payload.elements[elementIndex].buttons = buttons
    this.canBeSaved.next(true)
    this.container.change.emit();
  }

  openMediaVideos(current = null) {
    let data: iMediaSelectionData = {
      title: "Chọn video từ trang",
      confirmButtonText: "Chọn video",
      isVideo: true,
      current: current
    }

    let dialogRef = this.dialog.open(MediaSelectionDialog, {
      width: '600px',
      data: data
    })

    dialogRef.afterClosed().toPromise().then(result => {
      if (result) {
        this.setVideo(result)
      }
    })
  }

  cardValid() {
    if (!this.card.option.attachment.payload.elements[0].url) {
      return `Thẻ thứ ${this.index + 1} (${this.cardName}) không hợp lệ. Chưa chọn video.`
    }
    super.cardValid()
  }

  async openVideo() {
      let data = {
          video: this.card.option.preview.embed_html
      };
  
      let dialogRef = this.dialog.open(ImagePopupDialog, {
          maxHeight: '60vh',
          panelClass: 'image-popup',
          data: data
      });
  }

  async openLink() {
    let url = this.card.option.attachment.payload.elements[0].url
    window.open(
      url,
      '_blank'
    );
  }

}