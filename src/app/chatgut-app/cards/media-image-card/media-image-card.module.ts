import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MediaImageCardComponent } from 'app/chatgut-app/cards/media-image-card/media-image-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatButtonModule, MatIconModule } from '@angular/material';
import { ButtonContainerModule } from 'app/shared/buttons/button-container/button-container.module';
import { FormsModule } from '@angular/forms';
import { MediaSelectionModule } from 'app/modals/media-selection/media-selection.module';
import { ImagePopupModule } from 'app/modals/image-popup/image-popup.module';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    ButtonContainerModule,
    FormsModule,
    MediaSelectionModule,
    ImagePopupModule,
    CardComponentsModule
  ],
  declarations: [MediaImageCardComponent],
  entryComponents: [MediaImageCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: MediaImageCardComponent }
  ]
})
export class MediaImageCardModule { }
