import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { NgForm } from '@angular/forms'
import { MatDialog } from '@angular/material'
import { iMediaSelectionData, MediaSelectionDialog } from 'app/modals/media-selection/media-selection.component';
import { ImagePopupDialog } from 'app/modals/image-popup/image-popup.component';
import { iCard } from 'app/services/chatbot/api/crud/card';
import { VALID_BUTTONS_3 } from 'app/shared/buttons/button-container/validButtons';
import { ButtonContainerComponent } from 'app/shared/buttons/button-container/button-container.component';
import { ButtonService } from 'app/services/button.service';
import { merge } from 'lodash-es'
@Component({
  selector: 'app-media-image-card',
  templateUrl: './media-image-card.component.html',
  styleUrls: ['./media-image-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MediaImageCardComponent extends BaseCard implements OnInit {

	componentName = "MediaImageCardComponent";
  @ViewChild("btnContainer", { read: ButtonContainerComponent }) btnContainer: ButtonContainerComponent
  @ViewChild("swiper") swiperComp: any
  constructor(
    public dialog: MatDialog,
    private buttonSrv: ButtonService,
    public changeRef: ChangeDetectorRef
  ) {
     super()
  }
  cardName = 'Mẫu hình'
  validButtons = VALID_BUTTONS_3

  ngOnDestroy() {
    super.ngOnDestroy();
  }


  setImage(photo) {
    let element = this.card.option.attachment.payload.elements[0]
    if (element.url != photo.link) {
      element.url = photo.link
      this.card.option.preview = photo.images.find(i => i.width < 500)
      this.card.option.attachment.payload.elements[0] = element
      this.canBeSaved.next(true)
    }
  }

  resetForm(formCtrl: NgForm, card: iCard) {
    formCtrl.resetForm(merge(super.resetForm(formCtrl, card),{}))
  }

  onButtonsChange(elementIndex: number, buttons: any[]) {
    this.card.option.attachment.payload.elements[elementIndex].buttons = buttons
    this.canBeSaved.next(true)
    this.container.change.emit();
  }

  openMediaImages(current = null) {
    let data: iMediaSelectionData = {
      title: "Chọn ảnh từ trang",
      confirmButtonText: "Chọn ảnh",
      isImage: true,
      current: current
    }

    let dialogRef = this.dialog.open(MediaSelectionDialog, {
      width: '600px',
      data: data
    })

    dialogRef.afterClosed().toPromise().then(result => {
      if (result) {
        this.setImage(result)
      }
    })
  }

  cardValid() {
    if (!this.card.option.attachment.payload.elements[0].url) {
      return `Thẻ thứ ${this.index + 1} (${this.cardName}) không hợp lệ. Chưa chọn hình ảnh.`
    }
    super.cardValid()
  }

  async openImage() {
      let data = {
          image: this.card.option.preview.source
      };
  
     this.dialog.open(ImagePopupDialog, {
          maxHeight: '60vh',
          panelClass: 'image-popup',
          data: data
      });
  }

  async openLink() {
    let url = this.card.option.attachment.payload.elements[0].url
    window.open(
      url,
      '_blank'
    );
  }

}