import { Component, OnInit, ViewChild, ViewChildren, QueryList, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { NgForm } from '@angular/forms'
import { MatSlideToggleChange } from '@angular/material'
import { iIWCategory } from 'app/services/chatbot/api/crud/app';
import { iCard } from 'app/services/chatbot/api/crud/card';
import { VALID_BUTTONS_3 } from 'app/shared/buttons/button-container/validButtons';
import { ButtonContainerComponent } from 'app/shared/buttons/button-container/button-container.component';
import { ButtonService } from 'app/services/button.service';
import { merge, differenceBy } from 'lodash-es'
@Component({
  selector: 'app-list-products-card',
  templateUrl: './list-products-card.component.html',
  styleUrls: ['./list-products-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListProductsCardComponent extends BaseCard implements OnInit {

	componentName = "ListProductsCardComponent";
  @ViewChildren("btnContainer", { read: ButtonContainerComponent }) btnContainersRef: QueryList<ButtonContainerComponent>;
  @ViewChild("swiper") swiperComp: any
  constructor(
     private buttonSrv: ButtonService,
     public changeRef: ChangeDetectorRef
  ) {
     super();
  }
  cardName = 'Danh sách sản phẩm'
  swiperOptions: any = {
    pagination: { el: '.element-pagination', type: 'bullets', clickable: true, dynamicBullets: true, },
    slidesPerView: 1,
    centeredSlides: true,
    navigation: { nextEl: '.element-button-next', prevEl: '.element-button-prev', },
    spaceBetween: 10,
    simulateTouch: false
  }
  validButtons = VALID_BUTTONS_3
  preview: boolean = false
  previewPayload: any
  innowayCategories: iIWCategory[]
  btnContainers: ButtonContainerComponent[] = [];

  ngAfterViewInit() {
    this.regisBtnContainers();
    this.btnContainersRef.changes.subscribe(this.regisBtnContainers.bind(this));
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  regisBtnContainers() {
    const containers = this.btnContainersRef.toArray();
    differenceBy(this.btnContainers, containers, 'containerId').forEach(c => this.buttonSrv.clearContainer(c));
    this.btnContainers = containers;
  }

  async ngOnInit() {
    super.ngOnInit()
    if (!this.card.option.query) this.card.option.query = {}
    if (!this.card.option.query.filter) this.card.option.query.filter = {}
    // this.innowayCategories = await this.chatbotApi.app.getInnowayCategories()
  }


  addElement() {

  }

  removeElement() {

  }

  editImage() {
  }

  getImageUrl() {

  }

  resetForm(formCtrl: NgForm, card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card),{
      limit: card.option.query.limit,
      page: card.option.query.page,
      filter: card.option.query.filter.name,
      // category: card.option.query.filter.category_id
    }))
  }

  async onPreviewToggle(toggleChange: MatSlideToggleChange) {
    this.preview = toggleChange.checked
    if (this.preview) {
      //await this.saveContent()
      this.previewPayload = await this.chatbotApi.card.build(this.card._id)
    } else {
      this.previewPayload = undefined
    }
    this.detectChanges();
  }

  async onPreview() {
    this.preview = !this.preview;
    if (this.preview) {
      this.previewPayload = await this.chatbotApi.card.build(this.card._id)
    } else {
      this.previewPayload = undefined
    }
    this.detectChanges();
  }

  changeTopElemenetOption(toggleChange: MatSlideToggleChange) {
    if(toggleChange.checked) {
      this.card.option.topElementStyle = 'compact'
    } else {
      delete this.card.option.topElementStyle
    }
    this.canBeSaved.next(true)
    this.container.change.emit();
  }

}
