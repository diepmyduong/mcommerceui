import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListProductsCardComponent } from 'app/chatgut-app/cards/list-products-card/list-products-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule, MatSlideToggleModule, MatButtonModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { SwiperModule } from 'angular2-useful-swiper';
import { ButtonContainerModule } from 'app/shared/buttons/button-container/button-container.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    MatButtonModule,
    FormsModule,
    SwiperModule,
    ButtonContainerModule,
    FlexLayoutModule,
    CardComponentsModule
  ],
  declarations: [ListProductsCardComponent],
  entryComponents: [ListProductsCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ListProductsCardComponent }
  ]
})
export class ListProductsCardModule { }
