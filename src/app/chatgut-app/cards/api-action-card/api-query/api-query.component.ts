import { Component, OnInit, Input, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { ApiActionCardComponent } from 'app/chatgut-app/cards/api-action-card/api-action-card.component';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-api-query',
  templateUrl: './api-query.component.html',
  styleUrls: ['./api-query.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApiQueryComponent extends BaseComponent implements OnInit {

  @Input() query: IActionAPIQuery
  @Input() parent: ApiActionCardComponent
  @Input() disabled: boolean
  @ViewChild('queryFrm', { read: NgForm }) queryFrm: NgForm
  constructor() { 
    super("ApiQueryComponent")
  }

  ngOnInit() {
  }

  ngAfterContentInit() {
    this.subscriptions.push(this.queryFrm.valueChanges.subscribe(() => {
      this.query.isValid = this.queryFrm.valid
      if (this.parent.allowCanBeSaved) {
        this.parent.canBeSaved.next(true)
      }
    }))
  }

}

export interface IActionAPIQuery {
  id: string,
  property: string,
  value: string,
  isValid?: boolean
}
