import { Component, OnInit, Input, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { NgForm } from '@angular/forms';
import { ApiActionCardComponent } from 'app/chatgut-app/cards/api-action-card/api-action-card.component';

@Component({
  selector: 'app-api-param',
  templateUrl: './api-param.component.html',
  styleUrls: ['./api-param.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApiParamComponent extends BaseComponent implements OnInit {

  @Input() param: IActionAPIParam
  @Input() parent: ApiActionCardComponent
  @Input() disabled: boolean
  @ViewChild('paramFrm', { read: NgForm }) paramFrm: NgForm
  constructor() { 
    super("ApiParamComponent")
  }

  ngOnInit() {
  }

  ngAfterContentInit() {
    this.subscriptions.push(this.paramFrm.valueChanges.subscribe(() => {
      this.param.isValid = this.paramFrm.valid
      if (this.parent.allowCanBeSaved) {
        this.parent.canBeSaved.next(true)
      }
    }))
  }

}

export interface IActionAPIParam {
  id: string,
  property: string,
  value: string,
  isValid?: boolean
}