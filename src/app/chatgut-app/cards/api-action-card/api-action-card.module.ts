import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiActionCardComponent } from 'app/chatgut-app/cards/api-action-card/api-action-card.component';
import { ApiHeaderComponent } from 'app/chatgut-app/cards/api-action-card/api-header/api-header.component';
import { ApiParamComponent } from 'app/chatgut-app/cards/api-action-card/api-param/api-param.component';
import { ApiQueryComponent } from 'app/chatgut-app/cards/api-action-card/api-query/api-query.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatExpansionModule, MatButtonModule, MatRadioModule, MatIconModule, MatTooltipModule } from '@angular/material';
import { NgJsonEditorModule } from 'ang-jsoneditor';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatExpansionModule,
    MatButtonModule,
    MatRadioModule,
    MatIconModule,
    MatTooltipModule,
    NgJsonEditorModule,
    FormsModule,
    FlexLayoutModule,
    CardComponentsModule
  ],
  declarations: [ApiActionCardComponent, ApiHeaderComponent, ApiParamComponent, ApiQueryComponent],
  entryComponents: [ApiActionCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ApiActionCardComponent }
  ]
})
export class ApiActionCardModule { }
