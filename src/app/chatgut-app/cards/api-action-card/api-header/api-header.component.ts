import { Component, OnInit, Input, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BaseComponent } from 'app/base.component';
import { ApiActionCardComponent } from 'app/chatgut-app/cards/api-action-card/api-action-card.component';
@Component({
  selector: 'app-api-header',
  templateUrl: './api-header.component.html',
  styleUrls: ['./api-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApiHeaderComponent extends BaseComponent implements OnInit {

  @Input() header: IActionAPIHeader
  @Input() parent: ApiActionCardComponent
  @Input() disabled: boolean
  @ViewChild('headerFrm', { read: NgForm }) headerFrm: NgForm
  constructor() { 
    super("ApiHeaderComponent")
  }

  ngOnInit() {
  }

  ngAfterContentInit() {
    this.subscriptions.push(this.headerFrm.valueChanges.subscribe(() => {
      this.header.isValid = this.headerFrm.valid;
      if (this.parent.allowCanBeSaved) {
        this.parent.canBeSaved.next(true)
      }
    }))
  }

}

export interface IActionAPIHeader {
  id: string,
  property: string,
  value: string,
  isValid?: boolean
}
