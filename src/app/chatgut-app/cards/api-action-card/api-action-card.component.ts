import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { iCard } from 'app/services/chatbot';
import { NgForm } from '@angular/forms';
import { IActionAPIHeader } from 'app/chatgut-app/cards/api-action-card/api-header/api-header.component';
import { MatRadioChange } from '@angular/material';
import { JsonEditorComponent, JsonEditorOptions } from 'ang-jsoneditor';
import { IActionAPIQuery } from 'app/chatgut-app/cards/api-action-card/api-query/api-query.component';
import { IActionAPIParam } from 'app/chatgut-app/cards/api-action-card/api-param/api-param.component';
import { BaseCard } from 'app/shared/cards/base-card';
import { merge, forEach, remove } from 'lodash-es'

@Component({
  selector: 'app-api-action-card',
  templateUrl: './api-action-card.component.html',
  styleUrls: ['./api-action-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApiActionCardComponent extends BaseCard implements OnInit {

  componentName = "ApiActionCardComponent";

  @ViewChild(JsonEditorComponent) editor: JsonEditorComponent;
  constructor(
    public changeRef: ChangeDetectorRef
  ) {
    super()
    this.configJsonEditor();
  }
  cardName = 'Gọi API'
  headers: IActionAPIHeader[] = []
  queries: IActionAPIQuery[] = []
  params: IActionAPIParam[] = []
  contentType: string
  jsonEditorConfig: JsonEditorOptions = new JsonEditorOptions()
  data = {}

  configJsonEditor() {
    this.jsonEditorConfig.mode = 'code'
    let deboud;
    this.jsonEditorConfig.onChange = () => {
      if (deboud) clearTimeout(deboud);
      deboud = setTimeout(this.onJSONChange.bind(this), 250);
    }
  }

  getApiContentType(card: iCard) {
    if (card.option.request.headers) {
      return card.option.request.headers['Content-Type']
    }
  }

  resetForm(formCtrl: NgForm, card: iCard) {
    const resetData = merge(super.resetForm(formCtrl, card), {
      uri: card.option.request.uri,
      method: card.option.request.method,
      isCallback: card.option.isCallback,
      key: card.option.key,
      sourceKey: card.option.sourceKey,
      ['contentType-' + this.cardRefId]: this.getApiContentType(card)
    })
    formCtrl.reset(resetData)
  }

  ngOnInit() {
    super.ngOnInit()
    if (this.card.type == 'action_innoway_api') {
      this.cardName = "Innoway API"
    }
    if (this.card.option.request.headers) {
      forEach(this.card.option.request.headers, (value, property) => {
        this.headers.push({ id: this.guid.raw(), property, value })
        if (property === 'Content-Type') {
          this.contentType = value
        }
      })
    }
    if (this.card.option.request.qs) {
      forEach(this.card.option.request.qs, (value, property) => {
        this.queries.push({ id: this.guid.raw(), property, value })
      })
    }
    if (this.card.option.request.form) {
      forEach(this.card.option.request.form, (value, property) => {
        this.params.push({ id: this.guid.raw(), property, value })
      })
    }
    if (this.getApiContentType(this.card) === 'application/json' && !this.card.option.request.body) {
      this.card.option.request.body = {}
    }
  }

  ngAfterViewInit() {
    if (this.card) {
      this.data = this.card.option.request.body
    } else {
      this.data = {}
    }
    this.detectChanges();
  }

  async addHeaders() {
    this.headers.push({
      id: this.guid.raw(),
      property: "",
      value: ""
    })
    this.canBeSaved.next(true)
  }

  cardValid(): string {
    const headers = {}
    const queries = {}
    const body = {}
    const form = {}

    for (let h of this.headers) {
      if (!h.isValid) return `Thẻ thứ ${this.index + 1} (${this.cardName}) bị lỗi. Mục Headers không hợp lệ.`
      headers[h.property] = h.value
    }
    this.card.option.request.headers = headers

    for (let q of this.queries) {
      if (!q.isValid) return `Thẻ thứ ${this.index + 1} (${this.cardName}) bị lỗi. Mục Queries không hợp lệ.`
      queries[q.property] = q.value
    }
    this.card.option.request.qs = queries
    if (this.contentType) {
      switch (this.contentType) {
        case 'application/x-www-form-urlencoded':
          for (let p of this.params) {
            if (!p.isValid) return `Thẻ thứ ${this.index + 1} (${this.cardName}) bị lỗi. Mục Params không hợp lệ.`
            form[p.property] = p.value
          }
          delete this.card.option.request.body
          delete this.card.option.request.json
          this.card.option.request.form = form
          break
        case 'application/json':
          try {
            this.editor.get()
          } catch (err) {
            return `Thẻ thứ ${this.index + 1} (${this.cardName}) bị lỗi. JSON không hợp lệ`
          }
          this.card.option.request.json = true
          delete this.card.option.request.form
          break
      }
    }
    this.card.option.request.json = true
    return super.cardValid()
  }


  onJSONChange() {
    try {
      this.card.option.request.body = this.editor.get()
    } catch (err) {
      return;
    }
    this.canBeSaved.next(true)
  }

  removeHeader(id: string) {
    remove(this.headers, h => h.id === id)
    this.canBeSaved.next(true)
  }

  async addQuery() {
    this.queries.push({
      id: this.guid.raw(),
      property: "",
      value: ""
    })
    this.canBeSaved.next(true)
  }

  removeQuery(id: string) {
    remove(this.queries, q => q.id === id)
    this.canBeSaved.next(true)
  }

  changeContentType(event: MatRadioChange) {
    const headerIndex = this.headers.findIndex(h => h.property === "Content-Type")
    const header = {
      id: this.guid.raw(),
      property: 'Content-Type',
      value: event.value
    }
    if (headerIndex === -1) {
      this.headers.push(header)
    } else {
      this.headers[headerIndex] = header
    }
    if (header.value === 'application/json' && !this.card.option.request.body) this.card.option.request.body = {}
    this.canBeSaved.next(true)
  }

  async addParam() {
    this.params.push({
      id: this.guid.raw(),
      property: "",
      value: ""
    })
    this.canBeSaved.next(true)
  }

  removeParam(id: string) {
    remove(this.params, p => p.id === id)
    this.canBeSaved.next(true)
  }

}
