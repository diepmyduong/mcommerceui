import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { NgForm } from '@angular/forms'
import { iCard } from 'app/services/chatbot'
import { VALID_BUTTONS_3 } from 'app/shared/buttons/button-container/validButtons';
import { ButtonService } from 'app/services/button.service';
import { ButtonContainerComponent } from 'app/shared/buttons/button-container/button-container.component';
import { merge} from 'lodash-es'
@Component({
  selector: 'app-buttons-card',
  templateUrl: './buttons-card.component.html',
  styleUrls: ['./buttons-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonsCardComponent extends BaseCard implements OnInit {

	componentName = "ButtonsCardComponent";
  @ViewChild("btnContainer", { read: ButtonContainerComponent }) btnContainer: ButtonContainerComponent
  constructor(private buttonSrv: ButtonService, public changeRef: ChangeDetectorRef) { 
    super()
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  validButtons = VALID_BUTTONS_3
  cardName = 'Tin nhắn'

  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.resetForm(merge(super.resetForm(formCtrl, card),{
      text: card.option.attachment.payload.text
    }));
  }

  onButtonsChange(buttons: any[]) {
    this.card.option.attachment.payload.buttons = buttons
    this.canBeSaved.next(true)
    this.container.change.emit();
  }

  cardValid(): string {
    // if(this.card.option.attachment.payload.buttons.length == 0) {
    //   return `Thẻ thứ ${this.index + 1} (${this.cardName}) bị lỗi. Yêu cầu ít nhất 1 nút.`
    // } else {
    //   return super.cardValid();
    // }
    return super.cardValid();
  }

}
