import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonsCardComponent } from 'app/chatgut-app/cards/buttons-card/buttons-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ButtonContainerModule } from 'app/shared/buttons/button-container/button-container.module';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ButtonContainerModule,
    CardComponentsModule,
    TextFieldModule
  ],
  declarations: [ButtonsCardComponent],
  entryComponents: [ButtonsCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ButtonsCardComponent }
  ]
})
export class ButtonsCardModule { }
