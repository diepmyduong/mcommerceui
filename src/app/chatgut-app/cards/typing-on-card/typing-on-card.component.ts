import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { NgForm } from '@angular/forms';
import { iCard } from 'app/services/chatbot';
import { merge } from 'lodash-es'

@Component({
  selector: 'app-typing-on-card',
  templateUrl: './typing-on-card.component.html',
  styleUrls: ['./typing-on-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TypingOnCardComponent extends BaseCard implements OnInit {

	componentName = "TypingOnCardComponent";

  constructor(
    public changeRef: ChangeDetectorRef
  ) { 
    super()
  }
  cardName = 'Thời gian delay'

  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card),{
      duration: card.option.duration
    }))
  }

  ngAfterViewInit() {
    
  }

  async runIntro() {
  
  }

}