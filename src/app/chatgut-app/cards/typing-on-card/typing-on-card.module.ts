import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TypingOnCardComponent } from 'app/chatgut-app/cards/typing-on-card/typing-on-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { FormsModule } from '@angular/forms';
import { MatCardModule, MatFormFieldModule, MatListModule, MatInputModule, MatSliderModule } from '@angular/material';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatSliderModule,
    FormsModule,
    CardComponentsModule

  ],
  declarations: [ TypingOnCardComponent],
  entryComponents: [TypingOnCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: TypingOnCardComponent }
  ]
})
export class TypingOnCardModule { }
