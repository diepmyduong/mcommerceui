import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { NgForm } from '@angular/forms';
import { iStory } from 'app/services/chatbot/api/crud/story';
import { iCard } from 'app/services/chatbot/api/crud/card';
import { merge } from 'lodash-es'

@Component({
  selector: 'app-dynamic-quick-reply-activity-card',
  templateUrl: './dynamic-quick-reply-activity-card.component.html',
  styleUrls: ['./dynamic-quick-reply-activity-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicQuickReplyActivityCardComponent extends BaseCard implements OnInit {

	componentName = "DynamicQuickReplyActivityCardComponent";

  constructor(public changeRef: ChangeDetectorRef) { 
    super()
  }
  cardName = 'Gợi ý động'
  quickReplyTypes = [{
    display: "Gợi ý",
    value: "activity"
  }, {
    display: "Chuyển câu chuyện",
    value: "story"
  }]
  selectedStory: iStory
  stories: iStory[] = []
  disallowAdvancedQuickReplies = true

  async ngOnInit() {
    await this.reload()
    super.ngOnInit()
  }

  async reload(local: boolean = true) {
    this.stories = await this.chatbotApi.story.getList({
      local,
      query: {
        limit: 0,
        fields: ["_id", "name"],
        order: { 'createdAt': 1 },
        filter: { mode: 'normal' }
      }
    })
    this.card.option.type = this.card.option.type || "activity"
    this.selectedStory = this.stories.find(t => t._id === this.card.option.storyId)
  }

  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card),{
      text: card.option.text,
      key: card.option.key,
      allowAdditional: card.option.allowAdditional,
      image: card.option.mapping.image_url,
      title: card.option.mapping.title,
      value: card.option.mapping.value,
      offset: card.option.offset,
      limit: card.option.limit,
      data: card.option.data,
      type: card.option.type || "activity",
      storyId: card.option.storyId,
      fallback: card.option.fallback
    }))
  }

  ngAfterViewInit() {
  }

  async selectStory() {
    const storyId = await this.getStory(this.card.option.storyId)
    if (storyId != this.card.option.storyId) {
      this.selectedStory = this.stories.find(s => s._id === storyId)
      this.card.option.storyId = storyId
      this.canBeSaved.next(true)
    }
  }

  getStory(storyId?: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      const componentRef = await this.portalService.pushPortalAt(this.portalIndex + 1, 'StoriesPortalComponent', {
        select: true, multi: false, selectedStories: storyId
      })
      if (!componentRef) return;
      
      const component = componentRef.instance as any
      this.subscriptions.onStoriesSaved = component.onStorySaved.subscribe((storyId: string) => {
        resolve(storyId)
        component.hideSave();
        component.close();
      })
      this.subscriptions.onStoriesClosed = component.onStoryClosed.subscribe(storyId => {
        component.onStorySaved.unsubscribe();
        component.onStoryClosed.unsubscribe();
      })
    })
  }
}
