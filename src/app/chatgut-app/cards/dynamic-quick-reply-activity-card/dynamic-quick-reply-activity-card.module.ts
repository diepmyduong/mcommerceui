import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicQuickReplyActivityCardComponent } from 'app/chatgut-app/cards/dynamic-quick-reply-activity-card/dynamic-quick-reply-activity-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatCheckboxModule, MatInputModule, MatSelectModule, MatIconModule, MatButtonModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
    MatButtonModule,
    TextFieldModule,
    FlexLayoutModule,
    FormsModule,
    CardComponentsModule
  ],
  declarations: [DynamicQuickReplyActivityCardComponent],
  entryComponents: [DynamicQuickReplyActivityCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: DynamicQuickReplyActivityCardComponent }
  ]
})
export class DynamicQuickReplyActivityCardModule { }
