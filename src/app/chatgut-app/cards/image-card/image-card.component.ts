import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { NgForm } from '@angular/forms'
import { iCard } from 'app/services/chatbot'
import { merge } from 'lodash-es'
declare var Buffer: any;
@Component({
  selector: 'app-image-card',
  templateUrl: './image-card.component.html',
  styleUrls: ['./image-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageCardComponent extends BaseCard implements OnInit {

  constructor(public changeRef: ChangeDetectorRef) {
    super()
  }
  componentName = "ImageCardComponent";
  cardName = 'Hình ảnh';
  isQRImage = false;
  qrParams: any = {};
  symbologies = [ "QR_CODE", "EAN_13", "ITF", "CODE_39", "CODE_128"];

  async beforeSave() {
    if(this.isQRImage) {
      const url = await this.generateQRUrl();
      if(this.card.option.attachment.payload.url != url) {
        this.card.option.attachment.payload.url = url
      } 
    }
  }

  ngAfterContentInit() {
    super.ngAfterContentInit()
    const url = this.card.option.attachment.payload.url;
    if(/\/deeplink\/image\?src/g.test(url)) {
      const image = decodeURIComponent(new URL(url).searchParams.get('src'));
      if (/azurewebsites.net/g.test(image)) {
        this.qrParams = this.parseQRImageUrl(image);
        this.isQRImage = true;
        this.cardName = "Hình QR/Barcode"
      }
    } else if (/\/deeplink\/image\?base/g.test(url)) {
      const base64 = new URL(url).searchParams.get('base');
      const image = new Buffer(base64, 'base64').toString('utf8');
      if (/azurewebsites.net/g.test(image)) {
        this.qrParams = this.parseQRImageUrl(image);
        this.isQRImage = true;
        this.cardName = "Hình QR/Barcode"
      }
    }
  }

  resetForm(formCtrl: NgForm, card: iCard) {
    formCtrl.resetForm(merge(super.resetForm(formCtrl, card), {
      url: card.option.attachment.payload.url,
      size: this.isQRImage?this.qrParams.size:0,
      content: this.isQRImage?this.qrParams.content:'',
      symbology: this.isQRImage?this.qrParams.symbology:''
    }))
  }
  cardValid(): string {
    if (this.isUploading) {
      return `Thẻ thứ ${this.index + 1} (${this.cardName}) đang tải ảnh.`
    } else {
      return super.cardValid()
    }
  }

  async onImageSourceChanged(event) {
    this.card.option.attachment.payload.url = event
    this.canBeSaved.next(true)
  }

  parseQRImageUrl(url: string) {
    const params: any = {};
    params.content = /content=(.*)&size/g.exec(url)[1];
    params.size = parseInt(/size=(.*)&symbology/g.exec(url)[1]);
    params.symbology = /symbology=(.*)&format/g.exec(url)[1];
    params.preview = this.card.option.attachment.payload.url
    return params;
  }

  async generateQRUrl() {
    return await this.chatbotApi.deeplink.getImageBufferLink({ src: `https://mobiledemand-barcode.azurewebsites.net/barcode/image?content=${this.qrParams.content}&size=${this.qrParams.size}&symbology=${this.qrParams.symbology}&format=png&text=true` })
  }

  async preview() {
    this.qrParams.preview = await this.generateQRUrl();
  }

}
