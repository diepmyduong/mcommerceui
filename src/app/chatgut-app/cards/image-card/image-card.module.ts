import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageCardComponent } from 'app/chatgut-app/cards/image-card/image-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatButtonModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { CardImageModule } from 'app/shared/cards/card-image/card-image.module';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    FlexLayoutModule,
    FormsModule,
    CardImageModule,
    CardComponentsModule
  ],
  declarations: [ImageCardComponent],
  entryComponents: [ImageCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ImageCardComponent }
  ]
})
export class ImageCardModule { }
