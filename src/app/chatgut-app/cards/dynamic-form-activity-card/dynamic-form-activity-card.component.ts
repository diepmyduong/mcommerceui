import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { JsonEditorComponent, JsonEditorOptions } from 'ang-jsoneditor';
import { NgForm } from '@angular/forms';
import { iCard } from 'app/services/chatbot';
import * as guid from 'guid'
import { iEditInfoDialogData, EditInfoDialog } from 'app/modals/edit-info/edit-info.component';
import { MatDialog } from '@angular/material';
import { CdkDropList, CdkDropListContainer, CdkDropListGroup, CdkDrag } from '@angular/cdk/drag-drop';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { indexOf, remove } from 'lodash-es'
@Component({
  selector: 'app-dynamic-form-activity-card',
  templateUrl: './dynamic-form-activity-card.component.html',
  styleUrls: ['./dynamic-form-activity-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicFormActivityCardComponent extends BaseCard implements OnInit {

  componentName = "DynamicFormActivityCardComponent";

  @ViewChild(CdkDropListGroup) listGroup: CdkDropListGroup<CdkDropList>;
  @ViewChild(CdkDropList) placeholder: CdkDropList;
  @ViewChild(JsonEditorComponent) editor: JsonEditorComponent;
  constructor(public changeRef: ChangeDetectorRef,  public dialog: MatDialog, private popoverService: PopoverService) {
    super()
    this.configJsonEditor();
  }
  cardName = 'Biểu mẫu'
  jsonEditorConfig: JsonEditorOptions = new JsonEditorOptions()
  data = {}
  simpleData = []
  simpleDataTypeArray = [
    { code: '', display: 'Chuỗi' },
    { code: '$date', display: 'Ngày tháng năm' },
    { code: '$datetime-local', display: 'Ngày giờ' },
    { code: '$select', display: 'Lựa chọn' },
    { code: '$province', display: 'Tỉnh/Thành' },
    { code: '$district', display: 'Quận/Huyện' },
    { code: '$ward', display: 'Phường/Xã' },
    { code: '$number', display: 'Kiểu số' },
    { code: '$email', display: 'Email' },
    { code: '$tel', display: 'SĐT' },
    { code: '$url', display: 'URL' },
    { code: '$password', display: 'Mật khẩu' },
    { code: '$time', display: 'Giờ' },
    { code: '$week', display: 'Tuần' },
    { code: '$month', display: 'Tháng' },
    { code: '$provinceId', display: 'Mã tỉnh/thành' },
    { code: '$districtId', display: 'Mã quận/huyện' },
  ]
  
  public target: CdkDropList = null;
  public targetIndex: number;
  public source: CdkDropListContainer = null;
  public sourceIndex: number;

  
  resetForm(formCtrl: NgForm, card: iCard) {
    // formCtrl.reset(merge(super.resetForm(formCtrl, card), {
    //   schema: card.option.schema,
    //   key: card.option.key,
    //   message: card.option.message,
    //   button: card.option.button,
    //   title: card.option.title,
    //   allowSkip: card.option.allowSkip,
    //   submitButton: card.option.submitButton,
    // }))
  }

  ngAfterViewInit() {
    console.log( 'card',this.card)
    if (this.card) {
      this.data = this.card.option.schema
      if(this.card.option.schema.schema){
        this.simpleData = Object.keys(this.card.option.schema.schema)
        .map(key =>  {
          let schema = this.card.option.schema.schema[key]
          let required = false
          if (schema[0] == '!') {
            required = true
            schema = schema.slice(1)
          }
          if(schema[0] != "$"){
            return { 
              id: guid.raw(),
              property: key, 
              type: "",
              display: schema,
              required
            }
          }else{
            let tmp = schema.split(":")
            if(tmp[0] == '$select'){
              let options = tmp[1].split(',')
              if(options.length == 1 && options[0] == ''){
                options = []
              }
              return { 
                id: guid.raw(),
                property: key, 
                type: tmp[0],
                display: tmp[2],
                openSelect:true,
                required,
                options
              }
            }else{
              return { 
                id: guid.raw(),
                property: key, 
                type: tmp[0],
                display: tmp[1],
                required,
              }
            }
          }})
        console.log( 'data', this.simpleData)
      }
    } else {
      this.data = {}
      this.simpleData = []
    }

    this.detectChanges();
  }

  configJsonEditor() {
    this.jsonEditorConfig.modes = ['code'];
    this.jsonEditorConfig.mode = 'code';
    let deboud;
    this.jsonEditorConfig.onChange = () => {
      if (deboud) clearTimeout(deboud);
      deboud = setTimeout(this.onJSONChange.bind(this), 250);
    }
  }

  onJSONChange() {
    try {
      this.card.option.schema = this.editor.get();
    } catch (err) {
      return;
    }
    this.canBeSaved.next(true)
  }

  cardValid(): string {
    try {
      if(!this.card.option.schema){
        this.editor.get()
      }else{
        let tmp = {}
        this.simpleData.forEach(data => {
          if(!data.property || !data.display){
            return `Thẻ thứ ${this.index + 1} (${this.cardName}) bị lỗi. JSON không hợp lệ`
          }
          if(data.type == '$select'){
            tmp[data.property] = data.type + ':'
            data.options.forEach((option,index) =>{
              if(index != data.options.length -1){
                tmp[data.property] += option + ','
              }else{
                tmp[data.property] += option
              }
            })
            tmp[data.property] += ':' + data.display
          }else{
            if(data.type){
              tmp[data.property] = data.type + ':' + data.display
            } else{
              tmp[data.property] = data.display
            }
          }
          if (data.required) {
            tmp[data.property] = '!' + tmp[data.property]
          }
        })
        this.card.option.schema.schema = tmp
        console.log( 'res',tmp)
      }
      return super.cardValid()
    } catch (err) {
      return `Thẻ thứ ${this.index + 1} (${this.cardName}) bị lỗi. JSON không hợp lệ`
    }
  }

  example() {
    window.open('http://ulion.github.io/jsonform/playground/', '_blank');
  }

  async addField() {
    this.simpleData.push({
      id: guid.raw(),
      property: "",
      type:"",
      display: "",
      options:[]
    })
    
    this.canBeSaved.next(true)
    this.container.change.emit()
  }

  
  removeField(id: string) {
    remove(this.simpleData, f => f.id === id)
    this.canBeSaved.next(true)
    this.container.change.emit()
  }

  inputChanged(){
    this.canBeSaved.next(true)
    this.container.change.emit()
  }

  toggleRequired(index) {
    this.simpleData[index].required = !this.simpleData[index].required
    this.canBeSaved.next(true)
    this.container.change.emit()
  }

  selectChanged(index){
    if(this.simpleData[index].type == '$select'){
      this.simpleData[index].openSelect = true 
    }
    this.canBeSaved.next(true)
    this.container.change.emit()
  }
  removeFieldSelect(optionIndex,index){
    // remove(this.simpleData[index].options, f => f.id === id)
    this.simpleData[index].options.splice(optionIndex,1)
    console.log( 'res',this.simpleData[index].options)
    this.canBeSaved.next(true)
    this.container.change.emit()
  }
  addFieldSelect(index){
    this.simpleData[index].options.push({
      id: guid.raw(),
      value: "",
    })
    
    this.canBeSaved.next(true)
    this.container.change.emit()
  }

  openEditOptionSelect(index){
    this.simpleData[index].openSelect = true 
    setTimeout(()=>{
      let phElement = this.placeholder.element.nativeElement;
      phElement.style.display = 'none';
      if(phElement.parentNode) phElement.parentNode.removeChild(phElement);
    })
  }

  drop(index: number) {
    if (!this.target)
      return;

    let phElement = this.placeholder.element.nativeElement;
    let parent = phElement.parentNode;

    phElement.style.display = 'none';

    parent.removeChild(phElement);
    parent.appendChild(phElement);
    parent.insertBefore(this.source.element.nativeElement, parent.children[this.sourceIndex]);

    this.target = null;
    this.source = null;

    if (this.sourceIndex != this.targetIndex) {
      let element = this.simpleData[index].options[this.sourceIndex];
      this.simpleData[index].options.splice(this.sourceIndex, 1);
      this.simpleData[index].options.splice(this.targetIndex, 0, element);
      this.canBeSaved.next(true)
      this.container.change.emit()
    }
  }
  enter = (drag: CdkDrag, drop: CdkDropList) => {
    if (drop == this.placeholder)
      return true;
    let phElement = this.placeholder.element.nativeElement;
    let dropElement = drop.element.nativeElement;

    let dragIndex = indexOf(dropElement.parentNode.children, drag.dropContainer.element.nativeElement);
    let dropIndex = indexOf(dropElement.parentNode.children, dropElement);
    console.log(dragIndex, dropIndex)

    if (!this.source) {
      this.sourceIndex = dragIndex;
      this.source = drag.dropContainer;

      let sourceElement = this.source.element.nativeElement;
      phElement.style.width = sourceElement.clientWidth + 'px';
      phElement.style.height = sourceElement.clientHeight + 'px';

      sourceElement.parentNode.removeChild(sourceElement);
    }

    this.targetIndex = dropIndex;
    this.target = drop;

    phElement.style.display = '';
    dropElement.parentNode.insertBefore(phElement, (dragIndex < dropIndex)
      ? dropElement.nextSibling : dropElement);

    this.source.start();
    this.placeholder.enter(drag, drag.element.nativeElement.offsetLeft, drag.element.nativeElement.offsetTop);

    return false;
  }
  async openEditorDialog(option:any, index: number,indexSelect: number) {
    let data: iEditInfoDialogData = {
      title: "",
      confirmButtonText: "Xác nhận",
      inputs: [
        {
          title: "Thuộc tính",
          property: "value",
          current: option || '',
          type: "text",
          required: true,
        }
      ]
    };
    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    });
    const result = await dialogRef.afterClosed().toPromise();
    if(!result || !result.value) return
    if (index > -1 && result.value) {
      this.simpleData[index].options[indexSelect] = result.value
      console.log('res', result, this.simpleData)
      this.canBeSaved.next(true)
      this.container.change.emit();
    }
  }
 
  openPopover(event, index: number , indexOption?: number, option?: any) {
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      target: '.mat-chip',
      targetElement: event.target,
      width: '250px',
      confirm: option?'Đổi tên lựa chọn':'Thêm lựa chọn',
      horizontalAlign: 'left',
      verticalAlign: 'top',
      fields: [
        {
          placeholder: 'Lựa chọn',
          property: 'name',
          constraints: ['required'],
          current: option || ''
        }
      ],
      onSubmit: async (data) => {
        console.log( 'data',data)
        if (!data.name) return
        if(!option){
          this.simpleData[index].options.push(data.name)
        }else{
          if (indexOption > -1 ) {
            this.simpleData[index].options[indexOption] = data.name
          }
        }
        console.log('res', this.simpleData)
        this.canBeSaved.next(true)
        this.container.change.emit();
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }
  trackByFn(index, item) {
    return item; // or item.id
  }
}
