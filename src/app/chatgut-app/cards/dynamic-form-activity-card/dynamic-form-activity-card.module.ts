import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicFormActivityCardComponent } from 'app/chatgut-app/cards/dynamic-form-activity-card/dynamic-form-activity-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatSelectModule, MatChipsModule, MatIconModule, MatTooltipModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { NgJsonEditorModule } from 'ang-jsoneditor';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { TextFieldModule } from '@angular/cdk/text-field';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    TextFieldModule,
    FormsModule,
    NgJsonEditorModule,
    CardComponentsModule,
    MatSelectModule,
    MatChipsModule,
    DragDropModule,
    MatIconModule,
    MatTooltipModule,
  ],
  declarations: [DynamicFormActivityCardComponent],
  entryComponents: [DynamicFormActivityCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: DynamicFormActivityCardComponent }
  ]
})
export class DynamicFormActivityCardModule { }
