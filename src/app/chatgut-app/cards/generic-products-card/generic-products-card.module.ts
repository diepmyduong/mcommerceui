import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenericProductsCardComponent } from 'app/chatgut-app/cards/generic-products-card/generic-products-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule, MatButtonModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SwiperModule } from 'angular2-useful-swiper';
import { ButtonContainerModule } from 'app/shared/buttons/button-container/button-container.module';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FlexLayoutModule,
    SwiperModule,
    ButtonContainerModule,
    FormsModule,
    CardComponentsModule
  ],
  declarations: [GenericProductsCardComponent],
  entryComponents: [GenericProductsCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: GenericProductsCardComponent }
  ]
})
export class GenericProductsCardModule { }
