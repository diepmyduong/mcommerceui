import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { merge } from 'lodash-es'
@Component({
  selector: 'app-infusion-achive-goal-card',
  templateUrl: './infusion-achive-goal-card.component.html',
  styleUrls: ['./infusion-achive-goal-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfusionAchiveGoalCardComponent extends BaseCard implements OnInit {

	componentName = "InfusionAchiveGoalCardComponent";
  data: any = [
    {display: "Integration", code: "integration", value: "", required: true},
    {display: "Call Name", code: "callName", value: "", required:true},
  ]
  users: any = []
  loadDone: boolean = false
  constructor(
    public changeRef: ChangeDetectorRef
  ) { 
    super()
  }
  cardName = 'Thêm luồng Infusion'

  ngAfterViewInit() {
  }
  ngOnInit() {
    let userData = this.card.option.data
    this.data[0].value = userData.integration
    this.data[1].value = userData.callName
    this.reload()
  }
  async reload(local: boolean = false) {
    try{
      this.loadDone = false
      let res = merge([], await this.chatbotApi.infusion.getList({ local }))
      this.users = res.filter( user => {
        return user.status == "active"
      })
    } catch(err) {
      console.log('err',err)
      this.alert.error("Lỗi", "Lỗi khi lấy dữ liệu")
    } finally {
      this.loadDone = true
      this.detectChanges()
    }
  }
  cardValid(): string {
    this.card.option.data.integration = this.data[0].value
    this.card.option.data.callName = this.data[1].value
    return super.cardValid()
  }
}
