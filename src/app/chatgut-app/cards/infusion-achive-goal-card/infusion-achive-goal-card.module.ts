import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatFormFieldModule, MatInputModule, MatProgressSpinnerModule, MatSelectModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { InfusionAchiveGoalCardComponent } from './infusion-achive-goal-card.component';


@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatFormFieldModule,
    FormsModule,
    CardComponentsModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatInputModule
  ],
  declarations: [InfusionAchiveGoalCardComponent],
  entryComponents: [InfusionAchiveGoalCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: InfusionAchiveGoalCardComponent }
  ]
})
export class InfusionAchiveGoalCardModule { }
