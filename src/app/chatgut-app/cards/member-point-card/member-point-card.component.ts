import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { iCard } from 'app/services/chatbot'
import { NgForm } from '@angular/forms'
import { iStoryGroup } from 'app/services/chatbot/api/crud/storyGroup';
import { merge } from 'lodash-es'

@Component({
  selector: 'app-member-point-card',
  templateUrl: './member-point-card.component.html',
  styleUrls: ['./member-point-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MemberPointCardComponent extends BaseCard implements OnInit {

	componentName = "MemberPointCardComponent";

  constructor(public changeRef: ChangeDetectorRef) { 
    super()
  }
  cardName = 'Tích điểm'

  async ngOnInit() {
  }

  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card),{
      mode: card.option.mode,
      point: card.option.point,
      key: card.option.key
    }))
  }
}
