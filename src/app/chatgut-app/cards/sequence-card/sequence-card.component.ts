import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { iCard } from 'app/services/chatbot'
import { NgForm } from '@angular/forms'
import { iStoryGroup } from 'app/services/chatbot/api/crud/storyGroup';
import { merge } from 'lodash-es'

@Component({
  selector: 'app-sequence-card',
  templateUrl: './sequence-card.component.html',
  styleUrls: ['./sequence-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SequenceCardComponent extends BaseCard implements OnInit {

	componentName = "SequenceCardComponent";

  constructor(public changeRef: ChangeDetectorRef) { 
    super()
  }
  cardName = 'Gắn/hủy luồng'
  loadingStoryGroups = false
  storyGroups: iStoryGroup[] = []

  async ngOnInit() {
    await this.reload()
    this.detectChanges()

  }

  async reload(local = true) {
    try {
      this.loadingStoryGroups = true
      this.storyGroups = await this.chatbotApi.storyGroup.getList({
        local,
        query: {
          fields: ["_id", "name", "type", "stories", "sequenceEvents"],
          filter: { type: 'sequence' }, 
          order: { 'createdAt': 1 } 
        }
      })
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi tải câu chuyện");
      // console.error('Lỗi', 'Lỗi xảy ra khi tải câu chuyện')
      // this.alert.error("Lỗi khi tải câu chuyện", err.message)
    } finally {
      this.loadingStoryGroups = false
    }
  }

  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card),{
      mode: card.option.mode,
      isForce: card.option.isForce,
      storyGroupId: card.option.storyGroupId
    }))
  }
}
