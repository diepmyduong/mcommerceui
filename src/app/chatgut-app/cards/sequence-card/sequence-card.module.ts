import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatCheckboxModule, MatButtonToggleModule, MatSelectModule, MatTooltipModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { SequenceCardComponent } from './sequence-card.component';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatCheckboxModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatTooltipModule,
    FormsModule,
    CardComponentsModule
  ],
  declarations: [SequenceCardComponent],
  entryComponents: [SequenceCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: SequenceCardComponent }
  ]
})
export class SequenceCardModule { }
