import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from "@angular/core";
import { BaseCard } from "app/shared/cards/base-card";
import { NgForm } from "@angular/forms";
import { iCard } from "app/services/chatbot";
import { merge } from "lodash-es";
declare var Buffer: any;
@Component({
  selector: "app-audio-card",
  templateUrl: "./audio-card.component.html",
  styleUrls: ["./audio-card.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AudioCardComponent extends BaseCard implements OnInit {
  componentName = "AudioCardComponent";
  public isVoiceAPI = false;
  public voiceParams: any = {};
  constructor(public changeRef: ChangeDetectorRef) {
    super();
  }
  cardName = "Âm thanh";

  ngAfterContentInit() {
    super.ngAfterContentInit();
    const url = this.card.option.attachment.payload.url;
    if (/\/deeplink\/voice\?base/g.test(url)) {
      const base64 = new URL(url).searchParams.get("base");
      this.voiceParams.say = new Buffer(base64, "base64").toString("utf8");
      this.isVoiceAPI = true;
      this.cardName = "Văn bản Nói (Beta)";
    }
  }

  resetForm(formCtrl: NgForm, card: iCard) {
    formCtrl.resetForm(
      merge(super.resetForm(formCtrl, card), {
        url: card.option.attachment.payload.url,
        say: this.isVoiceAPI ? this.voiceParams.say : ""
      })
    );
  }

  async beforeSave() {
    if (this.isVoiceAPI) {
      const url = await this.generateVoiceUrl();
      if (this.card.option.attachment.payload.url != url) {
        this.card.option.attachment.payload.url = url;
      }
    }
  }

  async generateVoiceUrl() {
    return await this.chatbotApi.deeplink.getVoiceBufferLink(this.voiceParams);
  }
}
