import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AudioCardComponent } from 'app/chatgut-app/cards/audio-card/audio-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    CardComponentsModule
  ],
  declarations: [AudioCardComponent],
  entryComponents: [AudioCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: AudioCardComponent }
  ]
})
export class AudioCardModule { }
