import { Component, OnInit, ChangeDetectorRef, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { NgForm } from '@angular/forms';
import { iCard, iEnvironment } from 'app/services/chatbot';
import { MatDialog } from '@angular/material';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { GoogleApiService } from 'app/services/googleApi/googleApi.service';
import { iEditInfoDialogData, EditInfoDialog } from 'app/modals/edit-info/edit-info.component';
import { CdkDropList, CdkDropListGroup, CdkDropListContainer, CdkDrag } from '@angular/cdk/drag-drop';
import { iGsheetUser } from 'app/services/chatbot/api/crud/gsheetUser';
import { merge, indexOf } from 'lodash-es'
@Component({
  selector: 'app-google-spreadsheet-action-card',
  templateUrl: './google-spreadsheet-action-card.component.html',
  styleUrls: ['./google-spreadsheet-action-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GoogleSpreadsheetActionCardComponent extends BaseCard implements OnInit {

  componentName = "GoogleSpreadsheetActionCardComponent";

  @ViewChild(CdkDropListGroup) listGroup: CdkDropListGroup<CdkDropList>;
  @ViewChild(CdkDropList) placeholder: CdkDropList;
  constructor(
    public changeRef: ChangeDetectorRef,
    public googleApiSrv: GoogleApiService,
    public dialog: MatDialog
  ) {
    super()
  }
  cardName = 'Google spreadsheet'
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  allowUse: boolean = undefined
  chipInput: string = ''
  loadUserDone: boolean = false
  gsheetUsers: iGsheetUser[] = []

  resetForm(formCtrl: NgForm, card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card), {
      gsheetUser: card.option.gsheetUser,
      fields: card.option.fields,
      ssId: card.option.ssId,
      sheet: card.option.sheet,
      method: card.option.method,
      key: card.option.key
    }))
  }
  async ngOnInit() {
    super.ngOnInit()
   
    await this.reload()

  }

  ngAfterViewInit() {
    let phElement = this.placeholder.element.nativeElement;

    phElement.style.display = 'none';
    phElement.parentNode.removeChild(phElement);
  }

  async reload(local: boolean = true) {
    try{
      this.loadUserDone = false
      const { app } = this.chatbotApi.chatbotAuth
      this.allowUse = !!app.gsheetUser
  
      let gsheetUsers =  await this.chatbotApi.gsheetUser.getUser()
      console.log( 'sub',gsheetUsers,this.card)
      if (!this.card.option.gsheetUser){
        this.card.option.gsheetUser = ""
      } else {
        for (let el of gsheetUsers) {
          if(el._id == this.card.option.gsheetUser && el.status == "deactive"){
            this.card.option.gsheetUser = ""
            break
          }
        }
      }
    
      this.gsheetUsers = gsheetUsers.filter(user=> user.status != "deactive")
      if(this.gsheetUsers.length > 0 && !this.card.option.gsheetUser){
        this.card.option.gsheetUser = this.gsheetUsers[0]._id
      }
    } catch(err){
      this.alert.handleError(err, "Lỗi khi lấy dữ liệu");
      // this.alert.error("Lỗi khi lấy dữ liệu", err.message)
    } finally{
      this.loadUserDone = true
      this.changeRef.detectChanges()
    }
   
    
  }

  addField(): void {
    if ((this.chipInput || '').trim()) {
      this.card.option.fields.push(this.chipInput.trim());
      this.chipInput = ''
      this.container.change.emit();
    }

    this.detectChanges();
  }

  removeField(index: number): void {
    if (index >= 0) {
      this.card.option.fields.splice(index, 1);
    }
    this.canBeSaved.next(true)
    this.container.change.emit();
  }
  async connectGooogleSpreadsheet() {
    try {
      const response: any = await this.googleApiSrv.authorize()
      let res =  await this.chatbotApi.app.connectGSheet({ code: response.code })
      this.card.option.gsheetUser = res._id
      await this.reload()
      this.canBeSaved.next(true)
      this.container.change.emit();
      
      this.allowUse = true
      this.alert.success("Thành công", "Kết nối với google spreadsheet thành công")
    } catch (error) {
      this.allowUse = false
      this.alert.handleError(error);
      // this.alert.error("Có lỗi xảy ra", error.message)
    };
  }
  async openDialogEdit(title: string){
    let data: iEditInfoDialogData = {
      title: "",
      confirmButtonText: "Xác nhận",
      inputs: [
        {
          title,
          property: "value",
          current: '',
          type: "text",
          required: true,
        }
      ]
    };
    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    });
    const result = await dialogRef.afterClosed().toPromise();
    return result? result.value : undefined
  }
  async createSpreadsheet() {
    if (!this.card.option.gsheetUser) {
      return this.alert.error("Chưa kết nối", "Thẻ này chưa kết nối với tài khoản người dùng google nào")
    }   
    let nameApp = this.chatbotApi.chatbotAuth.app.activePage['name']
    console.log( 'app',nameApp)
    let nameSheet = this.card.option.sheet
    if(!nameSheet){
      nameSheet = await this.openDialogEdit("Nhâp tên bảng");
      if(!nameSheet) return
      this.card.option.sheet = nameSheet
    }else{
      if (!await this.alert.question("Tạo bảng tính mới", "Tính năng này sẽ tạo một bảng tính mới trong tài khoản google spreadsheet của bạn", "Tạo mới")) return;
    }
    try {
      const ssInfo = await this.chatbotApi.gsheetUser.createSpreadsheet(this.card.option.gsheetUser, { name:nameApp, sheets:[nameSheet] })
      await this.alert.success("Thành công", "Tạo bảng tính mới thành công")
      this.card.option.ssId = ssInfo.spreadsheetId
      this.canBeSaved.next(true)
      this.container.change.emit();
      window.open(
        ssInfo.spreadsheetUrl,
        "_blank"
      )
    } catch (error) {
      this.allowUse = false
      this.alert.handleError(error);
      // this.alert.error("Có lỗi xảy ra", error.message)
    };
  }
  async openSpreadsheet(local = true) {
    if (/^(\{{2})env\.([a-zA-Z0-9]*)(\}{2})$/g.test(this.card.option.ssId)) {
      let envName = this.card.option.ssId.replace(/({|}){2}/g, '').replace('env.', '')
      const { app } = this.chatbotApi.chatbotAuth
      let environment
      if (app.environment && typeof app.environment == "string") {
        environment = await this.chatbotApi.environment.getItem(app.environment, { local });
      } else {
        environment = await this.chatbotApi.environment.getItem((app.environment as iEnvironment)._id, { local });
      }
      window.open(
        `https://docs.google.com/spreadsheets/d/${environment.data[envName]}/edit`,
        "_blank"
      )
    } else {
      window.open(
        `https://docs.google.com/spreadsheets/d/${this.card.option.ssId}/edit`,
        "_blank"
      )
    }
  }

  async openEditorDialog(el: any, index: number) {
    if (!el) {
      el = {
        default_action: {}
      }
    }
    let data: iEditInfoDialogData = {
      title: "",
      confirmButtonText: "Xác nhận",
      inputs: [
        {
          title: "Dữ liệu",
          property: "field",
          current: el || '',
          type: "text",
          required: true,
        }
      ]
    };
    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    });
    const result = await dialogRef.afterClosed().toPromise();
    console.log('res', result)
    if (index > -1 && result.field) {
      this.card.option.fields[index] = result.field
      this.canBeSaved.next(true)
      this.container.change.emit();
    }


  }


  public target: CdkDropList = null;
  public targetIndex: number;
  public source: CdkDropListContainer = null;
  public sourceIndex: number;

  drop() {
    if (!this.target)
      return;

    let phElement = this.placeholder.element.nativeElement;
    let parent = phElement.parentNode;

    phElement.style.display = 'none';

    parent.removeChild(phElement);
    parent.appendChild(phElement);
    parent.insertBefore(this.source.element.nativeElement, parent.children[this.sourceIndex]);

    this.target = null;
    this.source = null;

    if (this.sourceIndex != this.targetIndex) {
      let element = this.card.option.fields[this.sourceIndex];
      this.card.option.fields.splice(this.sourceIndex, 1);
      this.card.option.fields.splice(this.targetIndex, 0, element);
      this.container.change.emit();
    }
  }

  enter = (drag: CdkDrag, drop: CdkDropList) => {
    if (drop == this.placeholder)
      return true;

    let phElement = this.placeholder.element.nativeElement;
    let dropElement = drop.element.nativeElement;

    let dragIndex = indexOf(dropElement.parentNode.children, drag.dropContainer.element.nativeElement);
    let dropIndex = indexOf(dropElement.parentNode.children, dropElement);
    console.log(dragIndex, dropIndex)

    if (!this.source) {
      this.sourceIndex = dragIndex;
      this.source = drag.dropContainer;

      let sourceElement = this.source.element.nativeElement;
      phElement.style.width = sourceElement.clientWidth + 'px';
      phElement.style.height = sourceElement.clientHeight + 'px';

      sourceElement.parentNode.removeChild(sourceElement);
    }

    this.targetIndex = dropIndex;
    this.target = drop;

    phElement.style.display = '';
    dropElement.parentNode.insertBefore(phElement, (dragIndex < dropIndex)
      ? dropElement.nextSibling : dropElement);

    this.source.start();
    this.placeholder.enter(drag, drag.element.nativeElement.offsetLeft, drag.element.nativeElement.offsetTop);

    return false;
  }

  trackByFn(index, item) {
    return item; // or item.id
  }

}