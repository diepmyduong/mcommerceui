import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GoogleSpreadsheetActionCardComponent } from 'app/chatgut-app/cards/google-spreadsheet-action-card/google-spreadsheet-action-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatButtonModule, MatChipsModule, MatIconModule, MatProgressSpinnerModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { CardImageModule } from 'app/shared/cards/card-image/card-image.module';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { DragDropModule } from '@angular/cdk/drag-drop'
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  imports: [
    CommonModule,
    DragDropModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    FlexLayoutModule,
    TextFieldModule,
    FormsModule,
    CardImageModule,
    MatChipsModule,
    MatIconModule,
    CardComponentsModule,
    MatProgressSpinnerModule
  ],
  declarations: [GoogleSpreadsheetActionCardComponent],
  entryComponents: [GoogleSpreadsheetActionCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: GoogleSpreadsheetActionCardComponent }
  ]
})
export class GoogleSpreadsheetActionCardModule { }
