import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { NgForm } from '@angular/forms';
import { JsonEditorOptions, JsonEditorComponent } from 'ang-jsoneditor';
import { iCard } from 'app/services/chatbot/api/crud/card';
import { merge } from 'lodash-es'

@Component({
  selector: 'app-json-input-action-card',
  templateUrl: './json-input-action-card.component.html',
  styleUrls: ['./json-input-action-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JsonInputActionCardComponent extends BaseCard implements OnInit {

	componentName = "JsonInputActionCardComponent";

  @ViewChild(JsonEditorComponent) editor: JsonEditorComponent;
  constructor(
     public changeRef: ChangeDetectorRef
  ) { 
     super()
     this.configJsonEditor();
  }
  cardName = 'Dữ liệu JSON'
  jsonEditorConfig:JsonEditorOptions = new JsonEditorOptions()
  data = {}

  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card),{
      json: card.option.json,
      key: card.option.key
    }))
  }

  ngAfterViewInit() {
    if (this.card) {
      this.data = this.card.option.json
    } else {
      this.data = {}
    }
    
    this.detectChanges()
  }

  configJsonEditor() {
    this.jsonEditorConfig.mode = 'code';
    let deboud;
    this.jsonEditorConfig.onChange = () => { 
      if(deboud) clearTimeout(deboud);
      deboud = setTimeout(this.onJSONChange.bind(this), 250);
    }
  }

  onJSONChange() {
    try {
      this.card.option.json = this.editor.get()
    } catch (err) {
      return;
    }
    this.canBeSaved.next(true)
  }

  cardValid(): string {
    try {
      this.editor.get()
      return super.cardValid()
    } catch (err) {
      return `Thẻ thứ ${this.index + 1} (${this.cardName}) bị lỗi. JSON không hợp lệ`
    }
  }
}

