import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JsonInputActionCardComponent } from 'app/chatgut-app/cards/json-input-action-card/json-input-action-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { NgJsonEditorModule } from 'ang-jsoneditor';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    NgJsonEditorModule,
    CardComponentsModule
  ],
  declarations: [JsonInputActionCardComponent],
  entryComponents: [JsonInputActionCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: JsonInputActionCardComponent }
  ]
})
export class JsonInputActionCardModule { }
