import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { iCard, iSubscriberTag } from 'app/services/chatbot'
import { NgForm } from '@angular/forms'
import { iStoryGroup } from 'app/services/chatbot/api/crud/storyGroup';
import { merge } from 'lodash-es'

@Component({
  selector: 'app-tag-card',
  templateUrl: './tag-card.component.html',
  styleUrls: ['./tag-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TagCardComponent extends BaseCard implements OnInit {

	componentName = "TagCardComponent";

  constructor(public changeRef: ChangeDetectorRef) { 
    super()
  }
  cardName = 'Gắn/gỡ tag'
  loadingTags = false
  tags: iSubscriberTag[] = []

  async ngOnInit() {
    await this.reload()
    this.detectChanges()

  }

  async reload(local = true) {
    try {
      this.loadingTags = true
      this.changeRef.markForCheck()
      this.tags = await this.chatbotApi.subscriberTag.getList({
        local,
        query: { fields: "name color", order: { position: 1 } }
      })
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi tải tag");
      // console.error('Lỗi', 'Lỗi xảy ra khi tải câu chuyện')
      // this.alert.error("Lỗi khi tải câu chuyện", err.message)
    } finally {
      this.loadingTags = false
      this.changeRef.detectChanges()
    }
  }

  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card),{
      mode: card.option.mode,
      tag: card.option.tag
    }))
  }
}
