import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule, MatAutocompleteModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { AddLabelCardComponent } from './add-label-card.component';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    CardComponentsModule,
    MatAutocompleteModule,
    ReactiveFormsModule 
  ],
  declarations: [AddLabelCardComponent],
  entryComponents: [AddLabelCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: AddLabelCardComponent }
  ]
})
export class AddLabelCardModule { }
