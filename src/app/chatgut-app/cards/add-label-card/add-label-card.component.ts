import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { iCard, iPage } from 'app/services/chatbot'
import { NgForm, FormControl } from '@angular/forms'
import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { merge} from 'lodash-es'

@Component({
  selector: 'app-add-label-card',
  templateUrl: './add-label-card.component.html',
  styleUrls: ['./add-label-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddLabelCardComponent extends BaseCard implements OnInit {

	componentName = "AddLabelCardComponent";
  labels = []
  filterLabels :  Observable<any[]>
  myControl = new FormControl();
  isFirst : boolean = true
  constructor(
    public changeRef: ChangeDetectorRef
  ) { 
    super()
  }
  cardName = 'Thêm nhãn'
  async ngOnInit() {
    super.ngOnInit()
    let result = await this.chatbotApi.label.getAllLabels((this.chatbotApi.chatbotAuth.app.activePage as iPage)._id)
    this.labels = result.data;
    this.myControl.setValue(this.card.option.name)
    this.filterLabels = this.myControl.valueChanges.pipe(
      startWith( this.card.option.name),
      map(value =>{
        //do not show save button when init value
        if(!this.isFirst){
          this.card.option.name = value
          this.canBeSaved.next(true)
          this.container.change.emit()
          console.log( 'name', this.card.option.name)
        }else{
          this.isFirst = false
        }
       
        return  this._filter(value)
      })
    );
  }
   _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.labels.filter(label => label.name.toLowerCase().indexOf(filterValue) > -1);
  }
  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card),{
      label: card.option.name
    }))
  }

  ngAfterViewInit() {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
  }
}
