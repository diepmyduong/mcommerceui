import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { merge } from 'lodash-es'

@Component({
  selector: 'app-infusion-add-contact-card',
  templateUrl: './infusion-add-contact-card.component.html',
  styleUrls: ['./infusion-add-contact-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfusionAddContactCardComponent extends BaseCard implements OnInit {

	componentName = "InfusionAddContactCardComponent";

  constructor(
    public changeRef: ChangeDetectorRef
  ) {
     super()
  }
  cardName = 'Thêm contact Infusion'
  data: any = [
    {display: "Fist Name", code: "given_name", value: ""},
    {display: "Last Name", code: "family_name", value: ""},
    {display: "Email", code: "email_addresses", value: "", required:true},
    {display: "Phone", code: "phone_numbers", value: ""},
  ]
  users: any = []
  loadDone: boolean = false
  type: string = "add"

  ngAfterViewInit() {
    
  }

  ngOnInit() {
    if (this.extra){
      this.type = this.extra.type
      this.cardName = "Cập nhật contact Infusion"
    }
    let userData = this.card.option.data
    this.data[0].value = userData.given_name
    this.data[1].value = userData.family_name
    this.data[2].value = userData.email_addresses[0].email
    this.data[3].value = userData.phone_numbers[0].number
    this.reload()
  }
  async reload(local: boolean = false) {
    try{
      this.loadDone = false
      let res = merge([], await this.chatbotApi.infusion.getList({ local }))
      this.users = res.filter( user => {
        return user.status == "active"
      })
    } catch(err) {
      console.log('err',err)
      this.alert.error("Lỗi", "Lỗi khi lấy dữ liệu")
    } finally {
      this.loadDone = true
      this.detectChanges()
    }
  }
  cardValid(): string {
    this.card.option.data.given_name = this.data[0].value
    this.card.option.data.family_name = this.data[1].value
    this.card.option.data.email_addresses[0].email = this.data[2].value
    this.card.option.data.phone_numbers[0].number = this.data[3].value
    return super.cardValid()
  }
  paymentWindow: Window;
  paymentPopupTimmer: any;
  async connectInfusion(){
    if (this.paymentWindow) {
      clearInterval(this.paymentPopupTimmer);
      this.paymentWindow.close()
    }
    this.getCodeInfu()
  }
  getCodeInfu(){
    try {
      const client_id = "hrd8j9st5yjcgez9e8k7gryc";
      const redirect_uri = location.origin+'/apps';
      const url = `https://signin.infusionsoft.com/app/oauth/authorize?client_id=${client_id}&redirect_uri=${redirect_uri}&response_type=code&scope=full`
      
      let width = 800
      let height = 700
      let left = (window.screen.width / 2) - ((width / 2) + 10);
      let top = (window.screen.height / 2) - ((height / 2) + 50);
      this.paymentWindow = window.open('about:blank', "MsgWindow", "width=" + width + ",height=" + height + ",left=" + left+ 
      ",top=" + top + ",screenX=" + left + ",screenY="+ top);
      window.focus();

      this.paymentWindow.location.href = url;
      this.paymentWindow.window.focus();
      (<any>window).infusionCallback = this.onInfusionCallback.bind(this);
      this.watchPaymentPopup(this.paymentWindow);
    } catch(err) {
      console.log("err: ", err)
    } finally {

    }
  }
  async onInfusionCallback(data: any, childWindow) {
    console.log('on payment callback', data);
    childWindow.close()
    try{
      let res = await this.chatbotApi.app.connectInfusion({code: data.code})
      this.alert.success('Thành công','Bạn đã kết nối thành công')
      this.reload()
    } catch(err){
      this.alert.error("Lỗi", "Kết nối Infusion thất bại")
    } finally{
          
    }
  }
  async watchPaymentPopup(window: any) {
    if (this.paymentPopupTimmer) clearInterval(this.paymentPopupTimmer);
    this.paymentPopupTimmer = setInterval(async () => {
      if (window.closed) {
        clearInterval(this.paymentPopupTimmer);
        // this.isBooking = false
        this.paymentWindow = null
      }
    }, 500);
  }
}