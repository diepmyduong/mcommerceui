import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule,  MatFormFieldModule, MatSelectModule, MatProgressSpinnerModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { InfusionAddContactCardComponent } from './infusion-add-contact-card.component';


@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatFormFieldModule,
    FormsModule,
    CardComponentsModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatInputModule
  ],
  declarations: [InfusionAddContactCardComponent],
  entryComponents: [InfusionAddContactCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: InfusionAddContactCardComponent }
  ]
})
export class InfusionAddContactCardModule { }
