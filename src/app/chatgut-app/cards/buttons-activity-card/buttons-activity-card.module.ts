import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonsActivityCardComponent } from 'app/chatgut-app/cards/buttons-activity-card/buttons-activity-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ButtonContainerModule } from 'app/shared/buttons/button-container/button-container.module';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    TextFieldModule,
    FormsModule,
    CardComponentsModule,
    ButtonContainerModule
  ],
  declarations: [ButtonsActivityCardComponent],
  entryComponents: [ButtonsActivityCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ButtonsActivityCardComponent }
  ]
})
export class ButtonsActivityCardModule { }
