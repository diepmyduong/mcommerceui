import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { NgForm } from '@angular/forms'
import { iCard } from 'app/services/chatbot'
import { IValidButtons, VALID_BUTTONS_1 } from 'app/shared/buttons/button-container/validButtons';
import { ButtonContainerComponent } from 'app/shared/buttons/button-container/button-container.component';
import { ButtonService } from 'app/services/button.service';
import { merge} from 'lodash-es'

@Component({
  selector: 'app-buttons-activity-card',
  templateUrl: './buttons-activity-card.component.html',
  styleUrls: ['./buttons-activity-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonsActivityCardComponent extends BaseCard implements OnInit {

  componentName = "ButtonsActivityCardComponent";
  @ViewChild("btnContainer", { read: ButtonContainerComponent }) btnContainer: ButtonContainerComponent
  constructor(private buttonSrv: ButtonService, public changeRef: ChangeDetectorRef) { 
    super()
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  validButtons: IValidButtons = VALID_BUTTONS_1
  cardName = 'Tương tác nút bấm'

  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.resetForm(merge(super.resetForm(formCtrl, card),{
      text: card.option.attachment.payload.text,
      key: card.option.key
    }));
  }

  cardValid(): string {
    if(this.card.option.attachment.payload.buttons.length == 0) {
      return `Thẻ thứ ${this.index + 1} (${this.cardName}) bị lỗi. Yêu cầu ít nhất 1 nút.`
    } else {
      return super.cardValid();
    }
  }

  onButtonsChange(buttons: any[]) {
    this.card.option.attachment.payload.buttons = buttons
    this.canBeSaved.next(true)
    this.container.change.emit();
  }

}