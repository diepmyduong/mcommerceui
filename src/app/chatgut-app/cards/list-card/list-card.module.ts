import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListCardComponent } from 'app/chatgut-app/cards/list-card/list-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatButtonModule, MatTooltipModule, MatIconModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { SwiperModule } from 'angular2-useful-swiper';
import { ButtonContainerModule } from 'app/shared/buttons/button-container/button-container.module';
import { CardImageModule } from 'app/shared/cards/card-image/card-image.module';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    SwiperModule,
    ButtonContainerModule,
    CardImageModule,
    CardComponentsModule
  ],
  declarations: [ListCardComponent],
  entryComponents: [ListCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ListCardComponent }
  ]
})
export class ListCardModule { }
