import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenericCategoriesCardComponent } from 'app/chatgut-app/cards/generic-categories-card/generic-categories-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule, MatButtonModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { SwiperModule } from 'angular2-useful-swiper';
import { ButtonContainerModule } from 'app/shared/buttons/button-container/button-container.module';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FlexLayoutModule,
    FormsModule,
    SwiperModule,
    ButtonContainerModule,
    CardComponentsModule
  ],
  declarations: [GenericCategoriesCardComponent],
  entryComponents: [GenericCategoriesCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: GenericCategoriesCardComponent }
  ]
})
export class GenericCategoriesCardModule { }
