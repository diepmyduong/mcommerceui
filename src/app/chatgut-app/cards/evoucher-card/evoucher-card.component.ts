import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { iEvoucher } from 'app/services/chatbot/api/crud/evoucher';

@Component({
  selector: 'app-evoucher-card',
  templateUrl: './evoucher-card.component.html',
  styleUrls: ['./evoucher-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EvoucherCardComponent extends BaseCard implements OnInit {

	componentName = "EvoucherCardComponent";

  constructor(public changeRef: ChangeDetectorRef) { 
    super()
  }
  cardName = 'Khuyến mãi'
  loading = false
  evouchers: iEvoucher[] = []

  async ngOnInit() {
    await this.reload()
    this.detectChanges()
  }

  async reload(local = true) {
    try {
      this.loading = true
      this.detectChanges()
      this.evouchers = await this.chatbotApi.evoucher.getList({
        local,
        query: {
          fields: ["_id", "title"],
          limit: 0,
          order: { 'createdAt': 1 }
        }
      })
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lấy danh sách khuyến mãi");
      // console.error('Lỗi', 'Lỗi xảy ra khi tải câu chuyện')
      // this.alert.error("Lỗi khi tải câu chuyện", err.message)
    } finally {
      this.loading = false
      this.detectChanges()
    }
  }

  openEvoucher(id) {
    this.portalService.pushPortal("EvoucherPortalComponent", { evoucherId: id })
  }

  // resetForm(formCtrl: NgForm, card: iCard) {
  //   console.log(this.card, card)
  //   formCtrl.reset(merge(super.resetForm(formCtrl, card), {
  //     mode: card.option.mode,
  //     evoucherId: card.option.evoucherId,
  //     key: card.option.key,
  //     code: card.option.code
  //   }))
  //   console.log(this.card, card)
  // }
}
