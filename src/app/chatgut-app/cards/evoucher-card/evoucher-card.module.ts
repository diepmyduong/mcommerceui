import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatCheckboxModule, MatButtonToggleModule, MatSelectModule, MatTooltipModule, MatProgressSpinnerModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { EvoucherCardComponent } from './evoucher-card.component';
import { HightlightTextareaModule } from 'app/directives/hightlight-textarea/hightlight-textarea.module';
@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    HightlightTextareaModule,
    FormsModule,
    CardComponentsModule
  ],
  declarations: [EvoucherCardComponent],
  entryComponents: [EvoucherCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: EvoucherCardComponent }
  ]
})
export class EvoucherCardModule { }
