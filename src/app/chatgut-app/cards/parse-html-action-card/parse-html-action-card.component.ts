import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { NgForm } from '@angular/forms';
import { JsonEditorOptions, JsonEditorComponent } from 'ang-jsoneditor';
import { iCard } from 'app/services/chatbot/api/crud/card';
import { merge } from 'lodash-es'

@Component({
  selector: 'app-parse-html-action-card',
  templateUrl: './parse-html-action-card.component.html',
  styleUrls: ['./parse-html-action-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ParseHtmlActionCardComponent extends BaseCard implements OnInit {

	componentName = "ParseHtmlActionCardComponent";

  @ViewChild(JsonEditorComponent) editor: JsonEditorComponent;
  constructor(
     public changeRef: ChangeDetectorRef
  ) { 
     super();
     this.configJsonEditor();
  }
  cardName = 'Dữ liệu Website'
  jsonEditorConfig:JsonEditorOptions = new JsonEditorOptions()
  data = {}

  configJsonEditor() {
    this.jsonEditorConfig.mode = 'code'
    let deboud;
    this.jsonEditorConfig.onChange = () => { 
      if(deboud) clearTimeout(deboud);
      deboud = setTimeout(this.onJSONChange.bind(this), 250);
    }
  }

  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card),{
      url: card.option.url,
      filter: card.option.filter,
      key: card.option.key
    }))
  }

  ngAfterViewInit() {
    if (this.card) {
      this.data = this.card.option.filter
    } else {
      this.data = {}
    }
    
    this.detectChanges();
  }
  onJSONChange() {
    try {
      this.card.option.filter = this.editor.get()
    } catch (err) {
      return;
    }
    this.canBeSaved.next(true)
  }

  cardValid(): string {
    try {
      this.editor.get()
      return super.cardValid()
    } catch (err) {
      return `Thẻ thứ ${this.index + 1} (${this.cardName}) bị lỗi. JSON không hợp lệ`
    }
  }
}
