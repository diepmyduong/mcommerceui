import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParseHtmlActionCardComponent } from 'app/chatgut-app/cards/parse-html-action-card/parse-html-action-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { NgJsonEditorModule } from 'ang-jsoneditor';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    NgJsonEditorModule,
    CardComponentsModule
  ],
  declarations: [ParseHtmlActionCardComponent],
  entryComponents: [ParseHtmlActionCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ParseHtmlActionCardComponent } 
  ]
})
export class ParseHtmlActionCardModule { }
