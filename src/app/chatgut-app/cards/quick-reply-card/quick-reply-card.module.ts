import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuickReplyCardComponent } from 'app/chatgut-app/cards/quick-reply-card/quick-reply-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ButtonContainerModule } from 'app/shared/buttons/button-container/button-container.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    FormsModule,
    ButtonContainerModule
  ],
  declarations: [QuickReplyCardComponent],
  entryComponents: [QuickReplyCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: QuickReplyCardComponent }
  ]
})
export class QuickReplyCardModule { }
