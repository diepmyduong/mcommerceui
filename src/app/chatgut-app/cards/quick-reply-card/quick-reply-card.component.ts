import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { VALID_BUTTONS_2 } from 'app/shared/buttons/button-container/validButtons';
import { ButtonContainerComponent } from 'app/shared/buttons/button-container/button-container.component';
import { ButtonService } from 'app/services/button.service';
@Component({
  selector: 'app-quick-reply-card',
  templateUrl: './quick-reply-card.component.html',
  styleUrls: ['./quick-reply-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuickReplyCardComponent extends BaseCard implements OnInit {

	componentName = "QuickReplyCardComponent";
  @ViewChild(ButtonContainerComponent) btnContainer: ButtonContainerComponent
  constructor(private buttonSrv: ButtonService, public changeRef: ChangeDetectorRef) {
     super();
  }
  quickReplies: any[]
  validButtons= VALID_BUTTONS_2
  draggable = false

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  onButtonsChange(buttons: any[]) {
    if (!this.quickReplies && this.card) {
      this.card.option.quick_replies = buttons
    } else {
      this.quickReplies = buttons;
    }
    this.canBeSaved.next(true)
    this.container.change.emit();
  }

  async remove() {
    if (!await this.alert.warning('Xóa trả lời nhanh', 'Bạn có muốn xóa nút trả lời nhanh này?'))
    delete this.card.option.quick_replies
    await this.chatbotApi.card.update(this.card._id, this.card)
    this.portalService.popPortal(this.portalIndex)
  }

  async saveQuickReplies() {
    const portal = this.container.parentPortal as any;
    try {
      const result = await this.chatbotApi.story.updateQuickReplies(portal.storyId, this.quickReplies);
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lưu");
      // this.alert.error("Lỗi khi lưu", err.message)
      throw Error;
    } finally {
    }
  }

  async save() {
    try {
      await this.saveContent();
    } catch (err) {
      throw err;
    }
  }

}
