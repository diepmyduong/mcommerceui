import { Component, OnInit, Input, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { NgForm } from '@angular/forms';
import { UpdateActionCardComponent } from 'app/chatgut-app/cards/update-action-card/update-action-card.component';

@Component({
  selector: 'app-update-field',
  templateUrl: './update-field.component.html',
  styleUrls: ['./update-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpdateFieldComponent extends BaseComponent implements OnInit {

  @Input() field: IActionUpdateField
  @Input() parent: UpdateActionCardComponent
  @ViewChild('fieldFrm', { read: NgForm }) fieldFrm: NgForm
  constructor(
    public changeRef: ChangeDetectorRef
  ) { 
    super("UpdateFieldComponent")
  }

  ngOnInit() {
  }

  ngAfterContentInit() {
    this.subscriptions.push(this.fieldFrm.valueChanges.subscribe(() => {
      this.field.isValid = this.fieldFrm.valid
      this.parent.canBeSaved.next(true)
      this.changeRef.detectChanges()
    }))
  }

}

export interface IActionUpdateField {
  id: string,
  property: string,
  value: string,
  isValid?: boolean
}
