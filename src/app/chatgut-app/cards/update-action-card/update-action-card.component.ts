import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { NgForm } from '@angular/forms';
import { IActionUpdateField } from 'app/chatgut-app/cards/update-action-card/update-field/update-field.component';
import { remove } from 'lodash-es'
import * as guid from 'guid'
import { iCard } from 'app/services/chatbot/api/crud/card';
import { merge } from 'lodash-es'
@Component({
  selector: 'app-update-action-card',
  templateUrl: './update-action-card.component.html',
  styleUrls: ['./update-action-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpdateActionCardComponent extends BaseCard implements OnInit {

	componentName = "UpdateActionCardComponent";

  constructor(
    public changeRef: ChangeDetectorRef
  ) {
     super()
  }
  cardName = 'Lưu dữ liệu'
  fields: IActionUpdateField[] = []

  resetForm(formCtrl: NgForm, card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card),{
    }))
  }

  ngAfterViewInit() {
    
  }

  ngOnInit() {
    if (this.card.option.fields) {
      this.fields = this.card.option.fields.map(f => {
        return {
          id: guid.raw(), 
          property: f.key, 
          value: f.value
        }
      })
    }
  }

  async addField() {
    this.fields.push({
      id: guid.raw(),
      property: "",
      value: ""
    })
    this.canBeSaved.next(true)
  }

  removeField(id: string) {
    remove(this.fields, f => f.id === id)
    this.canBeSaved.next(true)
  }

  cardValid() {
    const fields = []

    for (let f of this.fields) {
      if (!f.isValid) return `Thẻ thứ ${this.index + 1} (${this.cardName}) không hợp lệ. Xin nhập chính xác thông tin các trường.`
      fields.push({
        key: f.property,
        value: f.value
      })
    }
    this.card.option.fields = fields
    return super.cardValid()
  }

  
}