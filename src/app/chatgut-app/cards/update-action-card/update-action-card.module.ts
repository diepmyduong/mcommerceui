import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateActionCardComponent } from 'app/chatgut-app/cards/update-action-card/update-action-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatButtonModule, MatIconModule, MatFormFieldModule, MatInputModule, MatTooltipModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { UpdateFieldComponent } from './update-field/update-field.component';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    FlexLayoutModule,
    FormsModule,
    CardComponentsModule
  ],
  declarations: [UpdateActionCardComponent,UpdateFieldComponent],
  entryComponents: [UpdateActionCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: UpdateActionCardComponent }
  ]
})
export class UpdateActionCardModule { }
