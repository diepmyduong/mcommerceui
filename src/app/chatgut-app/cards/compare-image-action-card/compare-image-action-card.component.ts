import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { iCard } from 'app/services/chatbot';
import { BaseCard } from 'app/shared/cards/base-card';
import { merge} from 'lodash-es'

@Component({
  selector: 'app-compare-image-action-card',
  templateUrl: './compare-image-action-card.component.html',
  styleUrls: ['./compare-image-action-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompareImageActionCardComponent extends BaseCard implements OnInit {

  componentName = "CompareImageActionCardComponent";

  constructor(
    public changeRef: ChangeDetectorRef
  ) {
    super()
  }
  cardName = 'Tìm kiếm ảnh'

  resetForm(formCtrl: NgForm, card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card), {  
      image: card.option.image,
      key: card.option.key,
      imageKey: card.option.imageKey
    }))
  }

  ngAfterViewInit() {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
  }

  async runIntro() {
  }
  async onImageSourceChanged(event) {
    this.card.option.source = event
    this.canBeSaved.next(true)
  }
}
