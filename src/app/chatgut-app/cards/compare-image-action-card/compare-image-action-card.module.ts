import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompareImageActionCardComponent } from './compare-image-action-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatSelectModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { NgJsonEditorModule } from 'ang-jsoneditor';
import { CardImageModule } from 'app/shared/cards/card-image/card-image.module';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    FormsModule,
    NgJsonEditorModule,
    CardImageModule,
    CardComponentsModule
  ],
  declarations: [CompareImageActionCardComponent],
  entryComponents: [CompareImageActionCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: CompareImageActionCardComponent }
  ]
})
export class CompareImageActionCardModule { }
