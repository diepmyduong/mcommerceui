import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { NgForm } from '@angular/forms';
import { iCard } from 'app/services/chatbot';

@Component({
  selector: 'app-activity-datepicker-card',
  templateUrl: './activity-datepicker-card.component.html',
  styleUrls: ['./activity-datepicker-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatepickerCardComponent extends BaseCard implements OnInit {

  componentName = "DatepickerCardComponent";
  constructor(public changeRef: ChangeDetectorRef) {
    super()
 
  }
  cardName = 'Lấy dữ liệu ngày'
  formatDate = [
    {code:"HH:mm DD-MM-YYYY",display:"Đầy đủ"},
    {code:"DD-MM-YYYY",display:"Chỉ ngày, tháng, năm"},
    {code:"HH:mm",display:"Chỉ phút và giờ"},
    {code:"DD-MM",display:"Chỉ ngày và tháng"},
    {code:"DD",display:"Chỉ ngày"},
    {code:"MM",display:"Chỉ tháng"},
  ]
  resetForm(formCtrl: NgForm, card: iCard) {

  }

  ngAfterViewInit() {
    console.log( 'card',this.card)
    if (this.card) {
    
    }
    this.detectChanges();
  }

  cardValid(): string {
    try {
      return super.cardValid()
    } catch (err) {
      return `Thẻ thứ ${this.index + 1} (${this.cardName}) bị lỗi. JSON không hợp lệ`
    }
  }

}
