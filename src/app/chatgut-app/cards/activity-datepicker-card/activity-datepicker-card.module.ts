import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatSelectModule, MatTooltipModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { TextFieldModule } from '@angular/cdk/text-field';
import { DatepickerCardComponent } from './activity-datepicker-card.component';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    TextFieldModule,
    FormsModule,
    CardComponentsModule,
    MatSelectModule,
    MatTooltipModule,
  ],
  declarations: [DatepickerCardComponent],
  entryComponents: [DatepickerCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: DatepickerCardComponent }
  ]
})
export class DatepickerCardModule { }
