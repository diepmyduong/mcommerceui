import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicGenericCardComponent } from 'app/chatgut-app/cards/dynamic-generic-card/dynamic-generic-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ButtonContainerModule } from 'app/shared/buttons/button-container/button-container.module';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    TextFieldModule,
    FormsModule,
    FlexLayoutModule,
    ButtonContainerModule,
    CardComponentsModule
  ],
  declarations: [DynamicGenericCardComponent],
  entryComponents: [DynamicGenericCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: DynamicGenericCardComponent }
  ]
})
export class DynamicGenericCardModule { }
