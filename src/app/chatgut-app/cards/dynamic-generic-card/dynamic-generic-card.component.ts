import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { NgForm } from '@angular/forms';
import { iCard } from 'app/services/chatbot/api/crud/card';
import { VALID_BUTTONS_3 } from 'app/shared/buttons/button-container/validButtons';
import { ButtonService } from 'app/services/button.service';
import { ButtonContainerComponent } from 'app/shared/buttons/button-container/button-container.component';
import { merge } from 'lodash-es'

@Component({
  selector: 'app-dynamic-generic-card',
  templateUrl: './dynamic-generic-card.component.html',
  styleUrls: ['./dynamic-generic-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicGenericCardComponent extends BaseCard implements OnInit {

	componentName = "DynamicGenericCardComponent";

  @ViewChild("btnContainer", { read: ButtonContainerComponent }) btnContainer: ButtonContainerComponent
  constructor(private buttonSrv: ButtonService, public changeRef: ChangeDetectorRef) { 
    super()
  }
  cardName = 'Menu động'
  validButtons = VALID_BUTTONS_3

  ngOnDestroy() {
    super.ngOnDestroy();
  }
  
  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.resetForm(merge(super.resetForm(formCtrl, card),{
      data: card.option.data,
      title: card.option.mapping.title,
      subtitle: card.option.mapping.subtitle,
      image: card.option.mapping.image,
      offset: card.option.offset,
      limit: card.option.limit
    }))
  }

  onButtonsChange(buttons: any[]) {
    this.card.option.mapping.buttons = buttons
    this.canBeSaved.next(true)
  }
}
