import { Component, OnInit, ViewChild, Input, ViewChildren, QueryList, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { iSetting } from 'app/services/chatbot/api/crud/setting';
import { ButtonContainerComponent } from 'app/shared/buttons/button-container/button-container.component';
import { VALID_BUTTONS_5, VALID_BUTTONS_6 } from 'app/shared/buttons/button-container/validButtons';
import { ButtonService } from 'app/services/button.service';
import { ButtonComponent } from 'app/shared/buttons/button-component/button-component.component';
import { differenceBy } from 'lodash-es'
@Component({
  selector: 'app-menu-card',
  templateUrl: './menu-card.component.html',
  styleUrls: ['./menu-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuCardComponent extends BaseCard implements OnInit {

  componentName = "MenuCardComponent";
  @Input() localeMenuIndex: number
  @Input() setting: iSetting
  @ViewChild("swiper") swiperComp: any
  @ViewChildren(ButtonContainerComponent) btnContainersRef: QueryList<ButtonContainerComponent>;
  @ViewChild("secondMenu") secondMenu: ButtonContainerComponent
  @ViewChild("thirdMenu") thirdMenu: ButtonContainerComponent
  constructor(
    private buttonSrv: ButtonService,
    public changeRef: ChangeDetectorRef
  ) {
    super();
  }
  validButtons = VALID_BUTTONS_5;
  lastValidButtons = VALID_BUTTONS_6;
  swiperOptions: any = {
    pagination: { el: '.menu-pagination', type: 'bullets', clickable: true, dynamicBullets: true, },
    slidesPerView: 1,
    centeredSlides: true,
    navigation: { nextEl: '.menu-button-next', prevEl: '.menu-button-prev', },
    spaceBetween: 10,
    noSwiping: true,
    simulateTouch: false,
  } // Swiper Option
  activeMenu: number // Currend Active Menu level
  activeButton: any = {}
  menuContainers: ButtonContainerComponent[]
  self: this
  settingState: iSetting
  btnContainers: ButtonContainerComponent[] = [];
  draggable = false

  ngOnInit() {
    this.self = this
    this.updateSettingState()
  }

  ngAfterViewInit() {
    this.regisBtnContainers();
    this.btnContainersRef.changes.subscribe(this.regisBtnContainers.bind(this));
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  regisBtnContainers() {
    const containers = this.btnContainersRef.toArray();
    differenceBy(this.btnContainers, containers, 'containerId').forEach(c => this.buttonSrv.clearContainer(c));
    this.btnContainers = containers;
  }

  updateSettingState() {
    this.settingState = Object.assign({}, this.setting)
  }

  get swiper() { // Get Swiper Intanse
    return this.swiperComp.swiper
  }

  openSubMenu(menuIndex: number, subMenu: any) {
    this.activeMenu = menuIndex + 1
    this.viewRef.detectChanges()
    setTimeout(() => {
      switch (this.activeMenu) {
        case 2:
          this.secondMenu.setButtons(subMenu)
          break
        case 3:
          this.thirdMenu.setButtons(subMenu)
          break
      }
      this.swiper.slideTo(menuIndex)
    })
  }

  onClickMenuItem(buttonComp: ButtonComponent) {
    if (buttonComp.type == 'nested') {
      const { menuIndex } = buttonComp.container
      this.activeButton[menuIndex] = buttonComp.index
      this.openSubMenu(menuIndex, buttonComp.button.call_to_actions)
    };
  }

  onButtonsChange(menuIndex: number, buttons: any[]) {
    if (menuIndex === 1) {
      this.setting.option[this.localeMenuIndex].call_to_actions = buttons
    } else if (menuIndex === 2) {
      const firstMenuButtonIndex = this.activeButton[1]
      this.setting.option[this.localeMenuIndex]
        .call_to_actions[firstMenuButtonIndex]
        .call_to_actions = buttons
    } else if (menuIndex === 3) {
      const firstMenuButtonIndex = this.activeButton[1]
      const secondMenuButtonIndex = this.activeButton[2]
      this.setting.option[this.localeMenuIndex]
        .call_to_actions[firstMenuButtonIndex]
        .call_to_actions[secondMenuButtonIndex]
        .call_to_actions = buttons
    }
    this.activeMenu = menuIndex;
    this.dirty = true;
    this.container.change.emit()
    this.canBeSaved.next(true)
  }
  async save(){
    let minFlag = false;
    const min = 1;
    let option = this.setting.option[0];
    if (!option.call_to_actions || option.call_to_actions.length < min) {
      minFlag = true;
    } else {
      for (let option1 of option.call_to_actions) {
        if (option1.call_to_actions) {
          if (option1.call_to_actions.length < min) {
            minFlag = true;
            break;
          } else {
            for (let option2 of option1.call_to_actions) {
              if (option2.call_to_actions && option2.call_to_actions.length < min) {
                minFlag = true;
                break;
              }
            }
          }
        }
      }
    }

    if (minFlag == true) {
      this.alert.info("Chú ý", "Số nút trên menu không được nhỏ hơn " + min)
      return;
    }
    if (this.dirty && this.cardFrm) {
      this.cardFrm.form.disable()
      try {
        const setting = await this.chatbotApi.setting.update(this.setting._id, this.setting, { reload: true })
        // await this.chatbotApi.page.activeSetting(setting._id)
        this.cardFrm.form.enable()
        this.resetForm(this.cardFrm, this.setting)
        this.updateSettingState()
        this.container.change.emit({
          status: "save",
          data: this.setting
        })
        this.dirty = false;
      } catch (err) {
        this.cardFrm.form.enable()
        this.resetForm(this.cardFrm, this.settingState)
        throw err;
      }
    }
  }
   getOptionSetting() { 
    
    let minFlag = false;
    const min = 1;
    let option = this.setting.option[this.localeMenuIndex];
    if (!option.call_to_actions || option.call_to_actions.length < min) {
      minFlag = true;
    } else {
      for (let option1 of option.call_to_actions) {
        if (option1.call_to_actions) {
          if (option1.call_to_actions.length < min) {
            minFlag = true;
            break;
          } else {
            for (let option2 of option1.call_to_actions) {
              if (option2.call_to_actions && option2.call_to_actions.length < min) {
                minFlag = true;
                break;
              }
            }
          }
        }
      }
    }
    if (minFlag == true) {
      this.alert.info("Chú ý", "Menu số " + this.localeMenuIndex +", Số nút trên menu không được nhỏ hơn " + min)
      return;
    }
    return this.setting.option[this.localeMenuIndex]
  }

}
