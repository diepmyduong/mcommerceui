import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuCardComponent } from 'app/chatgut-app/cards/menu-card/menu-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule } from '@angular/material';
import { SwiperModule } from 'angular2-useful-swiper';
import { ButtonContainerModule } from 'app/shared/buttons/button-container/button-container.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
 
    SwiperModule,
    ButtonContainerModule,
    FormsModule
  ],
  declarations: [MenuCardComponent],
  entryComponents: [MenuCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: MenuCardComponent }
  ]
})
export class MenuCardModule { }
