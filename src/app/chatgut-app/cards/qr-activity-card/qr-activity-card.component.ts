import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { NgForm } from '@angular/forms';
import { iCard } from 'app/services/chatbot/api/crud/card';
import { merge } from 'lodash-es'
@Component({
  selector: 'app-qr-activity-card',
  templateUrl: './qr-activity-card.component.html',
  styleUrls: ['./qr-activity-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QrActivityCardComponent extends BaseCard implements OnInit {

	componentName = "QrActivityCardComponent";

  constructor(
     public changeRef: ChangeDetectorRef
  ) { 
     super()
  }
  cardName = 'Quét QR Code'

  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card),{
      text: card.option.text,
      key: card.option.key,
      fallback: card.option.fallback
    }))
  }

  ngAfterViewInit() {
  }
}
