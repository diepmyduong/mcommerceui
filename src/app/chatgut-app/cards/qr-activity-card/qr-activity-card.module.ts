import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QrActivityCardComponent } from 'app/chatgut-app/cards/qr-activity-card/qr-activity-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    TextFieldModule,
    FormsModule,
    CardComponentsModule
  ],
  declarations: [QrActivityCardComponent],
  entryComponents: [QrActivityCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: QrActivityCardComponent }
  ]
})
export class QrActivityCardModule { }
