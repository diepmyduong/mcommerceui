import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuickReplyActivityCardComponent } from 'app/chatgut-app/cards/quick-reply-activity-card/quick-reply-activity-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule, MatCheckboxModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ButtonContainerModule } from 'app/shared/buttons/button-container/button-container.module';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    TextFieldModule,
    FormsModule,
    ButtonContainerModule,
    CardComponentsModule
  ],
  declarations: [QuickReplyActivityCardComponent],
  entryComponents: [QuickReplyActivityCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: QuickReplyActivityCardComponent }
  ]
})
export class QuickReplyActivityCardModule { }
