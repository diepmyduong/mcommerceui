import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { NgForm } from '@angular/forms';
import { iCard } from 'app/services/chatbot/api/crud/card';
import { VALID_BUTTONS_4 } from 'app/shared/buttons/button-container/validButtons';
import { ButtonContainerComponent } from 'app/shared/buttons/button-container/button-container.component';
import { ButtonService } from 'app/services/button.service';
import { merge } from 'lodash-es'
@Component({
  selector: 'app-quick-reply-activity-card',
  templateUrl: './quick-reply-activity-card.component.html',
  styleUrls: ['./quick-reply-activity-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuickReplyActivityCardComponent extends BaseCard implements OnInit {

	componentName = "QuickReplyActivityCardComponent";
  @ViewChild(ButtonContainerComponent) btnContainer: ButtonContainerComponent
  constructor(
    private buttonSrv: ButtonService,
    public changeRef: ChangeDetectorRef
  ) { 
     super();
  }
  disallowAdvancedQuickReplies = true
  cardName = 'Trả lời nhanh'
  validButtons = VALID_BUTTONS_4 // Valid Menu Item for third Menu

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card),{
      text: card.option.text,
      key: card.option.key,
      allowAdditional: card.option.allowAdditional,
      fallback: card.option.fallback
    }))
  }

  onButtonsChange(buttons: any[]) {
    this.card.option.quick_replies = buttons
    this.canBeSaved.next(true)
  }

  cardValid(): string {
    if(this.card.option.quick_replies.length == 0) {
      return `Thẻ thứ ${this.index + 1} (${this.cardName}) bị lỗi. Yêu cầu ít nhất 1 nút.`
    } else {
      return super.cardValid();
    }
  }
}
