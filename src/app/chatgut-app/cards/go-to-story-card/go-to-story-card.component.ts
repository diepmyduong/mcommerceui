import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { NgForm } from '@angular/forms';
import { iCard, iStory } from 'app/services/chatbot';
import { StoriesPortalComponent } from 'app/chatgut-app/portals/stories-portal/stories-portal.component';
import { merge } from 'lodash-es'

@Component({
  selector: 'app-go-to-story-card',
  templateUrl: './go-to-story-card.component.html',
  styleUrls: ['./go-to-story-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GoToStoryCardComponent extends BaseCard implements OnInit {

  isNextOpened: boolean;
  componentName = "GoToStoryCardComponent";
  stories: iStory[] = []
  selectedStories: iStory[] = []
  loadingStories: boolean = false
  constructor(
    public changeRef: ChangeDetectorRef
  ) {
    super()
  }
  cardName = 'Chuyển câu chuyện'

  async ngOnInit() {
    await this.reload()
    this.selectedStories = this.stories.filter(x => this.card.option.story.includes(x._id))
    this.detectChanges()
  }

  async reload(local = true) {
    try {
      this.loadingStories = true
      this.stories = await this.chatbotApi.story.getList({
        local,
        query: {
          limit: 0,
          fields: ["_id", "name"],
          order: { 'createdAt': 1 },
          filter: { mode: 'normal' }
        }
      })
    } catch (err) {
      this.alert.handleError(err, 'Có lỗi xảy ra khi tải câu chuyện');
      // console.error('Lỗi', 'Lỗi xảy ra khi tải câu chuyện')
      // this.alert.error('Có lỗi xảy ra khi tải câu chuyện', err.message)
    } finally {
      this.loadingStories = false
    }
  }

  resetForm(formCtrl: NgForm, card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card), {
      story: card.option.story
    }))
  }

  ngAfterViewInit() {

  }

  async runIntro() {

  }
  async openGetStory(local: boolean = true) {
    const componentRef = await this.portalService.pushPortalAt(this.portalIndex + 1, "StoriesPortalComponent", {
      portalName: "Chuyển sang câu chuyện", select: true, multi: true, selectedStories: this.card.option.story
    });

    if (!componentRef) {
      return;
    }
    const component = componentRef.instance as StoriesPortalComponent
    this.subscriptions.onStorySaved = component.onStorySaved.subscribe(async storyIds => {
      try {
        this.stories = component.stories
        
        let reloadFlag = false
        for (let id of storyIds) {
          if (!this.stories.find(x => x._id == id)) {
            reloadFlag = true
            break
          }
        }        
        if (reloadFlag) {
          await this.reload(false)
        }

        this.selectedStories = this.stories.filter(x => storyIds.includes(x._id))
        this.card.option.story = storyIds
        component.showSaveButton();
      } catch (err) {
        this.alert.handleError(err, 'Có lỗi xảy ra khi chọn câu chuyện');
        // this.alert.error('Có lỗi xảy ra khi chọn câu chuyện', err.message)
      } finally {
        this.canBeSaved.next(true)
        this.container.change.emit();
        component.close() 
      }
    })
    this.subscriptions.onStoryClosed = component.onStoryClosed.subscribe(() => {
      component.onStorySaved.unsubscribe(); 
      component.onStoryClosed.unsubscribe();
    })
  }

}