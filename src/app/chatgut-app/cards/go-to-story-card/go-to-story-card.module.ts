import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { FormsModule } from '@angular/forms';
import { MatCardModule, MatFormFieldModule, MatListModule, MatInputModule, MatSliderModule } from '@angular/material';
import { GoToStoryCardComponent } from 'app/chatgut-app/cards/go-to-story-card/go-to-story-card.component';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatSliderModule,
    FormsModule,
    MatCardModule,
    CardComponentsModule
  ],
  declarations: [ GoToStoryCardComponent],
  entryComponents: [GoToStoryCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: GoToStoryCardComponent }
  ]
})
export class GoToStoryCardModule { }
