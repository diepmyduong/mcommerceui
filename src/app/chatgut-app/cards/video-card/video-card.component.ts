import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { NgForm } from '@angular/forms'
import { iCard } from 'app/services/chatbot/api/crud/card';
import { merge } from 'lodash-es'
@Component({
  selector: 'app-video-card',
  templateUrl: './video-card.component.html',
  styleUrls: ['./video-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VideoCardComponent extends BaseCard implements OnInit {

	componentName = "VideoCardComponent";

  constructor(public changeRef: ChangeDetectorRef) { 
     super()
  }
  cardName = 'Video'

  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.resetForm(merge(super.resetForm(formCtrl, card),{
      url: card.option.attachment.payload.url
    }))
  }
}
