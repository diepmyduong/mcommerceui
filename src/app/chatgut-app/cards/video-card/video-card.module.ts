import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoCardComponent } from 'app/chatgut-app/cards/video-card/video-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    CardComponentsModule
  ],
  declarations: [VideoCardComponent],
  entryComponents: [VideoCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: VideoCardComponent }
  ]
})
export class VideoCardModule { }
