import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RangeActivityCardComponent } from 'app/chatgut-app/cards/range-activity-card/range-activity-card.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    TextFieldModule,
    FormsModule,
    CardComponentsModule
  ],
  declarations: [RangeActivityCardComponent],
  entryComponents: [RangeActivityCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: RangeActivityCardComponent }
  ]
})
export class RangeActivityCardModule { }
