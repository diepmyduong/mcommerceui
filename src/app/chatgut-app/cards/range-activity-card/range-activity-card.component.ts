import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card';
import { NgForm } from '@angular/forms';
import { iCard } from 'app/services/chatbot/api/crud/card';
import { merge } from 'lodash-es'

@Component({
  selector: 'app-range-activity-card',
  templateUrl: './range-activity-card.component.html',
  styleUrls: ['./range-activity-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RangeActivityCardComponent extends BaseCard implements OnInit {

	componentName = "RangeActivityCardComponent";

  constructor(
    public changeRef: ChangeDetectorRef
  ) { 
     super()
  }
  cardName = 'Nhập số'
  disallowAdvancedQuickReplies = true

  resetForm(formCtrl: NgForm,card: iCard) {
    formCtrl.reset(merge(super.resetForm(formCtrl, card),{
      text: card.option.text,
      key: card.option.key,
      from: card.option.from,
      to: card.option.to,
      step: card.option.step,
      fallback: card.option.fallback
    }))
  }

  ngAfterViewInit() {
  }
}
