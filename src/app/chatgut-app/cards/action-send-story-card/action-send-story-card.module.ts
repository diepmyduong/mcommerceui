import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatCardModule, MatListModule, MatFormFieldModule, MatInputModule, MatTooltipModule, MatButtonModule, MatIconModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CardComponentsModule } from 'app/shared/cards/card-components.module';
import { ActionSendStoryCardComponent } from './action-send-story-card.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { UpdateFieldStoryComponent } from './update-field-story/update-field-story.component';



@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    CardComponentsModule,
    MatTooltipModule,
    MatButtonModule,
    MatIconModule,
    FlexLayoutModule,
  ],
  declarations: [ActionSendStoryCardComponent,UpdateFieldStoryComponent],
  entryComponents: [ActionSendStoryCardComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ActionSendStoryCardComponent }
  ]
})
export class ActionSendStoryCardModule { }
