import { Component, OnInit, Input, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { NgForm } from '@angular/forms';
import { ActionSendStoryCardComponent } from '../action-send-story-card.component';

@Component({
  selector: 'app-update-field-story',
  templateUrl: './update-field-story.component.html',
  styleUrls: ['./update-field-story.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpdateFieldStoryComponent extends BaseComponent implements OnInit {
  @Input() mode:string = 'field'
  @Input() field: IActionUpdateField
  @Input() parent: ActionSendStoryCardComponent
  @ViewChild('fieldFrm', { read: NgForm }) fieldFrm: NgForm
  @ViewChild('psidFrm', { read: NgForm }) psidFrm: NgForm
  constructor() { 
    super("UpdateFieldStoryComponent")
  }

  ngOnInit() {
  }

  ngAfterContentInit() {
    if(this.mode == 'field'){
      this.subscriptions.push(this.fieldFrm.valueChanges.subscribe((el) => {
        this.field.isValid = this.fieldFrm.valid
        if (this.parent.allowCanBeSaved) {
          this.parent.canBeSaved.next(true)
          this.parent.container.change.emit();
        }
      }))
    }
    if(this.mode != 'field'){
      this.subscriptions.push(this.psidFrm.valueChanges.subscribe((el) => {
        this.field.isValid = this.psidFrm.valid
        if (this.parent.allowCanBeSaved) {
          this.parent.canBeSaved.next(true)
          this.parent.container.change.emit();
        }
      }))
    }
  }

}

export interface IActionUpdateField {
  id: string,
  property: string,
  value: string,
  isValid?: boolean
}
