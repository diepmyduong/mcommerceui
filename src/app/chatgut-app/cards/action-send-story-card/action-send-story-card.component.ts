import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCard } from 'app/shared/cards/base-card'
import { iCard, iStory } from 'app/services/chatbot'
import { NgForm } from '@angular/forms'
import { IActionUpdateField } from '../update-action-card/update-field/update-field.component';
import * as guid from 'guid'
import { remove } from 'lodash-es'
@Component({
  selector: 'app-action-send-story-card',
  templateUrl: './action-send-story-card.component.html',
  styleUrls: ['./action-send-story-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionSendStoryCardComponent extends BaseCard implements OnInit {

	componentName = "ActionSendStoryCardComponent";
  stories: iStory[] = []
  selectedStory: iStory[] = []
  selectedStoryId: string[] = []
  loadingStories: boolean = false
  fields: IActionUpdateField[] = []
  PSIDs: any[] = []
  constructor(
    public changeRef: ChangeDetectorRef
  ) { 
    super()
  }
  cardName = 'Gửi câu chuyện'

  resetForm(formCtrl: NgForm,card: iCard) {
    // formCtrl.reset(merge(super.resetForm(formCtrl, card),{
    //   text: card.option.text
    // }))
  }

  ngAfterViewInit() {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
  }
  async ngOnInit() {
    if (this.card.option.context) {
      for (const key in this.card.option.context) {
        if (this.card.option.context.hasOwnProperty(key)) {
          this.fields.push({
            id: guid.raw(), 
            property: key, 
            value: this.card.option.context[key]
          })
        }
      }
      console.log( 'thi',this.fields)
    }
    this.card.option.psids.forEach(psid=>{
      this.PSIDs.push({
        id: guid.raw(), 
        value: psid
      })
    })
    this.reload()
  }
  async reload() {
    try {
      this.loadingStories = true
      this.stories = await this.chatbotApi.story.getList({
        local: true,
        query: {
          limit: 0,
          fields: ["_id", "name"],
          order: { 'createdAt': 1 },
          filter: { mode: 'normal' }
        }
      })
      console.log(this.stories, this.card.option.storyId, this.selectedStory)
      this.selectedStory = this.stories.filter(x => this.card.option.storyId == x._id)
      console.log(this.selectedStoryId, this.selectedStory)
      this.selectedStoryId = this.selectedStory.map(x => x._id) 
    } catch (err) {
      this.alert.handleError(err, 'Có lỗi xảy ra khi tải câu chuyện');
    } finally {
      this.loadingStories = false
      this.changeRef.detectChanges()
    }
  }
  async openGetStory(local: boolean = true) {
    const componentRef = await this.portalService.pushPortalAt(this.portalIndex + 1, "StoriesPortalComponent", {
      portalName: "Chuyển sang câu chuyện", select: true, multi: false, selectedStories: this.selectedStoryId
    });

    if (!componentRef) {
      return;
    }
    const component = componentRef.instance as any
    this.subscriptions.onStorySaved = component.onStorySaved.subscribe(async storyId => {
      console.log( 'storyId',storyId)
      try {
        this.card.option.storyId = storyId
        this.reload()
      } catch (err) {
        // this.alert.error('Có lỗi xảy ra khi chọn câu chuyện', err.message)
        this.alert.handleError(err, 'Có lỗi xảy ra khi chọn câu chuyện');
        component.showSaveButton();
      } finally {
        this.canBeSaved.next(true)
        this.container.change.emit();
        component.close() 
      }
    })
    this.subscriptions.onStoryClosed = component.onStoryClosed.subscribe(() => {
      component.onStorySaved.unsubscribe();
      component.onStoryClosed.unsubscribe();
    })
  }
  async addField() {
    this.fields.push({
      id: guid.raw(),
      property: "",
      value: ""
    })
    this.canBeSaved.next(true)
    this.container.change.emit();
  }
  removeField(id: string) {
    remove(this.fields, f => f.id === id)
    this.canBeSaved.next(true)
    this.container.change.emit();
  }
  cardValid() {
    const  context = {}
    console.log( 'field',this.fields)
    for (let f of this.fields) {
      console.log( 'a',f.isValid)
      if (!f.isValid) return `Thẻ thứ ${this.index + 1} (${this.cardName}) không hợp lệ. Xin nhập chính xác thông tin các trường.`
      context[f.property] = f.value
    }
    console.log( 'context',context)
    this.card.option.context = context
    let tmp =[] 
    for (let el of this.PSIDs) {
      console.log( 'b',el.isValid)
      if (!el.isValid) return `Thẻ thứ ${this.index + 1} (${this.cardName}) không hợp lệ. Xin nhập chính xác thông tin các trường.`
      tmp.push(el.value) 
    } 
    this.card.option.psids = tmp
    return super.cardValid()
  }
  addPSID(){
    this.PSIDs.push({
      id: guid.raw(),
      value: ""
    })
    this.canBeSaved.next(true)
    this.container.change.emit();
  }
  removePSID(id: string) {
    remove(this.PSIDs, f => f.id === id)
    this.canBeSaved.next(true)
    this.container.change.emit();
  }
}
