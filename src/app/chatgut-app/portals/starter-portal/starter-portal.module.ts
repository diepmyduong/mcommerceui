import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StarterPortalComponent } from 'app/chatgut-app/portals/starter-portal/starter-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatToolbarModule, MatButtonModule, MatProgressSpinnerModule, MatIconModule, MatCardModule, MatFormFieldModule, MatInputModule, MatAutocompleteModule, MatSelectModule, MatExpansionModule, MatTooltipModule, MatSlideToggleModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CardContainerModule } from 'app/shared/cards/card-container/card-container.module';
import { SelectionModule } from 'app/modals/selection/selection.module';
import { TextFieldModule } from '@angular/cdk/text-field';
import { HightlightTextareaModule } from 'app/directives/hightlight-textarea/hightlight-textarea.module';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    FormsModule,
    TextFieldModule,
    CardContainerModule,
    MatSelectModule,
    MatExpansionModule,
    SelectionModule,
    MatTooltipModule,
    HightlightTextareaModule,
    MatSlideToggleModule,
  ],
  declarations: [StarterPortalComponent],
  entryComponents: [StarterPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: StarterPortalComponent }
  ]
})
export class StarterPortalModule { }
