import { merge, words } from 'lodash-es'
import { Component, OnInit, Input, ViewChild, QueryList, ViewChildren, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { CardService, CardContainerRef } from 'app/services/card.service';
import { NgForm } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { iSetting } from 'app/services/chatbot/api/crud/setting';
import { iStory } from 'app/services/chatbot/api/crud/story';
import { CardContainerComponent } from 'app/shared/cards/card-container/card-container.component';
import { iSelectionData, SelectionDialog } from 'app/modals/selection/selection.component';
import { MatDialog } from '@angular/material';
import { MenuCardComponent } from 'app/chatgut-app/cards/menu-card/menu-card.component';
import { iPage } from 'app/services/chatbot';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { PopoverService } from 'app/shared/popover-form/popover.service';
declare var $: any;
@Component({
  selector: 'app-starter-portal',
  templateUrl: './starter-portal.component.html', 
  styleUrls: ['./starter-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class StarterPortalComponent extends BasePortal implements OnInit {
  @Input() settingId: string

  @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
  @ViewChildren('cardContainer') cardContainers:QueryList<CardContainerComponent> 
 
  constructor(
    public cardService: CardService,
    public selectionDialog: MatDialog,
    public changeRef: ChangeDetectorRef,
    public popoverService: PopoverService
  ) { 
    super()
    this.setPortalName('Bắt đầu')
  }
  componentName = "StarterPortalComponent"
  menuSetting: iSetting
  greetingSetting: iSetting
  cardContainerRef: CardContainerRef[] = []
  filteredOptions = new BehaviorSubject<any[]>([])
  loadDone: boolean = false
  selectedStories: iStory[] = []
  selectedStoryIds:string[] = []
  languageSelect:string = "en_US"
  languageOptions: any[] = [
    {code:"en_US", display:"United States"},
    {code:"en_GB", display:"United Kingdom"},
    {code:"es_ES", display:"Spain"},
    {code:"de_DE", display:"Germany"},
    {code:"ja_JP", display:"Japan"},
    {code:"ko_KR", display:"Korea"},
    {code:"zh_CN", display:"China"},
  ]
  languageOptionsGreeting: any[] = []
  isOpenSelectLanguage: boolean = false
  hasChangeGreeting: boolean = false
  hasChangeIceBreakers: boolean = false
  expandNewLanguage:number = -1
  expandNewLanguageGreeting:number = -1
  selectedMenu: string = "default"
  selectedGreeting: string = "default"
  selectedGreetingIndex: number = 0
  isMenuActive: boolean = true
  toggling: boolean = false
  stories: iStory[]
  iceBreakerSettings: any
  isIceBreakerActive: boolean
  async ngOnInit() {
    this.languageOptionsGreeting = JSON.parse(JSON.stringify(this.languageOptions))
    await this.reload(false)
    console.log( 'greeting',this.greetingSetting)
  }

  ngAfterViewInit() {
  }

  async ngOnDestroy() {
    this.cardContainers.forEach(el=>{
      this.cardService.clearContainer(el);
    })
    super.ngOnDestroy();
  }

  async reload(local: boolean = true) {
    try {
      this.loadDone = false;

      this.menuSetting = undefined
      this.greetingSetting = undefined

    const { app } = this.chatbotApi.chatbotAuth
    const pageId = (typeof(app.activePage)==='object') ? (app.activePage as iPage)._id : app.activePage as string

      this.detectChanges()

    let tasks = []

    tasks.push(this.chatbotApi.setting.getList({
      local, query: {
        filter: {
          type: 'persistent_menu',
          page: pageId
        }
      }}).then(async result => {
        if (result.length == 0) {
          this.menuSetting = await this.chatbotApi.page.addSetting({
            type: "persistent_menu",
            option: [{
              locale: "default",
              composer_input_disabled: false,
              call_to_actions: [{
                "type": "web_url",
                "title": "Web URL",
                "url": "http://google.com"
              }]
            }]
          })
        } else {
          this.menuSetting = result[0]
          console.log('res', this.menuSetting)
        }
        this.isMenuActive = (this.menuSetting.status == 'active')
      }))

    tasks.push(this.chatbotApi.setting.getList({
      local, query: {
        filter: {
          type: 'greeting',
          page: pageId
        }
      }}).then(async result => {
        if (result.length === 0) {
          this.showLoading()
          this.greetingSetting = await this.chatbotApi.page.addSetting({
            type: "greeting",
            option: [{
              locale: "default",
              text: "Xin chào {{user_first_name}} {{user_last_name}}!"
            }]
          })
        } else {
          this.greetingSetting = merge({}, result[0]);
        }
        this.subscribeTextChanged();
      }))

      tasks.push(this.chatbotApi.setting.getList({
        local, query: {
          filter: {
            type: 'ice_breakers',
            page: pageId
          }
        }}).then(async result => {
          this.iceBreakerSettings = result[0]
        }))

      tasks.push(this.chatbotApi.story.getList({
        local,
        query: {
          limit: 0,
          fields: ["_id", "name"],
          order: { 'createdAt': 1 },
          filter: { mode: 'normal', isStarted: true }
        }
      }).then(result => {
        this.selectedStories = result
        this.selectedStoryIds = result.map(s => s._id)
      }))

      tasks.push(this.chatbotApi.story.getAll().then(res => {
        this.stories = res
      }))

      await Promise.all(tasks);
      
      if (!this.iceBreakerSettings) {        
        this.iceBreakerSettings = await this.chatbotApi.setting.add({
          isDefault: false,
          option: [],
          page: this.chatbotApi.page.pageId,
          status: "deactive",
          type: "ice_breakers"
        })
      }

      this.isIceBreakerActive = this.iceBreakerSettings.status == 'active'
      this.iceBreakerSettings.option.forEach(option => {
        try {
          let parsedPayload = JSON.parse(option.payload)
          option.storyId = parsedPayload.data
          let story = this.stories.find(x => x._id == parsedPayload.data)
          if (story) {
            option.storyName = story.name
          }
        } catch (err) {}
      })
      setTimeout(() => {
        // this.cardContainers.forEach((el, i) => {
          this.cardContainerRef.push(this.cardService.regisContainer(this.cardContainers.first));
          this.cardContainerRef[0].pushCardComp("MenuCardComponent", { setting: this.menuSetting, localeMenuIndex: 0 }, false)
        // })
        // this.cardContainers.forEach((el, i) => {
        //   console.log(this.cardContainers, 'el',el,'i',this.cardContainers[i])
        //   this.cardContainerRef.push(this.cardService.regisContainer(el));
        //   this.cardContainerRef[i].pushCardComp("MenuCardComponent", { setting: this.menuSetting, localeMenuIndex: i }, false)
        // })
      }, 0)
    } catch (err){
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi lấy dữ liệu", err.message)
    } finally{
      this.loadDone = true;
      this.detectChanges()
    }
  }
  async menuStatusChanged() {
    this.toggling = true

    try {
      if (this.isMenuActive) {
        await  this.chatbotApi.page.activateSettings(this.menuSetting._id)
        this.alert.success('Đã kích hoạt menu', 'Kích hoạt menu thành công')
      } else {
        await this.chatbotApi.page.deactivateSettings(this.menuSetting._id)
        this.alert.info('Đã tắt menu', 'Tắt menu thành công')
      }
    } catch (err) {
      this.isMenuActive = !this.isMenuActive
      this.alert.handleError(err, 'Lỗi khi thay đổi trạng thái menu');
      // this.alert.error('Lỗi khi thay đổi trạng thái menu', err.message)
    } finally {
      this.toggling = false
      this.detectChanges()
    }
  }
  async cardChanged() {
    this.cardContainerRef[0].container.cardComps[0].dirty = true
    this.showSaveButton();
  }

  async saveContent() {
    try {
      this.cardFrm.form.disable()
      this.showSaving();
      let tasks = []
      let isChange = false
     
      if(this.cardContainerRef[0].container.cardComps[0].dirty){
        isChange = true
      }
      if(this.cardContainerRef[0] && this.cardContainerRef[0].container.cardComps[0]){
        let res:any = (this.cardContainerRef[0].container.cardComps[0] as MenuCardComponent).getOptionSetting()
        console.log( 'res',res)
        if(res){
            let indexPrevLocale = this.menuSetting.option.findIndex(menu => menu.locale == res.locale)
            this.menuSetting.option[indexPrevLocale].call_to_actions = res.call_to_actions
        }
      }
      if (isChange) {
        tasks.push(this.chatbotApi.setting.update(this.menuSetting._id, this.menuSetting, { reload: true }))
      }
      if(this.hasChangeGreeting){
        tasks.push(this.chatbotApi.setting.update(this.greetingSetting._id, this.greetingSetting, { reload: true }))
      }
      if (this.hasChangeIceBreakers) {
        tasks.push(this.chatbotApi.setting.update(this.iceBreakerSettings._id, {
          option: this.iceBreakerSettings.option.map(x => ({ question: x.question, payload: x.payload }))
        }, { reload: true }))
      }
      this.detectChanges()
      await Promise.all(tasks)
      this.showSaveSuccess();
      setTimeout(()=>{
        this.cardContainerRef[0].container.cardComps[0].dirty = false
      })
    } catch (err) {
      console.error(err.error);
      this.alert.handleError(err, "Lỗi khi lưu");
      // this.alert.error('Lỗi khi lưu', err.message)
      if (err.code != 'menu-button-below-threshold') {
        this.showSaveButton();
      } else {
        this.hideSave();
      }
    } finally {
      this.cardFrm.form.enable();
      this.detectChanges()
    }
  }
  
  async openGetStarted(local: boolean = true) {
    const componentRef = await this.portalService.pushPortalAt(this.index + 1, "StoriesPortalComponent", {
      portalName: "Câu chuyện đầu tiên", select: true, selectedStories: this.selectedStoryIds, multi: true
    });

    if (!componentRef) {
      return;
    }
    const component = componentRef.instance as any
    this.subscriptions.onStorySaved = component.onStorySaved.subscribe(async storyIds => {
      try {
        this.detectChanges()
        await this.chatbotApi.page.setGetStartedStories(storyIds);
        component.showSaveSuccess();
        component.close()
      } catch (err) {
        this.alert.handleError(err, "Lỗi khi lưu");
        // this.alert.error('Lỗi khi lưu', err.message)
        component.showSaveButton();
      } finally {
        this.selectedStories = (await this.chatbotApi.story.getList({
          local,
          query: {
            limit: 0,
            fields: ["_id", "name"],
            order: { 'createdAt': 1 },
            filter: { mode: 'normal', _id: { $in: storyIds } }
          }
        }));
        this.selectedStoryIds = storyIds
        this.detectChanges()
      }

    })
    this.subscriptions.onStoryClosed = component.onStoryClosed.subscribe(() => {
      component.onStorySaved.unsubscribe();
      component.onStoryClosed.unsubscribe();
    })
  }

  subscribeTextChanged() {
    this.subscriptions.textChange = this.cardFrm.valueChanges.subscribe((data) => {
      let text = data.text;
      let textArray: string[] = words(text, /[^, ]+/g)
      if (textArray[textArray.length - 1].startsWith('{{')) {
        this.filteredOptions.next([{
          value: `${text.substr(text, text.lastIndexOf("{{"))}{{user_fist_name}}`,
          display: "First Name"
        }, {
          value: `${text.substr(text, text.lastIndexOf("{{"))}{{user_last_name}}`,
          display: "Last Name"
        }, {
          value: `${text.substr(text, text.lastIndexOf("{{"))}{{user_full_name}}`,
          display: "Full Name"
        }])
      } else {
        this.filteredOptions.next([])
      }
      this.detectChanges()
    })
  }

  async textChanged() {
    this.showSaveButton()
    this.hasChangeGreeting = true
    this.selectedGreetingIndex = this.greetingSetting.option.findIndex(greeting => greeting.locale == this.selectedGreeting)
    console.log( 'forms',this.cardFrm)
    this.detectChanges()
  }

  onLanguageOptionSelect(lang){
    this.languageSelect = lang
    this.detectChanges()
  }
  selectAction(){
    this.isOpenSelectLanguage = !this.isOpenSelectLanguage
    this.detectChanges()
  }
  getLanguageDisplay(code){
    if (code == "default") return "Mặc định (Tiếng Việt)"
    let res
    this.languageOptions.forEach(lang =>{
      if(lang.code == code){
        res = lang.display
      }
    })
    return res
  }

  addLanguageButton(){
    let data: iSelectionData = {
      title: 'Chọn quốc gia cho menu',
        confirmButtonText: 'Chọn quốc gia',
        cancelButtonText: 'Quay lại',
        categories: [
          {
            name: 'Quốc gia',
            options: this.languageOptions
          }
        ] as any,
    };
    data.categories.forEach(categories => {
      for(let i = 0; i < categories.options.length; i++) {
        if (this.menuSetting.option.findIndex(x => x.locale == categories.options[i].code) != -1) {
          categories.options[i].disabled = true
        }
      }
    })
    
    let dialogRef = this.selectionDialog.open(SelectionDialog, {
      width: '600px',
      data: data
    });
    dialogRef.afterClosed().toPromise().then(async result => {
      if (result) {
        let option =JSON.parse(JSON.stringify(this.menuSetting.option[0])) 
        option.locale = result
        this.menuSetting.option.push(option)
        setTimeout(()=>{
          // this.cardContainerRef.push(this.cardService.regisContainer(this.cardContainers.last))
          // let length = this.cardContainerRef.length -1
          // this.cardContainerRef[length].pushCardComp("MenuCardComponent", { setting: this.menuSetting, localeMenuIndex: length }, false)
          // this.expandNewLanguage = length-1
          this.selectedMenu = option.locale
          this.changeCardLocate()
          this.showSaveButton();
          setTimeout(()=>{
            this.cardContainerRef[0].container.cardComps[0].dirty = true
          })
          this.detectChanges()
          // this.cardContainerRef[0].container.cardComps[0].
          console.log('menu', this.menuSetting.option)
        },100)
      }
    });
  }
  addLanguageButtonGreeting(){
    let data: iSelectionData = {
      title: 'Chọn quốc gia cho menu',
        confirmButtonText: 'Chọn quốc gia',
        cancelButtonText: 'Quay lại',
        categories: [
          {
            name: 'Quốc gia',
            options: this.languageOptionsGreeting
          }
        ] as any,
    };
    data.categories.forEach(categories => {
      for(let i = 0; i < categories.options.length; i++) {
        if (this.greetingSetting.option.findIndex(x => x.locale == categories.options[i].code) != -1) {
          categories.options[i].disabled = true
        }
      }
    })
    
    let dialogRef = this.selectionDialog.open(SelectionDialog, {
      width: '600px',
      data: data
    });
    dialogRef.afterClosed().toPromise().then(async result => {
      if(result){
        let option = {
          locale: result,
          text: "Hello {{user_first_name}}!"
        }
        this.greetingSetting.option.push(option)
        // this.expandNewLanguageGreeting = this.greetingSetting.option.length-2
        this.selectedGreeting = result
        this.selectedGreetingIndex = this.greetingSetting.option.findIndex(greeting => greeting.locale == this.selectedGreeting)

        this.showSaveButton();
        this.hasChangeGreeting = true
        console.log( 'res',this.greetingSetting)
        this.detectChanges()
      }
    })
  }
  changeGreeting(){
    this.selectedGreetingIndex = this.greetingSetting.option.findIndex(greeting => greeting.locale == this.selectedGreeting)
    //let tmp = this.greetingSetting.option[this.selectedGreetingIndex].text
    //triggle change event, because hlTextArea do not change when select other language
    // let $dom: any =  document.querySelector("#greeting-text")
    // $dom.click()
    // $dom = document.querySelector("#triggle-hlTextArea")
    // $dom.click()

    this.detectChanges()
    $('#greeting-text').highlightWithinTextarea('update');
    this.detectChanges()
  }
  changeCardLocate(mode:string = "normal"){
    let index = this.menuSetting.option.findIndex(menu => menu.locale == this.selectedMenu)
      console.log('ref',this.cardContainerRef)
    // if(this.cardContainerRef.length == 1){
    //   this.cardContainerRef.push(this.cardService.regisContainer(this.cardContainers.last));
    // }else{
      // save data from previous card to menuSetting
      if(mode != "delete"){
        let res:any = (this.cardContainerRef[0].container.cardComps[0] as MenuCardComponent).getOptionSetting()
        console.log( 'res',res)
        if(res){
          let indexPrevLocale = this.menuSetting.option.findIndex(menu => menu.locale == res.locale)
          this.menuSetting.option[indexPrevLocale].call_to_actions = res.call_to_actions
        }
      }
    
    this.cardContainerRef[0].container.cardComps = [] 
    this.cardContainerRef[0].pushCardComp("MenuCardComponent", { setting: this.menuSetting, localeMenuIndex: index }, false)
    this.detectChanges()
  }
  async deleteLanguage(){

    let index = this.menuSetting.option.findIndex(menu => menu.locale == this.selectedMenu)
    this.menuSetting.option.splice(index,1)

    this.selectedMenu = this.menuSetting.option[this.menuSetting.option.length-1].locale
    this.changeCardLocate('delete')
    console.log('ref',this.cardContainerRef)
    this.showSaveButton();
    setTimeout(()=>{
      this.cardContainerRef[0].container.cardComps[0].dirty = true
    })
    this.detectChanges()
  }
  deleteLanguageGreeting(){
    let index = this.greetingSetting.option.findIndex(menu => menu.locale == this.selectedGreeting)
    this.greetingSetting.option.splice(index,1)
    let newIndex = this.greetingSetting.option.length-1
    this.selectedGreeting = this.greetingSetting.option[newIndex].locale
    this.selectedGreetingIndex = newIndex
    this.hasChangeGreeting = true
    this.showSaveButton();
    this.detectChanges()
  }
  openIceBreakerOption(event, index = -1) {
    let option = this.iceBreakerSettings.option[index]    
    let formOptions: iPopoverFormOptions = {
      parent: '.ice-breakers',
      target: '.ice-breakers-option',
      targetElement: event.target,
      width: '92%',
      confirm: option?'Chỉnh sửa câu hỏi':'Tạo câu hỏi',
      horizontalAlign: 'center',
      verticalAlign: 'top',
      fields: [
        {
          placeholder: 'Câu hỏi',
          property: 'question',
          constraints: ['required'],
          current: option ? option.question : ''
        }, {
          placeholder: 'Câu chuyện',
          property: 'storyId',
          type: 'story',
          constraints: ['required'],
          current: option ? option.storyId : null
        }
      ],
      onSubmit: async (data) => {
        console.log( 'data',data)
        if (!data.question) return
        if (option) {
          this.iceBreakerSettings.option[index] = {
            question: data.question,
            storyId: data.storyId,
            payload: JSON.stringify({ type: 'story', data: data.storyId }),
            storyName: this.stories.find(x => x._id == data.storyId).name
          }
        } else {
          this.iceBreakerSettings.option.push({
            question: data.question,
            storyId: data.storyId,
            payload: JSON.stringify({ type: 'story', data: data.storyId }),
            storyName: this.stories.find(x => x._id == data.storyId).name
          })
        }
        this.showSaveButton()
        this.hasChangeIceBreakers = true
        this.detectChanges()
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  deleteIceBreakerOption(index) {
    this.iceBreakerSettings.option.splice(index, 1)
    this.showSaveButton()
    this.hasChangeIceBreakers = true
    this.detectChanges()
  }
  async iceBreakerStatusChanged(state) {
    if (state) {
      await  this.chatbotApi.page.activateSettings(this.iceBreakerSettings._id)
      this.iceBreakerSettings.status = 'active'
      this.alert.success('Đã kích hoạt câu hỏi mẫu', 'Kích hoạt câu hỏi mẫu thành công')
    } else {
      await this.chatbotApi.page.deactivateSettings(this.iceBreakerSettings._id)
      this.iceBreakerSettings.status = 'deactive'
      this.alert.info('Đã tắt câu hỏi mẫu', 'Tắt câu hỏi mẫu thành công')
    }
  }
}
