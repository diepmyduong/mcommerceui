import { Component, OnInit, Input, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { ImageSearchPortalComponent } from 'app/chatgut-app/portals/image-search-portal/image-search-portal.component';
import { ChatbotApiService, iClarifai } from 'app/services/chatbot';
import { MatDialog, MatSlideToggle } from '@angular/material';
import { PortalService } from 'app/services/portal.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-clarifai-info',
  templateUrl: './clarifai-info.component.html',
  styleUrls: ['./clarifai-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClarifaiInfoComponent extends BaseComponent implements OnInit {
  @Input() appId: string
  @Input() portal: ImageSearchPortalComponent
  @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
  constructor(
    public chatbotApi: ChatbotApiService,
    public dialog: MatDialog,
    public portalService: PortalService,
    public changeRef: ChangeDetectorRef
  ) {
    super('Clarifai Component')
  }
  clarifai: iClarifai = undefined
  clarifaiStatus: boolean
  async ngOnInit() {
    await this.reload()
  }

  async reload(local: boolean = true) {
    this.showLoading()
    try {
      this.changeRef.detectChanges()
      const clarifai = await this.chatbotApi.clarifai.getList({})
      this.clarifai = clarifai[0]
      this.clarifaiStatus = (this.clarifai.status === "active")
    } catch (err) {
      console.error('ERROR', err);
      this.portal.alert.error("Lỗi", "Lỗi khi lấy dữ liệu clarifai")
    } finally {
      this.hideLoading();
      this.changeRef.detectChanges()
    }
  }
  async toogleClarifai() {
    try {
      this.clarifai.status = this.clarifai.status == "active" ? "deactive" : "active"
      this.changeRef.detectChanges()
      this.clarifai = await this.chatbotApi.clarifai.update(this.clarifai._id, this.clarifai)
      this.clarifaiStatus = false
      this.portal.alert.success("Thành công", "Cập nhật thành công")
    } catch (err) {
      this.portal.alert.error("Lỗi", "Cập nhật thất bại")
    } finally {
      this.changeRef.detectChanges()
    }
  }

  async onChanged() {
    this.changeRef.detectChanges()
    this.portal.showSaveButton();
  }
}
