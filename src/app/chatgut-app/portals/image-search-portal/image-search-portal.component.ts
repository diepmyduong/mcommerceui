import { Component, OnInit, ViewChild, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { iClarifai, iPage, iApp } from 'app/services/chatbot';
import { iEditInfoDialogData, EditInfoDialog } from 'app/modals/edit-info/edit-info.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-image-search-portal',
  templateUrl: './image-search-portal.component.html',
  styleUrls: ['./image-search-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageSearchPortalComponent extends BasePortal implements OnInit {
  @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
  @Output() onChange = new EventEmitter<any>()
  constructor(
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef
  ) {
    super();
    this.setPortalName("Tìm kiếm ảnh");
  }
  componentName = "ImageSearchPortalComponent"
  clarifai: iClarifai
  app: iApp
  loadDone = false
  get _this() { return this; }
  async ngOnInit() {
    this.app = this.chatbotApi.chatbotAuth
    await this.reload()
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(local: boolean = true) {
    this.showLoading();
    try {
      this.detectChanges()
      const clarifai = await this.chatbotApi.clarifai.getList({})
      console.log("Clarifai: ", clarifai)
      this.clarifai = clarifai[0]
    } catch (err) {
      // console.error('ERROR', err.error.message);
      // this.alert.error("Lỗi khi lấy dữ liệu clarifai",err.message);
      this.alert.handleError(err);
    } finally {
      this.hideLoading();
      this.loadDone = true
      this.detectChanges()
    }
  }
  async saveContent() {
    try {
      this.showSaving();
      this.detectChanges()
      this.clarifai = await this.chatbotApi.clarifai.update(this.clarifai._id, this.clarifai);
      this.showSaveSuccess()
    } catch (err) {
      // this.alert.error('Không thể cập nhật', err.message)
      // this.topicInfo.cardFrm.form.enable()
      this.alert.handleError(err, "Lỗi khi lưu");
      this.showSaveButton()
    } finally {
      this.detectChanges()
    }
  }
  async addClarifai() {
    this.detectChanges()
    let result: any = await this.openNewClarifaiDialog();
    console.log("result: ", result)
    if (result) {
      try {
        const clarifai = await this.chatbotApi.clarifai.add({name: result.name, apiKey: result.apiKey, app: this.app._id } as any)
        this.alert.success('Thành công', 'Kết nối thành công')
        this.reload()
      }
      catch (err) {
        // this.alert.error('Kết nối không thành công', err.message)
        this.alert.handleError(err, 'Kết nối không thành công');
      } finally {
        this.detectChanges()
      }
    }
  }

  openNewClarifaiDialog() {
    let data: iEditInfoDialogData = {
      title: "Kết nối clarifai",
      confirmButtonText: "Kết nối",
      confirmButtonClass: 'i-create-story-confirm-button',
      cancelButtonText: "Quay lại",
      inputs: [
        {
          title: "Tên",
          property: "name",
          type: "text",
          required: true,
          className: 'i-story-title-input'
        },
        {
          title: "Api Key",
          property: "apiKey",
          type: "text",
          required: true,
          className: 'i-story-title-input',
        }
      ]
    };
    return this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    }).afterClosed().toPromise();
  }
}
