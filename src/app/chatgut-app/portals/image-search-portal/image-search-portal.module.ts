import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageSearchPortalComponent } from './image-search-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ImageLibraryComponent } from './image-library/image-library.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatDividerModule } from '@angular/material/divider';
import { ClarifaiInfoComponent } from './clarifai-info/clarifai-info.component';
import { FormsModule } from '@angular/forms';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatFormFieldModule, MatListModule } from '@angular/material';
import { MaterialModule } from 'app/shared';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    EditInfoModule
  ],
  declarations: [ImageSearchPortalComponent, ImageLibraryComponent, ClarifaiInfoComponent],
  entryComponents: [ImageSearchPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ImageSearchPortalComponent }
  ]
})
export class ImageSearchPortalModule { }
