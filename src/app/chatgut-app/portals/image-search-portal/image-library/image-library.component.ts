import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { ImageSearchPortalComponent } from 'app/chatgut-app/portals/image-search-portal/image-search-portal.component';
import { ChatbotApiService, iClarifaiImage } from 'app/services/chatbot';
import { MatDialog } from '@angular/material';
import { PortalService } from 'app/services/portal.service';
import { iEditInfoDialogData, EditInfoDialog } from 'app/modals/edit-info/edit-info.component';

@Component({
  selector: 'app-image-library',
  templateUrl: './image-library.component.html',
  styleUrls: ['./image-library.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageLibraryComponent extends BaseComponent implements OnInit {
  @Input() appId: string
  @Input() portal: ImageSearchPortalComponent
  @Output() onEventClick = new EventEmitter<iClarifaiImage>()
  constructor(
    public chatbotApi: ChatbotApiService,
    public dialog: MatDialog,
    public portalService: PortalService,
    public changeRef: ChangeDetectorRef
  ) {
    super('Image library Component')
  }
  clarifaiImages: iClarifaiImage[] = []

  async ngOnInit() {
    
  }
  async ngAfterViewInit() {
    await this.reload()
  }

  async reload(local: boolean = true) {
    this.showLoading()
    console.log("portal: ", this.portal)
    try {
      this.changeRef.detectChanges()
      this.clarifaiImages = await this.chatbotApi.clarifaiImage.getList({
        local, query: {
          filter: {
            app: this.appId
          }
        }
      })
      console.log("library: ", this.clarifaiImages)
    } catch (error) {
      console.log("err: ", error)
      this.portal.alert.error("Lỗi", "Lỗi khi lấy dữ liệu clarifaiImage")
    } finally {
      this.hideLoading()
      this.changeRef.detectChanges()
    }
  }
  async openClarifaiImage(clarifaiImageId: string){
    this.portalService.pushPortalAt(this.portal.index + 1, "ClarifaiImagePortalComponent", {
      clarifaiImageId: clarifaiImageId
    })
  }
  async openClarifaiImagePortal(clarifaiImage: iClarifaiImage) {
    console.log("event: ", clarifaiImage)
    this.onEventClick.emit(clarifaiImage)
    console.log("index: ", this.portal)
    const componentRef = await this.portalService.pushPortalAt(this.portal.index + 1, "ClarifaiImagePortalComponent", {
      clarifaiImageId: clarifaiImage._id
    })
    if (!componentRef) return;
    let component = componentRef.instance as any
    this.portal.subscriptions.ClarifaiImageDeleted = component.onClarifaiImageDeleted.subscribe(clarifaiImage => {
      console.log("image: ", clarifaiImage)
     
    })
    this.changeRef.detectChanges()

  }
  async addClarifaiImage() {
    let result: any = await this.openNewClarifaiImageDialog();
    console.log("result: ", result)
    if (result) {
      try {
        const clarifaiImage = await this.chatbotApi.clarifaiImage.add({name: result.name, app: this.appId } as any)
        this.portal.alert.success('Thành công', 'Tạo mới thành công')
        this.reload()
      }
      catch (err) {
        this.portal.alert.handleError(err, 'Lỗi khi tạo mới')
      } finally {
        this.changeRef.detectChanges()
      }
    }
  }

  openNewClarifaiImageDialog() {
    let data: iEditInfoDialogData = {
      title: "Tạo ablum ảnh mới",
      confirmButtonText: "Tạo mới",
      confirmButtonClass: 'i-create-story-confirm-button',
      cancelButtonText: "Quay lại",
      inputs: [
        {
          title: "Tên",
          property: "name",
          type: "text",
          required: true,
          className: 'i-story-title-input'
        }
       
      ]
    };
    return this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    }).afterClosed().toPromise();
  }
}