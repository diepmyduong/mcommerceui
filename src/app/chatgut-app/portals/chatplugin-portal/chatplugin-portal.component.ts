import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef, } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { environment } from '@environments';
import { iPage } from 'app/services/chatbot/api/crud/page';
import { SafePipe } from 'app/shared/pipes/safe-pipe/safe.pipe';
import { isObject } from 'lodash-es'
@Component({
  selector: 'app-chatplugin-portal',
  templateUrl: './chatplugin-portal.component.html',
  styleUrls: ['./chatplugin-portal.component.scss'],
  providers: [SafePipe],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatpluginPortalComponent extends BasePortal implements OnInit {

  @Input() ref: string
  constructor(
    public changeRef: ChangeDetectorRef
  ) {
    super()
    this.setPortalName("Chat Plugin")
  }
  componentName = "ChatpluginPortalComponent"
  page: iPage
  pageId: string
  source: string

  async ngOnInit() {
    await this.reload(true)
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(local: boolean = false) {
    const { app } = this.chatbotApi.chatbotAuth
    this.pageId = isObject(app.activePage) ? (app.activePage as iPage)._id : app.activePage as string
    this.page = await this.chatbotApi.page.getItem(this.pageId, { local })
    let src = `https://www.facebook.com/v2.9/plugins/customerchat.php?app_id=${environment.facebook.appId}&channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FlY4eZXm_YWu.js%3Fversion%3D42%23cb%3Df51f2b698c282c%26domain%3D${location.host}%26origin%3Dhttps%253A%252F%252F${location.host}%252Ff1f2ff51ebf7d6%26relation%3Dparent.parent&container_width=0&locale=en_US&page_id=${this.page.pageId}&sdk=joey`
    if (this.ref) src += `&ref=${this.ref}`
    this.source = src
    this.detectChanges()
  }

}
