import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatpluginPortalComponent } from 'app/chatgut-app/portals/chatplugin-portal/chatplugin-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';

// Material
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { SafePipeModule } from 'app/shared/pipes/safe-pipe/safe-pipe.module';
@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatProgressBarModule,
    SafePipeModule
  ],
  declarations: [ChatpluginPortalComponent],
  entryComponents: [ChatpluginPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ChatpluginPortalComponent }
  ]
})
export class ChatpluginPortalModule { }
