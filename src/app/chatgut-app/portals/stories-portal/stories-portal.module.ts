import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoriesPortalComponent } from 'app/chatgut-app/portals/stories-portal/stories-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatToolbarModule, MatTooltipModule, MatButtonModule, MatProgressSpinnerModule, MatIconModule, MatMenuModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { SelectionModule } from 'app/modals/selection/selection.module';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CopyModule } from 'app/modals/copy/copy.module';
import { StoryFilterPipeModule } from 'app/shared/pipes/story-filter-pipe/story-filter-pipe.module';
import { SequenceSubscribersDialogModule } from './sequence-subscribers-dialog/sequence-subscribers-dialog.module';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatTooltipModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatIconModule,
    StoryFilterPipeModule,
    FormsModule,
    SelectionModule,
    ScrollingModule,
    CopyModule,
    SequenceSubscribersDialogModule,
    DragDropModule,
    MatMenuModule
  ],
  declarations: [StoriesPortalComponent],
  entryComponents: [StoriesPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: StoriesPortalComponent }
  ]
})
export class StoriesPortalModule { }
