
import { cloneDeep } from 'lodash-es'
import * as moment from 'moment'
import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { MatDialog } from '@angular/material'
import { BehaviorSubject, Subscription } from 'rxjs';
import { iStory } from 'app/services/chatbot/api/crud/story';
import { DynamicComponentLoaderService } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { iApp } from 'app/services/chatbot';
import { Router } from '@angular/router';
import { iStoryGroup, iSequenceEvent } from 'app/services/chatbot/api/crud/storyGroup';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { StoryDetailPortalComponent } from '../story-detail-portal/story-detail-portal.component';
import { iCopyData, CopyDialog } from 'app/modals/copy/copy.component';
import { DEFAULT_POPULAR_CARDS } from 'app/services/card.service';
import { SequenceSubscribersDialogModule } from './sequence-subscribers-dialog/sequence-subscribers-dialog.module';
import { SequenceSubscribersDialog } from './sequence-subscribers-dialog/sequence-subscribers-dialog.component';

@Component({
  selector: 'app-stories-portal',
  templateUrl: './stories-portal.component.html',
  styleUrls: ['./stories-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StoriesPortalComponent extends BasePortal implements OnInit, OnDestroy {
  @Input() storyId: string 
  @Input() mode: 'normal' | 'broadcast' | 'sequence' = 'normal'
  @Input() select: boolean = false
  @Input() selectedStories: string[] = null
  @Input() multi: boolean = true
  @Input() returnIdOnly: boolean = true
  @Output() onStorySaved = new EventEmitter<any>()
  @Output() onStoryClosed = new EventEmitter<any>()
  @Output() onStoryRefresh = new EventEmitter<any>()

  constructor(
    public dialog: MatDialog,
    private dynamicLoader: DynamicComponentLoaderService,
    public router: Router,
    private popoverService: PopoverService,
    private changeRef: ChangeDetectorRef
  ) {
    super()
  }
  componentName = "StoriesPortalComponent"
  stories: iStory[] = []
  storyGroups: iStoryGroup[] = []
  openedStory: any;
  story_filter: string = '';
  apps:iApp[]
  groupName: string
  itemName: string

  public loadDone: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  loadStoriesDone: boolean = false
  loading: boolean = false
  hasSelectionSet: boolean = true; 

  normal: boolean = false
  broadcast: boolean = false
  sequence: boolean = false

  generalStoryGroup: iStoryGroup = {
    _id: "0",
    name: 'Mặc định',
    type: 'normal',
    stories: [],
    sequenceEvents: [],
    isDefault: true,
    expanded: true,
    page: typeof(this.chatbotApi.chatbotAuth.app.activePage)=='string'?this.chatbotApi.chatbotAuth.app.activePage:(this.chatbotApi.chatbotAuth.app.activePage as any)._id
  }
  defaultSequenceEvent: iSequenceEvent = {
    period: 'minutes',
    value: 1
  }
  sequenceEventPeriods = {
    days: 'ngày',
    minutes: 'phút',
    hours: 'giờ'
  }

  async ngOnInit() {
    switch(this.mode) {
      case 'broadcast':
        this.setPortalName('Broadcast')
        this.broadcast = true
        this.groupName = 'nhóm broadcast'
        this.itemName = 'broadcast'
        break
      case 'sequence':
        this.setPortalName('Luồng câu chuyện')
        this.sequence = true
        this.groupName = 'luồng câu chuyện'
        this.itemName = 'câu chuyện'
        break
      default:
        this.setPortalName('Câu chuyện')
        this.normal = true
        this.groupName = 'nhóm câu chuyện'
        this.itemName = 'câu chuyện'
        break
    }

    this.chatbotApi.app.getList({
      local: true, 
      query: { 
        order: { 
          createdAt: -1 
        },
        populates:["activePage","license"],
      }
    }).then(result => { this.apps = result })

    // this.showLoading()
    await this.reload()
    // this.subscriptions.itemChanged = this.chatbotApi.story.items.pipe(debounceTime(500)).subscribe(async item => {
    //   await this.reload();
    //   this.hideLoading();
    // })
  }

  async ngOnDestroy() {
    console.log(this.index)
    if (!this.select) {
      sessionStorage.setItem(`scroll-${this.chatbotApi.chatbotAuth.app._id}-${this.componentName}-${this.mode}`, 
      document.getElementById(this.portalId).scrollTop.toString())
    }
    super.ngOnDestroy()
    this.onStoryClosed.emit();
  }

  async reload(local: boolean = true) {

    let firstSelectedStory: iStory
    try {
      this.loading = true

      let stories = []
      let tasks = []
      tasks.push(this.chatbotApi.storyGroup.getList({ local, query: this.getGroupStoryQueries() })
      .then(result => {
        this.storyGroups = cloneDeep(result)
      }))
      tasks.push(this.chatbotApi.story.getList({ local, query: this.getStoryQueries() })
      .then(result => {
        this.stories = cloneDeep(result)
      }))
      await Promise.all(tasks)

      let storyGroupExpansion = JSON.parse(localStorage.getItem('storyGroupExpanded-' + this.chatbotApi.chatbotAuth.app._id))

      // sort stories into group
      stories = [...this.stories]
      for (let group of this.storyGroups) {
        for (let i = 0; i < group.stories.length; i++) {
          if (!group.stories[i]) { 
            group.stories.splice(i, 1)
            i--
            return
          }
          let index = stories.findIndex(x => x._id == group.stories[i])
          if (index >= 0) {
            group.stories[i] = stories[index]
            stories.splice(index, 1)
          } else {
            group.stories.splice(i, 1)
            i--
          }
        }
        // check if sequence events are valid if not then correct them by assuming default
        if (this.sequence) {
          if (!group.sequenceEvents) group.sequenceEvents = []
          while (group.sequenceEvents.length < group.stories.length) group.sequenceEvents.push(this.defaultSequenceEvent)
        }
        if (storyGroupExpansion && storyGroupExpansion.hasOwnProperty(group._id)) {
          group.expanded = storyGroupExpansion[group._id]
        } else {
          group.expanded = true
        }
      }

      // check for first group if it isn't default then add one
      if (this.storyGroups[0] && this.storyGroups[0].isDefault) {
        this.storyGroups[0].stories = [...stories]
      } else {
        this.generalStoryGroup.stories = [...stories]
        this.storyGroups.splice(0, 0, this.generalStoryGroup)
      }
      // also assign sequence event if needed
      if (this.sequence) {
        if (!this.storyGroups[0].sequenceEvents) this.storyGroups[0].sequenceEvents = []
        while (this.storyGroups[0].sequenceEvents.length < this.storyGroups[0].stories.length) 
          this.storyGroups[0].sequenceEvents.push(this.defaultSequenceEvent)
      }

      if (this.selectedStories && typeof(this.selectedStories)=='string') {
        this.selectedStories = [this.selectedStories]
      }
      if (this.select && this.selectedStories && this.selectedStories.length) {
        this.storyGroups.forEach(group => {
          for (let story of group.stories as iStory[]) {
            story.select = this.selectedStories.includes(story._id)
            if (!firstSelectedStory && story.select) firstSelectedStory = story
          }
        })
        this.hasSelectionSet = true
      }

      //check next portal
      let nextPortal = this.portalService.container.portals[this.index + 1] as StoryDetailPortalComponent
      if (nextPortal && nextPortal.componentName == 'StoryDetailPortalComponent') {
        this.openedStory = nextPortal.storyId

        for (let group of this.storyGroups) {
          let story = group.stories.find(x => (x as any)._id == this.openedStory)
          if (story) {
            group.active = true;
            (story as any).active = true
            break
          }
        }
        if (this.subscriptions.onStoryClosed) this.subscriptions.onStoryClosed.unsubscribe();
        this.subscriptions.onStoryClosed = nextPortal.onStoryClosed.subscribe(async () => {
          for (let group of this.storyGroups) {
            let story = group.stories.find(x => (x as any)._id == this.openedStory)
            if (story) {
              group.active = false;
              (story as any).active = false
              break
            }
          }
          this.openedStory = "";
          this.subscriptions.onStoryClosed.unsubscribe();
        })
      }

    } catch (err) {
      // console.error(err)
      this.alert.handleError(err);
      // this.alert.error(`Lỗi xảy ra khi tải ${this.itemName}`, err.message)
    } finally {
      this.loadStoriesDone = true
      this.loading = false
      this.loadDone.next(true)
      this.detectChanges()

      console.log(this.stories, this.storyGroups)
      setTimeout(() => { 
        if (this.select) {
            if (firstSelectedStory) this.scrollToStory(firstSelectedStory._id) 
            this.onStoryRefresh.emit(local)
        } else {
          let scroll = sessionStorage.getItem(`scroll-${this.chatbotApi.chatbotAuth.app._id}-${this.componentName}-${this.mode}`)
          if (scroll) document.getElementById(this.portalId).scrollTop = Number(scroll)
        }
      })
    }
  }

  async ngAfterViewInit() {
    this.dynamicLoader.getComponentFactory('StoryDetailPortalComponent')
    DEFAULT_POPULAR_CARDS.forEach(x => this.dynamicLoader.getComponentFactory(x.component))
    this.dynamicLoader.getComponentFactory('ButtonComponent')
  }

  async setSelectedStories(selectedStoryIds: string[]) {
    if (this.hasSelectionSet) return;
    this.hasSelectionSet = true;
    this.selectedStories = selectedStoryIds;

    this.storyGroups.forEach(group => {
      for (let story of group.stories as iStory[]) {
        story.select = selectedStoryIds.includes(story._id)
      }
    })
    this.detectChanges()
  }

  async createStory(event, group: iStoryGroup) {
    let groupIndex = group?this.storyGroups.findIndex(x => x._id == group._id):-1
    let noGroupStoryCount = this.stories.length - this.storyGroups.reduce((count, item) => count + item.stories.length, 0)
    let formOptions: iPopoverFormOptions = {
      parent: group?'.portal-content-block':'.swiper-slide',
      target: group?'.story-item':'.btn-create-story',
      targetElement: event.target,
      width: group?'90%':'96%',
      confirm: 'Tạo ' + this.itemName,
      horizontalAlign: 'center',
      verticalAlign: (groupIndex == 1 && noGroupStoryCount == 0)?'top':'bottom',
      fields: [
        {
          placeholder: 'Tên ' + this.itemName,
          property: 'name',
          constraints: ['required']
        }
      ],
      onSubmit: async (data) => {
        if (!data.name) return
        try {
          this.loading = true
          let story: iStory = {}
          story.type = 'custom';
          story.mode = this.mode;
          story.name = data.name,
          this.detectChanges()
          story = await this.chatbotApi.page.addStory(story)
          this.stories.push({ _id: story._id, name: story.name })
          this.chatbotApi.story.setItems(this.stories, { query: this.getStoryQueries() })
          this.onStoryRefresh.emit(false)
          if (group) {
            group.stories.push(story)
            group.sequenceEvents.push(this.defaultSequenceEvent)
            let sequenceEvents = group.sequenceEvents
            let stories = group.stories.map(x => { return (x as iStory)._id })
            group = this.chatbotApi.storyGroup.update(group._id, { stories: stories, sequenceEvents: this.sequence?sequenceEvents:[] })
            // this.chatbotApi.storyGroup.setItem(group, { query: this.getGroupStoryQueries() })
          } else {
            this.storyGroups[0].stories.push(story)
            this.storyGroups[0].sequenceEvents.push(this.defaultSequenceEvent)
          }
          this.openStory(story)
        } catch (err) {
          // console.error(err)
          // this.alert.error('Lỗi xảy ra khi tạo ' + this.itemName, err.message)
          this.alert.handleError(err);
        } finally {
          this.loading = false
          this.detectChanges()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }
  
  async createGroup(event, target=undefined) {
    let formOptions: iPopoverFormOptions = {
      parent: '.swiper-slide',
      target: target,
      targetElement: event.target,
      width: '96%',
      confirm: 'Tạo ' + this.groupName,
      horizontalAlign: 'center',
      verticalAlign: 'bottom',
      fields: [
        {
          placeholder: 'Tên ' + this.groupName,
          property: 'name',
          constraints: ['required']
        }
      ],
      onSubmit: async (data) => {
        if (!data.name) return
        try {
          this.loading = true
          let group: iStoryGroup = {}
          group.name = data.name
          group.type = this.mode;
          this.detectChanges()
          group = await this.chatbotApi.storyGroup.add(group)
          group.expanded = true
          this.storyGroups.push(group)

          this.scrollToGroup(this.storyGroups.length - 1)
        } catch (err) {
          // console.error(err)
          // this.alert.error('Lỗi xảy ra khi tạo ' + this.groupName, err.message)
          this.alert.handleError(err, "Lỗi khi tạo nhóm");
        } finally {
          this.loading = false
          this.detectChanges()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  getStoryQueries = () => ({
    limit: 0,
    fields: ["_id", "name"],
    order: { 'createdAt': 1 },
    filter: { mode: this.mode }
  })

  getGroupStoryQueries = () => ({
    limit: 0,
    fields: ["_id", "name", "type", "stories", "sequenceEvents"],
    filter: { type: this.mode }, 
    order: { 'createdAt': 1 } 
  })

  scrollToStory(storyId) {
    try {
      let groupIndex = 0
      let storyIndex = 0
      for (let group of this.storyGroups) {
        storyIndex = group.stories.findIndex(x => (x as iStory)._id == storyId)
        if (storyIndex > -1) {
          group.expanded = true
          break
        }
        groupIndex++
      }
      setTimeout(()=> {
        try {
          document.getElementById(this.portalId)
          .getElementsByClassName('story-group-item').item(groupIndex)
          .getElementsByClassName('story-item').item(storyIndex).scrollIntoView({ block:'center', behavior: 'smooth' })
        } catch (err) {

        }
      })
    } catch (err) {
      console.error(err)
    } finally {      
      this.detectChanges()
    }
  }

  scrollToGroup(index) {
    try {
      setTimeout(() => {
        document.getElementsByTagName('app-stories-portal').item(0)
        .getElementsByClassName('story-group-item').item(index).scrollIntoView({ block: 'start', behavior: 'smooth' })
      }, 300)
    } catch (err) {
    } finally {
      this.detectChanges()
    }
  }

  editGroup(event, group: iStoryGroup) {
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      target: '.story-group-header',
      targetElement: event.target,
      width: '96%',
      confirm: 'Đổi tên nhóm',
      horizontalAlign: 'center',
      fields: [
        {
          placeholder: 'Tên ' + this.groupName,
          property: 'name',
          current: group.name,
          constraints: ['required']
        }
      ],
      onSubmit: async (data) => {
        if (group.name == data.name || !data.name) return
        let fallback = group.name
        this.detectChanges()
        try {
          group.name = data.name
          group = await this.chatbotApi.storyGroup.update(group._id, { name: data.name })
        } catch (err) {
          group.name = fallback
          console.error(err)
        } finally {
          this.detectChanges()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  async openStory(story: iStory) {
    if (!this.select && this.index != 0) {
      let storyInfo = { 
        storyId: story._id, 
        portalName: story.name
      }
      this.close()
      setTimeout(() => {
        this.portalService.pushPortal("StoryDetailPortalComponent", storyInfo)
      })
      return;
    }

    // this.scrollToStory(story._id)

    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "StoryDetailPortalComponent", { 
      storyId: story._id, 
      portalName: story.name
    }) 

    if (!componentRef)
      return;

    let isPush
    if (<MouseEvent>event) {
      isPush = (<MouseEvent>event).ctrlKey || (<MouseEvent>event).metaKey;
    } else {
      isPush = false
    }
    if (!isPush) {
      this.openedStory = story._id;
      for (let group of this.storyGroups) {
        let story = group.stories.find(x => (x as any)._id == this.openedStory)
        if (story) {
          group.active = true;
          (story as any).active = true
          break
        }
      }
      let component = componentRef.instance as any;
      component.openStory = this.openStory.bind(this)
      this.subscriptions.onStoryClosed = component.onStoryClosed.subscribe(async () => {
        for (let group of this.storyGroups) {
          let story = group.stories.find(x => (x as any)._id == this.openedStory)
          if (story) {
            group.active = false;
            (story as any).active = false
            break
          }
        }
        this.openedStory = "";
        this.subscriptions.onStoryClosed.unsubscribe();
        this.detectChanges()
      })
    }
    this.detectChanges()
  }

  async removeStory(story: iStory) {
    if (!await this.alert.warning('Xoá ' + this.itemName, `Bạn có muốn xoá ${this.itemName} này không`, 'Xoá ' + this.itemName)) return;
    try {
      this.loading = true
      this.detectChanges()
      await this.chatbotApi.story.delete(story._id, { reload: true })
      if (this.openedStory == story._id) {
        this.portalService.popPortal(this.index + 1);
      }
      
      let storyIndex = 0
      this.stories.splice(this.stories.findIndex(x => x._id == story._id), 1)
      for (let group of this.storyGroups) {
        storyIndex = group.stories.findIndex(x => (x as iStory)._id == story._id)
        if (storyIndex > -1) {
          group.stories.splice(storyIndex, 1)
          group.sequenceEvents.splice(storyIndex, 1)
          if (!group.isDefault) {
            let sequenceEvents = group.sequenceEvents
            let stories = group.stories.map(x => { return (x as iStory)._id })
            this.chatbotApi.storyGroup.update(group._id, { stories: stories, sequenceEvents: this.sequence?sequenceEvents:[] })
          }
          break
        }
      }

    } catch (err) {
      this.alert.handleError(err, "Lỗi khi xóa");
      // this.alert.error('Có lỗi khi xóa ' + this.itemName, err.message)
      this.reload(false)
    } finally {
      this.loading = false
      this.detectChanges()
    }
  }

  async removeGroup(group: iStoryGroup, isDeleteStory = false) {
    if (isDeleteStory) {
      if (!await this.alert.warning(`Xoá ${this.groupName} + tất cả ${this.itemName}`, 
      `Bạn có muốn xoá ${this.groupName} cùng với tất cả ${this.itemName} thuộc nhóm không.`,  `Xoá nhóm + tất cả ${this.itemName}`)) return;
    } else {
      if (!await this.alert.warning('Xoá ' + this.groupName, 
      `Bạn có muốn xoá ${this.groupName} này không? Các ${this.itemName} thuộc nhóm sẽ dời lên nhóm gốc.`, 'Xoá ' + this.groupName)) return;
    }
    try {
      this.storyGroups[0].stories.concat(cloneDeep(group.stories))
      this.storyGroups.splice(this.storyGroups.findIndex(x => x._id == group._id), 1)
      this.detectChanges()
      await this.chatbotApi.storyGroup.delete(group._id, {}, { isDeleteStory })
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi xóa");
      // this.alert.error('Có lỗi khi xóa ' + this.groupName, err.message)
    } finally {
      this.reload(false)
    }
  }

  async selectStory(story: iStory, group: iStoryGroup) {
    if (this.multi) {
      if (story.select == undefined) story.select = true
      else story.select = !story.select
  
      let result = group.stories.filter(x => (x as iStory).select)
      if (result.length == group.stories.length) group.select = true
      else if (result.length == 0) group.select = false
    } else {
      if (story.select) return
      this.storyGroups.forEach(group => {
        for (let story of group.stories as iStory[]) {
          story.select = false
        }
      })
      story.select = true
    }
    this.showSaveButton()
  }

  async selectGroup(group: iStoryGroup) {
    if (!this.multi) return
    if (group.select == undefined) group.select = true
    else group.select = !group.select

    for (let story of group.stories as iStory[]) {
      story.select = group.select
    }
    this.detectChanges()
  }

  async saveContent() {

    this.showSaving()
    if (this.multi) {
      let selectedStories = []
      for (let group of this.storyGroups) {
        for (let story of group.stories as iStory[]) {
          if (story.select) {
            if (this.returnIdOnly) {
              selectedStories.push(story._id)
            } else {
              let { _id, name } = story
              selectedStories.push({ _id, name })
            }
          }
        }
      }
      
      this.onStorySaved.emit(selectedStories)
    } else {
      let selectedStory: any = ''
      for (let group of this.storyGroups) {
        for (let story of group.stories as iStory[]) {
          if (story.select) {
            if (this.returnIdOnly) {
              selectedStory = story._id
            } else {
              let { _id, name } = story
              selectedStory = { _id, name }
            }
            break
          }
        }
        if (selectedStory) break
      }

      this.onStorySaved.emit(selectedStory)
    }
  }

  isExpire(date){
    if(!date) return false
    let expiredMoment = moment(date);
    let expiredMomentUTC = expiredMoment.utc()
    let nowUTC = moment.utc()
    let diffDuration = moment.duration(expiredMomentUTC.diff(nowUTC))
    let minDiff = diffDuration.asMinutes();
    if (minDiff > 0) {
      return false
    }else{
      return true
    }
  }

  expandGroup(group, index) {
    group.expanded = !group.expanded
    this.scrollToGroup(index)
    this.detectChanges()

    let storyGroupExpansion = {}
    this.storyGroups.forEach(group => {
      storyGroupExpansion[group._id] = group.expanded
    })
    localStorage.setItem('storyGroupExpanded-' + this.chatbotApi.chatbotAuth.app._id, JSON.stringify(storyGroupExpansion))
  }
  
  async duplicateStory(story: iStory) {
    let data: iCopyData = {
      mode: 'story',
      name: story.name,
      type: 'Câu chuyện',
      story: story,
      app: this.chatbotApi.chatbotAuth.app
    }
    let dialogRef = this.dialog.open(CopyDialog, {
      width: '650px',
      data
    })
    let dialogClosedSub: Subscription
    dialogClosedSub = dialogRef.afterClosed().subscribe(async result => {
      if (result) {
        let { story, app } = result
        if (app._id == this.chatbotApi.chatbotAuth.app._id) {
          await this.reload(false)
          this.openStory(story)
        } else {
          this.router.navigate((['apps'])).then((e)=>{
            this.router.navigate((['apps', app._id]), { queryParams:{ mode: 'story', item: JSON.stringify(story) } })
          })
        }
      } else {
        if (dialogRef.componentInstance.result) {
          await this.reload(false)
        }
      }
      dialogClosedSub.unsubscribe()
    })
  }
  
  async duplicateStoryGroup(storyGroup: iStoryGroup) {
    let data: iCopyData = {
      mode: 'storyGroup',
      name: storyGroup.name,
      type: 'Nhóm câu chuyện',
      storyGroup: storyGroup,
      app: this.chatbotApi.chatbotAuth.app
    }
    let dialogRef = this.dialog.open(CopyDialog, {
      width: '650px',
      data
    })
    let dialogClosedSub: Subscription
    dialogClosedSub = dialogRef.afterClosed().subscribe(async result => {
      if (result) {
        if (result._id == this.chatbotApi.chatbotAuth.app._id) {
          await this.reload(false)
        } else {
          this.router.navigate((['apps'])).then((e)=>{
            this.router.navigate(['apps', result._id])
          })
        }
      } else {
        if (dialogRef.componentInstance.result) {
          await this.reload(false)
        }
      }
      dialogClosedSub.unsubscribe()
    })
  }
  async editSequenceEvent(event, group: iStoryGroup, index) {
    let sequenceEvent = group.sequenceEvents[index]
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      target: '.story-event',
      targetElement: event.target,
      width: '250px',
      confirm: 'Lưu thời gian',
      fields: [
        {
          type: 'sequence_event',
          placeholder: 'Thời gian',
          property: 'sequence_event',
          current: {...sequenceEvent},
          constraints: ['required']
        }
      ],
      onSubmit: async (data) => {
        if (!data.sequence_event) return
        try {
          group.sequenceEvents[index] = data.sequence_event
          let sequenceEvents = group.sequenceEvents
          let stories = group.stories.map(x => { return (x as iStory)._id })
          this.chatbotApi.storyGroup.update(group._id, { stories: stories, sequenceEvents: sequenceEvents })
        } catch (err) {
          group.sequenceEvents[index] = sequenceEvent
        } finally {
          this.detectChanges()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  async refresh() {
    this.reload(false)
  }

  dropStory(event) {

    try {
      let previousContainer = event.previousContainer.data as iStoryGroup
      let container = event.container.data as iStoryGroup
  
      // same group
      if (event.previousContainer === event.container) {
        if (event.previousIndex == event.currentIndex) return
  
        moveItemInArray(container.sequenceEvents, event.previousIndex, event.currentIndex);
        moveItemInArray(container.stories, event.previousIndex, event.currentIndex);
        if (!container.isDefault) {
          let sequenceEvents = container.sequenceEvents
          let stories = container.stories.map(x => { return (x as iStory)._id })
          this.chatbotApi.storyGroup.update(container._id, { stories: stories, sequenceEvents: this.sequence?sequenceEvents:[] })
        }
  
      } else { // different group
        if (!event.previousContainer || !event.container) return
  
        transferArrayItem(previousContainer.sequenceEvents, container.sequenceEvents, event.previousIndex, event.currentIndex)
        transferArrayItem(previousContainer.stories, container.stories, event.previousIndex, event.currentIndex)
  
        // from normal group
        if (!previousContainer.isDefault) {
          let sequenceEvents = previousContainer.sequenceEvents
          let stories = previousContainer.stories.map(x => { return (x as iStory)._id })
          this.chatbotApi.storyGroup.update(previousContainer._id, { stories: stories, sequenceEvents: this.sequence?sequenceEvents:[] })
        }
  
        // to normal group
        if (!container.isDefault) {
          let sequenceEvents = container.sequenceEvents
          let stories = container.stories.map(x => { return (x as iStory)._id })
          console.log(sequenceEvents, stories)
          this.chatbotApi.storyGroup.update(container._id, { stories: stories, sequenceEvents: this.sequence?sequenceEvents:[] })
        }
  
        if (this.openedStory) {
          for (let group of this.storyGroups) {
            if (group.stories.find(x => (x as iStory)._id == this.openedStory)) {
              group.active = true
            } else {
              group.active = false
            }
          }
        }
      }
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi dời câu chuyện");
      // this.alert.error('Có lỗi xảy ra khi dời câu chuyện', err.message)
      this.reload(false)
    } finally {
      this.detectChanges()
    }
  }

  openSequenSubscriberDialog(storyGroup) {
    this.dialog.open(SequenceSubscribersDialog, {
      width: '450px',
      data: {
        storyGroup
      }
    })
  }

  trackByFn(index, item) {
    return item._id;
  }

  async openDiagramPortal(){
    const componentRef = await this.portalService.pushPortalAt(this.index + 1, "DiagramPortalComponent", { mode: this.mode })
  }
}