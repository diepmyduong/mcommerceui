import { Component, Inject, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from "@angular/material";
import { ChatbotApiService, iSubscriber, iStory } from "app/services/chatbot";
import { AlertService } from "app/services/alert.service";
import { iStoryGroup } from "app/services/chatbot/api/crud/storyGroup";
import { BehaviorSubject } from "rxjs";
import { cloneDeep } from 'lodash-es'

@Component({
  selector: 'app-sequence-subscribers-dialog',
  templateUrl: './sequence-subscribers-dialog.component.html',
  styleUrls: ['./sequence-subscribers-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SequenceSubscribersDialog {

  sequence: iStoryGroup
  subscribers: iSubscriber[]
  subscriberCount: number
  selectedSubscribers$ = new BehaviorSubject<any>({})
  selectedSubscribersCount$ = new BehaviorSubject<number>(0)
  removing$ = new BehaviorSubject<boolean>(false)
  limit$ = new BehaviorSubject<number>(10)

  loadDone$ = new BehaviorSubject<boolean>(false)
  overlay: Element

  constructor(
    public dialogRef: MatDialogRef<SequenceSubscribersDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private chatbotApi: ChatbotApiService,
    private changeRef: ChangeDetectorRef,
    private snackBar: MatSnackBar,
    private alert: AlertService
  ) {
    this.sequence = cloneDeep(data.storyGroup)

    this.overlay = document.getElementsByClassName('cdk-global-overlay-wrapper').item(0)
    this.overlay.classList.add('overflow')
    this.overlay.addEventListener('click', event => this.overlayClickEvent(event) )
  }

  async ngOnInit() {
    this.loadData()
  }

  async loadData() {
    this.loadDone$.next(false)
    let tasks = []
    tasks.push(
      this.chatbotApi.app.statisticStoryGroup([this.sequence._id])
      .then(res => {
        let sequenceStats = res[0]
        this.sequence.subscribersCount = sequenceStats?sequenceStats.count:0
        for (let story of this.sequence.stories as iStory[]) {
          let event = sequenceStats?sequenceStats.events.find(x => x.story == story._id):null
          story.pendingSubscribersCount = event?event.count:0
        }
      })
    )
    tasks.push(
      this.chatbotApi.app.subscribersInStoryGroup(this.sequence._id)
      .then(res => {
        this.subscribers = res.filter(x => x.messengerProfile)
        for (let sub of this.subscribers) {
          sub.name = sub.messengerProfile.name
        }
        this.subscriberCount = this.subscribers.length
      })
    )
    await Promise.all(tasks)
    this.loadDone$.next(true)
  }

  toggleSubscriber(subscriberId) {
    let selectedSubscribers = this.selectedSubscribers$.getValue()
    this.selectedSubscribers$.next({ ...selectedSubscribers, [subscriberId]: !selectedSubscribers[subscriberId] })
    selectedSubscribers = this.selectedSubscribers$.getValue()
    this.selectedSubscribersCount$.next(
      Object.keys(selectedSubscribers)
      .reduce((count, key) => count + (selectedSubscribers[key]?1:0), 0)
    )
  }

  async removeSubscribers() {
    if (!this.selectedSubscribersCount$.getValue()) return
    this.removing$.next(true)
    
    let selectedSubscribers = this.selectedSubscribers$.getValue()
    let subscribers = Object.keys(selectedSubscribers).filter(id => selectedSubscribers[id]).map(x => x)
    try {
      await this.chatbotApi.app.removeSubscribersInStoryGroup(this.sequence._id, subscribers)
      this.snackBar.open('✔️ Đã xoá những người dùng trên khỏi luồng', '', { duration: 2500, });
      this.loadData()
    } catch(err) {
      console.error(err)
      this.snackBar.open('❌ Xoá người dùng khỏi luồng thất bại', '', { duration: 2500, });
    } finally {
      this.removing$.next(false)
    }
  }

  overlayClickEvent(e) {
    if (e.srcElement && e.srcElement.classList.contains('cdk-global-overlay-wrapper')) {
      this.dialogRef.close()
    }
  }

  loadMore() {
    this.limit$.next(this.limit$.getValue() + 10)
  }
}