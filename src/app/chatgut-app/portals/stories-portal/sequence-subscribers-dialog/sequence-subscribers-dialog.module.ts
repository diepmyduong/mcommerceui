import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatProgressSpinnerModule, MatSelectModule, MatSnackBarModule, MatTooltipModule } from '@angular/material';
import { SequenceSubscribersDialog } from './sequence-subscribers-dialog.component';
import { NameFilterPipeModule } from 'app/shared/pipes/name-filter-pipe/name-filter-pipe.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatDialogModule,
    NameFilterPipeModule,
    MatProgressSpinnerModule
  ],
  declarations: [SequenceSubscribersDialog],
  entryComponents: [SequenceSubscribersDialog]
})
export class SequenceSubscribersDialogModule { }
