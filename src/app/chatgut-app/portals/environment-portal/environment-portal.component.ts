
import { debounceTime, toArray } from 'rxjs/operators';
import { Component, OnInit, Output, EventEmitter, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy, ElementRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { NgForm } from '@angular/forms';
import { MatSlideToggle, MatDialog, MatSnackBar } from '@angular/material';
import { JsonEditorOptions, JsonEditorComponent } from 'ang-jsoneditor';
import { iEnvironment } from 'app/services/chatbot/api/crud/environment';
import { EditInfoDialog, iEditInfoDialogData } from 'app/modals/edit-info/edit-info.component';
@Component({
  selector: 'app-environment-portal',
  templateUrl: './environment-portal.component.html',
  styleUrls: ['./environment-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EnvironmentPortalComponent extends BasePortal implements OnInit {
  @Output() onChange = new EventEmitter<any>()
  @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
  @ViewChild(JsonEditorComponent) editor: JsonEditorComponent;
  @ViewChild('scrolling') scrolling : ElementRef;
  constructor(
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef,
    private snackBar: MatSnackBar,
  ) {
    super()
    this.setPortalName("Biến Môi Trường")
  }
  environmentDataArr: any[] = []
  componentName = "EnvironmentPortalComponent"
  environment: iEnvironment
  environment_filter: string = ""
  currentFocus: number = -1
  errors: any = {}

  async ngOnInit() {
    //this.configJsonEditor();
    await this.reload();
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(local: boolean = true) {
    this.showLoading()
    const { app } = this.chatbotApi.chatbotAuth
    this.detectChanges()
    try {
      if (app.environment && typeof app.environment == "string") {
        this.environment = await this.chatbotApi.environment.getItem(app.environment, { local });
      } else {
        this.environment = await this.chatbotApi.environment.getItem((app.environment as iEnvironment)._id, { local });
      }
      if (this.environment && this.environment.data) {
        const environmentKeys = Object.keys(this.environment.data)
        for (const key of environmentKeys) {
          this.environmentDataArr.push({
            key: key,
            value: this.environment.data[key]
          })
        }
      }
      
    } catch (err) {
      // this.log('load environmemt error', err)
      // this.alert.error("Lỗi khi lấy giữ liệu environment", err.message)
      this.alert.handleError(err);
    } finally {
      this.hideLoading();
      this.detectChanges();
      setTimeout(()=>{
        this.initUI()
      })
    }
  }
  initUI(){
    if(this.environmentDataArr.length < 1) return
    console.log( 'init',this.environmentDataArr)
    let valuesDom:any = document.querySelectorAll('.environment-value')
    valuesDom.forEach((el, i)=>{
      if(!el.innerText){
        el.contentEditable  = true
      }
    })
    this.detectChanges()
  }

  async changeContent(property, index, event) {
    console.log(event.target.textContent)
    this.environmentDataArr[index][property] = event.target.textContent;
    if (property == 'key') this.errors[index] = false 
    this.onChanged();
  }

  async saveContent() {
    let environmentData = {}
    let flag = false
    for (let i = 0; i < this.environmentDataArr.length; i++) {
      let variable = this.environmentDataArr[i]
      if(!variable.key) { 
        this.errors[i] = true
        flag = true
      }
      environmentData[variable.key] = variable.value
    }

    if (flag) { 
      return this.alert.error("Không thể lưu","Không được để trống trường")
    }

    this.environment.data = environmentData
    try {
      this.showSaving()
      this.detectChanges()
      this.environment = await this.chatbotApi.environment.update(this.environment._id, this.environment)
      this.showSaveSuccess();
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lưu");
      // this.alert.error("Lỗi khi lưu dữ liệu", err.message)
      this.hideSave();
    } finally {
      this.detectChanges()
    }
  }
  async deleteEnvironmentVariable(index: number) {
      this.environmentDataArr.splice(index, 1)
      this.showSaveButton()
      this.detectChanges()
  }
  copyEnvironmentValue(index:number){
  
    let input = document.createElement('input');
    document.body.appendChild(input)
    input.value = this.environmentDataArr[index].value
    input.select();
    document.execCommand('copy',false);
    input.remove();
    this.snackBar.open('✔️ Đã copy vào clipboard', '', {
      duration: 2000,
    });
    this.changeRef.detectChanges()
  }
  async addEnvironmentVariable() {
    try {
      this.environmentDataArr.push({
        key: "",
        value: ""
      })
      console.log( 'croll',this.scrolling)
      let newIndex = this.environmentDataArr.length -1
      this.currentFocus = newIndex
      setTimeout(()=>{
        this.focusValue(newIndex);
        this.focusKey(newIndex);
      })
      this.showSaveButton()
      this.detectChanges()
    } catch (err) {
      console.log( err)
    }finally{
      setTimeout(()=>{
        this.scrolling.nativeElement.scrollTop = this.scrolling.nativeElement.scrollHeight;
        this.detectChanges()
      },300)
    }
    

  }
  async openAddEnvironmentVariableDialog() {
    let data: iEditInfoDialogData = {
      title: "Thêm biến mới",
      confirmButtonText: "Thêm",
      confirmButtonClass: 'i-create-story-confirm-button',
      cancelButtonText: "Quay lại",
      inputs: [
        {
          title: "Tên biến",
          property: "key",
          type: "text",
          required: true,
          className: 'i-story-title-input'
        },
        {
          title: "Giá trị",
          property: "value",
          type: "text",
          required: true,
          className: 'i-story-title-input'
        }
      ]
    };

    return this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    }).afterClosed().toPromise();
  }

  async onChanged() {
    this.showSaveButton();
  }
  focusKey(index: number){
    // this.environmentDataArr.forEach(env=>{
    //   env.keyFocus = false
    // })
    // this.environmentDataArr[index].keyFocus = true

    let keysDom:any = document.querySelectorAll('.environment-key')
    keysDom.forEach((el, i)=>{
      if(i == index){
        el.contentEditable  = true
        setTimeout(()=>{
          el.focus()
        })
        if(el.classList.contains('truncate')){
          el.classList.remove('truncate')
        }
      }else if(!el.innerText){
        el.contentEditable  = true
      } else{
        el.contentEditable  = false
        if(!el.classList.contains('truncate')){
          el.classList.add('truncate')
        }
      }
    })
    this.detectChanges()
  }
  focusValue(index: number){
    let keysDom:any = document.querySelectorAll('.environment-value')
    keysDom.forEach((el, i)=>{
      if(i == index ){
        el.contentEditable  = true
        setTimeout(()=>{
          el.focus()
        })
        if(el.classList.contains('truncate-value')){
          el.classList.remove('truncate-value')
        }
      }else if(!el.innerText){
        el.contentEditable  = true
      } else{
        el.contentEditable  = false
        if(!el.classList.contains('truncate-value')){
          el.classList.add('truncate-value')
        }
      }
    })
    this.detectChanges()
  }
  focusInput(mode:string, i:number){
    if(this.currentFocus == i) return
    this.currentFocus = i
    if(mode == 'key'){
      this.focusValue(i);
      this.focusKey(i);
    }else{
      this.focusKey(i);
      this.focusValue(i);
    }
  }
}

