import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnvironmentPortalComponent } from 'app/chatgut-app/portals/environment-portal/environment-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';

// Material
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgJsonEditorModule } from 'ang-jsoneditor';
import { EditInfoDialog } from 'app/modals/edit-info/edit-info.component';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { MatTooltipModule } from '@angular/material';
import { MaterialModule } from 'app/shared';
import { FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatIconModule,
    EditInfoModule,
    MaterialModule,
    FormsModule
  ],
  declarations: [EnvironmentPortalComponent],
  entryComponents: [EnvironmentPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: EnvironmentPortalComponent }
  ]

})
export class EnvironmentPortalModule { }
