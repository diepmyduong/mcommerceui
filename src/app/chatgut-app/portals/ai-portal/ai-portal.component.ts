import { Component, OnInit, Input, ViewChild, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { NgForm } from '@angular/forms'
import { MatSlideToggleChange, MatSlideToggle, MatDialog } from '@angular/material'
import { BehaviorSubject } from 'rxjs'
import { iSelectionData, SelectionDialog } from 'app/modals/selection/selection.component';
import { iEditInfoDialogData, EditInfoDialog } from 'app/modals/edit-info/edit-info.component';
import { iPage, iIntent } from 'app/services/chatbot/api/crud/page';
import { iAI } from 'app/services/chatbot/api/crud/ai';
import { isObject } from 'lodash-es'

@Component({
  selector: 'app-ai-portal',
  templateUrl: './ai-portal.component.html',
  styleUrls: ['./ai-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AiPortalComponent extends BasePortal implements OnInit {
  @Input() code: string
  @Input() pageId: string
  @Output() onChange = new EventEmitter<iAI>()
  @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
  constructor(
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef
  ) {
    super()
    this.setPortalName("Trí tuệ nhân tạo")
  }
  public _this = this
  componentName = "AiPortalComponent"
  page: iPage
  ai: iAI
  intents = new BehaviorSubject<iIntent[]>([])
  isIntentsDisabled: boolean = false
  onReload = new EventEmitter<boolean>(true)
  async ngOnInit() {
    await this.reload()
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(local: boolean = true) {
    this.showLoading()
    this.detectChanges()
    try{
      const { app } = this.chatbotApi.chatbotAuth
      let pageId = isObject(app.activePage) ? (app.activePage as iPage)._id : app.activePage as string
      this.page = await this.chatbotApi.page.getItem(pageId, { local })
      if (this.page.ai) {
        const aiId = isObject(this.page.ai) ? (this.page.ai as iAI)._id : this.page.ai as string
        this.ai = await this.chatbotApi.ai.getItem(aiId, { local })
      } else {
        if(!await this.changeAI()) {
          this.close()
          return
        }
      }
      await this.getIntents(local)
    }catch (err){
      this.alert.handleError(err, "Lỗi khi lấy dữ liệu");
      // this.alert.error("Lỗi khi lấy giữ liệu",err.message)
    } finally {
      this.hideLoading()
      this.detectChanges()
    }
  }

  async getIntents(local: boolean = true) {
    this.intents.next(await this.chatbotApi.page.getIntents({ local }))
  }

  async onSetAIState(toggleChange: MatSlideToggleChange) {
    this.showLoading()
    this.detectChanges()
    try {
      if (toggleChange.checked) {
        await this.chatbotApi.page.setAIState('active')
      } else {
        await this.chatbotApi.page.setAIState('deactive')
      }
    } catch (err) {
      // this.log('set AI State error', err)
      // this.alert.error("Lỗi khi đổi trạng thái ai",err.message)
      this.alert.handleError(err, "Lỗi khi đổi trạng thái ai");
    } finally {
      this.hideLoading()
      this.detectChanges()
    }

  }

  async changeAI() {
    let hasChange = false
    this.detectChanges()
    const result = await this.openAIDialog()
    if (result) {
      const [type, aiId] = result.split('.')
      let setupAIInfo
      if (type === 'default') {
        setupAIInfo = {
          type: "default",
          aiId
        }
      } else {
        setupAIInfo = await this.openCreateCustomAIDialog()
        if(!setupAIInfo) return
        setupAIInfo.type = "custom"
        setupAIInfo.version = "20150910"
      }
      if(setupAIInfo) {
        this.showLoading()
        this.detectChanges()
        try {
          this.ai = await this.chatbotApi.page.setAi(setupAIInfo)
          this.onReload.emit(false)
          this.alert.success(`Thành công`, `Đã chuyển sang sử dụng AI ${this.ai.name}`)
          hasChange = true
        } catch (err) {
          this.alert.handleError(err, "Lỗi khi thay đổi AI");
          // this.alert.error("Không thể thay đổi AI",err.message)
          // this.log('setAI error', err)
        } finally {
          this.hideLoading()
          this.detectChanges()
        }
      }
      
    } 
    return hasChange
  }

  async openAIDialog() {
    const ais = await this.chatbotApi.ai.getList({ local: true })
    const defaultAis = []
    const customAis = []
    ais.forEach(ai => {
      if(ai.type === 'default') {
        defaultAis.push({
          code: `default.${ai._id}`,
          display: ai.name,
          icon: 'fab fa-earlybirds'
        })
      } else {
        customAis.push({
          code: `default.${ai._id}`,
          display: ai.name,
          icon: 'fab fa-earlybirds'
        })
      }
    })
    let data: iSelectionData = {
      title: 'Chọn',
      confirmButtonText: 'Chọn AI',
      cancelButtonText: 'Quay lại',
      categories: [{
        name: "Mặc định",
        options: defaultAis as any
      }, {
        name: "Tuỳ chỉnh",
        options: (customAis as any).concat([{
          code: 'custom.create',
          display: 'Kết nối AI của bạn',
          icon: "fas fa-user-secret"
        }])
      }]
    };

    let dialogRef = this.dialog.open(SelectionDialog, {
      width: '600px',
      data: data
    });
    return await dialogRef.afterClosed().toPromise()
  }

  async openCreateCustomAIDialog() {
    let data: iEditInfoDialogData = {
      title: "Thiết lập AI",
      confirmButtonText: "Xác nhận",
      inputs: [{
        title: "Tên",
        property: "name",
        current: "",
        type: "text",
        required: true,
      }, {
        title: "Mô tả",
        property: "description",
        current: "",
        type: "text",
      }, {
        title: "Client Token",
        property: "clientToken",
        current: "",
        type: "text",
        required: true
      }, {
        title: "Develop Token",
        property: "devToken",
        current: "",
        type: "text",
        required: true
      }]
    };
    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    });

    return await dialogRef.afterClosed().toPromise();
  }

  async changeAIToken() {
    let data: iEditInfoDialogData = {
      title: "Thay đổi Token",
      confirmButtonText: "Xác nhận",
      inputs: [{
        title: "Client Token",
        property: "clientToken",
        current: "",
        type: "text",
        required: true
      }, {
        title: "Develop Token",
        property: "devToken",
        current: "",
        type: "text",
        required: true
      }]
    };
    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    });

    const result = await dialogRef.afterClosed().toPromise()
    if(result) {
      this.showLoading()
      this.detectChanges()
      try {
        await this.chatbotApi.ai.changeToken(this.ai._id, result)
        this.onReload.emit(false)
      } catch (err) {
        this.alert.handleError(err, "Lỗi khi thay đổi token");
        // this.alert.error("Không thể thay đổi token",err.message)
      } finally {
        this.hideLoading()
        this.detectChanges()
      }
    }
  }

}
