import { Component, OnInit, Input, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { AiPortalComponent } from 'app/chatgut-app/portals/ai-portal/ai-portal.component';
import { NgForm } from '@angular/forms';
import { MatSlideToggleChange } from '@angular/material';
import { ChatbotApiService, iAI } from 'app/services/chatbot';
import { AlertService } from 'app/services/alert.service';

@Component({
  selector: 'app-ai-info',
  templateUrl: './ai-info.component.html',
  styleUrls: ['./ai-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AiInfoComponent extends BaseComponent implements OnInit {

  @Input() aiId: string
  @Input() portal: AiPortalComponent
  @Input() mode: string = 'detail'
  @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
  constructor(
    public chatbotApi: ChatbotApiService,
    public alert: AlertService
  ) {
    super('Topic Info Component')
  }
  ai: iAI = {}
  token: any = {}

  async ngOnInit() {
  }

  async ngAfterViewInit() {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    if (this.mode === 'detail') {
      await this.reload()
    }
    this.subscriptions.push(this.portal.onReload.subscribe((local) => {
      this.reload(local)
    }))
  }

  async reload(local: boolean = true) {
    this.portal.showLoading()
    try{
      this.ai = await this.chatbotApi.ai.getItem(this.aiId, { local })
    }catch (err){
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi get ai",err.message)
    }
    this.portal.hideLoading()
  }

  async onSave(formCtrl: NgForm, toggleChange: MatSlideToggleChange) {
    if (toggleChange.checked) {
      // Disable Change
      toggleChange.source.setDisabledState(true)
      formCtrl.form.disable()
      this.showLoading()
      try {
        this.ai = await this.chatbotApi.ai.update(this.aiId, this.ai)
        // await this.chatbotApi.page.activeSetting(setting._id)
        formCtrl.form.enable()
        this.resetForm(formCtrl, this.ai)
        this.portal.onChange.emit(this.ai)
      } catch (err) {
        this.alert.handleError(err, "Không thể lưu");
        // this.alert.error("Không thể lưu", err.message)
        toggleChange.source.toggle()
        toggleChange.source.setDisabledState(false)
      } finally {
        this.hideLoading()
        formCtrl.form.enable()
      }

    }
  }

  resetForm(formCtrl: NgForm, ai: iAI) {
    // formCtrl.resetForm({
    //   text: setting.option[0].text
    // })
  }

}

