import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AiPortalComponent } from 'app/chatgut-app/portals/ai-portal/ai-portal.component';
import { AiIntentsComponent } from 'app/chatgut-app/portals/ai-portal/ai-intents/ai-intents.component';
import { AiInfoComponent } from 'app/chatgut-app/portals/ai-portal/ai-info/ai-info.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';

// Material
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SelectionModule } from 'app/modals/selection/selection.module';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { MatTooltipModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatProgressBarModule,
    MatToolbarModule,
    MatSlideToggleModule,
    MatCardModule,
    MatFormFieldModule,
    MatListModule,
    MatButtonModule,
    MatInputModule,
    FormsModule,
    FlexLayoutModule,
    EditInfoModule,
    SelectionModule,
    MatTooltipModule
  ],
  declarations: [AiPortalComponent, AiInfoComponent, AiIntentsComponent],
  entryComponents: [AiPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: AiPortalComponent }
  ]
})
export class AiPortalModule { }
