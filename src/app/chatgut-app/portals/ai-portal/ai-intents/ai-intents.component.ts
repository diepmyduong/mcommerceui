import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { iIntent } from 'app/services/chatbot/api/crud/page';
import { ChatbotApiService, iStory } from 'app/services/chatbot';
import { AlertService } from 'app/services/alert.service';
import { PortalService } from 'app/services/portal.service';
import { AiPortalComponent } from 'app/chatgut-app/portals/ai-portal/ai-portal.component';
import { StoriesPortalComponent } from 'app/chatgut-app/portals/stories-portal/stories-portal.component';

@Component({
  selector: 'app-ai-intents',
  templateUrl: './ai-intents.component.html',
  styleUrls: ['./ai-intents.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AiIntentsComponent extends BaseComponent implements OnInit {

  @Input() portal: AiPortalComponent
  @Input() disable: boolean = false
  @Output() onIntentClick = new EventEmitter<iIntent>()
  constructor(
    public chatbotApi: ChatbotApiService,
    public alert: AlertService,
    public portalService: PortalService
  ) {
    super("AI Intents Component")
  }
  intents: iIntent[] = []

  async ngOnInit() {
    this.reload()
    this.subscriptions.push(this.portal.onReload.subscribe((local) => {
      this.reload(local)
    }))
  }

  async reload(local: boolean = true) {
    this.portal.showLoading()
    try {
      this.intents = await this.chatbotApi.page.getIntents({ local })
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lấy intents")
      // this.alert.error("Lỗi khi get intents",err.message)
    }
    this.portal.hideLoading()
  }

  async openIntent(intent: iIntent) {

    if (this.disable) return
    this.onIntentClick.emit(intent)
    const stories = await this.chatbotApi.story.getList({
      local: false,
      query: {
        limit: 0,
        fields: ["_id", "name"],
        order: { 'createdAt': 1 },
        filter: { mode: 'normal' }
      }
    })
    let intentStories = (stories.filter(s => s.intent == intent.id)).map(s => s._id)
    const componentRef = await this.portalService.pushPortalAt(this.portal.index + 1, 'StoriesPortalComponent', {
      portalName: "Câu chuyện ngữ cảnh", select: true, multi: true, selectedStories: intentStories
    })
    if (!componentRef) return
    const component = componentRef.instance as StoriesPortalComponent

    this.subscriptions.push(component.onStoryClosed.subscribe(() => {
      component.onStorySaved.unsubscribe();
      component.onStoryClosed.unsubscribe();
    }))
    this.subscriptions.push(component.onStorySaved.subscribe(async storyIds => {
      try {
        await this.chatbotApi.story.setIntentStories({ storyIds, intent: intent.id })
        await this.chatbotApi.story.getList({
          local: false,
          query: {
            limit: 0,
            fields: ["_id", "name"],
            order: { 'createdAt': 1 },
            filter: { mode: 'normal' }
          }
        })
      } catch (err) {
        this.alert.handleError(err, 'Có lỗi xảy ra khi lưu dữ liệu');
        // this.alert.error('Có lỗi xảy ra khi lưu dữ liệu', err.message)
        component.showSaveButton();
      } finally {
        let ids = (await this.chatbotApi.story.getList({
          local: false,
          query: {
            filter: { isStarted: true }
          }
        }));
        component.showSaveSuccess();
      }
    }))
  }

  async openStory(story: iStory) {
    this.portal.portalService.pushPortalAt(this.portal.index + 1, "StoryDetailPortalComponent", { storyId: story._id })
  }

}
