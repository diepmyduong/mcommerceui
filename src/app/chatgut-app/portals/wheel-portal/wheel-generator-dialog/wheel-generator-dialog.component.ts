import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject } from "@angular/core";
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from "@angular/material";
import { AlertService } from "app/services/alert.service";
import { ChatbotApiService } from "app/services/chatbot";
import { iLuckyWheel, iLuckyWheelGift } from "app/services/chatbot/api/crud/luckyWheel";
import { BehaviorSubject } from "rxjs";
import * as Winwheel from "winwheel";
import * as WebFont from 'webfontloader'
import { isEqual } from 'lodash-es'

@Component({
  selector: "app-wheel-generator-dialog",
  templateUrl: "./wheel-generator-dialog.component.html",
  styleUrls: ["./wheel-generator-dialog.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WheelGeneratorDialog {
  constructor(
    public dialogRef: MatDialogRef<WheelGeneratorDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private chatbotApi: ChatbotApiService,
    private changeRef: ChangeDetectorRef,
    private snackBar: MatSnackBar,
    private alert: AlertService
  ) {
    this.overlay = document.getElementsByClassName('cdk-global-overlay-wrapper').item(0)
    this.overlay.classList.add('overflow')
    this.overlay.addEventListener('mousedown', event => this.overlayClickEvent(event) )

    this.wheel = data.wheel;
  }
  
  overlay: Element

  winWheel: any;
  wheel: iLuckyWheel;
  gifts: iLuckyWheelGift[];

  designConfigProps = ['palette', 'giftPalettes', 'textOrientation', 'textFontSize', 'imageUrl', 'gifts',
  'textSplitterSize', 'textFontWeight', 'textFontFamily', 'hasPin', 'pinLogo', 'pinColor', 'pinUrl']
  palette = "deepskyblue";
  palettes$ = new BehaviorSubject<string[]>([]);
  schemePalettes$ = new BehaviorSubject<string[]>([])
  luminances = [];

  null = '∅'

  giftPalettes: {
    code: string;
    name: string;
    backgroundColor: string;
    color: string;
    image?: string;
    size?: number;
    offset?: number;
  }[] = null;

  focusedGift: string

  fontCase: string = "normal";
  textAlignment: string = "outer";
  textOrientation: string = "horizontal";
  textFontSize: number = 16;
  textSplitterSize = 16;
  textFontWeight: string = 'bold'
  textFontFamily: string = ''

  hasPin: boolean = false;
  pinLogo: string = '';
  pinColor: string = '';

  imageUrl: string = "";
  pinUrl: string = "";

  imageChanged$ = new BehaviorSubject<any>(null)
  uploadingImages$ = new BehaviorSubject<any>({})
  loadingWheel$ = new BehaviorSubject<boolean>(true);
  hasChanged$ = new BehaviorSubject<boolean>(false);
  uploading$ = new BehaviorSubject<boolean>(false);
  uploadingPin$ = new BehaviorSubject<boolean>(false);

  radius = 215

  webColors = [
    { label: "Đỏ", color: "crimson" },
    { label: "Nâu", color: "maroon" },
    { label: "Gạch", color: "IndianRed" },
    { label: "Đỏ cam", color: "orangered" },
    { label: "Cam", color: "Orange" },
    { label: "Vàng", color: "gold" },
    { label: "Olive", color: "DarkOliveGreen" },
    { label: "Xanh lá", color: "ForestGreen" },
    { label: "Xanh lục", color: "MediumAquamarine" },
    { label: "Xanh lam", color: "Turquoise" },
    { label: "Xanh dương", color: "deepskyblue" },
    { label: "Xanh biển", color: "RoyalBlue" },
    { label: "Chàm", color: "indigo" },
    { label: "Tím", color: "violet" },
    { label: "Hồng", color: "hotpink" },
    { label: "Xám", color: "gray" },
    { label: "Thép", color: "LightSlateGray" },
  ];

  fontFamilies = []
  proportionalFontFamilies = ['Open Sans', 'Kanit', 'Anton', 'Play', 'Nunito', 'Dosis', 'Exo', 'Raleway', 'Bungee', 'Arima Madurai']
  monoFontFamilies = ['Roboto Mono', 'Source Code Pro', 'Space Mono', 'VT323', 'Major Mono Display']
  fontLoaded = 0

  async ngOnInit() {
    this.setupDesignConfig();
    this.setupPalette();
    this.setupGifts(!this.giftPalettes);
  }

  setupDesignConfig() {
    let designConfig = this.wheel.designConfig;
    if (designConfig) {
      this.designConfigProps.forEach(prop => {
        if (designConfig[prop]) this[prop] = designConfig[prop]
      })
    }
  }

  setupFont() {
    if (this.textOrientation == 'horizontal') {
      this.fontFamilies = this.proportionalFontFamilies
    } else {
      this.fontFamilies = this.monoFontFamilies
    }
    if (!this.textFontFamily || !this.fontFamilies.find(x => x == this.textFontFamily)) {
      this.textFontFamily = this.fontFamilies[0]
    }
  }

  setupGifts(rerollPalette = false) {
    let hasGiftsChanged = false

    let gifts = this.wheel.gifts.map(x => ({ _id: x._id, name: x.name, code: x.code }))
    if (!this.gifts) {
      this.gifts = gifts
    } else if (!isEqual(this.gifts, gifts)) {
      this.gifts = gifts
      let designConfig = { ...this.designConfigProps.reduce((obj, item) => ({ ...obj, [item]: this[item] }), {}) }
      this.chatbotApi.luckyWheel
      .update(this.wheel._id, { designConfig })
      .then((res) => { this.wheel.designConfig = designConfig; });
      this.alert.info('Danh sách phần thưởng thay đổi', 'Có sự thay đổi danh sách quà. Thiết lập phần thưởng sẽ thay đổi.')
      hasGiftsChanged = true
    } else if (this.giftPalettes.find(x => !x.code)) {
      hasGiftsChanged = true
    }

    if (hasGiftsChanged) {
      let addedGifts = this.gifts.filter(x => !this.giftPalettes.find(y => y.code == (x.code || this.null)))
      for (let addedGift of addedGifts) {
        if (!this.giftPalettes.find(x => x.code == addedGift.code)) {
          this.giftPalettes.push({
            code: addedGift.code || this.null,
            name: addedGift.name.trim(),
            backgroundColor: this.palette,
            color: 'black',
          });
        }
      }
      let removedGifts = this.giftPalettes.filter(x => !this.gifts.find(y => (y.code || this.null) == x.code)).map(x => x.code)
      this.giftPalettes = this.giftPalettes.filter(x => !removedGifts.includes(x.code))

      let orders = this.gifts.map(x => x.code || this.null).filter((v, i, a) => a.indexOf(v) === i);
      this.giftPalettes = this.giftPalettes.sort((a, b) => orders.findIndex(x => x == a.code) < orders.findIndex(x => x == b.code) ? - 1 : 1)
    }

    let getColorByLuminance = (hsl) =>
      hsl[2] > 65 ? "hsl(" + hsl[0] + "," + (hsl[1] * 80) / 100 + "%," + 30 + "%)" : "white";
    if (rerollPalette || this.giftPalettes.find(x => !x.code)) {
      if (!this.giftPalettes) {
        this.giftPalettes = [];
      } else {
        this.giftPalettes.forEach(giftPalette => { giftPalette.backgroundColor = '' })
      }
      let palettes = [...this.palettes$.getValue()];
      for (let gift of this.gifts) {
        let index = Math.floor(Math.random() * palettes.length);
        let hsl = this.HexToHSL(palettes[index]);
        let giftPalette = this.giftPalettes.find((x) => x.code == (gift.code || this.null))
        if (!giftPalette) {
          this.giftPalettes.push({
            code: gift.code || this.null,
            name: gift.name.trim(),
            backgroundColor: palettes[index],
            color: getColorByLuminance(hsl),
          });
          palettes.splice(index, 1);
        }
        else if (!giftPalette.backgroundColor) {
          giftPalette.backgroundColor = palettes[index]
          giftPalette.color = getColorByLuminance(hsl)
          palettes.splice(index, 1);
        }
      }
    } else {
      for (let giftPalette of this.giftPalettes) {
        let hsl = this.HexToHSL(giftPalette.backgroundColor);
        giftPalette.color = getColorByLuminance(hsl);
      }
    }

    this.setupWheel();
  }

  setupWheel() {
    
    let canvas = document.getElementById("canvas") as HTMLCanvasElement;
    if (this.winWheel) this.winWheel.clearCanvas();
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);

    this.loadingWheel$.next(true)
    this.setupFont()    
    //load font before drawing wheel
    WebFont.load({
      google: {
        families: [`${this.textFontFamily}:400,600,700,800:vietnamese`]
      },
      fontactive: (family, fvd) => {
        if (family == this.textFontFamily && this.loadingWheel$.getValue()) {          
          this.drawWheel(canvas)
          this.drawWheelDecoration(canvas)
          this.drawPin()
          this.hasChanged$.next(true);
          this.loadingWheel$.next(false)
        }
      }
    }) 
  }

  setupPalette() {
    let hsl = this.getHSL(this.palette);

    this.luminances = [];
    let start = 95, end = 20
    for (let i = start; i >= end; i -= (start - end) / 10) {
      this.luminances.push(i);
    }

    let palettes = [];
    for (let i = 0; i < this.luminances.length; i++) {
      palettes.push(this.HSLtoHex(hsl[0], hsl[1], this.luminances[i]));
      // palettes.push("hsl(" + hsl[0] + "," + hsl[1] + "%," + this.luminances[i] + "%)")
    }

    this.palettes$.next(palettes);

    let schemePalettes = []
    let saturation = 85, luminance = 55, step = 45
    for (let i = step; i < 360; i += step) {
      schemePalettes.push(this.HSLtoHex(hsl[0] + i, saturation, luminance))
    }
    this.schemePalettes$.next(schemePalettes)
  }

  createImage() {
    let dataURL;
    let canvas = document.getElementById("canvas") as HTMLCanvasElement;
    try {
      dataURL = canvas.toDataURL("image/png", 0.9).split(",")[1];
    } catch (e) {
      dataURL = canvas.toDataURL().split(",")[1];
    }
    this.uploading$.next(true);
    this.chatbotApi.upload
      .uploadImageData(dataURL)
      .then((res) => {
        this.imageUrl = res.data.link;
        this.snackBar.open("✔️ Tạo ảnh vòng quay thành công. Xin bấm lưu ở giao diện vòng quay.", "", { duration: 2000 });
        this.imageChanged$.next({
          type: 'image',
          url: this.imageUrl
        })
        this.uploading$.next(false);

        let designConfig = this.designConfigProps.reduce((obj, item) => ({ ...obj, [item]: this[item] }), {})
        this.chatbotApi.luckyWheel
          .update(this.wheel._id, {
            designConfig,
          })
          .then((res) => {
            this.wheel.designConfig = designConfig;
          });
      })
      .catch((err) => {
        this.snackBar.open("❌ Lỗi khi upload ảnh", "", { duration: 2000 });
        this.uploading$.next(false);
      });
  }

  

  createPinImage() {
    let dataURL;
    let canvas = document.getElementById("pin-canvas") as HTMLCanvasElement;
    try {
      dataURL = canvas.toDataURL("image/png", 0.9).split(",")[1];
    } catch (e) {
      dataURL = canvas.toDataURL().split(",")[1];
    }
    this.uploadingPin$.next(true);
    this.chatbotApi.upload
      .uploadImageData(dataURL)
      .then((res) => {
        this.pinUrl = res.data.link;
        this.snackBar.open("✔️ Tạo ảnh pin thành công. Xin bấm lưu ở giao diện vòng quay.", "", { duration: 2000 });
        this.imageChanged$.next({
          type: 'pin',
          url: this.pinUrl
        })
        this.uploadingPin$.next(false);

        let designConfig = this.designConfigProps.reduce((obj, item) => ({ ...obj, [item]: this[item] }), {})
        this.chatbotApi.luckyWheel
          .update(this.wheel._id, {
            designConfig,
          })
          .then((res) => {
            this.wheel.designConfig = designConfig;
          });
        })
      .catch((err) => {
        this.snackBar.open("❌ Lỗi khi upload ảnh", "", { duration: 2000 });
        this.uploading$.next(false);
      });
  }

  copyImageUrl(url) {
    if (!url) return
    let input = document.createElement("input");
    document.body.appendChild(input);
    input.value = url;
    input.select();
    document.execCommand("copy", false);
    input.remove();
    this.snackBar.open("✔️ Đã copy đường dẫn ảnh", "", { duration: 2000 });
  }

  copyPalette(palette: string) {
    if (this.focusedGift) {
      if (this.focusedGift == 'pin') {
         this.pinColor = palette
         this.setupWheel()
      } else {
        this.giftPalettes.find(x => x.code == this.focusedGift).backgroundColor = palette
        this.setupGifts()
      }
    } else {
      let input = document.createElement("input");
      document.body.appendChild(input);
      input.value = palette;
      input.select();
      document.execCommand("copy", false);
      input.remove();
      this.snackBar.open("✔️ Đã copy mã màu", "", { duration: 2000 });
    }
  }

  getHSL(color) {
    if (color.startsWith("#")) {
      return this.HexToHSL(color);
    } else {
      let d = document.createElement("div");
      d.style.color = color;
      document.body.appendChild(d);
      let rgb: any = window.getComputedStyle(d).color;
      d.remove();
      rgb = rgb.replace("rgb(", "");
      rgb = rgb.replace(")", "");
      rgb = rgb.split(", ");
      return this.RGBToHSL(Number(rgb[0]), Number(rgb[1]), Number(rgb[2]));
    }
  }

  HexToHSL(H) {
    // Convert hex to RGB first
    let r: any = 0,
      g: any = 0,
      b: any = 0;
    if (H.length == 4) {
      r = "0x" + H[1] + H[1];
      g = "0x" + H[2] + H[2];
      b = "0x" + H[3] + H[3];
    } else if (H.length == 7) {
      r = "0x" + H[1] + H[2];
      g = "0x" + H[3] + H[4];
      b = "0x" + H[5] + H[6];
    }
    // Then to HSL
    r /= 255;
    g /= 255;
    b /= 255;
    let cmin = Math.min(r, g, b),
      cmax = Math.max(r, g, b),
      delta = cmax - cmin,
      h = 0,
      s = 0,
      l = 0;

    if (delta == 0) h = 0;
    else if (cmax == r) h = ((g - b) / delta) % 6;
    else if (cmax == g) h = (b - r) / delta + 2;
    else h = (r - g) / delta + 4;

    h = Math.round(h * 60);

    if (h < 0) h += 360;

    l = (cmax + cmin) / 2;
    s = delta == 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));
    s = +(s * 100).toFixed(1);
    l = +(l * 100).toFixed(1);

    return [h, s, l];
    // return "hsl(" + h + "," + s + "%," + l + "%)";
  }

  RGBToHSL(r, g, b) {
    (r /= 255), (g /= 255), (b /= 255);
    var max = Math.max(r, g, b),
      min = Math.min(r, g, b);
    var h,
      s,
      l = (max + min) / 2;

    if (max == min) {
      h = s = 0; // achromatic
    } else {
      var d = max - min;
      s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
      switch (max) {
        case r:
          h = (g - b) / d + (g < b ? 6 : 0);
          break;
        case g:
          h = (b - r) / d + 2;
          break;
        case b:
          h = (r - g) / d + 4;
          break;
      }
      h /= 6;
    }

    return [h * 360, s * 100, l * 100];
    // return "hsl(" + h*360 + "," + s*100 + "%," + l*100 + "%)";
  }

  HSLtoHex(h, s, l) {
    h /= 360;
    s /= 100;
    l /= 100;
    let r, g, b;
    if (s === 0) {
      r = g = b = l; // achromatic
    } else {
      const hue2rgb = (p, q, t) => {
        if (t < 0) t += 1;
        if (t > 1) t -= 1;
        if (t < 1 / 6) return p + (q - p) * 6 * t;
        if (t < 1 / 2) return q;
        if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
        return p;
      };
      const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
      const p = 2 * l - q;
      r = hue2rgb(p, q, h + 1 / 3);
      g = hue2rgb(p, q, h);
      b = hue2rgb(p, q, h - 1 / 3);
    }
    const toHex = (x) => {
      const hex = Math.round(x * 255).toString(16);
      return hex.length === 1 ? "0" + hex : hex;
    };
    return `#${toHex(r)}${toHex(g)}${toHex(b)}`;
  }

  splitter(str: string, l: number) {
    var strs = [];
    while (str.length > l) {
      var pos = str.substring(0, l).lastIndexOf(" ");
      pos = pos <= 0 ? l : pos;
      strs.push(str.substring(0, pos));
      var i = str.indexOf(" ", pos) + 1;
      if (i < pos || i > pos + l) i = pos;
      str = str.substring(i);
    }
    strs.push(str);
    return strs;
  }

  drawWheel(canvas: HTMLCanvasElement) {
    let center = canvas.width / 2

    let angle = 360 / this.gifts.length
    this.winWheel = new Winwheel({
      numSegments: this.gifts.length, 
      outerRadius: this.radius,
      centerX: center,
      centerY: center,
      textMargin: 24,
      textAlignment: this.textAlignment,
      textOrientation: this.textOrientation,
      textFontSize: this.textFontSize,
      lineWidth: 0.1,
      segments: this.gifts
        .map((x, i) => {
          let giftPalette = this.giftPalettes.find((y) => y.code == (x.code || this.null));
          let currentAngle = - angle * i - 90 - angle / 2
          return {
            fillStyle: giftPalette.backgroundColor,
            text: this.getText(giftPalette.name),
            textFillStyle: giftPalette.color,
            strokeStyle: giftPalette.color == 'white' ? "#111" : giftPalette.color,
            textFontFamily: this.textFontFamily,
            textFontWeight: this.textFontWeight,
            textDirection: this.textOrientation=='horizontal' && currentAngle < -90 && currentAngle > -270 ? 'reversed' : 'normal', 
          };
        })
        .reverse(),
    });

  }

  getText(text: string) {
    switch (this.fontCase) {
      case 'titlecase': {
        let words = text.toLowerCase().split(" ");
        for (let i = 0; i < words.length; i++){
          words[i] = words[i][0].toUpperCase() + words[i].slice(1);
        }
        text = words.join(" ");
        break
      }
      case 'uppercase': {
        text = text.toUpperCase()
        break
      }
      case 'lowercase': {
        text = text.toLowerCase()
        break
      }
    }

    if (this.textSplitterSize > 0) {
      text = this.splitter(text, this.textSplitterSize).join("\n")
    }
    
    return text
  }

  drawWheelDecoration(canvas: HTMLCanvasElement) {
    let ctx = canvas.getContext('2d')
    let center = canvas.width / 2
    let palettes = this.palettes$.getValue()

    ctx.beginPath()
    ctx.arc(center,center,this.radius + 2,0,Math.PI*2, false); // outer (filled)
    ctx.arc(center,center,this.radius,0,Math.PI*2, true); // outer (unfills it)
    ctx.shadowColor = palettes[9];
    ctx.shadowBlur = 12;
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 0;
    ctx.fillStyle = 'white'
    ctx.fill();

    ctx.beginPath()
    ctx.arc(center,center,this.radius + 24,0,Math.PI*2, false); // outer (filled)
    ctx.arc(center,center,this.radius,0,Math.PI*2, true); // outer (unfills it)
    
    let grd = ctx.createLinearGradient(0, 0, canvas.width, 0);
    grd.addColorStop(0, palettes[4]);
    grd.addColorStop(0.5, palettes[5]);
    grd.addColorStop(1, palettes[6]);
    ctx.fillStyle = grd;
    ctx.shadowBlur = 4;
    ctx.fill();

    let slices = this.gifts.length
    let angle = 2 * Math.PI / slices
    for (let i = 0; i < slices; i++) {
      let pointX = center + (this.radius + 12) * Math.cos(angle * i - Math.PI / 2)
      let pointY = center + (this.radius + 12) * Math.sin(angle * i - Math.PI / 2)

      ctx.beginPath()
      ctx.arc(pointX,pointY,6,0,Math.PI*2, false);
      ctx.fillStyle = palettes[1];
      ctx.shadowColor = palettes[9];
      ctx.shadowBlur = 3;

      ctx.fill();
    }

    ctx.shadowBlur = 0;

    ctx.beginPath()
    ctx.arc(center,center,this.radius,0,Math.PI*2, false);
    ctx.arc(center,center,this.radius - 4,0,Math.PI*2, true);
    ctx.fillStyle = 'white'
    ctx.fill();

    
    for (let i = 0; i < this.gifts.length; i++) {
      let gift = this.gifts[i]
      let giftPalette = this.giftPalettes.find(x => x.code == (gift.code || this.null))
      if (giftPalette && giftPalette.image) {
        if (!giftPalette.size) giftPalette.size = 80
        if (!giftPalette.offset) giftPalette.offset = 100

        let relativeAngle = - angle * i - (Math.PI / 2) - angle / 2
        let pointX = center + giftPalette.offset * Math.cos(relativeAngle)
        let pointY = center + giftPalette.offset * Math.sin(relativeAngle)
        let image = new Image()
        image.setAttribute('crossOrigin', 'anonymous'); //
        image.onload = () => {
          ctx.save();                
          ctx.translate(pointX,pointY);
          ctx.rotate(relativeAngle + Math.PI / 2);
          ctx.drawImage(image, -giftPalette.size / 2, -giftPalette.size / 2, giftPalette.size, giftPalette.size)
          ctx.restore();      
        }
        image.src = giftPalette.image
      }
    }
  }

  overlayClickEvent(e) {
    if (e.srcElement && e.srcElement.classList.contains('cdk-global-overlay-wrapper')) this.dialogRef.close()
  }

  drawPin() {
    if (this.hasPin) {
      if (!this.pinColor) {
        this.pinColor = this.schemePalettes$.getValue()[3]
      }

      let canvas = document.getElementById('pin-canvas') as HTMLCanvasElement
      let ctx = canvas.getContext('2d')
      ctx.clearRect(0, 0, canvas.width, canvas.height);

      let center = canvas.width / 2;
      
      ctx.shadowOffsetX = 0;
      ctx.shadowOffsetY = 0;
      
      ctx.beginPath();
      ctx.moveTo(center - 36, center);
      ctx.lineTo(center, center - 64);
      ctx.lineTo(center + 36, center);
      ctx.closePath();
      ctx.fillStyle = this.pinColor;
      ctx.shadowColor = 'rgba(0, 0, 0, 0.24)';
      ctx.shadowBlur = 6;
      ctx.fill();

      ctx.beginPath()
      ctx.arc(center,center,36,0,Math.PI*2, false);
      ctx.fillStyle = this.pinColor;
      ctx.shadowColor = 'rgba(0, 0, 0, 0.24)';
      ctx.shadowBlur = 3;
      ctx.fill();

      ctx.beginPath()
      ctx.arc(center,center, 28,0,Math.PI*2, false);
      ctx.fillStyle = 'white';
      ctx.shadowColor = '#333';
      ctx.shadowBlur = 1;
      ctx.fill();
      
      ctx.shadowBlur = 0;
      ctx.shadowColor = 'none';

      if (this.pinLogo) {
        let size = 32
        let image = new Image()
        image.setAttribute('crossOrigin', 'anonymous');
        image.style.objectFit = 'contain'
        image.width = size
        image.height = size
        image.onload = () => {
          ctx.drawImage(image, center - size / 2, center - size / 2, size, size)
        }
        image.src = this.pinLogo
      }
    }
  }

  onImageFileChanged(event, giftPalette = null) {
    let files = event.target.files
    if (files.length) {
      let file = files[0]

      let code = giftPalette?giftPalette.code:'pin'
      this.uploadingImages$.next({...this.uploadingImages$.getValue(), [code]: true })
      this.chatbotApi.upload.uploadImage(file).then((res: any) => {
        if (giftPalette) giftPalette.image = res.link       
        else this.pinLogo = res.link 
        this.uploadingImages$.next({...this.uploadingImages$.getValue(), [code]: false })
        this.setupGifts()
      })
    }
  }
}
