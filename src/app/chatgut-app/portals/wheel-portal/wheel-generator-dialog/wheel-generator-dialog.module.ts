import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatTooltipModule } from '@angular/material';
import { WheelGeneratorDialog } from './wheel-generator-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatTooltipModule,
    MatDialogModule,
  ],
  declarations: [WheelGeneratorDialog],
  entryComponents: [WheelGeneratorDialog]
})
export class WheelGeneratorDialogModule { }
