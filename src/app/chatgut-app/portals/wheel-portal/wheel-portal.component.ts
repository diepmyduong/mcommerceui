
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { iPage } from 'app/services/chatbot/api/crud/page';
import { iLuckyWheel, iLuckyWheelGift } from 'app/services/chatbot/api/crud/luckyWheel';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { moveItemInArray } from '@angular/cdk/drag-drop';
import { iLuckyWheelReceive } from 'app/services/chatbot/api/crud/luckyWheelReceive';
import { iCopyData, CopyDialog } from 'app/modals/copy/copy.component';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { saveAs } from 'file-saver';
import { WheelGeneratorDialog } from './wheel-generator-dialog/wheel-generator-dialog.component';
@Component({
  selector: 'app-wheel-portal',
  templateUrl: './wheel-portal.component.html',
  styleUrls: ['./wheel-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WheelPortalComponent extends BasePortal implements OnInit {
  page: iPage
  pageId: any
  componentName = "WheelPortalComponent"
  portalName = "Vòng quay"
  isUploading = {}
  
  @Input() wheelId: string
  @Output() onClosed = new EventEmitter<any>()
  wheels: iLuckyWheel[]
  selectedWheel: iLuckyWheel
  wheelResults: iLuckyWheelReceive[]
  loadDone: boolean = false
  loading: boolean = false
  loadingResult: boolean = false
  currentPage: number = 1
  totalWheelResult: number
  loadingMore: boolean = false
  limitWheelResult: number = 50
  filter: string = ''

  configs = [
    { property: 'wheelImage', display: 'Ảnh vòng quay', type: 'image', ratio: 'square' },
    { property: 'pinImage', display: 'Ảnh pin', type: 'image', ratio: 'square' },
    { property: 'bannerImage', display: 'Ảnh banner', type: 'image', ratio: 'horizontal' },
    { property: 'footerImage', display: 'Ảnh footer', type: 'image', ratio: 'horizontal' },
    { property: 'backgroundImage', display: 'Ảnh nền', type: 'image', ratio: 'horizontal' },
    { property: 'backgroundColor', display: 'Màu nền', type: 'color' },
    { property: 'buttonColor', display: 'Màu nút', type: 'color' },
    { property: 'btnTitle', display: 'Tựa đề nút vòng quay', type: 'input' },
    { property: 'title', display: 'Tựa đề webview', type: 'input' },
    { property: 'turn', display: 'Số lượt quay', type: 'number' }
  ]

  tabIndex = 0

  constructor(
    public changeRef: ChangeDetectorRef,
    private popoverService: PopoverService,
    private dialog: MatDialog,
    private router: Router
  ) {
    super()
  }

  async ngOnInit() {
    await this.reload()
    this.changeTab(0)
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
    this.onClosed.emit(true)
  }

  async reload(local = true) {
    try {
      this.loading = true
      this.detectChanges()
      let tasks = []
      
      tasks.push(this.chatbotApi.luckyWheel.getList({
        local, query: { limit: 0, order: { 'createdAt': 1 } }
      }).then(result => {
        this.wheels = result
      }))

      await Promise.all(tasks)
      console.log(this.wheels)
    } catch (err) {
      // console.error(err)
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi lấy dữ liệu", err.message)
    } finally {
      this.loadDone = true
      this.loading = false
      this.detectChanges()
      if (this.wheelId) {
        let wheel = this.wheels.find(x => x._id == this.wheelId)
        this.wheelId = ''
        if (wheel) this.openWheel(wheel)
      }
      // this.openWheel(this.wheels[0])
      // this.openWheelGenerator()
    }
  }
  
  async addWheel(event, center = false) {
    let formOptions: iPopoverFormOptions = {
      parent: '.swiper-slide',
      target: center?'.btn-create':'.btn-blue',
      targetElement: event.target,
      width: '96%',
      confirm: 'Tạo vòng quay',
      horizontalAlign: 'center',
      verticalAlign: 'bottom',
      fields: [
        {
          placeholder: 'Tên',
          property: 'name',
          constraints: ['required']
        }
      ],
      onSubmit: async (data) => {
        if (!data.name) return
        try {
          this.loading = true
          this.detectChanges()
          let wheel = await this.chatbotApi.luckyWheel.add({
            name: data.name,
            config: {
                backgroundColor: '#f5fafe',
                backgroundImage: '',
                buttonColor: '#64b5f6',
                bannerImage: 'https://i.imgur.com/akqC2EE.png',
                wheelImage: 'https://i.imgur.com/eoKgUEK.png',
                pinImage: 'https://i.imgur.com/CGH00Vd.png',
                btnTitle: 'Bắt đầu quay',
                title: 'Vòng quay may mắn',
                turn: 1
            },
            successRatio: 50,
            failRatio: 0,
            gifts: []
          })
          this.openWheel(wheel)
        } catch (err) {
          console.error(err)
          this.alert.handleError(err, "Lỗi khi tạo vòng xoay");
          // this.alert.error('Không tạo được vòng xoay', err.message)
        } finally {
          this.loading = false
          this.detectChanges()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  editName(event, wheel: iLuckyWheel) {
    let formOptions: iPopoverFormOptions = {
      parent: '.swiper-slide',
      target: '.portal-list-item',
      targetElement: event.target,
      width: '96%',
      confirm: 'Đổi tên vòng quay',
      horizontalAlign: 'center',
      verticalAlign: 'top',
      fields: [
        {
          placeholder: 'Tên vòng quay',
          property: 'name',
          constraints: ['required'],
          current: wheel.name
        }
      ],
      onSubmit: async (data) => {
        if (!data.name) return
        try {
          this.loading = true
          this.detectChanges()
          await this.chatbotApi.luckyWheel.update(wheel._id, {
            name: data.name
          })
          if (this.selectedWheel._id == wheel._id) {
            this.selectedWheel.name = data.name
          }
        } catch (err) {
          console.error(err)
          this.alert.handleError(err, "Lỗi khi đổi tên");
          // this.alert.error('Đổi tên thất bại', err.message)
        } finally {
          this.loading = false
          this.detectChanges()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  async copy(wheel: iLuckyWheel) { 
    let data: iCopyData = {
      mode: 'wheel',
      name: wheel.name,
      type: 'Vòng quay',
      wheel: wheel,
      app: this.chatbotApi.chatbotAuth.app
    }
    let dialogRef = this.dialog.open(CopyDialog, {
      width: '650px',
      data
    })
    let dialogClosedSub: Subscription
    dialogClosedSub = dialogRef.afterClosed().subscribe(async result => {
      if (result) {
        let { wheel, app } = result
        if (app._id == this.chatbotApi.chatbotAuth.app._id) {
          await this.reload(false)
          this.openWheel(wheel)
        } else {
          this.router.navigate((['apps'])).then((e)=>{
            this.router.navigate((['apps', app._id]), { queryParams:{ mode:"wheel", item: JSON.stringify({_id: wheel._id, name: wheel.name }) } })
          })
        }
      } else {
        if (dialogRef.componentInstance.result) {
          await this.reload(false)
        }
      }
      dialogClosedSub.unsubscribe()
    })
  }

  async remove(wheel: iLuckyWheel) {
    if (!await this.alert.warning('Xóa vòng quay', 'Bạn có muốn xóa vòng quay này?')) return

    try {
      this.loading = true
      this.detectChanges()
      let id = wheel._id
      await this.chatbotApi.luckyWheel.delete(wheel._id)
      if (this.selectedWheel && id == this.selectedWheel._id) this.openWheel()
    } catch (err) {
      console.error(err)
      this.alert.handleError(err, "Lỗi khi xóa vòng xoay");
      // this.alert.error('Không tạo được vòng xoay', err.message)
    } finally {
      this.loading = false
      this.detectChanges()
    }
  }

  async openWheel(wheel = null) {
    if (this.selectedWheel != wheel) {
      this.selectedWheel = wheel
      this.detectChanges()
      if (this.tabIndex == 2) {
        this.loadResult()
      }
    }
  }

  addGift() {
    this.selectedWheel.gifts.push({
      name: '',
      code: '',
      image: 'https://i.imgur.com/5JXbK3J.png',
      type: 'gift',
      amount: 0,
      used: 0
    })
    this.showSaveButton()
    this.detectChanges()
  }

  removeGift(index) {
    this.selectedWheel.gifts.splice(index, 1)
    this.showSaveButton()
    this.detectChanges()
  }

  drop(event) {
    if (event.previousIndex == event.currentIndex) return
    let container = event.container.data as iLuckyWheelGift[]
    moveItemInArray(container, event.previousIndex, event.currentIndex);
    this.showSaveButton()
    this.detectChanges()
  }

  moveGift(from, to) {
    if (to < 0 || to >= this.selectedWheel.gifts) return
    moveItemInArray(this.selectedWheel.gifts, from, to)
    this.showSaveButton()
  }

  changeTab(index) {
    this.tabIndex = index
    if (index == 2) {
      this.loadResult()
    }
    this.detectChanges()
  }

  async loadResult(local = true) {
    if (!this.selectedWheel) return
    try {
      this.loadingResult = true
      this.detectChanges()
      let tasks = []
      this.currentPage = 1
      tasks.push(this.chatbotApi.luckyWheelReceive.getList({
        local, query: { 
          limit: this.limitWheelResult, 
          page: this.currentPage,
          order: { 'createdAt': -1 }, 
          populates:["subscriber"], 
          filter: { luckyWheel: this.selectedWheel._id }
        }
      }).then(result => {
        this.wheelResults = result
        this.totalWheelResult = this.chatbotApi.luckyWheelReceive.pagination.totalItems
      }))

      await Promise.all(tasks)
      console.log(this.wheelResults, this.totalWheelResult)
    } catch (err) {
      console.error(err)
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi lấy dữ liệu", err.message)
    } finally {
      this.loadingResult = false
      this.detectChanges()
    }
  }

  onImageSourceChanged(event, property, index = null) {
    this.showSaveButton()
    if (index != null) {
      this.selectedWheel.gifts[index].image = event
    } else {
      this.selectedWheel.config[property] = event
    }
  }

  async onUploading(event, property, index = null) {
    if (index) {
      this.isUploading[property] = event
    } else {
      this.isUploading[index] = event
    }
    this.detectChanges()
  }

  dataChanged() {
    this.showSaveButton()
  }

  async saveContent() {
    if (!this.selectedWheel) return

    if (this.selectedWheel.successRatio < 0 || this.selectedWheel.successRatio > 100) this.alert.info('Tỉ lệ thắng không hợp lệ', 'Tỉ lệ thắng phải >= 0 và <=100')

    try {
      this.showSaving()
      this.selectedWheel = await this.chatbotApi.luckyWheel.update(this.selectedWheel._id, this.selectedWheel)
      this.wheels[this.wheels.findIndex(x => x._id == this.selectedWheel._id)] = this.selectedWheel
      this.showSaveSuccess()
    } catch (err) {
      this.alert.handleError(err, "Lôi khi lưu");
      // this.alert.error("Lưu không thành công", err.message)
    }
  }

  async removeResult(res) {
    let index = this.wheelResults.findIndex(x => x._id == res._id)
    try {
      await this.chatbotApi.luckyWheelReceive.deleteSubscribers([res._id])
      this.loadResult(false)
    } catch (err) {
      this.wheelResults.splice(index, 0, res)
      this.detectChanges()
    }
  }

  async exportCSV() {
    let res = await this.chatbotApi.luckyWheel.exportCSV(this.selectedWheel._id, this.selectedWheel.name)
    console.log(res)
  }

  async removeAllResults() {
    if (!await this.alert.warning('Xóa tất cả kết quả', 'Bạn có chắc chắn muốn xóa hết tất cả kết quả không')) return
    try {
      this.loadingResult = true
      this.detectChanges()
      await this.chatbotApi.luckyWheel.removeAllResults(this.selectedWheel._id)
      this.wheelResults = []
    } catch (err) {

    } finally {
      this.loadingResult = false
      this.detectChanges()
    }
  }

  async refreshResult() {
    try {
      this.loadingResult = true
      this.detectChanges()
      await this.loadResult(false)
    } catch (err) {
    } finally {
      this.loadingResult = false
      this.detectChanges()
    }
  }

  async resetGiftUsage(gift) {
    try {
      this.loading = true
      this.detectChanges()
      await this.chatbotApi.luckyWheel.resetGiftUsage(this.selectedWheel._id, gift._id)
      gift.used = 0
    } catch (err) {
    } finally {
      this.loading = false
      this.detectChanges()
    }
  }

  async editDesc(event, gift) {    
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      targetElement: event.target,
      width: 300,
      horizontalAlign: 'center',
      fields: [
        {
          type: 'textarea',
          placeholder: 'Ghi chú',
          property: 'desc',
          current: gift.desc,
          constraints: ['required']
        }
      ],
      onSubmit: (data) => {
        if (data.desc) {
          gift.desc = data.desc
          this.showSaveButton()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  trackByFn(index, item) {
    return item._id
  }

  async loadMoreWheelResult() {
    try {
      this.loadingMore = true
      this.changeRef.detectChanges()
      this.currentPage += 1
      let tasks = []
      tasks.push(this.chatbotApi.luckyWheelReceive.getList({
        local: false, query: { 
          limit: this.limitWheelResult, 
          page: this.currentPage,
          order: { 'createdAt': -1 }, 
          populates:["subscriber"], 
          filter: { luckyWheel: this.selectedWheel._id 
        } }
      }).then(result => {
        this.wheelResults = [...this.wheelResults, ...result]
        console.log( 'res',this.wheelResults)
      }))
      await Promise.all(tasks)
  } catch (err) {
      this.alert.handleError(err, "Lỗi khi lấy thêm dữ liệu");
  } finally {
      this.loadingMore = false
      this.changeRef.detectChanges()
  }
  }

  async downloadTemplate() {
    try {
      let data = await this.chatbotApi.luckyWheel.getImportTemplate()
      const blob = new Blob([data], {
        type:
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      });
      saveAs(
        blob,
        `MAU_IMPORT_VONG_QUAY.xlsx`
      );
    } catch (err) {
      console.error(err)
      this.alert.handleError(err, 'Lỗi xảy ra khi tải mẫu')
    }
  }

  async fileChanged(event) {
    let files = event.target.files;
    if (files && files.length) {
      try {
        let res = await this.chatbotApi.luckyWheel.importTemplate(files[0]);
        this.alert.success(`Import thành công`, `Đã import mẫu vòng quay.`);
        await this.reload(false)
      } catch (err) {
        console.error(err);
        this.alert.error(`Import thất bại`, err.message);
      } finally {
        event.target.value = '';
      }
    }
  }

  openWheelGenerator() {
    let dialogRef = this.dialog.open(WheelGeneratorDialog, {
      width: '960px',
      data: {
        wheel: this.selectedWheel
      }
    }) as MatDialogRef<WheelGeneratorDialog>
    
    let sub = dialogRef.componentInstance.imageChanged$.subscribe(change => {
      if (change) {
        if (change.type == 'image') {
          this.selectedWheel.config.wheelImage = change.url
          this.dataChanged()
        } else if (change.type == 'pin') {
          this.selectedWheel.config.pinImage = change.url
          this.dataChanged()
        }
      }
    })
    dialogRef.afterClosed().toPromise().then(res => {
      sub.unsubscribe()
    })
  }

}
