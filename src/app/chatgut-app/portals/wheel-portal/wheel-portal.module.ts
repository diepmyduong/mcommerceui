import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { WheelPortalComponent } from './wheel-portal.component';
import { MatToolbarModule, MatIconModule, MatTooltipModule, MatTabsModule, MatProgressSpinnerModule, MatButtonModule, MatSelectModule } from '@angular/material';
import { CardImageModule } from 'app/shared/cards/card-image/card-image.module';
import { ImagePopupModule } from 'app/modals/image-popup/image-popup.module';
import { FormsModule } from '@angular/forms';
import { NameFilterPipeModule } from 'app/shared/pipes/name-filter-pipe/name-filter-pipe.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { SelectionModule } from 'app/modals/selection/selection.module';
import { CopyModule } from 'app/modals/copy/copy.module';
import { WheelGeneratorDialogModule } from './wheel-generator-dialog/wheel-generator-dialog.module';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatTooltipModule,
    MatTabsModule,
    CardImageModule,
    MatProgressSpinnerModule,
    NameFilterPipeModule,
    MatButtonModule,
    MatSelectModule,
    FormsModule,
    DragDropModule,
    ImagePopupModule,
    WheelGeneratorDialogModule,
    CopyModule,
  ],
  declarations: [WheelPortalComponent],
  entryComponents: [WheelPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: WheelPortalComponent }
  ]
})
export class WheelPortalModule { }
