import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscribersPortalComponent } from 'app/chatgut-app/portals/subscribers-portal/subscribers-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { MatToolbarModule, MatTooltipModule, MatMenuModule, MatButtonModule, MatIconModule, MatProgressBarModule, MatProgressSpinnerModule, MatDividerModule, MatListModule, MatLineModule } from '@angular/material';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';
import { FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    EditInfoModule,
    MatToolbarModule,
    MatTooltipModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatDividerModule,
    MatListModule,
    MatLineModule,
    ScrollDispatchModule,
    FormsModule,
  ],
  declarations: [SubscribersPortalComponent],
  entryComponents: [SubscribersPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: SubscribersPortalComponent }
  ]
})
export class SubscribersPortalModule { }
