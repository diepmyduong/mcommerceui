import * as moment from 'moment'
import { Component, OnInit, ViewChild, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { MatDialog } from '@angular/material'
import { iSubscriber } from 'app/services/chatbot/api/crud/subscriber';
import { BehaviorSubject } from 'rxjs';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { DynamicComponentLoaderService } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { environment } from '@environments';
@Component({
  selector: 'app-subscribers-portal',
  templateUrl: './subscribers-portal.component.html',
  styleUrls: ['./subscribers-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SubscribersPortalComponent extends BasePortal implements OnInit {

  @ViewChild(CdkVirtualScrollViewport) scrolling: CdkVirtualScrollViewport;
  // @ViewChild(DatatableComponent) datatable: DatatableComponent;
  constructor(
    public dialog: MatDialog,
    public selectionDialog: MatDialog,
    public changeRef: ChangeDetectorRef,
    private dynamicLoader: DynamicComponentLoaderService
  ) {
    super()
    moment.locale('vi')
  }
  componentName = "SubscribersPortalComponent"
  selectedSubscriber: string
  searchString: string = ''
  loadingMore: boolean = false
  subscribers: iSubscriber[] = []
  searchedSubscribers: iSubscriber[] = []
  firstLoadDone: boolean = false
  loadDone: boolean = false
  rows: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  scrolledToBottom = new EventEmitter<number>()
  currentPage: number = 1
  searchPage: number = 1
  isMaximum: boolean = false
  total: number
  url: string

  async ngOnInit() {
    this.setPortalName("Live chat") 
    this.reload()
    this.url = `${environment.chatbot.livechatHost}?token=${this.chatbotApi.chatbotAuth.appToken}`
    this.dynamicLoader.getComponentFactory('ChatboxPortalComponent')
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
    if (this.subscriptions.onStoryClosed) this.subscriptions.onStoryClosed.unsubscribe()
  }

  async refresh() {
    this.reload(false)
  }

  async reload(local: boolean = false) {
    try {
      this.loadDone = false
      if (this.searchString) this.searchPage = 1
      this.detectChanges()
      let subscribers = await this.chatbotApi.subscriber.getList({
        local, query: {
          fields: "messengerProfile phone status snippetUpdatedAt uid snippet updatedAt",
          limit: this.searchString?(this.searchedSubscribers.length?this.searchedSubscribers.length:20):(this.subscribers.length?this.subscribers.length:20),
          page: 1,
          order: { "snippetUpdatedAt": -1 },
          filter: { $text: this.searchString?{ $search: this.searchString }:undefined }
        }
      })
      // subscribers = sortBy(subscribers, ["snippetUpdatedAt"])
      console.log(subscribers)
      this.postProcessData(subscribers)
      if (this.searchString) {
        this.searchedSubscribers = subscribers
      } else {
        this.subscribers = subscribers
      }
      this.rows.next(this.subscribers)
      this.total = this.chatbotApi.subscriber.pagination.totalItems
      if (this.subscribers.length >= this.total) this.isMaximum = true
    } catch(err) {
      this.alert.handleError(err, 'Lỗi khi lấy danh sách người dùng');
    } finally {
      this.loadDone = true
      this.firstLoadDone = true
      this.detectChanges()
    }
  }

  async loadMore(local = true) {
    if (this.isMaximum) return;

    try {
      this.loadingMore = true
      this.detectChanges()
      if (this.searchString) {
        this.searchPage += 1
      } else {
        this.currentPage += 1
      }
      
      let subscribers = await this.chatbotApi.subscriber.getList({
        local, query: {
          fields: "messengerProfile phone status snippetUpdatedAt uid snippet updatedAt",
          limit: 20,
          offset: this.subscribers.length,
          order: { "snippetUpdatedAt": -1 },
          filter: { $text: this.searchString?{ $search: this.searchString }:undefined }
        }
      })
      this.postProcessData(subscribers)
      if (this.searchString) {
        this.searchedSubscribers = this.searchedSubscribers.concat(subscribers)
      } else {
        this.subscribers = this.subscribers.concat(subscribers)
      }
      this.rows.next(this.subscribers)
      this.total = this.chatbotApi.subscriber.pagination.totalItems
      if (this.subscribers.length >= this.total) this.isMaximum = true
    } catch (err) {
      this.alert.handleError(err, 'Lỗi khi tải thêm người dùng')
    } finally {
      this.loadingMore = false
      this.detectChanges()
    }
  }

  postProcessData(subscribers) {

    subscribers.forEach(sub => {
      if (!sub.snippetUpdatedAt) return
        const duration = moment.duration(moment().diff(moment(sub.snippetUpdatedAt)))
        const years = Math.round(duration.asYears())
        if (years >= 1) {
          sub.fromNow = years + ' năm'
        } else {
          const months = Math.round(duration.asMonths())
          if (months >= 1) {
            sub.fromNow = months + ' tháng'
          } else {
            const days = Math.round(duration.asDays())
            if (days >= 1) {
              sub.fromNow = days + ' ngày'
            } else {
              const hours = Math.round(duration.asHours())
              if (hours >= 1) {
                sub.fromNow = hours + ' giờ'
              } else {
                const mins = Math.round(duration.asMinutes())
                if (mins >= 2) {
                  sub.fromNow = mins + ' phút'
                } else {
                  sub.fromNow = 'mới đây'
                }
              }
            }
          }
  
        }
    })
  }

  scrollToTop() {
    try {
      this.scrolling.scrollToIndex(0, 'smooth')
    } catch (err) { 
      console.log( err)
    }
  }

  scrollToBottom() {
    try {
      this.scrolling.scrollToIndex(this.scrolling.getDataLength() - 1, 'smooth')
    } catch (err) {
      console.log( err)
    }
  }

  async showInfo(subscriber: iSubscriber) {
    console.log( 'sub',subscriber)
    const componentRef = await this.portalService.pushPortalAt(this.index + 1, "ChatboxPortalComponent", {
      subscriberId: subscriber._id,
      uid: subscriber.uid,
      portalName: subscriber.messengerProfile.name?subscriber.messengerProfile.name:(subscriber.messengerProfile.first_name + ' ' + subscriber.messengerProfile.last_name)
    })
    if (!componentRef) return;

    const component = componentRef.instance as any;
    this.selectedSubscriber = subscriber._id;
    this.subscriptions.onStoryClosed = component.onPortalClosed.subscribe(() => {
      this.selectedSubscriber = null;
      this.subscriptions.onStoryClosed.unsubscribe()
      this.detectChanges()
    });
    this.detectChanges()
  }

  openLiveChat() {
    window.open(
      `${environment.chatbot.livechatHost}?token=${this.chatbotApi.chatbotAuth.appToken}`,
      '_blank'
    );
  }
}
