import { merge } from 'lodash-es'
import * as moment from 'moment'
import { Component, OnInit, Input, ViewChild, EventEmitter, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { MatDialog } from '@angular/material'
import { BehaviorSubject } from 'rxjs';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { GoogleApiService } from 'app/services/googleApi/googleApi.service';
import { iGsheetUser } from 'app/services/chatbot/api/crud/gsheetUser';
@Component({
  selector: 'app-user-googlesheet-portal',
  templateUrl: './user-googlesheet-portal.component.html',
  styleUrls: ['./user-googlesheet-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserGooglesheetPortalComponent extends BasePortal implements OnInit {

  @Input() type: 'gsheetUser' | 'infusion' 
  searchTerm:string = ""
  @ViewChild(CdkVirtualScrollViewport) scrolling: CdkVirtualScrollViewport;
  paymentWindow: Window;
  paymentPopupTimmer: any;
  constructor(
    public dialog: MatDialog,
    public selectionDialog: MatDialog,
    public googleApiSrv: GoogleApiService,
    public changeRef: ChangeDetectorRef
  ) {
    super()
    moment.locale('vi')
  }
  componentName = "UserGooglesheetPortalComponent"
  loadDone: boolean = false;
  rows: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  fullRows: any[]=[]
  scrolledToBottom = new EventEmitter<number>()
  isMaximum: boolean = false
  users: any[] = []
  reconnectUserId: string
  async ngOnInit() {
    setTimeout(() => { this.reload() }, 10)
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(local: boolean = false) {
    console.log('type', this.type)
    this.showLoading()
    try{
      if (this.type == "gsheetUser"){
        this.setPortalName("Người dùng Googlesheet")
        this.users = merge([], await this.chatbotApi.gsheetUser.getList({ local }))
      }
      if (this.type == "infusion"){
        this.setPortalName("Người dùng Infusionsoft")
        this.users = merge([], await this.chatbotApi.infusion.getList({ local }))
      }
      console.log( 'res',this.users)
        this.rows.next(this.users)
        this.fullRows = this.users
    } catch(err) {
      this.alert.error("Lỗi", "Lỗi khi lấy dữ liệu")
    } finally{
      this.hideLoading()
      this.loadDone = true
      this.detectChanges()
    }   
  }

  async loadMore() {
    if (this.isMaximum) return;
    this.reload()
  }

  scrollToTop() {

    try {
     this.scrolling.scrollToIndex(0)
    } catch (err) { 
      console.log( err)
    }
  }

  scrollToBottom() {
    try {
      this.scrolling.scrollToIndex(this.scrolling.getDataLength()-1)
    } catch (err) {
      console.log( err)
     }
  }

  stopPropagation(event) {
    event.stopPropagation();
  }
  //   applyFilter(val:string){
  //     console.log( val)
  //       if (val && val.trim() != '') {
  //           this.rows.next(
  //           this.fullRows.filter((item) => {
  //             let str = item.profile.name
  //               return (str.toLowerCase().indexOf(val.toLowerCase()) > -1);
  //           })) 
  //         }  else{
  //         this.rows.next(this.fullRows) 
  //     }
  // }
  async revokeUser(user:any){
    if (!await this.alert.warning('Hủy kết nối', 'Bạn có muốn hủy kết nối người dùng này', 'Đồng ý')) return;
    try {
      if( this.type == "gsheetUser" ){
        await this.chatbotApi.gsheetUser.revoke(user._id)
      }
      if( this.type == "infusion"){
        await this.chatbotApi.infusion.delete(user._id)
      }
      this.reload()
      this.alert.success("Thành công", "Hủy kết nối thành công")
    } catch (error) {
      this.alert.handleError(error);
      // this.alert.oror( "Không thể hủy kết nối", oror.message)
    } finally {
      this.detectChanges()
    }
  }
  addUser(){
    if( this.type == "gsheetUser"){
      this.connectGooogleSpreadsheet();
    }
    if (this.type == "infusion"){
      this.connectInfusion()
    }
  }
  async connectGooogleSpreadsheet() {
    try { 
      const response: any = await this.googleApiSrv.authorize()
      console.log( 'res',response)
      await this.chatbotApi.app.connectGSheet({ code: response.code })
      this.reload()
      this.alert.success("Thành công", "Kết nối với google spreadsheet thành công")
    } catch (error) {
      this.alert.handleError(error, "Lỗi khi kết nối");
      // this.alert.error("Không thể kết nối", error.message)
    } finally {
      this.detectChanges()
    }
  }
  async connectInfusion(mode: string = 'connect'){
    if (this.paymentWindow) {
      clearInterval(this.paymentPopupTimmer);
      this.paymentWindow.close()
    }
    this.getCodeInfu(mode)
  }
  getCodeInfu(mode){
    try {
      const client_id = "hrd8j9st5yjcgez9e8k7gryc";
      const redirect_uri = location.origin+'/apps';
      const url = `https://signin.infusionsoft.com/app/oauth/authorize?client_id=${client_id}&redirect_uri=${redirect_uri}&response_type=code&scope=full`
      
      let width = 800
      let height = 700
      let left = (window.screen.width / 2) - ((width / 2) + 10);
      let top = (window.screen.height / 2) - ((height / 2) + 50);
      this.paymentWindow = window.open('about:blank', "MsgWindow", "width=" + width + ",height=" + height + ",left=" + left+ 
      ",top=" + top + ",screenX=" + left + ",screenY="+ top);
      window.focus();

      this.paymentWindow.location.href = url;
      this.paymentWindow.window.focus();
      if(mode == "connect"){
        (<any>window).infusionCallback = this.onInfusionCallback.bind(this);
      }
      if(mode == "reconnect"){
        (<any>window).infusionCallback = this.onInfusionCallbackReconnect.bind(this);
      }
      this.watchPaymentPopup(this.paymentWindow);
    } catch(err) {
      console.log("err: ", err)
    } finally {

    }
  }
  async onInfusionCallback(data: any, childWindow) {
    console.log('on payment callback', data);
    childWindow.close()
    try{
      let res = await this.chatbotApi.app.connectInfusion({code: data.code})
      this.alert.success('Thành công','Bạn đã kết nối thành công')
      this.reload()
    } catch(err){
      this.alert.error("Lỗi", "Kết nối Infusion thất bại")
    } finally{
          
    }
  }
  async onInfusionCallbackReconnect(data: any, childWindow) {
    console.log('on payment callback', data);
    childWindow.close()
    try{
      let res = await this.chatbotApi.infusion.reconnect(this.reconnectUserId,{code: data.code})
      this.alert.success('Thành công','Bạn đã kết nối thành công')
      this.reload()
    } catch(err){
      this.alert.error("Lỗi", "Kết nối Infusion thất bại")
    } finally{
          
    }
  }
  async watchPaymentPopup(window: any) {
    if (this.paymentPopupTimmer) clearInterval(this.paymentPopupTimmer);
    this.paymentPopupTimmer = setInterval(async () => {
      if (window.closed) {
        clearInterval(this.paymentPopupTimmer);
        // this.isBooking = false
        this.paymentWindow = null
      }
    }, 500);
  }
  async reconnectUser(user: any) {
    if (!user) return
    this.reconnectUserId = user._id
    if (this.type == "gsheetUser"){
      this.reconnectUserGoogle()
    }
    if (this.type = "infusion"){
      this.connectInfusion("reconnect")
    }
  }
  async reconnectUserGoogle(){
    try { 
      const response: any = await this.googleApiSrv.authorize()
      console.log( 'res',response)
      await this.chatbotApi.gsheetUser.reconnectGSheet(this.reconnectUserId, { code: response.code })
      this.reload()
      this.alert.success("Thành công", "Kết nối người dùng thành công")
    } catch (error) {
      this.alert.handleError(error, "Lỗi khi kết nối");
      // this.alert.error("Không thể kết nối", error.message)
    } finally {
      this.detectChanges()
    }
  }

  // async showInfo(user){
  //   if (this.portalService.getLastPortalName() == "SubscriberDetailPortalComponent") {
  //     const component = this.portalService.container.portals[this.portalService.container.portals.length - 1] as any;
  //     await component.reload(user._id);
  //     component.focus()
  //   } else {
  //     this.portalService.pushPortalAt(this.index + 1, "SubscriberDetailPortalComponent", {
  //       subscriberId: user._id
  //     })
  //   }
  //   this.detectChanges()
  // }
}
