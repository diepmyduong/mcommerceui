import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { MatToolbarModule, MatTooltipModule, MatMenuModule, MatButtonModule, MatIconModule, MatProgressBarModule, MatProgressSpinnerModule, MatDividerModule, MatListModule, MatLineModule } from '@angular/material';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';
import { FormsModule } from '@angular/forms';
import { UserGooglesheetPortalComponent } from './user-googlesheet-portal.component';
@NgModule({
  imports: [
    CommonModule,
    EditInfoModule,
    MatToolbarModule,
    MatTooltipModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatDividerModule,
    MatListModule,
    MatLineModule,
    ScrollDispatchModule,
    FormsModule,
 
  ],
  declarations: [UserGooglesheetPortalComponent],
  entryComponents: [UserGooglesheetPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: UserGooglesheetPortalComponent }
  ]
})
export class UserGooglesheetPortalModule { }
