
import { Component, OnInit, ViewChild, ElementRef, Input, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import * as go from "gojs"
import { cloneDeep } from 'lodash-es'
import { iStory, iCard } from 'app/services/chatbot';
import { iStoryGroup } from 'app/services/chatbot/api/crud/storyGroup';

@Component({
  selector: 'app-diagram-portal',
  templateUrl: './diagram-portal.component.html',
  styleUrls: ['./diagram-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DiagramPortalComponent extends BasePortal implements OnInit {

  @Input() mode: 'normal' | 'broadcast' | 'sequence' = 'normal'
  @ViewChild('myDiagramDiv') element: ElementRef;
  $: any
  stories: iStory[] = []
  storyGroups: iStoryGroup[] = [];
  loadStoriesDone: boolean = false
  myDiagram: any
  constructor(
    private changeRef: ChangeDetectorRef
  ) {
    super()
    this.setPortalName("Sơ đồ câu chuyện")
  }
  componentName = "DiagramPortalComponent";
  operatorObject = {
    '==': '=',
    '!=': 'Khác',
    '!': 'không có',
    '!!': 'tồn tại',
    '>': '>',
    '>=': '>=',
    '<': '<',
    '<=': '<='
  }
  // ngAfterViewInit() {
  //   this.loadDiagram()
  // }
  async ngOnInit() {
    await this.reload();
    try {
      this.loadDiagram()
      this.loadDiagramJSON(true)
      document.querySelector('.diagram-box').children[1].classList.add('custom-scroll-bar')
    } catch (err) {
      // console.log(err)
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi load Diagram", err.message)
    } finally {
      this.detectChanges()
    }
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(local: boolean = true) {
    this.loadStoriesDone = false
    try {
      this.detectChanges()
      let tasks = []
      tasks.push(this.chatbotApi.story.getList({
        local, query: {
          limit: 0,
          fields: ["_id", "name"],
          populates: ['cards'],
          order: { 'createdAt': 1 },
        }
      })
        .then(result => {
          this.stories = cloneDeep(result);
        }))
      tasks.push(this.chatbotApi.storyGroup.getList({
        local, query: { limit: 0 }
      }).then((result) => {
        this.storyGroups = cloneDeep(result);
      }))
      await Promise.all(tasks)
    } catch (err) {
      console.log('error', err);
      this.alert.handleError(err[0]);
      // this.alert.error("Lỗi lấy dữ liệu câu chuyện", err.message)
      // console.log('err', err)
    } finally {
      this.loadStoriesDone = true;
      this.detectChanges()
    }
  }
  nodeClicked(e, obj) {
    // var evt = e.copy();
    var node = obj.part;
    // var type = evt.clickCount === 2 ? "Double-Clicked: " : "Clicked: ";
    var story = node.data
    this.openStory(story)
  }
  loadDiagram() {
    this.$ = go.GraphObject.make;
    var saveTimeout = null;
    this.myDiagram =
      this.$(go.Diagram, this.element.nativeElement,
        {
          initialAutoScale: go.Diagram.Uniform,
          toolManager: this.$(go.ToolManager, {
            mouseWheelBehavior: go.ToolManager.WheelZoom,
            panningTool: this.$(go.PanningTool, { isEnabled: true })
          }),
          "undoManager.isEnabled": true, // enable Ctrl-Z to undo and Ctrl-Y to redo
          linkTemplate: this.$(go.Link, go.Link.Bezier,
            { isLayoutPositioned: true, isTreeLink: false, curviness: -50 },
            { relinkableFrom: false, relinkableTo: false },
            this.$(go.Shape,
              { stroke: "green", strokeWidth: 2 }
            ),
            this.$(go.Shape,
              { toArrow: "Standard", stroke: "green", strokeWidth: 2 }
            ),
            this.$(go.Panel, "Auto",
              { visible: false },
              new go.Binding('visible', 'condition', function (t) { return !!t; }),
              this.$(go.Shape,
                { figure: "RoundedRectangle", fill: 'rgba(255,255,255,0.75)', stroke: null }
              ),
              this.$(go.TextBlock,
                new go.Binding("text", "condition"),
                {
                  stroke: "green", maxSize: new go.Size(300, NaN), margin: 0
                }
              )
            )
          ),
          groupTemplate: this.$(go.Group, "Auto",
            // declare the Group.layout:
            {
              layout: this.$(go.ForceDirectedLayout,
                { defaultSpringLength: 1, }
              )
            },
            this.$(go.Shape, "RoundedRectangle",  // surrounds everything
              { parameter1: 10, fill: "rgba(128,128,128,0.33)" }),
            this.$(go.Panel, "Vertical",  // position header above the subgraph
              this.$(go.TextBlock,     // group title near top, next to button
                { font: "Bold 12pt Sans-Serif" },
                new go.Binding("text", "name")),
              this.$(go.Placeholder,     // represents area for all member parts
                { padding: 5, background: "white" })
            )
          ),
          nodeTemplate: this.$(go.Node, "Auto",
            {
              click: this.nodeClicked.bind(this),
            },
            this.$(go.Shape, {
              figure: "RoundedRectangle",
              stroke: '#5dbadc',
              fill: "#44CCFF",
              cursor: "pointer"
            }),
            this.$(go.TextBlock, "Default Text",
              { margin: 12, stroke: "white", font: "bold 16px sans-serif", cursor: "pointer" },
              new go.Binding("text", "name")
            ),
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify)
          ),
          ModelChanged: function (e) {
            if (saveTimeout) clearTimeout(saveTimeout);
            saveTimeout = setTimeout(() => {
              this.saveDiagramJSON(this.myDiagram.model.toJson());
              this.detectChanges()
            }, 500)
          }.bind(this)
        });
    this.addTypeLink()
    this.addTypeGroup()
    this.detectChanges()

  }
  addTypeGroup() {
    this.myDiagram.groupTemplateMap.add("sequence", this.$(go.Group, "Auto",
      // declare the Group.layout:
      {
        layout: this.$(go.ForceDirectedLayout,
          { defaultSpringLength: 1, }
        )
      },
      this.$(go.Shape, "RoundedRectangle",  // surrounds everything
        { parameter1: 10, fill: "red" }),
      this.$(go.Panel, "Vertical",  // position header above the subgraph
        this.$(go.TextBlock,     // group title near top, next to button
          { font: "Bold 12pt Sans-Serif", stroke: "white" },
          new go.Binding("text", "name")),
        this.$(go.Placeholder,     // represents area for all member parts
          { padding: 5, background: "white" })
      )
    ))
    this.detectChanges()
  }
  addTypeLink() {
    this.myDiagram.linkTemplateMap.add("postback", this.$(go.Link, go.Link.Bezier,
      { isLayoutPositioned: true, isTreeLink: false, curviness: -50 },
      { relinkableFrom: true, relinkableTo: true },
      this.$(go.Shape,
        { stroke: "red", strokeWidth: 2 }
      ),
      this.$(go.Shape,
        { toArrow: "Standard", stroke: "red", strokeWidth: 2 }
      ),
      this.$(go.Panel, "Auto",
        this.$(go.Shape,
          { figure: "RoundedRectangle", fill: 'rgba(255,255,255,0.75)', stroke: null }
        ),
        this.$(go.TextBlock,
          new go.Binding("text", "title"),
          {
            stroke: "red", maxSize: new go.Size(300, NaN), margin: 0
          }
        )
      )
    ))
    this.myDiagram.linkTemplateMap.add("quickReplies", this.$(go.Link, go.Link.Bezier,
      { isLayoutPositioned: true, isTreeLink: false, curviness: -50 },
      { relinkableFrom: true, relinkableTo: true },
      this.$(go.Shape,
        { stroke: "#49b2e0", strokeWidth: 2 }
      ),
      this.$(go.Shape,
        { toArrow: "Standard", stroke: "#49b2e0", strokeWidth: 2 }
      ),
      this.$(go.Panel, "Auto",
        this.$(go.Shape,
          { figure: "RoundedRectangle", fill: 'rgba(255,255,255,0.75)', stroke: null }
        ),
        this.$(go.TextBlock,
          new go.Binding("text", "title"),
          {
            stroke: "#49b2e0", maxSize: new go.Size(300, NaN), margin: 0
          }
        )
      )
    ))
    this.detectChanges()
  }
  saveDiagramJSON(json) {
    localStorage.setItem(`diagram-${this.chatbotApi.chatbotAuth.app._id}`, json)
  }
  loadDiagramJSON(local) {
    const json = localStorage.getItem(`diagram-${this.chatbotApi.chatbotAuth.app._id}`)
    if (local && json) {
      this.myDiagram.model = go.Model.fromJson(json);
    } else {
      this.myDiagram.model = this.$(go.GraphLinksModel, {
        nodeKeyProperty: 'key',
        nodeDataArray: this.getStoriesData(this.stories, this.storyGroups),
        linkDataArray: this.getStoriesLinkData(this.stories, this.storyGroups)
      });
      this.myDiagram.layout = this.$(go.ForceDirectedLayout,
        { defaultSpringLength: 1, }
      );
    }
    this.detectChanges()
  }
  getStoriesLinkData(stoires, groups) {
    const linkDatas = []
    for (let story of this.stories) {
      for (let c of story.cards as iCard[]) {
        switch (c.type) {
          case 'go_to_story':
            for (let s of c.option.story) {
              linkDatas.push({ from: story._id, to: s, condition: this.parseCondition(c.condition) });
            }
            break;
          case 'button':
          case 'activity_button':
            for (let b of c.option.attachment.payload.buttons) {
              switch (b.type) {
                case 'postback':
                  const payload = JSON.parse(b.payload);
                  if (payload.type == 'story') {
                    linkDatas.push({ from: story._id, to: payload.data, title: b.title, category: 'postback' });
                  }
                  break;
                case 'lucky_wheel':
                  if (b.storyId) {
                    linkDatas.push({ from: story._id, to: b.storyId, title: b.title, category: 'postback' });
                  }
                  break;
              }
            }
            break;
          case 'generic':
          case 'media_video':
          case 'media_image':
            for (let element of c.option.attachment.payload.elements) {
              for (let b of element.buttons) {
                if (b.type == 'postback') {
                  const payload = JSON.parse(b.payload);
                  if (payload.type == 'story') {
                    linkDatas.push({ from: story._id, to: payload.data, title: b.title, category: 'postback' });
                  }
                }
              }
            }
            break;
          case 'activity_quickreply':
            for (let quickReply of c.option.quick_replies) {
              if (quickReply.type == "quick_reply_text") {
                const payload = JSON.parse(quickReply.payload);
                linkDatas.push({ from: story._id, to: payload.data, title: quickReply.title, category: 'quickReplies' });
              }
            }
            break;
          case 'list':
            for (let b of c.option.attachment.payload.buttons) {
              if (b.type == 'postback') {
                const payload = JSON.parse(b.payload);
                if (payload.type == 'story') {
                  linkDatas.push({ from: story._id, to: payload.data, title: b.title, category: 'postback' });
                }
              }
            }
            for (let element of c.option.attachment.payload.elements) {
              for (let b of element.buttons) {
                if (b.type == 'postback') {
                  const payload = JSON.parse(b.payload);
                  if (payload.type == 'story') {
                    linkDatas.push({ from: story._id, to: payload.data, title: b.title, category: 'postback' });
                  }
                }
              }
            }
            break;
          case 'action_send_story':
            if (c.option.storyId) {
              linkDatas.push({ from: story._id, to: c.option.storyId });
            }
            break;
          case "action_sequence":
            let mode = c.option.mode == "subscribe" ? "Gắn vào luồng":" Gở khỏi luồng";
            const condition = this.parseCondition(c.condition);
            if (!condition || condition == '') {
              mode += ' nếu ' + condition;
            }
            linkDatas.push({ from: story._id, to: c.option.storyGroupId, condition: mode });
            break;
        }
      }
      if (story.quickReplies) {
        for (let quickReply of story.quickReplies) {
          if (quickReply.type == "quick_reply_text") {
            const payload = JSON.parse(quickReply.payload);
            linkDatas.push({ from: story._id, to: payload.data, title: quickReply.title, category: 'quickReplies' });
          }
        }
      }
    }
    return linkDatas;
  }
  getStoriesData(stories, groups) {
    let data = [{
      key: 'empty',
      isGroup: true,
      name: 'CÂU CHUYỆN LẺ'
    }];
    let storyOfGroup = {};
    for (let g of groups) {
      for (let s of g.stories) {
        storyOfGroup[s] = {
          group: g._id,
          type: g.type
        }
      }
      data.push({
        ...g,
        key: g._id,
        isGroup: true,
        category: g.type
      })
    }
    for (let s of stories) {
      const node = { ...s, key: s._id };
      if (storyOfGroup[s._id]) {
        node.group = storyOfGroup[s._id].group;
      } else {
        node.group = 'empty';
      }
      data.push(node);
    }

    return data;
  }
  openStory(story: iStory) {
    this.portalService.pushPortalAt(this.index + 1, "StoryDetailPortalComponent", {
      storyId: story.key,
      portalName: story.name
    })
  }
  async refresh() {
    await this.reload(false)
    try {
      this.loadDiagramJSON(false)
    } catch (err) {
      this.alert.handleError(err);
      // this.alert.error("Có lỗi khi load Diagram", err.message)
    } finally {
      this.detectChanges()
    }
  }
  parseCondition(condition: any) {
    if (!condition) return '';
    if (typeof condition == 'string') {
      return condition;
    } else {
      const method = Object.keys(condition)[0];
      const s = method == 'and' ? 'và' : 'hoặc';
      let ops = [];
      for (let o of condition[method]) {
        const opMethod = Object.keys(o)[0];
        ops.push(`${o[opMethod][0]} ${this.operatorObject[opMethod]} ${o[opMethod][1]}`);
      }
      return ops.join(s);
    }
  }
  export(){
    
    let img = this.myDiagram.makeImage({
      scale: 0.9,
      background: 'white'
    });
    var link = document.createElement('a');
    link.href = img.src;
    link.download = 'Diagram.png';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);

  }
}
