import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';

// Material
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MaterialModule } from 'app/shared';
import { DiagramPortalComponent } from './diagram-portal.component';
@NgModule({
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MaterialModule
  ],
  declarations: [DiagramPortalComponent],
  entryComponents: [DiagramPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: DiagramPortalComponent }
  ]

})
export class DiagramPortalModule { }
