import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { iStory } from 'app/services/chatbot/api/crud/story';

@Component({
  selector: 'app-story-info-portal',
  templateUrl: './story-info-portal.component.html',
  styleUrls: ['./story-info-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StoryInfoPortalComponent extends BasePortal implements OnInit {

  @Input() storyId: string
  constructor(
    public changeRef: ChangeDetectorRef
  ) {
    super()
    this.setPortalName("Cài đặt câu chuyện")
  }
  componentName = "StoryInfoPortalComponent"
  story: iStory
  

  async ngOnInit() {
    this.reload()
  }

  async ngAfterViewInit() {

  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(local: boolean = true) {
    this.story = await this.chatbotApi.story.getItem(this.storyId, { local })
    this.detectChanges()
  }

  public _this = this
}
