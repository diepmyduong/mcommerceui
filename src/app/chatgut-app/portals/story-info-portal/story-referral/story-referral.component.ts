import * as moment from 'moment'
import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ChatbotApiService, iReferral } from 'app/services/chatbot';
import { BaseComponent } from 'app/base.component';
import { BehaviorSubject } from 'rxjs';
import { iEditInfoDialogData, EditInfoDialog } from 'app/modals/edit-info/edit-info.component';
import { MatDialog } from '@angular/material';
import { AlertService } from 'app/services/alert.service';
import { StoryInfoPortalComponent } from 'app/chatgut-app/portals/story-info-portal/story-info-portal.component';
import { distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'story-referral-card',
  templateUrl: './story-referral.component.html',
  styleUrls: ['./story-referral.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StoryReferralComponent extends BaseComponent implements OnInit {

  @Input() storyId: string
  @Input() portal: StoryInfoPortalComponent
  @Output() onRefClick = new EventEmitter<iReferral>()
  constructor(
    public chatbotApi: ChatbotApiService,
    public dialog: MatDialog,
    public alert: AlertService,
    public changeRef: ChangeDetectorRef
  ) {
    super('Story Referral Component')
  }
  referrals = new BehaviorSubject<iReferral[]>([])
  Refs: iReferral[] = []
  loadDone:boolean = false
  async ngOnInit() {
    await this.reload()
    this.subscriptions.push(this.referrals.pipe(distinctUntilChanged()).subscribe(refs => {
      // this.completedRefs = []
      // this.runningRefs = []
      // this.scheduledRefs = []
      // refs.forEach(r => {
      //   const toDate = moment()
      //   if (toDate < moment(r.startDate)) {
      //     this.scheduledRefs.push(r)
      //   } else if ((moment(r.startDate) <= toDate && moment(r.endDate) > toDate) && (r.amount == 0 || r.amount > r.used) && r.status == 'active') {
      //     this.runningRefs.push(r)
      //   } else if (r.status == 'active') {
      //     this.completedRefs.push(r)
      //   } else {
      //     this.deletedRefs.push(r)
      //   }
      // })
      refs.sort(ref => {
        return ref.status == "active"? -1: 1
      })
      this.Refs = refs
      console.log( 'res',refs)
      this.changeRef.detectChanges()
    }))
    this.subscriptions.push(this.chatbotApi.referral.items.subscribe(change => {
      this.reload()
    }))
  }

  async reload(local: boolean = true) {
    this.loadDone = false
    this.showLoading()
    this.changeRef.detectChanges()
    this.referrals.next(await this.chatbotApi.referral.getList({
      local,
      query: {
        filter: {
          type: 'story',
          'option.storyId': this.storyId
        }
      }
    }))
    this.hideLoading()
    this.loadDone = true
    this.changeRef.detectChanges()
  }

  openRef(ref: iReferral) {
    this.onRefClick.emit(ref)
    this.portal.portalService.pushPortalAt(this.portal.index + 1, "StoryReferralPortalComponent", {
      refId: ref._id,
      portalName: ref.name
    })
  } 
  
  async addRef() {
    let data: iEditInfoDialogData = {
      title: "Tạo referral",
      confirmButtonText: "Tạo mã QR mới",
      cancelButtonText: "Quay lại",
      inputs: [
        {
          title: "Tên mã QR",
          property: "name",
          type: "text",
          required: true,
        }
      ]
    };

    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    });
    dialogRef.afterClosed().toPromise().then(async result => {
      if (result) {
        try {
          let referral: iReferral = {
            name: result.name,
            limit: 0,
            amount: 0,
            type: 'story',
            option: {
              storyId: this.storyId
            },
            startDate: moment().add(1, 'days').startOf('day').toDate(),
            endDate :moment().add(1, 'days').endOf('day').toDate(),
          }
          let createdRef = await this.chatbotApi.referral.add(referral)
          this.reload(false)
          this.openRef(createdRef)
        } catch (err) {
          this.alert.handleError(err, "Lỗi khi tạo mã QR");
          // this.alert.error("Không thể tạo referral", err.message)
        } finally {
          this.changeRef.detectChanges()
        }
      }
    })
  }

  async removeRef(ref: iReferral, array: iReferral[]) {
    
    if (!await this.alert.warning("Xóa mã QR", "Bạn có muốn xóa mã QR " + ref.name + " không?", "Xóa mã QR")) return;
    
    try {
      this.changeRef.detectChanges()
      await this.chatbotApi.referral.delete(ref._id)
      this.reload(false)
      this.alert.success("Thành công", "Đã xóa thành công mã QR")
    } catch(err) {
      this.alert.handleError(err);
      // this.alert.error("Không thể xóa referral", "Vui lòng thử lại")
    } finally {
      this.changeRef.detectChanges()
    }
  }
}
