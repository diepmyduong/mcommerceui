import { isObject, merge } from 'lodash-es'
import { Component, OnInit, Input, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ChatbotApiService, iStory, iIntent, iPage } from 'app/services/chatbot';
import { NgForm } from '@angular/forms';
import { BaseComponent } from 'app/base.component';
import { StoryInfoPortalComponent } from 'app/chatgut-app/portals/story-info-portal/story-info-portal.component';
import { iDictionary } from 'app/services/chatbot/api/crud/dictionnary';
import { MatSlideToggle, MatRadioChange, MatDialog } from '@angular/material';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { AlertService } from 'app/services/alert.service';

@Component({
  selector: 'story-keys-card',
  templateUrl: './story-keys.component.html',
  styleUrls: ['./story-keys.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StoryKeysComponent extends BaseComponent implements OnInit {

  @Input() storyId: string
  @Input() portal: StoryInfoPortalComponent
  @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
  constructor(
    public chatbotApi: ChatbotApiService,
    public alert: AlertService,
    public dialog: MatDialog,
    private changeRef: ChangeDetectorRef
  ) {
    super('Story Keys Component')
  }
  keys: iDictionary[] = []
  intents: iIntent[] = []
  story: iStory
  chipOptions = {
    visible: true,
    selectable: true,
    removable: true,
    addOnBlur: false,
    separatorKeysCodes: [ENTER, COMMA]
  }
  new_key: string = ''
  page: iPage

  async ngOnInit() {
    this.reload()
  }

  async reloadKeys(local: boolean = true) {
    this.keys = [...await this.chatbotApi.dictionary.getList({
      local,
      query: {
        filter:  {
          stories: this.storyId
        }
      }
    })]
    this.changeRef.detectChanges()
  }

  async reload(local: boolean = false) {
    this.showLoading()
    this.changeRef.detectChanges()
    const { app } = this.chatbotApi.chatbotAuth
    let pageId = isObject(app.activePage) ? (app.activePage as iPage)._id : app.activePage as string
    this.page = await this.chatbotApi.page.getItem(pageId)
    this.story = merge({}, await this.chatbotApi.story.getItem(this.storyId, { local }))
    if(this.page.aiState === 'active') {
      this.intents = [...await this.chatbotApi.page.getIntents({ local })]
    }
    if(this.story.type === 'custom') {
      await this.reloadKeys(local)
    }
    this.hideLoading()
    this.changeRef.detectChanges()
  }

  async addKey(value: string) {

    if (this.keys.findIndex(dic => dic.key == value) != -1) {
      this.alert.info('Chú ý', 'Không thể thêm một từ khóa đã có sẵn')
      return;
    }

    let new_key:iDictionary = {
      key: value
    }
    try {
      this.new_key = ""
      this.keys.splice(this.keys.length, 0, new_key)
      this.changeRef.detectChanges()
      let dic = await this.chatbotApi.story.addkey(this.storyId, value)
      this.keys[this.keys.findIndex(dic => dic.key == value && dic._id == undefined)] = dic
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi thêm từ khóa");
      // this.alert.error("Có lỗi xảy ra khi thêm từ khóa " + value, err.message)
      this.keys.splice(this.keys.findIndex(dic => dic.key == value), 1)
    } finally {
      this.changeRef.detectChanges()
    }
  }

  async removeKey(dic: iDictionary) {
    let newDic = {...dic};
    let index = this.keys.findIndex(d => d.key === dic.key)
    try {
      this.keys.splice(index, 1)
      this.changeRef.detectChanges()
      await this.chatbotApi.dictionary.deleteStoryFromKey(newDic._id, this.storyId)
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi xóa từ khóa");
      // this.alert.error("Có lỗi xảy ra khi xóa từ khóa " + newDic.key, err.message) 
      this.keys.splice(index, 0, newDic)
    } finally {
      this.changeRef.detectChanges()
    }
  }

  async onChangeType(event: MatRadioChange) {
    const type = event.value
    if(type == 'intent' && !this.story.intent) {
      try {
        this.story.intent = ''
        const story = await this.chatbotApi.story.update(this.story._id, this.story, { reload: true })
      } catch(err) {
        console.error(err)
        this.story.type == 'custom'
        this.story.intent = ''
        const story = await this.chatbotApi.story.update(this.story._id, this.story, { reload: true })
        await this.reloadKeys()
      } finally {
        this.changeRef.detectChanges()
      }
    } else if(type == 'custom') {
      this.story.intent = ''
      const story = await this.chatbotApi.story.update(this.story._id, this.story, { reload: true })
      await this.reloadKeys()
      this.changeRef.detectChanges()
    }
  }

  async changeIntent() {
    try {
      const story = await this.chatbotApi.story.update(this.story._id, this.story, { reload: true })
    } catch(err) {
      this.alert.handleError(err);
      // this.alert.error('Lỗi', 'Có lỗi xảy ra khi đổi intent')
    } finally {
      this.changeRef.detectChanges()
    }
  }

}