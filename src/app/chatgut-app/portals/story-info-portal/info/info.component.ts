import { merge } from 'lodash-es'
import { Component, OnInit, Input, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { ChatbotApiService, iStory, iPage } from 'app/services/chatbot';
import { BaseComponent } from 'app/base.component';
import { StoryInfoPortalComponent } from 'app/chatgut-app/portals/story-info-portal/story-info-portal.component';
import { AlertService } from 'app/services/alert.service';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { PortalService } from 'app/services/portal.service';
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'story-info-card',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfoComponent extends BaseComponent implements OnInit {

  @Input() storyId: string
  @Input() portal: StoryInfoPortalComponent
  constructor(
    public chatbotApi: ChatbotApiService,
    public alert: AlertService,
    private popoverService: PopoverService,
    private portalService: PortalService,
    private snackBar: MatSnackBar,
    private changeRef: ChangeDetectorRef
  ) { 
    super('Story Info Component')
  }
  story: iStory
  pageId: string

  hasToggled:boolean = false
  toggling: boolean = false
  refLink:string = ''
  showCopied:boolean = false
  doneCopyLink:boolean = true
  copyLinkChange:boolean = false

  payloadStory:string = ''
  additionalStory:string = ''


  async ngOnInit() {
    //this.portal.showLoading()
    this.portal.detectChanges()
    let story = await this.chatbotApi.story.getItem(this.storyId)
    this.story = merge({}, story)
    this.refLink = this.story.ref || this.story.name
    this.hasToggled = this.story.isUseRef

    this.payloadStory = `{"type":"story","data":"${this.story._id}"}` 
    console.log('story', this.story)
    //this.portal.hideLoading()
    this.pageId = (this.chatbotApi.chatbotAuth.app.activePage as iPage).pageId
    this.changeRef.detectChanges()
  }  
  
  async editName(event) {
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      targetElement: event.target,
      width: 300,
      fields: [
        {
          placeholder: 'Tên câu chuyện',
          property: 'name',
          current: this.story.name,
          constraints: ['required']
        }
      ],
      onSubmit: (data) => {
        if (this.story.name == data.name || !data.name) return
        let fallback = this.story.name
        this.story.name = data.name
        this.chatbotApi.story.updateName(this.story._id, this.story.name, { reload: true })
        .then(result => {
          let storiesListPortal = this.portalService.container.portals.find(x => x.componentName == "StoriesPortalComponent")
          if (storiesListPortal) {
            storiesListPortal.refresh()
          }
          let storyDetailsPortal = this.portalService.container.portals.find(x => x.componentName == "StoryDetailPortalComponent" && x.portalName == fallback)
          if (storyDetailsPortal) {
            storyDetailsPortal.story.name = this.story.name
          }
          this.changeRef.detectChanges()
        })
        .catch(err => {
          console.error(err)
          this.story.name = fallback
        })
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  copyId(){
    let input = document.createElement('input');
    document.body.appendChild(input)
    input.value = this.story._id
    input.select();
    document.execCommand('copy',false);
    input.remove();
    this.snackBar.open('✔️ Đã copy vào clipboard', '', {
      duration: 2000,
    });
    this.changeRef.detectChanges()
  }

  useRefChanged() {
    this.chatbotApi.story.update(this.story._id, { isUseRef: this.story.isUseRef })
    this.changeRef.detectChanges()
  }
  editAdditional(event){
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      target: '.info-wrapper',
      targetElement: event.target,
      width: '300px',
      horizontalAlign: 'left',
      fields: [
        {
          placeholder: 'Nhập thông tin additional',
          property: 'name',
          current: this.additionalStory,
          constraints: ['ref']
        }
      ],
      onSubmit: (data) => {
        if (this.additionalStory == data.name) return
        this.additionalStory = data.name
        if(this.additionalStory){
          this.payloadStory = `{"type":"story","data":"${this.story._id}","additional":"${this.additionalStory}"}`
        }else{
          this.payloadStory = `{"type":"story","data":"${this.story._id}"}`
        }
        
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }
  editRef(event) {
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      target: '.info-wrapper',
      targetElement: event.target,
      width: '300px',
      horizontalAlign: 'left',
      fields: [
        {
          placeholder: 'Tên ref',
          property: 'ref',
          current: this.story.ref,
          constraints: ['ref']
        }
      ],
      onSubmit: (data) => {
        if (this.story.ref == data.ref) return
        let fallback = this.story.ref
        this.story.ref = data.ref
        this.chatbotApi.story.update(this.story._id, { ref: this.story.ref })
        .then(result => {
          this.changeRef.detectChanges()
        })
        .catch(err => {
          console.error(err)
          this.story.ref = fallback
        })
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  copyLink(){
    let input = document.createElement('input');
    document.body.appendChild(input)
    input.value = 'https://m.me/' + this.pageId + '?ref=story.' + this.story.ref
    input.select();
    document.execCommand('copy',false);
    input.remove();
    this.snackBar.open('✔️ Đã copy vào clipboard', '', {
      duration: 2000,
    });
    this.changeRef.detectChanges()
  }
  copyPayload(){
    let input = document.createElement('input');
    document.body.appendChild(input)
    input.value = this.payloadStory
    input.select();
    document.execCommand('copy',false);
    input.remove();
    this.snackBar.open('✔️ Đã copy vào clipboard', '', {
      duration: 2000,
    });
    this.changeRef.detectChanges()
  }
  async linkRefChanged() {
    if(!this.story.isUseRef){
      this.copyLinkChange = true
    }
    this.toggling = true
    try {
      if (!this.hasToggled && this.story.isUseRef) {
        this.story.isUseRef = false
        this.changeRef.detectChanges()
        await this.chatbotApi.story.update(this.storyId, this.story, { reload: true })
      }
    } catch (err) {
      // console.log(err)
      this.alert.handleError(err, "Lỗi khi thay đổi trạng thái");
      // this.alert.error('Xảy ra lỗi khi thay đổi trạng thái', err.message)
    } finally {
      this.toggling = false
      this.changeRef.detectChanges()
    }
  }
  async setupLinkRef() {
    this.doneCopyLink = false
    try {
      this.story.isUseRef = true
      this.story.ref = this.refLink
      this.changeRef.detectChanges()
      await this.chatbotApi.story.update(this.storyId, this.story, { reload: true })
    } catch (err) {
      // console.log(err)
      this.alert.handleError(err, "Lỗi khi lưu đường dẫn ref");
      // this.alert.error('Không thể lưu đường dẫn ref', err.message)
    } finally {
      this.doneCopyLink = true
      this.copyLinkChange = false
      this.changeRef.detectChanges()
    }
  }
  copyLinkClipboard() {
    let tmp = document.getElementById('link-content') as any
    let content = tmp.value
    let link = document.getElementById('link-ref') as any
    let tmp2 = link.value
    link.value = content + link.value
    link.select()
    document.execCommand("copy");
    link.value = tmp2
    this.showCopied = true
    this.changeRef.detectChanges()
    setTimeout(() => {
      this.showCopied = false
      this.changeRef.detectChanges()
    }, 1500)
  }
}
