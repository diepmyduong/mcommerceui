import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoryInfoPortalComponent } from 'app/chatgut-app/portals/story-info-portal/story-info-portal.component';
import { InfoComponent } from 'app/chatgut-app/portals/story-info-portal/info/info.component';
import { StoryReferralComponent } from 'app/chatgut-app/portals/story-info-portal/story-referral/story-referral.component';
import { StoryKeysComponent } from 'app/chatgut-app/portals/story-info-portal/story-keys/story-keys.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { MatCardModule, MatInputModule, MatFormFieldModule, MatDividerModule, MatToolbarModule, MatIconModule, MatRadioModule, MatChipsModule, MatSelectModule, MatExpansionModule, MatLineModule, MatMenuModule, MatTooltipModule, MatListModule, MatButtonModule, MatSnackBarModule, MatCheckboxModule, MatProgressSpinnerModule, MatSlideToggleModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';

@NgModule({
  imports: [
    CommonModule,
    EditInfoModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatDividerModule,
    MatToolbarModule,
    MatIconModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatRadioModule,
    MatChipsModule,
    MatSelectModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatLineModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatListModule,
    FormsModule,
    ClipboardModule,
    MatButtonModule,
  ],
  declarations: [StoryInfoPortalComponent, InfoComponent, StoryKeysComponent, StoryReferralComponent],
  entryComponents: [StoryInfoPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: StoryInfoPortalComponent }
  ]
})
export class StoryInfoPortalModule { }
