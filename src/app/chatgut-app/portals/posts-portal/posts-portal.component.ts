import { Component, OnInit, NgZone, ViewChild, ElementRef, EventEmitter, Input, Output, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { ImagePopupDialog } from 'app/modals/image-popup/image-popup.component';
import { MatDialog } from '@angular/material';
import { iInteractPost } from 'app/services/chatbot/api/crud/interactPost';
import { iFbPost } from 'app/services/chatbot/api/crud/page';
import { debounce, debounceTime } from 'rxjs/operators';

declare var $: any
@Component({
  selector: 'app-posts-portal',
  templateUrl: './posts-portal.component.html',
  styleUrls: ['./posts-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostsPortalComponent extends BasePortal implements OnInit {

  componentName = "PostsPortalComponent"
  @Input() mode: string = 'list'
  @Output() selectedPost: EventEmitter<iInteractPost> = new EventEmitter()
  @ViewChild('postsContainer') postsContainer: ElementRef
  constructor(
    private zone: NgZone,
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef
  ) {
    super()
    this.setPortalName("Bài đăng")
  }
  posts: iFbPost[] = []
  paging: any
  scrolledToBottom = new EventEmitter<number>()
  noMore: boolean = false
  openedPost: string = ''
  loadDone: boolean = false
  hideCaselessPost: boolean = false
  hiddenPostId = ''

  async ngOnInit() {
    this.reload()
  }

  async ngAfterViewInit() {
    const postsContainerEl = $(this.postsContainer.nativeElement)
    postsContainerEl.scroll(() => {
      const offset = postsContainerEl.scrollTop() + postsContainerEl.innerHeight()
      if (offset >= (this.postsContainer.nativeElement.scrollHeight - 50) && !this.progressBar.show) this.scrolledToBottom.next(offset)
    })
    this.subscriptions.onScrolledToBottom = this.scrolledToBottom.pipe(debounceTime(300)).subscribe(offset => {
      this.loadMore()
    })
  }

  async ngOnDestroy() {    
    super.ngOnDestroy()
  }

  async reload(local: boolean = true) {
    this.loadDone = false
    try {
      this.detectChanges()
      const result = await this.chatbotApi.page.getPosts({ local, query: { 
        limit: 10
      } }) as any
      this.posts = result.items
      this.paging = result.paging
    } finally {
      this.loadDone = true
      this.detectChanges()
    }
  }

  async loadMore(event?: any) {
    if (this.noMore) return
    this.showLoading()
    try {
      this.detectChanges()
      const result = await this.chatbotApi.page.getPosts({ query: { limit: 10, after: this.paging.cursors.after } }) as any
      if (result.items.length === 0) {
        this.noMore = true
      }
      this.zone.run(() => {
        this.posts = this.posts.concat(result.items)
        this.paging = result.paging
      })
    } finally {
      this.hideLoading()
      this.detectChanges()
    }
  }

  async openImage(source) {
      let data = {
          image: source
      };
  
      let dialogRef = this.dialog.open(ImagePopupDialog, {
          maxHeight: '60vh',
          panelClass: 'image-popup',
          data: data
      });
  }
  
  selectPost(post) {
    this.selectedPost.emit(post)
    this.close()
  }
  async exportStatisticPostToCsv(postId: string){
    let data: any = []
    const csv: any = await this.chatbotApi.statistic.summaryPostDetailCsvData({postId})
    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = `data.csv`;
    hiddenElement.click();
  }

  async connectPost(post: iFbPost) {
    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "PostDetailPortalComponent", { post, portalName: post.name?post.name:'Kết nối bài đăng' })
    
    if (!componentRef) return;
    this.openedPost = post.id;
    
    let component = componentRef.instance as any
    this.subscriptions.postUpdated = component.onPostUpdated.subscribe(interactPost => {
      post.isInteracted = true
      post.interactPost = interactPost
      this.detectChanges()
    })

    this.subscriptions.postClosed = component.onPostClosed.subscribe(() => {
      this.openedPost = "";
      this.subscriptions.postClosed.unsubscribe();
      this.subscriptions.postUpdated.unsubscribe();
      this.detectChanges()
    })
  }

  async openStatisticPostal(postId: string){
   
    console.log('id',postId)
      if (this.portalService.container.getPortalNameAt(this.index + 1) != "StatisticsResultPortalComponent") {
          console.log( "pushPortal")
        await this.portalService.pushPortalAt(this.index + 1,'StatisticsResultPortalComponent', { portalName: "Thống kê" })
      }
      const component = this.portalService.container.portals[this.index + 1]
      component.getStatisticResult("page-post", "Thống kê tương tác", { postId })
     
  }
}
