import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostsPortalComponent } from 'app/chatgut-app/portals/posts-portal/posts-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatToolbarModule, MatIconModule, MatProgressBarModule, MatProgressSpinnerModule, MatTooltipModule, MatButtonModule, MatDividerModule } from '@angular/material';
import { ImagePopupModule } from 'app/modals/image-popup/image-popup.module';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatButtonModule,
    MatDividerModule,
    ImagePopupModule
  ],
  declarations: [PostsPortalComponent],
  entryComponents: [PostsPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: PostsPortalComponent }
  ]
})
export class PostsPortalModule { }
