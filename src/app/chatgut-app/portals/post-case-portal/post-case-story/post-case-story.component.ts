import { Component, OnInit, Input, Output, EventEmitter, ViewChild, Host, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { iInteractPostCase, ChatbotApiService, iStory } from 'app/services/chatbot';
import { NgForm } from '@angular/forms';
import { PostCasePortalComponent } from 'app/chatgut-app/portals/post-case-portal/post-case-portal.component';

@Component({
  selector: 'app-post-case-story',
  templateUrl: './post-case-story.component.html',
  styleUrls: ['./post-case-story.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostCaseStoryComponent extends BaseComponent implements OnInit {

  @Input() case: iInteractPostCase
  @Output() change = new EventEmitter<iInteractPostCase>()
  @ViewChild('myFrm', { read: NgForm }) myFrm: NgForm
  constructor(
    @Host() public postCasePortal: PostCasePortalComponent,
    public chatbotApi: ChatbotApiService,
    public changeRef: ChangeDetectorRef
  ) {
    super('PostCaseStoryComponent')
  }
  canEmmitChange: boolean = false
  selectedStory: iStory
  stories: iStory[] = []

  async ngOnInit() {
    this.subscriptions.push(this.myFrm.valueChanges.subscribe(this.formChange.bind(this)))
    setTimeout(() => {
      this.canEmmitChange = true
      this.changeRef.detectChanges()
    }, 500)
    this.reload()
  }

  ngAfterViewInit() {
    this.case.isStoryValid = this.myFrm.valid
    this.changeRef.detectChanges()
  }

  async reload(local: boolean = true) {
    this.stories = await this.chatbotApi.story.getList({
      local, query: {
        limit: 0,
        fields: ["_id", "name"],
        order: { 'createdAt': 1 },
        filter: { mode: 'normal' }
      }
    }),
    this.selectedStory = this.stories.find(t => t._id === this.case.responseStory)
    this.changeRef.detectChanges()
  }

  formChange() {
    if (!this.canEmmitChange && !this.myFrm.dirty) return
    this.case.isStoryValid = this.myFrm.valid
    this.change.emit(this.case)
    this.changeRef.detectChanges()
  }

  enableStoryResponse(enable: boolean) {
    this.case.isResponseStory = enable
    this.case.isStoryValid = this.myFrm.valid
    this.change.emit(this.case)
    this.changeRef.detectChanges()
  }

  async selectStory() {
    const storyId = await this.postCasePortal.getStory(this.case.responseStory)
    if (storyId != this.case.responseStory) {
      this.selectedStory = this.stories.find(s => s._id === storyId)
      this.case.responseStory = storyId
    }
    this.changeRef.detectChanges()
  }
}
