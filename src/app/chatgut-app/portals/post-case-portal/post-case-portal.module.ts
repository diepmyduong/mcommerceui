import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostCasePortalComponent } from 'app/chatgut-app/portals/post-case-portal/post-case-portal.component';
import { PostCaseCommentComponent } from 'app/chatgut-app/portals/post-case-portal/post-case-comment/post-case-comment.component';
import { PostCaseOptionalComponent } from 'app/chatgut-app/portals/post-case-portal/post-case-optional/post-case-optional.component';
import { PostCasePrivateReplyComponent } from 'app/chatgut-app/portals/post-case-portal/post-case-private-reply/post-case-private-reply.component';
import { PostCaseReplyConfigComponent } from 'app/chatgut-app/portals/post-case-portal/post-case-reply-config/post-case-reply-config.component';
import { PostCaseStoryComponent } from 'app/chatgut-app/portals/post-case-portal/post-case-story/post-case-story.component';
import { PostCaseTypeComponent } from 'app/chatgut-app/portals/post-case-portal/post-case-type/post-case-type.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatFormFieldModule, MatSelectModule, MatInputModule, MatCheckboxModule, MatProgressSpinnerModule, MatProgressBarModule, MatDividerModule, MatExpansionModule, MatListModule, MatIconModule, MatToolbarModule, MatTooltipModule, MatButtonModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { SelectionModule } from 'app/modals/selection/selection.module';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatSelectModule,
    MatSelectModule,
    MatInputModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDividerModule,
    MatExpansionModule, 
    MatListModule,
    MatIconModule,
    MatButtonModule,
    TextFieldModule,
    FormsModule,
    SelectionModule,
    EditInfoModule
  ],
  declarations: [PostCasePortalComponent, PostCaseCommentComponent, PostCaseOptionalComponent, PostCasePrivateReplyComponent, PostCaseReplyConfigComponent, PostCaseStoryComponent, PostCaseTypeComponent],
  entryComponents: [PostCasePortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: PostCasePortalComponent }
  ]
})
export class PostCasePortalModule { }
