import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { NgForm } from '@angular/forms';
import { MatSelectChange } from '@angular/material';
import { iInteractPostCase } from 'app/services/chatbot/api/crud/interactPost';

@Component({
  selector: 'app-post-case-comment',
  templateUrl: './post-case-comment.component.html',
  styleUrls: ['./post-case-comment.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostCaseCommentComponent extends BaseComponent implements OnInit {

  @Input() case: iInteractPostCase
  @Output() change = new EventEmitter<iInteractPostCase>()
  @ViewChild('myFrm', { read: NgForm }) myFrm: NgForm
  constructor(
    public changeRef: ChangeDetectorRef
    ) {
    super('PostCaseCommentComponent')
  }
  canEmmitChange: boolean = false
  responseType: string
  cacheValue: any = {}

  async ngOnInit() {
    this.subscriptions.push(this.myFrm.valueChanges.subscribe(this.formChange.bind(this)))
    setTimeout(() => {
      this.canEmmitChange = true
      this.changeRef.detectChanges()
    }, 500)
    this.reload()
  }

  ngAfterViewInit() {
    this.case.isCommentValid = this.myFrm.valid
    this.changeRef.detectChanges()
  }

  async reload(local: boolean = true) {
    this.responseType = "message"
    if(this.case.isResponseComment && this.case.responseComment) {
      if(this.case.responseComment.message && this.case.responseComment.message.trim().length != 0) {
        this.responseType = 'message'
      } else {
        this.responseType = 'attachment'
      }
    }
    this.changeRef.detectChanges()
  }
  
  formChange() {
    if(!this.canEmmitChange && !this.myFrm.dirty) return
    this.case.isCommentValid = this.myFrm.valid
    this.change.emit(this.case)
    this.changeRef.detectChanges()
  }

  enableCommentResponse(enable: boolean) {
    this.case.isResponseComment = enable
    this.case.isCommentValid = this.myFrm.valid
    if(enable && !this.case.responseComment) {
      this.case.responseComment = {}
    }
    this.change.emit(this.case)
    this.changeRef.detectChanges()
  }

  changeResponseType(change: MatSelectChange) {
    switch(change.value) {
      case 'message':
        this.cacheValue.attachmentUrl = this.case.responseComment.attachment_url
        delete this.case.responseComment.attachment_url
        this.case.responseComment.message = this.cacheValue.message
        break
      case 'attachment':
        this.cacheValue.message = this.case.responseComment.message
        delete this.case.responseComment.message
        this.case.responseComment.attachment_url = this.cacheValue.attachmentUrl
        break
    }
    this.change.emit(this.case)
    this.changeRef.detectChanges()
  }
}
