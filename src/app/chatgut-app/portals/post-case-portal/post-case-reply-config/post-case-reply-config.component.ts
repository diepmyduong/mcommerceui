import { Component, OnInit, Input, Output, EventEmitter, ViewChild, Host, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { iInteractPostPrivateReplyConfig, ChatbotApiService, iIntent, iStory, iPage } from 'app/services/chatbot';
import { NgForm } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { iSelectionData, SelectionDialog } from 'app/modals/selection/selection.component';
import { PostCasePrivateReplyComponent } from 'app/chatgut-app/portals/post-case-portal/post-case-private-reply/post-case-private-reply.component'

@Component({
  selector: 'app-post-case-reply-config',
  templateUrl: './post-case-reply-config.component.html',
  styleUrls: ['./post-case-reply-config.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostCaseReplyConfigComponent extends BaseComponent implements OnInit {

  @Input() config: iInteractPostPrivateReplyConfig
  @Output() change = new EventEmitter<iInteractPostPrivateReplyConfig>()
  @ViewChild('myFrm', { read: NgForm }) myFrm: NgForm
  constructor(
    @Host() public postCasePrivateReplyComp: PostCasePrivateReplyComponent,
    public dialog: MatDialog,
    public chatbotApi: ChatbotApiService,
    public changeRef: ChangeDetectorRef
  ) {
    super('PostCaseTypeComponent')
  }
  canEmmitChange: boolean = false
  selectedIntent: iIntent
  configTypes = [
    { value: 'fail', display: 'Bất kỳ' },
    { value: 'regex', display: 'Nhập chuỗi' },
    { value: 'number', display: 'Nhập số' },
    { value: 'range', display: 'Nhập khoảng số' },
  ]
  intents: iIntent[] = []
  selectedStory: iStory
  stories: iStory[] = []

  get configTitle() {
    return this.configTypes.find(c => c.value === this.config.type).display
  }

  async ngOnInit() {
    const page = this.chatbotApi.chatbotAuth.app.activePage as iPage

    if (page.ai) {
      this.configTypes.push({
        value: 'intent',
        display: 'Sử dụng AI'
      })
    }

    await this.reload()
    this.subscriptions.push(this.myFrm.valueChanges.subscribe(this.formChange.bind(this)))
    this.subscriptions.push(this.myFrm.statusChanges.subscribe(this.formChange.bind(this)))
    setTimeout(() => {
      this.canEmmitChange = true
      this.changeRef.detectChanges()
    }, 500)
  }

  ngAfterViewInit() {
    this.config.isValid = this.myFrm.valid
  }

  async reload(local: boolean = true) {
    try {
      let tasks = []
      tasks.push(this.loadStories(local))
      // tasks.push(this.loadIntents(local))
      await Promise.all(tasks)
    } catch (err) {
      console.error(err)
    } finally {
      this.changeRef.detectChanges()
    }
  }

  async loadStories(local: boolean = true) {
    this.stories = await this.chatbotApi.story.getList({ local, query: {
      limit: 0,
      fields: ["_id", "name"],
      order: { 'createdAt': 1 },
      filter: { mode: 'normal' }
    }})
    this.selectedStory = this.stories.find(t => t._id === this.config.value)
  }

  // async loadIntents(local: boolean = true) {
  //   this.intents = await this.chatbotApi.page.getIntents({ local })
  //   if (this.config.key.intentId) {
  //     this.selectedIntent = this.intents.find(i => i.id === this.config.key.intentId)
  //   }
  // }

  formChange() {
    if (!this.canEmmitChange && !this.myFrm.dirty) return
    this.config.isValid = this.myFrm.valid
    this.change.emit(this.config)
    this.changeRef.detectChanges()
  }

  // async selectIntent() {
  //   let data: iSelectionData = {
  //     title: 'Chọn ngữ cảnh',
  //     confirmButtonText: 'Chọn',
  //     cancelButtonText: 'Quay lại',
  //     categories: [{
  //       name: "Các ngữ cảnh",
  //       options: this.intents.map(i => {
  //         return {
  //           code: i.id,
  //           display: i.name
  //         }
  //       }) as any
  //     }]
  //   };
  //   let dialogRef = this.dialog.open(SelectionDialog, {
  //     width: '600px',
  //     data: data
  //   });
  //   const intentId = await dialogRef.afterClosed().toPromise()
  //   this.selectedIntent = this.intents.find(i => i.id === intentId)
  //   this.config.key.intentId = intentId
  //   this.changeRef.detectChanges()
  // }

  async removeConfig() {
    this.postCasePrivateReplyComp.removeConfig(this.config)
    this.changeRef.detectChanges()
  }

  async selectStory() {
    const storyId = await this.postCasePrivateReplyComp.postCasePortal.getStory(this.config.value)
    console.log('herere', this.config, storyId)
    if (storyId != this.config.value) {
      this.selectedStory = this.stories.find(s => s._id === storyId)
      this.config.value = storyId
    }
    this.changeRef.detectChanges()
  }
}