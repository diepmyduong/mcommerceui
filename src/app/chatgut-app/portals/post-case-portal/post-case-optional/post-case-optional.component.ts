import { Component, OnInit, Input, Output, EventEmitter, ViewChild, Host, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { NgForm } from '@angular/forms';
import { PostCasePortalComponent } from 'app/chatgut-app/portals/post-case-portal/post-case-portal.component';
import { iInteractPostCase } from 'app/services/chatbot/api/crud/interactPost';

@Component({
  selector: 'app-post-case-optional',
  templateUrl: './post-case-optional.component.html',
  styleUrls: ['./post-case-optional.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostCaseOptionalComponent extends BaseComponent implements OnInit {

  @Input() case: iInteractPostCase
  @Output() change = new EventEmitter<iInteractPostCase>()
  @ViewChild('myFrm', { read: NgForm }) myFrm: NgForm
  constructor(
    @Host() public postCasePortal: PostCasePortalComponent,
    public changeRef: ChangeDetectorRef
    // public dialog: MatDialog
  ) {
    super('PostCaseOptionalComponent')
  }
  canEmmitChange: boolean = false
  responseType: string
  cacheValue: any = {}
  reactions = [ "LIKE" , "LOVE" , "HAHA" , "WOW" , "ANGRY"]

  async ngOnInit() {
    this.subscriptions.push(this.myFrm.valueChanges.subscribe(this.formChange.bind(this)))
    setTimeout(() => {
      this.canEmmitChange = true
      this.changeRef.detectChanges()
    }, 500)
    this.reload()
  }

  ngAfterViewInit() {
    this.case.isOptionalValid = this.myFrm.valid
    this.changeRef.detectChanges()
  }

  async reload(local: boolean = true) {
  }
  
  formChange() {
    if(!this.canEmmitChange && !this.myFrm.dirty) return
    this.case.isOptionalValid = this.myFrm.valid
    this.change.emit(this.case)
    this.changeRef.detectChanges()
  }
}

