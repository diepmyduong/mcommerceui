import * as guid from 'guid'
import { remove } from 'lodash-es'
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, Host, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { NgForm } from '@angular/forms';
import { PostCasePortalComponent } from 'app/chatgut-app/portals/post-case-portal/post-case-portal.component';
import { iInteractPostCase, iInteractPostPrivateReplyConfig } from 'app/services/chatbot/api/crud/interactPost';

@Component({
  selector: 'app-post-case-private-reply',
  templateUrl: './post-case-private-reply.component.html',
  styleUrls: ['./post-case-private-reply.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostCasePrivateReplyComponent extends BaseComponent implements OnInit {

  @Input() case: iInteractPostCase
  @Output() change = new EventEmitter<iInteractPostCase>()
  @ViewChild('myFrm', { read: NgForm }) myFrm: NgForm
  constructor(
    @Host() public postCasePortal: PostCasePortalComponent,
    public changeRef: ChangeDetectorRef
    // public dialog: MatDialog
  ) {
    super('PostCasePrivateReplyComponent')
  }
  canEmmitChange: boolean = false
  responseType: string
  cacheValue: any = {}

  async ngOnInit() {
    this.subscriptions.push(this.myFrm.valueChanges.subscribe(this.formChange.bind(this)))
    setTimeout(() => {
      this.canEmmitChange = true
      this.changeRef.detectChanges()
    }, 500)
    this.reload()
  }

  ngAfterViewInit() {
    this.case.isPrivateRepliesValid = this.myFrm.valid
    this.changeRef.detectChanges()
  }

  async reload(local: boolean = true) {
    if (!this.case.privateRepliesConfig) this.case.privateRepliesConfig = []
    this.case.privateRepliesConfig.forEach(c => c.id = guid.raw())
    this.changeRef.detectChanges()
  }

  formChange() {
    if (!this.canEmmitChange && !this.myFrm.dirty) return
    this.case.isPrivateRepliesValid = this.myFrm.valid
    if(this.case.isResponsePrivateReplies && this.case.privateRepliesConfig) {
      this.case.privateRepliesConfig.forEach(c => {
        if(!c.isValid) {
          this.log('config not valid', c)
          this.case.isPrivateRepliesValid = false
          return
        }
      })
    }
    this.change.emit(this.case)
    this.changeRef.detectChanges()
  }

  enablePrivateRepliesResponse(enable: boolean) {
    this.case.isResponsePrivateReplies = enable
    if (enable && !this.case.responsePrivateReplies) {
      this.case.responsePrivateReplies = ""
    }
    this.formChange()
    this.changeRef.detectChanges()
  }

  async addConfig() {
    const type = await this.postCasePortal.openConfigTypeDialog()
    if (type) {
      const config: iInteractPostPrivateReplyConfig = {
        id: guid.raw(),
        type: type,
        key: {
          regex: ""
        },
        value: undefined
      }
      this.case.privateRepliesConfig.push(config)
      this.formChange()
    }
    this.changeRef.detectChanges()
  }

  async removeConfig(config: iInteractPostPrivateReplyConfig) {
    remove(this.case.privateRepliesConfig, c => c.id === config.id)
    this.formChange()
    this.changeRef.detectChanges()
  }

}
