import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  Host,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from "@angular/core";
import { BaseComponent } from "app/base.component";
import { NgForm } from "@angular/forms";
import { PostCasePortalComponent } from "app/chatgut-app/portals/post-case-portal/post-case-portal.component";
import { MatDialog } from "@angular/material";
import { iSelectionData, SelectionDialog } from "app/modals/selection/selection.component";
import { isObject } from "lodash-es";
import { iInteractPostCase } from "app/services/chatbot/api/crud/interactPost";
import { iIntent, iPage } from "app/services/chatbot/api/crud/page";

@Component({
  selector: "app-post-case-type",
  templateUrl: "./post-case-type.component.html",
  styleUrls: ["./post-case-type.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PostCaseTypeComponent extends BaseComponent implements OnInit {
  @Input() case: iInteractPostCase;
  @Output() change = new EventEmitter<iInteractPostCase>();
  @ViewChild("myFrm", { read: NgForm }) myFrm: NgForm;
  constructor(
    @Host() public postCasePortal: PostCasePortalComponent,
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef
  ) {
    super("PostCaseTypeComponent");
  }
  canEmmitChange: boolean = false;
  selectedIntent: iIntent;
  caseTypes = [
    { value: "fail", display: "Bất kỳ" },
    { value: "regex", display: "Nhập chuỗi" },
    { value: "number", display: "Nhập số" },
    { value: "range", display: "Nhập khoảng số" },
    { value: "overLimit", display: "Đạt giới hạn số lần" },
    { value: "overAmount", display: "Đạt giới hạn số lượng" },
    { value: "premature", display: "Trước ngày lên lịch" },
    { value: "expired", display: "Sau ngày lên lịch" },
    { value: "tagLimit", display: "Chưa đặt số lượng tag" },
  ];
  intents: iIntent[] = [];

  async ngOnInit() {
    this.subscriptions.push(this.myFrm.valueChanges.subscribe(this.formChange.bind(this)));
    setTimeout(() => {
      this.canEmmitChange = true;
      this.changeRef.detectChanges();
    }, 500);
    this.reload();
  }

  ngAfterViewInit() {
    this.case.isTypeValid = this.myFrm.valid;
    this.changeRef.detectChanges();
  }

  async reload(local: boolean = true) {
    const { app } = this.postCasePortal.chatbotApi.chatbotAuth;
    let pageId = isObject(app.activePage) ? (app.activePage as iPage)._id : (app.activePage as string);
    const page = await this.postCasePortal.chatbotApi.page.getItem(pageId, { local: true });
    if (page.ai) {
      this.changeRef.detectChanges();
      this.intents = await this.postCasePortal.chatbotApi.page.getIntents({ local });
      if (this.case.value.intentId) {
        this.selectedIntent = this.intents.find((i) => i.id === this.case.value.intentId);
      }
      this.caseTypes.push({ value: "intent", display: "Sử dụng AI" });
      this.changeRef.detectChanges();
    }
  }

  formChange() {
    if (!this.canEmmitChange && !this.myFrm.dirty) return;
    this.case.isTypeValid = this.myFrm.valid;
    this.change.emit(this.case);
  }

  async selectIntent() {
    let data: iSelectionData = {
      title: "Chọn ngữ cảnh",
      confirmButtonText: "Chọn",
      cancelButtonText: "Quay lại",
      categories: [
        {
          name: "Các ngữ cảnh",
          options: this.intents.map((i) => {
            return { code: i.id, display: i.name };
          }) as any,
        },
      ],
    };
    let dialogRef = this.dialog.open(SelectionDialog, { width: "600px", data: data });
    const intentId = await dialogRef.afterClosed().toPromise();
    this.selectedIntent = this.intents.find((i) => i.id === intentId);
    this.case.value.intentId = intentId;
  }

  caseName(type: string) {
    const option = this.caseTypes.find((c) => c.value == type);
    return option.display;
  }
}
