import { isObject } from 'lodash-es';
import { Component, OnInit, Input, EventEmitter, Output, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { iSelectionData, SelectionDialog } from 'app/modals/selection/selection.component';
import { MatDialog } from '@angular/material';
import { iInteractPostCase, iInteractPost } from 'app/services/chatbot/api/crud/interactPost';
import { iPage } from 'app/services/chatbot/api/crud/page';
@Component({
  selector: 'app-post-case-portal',
  templateUrl: './post-case-portal.component.html',
  styleUrls: ['./post-case-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostCasePortalComponent extends BasePortal implements OnInit {

  componentName = "PostCasePortalComponent"
  @Input() case: iInteractPostCase
  @Output() onChange = new EventEmitter<iInteractPostCase>()
  constructor(
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef
  ) {
    super()
    this.setPortalName("Trường hợp")
  }
  interactPost: iInteractPost
  subscribers: any[] = []

  async ngOnInit() {
    this.reload()
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  get caseName() {
    switch (this.case.type) {
      case "fail": return "Bất kỳ";
      case 'regex': return "Nhập chuỗi";
      case 'number': return "Nhập số";
      case 'range': return "Nhập khoảng số";
      case 'intent': return "Sử dụng AI";
    }
  }

  async reload(local: boolean = true) {
    this.showLoading()
    try {
      this.detectChanges()
      await this.getCommentReplies()
    } finally {
      this.hideLoading()
      this.detectChanges()
    }
  }

  async saveContent() {
    const validChecks = ["isTypeValid", "isCommentValid", "isPrivateRepliesValid", "isStoryValid", "isOptionalValid"]
    for (let valid of validChecks) {
      if (!this.case[valid]) {
        this.alert.info('Không hợp lệ', 'Các trường nhập vào có thể bị thiếu hoặc không hợp lệ')
        return;
      }
    }
    try {
      this.showSaving()
      const index = this.interactPost.cases.findIndex(c => c.id === this.case.id)
      this.interactPost.cases[index] = this.case
      this.detectChanges()
      await this.chatbotApi.interactPost.update(this.interactPost._id, this.interactPost)
      this.showSaveSuccess();
      this.onChange.emit(this.case)
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lưu");
      // this.alert.error('Lỗi khi lưu', err.message)
      this.showSaveButton();
    } finally {
      this.detectChanges()
    }
  }

  getStory(storyId?: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      const componentRef = await this.portalService.pushPortalAt(this.index + 1, "StoriesPortalComponent", {
        select: true, selectedStories: storyId, multi: false
      })
      if (!componentRef) {
        return;
      }
      const component = componentRef.instance as any
      this.subscriptions.onStoriesSaved = component.onStorySaved.subscribe((storyId: string) => {
        resolve(storyId)
        component.hideSave();
        component.close();
      })
      this.subscriptions.onStoriesClosed = component.onStoryClosed.subscribe(storyId => {
        component.onStorySaved.unsubscribe();
        component.onStoryClosed.unsubscribe();
      })
    })
  }

  async openConfigTypeDialog() {
    let data: iSelectionData = {
      title: 'Thêm trường hợp bắt Comment',
      confirmButtonText: 'Thêm',
      cancelButtonText: 'Quay lại',
      categories: [{
        name: "Loại trường hợp",
        options: [
          { code: 'fail', display: 'Nhập bất kỳ', icon: "fas fa-random" },
          { code: 'regex', display: 'Nhập chuỗi', icon: "fas fa-font" },
          { code: 'number', display: 'Nhập số', icon: "fas fa-sort-numeric-down" },
          { code: 'range', display: 'Nhập khoảng số', icon: "fas fa-sliders-h" },
          { code: 'premature', display: 'Trước ngày lên lịch', icon: "fas fa-clock" },
          { code: 'expired', display: 'Sau ngày lên lịch', icon: "fas fa-clock" }
        ]
      }]
    };

    const { app } = this.chatbotApi.chatbotAuth
    let pageId = isObject(app.activePage) ? (app.activePage as iPage)._id : app.activePage as string
    const page = await this.chatbotApi.page.getItem(pageId, { local: true })
    if (page.ai) {
      data.categories[0].options.push({
        code: 'intent',
        display: 'Sử dụng AI',
        icon: "fab fa-android"
      })
    }
    let dialogRef = this.dialog.open(SelectionDialog, {
      width: '600px',
      data: data
    });
    return await dialogRef.afterClosed().toPromise()
  }

  async getCommentReplies(local: boolean = true) {
    const commentReplies = await this.chatbotApi.interactPost.getCommentReplies(this.interactPost._id, local)
    this.subscribers = [];
    if (commentReplies) {
      commentReplies.filter(c => {
        return c.repData.length > 0 && c.repData[0].case == this.case._id
      }).forEach(rep => {
        let sub = this.subscribers.find(sub => sub.uid == rep.uid)
        if (sub) {
          sub.amount++;
        } else {
          this.subscribers.push({
            uid: rep.uid,
            name: rep.userName,
            amount: 1,
          })
        }
      })
    }
  }
}