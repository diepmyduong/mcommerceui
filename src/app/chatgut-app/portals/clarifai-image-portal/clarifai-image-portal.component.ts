import { Component, OnInit, Input, ViewChild, Output, EventEmitter, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { MatDialog } from '@angular/material';
import { iClarifaiImage } from 'app/services/chatbot';
import { JsonEditorOptions, JsonEditorComponent } from 'ang-jsoneditor';
import { iEditInfoDialogData, EditInfoDialog } from 'app/modals/edit-info/edit-info.component';

@Component({
  selector: 'app-clarifai-image-portal',
  templateUrl: './clarifai-image-portal.component.html',
  styleUrls: ['./clarifai-image-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClarifaiImagePortalComponent extends BasePortal implements OnInit {

  @Input() clarifaiImageId: string
  @Output() onClarifaiImageDeleted: EventEmitter<any> = new EventEmitter()
  @ViewChild(JsonEditorComponent) editor: JsonEditorComponent;
  constructor(
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef,
  ) {
    super();
    this.setPortalName('Ablum ảnh');
    this.configJsonEditor()
  }
  componentName = "ClarifaiImagePortalComponent"
  clarifaiImage: iClarifaiImage = { images: [], metaData: {} } as any
  formData: any = {
    url: "https://i.imgur.com/qgxSCqD.png",
    metadata: {}
  }
  images: any
  selectedStory: any
  jsonEditorConfig: JsonEditorOptions = new JsonEditorOptions()
  data: any = {}
  swiperOptions = {
    pagination: {
      el: '.clarifai-image-photos-pagination',
      type: 'bullets',
      clickable: true,
      dynamicBullets: true,
    },
    slidesPerView: 1,
    centeredSlides: true,
    navigation: {
      nextEl: '.clarifai-image-photos-next',
      prevEl: '.clarifai-image-photos-prev',
    },
    spaceBetween: 10,
    simulateTouch: false
  }
  configJsonEditor() {
    this.jsonEditorConfig.mode = 'code'
    let deboud;
    this.jsonEditorConfig.onChange = () => {
      if (deboud) clearTimeout(deboud);
      deboud = setTimeout(this.onJSONChange.bind(this), 250);
    }
  }
  onJSONChange() {
    try {
      this.clarifaiImage.metaData = this.editor.get()
      this.showSaveButton()
    } catch (err) {
      return;
    } finally {
      this.detectChanges();
    }
  }
  ngAfterViewInit() {
    this.data = this.clarifaiImage.metaData;
    this.detectChanges();
  }
  async ngOnInit() {
    await this.reload();
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }
  async reload(local: boolean = true) {
    this.showLoading();
    this.detectChanges();
    try {
      this.clarifaiImage = await this.chatbotApi.clarifaiImage.getItem(this.clarifaiImageId) as any
      this.images = this.clarifaiImage.images
      this.data = this.clarifaiImage.metaData
    } catch (err) {
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi lấy dữ liệu clarifaiImage", err.message)
    } finally {
      this.hideLoading();
      this.detectChanges();
    }
  }
  async saveContent() {
    try {
      this.showSaving();
      this.detectChanges();
      this.clarifaiImage = await this.chatbotApi.clarifaiImage.update(this.clarifaiImage._id, this.clarifaiImage);
      this.showSaveSuccess()
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lưu");
      // this.alert.error('Không thể cập nhật', err.message)
      // this.topicInfo.cardFrm.form.enable()
      this.showSaveButton()
    } finally {
      this.detectChanges();
    }
  }
  async onImageSourceChanged(event) {
    this.formData.url = event
  }


  async uploadImage(event) {
    event.preventDefault()
    const result: any = await this.openUploadImageDialog()
    console.log("result: ", result)
    if (result) {
      this.showLoading()
      this.detectChanges();
      try {
        const clarifaiImage = await this.chatbotApi.clarifaiImage.upload({ clarifaiImageId: this.clarifaiImageId, url: result.url, metadata: {} })
        this.alert.success("Thành công", "Upload ảnh lên thành công")
        this.clarifaiImage.images = clarifaiImage.images
        this.detectChanges();
        this.reload(true)
      } catch (err) {
        this.alert.handleError(err, "Lỗi khi upload ảnh");
        // this.alert.error("Upload ảnh lên thất bại", err.message)
      } finally {
        this.hideLoading()
        this.detectChanges();
      }
    }
  }
  openUploadImageDialog() {
    let data: iEditInfoDialogData = {
      title: "Thêm ảnh mới",
      confirmButtonText: "Thêm ảnh",
      confirmButtonClass: 'i-create-story-confirm-button',
      cancelButtonText: "Quay lại",
      inputs: [
        {
          title: "Link ảnh",
          property: "url",
          type: "text",
          required: true,
          className: 'i-story-title-input'
        }
      ]
    };

    return this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    }).afterClosed().toPromise();
  }
  async openImage(image) {
    const componentRef = await this.portalService.pushPortalAt(this.index + 1, "ClarifaiImageItemPortalComponent", { image, clarifaiImageId: this.clarifaiImageId })
    if (!componentRef) return;
    let component = componentRef.instance as any
    this.subscriptions.ImageUpdated = component.onImageUpdated.subscribe(image => {
      let imageIndex = this.clarifaiImage.images.findIndex((item: any) => { return item.clarifaiId == image.clarifaiId })
      this.clarifaiImage.images[imageIndex].metadata = image.metadata
      this.detectChanges();
    })

    this.subscriptions.ImageDeleted = component.onImageDeleted.subscribe((image) => {
      let imageIndex = this.clarifaiImage.images.findIndex((item: any) => { return item.clarifaiId == image.clarifaiId })
      this.clarifaiImage.images.splice(imageIndex, 1)
      this.detectChanges();
    })
  }
  async onUploading(event) {

  }
  async deleteClarifaiImage() {
    if (!await this.alert.warning('Xoá Album', "Bạn có muốn xoá album này không?", 'Xóa Ablum')) return;
    this.showLoading()
    this.detectChanges();
    try {
      await this.chatbotApi.clarifaiImage.delete(this.clarifaiImageId);
      this.alert.success("Thành không", "Xoá thành công")
      this.onClarifaiImageDeleted.emit(this.clarifaiImage)
      this.portalService.popPortal(this.index)
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi xóa ảnh");
      // this.alert.error('Không thể xoá', err.message)
    } finally {
      this.hideLoading()
      this.detectChanges();
    }
  }
  async selectStory() {
    try{
      this.detectChanges();
      const storyId = await this.getStory(this.clarifaiImage.story as string)
      if (storyId != this.clarifaiImage.storyId) {
        this.clarifaiImage.story = storyId
        this.showSaveButton()
      }
    } catch (err){
      this.alert.handleError(err, "Lỗi khi chọn câu chuyện");
      // this.alert.error("Lỗi khi chọn câu chuyện", err.message)
    } finally {
      this.detectChanges();
    }
  }

  getStory(storyId?: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      const componentRef = await this.portalService.pushPortalAt(this.index + 1, 'StoriesPortalComponent', {
        select: true, multi: false, selectedStories: storyId
      })
      if (!componentRef) return;

      const component = componentRef.instance as any
      this.subscriptions.onStoriesSaved = component.onStorySaved.subscribe((storyId: string) => {
        resolve(storyId)
        component.hideSave();
        component.close();
      })
      this.subscriptions.onStoriesClosed = component.onStoryClosed.subscribe(storyId => {
        component.onStorySaved.unsubscribe();
        component.onStoryClosed.unsubscribe();
      })
    })
  }

}
