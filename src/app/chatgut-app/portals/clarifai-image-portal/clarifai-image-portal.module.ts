import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarifaiImagePortalComponent } from './clarifai-image-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatIconModule, MatToolbarModule, MatProgressBarModule, MatGridListModule } from '@angular/material';
import { MaterialModule } from 'app/shared';
import { CardImageModule } from 'app/shared/cards/card-image/card-image.module';
import { NgJsonEditorModule } from 'ang-jsoneditor';
import { SwiperModule } from 'angular2-useful-swiper';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatToolbarModule,
    MatProgressBarModule,
    MaterialModule,
    CardImageModule,
    NgJsonEditorModule,
    MatGridListModule,
    SwiperModule,
    EditInfoModule
  ],
  declarations: [ClarifaiImagePortalComponent],
  entryComponents: [ClarifaiImagePortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ClarifaiImagePortalComponent }
  ]
})
export class ClarifaiImagePortalModule { }
