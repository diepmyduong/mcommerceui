import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GuidePortalComponent } from 'app/chatgut-app/portals/guide-portal/guide-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { SafePipeModule } from 'app/shared/pipes/safe-pipe/safe-pipe.module';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatProgressBarModule,
    MatToolbarModule,
    SafePipeModule
  ],
  declarations: [GuidePortalComponent],
  entryComponents: [GuidePortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: GuidePortalComponent }
  ]
})
export class GuidePortalModule { }
