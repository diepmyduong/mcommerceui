import { Component, OnInit, Input } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { environment } from '@environments';
import { iPage } from 'app/services/chatbot/api/crud/page';
@Component({
  selector: 'app-guide-portal',
  templateUrl: './guide-portal.component.html',
  styleUrls: ['./guide-portal.component.scss']
})
export class GuidePortalComponent extends BasePortal implements OnInit {
  page: iPage
  pageId: any
  componentName = "GuidePortalComponent"

  @Input() slug: string
  constructor() {
    super()
    this.setPortalName("Hướng dẫn")
  }
  source: string;

  async ngOnInit() {
    this.source = `${environment.chatbot.documentHost}/${this.slug || ""}`
  }
}
