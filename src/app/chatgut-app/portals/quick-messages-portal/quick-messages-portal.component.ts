declare const chrome: any;
import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MatDialog } from '@angular/material';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { iQuickMessage } from 'app/services/chatbot/api/crud/quickMessage';
import { iEditInfoDialogData, EditInfoDialog } from 'app/modals/edit-info/edit-info.component';
import { iTopicQuickMessage } from 'app/services/chatbot/api/crud/topicQuickMessage';
import { BehaviorSubject } from 'rxjs';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { QuickMessageTextDialog, QuickMessageTextData } from './quick-message-text-dialog/quick-message-text-dialog.component';

@Component({
  selector: 'app-quick-messages-portal',
  templateUrl: './quick-messages-portal.component.html',
  styleUrls: ['./quick-messages-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuickMessagesPortalComponent extends BasePortal implements OnInit {

  constructor(
    public dialog: MatDialog,
    public popoverService: PopoverService,
    public changeRef: ChangeDetectorRef
  ) {
    super();
    this.setPortalName('Tin nhắn nhanh');
  }
  componentName = "QuickMessagesPortalComponent"

  loading$ = new BehaviorSubject<boolean>(false)
  loadDone$ = new BehaviorSubject<boolean>(false)

  quickMessages: iQuickMessage[] = []
  topicQuickMessages$ = new BehaviorSubject<iTopicQuickMessage[]>(null)
  set topicQuickMessages(val) { this.topicQuickMessages$.next(val) }
  get topicQuickMessages() { return this.topicQuickMessages$.getValue() }
  story_filter = ''

  colors = [
    { code: '#29C5FF', display: 'Xanh lơ' },
    { code: '#0287D0', display: 'Xanh dương' },
    { code: '#004790', display: 'Xanh biển' },
    { code: '#fa8072', display: 'Đỏ cá hồi' },
    { code: '#f24b3a', display: 'Đỏ tươi' },
    { code: '#B71C0C', display: 'Đỏ lựu' },
    { code: '#3EDC81', display: 'Xanh lá cây' },
    { code: '#30b848', display: 'Xanh lục' },
    { code: '#006C11', display: 'Xanh quân đội' },
    { code: '#F9BF3B', display: 'Cam cà rốt' },
    { code: '#F1892D', display: 'Cam quả cam' },
    { code: '#C86400', display: 'Cam bí ngô' },
    { code: '#AB69C6', display: 'Tím nho' },
    { code: '#7E349D', display: 'Tím hoàng gia' },
    { code: '#4E046D', display: 'Tím mực' },
    { code: '#FFA27B', display: 'Da người nhạt' },
    { code: '#E3724B', display: 'Da người trung' },
    { code: '#B3421B', display: 'Da người đậm' },
    { code: '#47EBE0', display: 'Ngọc trời' },
    { code: '#17BBB0', display: 'Ngọc bích' },
    { code: '#008B80', display: 'Ngọc lam' },
    { code: '#FF8CC8', display: 'Hồng phấn' },
    { code: '#EA4C88', display: 'Hồng tươi' },
    { code: '#BA1C58', display: 'Hồng nhung' },
    { code: '#EAB897', display: 'Nâu be' },
    { code: '#AE7C5B', display: 'Nâu trầm' },
    { code: '#8E5C3B', display: 'Nâu đất' },
    { code: '#ACBAC9', display: 'Màu bạc' },
    { code: '#8C9AA9', display: 'Màu sắt' },
    { code: '#3C4A59', display: 'Màu bão' },
  ]

  async ngOnInit() {
    await this.reload();
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(local: boolean = true) {
    this.loadDone$.next(false)
    try {
      let tasks = []
      tasks.push(this.chatbotApi.quickMessage.getList({ 
        local, query: { 
          limit: 0,
          fields: '_id message option type topicQuickMessage',          
          filter: { page: (this.chatbotApi.chatbotAuth.app.activePage as any)._id} 
        } 
      }).then(res => {
        this.quickMessages = res
      }))
      tasks.push(this.chatbotApi.topicQuickMessage.getList({ 
        local, query: { 
          limit: 0, 
          fields: 'name _id color',
          order: { createdAt: 1 },
          filter: { page: (this.chatbotApi.chatbotAuth.app.activePage as any)._id}
        } 
      }).then(res => {
        this.topicQuickMessages = res
      }))
      await Promise.all(tasks)
      for (let topicQuickMessage of this.topicQuickMessages) {
        topicQuickMessage.quickMessages = this.quickMessages
        .filter(x => x.topicQuickMessage == topicQuickMessage._id)
        .sort((a, b) => a.message < b.message?-1:1)
        topicQuickMessage.expanded = true
      }
      if (this.quickMessages.length || this.topicQuickMessages.length) {
        this.topicQuickMessages = [
          { 
            _id: "0",
            name: 'Mặc định',
            isDefault: true,
            expanded: true,
            quickMessages: this.quickMessages.filter(x => !x.topicQuickMessage)
          },
          ...this.topicQuickMessages
        ]
      }
    } catch (err) {
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi lấy dữ liệu", err.message)
      // console.error("error: ", err.error);
    } finally {
      this.loadDone$.next(true)
    }
  }
  
  async openMessage(event, topic: iTopicQuickMessage = null, message: iQuickMessage = null) {
    let formOptions: iPopoverFormOptions = {
      parent: topic?'.portal-content-block':'.swiper-slide',
      target: topic?(message?'.btn-story':'.story-item'):'.btn-blue',
      targetElement: event.target,
      width: '96%',
      confirm: 'Tạo tin nhắn nhanh',
      horizontalAlign: 'center',
      verticalAlign: message?'top':'bottom',
      fields: message?[
        {
          placeholder: 'Tên tin nhắn nhanh (dạng hashtag)',
          property: 'message',
          constraints: ['required', 'hashtag'],
          current: message.message
        }
      ]:[
        {
          placeholder: 'Tên tin nhắn nhanh (dạng hashtag)',
          property: 'message',
          constraints: ['required', 'hashtag'],
          current: '#'
        }, {
          placeholder: 'Loại tin nhắn',
          property: 'type',
          type: 'select',
          constraints: ['required'],
          options: [
            { code: 'text', display: 'Văn bản & Hình ảnh' },
            { code: 'story', display: 'Câu chuyện' },
          ],
          current: 'text'
        }
      ],
      onSubmit: async (data) => {
        if (!data.message) return
        try {
          this.loading$.next(true)
          if (message) {
            message.message = data.message
            await this.chatbotApi.quickMessage.update(message._id, message)
          } else {
            let { message, type } = data
            let newMessage: iQuickMessage = { 
              message, type, option: type=='text'?{ text: '' }:{ storyId: '' },
              // page: (this.chatbotApi.chatbotAuth.app.activePage as any)._id,
              topicQuickMessage: topic?topic._id:undefined,
            }
            newMessage = await this.chatbotApi.quickMessage.add(newMessage)
            if (topic) {
              topic.quickMessages.push(newMessage)
            } else {
              this.topicQuickMessages[0].quickMessages.push(newMessage)
            }            
          }

          this.topicQuickMessages = [...this.topicQuickMessages]
        } catch (err) {
          this.alert.handleError(err, "Lỗi khi tạo tin nhắn nhanh");
        } finally {
          this.loading$.next(false)
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  async removeMessage(message: iQuickMessage, topic: iTopicQuickMessage, index) {    
    if (!await this.alert.warning('Xoá tin nhắn nhanh', `Bạn có muốn xoá tin nhắn nhanh này không?.`, 'Xoá tin nhắn nhanh')) return;

    try {
      await this.chatbotApi.quickMessage.delete(message._id)
      this.quickMessages.splice(this.quickMessages.findIndex(x => x._id == message._id), 1)
      topic.quickMessages.splice(index, 1)
      this.topicQuickMessages = [...this.topicQuickMessages]
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi xóa");
    }
  }
  
  async openTopic(event, topic: iTopicQuickMessage = null, center = false) {
    let formOptions: iPopoverFormOptions = {
      parent: topic?'.portal-content-block':'.swiper-slide',
      target: topic?'.story-group-header':(center?'.button-remove-overlay':'.btn-blue'),
      targetElement: event.target,
      width: '96%',
      confirm: 'Tạo nhóm tin nhắn',
      horizontalAlign: 'center',
      verticalAlign: topic?'top':(center?'center':'bottom'),
      fields: [
        {
          placeholder: 'Tên nhóm tin nhắn',
          property: 'name',
          constraints: ['required'],
          current: topic?topic.name:''
        }, {
          placeholder: 'Màu sắc',
          property: 'color',
          type: 'select',
          extra: 'color',
          constraints: ['required'],
          options: (topic && !this.colors.find(x => x.code == topic.color))?[{ code: topic.color, display: topic.color }, ...this.colors]:this.colors,
          current: topic?topic.color:''
        }
      ],
      onSubmit: async (data) => {
        if (!data.name) return
        try {
          this.loading$.next(true)
          if (topic) {
            topic.name = data.name
            topic.color = data.color
            await this.chatbotApi.topicQuickMessage.update(topic._id, topic)
          } else {
            let { name, color } = data
            let newTopic: iTopicQuickMessage = { name, color, page: (this.chatbotApi.chatbotAuth.app.activePage as any)._id }
            newTopic = await this.chatbotApi.topicQuickMessage.add(newTopic)
            newTopic.expanded = true
            newTopic.quickMessages = []
            this.topicQuickMessages.push(newTopic)
          }

          this.topicQuickMessages = [...this.topicQuickMessages]
          if (!topic) {
            setTimeout(() => {
              this.scrollToGroup(this.topicQuickMessages.length - 1)
            })
          }
        } catch (err) {
          this.alert.handleError(err, "Lỗi khi tạo nhóm");
        } finally {
          this.loading$.next(false)
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  scrollToGroup(index) {
    try {
      setTimeout(() => {
        document.getElementsByTagName('app-quick-messages-portal').item(0)
        .getElementsByClassName('story-group-item').item(index).scrollIntoView({ block: 'start', behavior: 'smooth' })
      }, 300)
    } catch (err) {
    } finally {
      this.detectChanges()
    }
  }

  async removeTopic(topic: iTopicQuickMessage, index) {    
    if (!await this.alert.warning('Xoá nhóm tin nhắn', 
    `Bạn có muốn xoá nhóm tin nhắn này không? Tất cả tin nhắn nhanh trong nhóm sẽ bị xoá.`, 'Xoá nhóm')) return;

    try {
      await this.chatbotApi.topicQuickMessage.delete(topic._id)
      this.topicQuickMessages.splice(index, 1)
      this.topicQuickMessages = [...this.topicQuickMessages]
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi xóa");
    }
  }

  async selectStory(message, topic) {
    let storyId = message.option.storyId
    const componentRef = await this.portalService.pushPortalAt(this.index + 1, "StoriesPortalComponent", {
      portalName: "Chọn câu chuyện", select: true, selectedStories: storyId?[storyId]:[], multi: false
    });

    if (!componentRef) {
      return;
    }
    const component = componentRef.instance as any
    message.active = true
    topic.active = true
    component.showSaveButton();
    this.detectChanges()
    this.subscriptions.onStorySaved = component.onStorySaved.subscribe(async storyIds => {
      try {
        component.showSaving()
        this.detectChanges()
        await this.chatbotApi.quickMessage.update(message._id, {
          option: { storyId: storyIds }
        })
        message.option = { storyId: storyIds }
        component.showSaveSuccess();
        component.close()
      } catch (err) {
        this.alert.handleError(err, "Lỗi khi lưu");
        component.showSaveButton();
      }

    })
    this.subscriptions.onStoryClosed = component.onStoryClosed.subscribe(() => {
      message.active = false
      topic.active = false
      this.topicQuickMessages = [...this.topicQuickMessages]
      component.onStorySaved.unsubscribe();
      component.onStoryClosed.unsubscribe();
    })
  }

  openQuickMessageText(message: iQuickMessage) {
    let dialogRef = this.dialog.open(QuickMessageTextDialog, {
      width: '500px',
      data: {
        id: message._id,
        message: message.message,
        text: message.option.text,
        images: message.option.images
      } as QuickMessageTextData
    })

    dialogRef.afterClosed().toPromise().then(res => {
      if (res) {
        message.option = res
      }
    })
  }

  async fileChanged(event) {    
    let files = event.target.files
    if (files.length) {
      let file = files[0]
      
      try {
        await this.chatbotApi.app.importExcelQuickMessage(file)
        this.alert.success('Import tin nhắn nhanh thành công', 'Mẫu tin nhắn nhanh đã được import')
        this.reload(false)
      } catch (err) {
        this.alert.handleError(err)
      } finally {
        event.target.value = ''
      }
    }
  }
}