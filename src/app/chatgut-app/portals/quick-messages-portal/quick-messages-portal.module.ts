import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuickMessagesPortalComponent } from 'app/chatgut-app/portals/quick-messages-portal/quick-messages-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatToolbarModule, MatProgressSpinnerModule, MatListModule, MatIconModule, MatTooltipModule, MatDividerModule, MatButtonModule } from '@angular/material';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { QuickMessageTextDialogModule } from './quick-message-text-dialog/quick-message-text-dialog.module';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatTooltipModule,
    MatButtonModule,
    QuickMessageTextDialogModule
  ],
  declarations: [QuickMessagesPortalComponent],
  entryComponents: [QuickMessagesPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: QuickMessagesPortalComponent }
  ]
})
export class QuickMessagesPortalModule { }
