import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { QuickMessageTextDialog } from './quick-message-text-dialog.component';
import { TextFieldModule } from '@angular/cdk/text-field';
import { MatProgressSpinnerModule, MatDialogModule, MatSnackBarModule } from '@angular/material';
import { CardImageModule } from 'app/shared/cards/card-image/card-image.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    TextFieldModule,
    CardImageModule,
    MatSnackBarModule
  ],
  declarations: [QuickMessageTextDialog],
  entryComponents: [QuickMessageTextDialog],
  exports: [QuickMessageTextDialog]
})
export class QuickMessageTextDialogModule { }
