import { CdkTextareaAutosize } from "@angular/cdk/text-field";
import { ChangeDetectionStrategy, Component, Inject, OnInit, ViewChild } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from "@angular/material";
import { ChatbotApiService } from "app/services/chatbot";
import { BehaviorSubject } from "rxjs";

@Component({
  selector: "quick-message-text-dialog",
  styleUrls: ["./quick-message-text-dialog.component.scss"],
  templateUrl: "./quick-message-text-dialog.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuickMessageTextDialog implements OnInit {
  id: string;
  message: string;
  text: string;
  images$ = new BehaviorSubject<any[]>(undefined);
  set images(val) {
    this.images$.next(val);
  }
  get images() {
    return this.images$.getValue();
  }

  submitting$ = new BehaviorSubject<boolean>(false);
  uploading$ = new BehaviorSubject<boolean>(false);

  @ViewChild(CdkTextareaAutosize) textAreaAutosize: CdkTextareaAutosize;

  constructor(
    public dialogRef: MatDialogRef<QuickMessageTextDialog>,
    public chatbotApi: ChatbotApiService,
    @Inject(MAT_DIALOG_DATA) public data: QuickMessageTextData,
    private snackBar: MatSnackBar
  ) {
    this.id = data.id;
    this.message = data.message;
    this.text = data.text;
    this.images = data.images ? data.images.map((x) => ({ src: x })) : [];
  }
  overlay: Element;

  ngOnInit() {
    this.textAreaAutosize.resizeToFitContent();

    this.overlay = document.getElementsByClassName("cdk-global-overlay-wrapper").item(0);
    this.overlay.classList.add("overflow");
    this.overlay.addEventListener("click", (event) => this.overlayClickEvent(event));
  }

  submit() {
    this.submitting$.next(true);
    let option = {
      text: this.text,
      images: this.images.map((x) => x.src),
    };
    this.chatbotApi.quickMessage
      .update(this.id, { option })
      .then((res) => {
        this.snackBar.open("✔️ Đã cập nhật tin nhắn nhanh", "", { duration: 2000 });
        this.dialogRef.close(option);
      })
      .catch((err) => {
        this.snackBar.open("❌ Lỗi khi cập nhật tin nhắn nhanh", "", { duration: 2000 });
        this.submitting$.next(false);
      });
  }

  moveImage(index, offset) {
    let destination = index + offset;
    if (destination < 0 || destination > this.images.length - 1) return;

    let images = this.images;
    let temp = images[index];
    images[index] = images[destination];
    images[destination] = temp;
    this.images = [...this.images];
  }

  removeImage(index) {
    let images = this.images;
    images.splice(index, 1);
    this.images = [...this.images];
  }

  onImageSourceChanged(event, index) {
    let images = this.images;
    images[index].src = event;
    this.images = [...this.images];
  }

  addImage() {
    let images = this.images;
    images.push({ src: "" });
    this.images = [...this.images];
  }

  ngOnDestroy() {
    this.overlay.removeEventListener("click", (event) => this.overlayClickEvent(event));
  }

  overlayClickEvent(e) {
    if (e.srcElement && e.srcElement.classList.contains("cdk-global-overlay-wrapper")) {
      this.onNoClick();
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}

export interface QuickMessageTextData {
  id: string;
  message: string;
  text: string;
  images: string[];
}
