import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { iReferral, iStory } from 'app/services/chatbot';
import { iDictionary } from 'app/services/chatbot/api/crud/dictionnary';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { groupBy } from 'lodash-es';
import * as moment from 'moment';
import { BehaviorSubject } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-dictionary-portal',
  templateUrl: './dictionary-portal.component.html',
  styleUrls: ['./dictionary-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class DictionaryPortalComponent extends BasePortal implements OnInit {

  componentName = "DictionaryPortalComponent"

  loadDone$ = new BehaviorSubject<boolean>(false)
  loading$ = new BehaviorSubject<boolean>(false)

  keywordGroups$ = new BehaviorSubject<{
    story: iStory,
    keywords: iDictionary[]
  }[]>(undefined)
  get keywordGroups() { return this.keywordGroups$.getValue() }
  set keywordGroups(val) { this.keywordGroups$.next(val) }

  keywords$ = new BehaviorSubject<iDictionary[]>(undefined)
  get keywords() { return this.keywords$.getValue() }
  set keywords(val) { this.keywords$.next(val) }

  stories: iStory[]

  openedStory$ = new BehaviorSubject<string>('')

  filter: string

  constructor (
    public dialog: MatDialog,
    public popoverService: PopoverService,
    private router: Router,
    public changeRef: ChangeDetectorRef
  ) {
    super()
    this.setPortalName('Danh sách từ khóa')
  }

  async ngOnInit() {
    this.reload()
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(local: boolean = true) {
    this.loading$.next(true)
    this.detectChanges()
    try {
      let keywords
      let tasks = []
      tasks.push(this.chatbotApi.dictionary.getList({ local, query: { 
          order: { 'createdAt': 1 }, 
          limit: 0 
      } }).then(res => keywords = res))
      tasks.push(this.chatbotApi.story.getAll().then(res => {
        this.stories = res
      }))
      await Promise.all(tasks)
      this.keywords = keywords
      this.keywordGroups = this.createKeywordGroups(this.keywords)
    } catch (err) {
      this.alert.handleError(err);
    } finally {
      this.loadDone$.next(true)
      this.loading$.next(false)
    }
  }

  createKeywordGroups(keywords) {
    let groupObj = groupBy(keywords, 'stories[0]')
    return Object.keys(groupObj).map(storyId => {      
      let story = this.stories.find(x => x._id == storyId)
      let keys = groupObj[storyId]
      keys.forEach(ref => {
        keys.storyName = story?story.name:'[Không có câu chuyện]'
      })
      return { story, keywords: keys }
    })
  }

  async openKeyword(event, center = false, keyword = null, index = -1) {
    let formOptions: iPopoverFormOptions = {
      parent: '.swiper-slide',
      target: keyword?'.portal-list-item':center?'.btn-create-qrcode':'.btn-blue',
      targetElement: event.target,
      width: '96%',
      confirm: keyword?'Chỉnh sửa từ khóa':'Tạo từ khóa',
      horizontalAlign: 'center',
      verticalAlign: (keyword && index >= 0 && index < this.keywordGroups.length - 1)?'top':'bottom',
      fields: [
        {
          placeholder: 'Từ khóa',
          property: 'key',
          col: 'col-7',
          constraints: ['required'],
          current: keyword ? keyword.key : ''
        }, {
          placeholder: 'Chính xác',
          property: 'exact',
          type: 'checkbox',
          col: 'col-5',
          current: keyword ? (keyword.type == 'text') : false
        }, {
          placeholder: 'Câu chuyện',
          property: 'storyId',
          type: 'story',
          constraints: ['required'],
          current: keyword ? keyword.stories[0] : ''
        }
      ],
      onSubmit: async (data) => {
        if (!data.key || !data.storyId) return
        try {
          this.loading$.next(true)
          let { key, storyId, exact } = data
          if (keyword) {
            let newKeyword = await this.chatbotApi.dictionary.update(keyword._id, {
              key, type: exact?'text':'regex', stories: [storyId]
            })
            let index = this.keywords.findIndex(x => x._id == keyword._id)
            if (index >= 0) {
              let keywords = this.keywords
              keywords[index] = newKeyword
              this.keywords = [...keywords]
            }
          } else {
            const newKeyword = await this.chatbotApi.dictionary.add({
              key, type: exact?'text':'regex', stories: [storyId]
            })
            this.keywords = [...this.keywords, newKeyword]
          }
          this.keywordGroups = this.createKeywordGroups(this.keywords)
        } catch (err) {
          this.alert.handleError(err, 'Lỗi khi tạo từ khóa');
        } finally {
          this.loading$.next(false)
          setTimeout(() => {
            let el = document.getElementById(this.portalId)
            el.scroll({ top: el.scrollHeight, behavior: 'smooth' })
          })
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  async openStory(story) {
    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "StoryDetailPortalComponent", { 
      storyId: story._id, 
      portalName: story.name
    })
    this.openedStory$.next(story._id)

    if (!componentRef) return;
    let component = componentRef.instance as any
    
    component.onClosed.pipe(take(1)).subscribe(() => {
      this.openedStory$.next('')
    })
  }

  async removeKeyword(keyword: iDictionary) {
    if (!await this.alert.warning('Xoá mã QR', "Bạn có muốn xoá mã QR này không?", 'Xóa')) return;
    try {
      this.loading$.next(true)
      await this.chatbotApi.dictionary.delete(keyword._id);
      let keywords = this.keywords
      let index = keywords.findIndex(x => x._id == keyword._id)
      if (index >= 0) {
        keywords.splice(index, 1)
        this.keywords = [...keywords]
      }      
      this.keywordGroups = this.createKeywordGroups(this.keywords)
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi xóa từ khóa");
    } finally {
      this.loading$.next(false)
    }
  }

  trackByFn(index, item) {
    return item._id
  }

}

