import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FormsModule } from '@angular/forms';
import { MatProgressSpinnerModule, MatToolbarModule, MatButtonModule, MatSnackBarModule, MatIconModule } from '@angular/material';
import { CopyModule } from 'app/modals/copy/copy.module';
import { DictionaryPortalComponent } from './dictionary-portal.component';
import { StoryFilterPipeModule } from 'app/shared/pipes/story-filter-pipe/story-filter-pipe.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CopyModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatToolbarModule,
    MatTooltipModule,
    StoryFilterPipeModule,
    MatSnackBarModule,
    MatIconModule,
  ],
  declarations: [DictionaryPortalComponent],
  entryComponents: [DictionaryPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: DictionaryPortalComponent }
  ]
})
export class DictionaryPortalModule { }
