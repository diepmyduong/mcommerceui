import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { iShop } from 'app/services/chatbot/api/crud/shop';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { iHaravan } from 'app/services/chatbot/api/crud/haravan';
import { BehaviorSubject } from 'rxjs';
import { iHaravanCategory } from 'app/services/chatbot/api/crud/haravanCategory';
import { iHaravanProduct } from 'app/services/chatbot/api/crud/haravanProduct';

@Component({
  selector: 'app-haravan-portal',
  templateUrl: './haravan-portal.component.html',
  styleUrls: ['./haravan-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HaravanPortalComponent extends BasePortal implements OnInit {

  componentName = "HaravanPortalComponent"
  
  loading: boolean = false
  loadDone: boolean = false
  hasShopChanged: boolean = false

  loadingProducts$ = new BehaviorSubject<boolean>(false)
  loadingMore$ = new BehaviorSubject<boolean>(false)
  sycning$ = new BehaviorSubject<boolean>(false)

  searchText: string = ''

  categories$ = new BehaviorSubject<iHaravanCategory[]>(undefined)
  set categories(val) { this.categories$.next(val) }
  get categories() { return this.categories$.getValue() }
  products$ = new BehaviorSubject<iHaravanProduct[]>(undefined)
  set products(val) { this.products$.next(val) }
  get products() { return this.products$.getValue() }
  total$ = new BehaviorSubject<number>(0)

  haravan: iHaravan

  constructor(
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef,
    public popoverService: PopoverService
  ) {
    super()
    this.setPortalName('Haravan Shop')
  }

  async ngOnInit() {
    await this.reload()
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(local = true) {
    this.loadDone = false
    this.detectChanges()
    try {

      let tasks = []
      tasks.push(this.chatbotApi.haravan.getList({ local, query: { filter: { page: this.chatbotApi.page.pageId }} }).then(result => {
        this.haravan = result[0]
      }))
      await Promise.all(tasks)
      if (this.haravan) await this.loadProducts(local)

    } catch (err) {
      this.alert.handleError(err)
    } finally {
      this.loadDone = true
      this.detectChanges()
    }
  }

  async loadProducts(local = true) {
    try {
      if (!this.products || !local) this.loadingProducts$.next(true)
      let products
      let tasks = []
      tasks.push(this.chatbotApi.haravanCategory.getList({
        local, query: { filter: { shop: this.haravan._id }, limit: 0 }
      }).then(res => { this.categories = res }))
      tasks.push(this.chatbotApi.haravanProduct.getList({
        local, query: { 
          filter: { shop: this.haravan._id, name: this.searchText?{ $regex: this.searchText }:undefined }, 
          offset: this.products?this.products.length:0, 
          limit: 10 
        }
      }).then(res => {
        products = res
      }))
      await Promise.all(tasks)
      for (let product of products) {
        product.categoryName = this.categories.find(x => x._id == product.category).name
      }
      this.products = [...products]
      this.total$.next(this.chatbotApi.haravanProduct.pagination.totalItems) 
      console.log('here', this.categories, this.products)
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lấy danh sách sản phẩm");
    } finally {
      this.loadingProducts$.next(false)
    }
  }

  async loadMore(local = false) {
    try {
      this.loadingMore$.next(true)
      let products
      let tasks = []
      tasks.push(this.chatbotApi.haravanProduct.getList({
        local, query: { filter: { shop: this.haravan._id }, offset: this.products?this.products.length:0, limit: 10 }
      }).then(res => { products = res }))
      await Promise.all(tasks)
      for (let product of products) {
        product.categoryName = this.categories.find(x => x._id == product.category).name
      }
      this.products = [...this.products, ...products]
      this.total$.next(this.chatbotApi.haravanProduct.pagination.totalItems) 
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lấy danh sách sản phẩm");
    } finally {
      this.loadingMore$.next(false)
    }
  }

  async syncProducts() {
    try {
      this.sycning$.next(true)
      await this.chatbotApi.haravan.triggerProduct()
      this.alert.success('Đồng bộ thành công', 'Đã đồng bộ các sản phẩm từ Haravan')
      this.loadProducts(false)
      console.log('here', this.categories, this.products)
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi đồng bộ sản phẩm từ Harvan");
    } finally {
      this.sycning$.next(false)
    }
  }
  
  async saveContent() {
    try {
      this.showSaving()
      this.detectChanges()
      let tasks = []
      let { domain, username, password } = this.haravan
      tasks.push(this.chatbotApi.haravan.update(this.haravan._id, { domain, username, password }).then(res => {
        if (this.haravan) this.loadProducts(false)
      }))
      await Promise.all(tasks)
      this.showSaveSuccess()
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lưu thông tin haravan");
      this.showSaveButton()
    } finally {
      this.detectChanges()
    }
  }

  async createHaravan(event) {
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      target: '.btn-create',
      targetElement: event.target,
      width: '96%',
      confirm: 'Thêm thông tin',
      horizontalAlign: 'center',
      verticalAlign: 'bottom',
      fields: [
        {
          placeholder: 'Tên miền Haravan',
          property: 'domain',
          constraints: ['required'],
          current: ''
        },{
          placeholder: 'Tên đăng nhập',
          property: 'username',
          constraints: ['required'],
          current: ''
        },{
          placeholder: 'Mật khẩu',
          property: 'password',
          inputType: 'password',
          constraints: ['required'],
          current: ''
        }
      ],
      onSubmit: async (data) => {
        if (!data) return
        try {
          this.loadDone = false
          this.detectChanges()
          let { domain, username, password } = data
          this.haravan = (await this.chatbotApi.haravan.add({ domain, username, password, page: this.chatbotApi.page.pageId }))
          this.loadProducts()
          this.alert.success("Thêm Haravan thành công", "Thông tin Haravan đã được lưu")
          this.loadDone = true
        } catch (err) {
          console.error(err)
          this.alert.handleError(err, "Lỗi khi thêm thông tin Harvan")
        } finally {
          this.detectChanges()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  searchTimeout
  searchChanged() {
    if (this.searchTimeout) clearTimeout(this.searchTimeout)
    this.searchTimeout = setTimeout(() => {
      this.searchTimeout = null
      this.products = undefined
      this.loadProducts()
    }, 500)
  }
  
  trackByFn(index, item) {
    return item._id; // or item.id
  }

  shopChanged() {
    this.showSaveButton()
    this.hasShopChanged = true
    this.detectChanges()
  }
}
