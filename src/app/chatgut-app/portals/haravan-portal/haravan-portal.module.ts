import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MaterialModule } from 'app/shared';
import { HaravanPortalComponent } from './haravan-portal.component';
import { MatTooltipModule, MatProgressSpinnerModule, MatTabsModule, MatButtonModule, MatSelectModule, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { CardImageModule } from 'app/shared/cards/card-image/card-image.module';
import { FormsModule } from '@angular/forms';
import { AccountingPipeModule } from 'app/shared/pipes/accounting-pipe/accounting-pipe.module';
import { AppDateAdapter, APP_DATE_FORMATS } from 'app/shared/date-adapter';
import { StorySelectionModule } from 'app/components/story-selection/story-selection.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    AccountingPipeModule,
    FormsModule,
  ],
  declarations: [HaravanPortalComponent],
  entryComponents: [HaravanPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: HaravanPortalComponent }
  ]
})
export class HaravanPortalModule { }
