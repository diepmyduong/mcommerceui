import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotifyPortalComponent } from 'app/chatgut-app/portals/notify-portal/notify-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';

// Material
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { HightlightTextareaModule } from 'app/directives/hightlight-textarea/hightlight-textarea.module';
import { SubscriberSelectionModule } from 'app/components/subscriber-selection/subscriber-selection.module';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatCheckboxModule,
    MatDividerModule,
    TextFieldModule,
    HightlightTextareaModule,
    SubscriberSelectionModule,
    FormsModule
  ],
  declarations: [NotifyPortalComponent],
  entryComponents: [NotifyPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: NotifyPortalComponent }
  ]
})
export class NotifyPortalModule { }
