import { Component, OnInit, Output, EventEmitter, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { NgForm } from '@angular/forms';
import { iCard, iStory, iSubscriber } from 'app/services/chatbot';
import { uniq } from 'lodash-es'

@Component({
  selector: 'app-notify-portal',
  templateUrl: './notify-portal.component.html',
  styleUrls: ['./notify-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotifyPortalComponent extends BasePortal implements OnInit {
  @Output() onChange = new EventEmitter<any>()
  @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
  constructor(
    public changeRef: ChangeDetectorRef
  ) {
    super();
    this.setPortalName("Thông báo")
  }
  componentName = "NotifyPortalComponent"
  // allowNotify = undefined
  // setting: any
  cards: iCard[]
  stories: iStory[] = []
  subscribers: iSubscriber[] = []
  loadDone: boolean = false
  cardSubscribers = {}
  cardChanged = {}
  storyCards = {}

  async ngOnInit() {
    await this.reload(false)
  }

  async reload(local = true) {
    try {
      this.loadDone = false
      this.stories = []
      this.changeRef.markForCheck()
      this.cards = await this.chatbotApi.card.getList({ local, query: {
        filter: { type: 'action_notify' },
        limit: 0,
        populates: [{path: 'story', select: '_id name cards'}]
      }})
      
      let psids: any[] = []
      for (let card of this.cards) {
        if (!card.story) continue
        let index = this.stories.findIndex(x => x._id == (card.story as iStory)._id)
        if (index == -1) this.stories.push(card.story as iStory)
        if (card.option.psids && card.option.psids.length) {
          psids = [...psids, ...card.option.psids]
        }
      }
      psids = uniq(psids)

      if (psids.length) {
        this.subscribers = await this.chatbotApi.subscriber.getList({ local, query: {
          fields: ["psid", "_id", "messengerProfile"],
          limit: 0,
          filter: { psid: { $in: psids }}
        }})
      } else {
        this.subscribers = []
      }

      for (let card of this.cards) {
        if (card.option.psids && card.option.psids.length) {
          this.cardSubscribers[card._id] = this.subscribers.filter(x => card.option.psids.includes(x.psid))
        } else {
          this.cardSubscribers[card._id] = []
        }
      }

      for (let story of this.stories) {
        this.storyCards[story._id] = []
        for (let id of story.cards) {
          let index = this.cards.findIndex(x => x._id == id)
          if (index >= 0) this.storyCards[story._id].push(this.cards[index])
        }
      }
      console.log(this.stories)
    } catch (err) {
      this.alert.handleError(err)
    } finally {
      this.loadDone = true
      this.changeRef.detectChanges()
    }
  }

  async onSubscriberChanged(event, card) {
    let subscribers = event
    let psids = subscribers.map(x => x.psid)
    card.option.psids = psids
    // this.cardChanged[card._id] = true
    this.chatbotApi.card.update(card._id, card)
    // this.showSaveButton()
  }

  async saveContent() {
      try {
        this.showSaving();
        this.detectChanges()

        let tasks = []
        for (let card of this.cards) {
          if (this.cardChanged[card._id]) {
            tasks.push(this.chatbotApi.card.update(card._id, card))
          }
        }
        await Promise.all(tasks)

        this.showSaveSuccess()
        this.cardChanged = {}
      } catch (err) {
        this.alert.handleError(err, "Lỗi khi lưu");
        this.showSaveButton()
      } finally {
        this.detectChanges()
      }
  }

  async openStory(story: iStory) {
    this.portalService.pushPortalAt(this.index + 1, "StoryDetailPortalComponent", { storyId: story._id })
  }

  // async ngOnDestroy() {
  //   super.ngOnDestroy()

  //   if (this.subscriptions.onRegisNotify) this.subscriptions.onRegisNotify.unsubscribe()
  // }

  // async reload(local: boolean = true) {
  //   this.showLoading();
  //   try {
  //     this.detectChanges()
  //     const users = await this.chatbotApi.notifyUser.getList({ local, query: {
  //       filter: { uid: this.chatbotApi.chatbotAuth.firebaseUser.uid }
  //     }});
  //     this.allowNotify = users.length > 0;
  //     if(this.allowNotify) this.setting = Object.assign({},users[0].options);
  //   } catch (err) {
  //     // console.error('ERROR', err.error.message);
  //     // this.alert.error("Lỗi khi lấy dữ liệu", err.message);
  //     this.alert.handleError(err);
  //   } finally {
  //     this.hideLoading()
  //     this.detectChanges()
  //   }
  // }

  // regisNotify() {
  //   window.open(`https://m.me/${environment.chatbot.notifyPageId}?ref=user-regis-notify=${this.chatbotApi.chatbotAuth.app._id}=${this.chatbotApi.chatbotAuth.app.accessToken}`, '_blank')
  //   setTimeout(() => {
  //     this.reload(false);
  //   }, 5000)
  // }

  // async saveContent() {
  //   try {
  //     this.showSaving();
  //     this.detectChanges()
  //     await this.chatbotApi.notifyUser.setting(this.setting)
  //     await this.reload(false);
  //     this.showSaveSuccess();
  //   } catch (err) {
  //     if (err.code != 'menu-button-below-threshold') {
  //       // console.error(err.error);
  //       this.alert.handleError(err, "Lỗi khi lưu");
  //       // this.alert.error('Có lỗi xảy ra khi lưu dữ liệu', err.message)
  //       this.showSaveButton();
  //     } else {
  //       this.hideSave();
  //     }
  //   } finally {
  //     this.detectChanges()
  //   }
  // }

}

