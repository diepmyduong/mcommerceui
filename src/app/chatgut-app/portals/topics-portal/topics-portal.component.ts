import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { MatDialog } from '@angular/material';

import { iTopic } from 'app/services/chatbot/api/crud/topic';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { Router } from '@angular/router';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';

@Component({
  selector: 'app-topics-portal',
  templateUrl: './topics-portal.component.html',
  styleUrls: ['./topics-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicsPortalComponent extends BasePortal implements OnInit {

  @Input() select: boolean = false
  @Input() selectedTopics: string[] = null
  @Input() multi: boolean = true
  @Output() onTopicSaved = new EventEmitter<any>()
  @Output() onTopicClosed = new EventEmitter<any>()
  @Output() onTopicRefresh = new EventEmitter<any>()
  
  componentName = "TopicsPortalComponent"
  topics: iTopic[] = []
  topic_filter: string = ''
  openedTopic: string
  loadTopicsDone: boolean = false
  loading: boolean = false
  hasSelectionSet: boolean = true; 

  constructor (
    public dialog: MatDialog,
    public popoverService: PopoverService,
    public changeRef: ChangeDetectorRef
  ) {
    super()
    this.setPortalName('Chủ đề')
  }

  async ngOnInit() {
    this.reload()
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
    this.onTopicClosed.emit();
  }

  async reload(local: boolean = true) {
    this.loading = true
    this.detectChanges()
    const { app } = this.chatbotApi.chatbotAuth
    try {
      this.topics = await this.chatbotApi.topic.getList({ local, 
        query: { limit: 0, order: { 'createdAt': 1 } } }) as any
      console.log('topics', this.topics)

      if (this.select) {
        if (this.selectedTopics) {
          this.setSelectedTopics(this.selectedTopics)
        }
      }
    } catch (err) {
      console.error("error: ", err)
      this.alert.error("Lỗi khi lấy dữ liệu", err.message)
    } finally {
      this.loadTopicsDone = true
      this.loading = false
      this.detectChanges()
      this.onTopicRefresh.emit(local)
    }
  }

  async setSelectedTopics(selectedTopicIds: string[]) {
    if (this.hasSelectionSet) return;
    this.hasSelectionSet = true;
    this.selectedTopics = selectedTopicIds;

    this.topics.forEach(t => {
      t.select = selectedTopicIds.includes(t._id)
    })
    this.detectChanges()
  }

  async selectTopic(topic: iTopic) {
    if (this.multi) {
      if (topic.select == undefined) topic.select = true
      else topic.select = !topic.select
    } else {
      if (topic.select) return
      this.topics.forEach(t => {
        t.select = false
      })
      topic.select = true
    }
    this.detectChanges()
    this.showSaveButton()
  }

  async removeTopic(topic) {
    if (!await this.alert.warning('Xoá chủ đề', "Bạn có muốn xoá chủ đề này không", 'Xóa')) return;
    try {
      await this.chatbotApi.topic.delete(topic._id);
      this.portalService.popPortal(this.index + 1);
      this.alert.success("Thành Công", "Xoá thành công");

    } catch (err) {
      this.alert.error("Xoá không thành công", err.message)
      console.error(err.error);
    } finally {
      this.detectChanges()
    }
  }

  async openTopic(topic: iTopic) {
    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "TopicPortalComponent", { 
      topicId: topic._id, portalName: topic.name })
    this.openedTopic = topic._id
    if (!componentRef) return;
    let component = componentRef.instance as any

    this.subscriptions.onTopicClosed = component.onTopicClosed.subscribe(() => {
      this.openedTopic = ''
      this.subscriptions.onTopicClosed.unsubscribe()
      this.detectChanges()
    })
    this.detectChanges()
  }
  async saveContent() {

    this.showSaving();
    if (this.multi) {
      let selectedTopicIds = []
      for (let topic of this.topics) {
        if (topic.select) {
          selectedTopicIds.push(topic._id)
        }
      }      
      this.onTopicSaved.emit(selectedTopicIds)
    } else {
      let selectedTopic = ''
      for (let topic of this.topics) {
        if (topic.select) {
          selectedTopic = topic._id
          break
        }
      }

      this.onTopicSaved.emit(selectedTopic)
    }
  }

  async addTopic(event, center = false) {
    let formOptions: iPopoverFormOptions = {
      parent: '.swiper-slide',
      target: center?'.btn-create-topic':'.btn-blue',
      targetElement: event.target,
      width: '96%',
      confirm: 'Tạo chủ đề',
      horizontalAlign: 'center',
      verticalAlign: 'bottom',
      fields: [
        {
          placeholder: 'Tên chủ đề',
          property: 'name',
          constraints: ['required']
        }
      ],
      onSubmit: async (data) => {
        if (!data.name) return
        try {
          this.loading = true
          this.detectChanges()
                    
          let topic: iTopic = {}
          topic.name = data.name
          topic.intro =  'Giới thiệu chủ đề'
          topic.image = this.chatbotApi.cardBuilder.defaultImage

          for (let item of this.topics) {
            if (topic.name == item.name) {
              this.alert.info("Trùng tên chủ đề", "Tên chủ đề này đã có người tạo")
              return;
            }
          }

          topic = await this.chatbotApi.page.addTopic(topic)
          this.onTopicRefresh.emit(false)

          if (!this.openedTopic) {
            this.openTopic(topic)
          }
        } catch (err) {
          console.error(err)
          this.alert.handleError(err);
        } finally {
          this.loading = false
          this.detectChanges()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  async remveTopic(topic) {
    if (!await this.alert.warning('Xoá chủ đề', "Bạn có muốn xoá chủ đề này không?", 'Xóa')) return;
    try {
      this.loading = true
      this.detectChanges()
      await this.chatbotApi.topic.delete(topic._id)
      if (this.openTopic == topic._id) {
        this.portalService.popPortal(this.index + 1);
      }
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi xóa");
      console.error(err.error);
    } finally {
      this.loading = false
      this.detectChanges()
    }
  }

  trackByFn(index, item) {
    return item._id
  }


}
