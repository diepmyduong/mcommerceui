import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopicsPortalComponent } from 'app/chatgut-app/portals/topics-portal/topics-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatToolbarModule, MatButtonModule, MatProgressSpinnerModule, MatIconModule, MatListModule, MatTooltipModule, MatDividerModule, MatSelectModule, MatLineModule } from '@angular/material';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { NameFilterPipeModule } from 'app/shared/pipes/name-filter-pipe/name-filter-pipe.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatListModule,
    MatTooltipModule,
    MatDividerModule,
    MatSelectModule,
    MatLineModule,
    EditInfoModule,
    NameFilterPipeModule,
    FormsModule
  ],
  declarations: [TopicsPortalComponent],
  entryComponents: [TopicsPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: TopicsPortalComponent }
  ]
})
export class TopicsPortalModule { }
