import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartnerPortalComponent } from 'app/chatgut-app/portals/partner-portal/partner-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatTooltipModule, MatToolbarModule, MatProgressBarModule, MatFormFieldModule, MatButtonModule, MatIconModule, MatInputModule, MatDividerModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';

@NgModule({
  imports: [
    CommonModule,
    MatTooltipModule,
    MatToolbarModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatDividerModule,
    FormsModule,
    ClipboardModule
  ],
  declarations: [PartnerPortalComponent],
  entryComponents: [PartnerPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: PartnerPortalComponent }
  ]
})
export class PartnerPortalModule { }
