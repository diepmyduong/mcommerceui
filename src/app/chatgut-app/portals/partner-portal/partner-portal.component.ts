import { Component, OnInit, Input, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { NgForm } from '@angular/forms'
import { iApp } from 'app/services/chatbot/api/crud/app';
import { iApiKey } from 'app/services/chatbot/api/crud/apiKey';
@Component({
  selector: 'app-partner-portal',
  templateUrl: './partner-portal.component.html',
  styleUrls: ['./partner-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PartnerPortalComponent extends BasePortal implements OnInit {
  @Input() code: string
  @Input() pageId: string
  @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
  constructor(
    public changeRef: ChangeDetectorRef
  ) {
    super()
    this.setPortalName("Partner")
  }
  componentName = "PartnerPortalComponent"
  app: iApp
  apiKeys: iApiKey[] = []

  async ngOnInit() {
    await this.reload()
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(local: boolean = true) {
    try{
      this.showLoading()
      this.app = this.chatbotApi.chatbotAuth.app
      this.detectChanges()
      this.apiKeys = await this.chatbotApi.apiKey.getList({ local })
      
    }catch (err) {
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi lấy dữ liệu apiKey", err.message)
    }finally {
      this.hideLoading()
      this.detectChanges()
    }
    
  }

  async getSecret() {
    try {
      this.detectChanges()
      this.app.appSecret = await this.chatbotApi.app.getSecret()
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lấy secret");
      // this.alert.error('Không thể láy secret', err.message)
    } finally {
      this.detectChanges()
    }
  }

  async revoke(apiKey: iApiKey) {
    const confirm = await this.alert.warning("Chú ý", "Làm mới sẽ vô hiệu hoá token hiện tại", "Làm mới", "Huỷ")
    if (confirm) {
      try {
        this.detectChanges()
        await this.chatbotApi.apiKey.revoke(apiKey._id);
        this.reload(false);
      } catch (error) {
        this.alert.handleError(error, "Lỗi khi làm mới token");
        // this.alert.error("Không thể làm mới token", error.message);
      } finally {
        this.detectChanges()
      }
    }
  }

  async addNewApiKey() {
    try {
      this.detectChanges()
      await this.chatbotApi.app.addApiKey();
      this.reload(false);;
    } catch (err) {
      if(err.error && err.error.message=="Permission Deny"){
          this.alert.info("Không thể thêm key","Tải khoản của bạn không có đủ quyền để thêm key")
      }else{
        this.alert.handleError(err, "Lỗi khi thêm key");
        // this.alert.error("Không thể thêm key", err.message);
        this.detectChanges()
      }
    }
  }

}
