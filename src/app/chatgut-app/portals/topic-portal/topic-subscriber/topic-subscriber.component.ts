import { Component, OnInit, Input, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { iTopic, ChatbotApiService, iSubscriber } from 'app/services/chatbot';
import { NgForm } from '@angular/forms';
import { AlertService } from 'app/services/alert.service';
import { remove }from 'lodash-es'
import { TopicPortalComponent } from 'app/chatgut-app/portals/topic-portal/topic-portal.component';

@Component({
  selector: 'app-topic-subscriber',
  templateUrl: './topic-subscriber.component.html',
  styleUrls: ['./topic-subscriber.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicSubscriberComponent extends BaseComponent implements OnInit {
  @Input() topic: iTopic
  @Input() index: number
  @Input() portal: TopicPortalComponent
  @Input() subscribers: iSubscriber[]
  @Input() mode: string = 'detail'
  @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
  constructor(
    public chatbotApi: ChatbotApiService,
    public alert: AlertService,
    public changeRef: ChangeDetectorRef
  ) {
    super('Topic Subscribers Component')
  }
  createdTopic: iTopic
  subscribersAutocomplete: iSubscriber[] = []
  new_subscriber: any
  loadSubscribersAutocomplete: boolean = false

  async ngOnInit() {
    this.reload()
  }

  async ngAfterViewInit() {

  }

  async ngOnDestroy() {
    if (this.portal.subscriptions.formChanges)
      this.portal.subscriptions.formChanges.unsubscribe()
  }

  async reload(local: boolean = true) {
    this.showLoading()
    try {
    } catch (error) {

    } finally {

    }
    this.hideLoading()
  }
  async showInfo(subscriber: iSubscriber) {
    this.portal.portalService.pushPortalAt(this.index + 1, "SubscriberDetailPortalComponent", { subscriberId: subscriber._id })
    this.changeRef.detectChanges()
  }
  async removeSubscriber(subscriber: iSubscriber, event) {
    event.stopPropagation();
    try {
      remove(this.subscribers, s => s._id == subscriber._id);
      await this.portal.chatbotApi.topic.deleteSubscribersToTopic(this.topic._id, [subscriber._id]);
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi xóa");
      // this.alert.error('Không thể xoá', err.error.message);
      this.subscribers.push(subscriber);
    } 
  }

  async addSubscriber() {

  }

  async newSubscriberChanged() {
    if (!this.new_subscriber || this.new_subscriber.length == 0) {
      this.subscribersAutocomplete = []
    } else {
      if (typeof this.new_subscriber === 'string' || this.new_subscriber instanceof String) {
        this.loadSubscribersAutocomplete = true
        let queryFilterOr = []
        let subscriberWords = this.new_subscriber.trim().split(' ')
        for (let word of subscriberWords) {
          let regex = { $regex: word, $options: "si" }
          queryFilterOr.push({ name: regex })
          queryFilterOr.push({ 'messengerProfile.first_name': regex })
          queryFilterOr.push({ 'messengerProfile.last_name': regex })
        }
        this.chatbotApi.subscriber.getList({ local: true, query: {
          fields: ["name", "messengerProfile.profile_pic", "messengerProfile.first_name", "messengerProfile.last_name", "_id"],
          filter: { $or: queryFilterOr },
          limit: 10
        }}).then(result => {
          this.subscribersAutocomplete = result
          console.log(this.subscribersAutocomplete)
          this.loadSubscribersAutocomplete = false
          this.changeRef.detectChanges()
        }).catch(err => {
          this.loadSubscribersAutocomplete = false
        })
      } else {
        this.new_subscriber = ""
      }
    }
    this.changeRef.detectChanges()
  }

  async subscriberSelected(event) {
    this.new_subscriber = ""
    try {
      this.showLoading()
      await this.chatbotApi.topic.addSubscribersToTopic(this.topic._id, [event.option.value._id]).then()
      await this.portal.reloadSubscribers()
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi thêm khách hàng");
      // this.alert.error('Lỗi khi thêm khách hàng', err.message);
    } finally {
      this.hideLoading()
      this.changeRef.detectChanges()
    }
  }
}
