import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { TopicPortalComponent } from 'app/chatgut-app/portals/topic-portal/topic-portal.component';
import { iTopicEvent, ChatbotApiService } from 'app/services/chatbot';
import { iEditInfoDialogData, EditInfoDialog } from 'app/modals/edit-info/edit-info.component';
import { MatDialog } from '@angular/material';
import { PortalService } from 'app/services/portal.service';

@Component({
  selector: 'app-topic-event',
  templateUrl: './topic-event.component.html',
  styleUrls: ['./topic-event.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicEventComponent extends BaseComponent implements OnInit {

  @Input() topicId: string
  @Input() portal: TopicPortalComponent
  @Output() onEventClick = new EventEmitter<iTopicEvent>()
  constructor(
    public chatbotApi: ChatbotApiService,
    public dialog: MatDialog,
    public portalService: PortalService,
    public changeRef: ChangeDetectorRef
  ) {
    super('Story Referral Component')
  }
  topicEvents: iTopicEvent[] = []

  async ngOnInit() {
    this.reload()
  }

  async reload(local: boolean = true) {
    this.showLoading()
    this.topicEvents = await this.chatbotApi.topicEvent.getList({
      local, query: {
        filter: {
          topic: this.topicId
        }
      }
    })
    this.hideLoading()
    this.changeRef.detectChanges()
  }  
  
  async addEvent() {
    let data: iEditInfoDialogData = {
      title: "Tạo sự kiện",
      confirmButtonText: "Tạo sự kiện mới",
      cancelButtonText: "Quay lại",
      inputs: [
        {
          title: "Tên sự kiện",
          property: "name",
          type: "text",
          required: true,
        }
      ]
    };

    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    });
    dialogRef.afterClosed().toPromise().then(async result => {
      if (result) {        
        let event: iTopicEvent = {
          name: result.name,
          description: '',
          eventData: {
            type: 'story',
            story: ''
          },
          eventSchedule: new Date(),
          channel: 'facebook'
        }
        try {
          //this.showLoading()
          this.portal.showLoading()
          let newEvent = await this.chatbotApi.topic.addEvent(this.topicId, event)
          this.topicEvents.push(newEvent)
          this.openEvent(newEvent)
        } catch (err) {
          this.portal.alert.error("Lỗi", 'Không thể tạo mới')
        } finally {
          //this.hideLoading()
          this.portal.hideLoading()
          this.changeRef.detectChanges()
        }
      }
    })
  }

  async openEvent(event: iTopicEvent) {
    this.onEventClick.emit(event)
    this.portalService.pushPortalAt(this.portal.index + 1, "TopicEventPortalComponent", {
      event: event,
      eventId: event._id,
      topicId: this.topicId
    })
    this.changeRef.detectChanges()
  }
}
