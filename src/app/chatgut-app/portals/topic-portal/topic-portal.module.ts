import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopicPortalComponent } from 'app/chatgut-app/portals/topic-portal/topic-portal.component';
import { TopicEventComponent } from 'app/chatgut-app/portals/topic-portal/topic-event/topic-event.component';
import { TopicInfoComponent } from 'app/chatgut-app/portals/topic-portal/topic-info/topic-info.component';
import { TopicSubscriberComponent } from 'app/chatgut-app/portals/topic-portal/topic-subscriber/topic-subscriber.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatTooltipModule, MatFormFieldModule, MatInputModule, MatDividerModule, MatToolbarModule, MatButtonModule, MatProgressSpinnerModule, MatIconModule, MatProgressBarModule, MatListModule, MatLineModule, MatAutocompleteModule } from '@angular/material';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CardImageModule } from 'app/shared/cards/card-image/card-image.module';
import { TopicGroupComponent } from './topic-group/topic-group.component';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  imports: [
    CommonModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule,
    MatToolbarModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatProgressBarModule,
    MatListModule,
    MatLineModule,
    TextFieldModule,
    EditInfoModule,
    FormsModule,
    FlexLayoutModule,
    CardImageModule,
    MatAutocompleteModule
  ],
  declarations: [TopicPortalComponent, TopicEventComponent, TopicInfoComponent, TopicSubscriberComponent, TopicGroupComponent],
  entryComponents: [TopicPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: TopicPortalComponent }
  ]
})
export class TopicPortalModule { }
