import { Component, OnInit, Input, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { iTopic, ChatbotApiService, iGroup } from 'app/services/chatbot';
import { NgForm } from '@angular/forms';
import { AlertService } from 'app/services/alert.service';
import { TopicPortalComponent } from 'app/chatgut-app/portals/topic-portal/topic-portal.component';

@Component({
  selector: 'app-topic-group',
  templateUrl: './topic-group.component.html',
  styleUrls: ['./topic-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicGroupComponent extends BaseComponent implements OnInit {
  @Input() topic: iTopic
  @Input() index: number
  @Input() portal: TopicPortalComponent
  @Input() groups: iGroup[] = []
  @Input() mode: string = 'detail'
  @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
  constructor(
    public chatbotApi: ChatbotApiService,
    public alert: AlertService,
    public changeRef: ChangeDetectorRef
  ) {
    super('Topic Subscribers Component')
  }
  createdTopic: iTopic
  selectedSubscribers = []

  async ngOnInit() {
    console.log('groups', this.groups)
  }

  async ngAfterViewInit() {

  }

  async ngOnDestroy() {
    if (this.portal.subscriptions.formChanges)
      this.portal.subscriptions.formChanges.unsubscribe()
  }

  async selectGroup(group) {
    if (!group.select) group.select = true
    else group.select = false
    this.portal.showSaveButton()
    this.portal.detectChanges()
    this.changeRef.detectChanges()
  }
}
