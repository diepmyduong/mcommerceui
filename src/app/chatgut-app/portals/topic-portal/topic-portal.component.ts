import { Component, OnInit, Input, EventEmitter, Output, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { PortalService } from 'app/services/portal.service';
import { iTopic } from 'app/services/chatbot/api/crud/topic';
import { iSubscriber } from 'app/services/chatbot/api/crud/subscriber';
import { iGroup } from 'app/services/chatbot';
import { cloneDeep } from 'lodash-es';

@Component({
  selector: 'app-topic-portal',
  templateUrl: './topic-portal.component.html',
  styleUrls: ['./topic-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicPortalComponent extends BasePortal implements OnInit {
  @Input() topicId: string
  @Input() mode: string = 'detail'

  @Output() onTopicCreated = new EventEmitter<iTopic>()
  @Output() onChange = new EventEmitter<iTopic>()
  @Output() onTopicClosed = new EventEmitter<iTopic>()
  constructor(
    public portalService: PortalService,
    public changeRef: ChangeDetectorRef
  ) {
    super()
    
  }
  componentName = "TopicPortalComponent"
  topic: iTopic = {}
  subscribers: iSubscriber[]
  createdTopic: iTopic
  subName = "Chủ đề"
  isUploading: boolean = false
  loadDone = false

  groups: iGroup[]

  async ngOnInit() {
    await this.reload()
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
    this.onTopicClosed.emit();
  }

  async ngAfterViewInit() {
  }

  async reload(local: boolean = true) {
    let tasks = []

    tasks.push(this.chatbotApi.group.getList({ local, query: { order: { createdAt: 1 } }}).then(result => {
      this.groups = cloneDeep(result)
    }))
    tasks.push(this.chatbotApi.topic.getItem(this.topicId, { local }).then(result => {
      this.topic = result
    }))
    tasks.push(this.chatbotApi.topic.getSubscribersOfTopic(this.topicId).then(result => {
      this.subscribers = result
    }))
    await Promise.all(tasks)

    console.log(this.groups)

    this.groups.forEach(group => {
      if ((this.topic.groups as string[]).includes(group._id)) {
        group.select = true
      }
    })
    this.setPortalName(this.topic.name)
    this.loadDone = true;
    console.log(this.topic)
    this.detectChanges()
  }

  async reloadGroup() {
    this.topic = await this.chatbotApi.topic.getItem(this.topicId, { local: false, query: {
      populates: ["groups"]
    } })
    this.detectChanges()
  }

  async reloadSubscribers() {    
    this.subscribers = await this.chatbotApi.topic.getSubscribersOfTopic(this.topic._id)
    this.detectChanges()
  }

  async close() {
    if (this.mode === "add") {
      this.onTopicCreated.emit(this.createdTopic)
    }
    super.close()
    this.detectChanges()
  }

  async createTopic(topic: iTopic) {
    this.createdTopic = topic
    this.close()
    this.detectChanges()
  }

  async saveContent() {
    try {
      this.showSaving()
      this.topic.groups = []
      for (let group of this.groups) {
        if (group.select) {
          (this.topic.groups as string[]).push(group._id)
        }
      }
      this.topic = await this.chatbotApi.topic.update(this.topicId, this.topic)
      this.showSaveSuccess()
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lưu");
      // this.alert.error('Không thể cập nhật', err.message)
      // this.topicInfo.cardFrm.form.enable()
      this.showSaveButton()
    } finally {
      this.detectChanges()
    }
  }

  public _this = this
}
