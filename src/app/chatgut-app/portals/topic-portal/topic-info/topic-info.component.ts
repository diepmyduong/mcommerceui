import { Component, OnInit, Input, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { NgForm } from '@angular/forms';
import { MatSlideToggle } from '@angular/material';
import { ChatbotApiService, iTopic } from 'app/services/chatbot';
import { AlertService } from 'app/services/alert.service';
import { TopicPortalComponent } from 'app/chatgut-app/portals/topic-portal/topic-portal.component';
@Component({
  selector: 'app-topic-info',
  templateUrl: './topic-info.component.html',
  styleUrls: ['./topic-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicInfoComponent extends BaseComponent implements OnInit {

  @Input() topic: iTopic
  @Input() portal: TopicPortalComponent
  @Input() mode: string = 'detail'
  @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
  constructor(
    public chatbotApi: ChatbotApiService,
    public alert: AlertService,
    public changeRef: ChangeDetectorRef
  ) {
    super('Topic Info Component')
  }
  createdTopic: iTopic

  async ngOnInit() {
    // await this.reload()
    setTimeout(() => {this.portal.subscriptions.formChanges = this.cardFrm.valueChanges.subscribe(change => {
      if (!this.portal.isSaving && this.portal.loadDone) {
        this.portal.showSaveButton();
        this.changeRef.detectChanges()
      }
    })}, 200)
  }

  async ngAfterViewInit() {
  }

  async ngOnDestroy() {
    if (this.portal.subscriptions.formChanges)
      this.portal.subscriptions.formChanges.unsubscribe()
  }

  async reload(local: boolean = true) {
    // let topic = await this.chatbotApi.topic.getItem(this.topicId)
    // this.topic = merge({}, topic)
    // this.updateTopicState()
    // this.portal.loadDone = true
  }

  // updateTopicState() {
  //   this.topicState = merge({}, this.topic)
  // }

  // async onSave(formCtrl: NgForm, toggleChange: MatSlideToggleChange) {
  //   if (toggleChange.checked) {
  //     // Disable Change
  //     toggleChange.source.setDisabledState(true)
  //     formCtrl.form.disable()
  //     this.showLoading()
  //     try {
  //       const topic = await this.chatbotApi.topic.update(this.topicId, this.topic)
  //       // await this.chatbotApi.page.activeSetting(setting._id)
  //       formCtrl.form.enable()
  //       this.resetForm(formCtrl, this.topic)
  //       this.updateTopicState()
  //       this.portal.onChange.emit(topic)
  //     } catch (err) {
  //       this.alert.error("Không thể lưu", "Vui lòng thử lại sau")
  //       toggleChange.source.toggle()
  //       toggleChange.source.setDisabledState(false)
  //     } finally {
  //       this.hideLoading()
  //       formCtrl.form.enable()
  //     }

  //   }
  // }

  resetForm(formCtrl: NgForm, topic: iTopic) {
    // formCtrl.resetForm({
    //   text: setting.option[0].text
    // })
  }

  async openSubscriber(){
    
  }

  async onImageSourceChanged(event) {
    this.topic.image = event
    this.portal.showSaveButton()
    this.changeRef.detectChanges()
  }

  async onUploading(event) {
    this.portal.isUploading = event
    this.changeRef.detectChanges()
  }

  async openStatistics() {
    let componentRef = await this.portal.portalService.pushPortalAt(this.portal.index + 1, "StatisticsDetailsResultPortalComponent", { 
      mode: 'topic',
      topicId: this.topic._id, 
      portalName: 'Thống kê ',
      name: this.topic.topicName,
      hasFooter: false
    })
    this.changeRef.detectChanges()
  }

}
