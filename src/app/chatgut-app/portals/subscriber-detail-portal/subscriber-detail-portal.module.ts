import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriberDetailPortalComponent } from 'app/chatgut-app/portals/subscriber-detail-portal/subscriber-detail-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatToolbarModule, MatIconModule, MatProgressBarModule, MatDividerModule, MatChipsModule, MatAutocompleteModule, MatButtonModule, MatTooltipModule, MatProgressSpinnerModule } from '@angular/material';
import { NgJsonEditorModule } from 'ang-jsoneditor';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatProgressBarModule,
    MatDividerModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    NgJsonEditorModule,
    FormsModule,

  ],
  declarations: [SubscriberDetailPortalComponent],
  entryComponents: [SubscriberDetailPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: SubscriberDetailPortalComponent }
  ]
})
export class SubscriberDetailPortalModule { }
