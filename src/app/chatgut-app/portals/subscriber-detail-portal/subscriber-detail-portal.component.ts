import * as countryCode from "app/shared/countries-code";
import * as moment from "moment";
import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from "@angular/core";
import { BasePortal } from "app/chatgut-app/portals/portal-container/base-portal";
import { JsonEditorOptions, JsonEditorComponent } from "ang-jsoneditor";
import { iSubscriber } from "app/services/chatbot/api/crud/subscriber";
import { iPage } from "app/services/chatbot/api/crud/page";
import { environment } from "../../../../environments";
import { get } from "lodash-es";
@Component({
  selector: "app-subscriber-detail-portal",
  templateUrl: "./subscriber-detail-portal.component.html",
  styleUrls: ["./subscriber-detail-portal.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriberDetailPortalComponent extends BasePortal implements OnInit {
  @Input() subscriberId: string;
  @ViewChild("textarea") textarea: ElementRef;
  @ViewChild(JsonEditorComponent) editor: JsonEditorComponent;
  constructor(public changeRef: ChangeDetectorRef) {
    super();
  }
  componentName = "SubscriberDetailPortalComponent";
  subscriber: iSubscriber;
  subscriberState: iSubscriber;
  country: string;
  timezone: string;
  gender: string;
  avatar: string;
  labels = [];
  notes = [];
  phone = "";
  created_at: any;
  updated_at: any;
  display_new_note = false;
  new_note: string = "";
  new_label: string = "";
  allLabels = [];
  subscriber_loaded: boolean = false;
  allLabels_loaded: boolean = false;
  labels_loaded: boolean = false;
  notes_loaded: boolean = false;
  selectable = false;
  pageId: string;
  jsonEditorConfig: JsonEditorOptions = new JsonEditorOptions();
  data = {};
  otherInfoOpened: boolean = false;

  async ngOnInit() {
    this.pageId = (this.chatbotApi.chatbotAuth.app.activePage as iPage)._id;
    this.configJsonEditor();
    this.reload(false);
  }

  async ngOnDestroy() {
    super.ngOnDestroy();
  }
  configJsonEditor() {
    this.jsonEditorConfig.mode = "code";
    let deboud;
    this.jsonEditorConfig.onChange = () => {
      if (deboud) clearTimeout(deboud);
      deboud = setTimeout(this.onJSONChange.bind(this), 250);
      this.detectChanges();
    };
  }
  async onJSONChange() {
    if (this.chatbotApi.chatbotAuth.user.rule == "read") return;
    try {
      const json = this.editor.get();
      if (JSON.stringify(json) != JSON.stringify(this.subscriber.otherInfo)) {
        this.subscriber.otherInfo = json;
        this.showSaveButton();
      }
    } catch (err) {
      this.hideSave();
    } finally {
      this.detectChanges();
    }
  }

  async reload(local = true, subscriberId = null) {
    try {
      if (subscriberId) {
        this.subscriberId = subscriberId;
      }

      if (this.subscriberId) {
        this.subscriber_loaded = false;
        this.labels_loaded = false;
        this.notes_loaded = false;
        this.display_new_note = false;
        this.otherInfoOpened = false;
        this.labels = [];
        this.notes = [];
        this.phone = "";
        this.subscriber = await this.chatbotApi.subscriber.getItem(this.subscriberId, {
          local,
          query: {
            fields: [
              "messengerProfile",
              "phone",
              "status",
              "createdAt",
              "updatedAt",
              "labels",
              "uid",
              "otherInfo",
              "name",
            ],
          },
        });
        await this.reloadSubscriber(this.subscriber);
      }
    } catch (err) {
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi lấy dữ liệu", err.message)
    } finally {
      this.detectChanges();
    }
  }

  async reloadSubscriber(subscriber) {
    this.subscriber = subscriber;

    this.subscriber_loaded = false;
    this.portalName = "";
    this.subName = "Khách hàng";
    this.chatbotApi.label.getAllLabels((this.chatbotApi.chatbotAuth.app.activePage as iPage)._id).then((result) => {
      this.allLabels = result.data;
      if (!this.allLabels) {
        this.allLabels = [];
      }
      this.allLabels_loaded = true;
    });

    if (this.subscriber.otherInfo) {
      this.data = this.subscriber.otherInfo;
      this.jsonEditorConfig.mode = "code";
    } else {
      this.data = null;
    }

    this.setPortalName(
      this.subscriber.name
        ? this.subscriber.name
        : this.subscriber.messengerProfile.first_name + " " + this.subscriber.messengerProfile.last_name
    );
    this.avatar = this.subscriber.messengerProfile.profile_pic;
    switch (this.subscriber.messengerProfile.gender) {
      case "male":
        this.gender = "Nam";
        break;
      case "female":
        this.gender = "Nữ";
        break;
      case "other":
        this.gender = "Khác";
        break;
    }
    const timezone = this.subscriber.messengerProfile.timezone;
    if (timezone) {
      this.timezone = `${moment().utc().utcOffset(timezone).format("HH:mm")}`;
      this.timezone += ` UTC(${timezone >= 0 ? "+" : ""}${Math.abs(timezone) < 10 ? "0" : ""}${
        this.subscriber.messengerProfile.timezone
      })`;
    }
    if (this.subscriber.phone) this.phone = this.subscriber.phone;
    this.country = countryCode.getCountryName(get(this.subscriber, "messengerProfile.locale", "").substring(3, 5));
    this.created_at = this.subscriber.createdAt;
    this.updated_at = this.subscriber.updatedAt;
    //this.allLabels = await this.chatbotApi.label.getAllLabels((this.chatbotApi.chatbotAuth.app.activePage as iPage)._id)
    if (this.subscriber.status == "active") {
      this.chatbotApi.label.getLabelListFromSubscriber(this.subscriber._id).then((result) => {
        this.labels = result;
        if (!this.labels) {
          this.labels = [];
        }
        this.labels_loaded = true;
      });
      this.chatbotApi.adminNote.getAdminNotes(this.subscriber._id).then((result) => {
        this.notes = result.admin_notes;
        if (!this.notes) {
          this.notes = [];
        }
        this.notes_loaded = true;
      });
    } else {
      this.labels_loaded = false;
      this.notes_loaded = false;
    }
    this.subscriber_loaded = true;
    this.detectChanges();
  }

  async addLabel(label_text) {
    let addedLabel = {
      name: label_text,
      id: "0",
    };
    let type;

    // if (this.allLabels.some(elem => elem.name == label_text)) {
    //   type = "current_label"
    // } else {
    //   type = "new_label"
    // }
    try {
      this.labels.splice(0, 0, addedLabel);
      this.new_label = "";
      this.detectChanges();
      let result = await this.chatbotApi.label.addLabel(this.pageId, [this.subscriber._id], label_text);
      addedLabel.id = result.id;
    } catch (err) {
      console.error(err);
      this.alert.handleError(err, "Lỗi khi thêm nhãn");
      // this.alert.error("Lỗi khi thêm nhãn", err.message)
      let index = this.labels.indexOf(addedLabel);
      if (index > -1) {
        this.labels.splice(index, 1);
      }
    } finally {
      this.detectChanges();
    }
  }

  async removeLabel(label_id, label_text) {
    let result;
    let deletedLabel;
    let deletedLabelIndex = 0;
    try {
      deletedLabel = this.labels.find((elem) => elem.id == label_id);
      deletedLabelIndex = this.labels.indexOf(deletedLabel);
      this.labels.splice(deletedLabelIndex, 1);
      this.detectChanges();
      result = await this.chatbotApi.label.removeLabel(this.pageId, [this.subscriber._id], label_text);
    } catch (error) {
      console.error(error.error);
      this.alert.handleError(error, "Lỗi khi xóa nhãn");
      // this.alert.error("Lỗi khi xóa nhãn", error.message)
      this.labels.splice(deletedLabelIndex, 0, deletedLabel);
    } finally {
      this.detectChanges();
    }
  }

  async removeNote(note) {
    let result;
    let deletedNote;
    let deletedNoteIndex = 0;
    try {
      deletedNote = this.notes.find((elem) => elem.id == note.id);
      deletedNoteIndex = this.notes.indexOf(deletedNote);
      this.notes.splice(deletedNoteIndex, 1);
      this.detectChanges();
      result = await this.chatbotApi.adminNote.deleteAdminNote(this.subscriber._id, note.id);
    } catch (error) {
      this.alert.handleError(error, "Lỗi khi xóa note");
      // this.alert.error("Lỗi khi xóa note", error.message)
      this.notes.splice(deletedNoteIndex, 0, deletedNote);
    } finally {
      this.detectChanges();
    }
  }

  async addNote(note_text) {
    let addedNote = {
      body: note_text,
      id: "0",
    };
    try {
      this.notes.splice(0, 0, addedNote);
      this.display_new_note = false;
      this.new_note = "";
      let result = await this.chatbotApi.adminNote.addAdminNote(this.subscriber._id, note_text);
      addedNote.id = result.id;
    } catch (err) {
      console.error(err);
      this.alert.handleError(err, "Lỗi khi thêm note");
      // this.alert.error("Lỗi khi thêm note", err.message)
      let index = this.notes.indexOf(addedNote);
      if (index > -1) {
        this.notes.splice(index, 1);
      }
    }
  }

  async openNoteTextArea() {
    this.display_new_note = true;
    this.textarea.nativeElement.focus();
  }

  async openLiveChat() {
    const url = `${environment.chatbot.livechatHost}/chat/${this.subscriber._id}?token=${this.chatbotApi.chatbotAuth.appToken}`;
    window.open(url, "_blank");
    // const componentRef = await this.portalService.pushPortalAt(this.index + 1, "ChatboxPortalComponent", {
    //   subscriberId: this.subscriber._id,
    //   uid: this.subscriber.uid,
    //   portalName: this.subscriber.messengerProfile.name?this.subscriber.messengerProfile.name:(this.subscriber.messengerProfile.first_name + ' ' + this.subscriber.messengerProfile.last_name)
    // })
  }

  async openProfile() {
    window.open("https://www.facebook.com/" + this.subscriber.uid, "_blank");
  }
  async saveContent() {
    try {
      this.showSaving();
      this.subscriber = await this.chatbotApi.subscriber.update(this.subscriber._id, this.subscriber);
      this.showSaveSuccess();
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lưu");
      // this.alert.error("Lỗi khi lưu", err.message)
      this.hideSave();
    }
  }
}
