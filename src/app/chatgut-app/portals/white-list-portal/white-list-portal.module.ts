import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WhiteListPortalComponent } from 'app/chatgut-app/portals/white-list-portal/white-list-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { DomainComponent } from 'app/chatgut-app/portals/white-list-portal/domain/domain.component';
import { MatListModule, MatLineModule, MatIconModule, MatTooltipModule, MatDividerModule, MatToolbarModule, MatButtonModule, MatProgressSpinnerModule, MatProgressBarModule } from '@angular/material';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';

@NgModule({
  imports: [
    CommonModule,
    MatListModule,
    MatLineModule,
    MatIconModule,
    MatTooltipModule,
    MatDividerModule,
    MatToolbarModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    EditInfoModule
  ],
  declarations: [WhiteListPortalComponent, DomainComponent],
  entryComponents: [WhiteListPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: WhiteListPortalComponent }
  ]
})
export class WhiteListPortalModule { }
