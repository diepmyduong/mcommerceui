import { Component, OnInit, Input, Host } from '@angular/core';
import { WhiteListPortalComponent } from 'app/chatgut-app/portals/white-list-portal/white-list-portal.component'
@Component({
  selector: 'app-domain',
  templateUrl: './domain.component.html',
  styleUrls: ['./domain.component.scss']
})
export class DomainComponent implements OnInit {

  @Input() url: string
  @Input() readOnly: boolean = false
  constructor(
    @Host() public whiteList: WhiteListPortalComponent
  ) { }

  ngOnInit() {
  }

}
