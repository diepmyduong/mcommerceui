import { merge } from 'lodash-es'
import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { environment } from '@environments';
import { iEditInfoDialogData, EditInfoDialog } from 'app/modals/edit-info/edit-info.component';
import { MatDialog } from '@angular/material';
import { iSetting } from 'app/services/chatbot/api/crud/setting';
import { iPage } from 'app/services/chatbot';
@Component({
  selector: 'app-white-list-portal',
  templateUrl: './white-list-portal.component.html',
  styleUrls: ['./white-list-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WhiteListPortalComponent extends BasePortal implements OnInit {
  constructor(
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef
  ) {
    super()
    this.setPortalName("Tên miền")
  }
  componentName = "WhiteListPortalComponent"
  setting: iSetting
  defaultWhitelistDomains = [
    environment.chatbot.uiHost,
    environment.chatbot.host,
    environment.chatbot.orderHost
  ]
  defaultDomainRegex = new RegExp(`(${this.defaultWhitelistDomains.join('|')})`)
  limitDomain = 20

  async ngOnInit() {
    await this.reload()
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(local: boolean = true) {
    this.showLoading()
    const { app } = this.chatbotApi.chatbotAuth
    const pageId = (typeof(app.activePage)==='object') ? (app.activePage as iPage)._id : app.activePage as string

    const settings = await this.chatbotApi.setting.getList({
      local, query: {
        filter: {
          type: 'whitelisted_domains',
          page: pageId
        }
      }
    })
    this.setting = undefined
    if (settings.length === 0) {
      this.showLoading()
      this.setting = await this.chatbotApi.page.addSetting({
        type: "whitelisted_domains",
        page: pageId,
        option: this.defaultWhitelistDomains
      })
      await this.reload()
      this.hideLoading()
    } else {
      this.setting = merge({}, settings[0]);
    }
    this.hideLoading()
  }

  async addDomain() {
    const result = await this.openDomainDialog("")
    if (result) {
      for(let domain of this.setting.option) {
        if(this.equalDomain(result.url, domain)) {
          this.alert.info("Tên miền đã tồn tại", "Vui lòng chọn tên miền khác")
          return
        }
      }
      this.setting.option.push(result.url)
      this.saveSetting("add")
    }
  }

  async editDomain(url: string) {
    const index = this.setting.option.findIndex(u => u === url)
    const result = await this.openDomainDialog(url)

    if (result && !this.equalDomain(result.url, url)) {
      this.setting.option[index] = result.url
      this.saveSetting("change")
    }
  }

  equalDomain(a: string, b: string) {
    const aUrl = new URL(a)
    const bUrl = new URL(b)
    return aUrl.hostname === bUrl.hostname && aUrl.protocol === bUrl.protocol
  }

  async removeDomain(url: string, event) {
    event.stopPropagation();
    const index = this.setting.option.findIndex(u => u === url)
    this.setting.option.splice(index, 1)
    this.saveSetting("delete")
  }

//   settingChanged() {
//     this.showSave = true;
//     this.saved = false;
//   }

  async saveSetting(mode?:string) {
    let message:string = ""
    if(mode == "add"){
        message = "thêm"
    }
    if(mode == "delete"){
        message = "xóa"
    }
    if (mode == "change"){
        message = "sửa"
    }
      // Update Card
      this.showLoading()
      try {
        this.setting = await this.chatbotApi.setting.update(this.setting._id, this.setting, { reload: true })
        this.alert.success('Thành công',`Đã ${message} thành công`)
      } catch (err) {
        this.alert.handleError(err);
        // this.alert.error(`Không thể ${message}`, err.message)
        throw err;
      } finally {
        this.hideLoading();
      }
    
  }

  async saveContent() {
    try {
      this.showSaveButton()
      await this.saveSetting();
      this.showSaveSuccess();
    } catch (err) {
      this.alert.handleError(err, "Lôi khi lưu");
      // this.alert.error("Lưu không thành công", err.message)
      this.hideSave();
    }
  }

  async openDomainDialog(url) {
    let data: iEditInfoDialogData = {
      title: "",
      confirmButtonText: "Xác nhận",
      inputs: [{
        title: "Đường dẫn URL",
        property: "url",
        current: url,
        type: "text",
        required: true,
        isURL: true
      }]
    };
    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    });

    return await dialogRef.afterClosed().toPromise();
  }

  isReadonlyDomain(url: string) {
    return this.defaultDomainRegex.test(url)
  }
}
