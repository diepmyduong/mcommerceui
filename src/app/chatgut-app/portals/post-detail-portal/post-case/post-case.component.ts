import { Component, OnInit, Input, Host, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { iInteractPostCase } from "app/services/chatbot";
import { PostDetailPortalComponent } from "app/chatgut-app/portals/post-detail-portal/post-detail-portal.component";

@Component({
  selector: "app-post-case",
  templateUrl: "./post-case.component.html",
  styleUrls: ["./post-case.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PostCaseComponent implements OnInit {
  @Input() case: iInteractPostCase;
  @Input() readOnly: boolean = false;
  @Input() index: number;
  constructor(@Host() public postDetail: PostDetailPortalComponent, public changeRef: ChangeDetectorRef) {}

  ngOnInit() {}

  get caseName() {
    switch (this.case.type) {
      case "fail":
        return "Bất kỳ";
      case "regex":
        return "Nhập chuỗi";
      case "number":
        return "Nhập số";
      case "range":
        return "Nhập khoảng số";
      case "intent":
        return "Sử dụng AI";
      case "overLimit":
        return "Đạt giới hạn số lần";
      case "overAmount":
        return "Đạt giới hạn số lượng";
      case "premature":
        return "Trước ngày lên lịch";
      case "expired":
        return "Sau ngày lên lịch";
      case "tagLimit":
        return "Chưa đạt số lương tag";
    }
  }
}
