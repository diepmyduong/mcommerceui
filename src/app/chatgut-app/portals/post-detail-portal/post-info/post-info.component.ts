import { Component, OnInit, Input, ViewChild, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { NgForm } from '@angular/forms';
import * as moment from 'moment'
import { ITime } from 'app/shared/material-time/time-control';
import { iInteractPost } from 'app/services/chatbot/api/crud/interactPost';
@Component({
  selector: 'app-post-info',
  templateUrl: './post-info.component.html',
  styleUrls: ['./post-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostInfoComponent extends BaseComponent implements OnInit {

  @Input() interactPost: iInteractPost
  @Output() change = new EventEmitter<iInteractPost>()
  @ViewChild('myFrm', { read: NgForm }) myFrm: NgForm
  constructor(
    public changeRef: ChangeDetectorRef
  ) {
    super('PostInfoComponent')
  }
  interactDateRange: any = {}
  canEmmitChange: boolean = false
  // editDate:boolean = false
  async ngOnInit() {
    // this.editDate = this.interactPost.isCheckDate
    this.subscriptions.push(this.myFrm.valueChanges.subscribe(this.formChange.bind(this)))
    const startDate = moment(this.interactPost.startDate).locale('en')
    const endDate = moment(this.interactPost.endDate).locale('en')
    
    this.interactDateRange = {
      startDate: startDate.toDate(),
      startTime: {
        hour: startDate.get('hours'),
        minute: startDate.get('minute'),
        meriden: startDate.format('A')
      } as ITime,
      endDate: endDate.toDate(),
      endTime: {
        hour: endDate.get('hours'),
        minute: endDate.get('minute'),
        meriden: endDate.format('A')
      } as ITime,
    }
    console.log("date",this.interactDateRange)
    setTimeout(() => {
      this.canEmmitChange = true
    }, 500)
    this.changeRef.detectChanges()
  }

  async formChange(event?: any) {
    if(!this.canEmmitChange && !this.myFrm.dirty) return
    if (this.interactDateRange) {
      const startDate = moment(this.interactDateRange.startDate).locale('en')
      startDate.hours(this.interactDateRange.startTime.hour)
      startDate.minutes(this.interactDateRange.startTime.minute)
      const endDate = moment(this.interactDateRange.endDate).locale('en')
      endDate.hours(this.interactDateRange.endTime.hour)
      endDate.minutes(this.interactDateRange.endTime.minute)
      // this.interactPost.isCheckDate = !this.editDate
      console.log( 'this',this.interactPost.isCheckDate)
        this.interactPost.startDate = startDate.toDate()
        this.interactPost.endDate = endDate.toDate()

      this.change.emit(this.interactPost)
    }
    this.changeRef.detectChanges()
  }
}