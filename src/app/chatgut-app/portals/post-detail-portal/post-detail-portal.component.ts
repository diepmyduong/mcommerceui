import { merge, isObject, maxBy, remove } from "lodash-es";
import * as guid from "guid";
import * as moment from "moment";
import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from "@angular/core";
import { BasePortal } from "app/chatgut-app/portals/portal-container/base-portal";
import { MatDialog } from "@angular/material";
import { iEditInfoDialogData, EditInfoDialog } from "app/modals/edit-info/edit-info.component";
import { iSelectionData, SelectionDialog } from "app/modals/selection/selection.component";
import { iFbPost, iPage } from "app/services/chatbot/api/crud/page";
import { iInteractPost, iInteractPostCase } from "app/services/chatbot/api/crud/interactPost";

@Component({
  selector: "app-post-detail-portal",
  templateUrl: "./post-detail-portal.component.html",
  styleUrls: ["./post-detail-portal.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PostDetailPortalComponent extends BasePortal implements OnInit {
  componentName = "PostDetailPortalComponent";
  @Input() post: iFbPost;
  @Input() interactPostId: string;
  @Output() onPostClosed: EventEmitter<any> = new EventEmitter();
  @Output() onPostUpdated: EventEmitter<iInteractPost> = new EventEmitter();
  loadDone: boolean = false;
  constructor(public dialog: MatDialog, public changeRef: ChangeDetectorRef) {
    super();
  }

  subName = "Kết nối";
  interactPost: iInteractPost;
  //interactPostId: string
  loadIndividualPost = false;
  commentReplies: any;
  subscribers = [];

  async ngOnInit() {
    this.reload();
    const { app } = this.chatbotApi.chatbotAuth;
    let pageId = isObject(app.activePage) ? (app.activePage as iPage)._id : (app.activePage as string);
    const page = await this.chatbotApi.page.getItem(pageId, { local: true });
  }

  async ngOnDestroy() {
    super.ngOnDestroy();
    this.onPostClosed.emit();
  }

  async reload(local: boolean = true) {
    try {
      this.detectChanges();
      let tasks = [];
      if (this.interactPostId) {
        this.interactPost = await this.chatbotApi.interactPost.getItem(this.interactPostId);
      } else {
        if (this.loadIndividualPost) {
          this.interactPost = await this.chatbotApi.interactPost.getItem(this.interactPostId, { local });
        } else {
          await this.chatbotApi.interactPost
            .getList({
              local,
              query: {
                filter: { postId: this.post.id },
              },
            })
            .then((result) => {
              if (result.length > 0) {
                this.interactPost = result[0];
              }
            });
        }
      }
    } catch (err) {
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi lấy dữ liệu", err.message)
    } finally {
      if (this.interactPost) {
        this.setPortalName(this.interactPost.hashTag);
        this.interactPost.cases.forEach((postCase) => {
          postCase.id = guid.raw();
        });
        this.loadDone = true;
        this.getCommentReplies();
      }
      this.loadDone = true;
      this.detectChanges();
    }
  }

  async getCommentReplies(local: boolean = true) {
    this.commentReplies = await this.chatbotApi.interactPost.getCommentReplies(this.interactPost._id, local);
    this.subscribers = [];
    if (this.commentReplies) {
      this.commentReplies.forEach((rep) => {
        let sub = this.subscribers.find((sub) => sub.uid == rep.uid);
        if (sub) {
          sub.amount++;
        } else {
          this.subscribers.push({
            uid: rep.uid,
            name: rep.userName,
            amount: 1,
          });
        }
      });
    }
    this.detectChanges();
  }

  async openSelectionPost() {
    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "PostsPortalComponent", {
      mode: "select",
    });

    if (!componentRef) return;

    let component = componentRef.instance as any;
    this.subscriptions.selected = component.selectedPost.subscribe((post) => {
      let newInteractPost = {} as iInteractPost;
      let interactPost = post.interactPost as iInteractPost;

      newInteractPost.name = interactPost.name;
      newInteractPost.description = interactPost.description;
      newInteractPost.postId = this.post.id;
      newInteractPost.type = interactPost.type;
      newInteractPost.amount = interactPost.amount;
      newInteractPost.limit = interactPost.limit;
      newInteractPost.cases = [];
      for (let i = 0; i < interactPost.cases.length; i++) {
        let currentCase = interactPost.cases[i];
        let newCase = {} as iInteractPostCase;
        newCase.isResponseStory = currentCase.isResponseStory;
        newCase.responseStory = currentCase.responseStory;
        newCase.isResponseComment = currentCase.isResponseComment;
        newCase.isResponsePrivateReplies = currentCase.isResponsePrivateReplies;
        newCase.responsePrivateReplies = currentCase.responsePrivateReplies;
        newCase.isCheckReaction = currentCase.isCheckReaction;
        newCase.reactionRequires = currentCase.reactionRequires;
        newCase.isSendOnlyParentComment = currentCase.isSendOnlyParentComment;
        newCase.priority = currentCase.priority;
        newCase.value = currentCase.value;
        newCase.type = currentCase.type;
        newCase.amount = currentCase.amount;
        newCase.limit = currentCase.limit;

        if (currentCase.responseComment) {
          newCase.responseComment = {
            message: currentCase.responseComment.message,
            attachment_id: currentCase.responseComment.attachment_id,
            attachment_url: currentCase.responseComment.attachment_url,
            source: currentCase.responseComment.source,
          };
        }

        if (currentCase.privateRepliesConfig) {
          newCase.privateRepliesConfig = [];
          currentCase.privateRepliesConfig.forEach((config) => {
            let newConfig = {
              key: config.key,
              value: config.value,
              type: config.type,
            };

            newCase.privateRepliesConfig.push(newConfig);
          });
        }

        newInteractPost.cases.push(newCase);
      }

      this.addInteractPost(newInteractPost);
      component.selectedPost.unsubscribe();
      this.detectChanges();
    });
  }

  async copyCases() {
    const option: any = { mode: "select", hideCaselessPost: true };
    if (this.interactPost.type != "hash_tag") {
      option.hiddenPostId = this.post.id;
    }
    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "PostsPortalComponent", option);

    if (!componentRef) return;

    let component = componentRef.instance as any;
    this.subscriptions.selected = component.selectedPost.subscribe(async (post) => {
      if (
        !(await this.alert.warning(
          "Sao chép trường hợp vào kết nối",
          "Các trường hợp ở kết nối cũ sẽ bị ghi đè. Bạn có chắc chắn muốn sao chép?"
        ))
      )
        return;

      let interactPost = post.interactPost as iInteractPost;

      let interactPostCases = [] as iInteractPostCase[];
      for (let i = 0; i < interactPost.cases.length; i++) {
        let currentCase = interactPost.cases[i];
        let newCase = {} as iInteractPostCase;
        newCase.isResponseStory = currentCase.isResponseStory;
        newCase.responseStory = currentCase.responseStory;
        newCase.isResponseComment = currentCase.isResponseComment;
        newCase.isResponsePrivateReplies = currentCase.isResponsePrivateReplies;
        newCase.responsePrivateReplies = currentCase.responsePrivateReplies;
        newCase.isCheckReaction = currentCase.isCheckReaction;
        newCase.reactionRequires = currentCase.reactionRequires;
        newCase.isSendOnlyParentComment = currentCase.isSendOnlyParentComment;
        newCase.priority = currentCase.priority;
        newCase.value = currentCase.value;
        newCase.type = currentCase.type;
        newCase.amount = currentCase.amount;
        newCase.limit = currentCase.limit;

        if (currentCase.responseComment) {
          newCase.responseComment = {
            message: currentCase.responseComment.message,
            attachment_id: currentCase.responseComment.attachment_id,
            attachment_url: currentCase.responseComment.attachment_url,
            source: currentCase.responseComment.source,
          };
        }

        if (currentCase.privateRepliesConfig) {
          newCase.privateRepliesConfig = [];
          currentCase.privateRepliesConfig.forEach((config) => {
            let newConfig = {
              key: config.key,
              value: config.value,
              type: config.type,
            };

            newCase.privateRepliesConfig.push(newConfig);
          });
        }

        interactPostCases.push(newCase);
      }

      let newInteractPost = { ...this.interactPost };
      newInteractPost.cases = interactPostCases;
      try {
        this.loadDone = false;
        this.detectChanges();
        this.interactPost = await this.chatbotApi.interactPost.update(this.interactPost._id, newInteractPost);
      } catch (err) {
        // console.error(err)
        this.alert.handleError(err, "Lỗi khi sao chép trường hợp");
        // this.alert.error("Lỗi khi sao chép trường hợp", err.message)
      } finally {
        this.loadDone = true;
        this.detectChanges();
      }
      component.selectedPost.unsubscribe();
    });
  }

  async connectPost() {
    const interactPost = await this.openNewInteractPostDialog();
    this.addInteractPost(interactPost);
  }

  async addInteractPost(interactPost) {
    if (interactPost) {
      this.loadDone = false;
      try {
        this.detectChanges();
        this.interactPost = await this.chatbotApi.interactPost.add(interactPost);
        this.interactPost.cases.forEach((postCase) => {
          postCase.id = guid.raw();
        });
        this.alert.success("Thành công", "Tạo kết nối thành công");
        this.onPostUpdated.emit(this.interactPost);
      } catch (err) {
        this.alert.handleError(err, "Lỗi khi kết nối với bài đăng");
        // this.alert.error('Lỗi khi kết nối với bài đăng', err.message)
      } finally {
        this.setPortalName(this.interactPost.hashTag);
        this.getCommentReplies();
        this.loadDone = true;
        this.detectChanges();
      }
    }
  }

  openSubscriber(sub) {
    this.portalService.pushPortalAt(this.index + 1, "SubscriberDetailPortalComponent", {
      portalName: sub.name,
      subscriberId: sub._id,
    });
  }

  openNewInteractPostDialog() {
    let data: iEditInfoDialogData = {
      title: "Tạo kết nối bài đăng",
      confirmButtonText: "Kết nối",
      confirmButtonClass: "i-create-story-confirm-button",
      cancelButtonText: "Quay lại",
      inputs: [
        {
          title: "Đặt tên",
          property: "name",
          type: "text",
          required: true,
          className: "i-story-title-input",
        },
      ],
    };

    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: "500px",
      data: data,
    });
    return new Promise((resolve, reject) => {
      dialogRef
        .afterClosed()
        .toPromise()
        .then(async (result) => {
          if (result) {
            resolve({
              name: result.name,
              postId: this.post.id,
              amount: 0,
              limit: 0,
              startDate: moment(result.startDate).format() as any,
              endDate: moment(result.endDate).format() as any,
              cases: [
                {
                  type: "fail",
                  priority: 0,
                  isResponseStory: false,
                  isResponseComment: false,
                  isResponsePrivateReplies: false,
                  value: {
                    regex: "",
                  },
                  isCheckReaction: false,
                  isSendOnlyParentComment: false,
                  amount: 0,
                  limit: 0,
                },
              ],
            } as iInteractPost);
          } else {
            resolve();
          }
        });
    });
  }

  async openNewCaseTypeDialog() {
    let data: iSelectionData = {
      title: "Thêm trường hợp bắt Comment",
      confirmButtonText: "Thêm",
      cancelButtonText: "Quay lại",
      categories: [
        {
          name: "Loại trường hợp",
          options: [
            { code: "fail", display: "Nhập bất kỳ", icon: "fas fa-random" },
            { code: "regex", display: "Nhập chuỗi", icon: "fas fa-font" },
            { code: "number", display: "Nhập số", icon: "fas fa-sort-numeric-down" },
            { code: "range", display: "Nhập khoảng số", icon: "fas fa-sliders-h" },
            { code: "overLimit", display: "Đạt giới hạn số lần", icon: "fas fa-tachometer-alt" },
            { code: "overAmount", display: "Đạt giới hạn số lượng", icon: "fas fa-thermometer-full" },
            { code: "premature", display: "Trước ngày lên lịch", icon: "fas fa-clock" },
            { code: "expired", display: "Sau ngày lên lịch", icon: "fas fa-clock" },
            { code: "tagLimit", display: "Chưa đạt số lượng tag", icon: "fas fa-clock" },
          ],
        },
      ],
    };

    const { app } = this.chatbotApi.chatbotAuth;
    let pageId = isObject(app.activePage) ? (app.activePage as iPage)._id : (app.activePage as string);
    const page = await this.chatbotApi.page.getItem(pageId, { local: true });
    if (page.ai) {
      data.categories[0].options.push({
        code: "intent",
        display: "Sử dụng AI",
        icon: "fab fa-android",
      });
    }

    let dialogRef = this.dialog.open(SelectionDialog, {
      width: "600px",
      data: data,
    });
    return await dialogRef.afterClosed().toPromise();
  }

  async saveContent() {
    try {
      this.showSaving();
      this.detectChanges();
      await this.chatbotApi.interactPost.update(this.interactPost._id, this.interactPost);
      this.showSaveSuccess();
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lưu dữ liệu");
      // this.alert.error("Lỗi khi lưu dữ liệu", err.message)
      this.showSaveButton();
      this.hideSave();
      this.detectChanges();
    }
  }

  async addNewCase() {
    const caseType = await this.openNewCaseTypeDialog();
    if (caseType) {
      const maxPriorityCase = maxBy(this.interactPost.cases, "priority");
      let newCase: iInteractPostCase = {
        id: guid.raw(),
        type: caseType,
        isResponseStory: false,
        isResponseComment: false,
        isResponsePrivateReplies: false,
        isCheckReaction: false,
        isSendOnlyParentComment: false,
        priority: maxPriorityCase ? maxPriorityCase.priority + 1 : 0,
        value: {
          regex: "",
        },
        limit: 0,
        amount: 0,
      };
      this.interactPost.cases.push(newCase);
      let interactPost = await this.chatbotApi.interactPost.update(this.interactPost._id, this.interactPost);
      this.detectChanges();
      //this.showSaveButton()
    }
  }

  async openCase(postCase: iInteractPostCase) {
    const compRef = await this.portalService.pushPortalAt(this.index + 1, "PostCasePortalComponent", {
      case: postCase,
      interactPost: this.interactPost,
    });
    const component = compRef.instance as any;
    if (this.subscriptions.onPostCaseChange) this.subscriptions.onPostCaseChange.unsubscribe();
    this.subscriptions.onPostCaseChange = component.onChange.subscribe((changeCase: iInteractPostCase) => {
      const index = this.interactPost.cases.findIndex((c) => c.id === changeCase.id);
      this.interactPost.cases[index] = changeCase;
    });
    this.detectChanges();
  }

  async removeCase(caseId: string) {
    if (!(await this.alert.warning("Xóa trường hợp", "Xóa trường hợp này khỏi kết nối?"))) return;
    remove(this.interactPost.cases, (c) => c.id === caseId);
    let interactPost = await this.chatbotApi.interactPost.update(this.interactPost._id, this.interactPost);
    this.detectChanges();
    //this.showSaveButton()
  }

  async duplicate(caseId: string) {
    if (!(await this.alert.question("Sao chép trường hợp", "Tạo một bản sao của trường hợp đã chọn?"))) return;
    const srcCase = this.interactPost.cases.find((c) => c.id === caseId);
    const newCase = merge({}, srcCase, { id: guid.raw() });
    const newInteractPost = { ...this.interactPost };
    newInteractPost.cases.push(newCase);
    this.interactPost = await this.chatbotApi.interactPost.update(newInteractPost._id, newInteractPost);
    this.detectChanges();
    //this.showSaveButton()
  }

  async openLink() {
    let url = "https://facebook.com/" + this.post.id;
    window.open(url, "_blank");
  }

  async addLabels(subscribers: any[]) {
    let data: iEditInfoDialogData = {
      title: "Thêm nhãn cho khách hàng",
      confirmButtonText: "Xác nhận",
      inputs: [
        {
          title: "Nhãn",
          property: "label",
          current: "",
          type: "text",
          required: true,
        },
      ],
    };
    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: "500px",
      data: data,
    });

    const pageId = (this.chatbotApi.chatbotAuth.app.activePage as iPage)._id;
    let subscriberIds = [];
    subscribers.forEach((sub) => {
      subscriberIds.push(sub._id);
    });

    dialogRef
      .afterClosed()
      .toPromise()
      .then(async (result) => {
        if (result && result.label) {
          try {
            await this.chatbotApi.label.addLabel(pageId, subscriberIds, result.label);
            this.alert.success("Thành công", "Thêm nhãn thành công");
          } catch (err) {
            this.alert.handleError(err, "Lỗi khi thêm nhãn");
            // this.alert.error("Lỗi khi thêm nhãn", err.message)
          } finally {
            this.detectChanges();
          }
        }
      });
  }

  async openStatistics() {
    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "StatisticsDetailsResultPortalComponent", {
      mode: "post",
      postId: this.interactPost._id,
      portalName: "Thống kê ",
      name: this.interactPost.name,
      hasFooter: false,
    });
  }
}
