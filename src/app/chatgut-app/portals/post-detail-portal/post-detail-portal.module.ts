import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostDetailPortalComponent } from 'app/chatgut-app/portals/post-detail-portal/post-detail-portal.component';
import { PostCaseComponent } from 'app/chatgut-app/portals/post-detail-portal/post-case/post-case.component';
import { PostInfoComponent } from 'app/chatgut-app/portals/post-detail-portal/post-info/post-info.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatLineModule, MatIconModule, MatTooltipModule, MatListModule, MatButtonModule, MatToolbarModule, MatProgressSpinnerModule, MatDividerModule, MatFormFieldModule, MatInputModule, MatDatepickerModule, MatNativeDateModule, MatCheckboxModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { MaterialTimeControlModule } from 'app/shared';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { SelectionModule } from 'app/modals/selection/selection.module';

@NgModule({
  imports: [
    CommonModule,
    MatLineModule,
    MatIconModule,
    MatTooltipModule,
    MatListModule,
    MatButtonModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    MaterialTimeControlModule,
    EditInfoModule,
    SelectionModule,
    MatCheckboxModule
  ],
  declarations: [PostDetailPortalComponent, PostCaseComponent, PostInfoComponent],
  entryComponents: [PostDetailPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: PostDetailPortalComponent }
  ]
})
export class PostDetailPortalModule { }
