import { HostBinding, Input, Output, EventEmitter, ViewRef, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject ,  Subscription } from 'rxjs'
import * as Console from 'console-prefix'
import * as guid from 'guid'
import { AlertService } from 'app/services/alert.service';
import { ChatbotApiService } from 'app/services/chatbot';
import { PortalService } from 'app/services/portal.service';
import { ServicesInjector } from 'app/services/serviceInjector';
export interface IProgressBarOption {
    show: boolean
    color: 'primary' | 'accent' | 'warn'
    mode: 'determinate' | 'indeterminate' | 'buffer' | 'query'
    value: number
    buffer: number
}
export class BasePortal {
    @HostBinding('class.swiper-slide') swiperSlideClass = true
    @HostBinding('class.main-swiper') mainSwiperClass = true
    @Input() portalName: string = "";
    @Output() onClosed = new EventEmitter();
    componentName = ""

    constructor(
    ) {
        this.alert = ServicesInjector.get(AlertService)
        this.chatbotApi = ServicesInjector.get(ChatbotApiService)
        this.portalService = ServicesInjector.get(PortalService);
        this.subscriptions = {}
    }
    viewInited: BehaviorSubject<number>
    subscriptions: { [key:string]: Subscription }
    viewRef: ViewRef
    progressBar: IProgressBarOption = {
        show: false,
        color: 'primary',
        mode: 'indeterminate',
        value: 0,
        buffer: 0
    }
    portalId = guid.raw()
    subName = "";
    showSave = false;
    isSaving = false;
    saved = false;
    isSending = false;
    public alert: AlertService
    public chatbotApi: ChatbotApiService
    public portalService: PortalService

    trackByFn(index, item) {
        return index; // or item.id
    }

    get log() { return Console('Portal '+ this.index).log }
    // get currentIndex(){ return 1 }
    get index() { return this.portalService.container.swiperWrapper.indexOf(this.viewRef) }

    get _changeRef(): ChangeDetectorRef { return (<any>this).changeRef; }
    detectChanges() { this._changeRef?this._changeRef.detectChanges(): null; }

    async showSaveButton() {
        this.showSave = true;
        this.isSaving = false;
        this.saved = false;
        this.detectChanges();
    }

    async showSaving() {
        this.showSave = true;
        this.isSaving = true;
        this.saved = false;
        this.detectChanges();
    }

    async showSaveSuccess() {
        this.showSave = false;
        this.isSaving = false;
        this.saved = true;
        this.detectChanges();
    }

    async hideSave() {
        this.showSave = false;
        this.isSaving = false;
        this.saved = false;
        this.detectChanges();
    }

    async ngAfterContentInit() {
        setTimeout(()=>{
            this.viewInited.next(this.index)
        })
    }

    async ngOnDestroy() {
        this.onClosed.emit()
        for (let key in this.subscriptions) {
            this.subscriptions[key].unsubscribe()
        }
    }

    async close() {
        this.portalService.container.popPortal(this.index)
    }

    async focus() {
        this.portalService.container.swiper.slideTo(this.index)
        this.portalService.container.activePortal = this.portalId
    }

    showLoading() {
        setTimeout(() => { this.progressBar.show = true; this.detectChanges(); })
    }

    hideLoading() {
        setTimeout(() => { this.progressBar.show = false; this.detectChanges(); })
    }

    setPortalName(name: string) {
        setTimeout(() => {
            this.portalName = name;
            this.detectChanges();
        })
    }

    async runIntro() {

    }
}