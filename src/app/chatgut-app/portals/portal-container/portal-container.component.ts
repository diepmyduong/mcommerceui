import { merge } from 'lodash-es'
import * as Console from 'console-prefix'
import { Component, OnInit, ViewChild, ComponentFactoryResolver, ViewContainerRef, ChangeDetectorRef, NgZone, ChangeDetectionStrategy } from '@angular/core';
import { BehaviorSubject ,  Subscription } from 'rxjs'
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { AlertService } from 'app/services/alert.service';
import { DynamicComponentLoaderService } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { SwiperComponent } from 'angular2-useful-swiper';
@Component({
  selector: 'app-portal-container',
  templateUrl: './portal-container.component.html',
  styleUrls: ['./portal-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PortalContainerComponent implements OnInit {

  @ViewChild(SwiperComponent) swiperComp: SwiperComponent
  @ViewChild("swiperWrapper", {read: ViewContainerRef}) swiperWrapper: ViewContainerRef
  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    public changeRef: ChangeDetectorRef,
    public zone: NgZone,
    public alert: AlertService,
    private dynamicComponentLoader: DynamicComponentLoaderService
  ) {
    this.swiperOptions = {
      scrollbarHide: false,
      mousewheelControl: true,
      slidesPerView: 'auto',
      grabCursor: false,
      autoResize: false,
      slideToClickedSlide: true,
      speed: 350,
      scrollbar: {
        el: '.portal-scrollbar',
        draggable: true,
      },
      simulateTouch: false,
    }
    this.portals = []
    this.portalInited = new BehaviorSubject<number>(-1)
  }
  swiperOptions
  portals:any[]
  portalInited: BehaviorSubject<number>
  subscriptions: { [key:string] : Subscription } = {}
  activePortal: string

  get log() { return Console('[PortalContainer]').log }

  async ngOnInit() {
  }

  async ngAfterViewInit() {
    this.subscriptions.portalInited = this.portalInited.subscribe((index => {
      this.swiper.slideTo(index);
      if(this.portals[index]) {
        this.activePortal = this.portals[index].portalId
        this.changeRef.detectChanges()
      }
    }).bind(this))
  }

  async ngOnDestroy() {
    this.subscriptions.portalInited.unsubscribe()
    this.clear()
  }

  get swiper(){
    return this.swiperComp.swiper
  }
  get warpper() {
    return this.swiperWrapper
  }

  async pushPortal(portalComp:any, params:any = {}) {
    let componentFactory 
    if(typeof portalComp == "string") {
      componentFactory = await this.dynamicComponentLoader.getComponentFactory(portalComp);
    } else {
      componentFactory = await this.componentFactoryResolver.resolveComponentFactory(portalComp);
    }

    let componentRef = componentFactory.create(this.swiperWrapper.parentInjector)
    let newIndex = this.portals.length
    // await new Promise(r => setTimeout(r, 3000))
    let viewRef = this.swiperWrapper.insert(componentRef.hostView,newIndex)
    let component:any = componentRef.instance
    component.viewRef = viewRef
    component.viewInited = this.portalInited
    merge(component,params)
    this.portals.push(component)
    this.changeRef.detectChanges()
    return componentRef
  }

  clear() {
    this.changeRef.detectChanges()
    this.swiperWrapper.clear()
    this.portals = []
  }

  async popPortal(index:number) {
    let isSavingFlag = false;
    let canBeSavedFlag = false;

    for (let i = index; i < this.portals.length; i++) {
      let portalAtIndex = this.portals[i] as BasePortal;

      if (portalAtIndex.componentName == 'StoriesPortalComponent' || 
          portalAtIndex.componentName == 'TopicsPortalComponent')
          continue;
          
      if (portalAtIndex) {
        if (portalAtIndex.isSaving) {
          isSavingFlag = true;
          break;
        }
        if (portalAtIndex.showSave) {
          canBeSavedFlag = true;
          break;
        }
      }
    }

    if (isSavingFlag) {
      await this.alert.info("Chú ý", "Không thể đóng trang khi đang lưu dữ liệu")
      this.changeRef.detectChanges()
      return false;
    }

    let checkFlag;
    if (canBeSavedFlag && !isSavingFlag) {
      await this.alert.warning("Chú ý", "Bạn đang đóng một hoặc nhiều mục mà chưa lưu dữ liệu chỉnh sửa", "Đóng và không lưu", "Quay lại")
      .then(async result => {
        if (result) {
          await this.removePortalFromIndex(index);
          checkFlag = true;
        } else {
          checkFlag = false;
        }
      }).catch(() => {
        checkFlag = false;
      });
    } else {
      await this.removePortalFromIndex(index);
      checkFlag = true;
    }
    this.changeRef.detectChanges()
    return checkFlag;
  }

  async pushPortalAt(index:number, portalComp:any, params:any = {}) {
      if (await this.popPortal(index)) {
        return this.pushPortal(portalComp,params)
      } else {
        return null
      }
  }

  async removePortalFromIndex(index) {
    await this.zone.run(() => {
      const l = this.swiperWrapper.length
      // for(let i = index; i < l; i++){
      //   this.swiperWrapper.remove(i)
      //   this.ref.detectChanges()
      // }
      if (index >= l) return;
      for(let i = l - 1; i >= index; i--){
        this.swiperWrapper.remove(i)
        this.changeRef.detectChanges()
      }
      this.portals.length = index
      this.swiper.slideTo(index - 1)
      if(this.portals[index -1]) {
        this.activePortal = this.portals[index -1].portalId
      }
      this.changeRef.detectChanges()
    })
  }

  getLastPortalName(): string {
    return this.portals.length === 0?"":this.portals[this.portals.length - 1].componentName;
  }

  getPortalNameAt(index) {
    return (0 <= index && index < this.portals.length && this.portals.length > 0)?this.portals[index].componentName:'';
  }

}
