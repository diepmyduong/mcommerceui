import { Component, OnInit, Input, ViewChild, Output, EventEmitter, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { JsonEditorComponent, JsonEditorOptions } from 'ang-jsoneditor';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-clarifai-image-item-portal',
  templateUrl: './clarifai-image-item-portal.component.html',
  styleUrls: ['./clarifai-image-item-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClarifaiImageItemPortalComponent extends BasePortal implements OnInit {

  @Input() image: any
  @Input() clarifaiImageId: string
  @Output() onImageDeleted: EventEmitter<any> = new EventEmitter()
  @Output() onImageUpdated: EventEmitter<any> = new EventEmitter()
  @ViewChild(JsonEditorComponent) editor: JsonEditorComponent;
  constructor(
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef
  ) {
    super();
    this.setPortalName('Metadata');
    this.configJsonEditor()
  }
  componentName = "ClarifaiImagePortalComponent"
  images: any
  jsonEditorConfig: JsonEditorOptions = new JsonEditorOptions()
  data = {}

  configJsonEditor() {
    this.jsonEditorConfig.mode = 'code'
    let deboud;
    this.jsonEditorConfig.onChange = () => {
      if (deboud) clearTimeout(deboud);
      deboud = setTimeout(this.onJSONChange.bind(this), 250);
    }
  }
  onJSONChange() {
    try {
      this.image.metadata = this.editor.get();
      this.showSaveButton()
    } catch (err) {
      return;
    } finally {
      this.detectChanges()
    }
  }
  async ngOnInit() {
    await this.reload();
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }
  
  ngAfterViewInit() {
    this.data = this.image.metadata || {}
    this.detectChanges();
  }

  async reload(local: boolean = true) {

  }
  async saveContent() {
    try {
      this.showSaving();
      this.detectChanges()
      await this.chatbotApi.clarifaiImage.updateImage({ clarifaiImageId: this.clarifaiImageId, imageId: this.image.clarifaiId, metadata: this.image.metadata });
      this.onImageUpdated.emit(this.image)
      this.showSaveSuccess()
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lưu");
      // this.alert.error('Không thể cập nhật', err.message)
      // this.topicInfo.cardFrm.form.enable()
      this.showSaveButton()
    } finally {
      this.detectChanges()
    }
  }
  async updateImageMetadata() {

  }
  async deleteImage() {
    if (!await this.alert.warning('Xoá hình ảnh', "Bạn có muốn xoá hình ảnh này không", 'Xóa hình ảnh')) return;
    this.showLoading()
    try {
      this.detectChanges()
      await this.chatbotApi.clarifaiImage.deleteImage({ clarifaiImageId: this.clarifaiImageId, imageId: this.image.clarifaiId });
      this.alert.success("Thành không", "Xoá hình ảnh thành công")
      this.onImageDeleted.emit(this.image)
      this.portalService.popPortal(this.index)
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi xóa ảnh");
      // this.alert.error('Không thể xoá', err.message)
    } finally {
      this.hideLoading()
      this.detectChanges()
    }
  }
  async onChanged(){
    this.showSaveButton()
    this.detectChanges()
  }

}
