import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarifaiImageItemPortalComponent } from './clarifai-image-item-portal.component';
import { MaterialModule } from 'app/shared';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { NgJsonEditorModule } from 'ang-jsoneditor';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    NgJsonEditorModule
  ],declarations: [ClarifaiImageItemPortalComponent],
  entryComponents: [ClarifaiImageItemPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ClarifaiImageItemPortalComponent }
  ]
})
export class ClarifaiImageItemPortalModule { }
