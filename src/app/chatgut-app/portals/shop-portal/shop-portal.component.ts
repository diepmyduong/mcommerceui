/// <reference types="googlemaps" />
import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { MatDialog, MatSnackBar } from '@angular/material';
import * as moment from 'moment'
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { iProductCategory } from 'app/services/chatbot/api/crud/productCategory';
import { iProduct } from 'app/services/chatbot/api/crud/product';
import { iShop } from 'app/services/chatbot/api/crud/shop';
import { MapsAPILoader } from '@agm/core';
import { iOrder } from 'app/services/chatbot/api/crud/order';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { GsheetUser } from 'app/services/chatbot/api/crud/gsheetUser';
import { GoogleApiService } from 'app/services/googleApi/googleApi.service';
import { iEnvironment } from 'app/services/chatbot';
import { ToppingDialog } from './topping-dialog/topping-dialog.component';
import { cloneDeep } from 'lodash-es'
import { saveAs } from 'file-saver';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-shop-portal',
  templateUrl: './shop-portal.component.html',
  styleUrls: ['./shop-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShopPortalComponent extends BasePortal implements OnInit {

  componentName = "ShopPortalComponent"
  orders: iOrder[] = []
  products: iProduct[] = []
  categories: iProductCategory[] = []
  date: Date
  maxDate: Date
  
  loading: boolean = false
  loadProductsDone: boolean = false
  loadOrdersDone: boolean = false
  loadDone: boolean = false
  confirmingOrders: any = {}
  hasShopChanged: boolean = false
  tabIndex: number = 0
  shop: iShop
  autocompleteList: any[] = []
  @Output() onClosed = new EventEmitter<any>()
  scrolledToBottom = new BehaviorSubject<number>(0)
  totalOrders: number = 0

  EVoucherTypes = {
    "manually": "ƯU ĐÃI",
    "discount_bill": "GIẢM GIÁ",
    "discount_item": "GIẢM GIÁ",
    "buy_a_discount_b": "GIẢM GIÁ",
    "buy_a_offer_b": "QUÀ TẶNG",
    "offer_item": "QUÀ TẶNG",
    "same_price": "ĐỒNG GIÁ",
    "acc_decrease": "CHIẾT KHẤU"
  }

  constructor(
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef,
    public popoverService: PopoverService,
    private mapsAPILoader: MapsAPILoader,
    public googleApiSrv: GoogleApiService,
    private snackBar: MatSnackBar
  ) {
    super()
    this.setPortalName('Cửa hàng');
    this.maxDate = new Date()
    moment.locale('vi')

    this.subscriptions.onScrolledToBottom = this.scrolledToBottom.pipe(debounceTime(300)).subscribe(offset => {
      if (offset) this.loadMoreOrders()
    })
  }

  async ngOnInit() {
    this.date = new Date()
    this.generateDateText()
    await this.reload()
    this.changeTab(0)
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
    this.onClosed.emit(true)
  }

  async reload(local = true) {
    this.loadDone = false
    this.detectChanges()
    try {
      let shops

      let tasks = []
      tasks.push(this.chatbotApi.shop.getList({ local }).then(result => shops = result))
      tasks.push(this.mapsAPILoader.load())
      await Promise.all(tasks)

      if (shops && shops.length) this.shop = shops[0]
      console.log(this.shop)
      if (this.shop && this.shop.forwardGSheet) {
        this.loadGSheet()
      }
    } catch (err) {
      this.alert.handleError(err)
    } finally {
      this.loadDone = true
      this.detectChanges()
      this.setupPlacesAutocomplete()
    }
  }

  async loadProducts(local = true) {
    this.loadProductsDone = false
    this.detectChanges()
    try{
      let tasks = []
      tasks.push(this.chatbotApi.productCategory.getList({ 
        local, query: { fields: ["_id", "name", "position", "listProductId"], limit: 0, order: { position: 'asc' }} 
      }).then(result => { this.categories = cloneDeep(result) }))
      tasks.push(this.chatbotApi.product.getList({ 
        local, query: { fields: ["_id", "name", "description", "image", "category", "price", "toppings"], limit: 0, order: { createdAt: 1 }} 
      }).then(result => { this.products = cloneDeep(result) }))
      await Promise.all(tasks)
      
      this.categories.forEach(category => {
        category.products = category.listProductId.map(x => this.products.find(y => y._id == x)).filter(x => x)
      })

      console.log('products', this.categories)
      // console.log('product', products)
    } catch(err) {
      this.alert.handleError(err, 'Lỗi khi lấy sản phẩm')
    } finally {
      this.loadProductsDone = true
      this.detectChanges()
    }
  }

  async loadOrders(local = true) {
    this.loadOrdersDone = false
    this.detectChanges()
    try {
      let tasks = []

      tasks.push(this.chatbotApi.order.getList({ 
        local, query: {
          order: { createdAt: -1 },
          limit: 10,
          filter: this.getOrderFilter()
        }
      }).then(result => { 
        this.orders = result 
        this.totalOrders = this.chatbotApi.order.pagination.totalItems
      }))
      await Promise.all(tasks)

      let i = 1
      this.orders.forEach(order => {
        order.numbering = i++
        order.fromNow = moment(order.createdAt).fromNow()
        for (let item of order.items) {
          let toppingDesc = ``
          if (item.toppings && item.toppings.length) {
            toppingDesc = item.toppings.map(x => x.display).join(', ')
          }
          item.toppingDesc = toppingDesc
        }
      })
      console.log('order', this.orders)
      // console.log('product', products)
    } catch(err) {
      this.alert.handleError(err, 'Lỗi khi lấy danh sách đơn hàng')
    } finally {
      this.loadOrdersDone = true
      this.detectChanges()
    }
  }

  async loadMoreOrders(local = true) {
    this.progressBar.show = true
    this.changeRef.markForCheck()
    try {
      let tasks = []
      let orders = []

      tasks.push(this.chatbotApi.order.getList({ 
        local, query: {
          order: { createdAt: -1 },
          limit: 10,
          offset: this.orders.length,
          filter: this.getOrderFilter()
        }
      }).then(result => { 
        orders = result 
        this.totalOrders = this.chatbotApi.order.pagination.totalItems
      }))
      await Promise.all(tasks)

      let i = this.orders.length + 1
      orders.forEach(order => {
        order.numbering = i++
        order.fromNow = moment(order.createdAt).fromNow()
        for (let item of order.items) {
          let toppingDesc = ``
          if (item.toppings && item.toppings.length) {
            toppingDesc = item.toppings.map(x => x.display).join(', ')
          }
          item.toppingDesc = toppingDesc
        }
      })
      this.orders = [...this.orders, ...orders]
    } catch(err) {
      this.alert.handleError(err, 'Lỗi khi lấy danh sách đơn hàng')
    } finally {
      this.progressBar.show = false
      this.detectChanges()
    }
  }

  getOrderFilter() {
    let filter = undefined
    if (this.startDate || this.endDate) {
      filter = { createdAt: {} }
      if (this.startDate) {
        let startOfDay = new Date(this.startDate)
        startOfDay.setHours(0, 0, 0, 0)
        filter.createdAt['$gte'] = startOfDay.toUTCString()
      }
      if (this.endDate) {
        let endOfDay = new Date(this.endDate)
        endOfDay.setHours(23,59,59,999)
        filter.createdAt['$lte'] = endOfDay.toUTCString()
      }
    }
    return filter
  }

  onScroll(event) {
    let el: HTMLElement = event.srcElement
    const offset = el.scrollTop + el.clientHeight
    if (offset >= (el.scrollHeight - 50) && !this.progressBar.show && this.totalOrders > this.orders.length) this.scrolledToBottom.next(offset)
  }

  async confirmOrder(index) {
    if (!await this.alert.question('Xác nhận đơn hàng?', 'Đánh dấu đơn hàng là đã xác nhận.')) return
    let order = this.orders[index]
    try {
      this.confirmingOrders[order._id] = true
      this.detectChanges()
      order = await this.chatbotApi.order.update(order._id, { isConfirm: true })
      order.isConfirm = true
      order.numbering = index + 1
      order.fromNow = moment(order.createdAt).fromNow()
      console.log(this.orders)
    } catch (err) {
      this.alert.handleError(err, 'Không thể xác nhận đơn hàng')
    } finally {
      this.confirmingOrders[order._id] = false
      this.detectChanges()
    }
  }

  async toggleOrderDetails(order: iOrder) {
    order.detailsOpened = !order.detailsOpened
  }
  
  changeTab(index) {
    this.tabIndex = index
    switch(this.tabIndex) {
      case 1: {
        if (!this.loadProductsDone) {
          this.loadProducts()
        }
      }
      break
      case 2: {
        if (!this.loadOrdersDone) {
          this.loadOrders()
        }
      }
      break
      default: break
    }
    this.detectChanges()
  }
  
  async saveContent() {
    switch(this.tabIndex) {
      case 0: {
        try {
          this.showSaving()
          this.detectChanges()
          let tasks = []
          let { name, image, places, shipFeePerKilometer, kilometerFreeShip, shipNote, forwardGSheet, enableOTP, sendReceipt,
             forwardStory, gSheetId, gSheetUser, bookingSheetName, orderSheetName } = this.shop
          tasks.push(this.chatbotApi.shop.update(this.shop._id, { name, image, places, shipFeePerKilometer, kilometerFreeShip, shipNote, forwardGSheet,
            enableOTP, sendReceipt, forwardStory, gSheetId, gSheetUser, bookingSheetName, orderSheetName }))
          await Promise.all(tasks)
          this.showSaveSuccess()
        } catch (err) {
          this.alert.handleError(err, "Lỗi khi lưu thông tin cửa hàng");
          this.showSaveButton()
        } finally {
          this.detectChanges()
        }
      }
      break
    }
  }
  
  async removeProduct(category: iProductCategory, i) {
    if (!await this.alert.question("Xoá","Bạn có chắc muốn xóa sản phẩm này")) return
    try {
      this.loading = true
      this.detectChanges()
      let productId = category.products[i]._id
      await this.chatbotApi.product.delete(productId)
      category.products.splice(i, 1)
      category.listProductId.splice(i, 1)
      this.products.splice(this.products.findIndex(x => x._id == productId), 1)
      this.snackBar.open('🗑️ Đã xoá sản phẩm', '', { duration: 3000 });
    } catch(err) {
      console.log('err', err)
      this.alert.handleError(err,"Lỗi khi xóa sản phẩm")
    } finally {
      this.loading = false
      this.detectChanges()
    }
  }

  editProduct(event, category, id = null) {
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      target: '.product',
      targetElement: event.target,
      width: '92%',
      confirm: id?'Lưu thay đổi':'Tạo sản phẩm',
      horizontalAlign: 'center',
      verticalAlign: id?'top':'bottom',
      fields: [
        {
          placeholder: 'Sản phẩm',
          property: 'product',
          type: 'product',
          constraints: ['required'],
          current: id?(({ name, price, image, description }) => ({ name, price, image, description }))(this.products.find(x => x._id == id)):{
            name: '',
            price: 0,
            description: '',
            image: ''
          }
        }
      ],
      onSubmit: async (data) => {
        if (!data['product'].name) return
        try {
          this.loading = true
          this.detectChanges()
          if (id) {
            data['product'].price = Number(data['product'].price)
            if (data['product'].price == NaN) data['product'].price = 0
            console.log(data['product'])
            let index = category.products.findIndex(x => x._id == id)
            let product = await this.chatbotApi.product.update(id, data['product'])
            category.products[index] = product
          } else {
            let product = await this.chatbotApi.product.add({ ...data['product'], category: category._id })
            category.products.push(product)
            category.listProductId.push(product._id)
            this.products.push(product)
          }
          this.snackBar.open(`✔️ Đã ${id?'cập nhật':'thêm'} sản phẩm`, '', { duration: 3000 });
        } catch (err) {
          if (id) {
            this.alert.handleError(err, 'Lỗi khi thay đổi sản phẩm')
          } else {
            this.alert.handleError(err, 'Lỗi khi tạo sản phẩm')
          }
          this.snackBar.open(`❌ Lỗi khi ${id?'cập nhật':'thêm'} sản phẩm`, '', { duration: 3000 })
        } finally {
          this.loading = false
          this.detectChanges()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  async move(index, offset) {
    if (index + offset < 0 || index + offset >= this.categories.length) return
    let sourceCategory = this.categories[index]
    let destinationCategory = this.categories[index + offset]
    let sourcePosition = sourceCategory.position
    let destinationPosition = destinationCategory.position
    try {
      this.loading = true
      this.changeRef.markForCheck()
      let tasks = []
      tasks.push(this.chatbotApi.productCategory.update(sourceCategory._id, { position: destinationPosition }).then(res => {
        sourceCategory.position = destinationPosition
      }))
      tasks.push(this.chatbotApi.productCategory.update(destinationCategory._id, { position: sourcePosition }).then(res => {
        destinationCategory.position = sourcePosition
      }))
      await Promise.all(tasks)
      this.categories = this.categories.sort((a, b) => (a.position < b.position)?-1:(a.position > b.position?1:0))
      this.snackBar.open('🔃 Di chuyển thành công', '', { duration: 3000 });
    } catch(err) {
      this.alert.handleError(err, "Lỗi khi di chuyển")
    } finally {
      this.loading = false
      this.detectChanges()
    }
  }
  
  drop(event: CdkDragDrop<string[]>, category) {
    moveItemInArray(category.products, event.previousIndex, event.currentIndex);
    this.chatbotApi.productCategory.update(category._id, {
      listProductId: category.products.map(x => x._id)
    }).then(res => {
      this.snackBar.open('🔃 Di chuyển thành công', '', { duration: 1500 });
    })
  }

  async removeCategory(category) {
    if (!await this.alert.question("Xoá " + category.name, "Bạn có chắc muốn xóa loại sản phẩm này và TẤT CẢ SẢN PHẨM thuộc nó không?")) return
    try {
      this.loading = true
      this.detectChanges()
      await this.chatbotApi.productCategory.delete(category._id)
      let index = this.categories.findIndex(x => x._id == category._id)
      this.categories.splice(index, 1)
      this.snackBar.open('🗑️ Đã xoá loại sản phẩm', '', { duration: 3000 });
    } catch(err) {
      this.alert.handleError(err, "Lỗi khi xóa loại sản phẩm")
    } finally {
      this.loading = false
      this.detectChanges()
    }
  }
  
  async editCategoryName(event, id = null) {

    let category = id?this.categories.find(x => x._id == id):null

    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      target: id?'.name':'.btn-add',
      targetElement: event.target,
      width: '94%',
      confirm: id?'Đổi tên loại sản phẩm':'Tạo loại sản phẩm',
      horizontalAlign: id?'left':'center',
      verticalAlign: id?'top':'bottom',
      fields: [
        {
          placeholder: 'Tên loại sản phẩm',
          property: 'name',
          constraints: ['required'],
          current: id?category.name:''
        }
      ],
      onSubmit: async (data) => {
        if (!data.name) return
        try {
          this.loading = true
          this.detectChanges()
          if (id) {
            let index = this.categories.findIndex(x => x._id == id)
            let products = this.categories[index].products
            await this.chatbotApi.productCategory.update(id, { name: data.name })
            this.categories[index].name = data.name
            this.categories[index].products = products
          } else {
            let category = await this.chatbotApi.productCategory.addCategory(data.name, 
              (this.categories && this.categories.length)?this.categories[this.categories.length - 1].position + 1:1)
            category.products = []
            this.categories.push(category)
          }
          this.snackBar.open(`✔️ Đã ${id?'đổi tên loại sản phẩm':'thêm loại sản phẩm'}`, '', { duration: 3000 });
        } catch (err) {
          if (id) {
            this.alert.handleError(err, 'Lỗi khi đổi tên loại sản phẩm');
          } else {
            this.alert.handleError(err, 'Lỗi khi tạo loại sản phẩm');
          }
          this.snackBar.open(`❌ Lỗi khi ${id?'đổi tên loại sản phẩm':'thêm loại sản phẩm'}`, '', { duration: 3000 })
        } finally {
          this.loading = false
          this.detectChanges()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }
  
  async editShopName(event, edit = false) {
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      target: edit?'.shop-name':'.btn-create',
      targetElement: event.target,
      width: '96%',
      confirm: edit?'Đổi tên cửa hàng':'Tạo cửa hàng',
      horizontalAlign: edit?'left':'center',
      verticalAlign: edit?'top':'bottom',
      fields: [
        {
          placeholder: 'Tên cửa hàng',
          property: 'name',
          constraints: ['required'],
          current: edit?this.shop.name:''
        }
      ],
      onSubmit: async (data) => {
        if (!data.name) return
        try {
          if (edit) {
            this.shop.name = (await this.chatbotApi.shop.update(this.shop._id, { name: data.name })).name
          } else {
            this.loadDone = false
            this.detectChanges()
            this.shop = (await this.chatbotApi.shop.add({ name: data.name }))
            this.loadDone = true
          }
        } catch (err) {
          console.error(err)
          if (edit) {
            this.alert.handleError(err, "Lỗi khi đổi tên cửa hàng")
          } else {
            this.alert.handleError(err, "Lỗi khi tạo cửa hàng")
          }
        } finally {
          this.detectChanges()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  addPlace() {
    this.shop.places.push({
      name: '',
      fullAddress: '',
      lat: 10.7797855,
      long: 106.6990189
    })
    this.shopChanged()
    this.setupPlaceAutocomplete(this.shop.places.length - 1)
    let portalElement = document.getElementsByClassName(this.portalId).item(0)
    portalElement.scroll({ top: portalElement.scrollHeight, behavior: 'smooth' })
    document.getElementById('place-name-' + (this.shop.places.length - 1)).focus()
  }

  removePlace(index) {
    this.shop.places.splice(index, 1)
    this.shopChanged()
    this.setupPlacesAutocomplete()
  }
  
  setupPlacesAutocomplete() {
    if (!this.shop) return 
    this.autocompleteList.forEach(x => {
      google.maps.event.clearInstanceListeners(x)
    })
    this.autocompleteList = []

    for (let i = 0; i < this.shop.places.length; i++) {
      this.setupPlaceAutocomplete(i)
    }
    this.detectChanges()
  }

  setupPlaceAutocomplete(index) {
    let autocomplete = new google.maps.places.Autocomplete(document.getElementById('place-address-' + index) as HTMLInputElement, {
        types: ["address"],
        componentRestrictions: { country: "vn" },
    });
    autocomplete.addListener("place_changed", () => {
        //get the place result
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();

        //verify result
        if (place.geometry === undefined || place.geometry === null) {
            return;
        }

        this.shop.places[index].fullAddress = place.formatted_address

        //set latitude, longitude and zoom
        this.shop.places[index].lat = place.geometry.location.lat()
        this.shop.places[index].long = place.geometry.location.lng()
        this.shopChanged()
        // this.ref.detectChanges();
    })
    this.autocompleteList.push(autocomplete)
  }
  
  trackByFn(index, item) {
    return item._id; // or item.id
  }

  shopChanged() {
    this.showSaveButton()
    this.hasShopChanged = true
    this.detectChanges()
  }

  onShopImageChanged(event) {
    this.showSaveButton()
    this.shop.image = event;
    this.hasShopChanged = true
    this.detectChanges()
  }

  startDate
  endDate
  startDateText
  endDateText
  startDateChanged(event) {
    this.startDate = new Date(event.value)
    this.startDateText = moment(this.startDate).format('DD-MM-YYYY')
    this.loadOrders()
    setTimeout(() => {
      event.focus()
    })
  }

  endDateChanged(event) {
    this.endDate = new Date(event.value)
    this.endDateText = moment(this.endDate).format('DD-MM-YYYY')
    this.loadOrders()
    setTimeout(() => {
      event.focus()
    })
  }
  
  blurAll(){
    var tmp = document.createElement("input");
    document.body.appendChild(tmp);
    tmp.focus();
    document.body.removeChild(tmp);
   }

  toPreviousDate() {
    this.date.setDate(this.date.getDate() - 1)
    this.generateDateText()
    this.loadOrders()
    this.detectChanges()
  }

  toNextDate() {
    this.date.setDate(this.date.getDate() + 1)
    this.generateDateText()
    this.loadOrders()
    this.detectChanges()
  }

  dateText: string
  isToday: boolean = false
  generateDateText() {
    this.isToday = false
    this.dateText = '<b class="mr-1">'
    let today = new Date() 
    let yesterday = new Date()
    yesterday.setDate(today.getDate() - 1)
    if (today.getMonth() == this.date.getMonth() && today.getFullYear() == this.date.getFullYear()) {
      if (today.getDate() == this.date.getDate()) {
        this.dateText += 'Hôm nay, '
        this.isToday = true
      } else if (yesterday.getDate() == this.date.getDate()) {
        this.dateText += 'Hôm qua, '
      }
    }

    this.dateText += ['Chủ nhật', 'Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ bảy'][this.date.getDay()]
    this.dateText += '</b> ngày '
    this.dateText += this.date.getDate().toString() + '/'
    this.dateText += (this.date.getMonth() + 1).toString() + '/'
    this.dateText += this.date.getFullYear()
  }

  selectedOrder
  gotoDetailOrder(order){
    this.selectedOrder = order
    this.detectChanges()
  }
  gotoOrder(){
    this.selectedOrder = null
    this.detectChanges()
  }

  storyChanged(event) {
    this.shop.forwardStory = event
    this.shopChanged()
  }

  toggleSendReceipt() {
    this.shop.sendReceipt = !this.shop.sendReceipt
    this.shopChanged()
  }

  toggleEnableOTP() {
    this.shop.enableOTP = !this.shop.enableOTP
    this.shopChanged()
  }

  toggleGSheet() {
    this.shop.forwardGSheet = !this.shop.forwardGSheet
    this.shopChanged()
    if (this.shop.forwardGSheet) {
      this.loadGSheet()
    }
  }
  loadingGSheet$ = new BehaviorSubject<boolean>(false)
  gsheetUsers: GsheetUser[]
  async loadGSheet() {
    if (this.gsheetUsers) return
    try {
      this.loadingGSheet$.next(true)
      this.gsheetUsers = await this.chatbotApi.gsheetUser.getUser()
      if (this.gsheetUsers.length) this.shop.gsheetUser = this.gsheetUsers[0]
      this.changeRef.markForCheck()
    } catch (err) {
      console.error(err)
    } finally {
      this.loadingGSheet$.next(false)
    }
  }

  async connectGooogleSpreadsheet() {
    try {
      const response: any = await this.googleApiSrv.authorize()
      await this.chatbotApi.app.connectGSheet({ code: response.code })
      this.alert.success("Thành công", "Kết nối với google spreadsheet thành công")
    } catch (error) {
      this.alert.handleError(error);
    }
  } 
  async createSpreadsheet(event) {
    if (!this.shop.gsheetUser) {
      return this.alert.error("Chưa kết nối", "Thẻ này chưa kết nối với tài khoản người dùng google nào")
    }   
    let nameApp = this.chatbotApi.chatbotAuth.app.activePage['name']
    await this.popoverService.createPopoverForm({
      parent: '.portal-content-block',
      target: '.btn-add-sheet',
      targetElement: event.target,
      width: '280px',
      confirm: 'Tạo spreadsheet mới',
      horizontalAlign: 'right',
      verticalAlign: 'top',
      fields: [
        {
          placeholder: 'Tên spreadsheet',
          property: 'name',
          constraints: ['required'],
          current: ''
        }
      ],
      onSubmit: async (data) => {
        if (!data.name) return
        
        try {
          const ssInfo = await this.chatbotApi.gsheetUser.createSpreadsheet(this.shop.gsheetUser._id, { name:nameApp, sheets:[data.name] })
          await this.alert.success("Thành công", "Tạo bảng tính mới thành công")
          this.shop.gSheetId = ssInfo.spreadsheetId
          this.shopChanged()
          window.open(
            ssInfo.spreadsheetUrl,
            "_blank"
          )
        } catch (error) {
          this.alert.handleError(error)
        }
      }
    })
  }
  async openSpreadsheet(local = true) {
    if (!this.shop.gSheetId) return

    
    if (/^(\{{2})env\.([a-zA-Z0-9]*)(\}{2})$/g.test(this.shop.gSheetId)) {
      let envName = this.shop.gSheetId.replace(/({|}){2}/g, '').replace('env.', '')
      const { app } = this.chatbotApi.chatbotAuth
      let environment
      if (app.environment && typeof app.environment == "string") {
        environment = await this.chatbotApi.environment.getItem(app.environment, { local });
      } else {
        environment = await this.chatbotApi.environment.getItem((app.environment as iEnvironment)._id, { local });
      }
      window.open(
        `https://docs.google.com/spreadsheets/d/${environment.data[envName]}/edit`,
        "_blank"
      )
    } else {
      window.open(
        `https://docs.google.com/spreadsheets/d/${this.shop.gSheetId}/edit`,
        "_blank"
      )
    }
  }
  openToppings(product: iProduct) {
    let dialogRef = this.dialog.open(ToppingDialog, {
      width: '400px',
      data: product
    })

    dialogRef.afterClosed().toPromise().then(res => {
      if (res) {
        product.toppings = res
        this.snackBar.open(`✔️ Cập nhật topping thành công`, '', { duration: 3000 });
        this.changeRef.detectChanges()
      }
    })
  }

  async duplicateProduct(category, product: iProduct) {
    if (!await this.alert.question('Nhân bản sản phẩm', `Bạn có chắc chắn muốn nhân bản sản phẩm "${product.name}" không?`)) return

    try {
      this.loading = true
      this.changeRef.markForCheck()
      let newProduct = cloneDeep(product)
      delete newProduct._id
      let count = 1
      do {
        count++
        newProduct.name = `${product.name} (${count})`
      } while(category.products.find(x => x.name == newProduct.name))
      let result = await this.chatbotApi.product.add({ ...newProduct })
      this.products = [...this.products, result]
      category.products = [...category.products, result]
      this.snackBar.open(`✔️ Nhân bản sản phẩm thành công`, '', { duration: 3000 });
    } catch (err) {
      console.error(err)
      this.snackBar.open(`❌ Lỗi khi nhân bản sản phẩm`, '', { duration: 3000 })
    } finally {
      this.loading = false
      this.changeRef.detectChanges()
    }
  }

  exportingProducts$ = new BehaviorSubject<boolean>(false)
  importingProducts$ = new BehaviorSubject<boolean>(false)
  async exportProducts() {
    try {
      this.exportingProducts$.next(true)
      let data = await this.chatbotApi.shop.exportExcel()
      const blob = new Blob([data], {
        type:
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      });
      saveAs(
        blob,
        `mau_ds_san_pham.xlsx`
      );
    } catch (err) {
      console.error(err)
      this.alert.handleError(err, 'Lỗi xảy ra khi xuất dữ liệu')
    } finally {
      this.exportingProducts$.next(false)
    }
  }

  async importFileChanged(event) {
    let files = event.target.files
    if (files.length) {
      let file = files[0] as File
      try {
        this.importingProducts$.next(true)
        await this.chatbotApi.shop.importExcel(file)
        this.alert.success('Import sản phẩm thành công', 'Sản phẩm đã được import')
        this.loadProducts(false)
      } catch (err) {
        this.alert.handleError(err, 'Import sản phẩm thất bại')
      } finally {
        event.target.value = ''
        this.importingProducts$.next(false)
      }
    }
  }
}
