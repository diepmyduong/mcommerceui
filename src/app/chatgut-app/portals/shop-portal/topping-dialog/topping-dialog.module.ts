import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatProgressSpinnerModule, MatSnackBarModule } from '@angular/material';
import { AccountingPipeModule } from 'app/shared/pipes/accounting-pipe/accounting-pipe.module';
import { ToppingDialog } from './topping-dialog.component';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatDialogModule,
    DragDropModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    AccountingPipeModule
  ],
  declarations: [ToppingDialog],
  entryComponents: [ToppingDialog],
  exports: [ToppingDialog]
})
export class ToppingDialogModule { }
