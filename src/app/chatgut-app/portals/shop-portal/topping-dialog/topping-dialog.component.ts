import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { iProduct } from 'app/services/chatbot/api/crud/product';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { BehaviorSubject } from 'rxjs';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ChatbotApiService } from 'app/services/chatbot';
import { AlertService } from 'app/services/alert.service';
import { cloneDeep } from 'lodash-es'
@Component({
  selector: 'app-topping-dialog',
  styleUrls: ['./topping-dialog.component.scss'],
  templateUrl: './topping-dialog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToppingDialog implements OnInit {

  toppings$ = new BehaviorSubject<any[]>(undefined)
  set toppings(val) { this.toppings$.next(val) }
  get toppings() { return this.toppings$.getValue() }

  loading$ = new BehaviorSubject<boolean>(false)
  success$ = new BehaviorSubject<boolean>(false)

  toppingData$ = new BehaviorSubject<any>(undefined)
  overlay: Element

  constructor(
    public dialogRef: MatDialogRef<ToppingDialog>,
    @Inject(MAT_DIALOG_DATA) public product: iProduct,
    public popoverService: PopoverService,
    private alert: AlertService,
    private api: ChatbotApiService,
    private snackBar: MatSnackBar
    ) {
      this.toppings = product.toppings?cloneDeep(product.toppings):[]

      this.overlay = document.getElementsByClassName('cdk-global-overlay-wrapper').item(0)
      this.overlay.classList.add('overflow')
      this.overlay.addEventListener('click', event => this.overlayClickEvent(event) )
  }

  overlayClickEvent(e) {
    if (e.srcElement && e.srcElement.classList.contains('cdk-global-overlay-wrapper')) {
      this.onNoClick()
    }
  }

  ngOnInit() {
    let toppingData = sessionStorage.getItem('toppingData')
    if (toppingData) {
      let topping = JSON.parse(toppingData)
      if (topping.groupName) {
        this.toppingData$.next(topping)
      }
    }
  }

  openTopping(event, index) {
    let topping = index > -1 ? this.toppings[index] : null
    let formOptions: iPopoverFormOptions = {
      parent: '.toppings',
      target: '.topping-title',
      targetElement: event.target,
      width: '96%',
      confirm: topping?'Lưu thay đổi':'Tạo topping',
      horizontalAlign: 'center',
      verticalAlign: topping?'top':'bottom',
      fields: [
        {
          placeholder: 'Tên topping',
          property: 'groupName',
          constraints: ['required'],
          current: topping?topping.groupName:'',
          col: 'col-7'
        },
        {
          type: 'checkbox',
          placeholder: 'Bắt buộc',
          property: 'require',
          current: topping?topping.require:false,
          col: 'col-5'
        },
        {
          placeholder: 'Giới hạn số lượng (0 nếu không giới hạn)',
          property: 'limit',
          inputType: 'number',
          constraints: ['required'],
          current: topping?topping.limit:1,
          col: 'col-12'
        }
      ],
      onSubmit: async (data) => {
        if (!data.groupName) return
        let { groupName, require, limit } = data
        limit = Number(limit)
        require = require?true:false
        if (topping) {
          let toppings = this.toppings
          toppings[index] = {...toppings[index], ...{ groupName, require, limit }}
          this.toppings = [...toppings]
        } else {
          this.toppings = [...this.toppings, { groupName, require, limit }]
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  toppingDropped(event: CdkDragDrop<any[]>) {
    let toppings = this.toppings
    moveItemInArray(toppings, event.previousIndex, event.currentIndex)
    this.toppings = [...toppings]
  }

  openOption(event, topping, index) {
    let option = index > -1 ? topping.options[index] : null
    let formOptions: iPopoverFormOptions = {
      parent: '.toppings',
      target: '.topping-option',
      targetElement: event.target,
      width: '96%',
      confirm: topping?'Lưu thay đổi':'Tạo lựa chọn',
      horizontalAlign: 'center',
      verticalAlign: 'bottom',
      fields: [
        {
          placeholder: 'Lựa chọn',
          property: 'display',
          constraints: ['required'],
          current: option?option.display:'',
          col: 'col-7'
        },
        {
          inputType: 'number',
          placeholder: 'Giá tiền',
          property: 'price',
          current: option?option.price:'0',
          col: 'col-5'
        }
      ],
      onSubmit: async (data) => {
        let { display, price } = data
        price = Number(price)
        if (!display || isNaN(price)) return
        if (!topping.options) topping.options = []
        if (option) {
          topping.options[index] = {...topping.options[index], ...{ display, price }}
          this.toppings = [...this.toppings]
        } else {
          topping.options = [...topping.options, { display, price }]
          this.toppings = [...this.toppings]
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  optionDropped(event: CdkDragDrop<any[]>, topping) {
    moveItemInArray(topping.options, event.previousIndex, event.currentIndex)
    this.toppings = [...this.toppings]
  }

  async saveTopping() {
    try {
      this.loading$.next(true)
      await this.api.product.update(this.product._id, {
        toppings: this.toppings
      })
      this.success$.next(true)
      setTimeout(() => {
        this.dialogRef.close(this.toppings)
      }, 300)
    } catch (err) {
      console.error(err)
      this.alert.error('Lỗi khi lưu topping', 'Lưu topping thất bại.')
    } finally {
      this.loading$.next(false)
    }
  }

  deleteTopping(index) {
    let toppings = this.toppings
    toppings.splice(index, 1)
    this.toppings = [...toppings]
  }

  deleteOption(topping, index) {
    topping.options.splice(index, 1)
    this.toppings = [...this.toppings]
  }

  copyTopping(topping) {
    let newTopping = cloneDeep(topping)
    delete newTopping._id
    if (newTopping.options) {
      for (let option of newTopping.options) {
        delete option._id
      }
    }
    sessionStorage.setItem("toppingData", JSON.stringify(newTopping));
    this.snackBar.open(
      "✔️ Đã copy dữ liệu topping. Hãy bấm nút Paste dữ liệu vào sản phẩm cần thiết.",
      "",
      { duration: 2500 }
    );
    this.toppingData$.next(topping)
  }

  onPasteTopping() {
    this.toppings = [...this.toppings, cloneDeep(this.toppingData$.getValue())]
  }

  onNoClick() {
    this.dialogRef.close()
  }
}