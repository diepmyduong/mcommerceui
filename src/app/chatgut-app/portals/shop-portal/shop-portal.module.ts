import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MaterialModule } from 'app/shared';
import { ShopPortalComponent } from './shop-portal.component';
import { MatTooltipModule, MatProgressSpinnerModule, MatTabsModule, MatButtonModule, MatSelectModule, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS, MatSnackBarModule } from '@angular/material';
import { CardImageModule } from 'app/shared/cards/card-image/card-image.module';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from "@agm/core";
import { AccountingPipeModule } from 'app/shared/pipes/accounting-pipe/accounting-pipe.module';
import { AppDateAdapter, APP_DATE_FORMATS } from 'app/shared/date-adapter';
import { StorySelectionModule } from 'app/components/story-selection/story-selection.module';
import { ToppingDialogModule } from './topping-dialog/topping-dialog.module';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatTabsModule,
    CardImageModule,
    AccountingPipeModule,
    MatButtonModule,
    MatSnackBarModule,
    MatSelectModule,
    StorySelectionModule,
    DragDropModule,
    AgmCoreModule.forRoot({
        apiKey: "AIzaSyAHY5EcP_p37t5VgOxKeg_vZW2IbrdaTtw",
        libraries: ["places"],
        language: 'vi'
    }),
    FormsModule,
    ToppingDialogModule
  ],
  declarations: [ShopPortalComponent],
  entryComponents: [ShopPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ShopPortalComponent },
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
  ]
})
export class ShopPortalModule { }
