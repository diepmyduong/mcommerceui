import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { iInteractPost } from 'app/services/chatbot';
import * as moment from 'moment'
import { MatDialog } from '@angular/material';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { iCopyData, CopyDialog } from 'app/modals/copy/copy.component';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hash-tag-portal',
  templateUrl: './hash-tag-portal.component.html',
  styleUrls: ['./hash-tag-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class HashTagPortalComponent extends BasePortal implements OnInit {

  componentName = "HashTagPortalComponent"
  interactPosts: iInteractPost[] = [];
  interactPost_filter: string = ''
  openedInteractPost: string
  loadHashtagDone: boolean = false
  loading: boolean = false

  constructor (
    public dialog: MatDialog,
    public popoverService: PopoverService,
    private router: Router,
    public changeRef: ChangeDetectorRef
  ) {
    super()
    this.setPortalName('Hashtag')
  }

  async ngOnInit() {
    this.reload()
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(local: boolean = true) {
    this.loading = true
    this.detectChanges()
    const { app } = this.chatbotApi.chatbotAuth
    try {
      this.interactPosts = await this.chatbotApi.interactPost.getList({ local, 
        query: { filter: { type: "hash_tag", page: app.activePage }, order: { 'createdAt': 1 }, limit: 0 } }) as any
      console.log('interact posts', this.interactPosts)
    } catch (err) {
      // console.error("error: ", err)
      // this.alert.error("Lỗi khi lấy dữ liệu", err.message)
      this.alert.handleError(err);
    } finally {
      this.loadHashtagDone = true
      this.loading = false
      this.detectChanges()
    }
  }

  async removeInteractPost(interactPost: iInteractPost, event) {
    event.stopPropagation();
    if (!await this.alert.warning('Xoá hashtag', "Bạn có muốn xoá hashtag này không", 'Xóa')) return;
    try {
      await this.chatbotApi.interactPost.delete(interactPost._id);
      this.portalService.popPortal(this.index + 1);
      this.alert.success("Thành Công", "Xoá thành công");

    } catch (err) {
      // this.alert.error("Xoá không thành công", err.message)
      // console.error(err.error);
      this.alert.handleError(err, "Lỗi khi xóa");
    } finally {
      this.detectChanges()
    }
  }

  async openInteractPost(interactPost: iInteractPost) {
    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "PostDetailPortalComponent", { 
      portalName: interactPost.hashTag, interactPostId: interactPost._id })
    this.openedInteractPost = interactPost._id
    if (!componentRef) return;
    let component = componentRef.instance as any

    this.subscriptions.postClosed = component.onPostClosed.subscribe(() => {
      this.openedInteractPost = ''
      this.subscriptions.postClosed.unsubscribe()
      this.detectChanges()
    })
    this.detectChanges()
  }

  async addHashtag(event, center = false) {
    let formOptions: iPopoverFormOptions = {
      parent: '.swiper-slide',
      target: center?'.btn-create-hashtag':'.btn-blue',
      targetElement: event.target,
      width: '96%',
      confirm: 'Tạo hashtag',
      horizontalAlign: 'center',
      verticalAlign: 'bottom',
      fields: [
        {
          placeholder: 'Hashtag',
          property: 'name',
          constraints: ['required', 'hashtag']
        }
      ],
      onSubmit: async (data) => {
        if (!data.name) return
        try {
          this.loading = true
          this.detectChanges()
          const hashtag = data.name
          const interactPost = await this.chatbotApi.interactPost.add({
            name: hashtag,
            type: "hash_tag",
            hashTag: hashtag,
            amount: 0,
            limit: 0,
            startDate: moment().format() as any,
            endDate: moment().format() as any,
            cases: [{
              type: "fail",
              priority: 0,
              isResponseStory: false,
              isResponseComment: false,
              isResponsePrivateReplies: false,
              value: {
                regex: ""
              },
              isCheckReaction: false,
              isSendOnlyParentComment: false,
              amount: 0,
              limit: 0,
            }],
          })
          if (!this.openedInteractPost) {
            this.openInteractPost(interactPost)
          }
        } catch (err) {
          // console.error(err)
          // this.alert.error('Lỗi khi tạo hashtag', err.message)
          this.alert.handleError(err, 'Lỗi khi tạo hashtag');
        } finally {
          this.loading = false
          this.detectChanges()
          setTimeout(() => {
            let el = document.getElementById(this.portalId)
            el.scroll({ top: el.scrollHeight, behavior: 'smooth' })
          })
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  async removeHashtag(interactPost) {
    if (!await this.alert.warning('Xoá hashtag', "Bạn có muốn xoá hashtag này không?", 'Xóa')) return;
    try {
      this.loading = true
      this.detectChanges()
      await this.chatbotApi.interactPost.delete(interactPost._id);
      if (this.openedInteractPost == interactPost._id) {
        this.portalService.popPortal(this.index + 1);
      }
    } catch (err) {
      // this.alert.error("Lỗi khi xoá", err.message)
      // console.error(err.error);
      this.alert.handleError(err, "Lỗi khi xóa");
    } finally {
      this.loading = false
      this.detectChanges()
    }
  }

  async copyHashtag(interactPost: iInteractPost) {
    let data: iCopyData = {
      mode: 'hashtag',
      name: interactPost.name,
      type: 'Hashtag',
      interactPost: interactPost,
      app: this.chatbotApi.chatbotAuth.app
    }
    let dialogRef = this.dialog.open(CopyDialog, {
      width: '650px',
      data
    })
    let dialogClosedSub: Subscription
    dialogClosedSub = dialogRef.afterClosed().subscribe(async result => {
      if (result) {
        let { interactPost, app } = result
        if (app._id == this.chatbotApi.chatbotAuth.app._id) {
          await this.reload(false)
          this.openInteractPost(interactPost)
        } else {
          this.router.navigate((['apps'])).then((e)=>{
            this.router.navigate((['apps', app._id]), { queryParams:{ mode:"hashtag", item: JSON.stringify(interactPost) } })
          })
        }
      } else {
        if (dialogRef.componentInstance.result) {
          await this.reload(false)
        }
      }
      dialogClosedSub.unsubscribe()
    })
  }

  trackByFn(index, item) {
    return item._id
  }

}

