import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HashTagPortalComponent } from 'app/chatgut-app/portals/hash-tag-portal/hash-tag-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NameFilterPipeModule } from 'app/shared/pipes/name-filter-pipe/name-filter-pipe.module';
import { FormsModule } from '@angular/forms';
import { MatProgressSpinnerModule, MatToolbarModule, MatButtonModule, MatSnackBarModule, MatIconModule } from '@angular/material';
import { CopyModule } from 'app/modals/copy/copy.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CopyModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatToolbarModule,
    MatTooltipModule,
    NameFilterPipeModule,
    MatSnackBarModule,
    MatIconModule,
  ],
  declarations: [HashTagPortalComponent],
  entryComponents: [HashTagPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: HashTagPortalComponent }
  ]
})
export class HashTagPortalModule { }
