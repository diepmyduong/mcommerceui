import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoryDetailPortalComponent } from 'app/chatgut-app/portals/story-detail-portal/story-detail-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatToolbarModule, MatButtonModule, MatProgressSpinnerModule, MatIconModule, MatMenuModule, MatSnackBarModule, MatTooltipModule, MatCardModule } from '@angular/material';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { SelectionModule } from 'app/modals/selection/selection.module';
import { CardContainerModule } from 'app/shared/cards/card-container/card-container.module';
import { FormsModule } from '@angular/forms';
import { SendModule } from 'app/modals/send/send.module';
import { CopyModule } from 'app/modals/copy/copy.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CardHeaderModule } from 'app/shared/cards/card-header/card-header.module';
import { QuickCardModule } from 'app/components/quick-card/quick-card.module';
import { ButtonFormModule } from 'app/shared/buttons/button-form/button-form.module';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatIconModule,
    MatMenuModule,
    EditInfoModule,
    SelectionModule,
    CardContainerModule,
    FormsModule,
    MatSnackBarModule,
    CopyModule,
    SendModule,
    DragDropModule,
    CardHeaderModule,
    MatCardModule,
    QuickCardModule,
    ButtonFormModule
  ],
  declarations: [StoryDetailPortalComponent],
  entryComponents: [StoryDetailPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: StoryDetailPortalComponent }
  ]
})
export class StoryDetailPortalModule { }
