import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { environment } from '@environments';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { CopyDialog, iCopyData } from 'app/modals/copy/copy.component';
import { EditInfoDialog, iEditInfoDialogData } from 'app/modals/edit-info/edit-info.component';
import { iSelectionData, SelectionDialog } from 'app/modals/selection/selection.component';
import { SendDialog } from 'app/modals/send/send.component';
import { CardContainerRef, CardService, CARD_CODE_COMPONENT_LIST, DEFAULT_POPULAR_CARDS, DEFAULT_POPULAR_CARDS_BROADCAST } from 'app/services/card.service';
import { iApp, iPartner } from 'app/services/chatbot/api/crud/app';
import { iCard } from 'app/services/chatbot/api/crud/card';
import { iPage } from 'app/services/chatbot/api/crud/page';
import { iStory } from 'app/services/chatbot/api/crud/story';
import { BaseCard } from 'app/shared/cards/base-card';
import { CardContainerComponent } from 'app/shared/cards/card-container/card-container.component';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { cloneDeep, isEmpty, isObject, merge } from 'lodash-es';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { StoriesPortalComponent } from '../stories-portal/stories-portal.component';

@Component({
  selector: 'app-story-detail-portal',
  templateUrl: './story-detail-portal.component.html',
  styleUrls: ['./story-detail-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StoryDetailPortalComponent extends BasePortal implements OnInit {
  
  @Input() storyId: string;
  @Input() mode: "story" | "get_started" = "story"
  @Output() onStoryChanged = new EventEmitter()
  @Output() onStoryClosed = new EventEmitter()
  @ViewChild("cardContainer", { read: CardContainerComponent }) cardContainer: CardContainerComponent
  @ViewChild("quickRepliesContainer", { read: CardContainerComponent }) quickRepliesContainer: CardContainerComponent
  constructor(
    public snackBar: MatSnackBar,
    public selectionDialog: MatDialog,
    public cardService: CardService,
    public dialog: MatDialog,
    private popoverService: PopoverService,
    public changeRef: ChangeDetectorRef,
    public router: Router
  ) {
    super()
  }
  componentName = "StoryDetailPortalComponent"
  story: iStory = {}
  app: iApp
  innowayPartner: iPartner
  page: iPage
  loadDone = false
  cardContainerRef: CardContainerRef;
  quickRepliesCardContainerRef: CardContainerRef;
  stories: any[]
  apps:iApp[]
  selectApp:any[]
  showPopular: boolean = false
  loadPopularDone: boolean = false
  normal: boolean = false
  broadcast: boolean = false
  sequence: boolean = false
  // broadcastHistory: iBroadcast[]
  // loadingHistory: boolean = false

  sendingTest: boolean = false
  popularCards: any[]
  async ngOnInit() {
  }

  async ngAfterViewInit() {
    this.cardContainerRef = this.cardService.regisContainer(this.cardContainer)
    this.quickRepliesCardContainerRef = this.cardService.regisContainer(this.quickRepliesContainer)
    this.reload(false)
  }
  async ngOnDestroy() {
    this.onStoryClosed.emit()
    this.cardService.clearContainer(this.cardContainer)
    this.cardService.clearContainer(this.quickRepliesContainer)
    if (this.subscriptions.storyList) this.subscriptions.storyList.unsubscribe()
    super.ngOnDestroy()
  }

  async reload(local: boolean = true) {
  
    let tasks = []
    let story
    tasks.push(this.chatbotApi.story.getList({ local: true, query: {
      limit: 0,
      fields: ["_id", "name"],
      order: { 'createdAt': 1 },
      filter: { mode: 'normal' }
    } }).then( result => this.stories = result ) )
    tasks.push(this.chatbotApi.story.getItem(this.storyId, { local, query: { populates: ["cards"] } }).then(result => {
      story = result
    }))
    await Promise.all(tasks)
    console.log('story',story)
    this.setPortalName(story.name)
    this.story = merge({}, story)
    
    switch (this.story.mode) {
      case 'broadcast': this.broadcast = true; this.subName = 'Broadcast'; break;
      case 'sequence': this.sequence = true; this.subName = 'Câu chuyện'; break;
      case 'normal': default: this.normal = true; this.subName = 'Câu chuyện'; break;
    }
    // if (this.broadcast) this.loadBroadcastHistory(local)
    this.loadPopularCards()

    let sortedCards = story.cards
    if(sortedCards.length == 0) this.loadDone = true
    else {
      for (let card of sortedCards) {
        if (!card) continue;
        const compRef = await this.loadCardToView(card as iCard)
        const comp = compRef.instance as BaseCard;
        // comp.duplicateCard = this.duplicateCard.bind(this,card["_id"])
        if(!this.subscriptions.onFirstCardInited) {
          this.subscriptions.onFirstCardInited = comp.viewInited.subscribe(() => {
            this.loadDone = true;
            this.detectChanges()
            setTimeout(() => {
              try {
                let wrapper = document.getElementsByClassName(this.portalId).item(0).getElementsByClassName('card-wrapper')
                for (let i = 0; i < wrapper.length; i++) {
                    let item = wrapper.item(i)
                    if (i == 0) {
                        item.classList.add('active')
                    } else {
                        item.classList.remove('active')
                    }
                }
              } catch (err) {}
              this.subscriptions.onFirstCardInited.unsubscribe()
            })
          })
        } 
        comp.storyId = this.storyId;
      }
    
      this.cardContainer.updateIndex()
    }
    this.initQuickReplies();
    this.detectChanges()
  }

  // async loadBroadcastHistory(local = true) {
  //   this.loadingHistory = true
  //   this.detectChanges()
  //   this.chatbotApi.broadcast.getList({ local, query: { limit: 0, filter: { story: this.story._id }, 
  //     order: { createdAt: 1 } } }).then(async result => {
  //     console.log('broadcast', result)
  //     this.broadcastHistory = result
  //     let tasks = []
  //     for (let i = 0; i < this.broadcastHistory.length; i++) {
  //       let history = this.broadcastHistory[i]
  //       if ('SCHEDULED IN_PROGRESS'.includes(history.status)) {
  //         tasks.push(this.chatbotApi.broadcast.getItem(history._id).then(result => 
  //           this.broadcastHistory[i] = result
  //         ))
  //       }
  //     }
  //     await Promise.all(tasks)
  //     this.broadcastHistory.forEach(history => {
  //       history.reached = 0
  //       history.broadcasts.forEach(x => {
  //         history.reached += x.reached
  //       })
        
  //       switch(history.status) {
  //         case 'FINISHED': history.statusText = 'Hoàn thành'; break;
  //         case 'IN_PROGRESS': history.statusText = 'Đang gửi'; break;
  //         case 'CANCELED': history.statusText = 'Đã hủy'; break;
  //         case 'ERROR': history.statusText = 'Bị lỗi'; break
  //         case 'SCHEDULED': history.statusText = 'Đã đặt lịch'; break;;
  //       }

  //       switch (history.type) {
  //         case 'label': history.typeText = 'Nhãn'; break;
  //         case 'all': history.typeText = 'Tất cả'; break;
  //       }

  //       history.createdText = moment(history.createdAt).format('DD-MM-YYYY HH:mm')
  //     })
  //     this.loadingHistory = false
  //     this.detectChanges()
  //   })
  // }

  async onChanged() {
    this.showSaveButton()
    this.detectChanges()
  }

  async initQuickReplies() {
    this.quickRepliesCardContainerRef.pushCardComp("QuickReplyCardComponent", { quickReplies: this.story.quickReplies }, false)
  }

  async showStoryInfo() {
    await this.portalService.pushPortalAt(this.index + 1, "StoryInfoPortalComponent", { storyId: this.storyId })
  }

  async handleError() {
    for (let cardCmp of this.cardContainer.cardComps as BaseCard[]) {
      cardCmp.cardFrm.form.enable();
    }
    await this.hideLoading();
    await this.showSaveButton();
  }

  async saveContent() {
    try {
      //DISABLE FORM
      for (let cardCmp of this.cardContainer.cardComps as BaseCard[]) {
        cardCmp.allowCanBeSaved = false

        let validText = cardCmp.cardValid();
        if (validText) {
          await this.alert.error("Lỗi", validText)
          await this.handleError()
          return
        }
        cardCmp.cardFrm.form.disable()
        cardCmp.detectChanges()
      }

      this.quickRepliesContainer.cardComps[0].allowCanBeSaved = false
      this.quickRepliesContainer.cardComps[0].cardFrm.form.disable()
      this.quickRepliesContainer.cardComps[0].detectChanges()

      this.showSaving();
      this.detectChanges()

      const task = []
      task.push(this.cardContainer.saveAllCards())
      task.push(this.quickRepliesContainer.cardComps[0].saveQuickReplies())
      await Promise.all(task)

      let cardItems = []
      for (let cardComp of this.cardContainer.cardComps) {
        cardItems.push(cardComp.card)
      }

      this.story.cards = cardItems
      this.story.quickReplies = this.quickRepliesContainer.cardComps[0].quickReplies

      this.chatbotApi.story.setItem(this.story, { local: false, query: { populates: ["cards"] } })

      //ENABLE FORM AGAIN AND RESET IT
      this.quickRepliesContainer.cardComps[0].cardFrm.form.enable()
      this.quickRepliesContainer.cardComps[0].disableSaveDetection();
      this.quickRepliesContainer.cardComps[0].detectChanges()
      for (let cardCmp of this.cardContainer.cardComps as BaseCard[]) {
        // cardCmp.disableSaveDetection();
        cardCmp.cardFrm.form.enable();
        cardCmp.resetForm(cardCmp.cardFrm, cardCmp.card)
        cardCmp.detectChanges()
      }

      this.showSaveSuccess();
      this.detectChanges()
    } catch (err) {
      // console.log('err', err);
      // await this.alert.error("Xảy ra lỗi khi lưu", err.message)
      this.alert.handleError(err, "Lỗi khi lưu");
      //ENABLE FORM AGAIN AND RESET IT
      await this.handleError()
    } finally {
      this.detectChanges()
    }
  }

  async editName(event) {
    let formOptions: iPopoverFormOptions = {
      parent: '.swiper-slide',
      targetElement: event.target,
      width: 300,
      fields: [
        {
          placeholder: 'Tên câu chuyện',
          property: 'name',
          current: this.story.name
        }
      ],
      onSubmit: (data) => {
        if (this.story.name == data.name || !data.name) return
        let fallback = this.story.name
        this.story.name = data.name
        this.portalName = data.name
        this.detectChanges()
        this.chatbotApi.story.updateName(this.story._id, this.story.name, { reload: true })
        .then(result => {
          let storiesListPortal = this.portalService.container.portals[0] as any
          if (storiesListPortal) {
            storiesListPortal.refresh()
          }
          this.detectChanges()
        })
        .catch(err => {
          console.error(err)
          this.story.name = fallback
          this.portalName = fallback
          this.detectChanges()
        })
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  async loadCardToView(card: iCard, scroll: boolean = false, pos = -1) {
    if (!card) return;
    // let cardComponentName = CARD_CODE_COMPONENT_LIST.find(x => x.code == card.type).component
  
    let res = CARD_CODE_COMPONENT_LIST.find(x => x.code == card.type)
    let cardComponentName = res.component
    let extra = res['extra']
    let compRef = await this.cardContainerRef.pushCardComp(cardComponentName, { card , extra}, scroll, pos)
    this.detectChanges()
    return compRef;
  }
  async addCard(type: string, options = {} as any, scroll = true, position = -1, condition = '', isBreak = false) {
    if(!type) return
    let comp;
    //this.showLoading();
    try {
      //let card = { _id: ""} as any;
      let card = {} as any;
      let compRef;
      switch (type) {
        case 'story': await this.portalService.pushPortalAt(this.index + 1, "StoriesPortalComponent"); return;
        default: card = merge(card, await this.chatbotApi.cardBuilder[type]()); break;
      }
      
      if (!card.option) return;
 
      if (condition) card.condition = condition
      card.isBreak = isBreak

      if (isEmpty(options)) {
        card.option = merge(card.option, options)
      } else {
        card.option = options
      }
      compRef = await this.loadCardToView(card, scroll, position)
      comp = compRef.instance as BaseCard;
      comp.operation = 'add';
      comp.storyId = this.storyId;
      // comp.duplicateCard = this.duplicateCard.bind(this,card["_id"])
      this.showSaveButton();
      this.cardContainer.updateIndex()
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi thêm thẻ");
      // this.alert.error("Có lỗi xảy ra khi thêm thẻ", err.message)
      // console.error(err)
    } finally {
      //this.hideLoading()
      this.detectChanges()
      return comp;
    }
  }

  openSnackBar(message: string, duration: number = 1000, action: string) {
    this.snackBar.open(message, action, {
      duration
    });
  }
  async getDataSelection(){
    let data: iSelectionData = {
      title: 'Thêm thẻ mới',
      confirmButtonText: 'Chọn loại thẻ',
      cancelButtonText: 'Quay lại',
      categories: []
    }

    if (this.broadcast) {
      data.categories =  [{
        name: 'Cơ bản',
        options: [
          { code: 'button', display: 'Tin nhắn', icon: 'fas fa-font' },
          { code: 'image', display: 'Hình ảnh', icon: 'far fa-image' },
          { code: 'audio', display: 'Âm thanh', icon: 'fas fa-music' },
          { code: 'video', display: 'Video', icon: 'fas fa-video' },
          // { code: 'button', display: 'Nút bấm', icon: 'fab fa-stack-exchange' },
          { code: 'generic', display: 'Hình và nút', icon: 'fas fa-newspaper' },
          { code: 'list', display: 'Mẫu danh sách', icon: 'far fa-list-alt' },
          { code: 'media_image', display: 'Mẫu hình ảnh', icon: 'far fa-images' },
          { code: 'media_video', display: 'Mẫu video', icon: 'far fa-file-video' },
        ]
      }]
    } else {
      data.categories = [{
        name: 'Cơ bản',
        options: [
          { code: 'button', display: 'Tin nhắn', icon: 'fas fa-font' },
          { code: 'image', display: 'Hình ảnh', icon: 'far fa-image' },
          { code: 'audio', display: 'Âm thanh', icon: 'fas fa-music' },
          { code: 'video', display: 'Video', icon: 'fas fa-video' },
          // { code: 'button', display: 'Nút bấm', icon: 'fab fa-stack-exchange' },
          { code: 'generic', display: 'Hình và nút', icon: 'fas fa-newspaper' },
          { code: 'media_image', display: 'Mẫu hình ảnh', icon: 'far fa-images' },
          { code: 'media_video', display: 'Mẫu video', icon: 'far fa-file-video' },
          { code: 'list', display: 'Mẫu danh sách', icon: 'far fa-list-alt' },
          { code: 'typing_on', display: 'Thẻ chờ', icon: 'far fa-keyboard' },
          { code: 'go_to_story', display: 'Chuyển câu chuyện', icon: 'fas fa-book' },
          // { code: 'story', display: 'Từ câu chuyện khác', icon: 'fas fa-copy' },
        ]
      }, {
        name: "Hành động",
        canToggle: true,
        defaultToggle: false,
        options: [
          { code: 'action_api', display: 'Gọi API', icon: 'fas fa-bolt' },
          { code: 'action_update', display: 'Lưu dữ liệu', icon: 'fas fa-save' },
          { code: 'action_json_input', display: 'Dữ liệu JSON', icon: 'fab fa-node-js' },
          { code: 'action_parse_html', display: 'Dữ liệu Website', icon: 'fas fa-globe' },
          { code: 'action_sequence', display: 'Gắn/hủy luồng', icon: 'fas fa-stream' },
          { code: 'action_subscriber_tag', display: 'Gắn/gỡ tag', icon: 'fas fa-tags' },
          { code: 'action_notify', display: 'Thông báo', icon: 'fas fa-bell' },
          { code: 'action_send_story', display: 'Gửi câu chuyện', icon: 'fas fa-paper-plane' },
          { code: 'action_e_voucher', display: 'Khuyến mãi', icon: 'fas fa-ticket-alt' },
          { code: 'action_member_point', display: 'Tích điểm', icon: 'fas fa-dice' },
        ]
      }, {
        name: 'Nâng cao',
        canToggle: true,
        defaultToggle: false,
        options: [
          { code: 'generic_dynamic', display: 'Menu động', icon: 'fas fa-file-code' },
          { code: 'barcode', display: 'QR/Barcode', icon: 'fas fa-barcode' },
          { code: 'voice_response', display: 'Đọc văn bản', icon: 'fas fa-microphone-alt' }, 
          { code: 'activity_qrcode', display: 'Quét QR code', icon: 'fas fa-qrcode' },
          { code: 'activity_input', display: 'Nhập chuỗi', icon: 'fas fa-keyboard' },
          { code: 'activity_quickreply', display: 'Trả lời nhanh', icon: 'fas fa-comment-alt' },
          { code: 'activity_range', display: 'Nhập số', icon: 'fas fa-sort-numeric-down' },
          { code: 'activity_media', display: 'Gửi File', icon: 'fas fa-paperclip' },
          { code: 'activity_dynamic_quickreply', display: 'Gợi ý động', icon: 'fas fa-comments' },
          { code: 'activity_dynamic_form', display: 'Biểu mẫu', icon: 'fas fa-clipboard' },
          { code: 'activity_button', display: 'Tương tác nút bấm', icon: 'fab fa-stack-exchange' },
          { code: 'action_compare_image', display: 'So sánh ảnh', icon: 'fas fa-exchange-alt'},
          { code: 'action_google_spreadsheet', display: 'Google spreadsheet', icon: 'fas fa-table'},
          { code: 'activity_datepicker', display: 'Lấy dữ liệu ngày', icon: 'far fa-calendar-check' },
          // { code: 'infusion_add_contact', display: 'Thêm contact Infusion', icon: 'far fa-plus-square'},
          // { code: 'infusion_update_contact', display: 'Cập nhật contact Infusion', icon: 'fas fa-wrench'},
          // { code: 'infusion_achive_goal', display: 'Thêm luồng Infusion', icon: 'fas fa-project-diagram'},
        ]
      }]

      
      // this.innowayPartner = this.app.partners.find(p => p.partner == 'innoway')
  
      // if (this.innowayPartner) {
      //   data.categories.push({
      //     name: 'Innoway',
      //     canToggle: true,
      //     defaultToggle: false,
      //     options: [
      //       { code: 'generic_categories', display: 'Loại sản phẩm', icon: 'fas fa-cubes' },
      //       { code: 'generic_products', display: 'Sản phẩm', icon: 'fas fa-cube' },
      //       { code: 'list_categories', display: 'Danh sách mục sản phẩm', icon: 'fas fa-th-list' },
      //       { code: 'list_products', display: 'Danh sách sản phẩm', icon: 'fas fa-list' },
      //       { code: 'action_innoway_api', display: 'Gọi API', icon: 'fas fa-bolt' },
      //     ]
      //   })
      // }
    }
    return data
  }
  async openCreateCardDialog(index = null) {
    if (this.broadcast && this.cardContainer.cardComps.length >= 1) {
      this.alert.info('Chỉ được gửi một thẻ', 'Câu chuyện broadcast không được gửi quá 1 thẻ')
      return
    }
    let data = await this.getDataSelection()
  
    let dialogRef = this.selectionDialog.open(SelectionDialog, {
      width: '600px',
      data: data
    });
    dialogRef.afterClosed().toPromise().then(async result => {
      if (result) {
        if (index != null) {
          this.addCard(result, {}, true, index)
        } else {
          this.addCard(result);
        }
      }
    })
  }

  async sendStory() {
    if (this.showSave || this.isSaving) {
      this.alert.info('Không thể gửi', 'Không thể gửi câu chuyện khi câu chuyện chưa lưu hoặc đang lưu')
      return;
    }

    this.dialog.open(SendDialog, {
      width: '700px',
      data: {
        contentId: this.storyId,
        contentName: this.story.name,
        contentMode: this.story.mode
      }
    })
  }

  scrollToTop() {
    this.cardContainer.scrollToTop()
  }

  scrollToBottom() {
    this.cardContainer.scrollToBottom()
  }

  async openTasks() {
    await this.portalService.pushPortalAt(this.index + 1, "StoryManagePortalComponent", {
      story: this.story,
      portalName: 'Quản lý ' + this.story.name,
    })
  }

  // async openStoryStatistics() {
  //   const storyId = this.storyId
  //   const storyName = this.portalName
  //   let componentRef = await this.portalService.pushPortalAt(this.index + 1, "StatisticsDetailsResultPortalComponent", {
  //     mode: 'story',
  //     storyId: storyId,
  //     portalName: 'Thống kê',
  //     name: storyName,
  //     hasFooter: false
  //   })
  // }

  async testStory() {

    if (this.showSave || this.isSaving) {
      this.alert.info('Không thể gửi', 'Không thể gửi câu chuyện khi câu chuyện chưa lưu hoặc đang lưu')
      return;
    }

    if (environment.production == true) {
      if (this.portalService.getLastPortalName() != "ChatpluginPortalComponent") {
        this.portalService.pushPortalAt(this.index + 1, "ChatpluginPortalComponent")
      }
    }
    if (this.sendingTest) return;

    try { 
      this.sendingTest = true
      this.detectChanges()
      let testResult = (await this.chatbotApi.story.test(this.storyId)).object
      this.chatbotApi.socket.onTaskChange.emit({
        type: 'test',
        testResult
      })
      console.log(testResult)
    } catch (err) {
      console.error(err.error);
      if (err.error.type == 'app_exception_must_regis_tester' || err.error.type == 'database_exception_record_not_found') {
        if (!await this.alert.question('Test câu chuyện', 'Bạn có muốn đăng ký test trang này. Một cuộc nói chuyện trên Messenger sẽ được mở trên tab mới.',
        'Mở Messenger', 'Quay lại', async () => {          
          let popup = window.open(
            '',
            '_blank'
          )
          let pageId = ((await this.chatbotApi.chatbotAuth.app.activePage) as any).pageId
          let appAccessToken = await this.chatbotApi.chatbotAuth.app.accessToken
          let url = 'https' + `://m.me/${pageId}?ref=regis.${appAccessToken}`
          popup.location.href = url
        })) return
      } else {
        this.alert.handleError(err, "Lỗi khi test câu chuyện");
        // await this.alert.error('Có lỗi xảy ra khi test câu chuyện', err.message)
      }
    } finally {
      this.sendingTest = false
      this.detectChanges()
    }
  }

  async openStoryGuide(){
    
  }
  async addQuickMessage() {
    event.stopPropagation();
    let data: iEditInfoDialogData = {
      title: "Thêm tin nhắn nhanh",
      confirmButtonText: "Thêm",
      confirmButtonClass: 'i-create-story-confirm-button',
      cancelButtonText: "Quay lại",
      inputs: [
        {
          title: "Tin nhắn nhanh",
          property: "message",
          type: "text",
          current: '#',
          required: true,
        }
      ]
    };

    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    });
    dialogRef.afterClosed().toPromise().then(async result => {
      if (result) {
        const message = /^\#/g.test(result.message) ? result.message : "#" + result.message;
        try {
          await this.chatbotApi.page.addQuickMessage(message, this.story._id)    
          this.alert.success("Thêm thành công",`Thêm tin nhắn nhanh cho câu chuyện ${this.story.name} thành công`)  
        } catch (err) {
          // console.error("err: ", err.error.message);
          this.alert.handleError(err, "Lỗi khi thêm tin nhắn nhanh");
          // this.alert.error("Không thể thêm tin nhắn nhanh", err.message)
          this.progressBar.show = false;
        } finally {
          this.detectChanges()
        }
      }
    })
  }
  isExpire(date){
    if(!date) return false
    let expiredMoment = moment(date);
    let expiredMomentUTC = expiredMoment.utc()
    let nowUTC = moment.utc()
    let diffDuration = moment.duration(expiredMomentUTC.diff(nowUTC))
    let minDiff = diffDuration.asMinutes();
    if (minDiff > 0) {
      return false
    }else{
      return true
    }
  }

  async duplicateCard(cardIndex, sameStory) {
    let cardComp = this.cardContainer.cardComps[cardIndex]
    if (sameStory) {
      this.addCard(cardComp.card.type, cardComp.card.option, true, cardIndex + 1, cardComp.card.condition, cardComp.card.isBreak)
    } else {
      if (cardComp.operation == 'add') {
        this.alert.info('Không thể sao chép', 'Không thể sao chép thẻ mới tạo sang câu chuyện khác. Yêu cầu lưu câu chuyện trước khi sao chép.')
        return
      }
      let data: iCopyData = {
        mode: 'card',
        name: cardComp.cardName,
        type: 'Thẻ',
        card: cardComp.card,
        story: this.story
      }
      let dialogRef = this.dialog.open(CopyDialog, {
        width: '650px',
        data
      })
      let dialogClosedSub: Subscription
      dialogClosedSub = dialogRef.afterClosed().subscribe(result => {
        if (result) {
          let storiesPortal = this.portalService.container.portals[0] as StoriesPortalComponent
          storiesPortal.openStory(result)
        }
        dialogClosedSub.unsubscribe()
      })
    }
    
  }

  async loadPopularCards() {
    if(!this.page){
      this.app = this.chatbotApi.chatbotAuth.app
      if (isObject(this.app.activePage)) {
        this.page = this.app.activePage as iPage
      } else {
        this.page = await this.chatbotApi.page.getItem(this.app.activePage as string)
      }
    }
    // let popularCards = localStorage.getItem(`${this.page._id}-popularsCard${this.story.mode =="broadcast"?"-broadcast":""}`)
    // if(!popularCards){
      this.popularCards = this.story.mode =="broadcast"? cloneDeep(DEFAULT_POPULAR_CARDS_BROADCAST):cloneDeep(DEFAULT_POPULAR_CARDS)
    // } else{
    //   this.popularCards = JSON.parse(popularCards)
    // }
    // add action
    for (let card of this.popularCards) {
      card.action = (index = -1) => { this.addCard(card.code, {}, true, index) } 
    }
    this.loadPopularDone = true
    this.detectChanges()
    console.log( 'popularCard', this.popularCards)
  }

  dynamicQuickCardIndex: number
  dynamicQuickCardId: string
  createDialogFunc = (index) => this.openCreateCardDialog(index)
  dynamicQuickCardClick = () => {
    let card = document.getElementById(this.dynamicQuickCardId)
    if (!card) return
    card.style.marginTop = '12px'
    this.dynamicQuickCardId = null
    this.detectChanges()
    document.removeEventListener('click', this.dynamicQuickCardClick)
  }

  async openQuickCards(index, id) {
    if (this.dynamicQuickCardId == id) {
      await new Promise(r => setTimeout(r, 300))
      return
    }
    try {
      if (this.dynamicQuickCardId) {
        this.dynamicQuickCardClick()
        await new Promise(r => setTimeout(r, 300))
      }
      let card = document.getElementById(id);
      // console.log(index, id, card)
      document.getElementById(this.portalId + '-dynamic-quick-card').style.top = (card.offsetTop - 12 ) + 'px'
      card.style.marginTop = '100px'
      this.dynamicQuickCardIndex = index
      this.dynamicQuickCardId = id
      this.detectChanges()
    } catch (err) {

    } finally {
      setTimeout(() => {
        document.addEventListener('click', this.dynamicQuickCardClick)
      }, 300)
    }
  }
  // async editQuickCard(index){
  //   console.log( 'edit card', index)
  //   let data = await this.getDataSelection()
  
  //   let dialogRef = this.selectionDialog.open(SelectionDialog, {
  //     width: '600px',
  //     data: data
  //   });
  //   dialogRef.afterClosed().toPromise().then(async result => {
  //     console.log( 'res------',result)
  //     if (result) {
  //       let res = CARD_CODE_COMPONENT_LIST.find(card=> card.code == result)
  //       if(res){
  //         this.popularCards[index] = res
  //         localStorage.setItem(`${this.page._id}-popularsCard${this.story.mode =="broadcast"?"-broadcast":""}`, JSON.stringify(this.popularCards))
  //         this.popularCards = JSON.parse(JSON.stringify(this.popularCards))
  //         for (let card of this.popularCards) {
  //           card.action = (index = -1) => this.addCard(card.code, {}, true, index)
  //         }
  //         this.detectChanges()
  //       }else{
  //         this.alert.error("Lỗi", "Không tìm thấy card trong danh sách")
  //       }
  //     }
  //   })
  // }
}
