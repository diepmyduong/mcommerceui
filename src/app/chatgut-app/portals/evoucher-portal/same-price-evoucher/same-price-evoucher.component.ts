import { ChangeDetectionStrategy, Component, Input, EventEmitter, Output } from '@angular/core';
import { iEvoucher } from 'app/services/chatbot/api/crud/evoucher';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-same-price-evoucher',
  templateUrl: './same-price-evoucher.component.html',
  styleUrls: ['./same-price-evoucher.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SamePriceEvoucherComponent {
  @Input() disabled: boolean = false
  @Input() evoucher: iEvoucher
  @Output() dataChange = new EventEmitter<any>()

  constructor() {
  }

  ngOnInit() {
  }

  applyItemsChanged(event) {
    this.evoucher.option.applyItemType = event.applyItemType
    this.evoucher.option.applyItems = event.applyItems
    this.dataChange.emit()
  }

  dataChanged() {
    this.dataChange.emit()
  }
}
