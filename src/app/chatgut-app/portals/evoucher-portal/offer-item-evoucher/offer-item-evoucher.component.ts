import { ChangeDetectionStrategy, Component, Input, EventEmitter, Output } from '@angular/core';
import { iEvoucher } from 'app/services/chatbot/api/crud/evoucher';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-offer-item-evoucher',
  templateUrl: './offer-item-evoucher.component.html',
  styleUrls: ['./offer-item-evoucher.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OfferItemEvoucherComponent {
  @Input() disabled: boolean = false
  @Input() evoucher: iEvoucher
  @Output() dataChange = new EventEmitter<any>()

  constructor() {
  }

  ngOnInit() {
  }

  offerItemsChanged(event) {
    this.evoucher.option.offerItems = event.applyItems
    this.dataChange.emit()
  }

  dataChanged() {
    this.dataChange.emit()
  }
}
