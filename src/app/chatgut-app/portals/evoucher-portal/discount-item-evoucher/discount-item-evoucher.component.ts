import { ChangeDetectionStrategy, Component, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { iEvoucher } from 'app/services/chatbot/api/crud/evoucher';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-discount-item-evoucher',
  templateUrl: './discount-item-evoucher.component.html',
  styleUrls: ['./discount-item-evoucher.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DiscountItemEvoucherComponent {
  @Input() disabled: boolean = false
  @Input() evoucher: iEvoucher
  @Output() dataChange = new EventEmitter<any>()

  mode$ = new BehaviorSubject<'for' | 'from'>(undefined)

  constructor() {
  }

  ngOnInit() {
    this.checkDiscountCondition()
  }

  checkDiscountCondition() {
    if (this.evoucher.option.inputDiscountForItem) {
      delete this.evoucher.option.inputDiscountFromItem
      this.mode$.next('for')
    } else if (this.evoucher.option.inputDiscountFromItem) {
      delete this.evoucher.option.inputDiscountForItem
      this.mode$.next('from')
    } else {
      delete this.evoucher.option.inputDiscountFromItem
      delete this.evoucher.option.inputDiscountForItem
      this.mode$.next(undefined)
    }
  }

  applyItemsChanged(event) {
    this.evoucher.option.applyItemType = event.applyItemType
    this.evoucher.option.applyItems = event.applyItems
    this.dataChange.emit()
  }

  dataChanged() {
    this.checkDiscountCondition()
    this.dataChange.emit()
  }
}
