import { ChangeDetectionStrategy, Component, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { iEvoucher } from 'app/services/chatbot/api/crud/evoucher';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';

@Component({
  selector: 'app-manually-evoucher',
  templateUrl: './manually-evoucher.component.html',
  styleUrls: ['./manually-evoucher.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManuallyEvoucherComponent {
  @Input() disabled: boolean = false
  @Input() evoucher: iEvoucher
  @Output() dataChange = new EventEmitter<any>()

  @ViewChild('autoSize') autoSizeTextarea: CdkTextareaAutosize

  constructor() {
  }

  ngOnInit() {
    setTimeout(() => {this.autoSizeTextarea.resizeToFitContent(true)})
  }

  dataChanged() {
    this.dataChange.emit()
  }
}
