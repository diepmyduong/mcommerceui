import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter } from '@angular/core';
import { ChatbotApiService } from 'app/services/chatbot';
import { iProduct } from 'app/services/chatbot/api/crud/product';
import { iProductCategory } from 'app/services/chatbot/api/crud/productCategory';
import { cloneDeep } from 'lodash-es';
import { BehaviorSubject } from 'rxjs';
import { AlertService } from 'app/services/alert.service';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';

@Component({
  selector: 'product-picker',
  templateUrl: './product-picker.component.html',
  styleUrls: ['./product-picker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductPickerComponent {
  @Input() disabled: boolean = false
  @Input() applyItemType: 'category' | 'product' | 'all' = 'category'
  @Input() applyItems: any[] = []

  @Input() disableChangeItemType: boolean = false
  @Input() allowSelectQuantity: boolean = false
  @Input() hasAllType: boolean = false
  
  @Output() applyItemsChange = new EventEmitter<any>()

  constructor(
    private chatbotApi: ChatbotApiService,
    private alert: AlertService,
    private popoverService: PopoverService
  ) {
  }

  searchText = ''
  loadProductsDone$ = new BehaviorSubject<boolean>(false)

  categories$ = new BehaviorSubject<iProductCategory[]>(undefined)
  set categories(val) { this.categories$.next(val) }
  get categories() { return this.categories$.getValue() }
  products$ = new BehaviorSubject<iProduct[]>(undefined)
  set products(val) { this.products$.next(val) }
  get products() { return this.products$.getValue() }

  selectedCategories$ = new BehaviorSubject<iProductCategory[]>([])
  set selectedCategories(val) { this.selectedCategories$.next(val) }
  get selectedCategories() { return this.selectedCategories$.getValue() }
  selectedProducts$ = new BehaviorSubject<iProduct[]>([])
  set selectedProducts(val) { this.selectedProducts$.next(val) }
  get selectedProducts() { return this.selectedProducts$.getValue() }

  async ngOnInit() {
    this.loadProducts()
  }

  async loadProducts(local = true) {
    this.loadProductsDone$.next(false)
    try{
      let tasks = []
      tasks.push(this.chatbotApi.productCategory.getList({ 
        local, query: { fields: ["_id", "name", "position", "listProductId"], limit: 0, order: { position: 'asc' }} 
      }).then(result => { this.categories = cloneDeep(result) }))
      tasks.push(this.chatbotApi.product.getList({ 
        local, query: { fields: ["_id", "name", "description", "image", "category", "price", "toppings"], limit: 0, order: { createdAt: 1 }} 
      }).then(result => { this.products = cloneDeep(result) }))
      await Promise.all(tasks)
      
      let categories = this.categories
      categories.forEach(category => {
        category.products = category.listProductId.map(x => this.products.find(y => y._id == x)).filter(x => x)
      })
      this.categories =  categories
      
      this.products = this.categories.reduce((arr: any, cat) => ([ ...arr, ...cat.products.map(x => x._id) ]), []).map(x => this.products.find(y => y._id == x))

      if (!this.applyItems) this.applyItems = []
      if (this.applyItemType == 'category') {
        this.selectedCategories = this.categories.filter(x => this.applyItems.includes(x._id))
        this.selectedCategories.forEach(x => {
          let index = this.categories.findIndex(y => y._id == x._id)
          this.categories[index].checked = true
        })
        this.categories = [...this.categories]
      } else {
        if (this.allowSelectQuantity) {
          this.selectedProducts = this.products.filter(x => this.applyItems.find(y => y.id == x._id))
          this.selectedProducts.forEach(x => {
            let index = this.products.findIndex(y => y._id == x._id)
            this.products[index].checked = true
            let item = this.applyItems.find(y => y.id == x._id)
            this.products[index].qty = item.qty
          })
        } else {
          this.selectedProducts = this.products.filter(x => this.applyItems.includes(x._id))
          this.selectedProducts.forEach(x => {
            let index = this.products.findIndex(y => y._id == x._id)
            this.products[index].checked = true
          })
        }
        this.products = [...this.products]
      }
    } catch(err) {
      this.alert.handleError(err, 'Lỗi khi lấy sản phẩm')
    } finally {
      this.loadProductsDone$.next(true)
    }
  }

  toggleCategory(id) {
    let category = this.categories.find(x => x._id == id)
    category.checked = !category.checked
    if (category.checked) {
      this.selectedCategories.push(category)
    } else {
      this.selectedCategories.splice(this.selectedCategories.findIndex(x => x._id == id), 1)
    }
    this.selectedCategories = [...this.selectedCategories]
    this.categories = [...this.categories]
    this.dataChanged()
  }

  toggleProduct(id, event = null) {
    let product = this.products.find(x => x._id == id)
    if (this.allowSelectQuantity && !product.checked) {
      this.openSelectQuantity(event, product)
      return
    }
    product.checked = !product.checked
    if (product.checked) {
      this.selectedProducts.push(product)
    } else {
      this.selectedProducts.splice(this.selectedProducts.findIndex(x => x._id == id), 1)
    }
    this.selectedProducts = [...this.selectedProducts]
    this.products = [...this.products]
    this.dataChanged()
  }

  searchChanged() {
    this.categories.forEach(x => {
      if (this.searchText && this.convertViToEng(x.name.toLowerCase().trim())
      .indexOf(this.convertViToEng(this.searchText.toLowerCase().trim())) == -1) {        
        x.hidden = true
      } else {
        x.hidden = false
      }
    })
    this.categories = [...this.categories]
    
    this.products.forEach(x => {
      if (this.searchText && this.convertViToEng(x.name.toLowerCase().trim())
      .indexOf(this.convertViToEng(this.searchText.toLowerCase().trim())) == -1) {
        x.hidden = true
      } else {
        x.hidden = false
      }
    })
    this.products = [...this.products]
  }

  clear() {
    if (this.applyItemType == 'category') {
      this.selectedCategories = []
    } else {
      this.selectedProducts = []
    }
    this.dataChanged()
  }

  dataChanged() {
    let applyItems 
    if (this.allowSelectQuantity) {
      applyItems = this.selectedProducts.map(x => ({ _id: x._id, qty: Number(x.qty) }))
    } else {
      applyItems = (this.applyItemType=='category')?this.selectedCategories.map(x => x._id):this.selectedProducts.map(x => x._id)
    }
    this.applyItemsChange.emit({
      applyItemType: this.applyItemType,
      applyItems
    })
  }

  async openSelectQuantity(event, product) {
    let formOptions: iPopoverFormOptions = {
      parent: '.items',
      target: '.item',
      targetElement: event.target,
      width: '220px',
      confirm: 'Chọn món',
      horizontalAlign: 'center',
      verticalAlign: 'top',
      fields: [
        {
          placeholder: 'Số lượng món',
          property: 'qty',
          inputType: 'number',
          constraints: ['required'],
          current: 1
        }
      ],
      onSubmit: async (data) => {
        product.checked = true
        product.qty = Number(data.qty)
        this.selectedProducts.push(product)
        this.selectedProducts = [...this.selectedProducts]
        this.products = [...this.products]
        this.dataChanged()
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  convertViToEng(string) {
    let obj = {
        Đ: 'D', đ: 'd', â: 'a',
        ă: 'a', ê: 'e', ô: 'o', ơ: 'o',
        ư: 'u',
        á: 'a', à: 'a', ạ: 'a', ả: 'a', ã: 'a',
        ắ: 'a', ằ: 'a', ặ: 'a', ẳ: 'a', ẵ: 'a',
        ấ: 'a', ầ: 'a', ậ: 'a', ẩ: 'a', ẫ: 'a',
        é: 'e', è: 'e', ẻ: 'e', ẽ: 'e', ẹ: 'e',
        ế: 'e', ề: 'e', ể: 'e', ễ: 'e', ệ: 'e',
        ý: 'y', ỳ: 'y', ỵ: 'y', ỷ: 'y', ỹ: 'y',
        ú: 'u', ù: 'u', ủ: 'u', ũ: 'u', ụ: 'u',
        ứ: 'u', ừ: 'u', ử: 'u', ữ: 'u', ự: 'u',
        í: 'i', ì: 'i', ị: 'i', ỉ: 'i', ĩ: 'i',
        ó: 'o', ò: 'o', ỏ: 'o', õ: 'o', ọ: 'o',
        ố: 'o', ồ: 'o', ổ: 'o', ỗ: 'o', ộ: 'o',
        ớ: 'o', ờ: 'o', ở: 'o', ỡ: 'o', ợ: 'o'
    }

    string = string.trim();
    string = string.toLowerCase();

    let arr = string.split('');

    for (let i in arr) {
        if (obj[arr[i]]) {
            arr[i] = obj[arr[i]];
        }
    }

    return arr.join('');
  }
}
