import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatToolbarModule, MatIconModule, MatTooltipModule, MatTabsModule, MatProgressSpinnerModule, MatButtonModule, MatSelectModule, MatDatepickerModule, MatFormFieldModule, MatNativeDateModule, MatInput, MatInputModule } from '@angular/material';
import { CardImageModule } from 'app/shared/cards/card-image/card-image.module';
import { ImagePopupModule } from 'app/modals/image-popup/image-popup.module';
import { FormsModule } from '@angular/forms';
import { NameFilterPipeModule } from 'app/shared/pipes/name-filter-pipe/name-filter-pipe.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CopyModule } from 'app/modals/copy/copy.module';
import { EvoucherPortalComponent } from './evoucher-portal.component';
import { ManuallyEvoucherComponent } from './manually-evoucher/manually-evoucher.component';
import { DiscountBillEvoucherComponent } from './discount-bill-evoucher/discount-bill-evoucher.component';
import { DiscountItemEvoucherComponent } from './discount-item-evoucher/discount-item-evoucher.component';
import { ProductPickerComponent } from './product-picker/product-picker.component';
import { BuyADiscountBEvoucherComponent } from './buy-a-discount-b-evoucher/buy-a-discount-b-evoucher.component';
import { BuyAOfferBEvoucherComponent } from './buy-a-offer-b-evoucher/buy-a-offer-b-evoucher.component';
import { OfferItemEvoucherComponent } from './offer-item-evoucher/offer-item-evoucher.component';
import { SamePriceEvoucherComponent } from './same-price-evoucher/same-price-evoucher.component';
import { AccDecreaseEvoucherComponent } from './acc-decrease-evoucher/acc-decrease-evoucher.component';
import { StorySelectionModule } from 'app/components/story-selection/story-selection.module';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatTooltipModule,
    MatTabsModule,
    CardImageModule,
    MatProgressSpinnerModule,
    NameFilterPipeModule,
    MatButtonModule,
    MatSelectModule,
    MatInputModule,
    MatNativeDateModule, 
    MatDatepickerModule,
    FormsModule,
    DragDropModule,
    ImagePopupModule,
    StorySelectionModule,
    CopyModule,
  ],
  declarations: [
    EvoucherPortalComponent, 
    ProductPickerComponent,
    ManuallyEvoucherComponent,
    DiscountBillEvoucherComponent,
    DiscountItemEvoucherComponent,
    BuyADiscountBEvoucherComponent,
    BuyAOfferBEvoucherComponent,
    OfferItemEvoucherComponent,
    SamePriceEvoucherComponent,
    AccDecreaseEvoucherComponent
  ],
  entryComponents: [
    EvoucherPortalComponent
  ],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: EvoucherPortalComponent }
  ]
})
export class EvoucherPortalModule { }
