import { ChangeDetectionStrategy, Component, Input, EventEmitter, Output } from '@angular/core';
import { iEvoucher } from 'app/services/chatbot/api/crud/evoucher';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-buy-a-offer-b-evoucher',
  templateUrl: './buy-a-offer-b-evoucher.component.html',
  styleUrls: ['./buy-a-offer-b-evoucher.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BuyAOfferBEvoucherComponent {
  @Input() disabled: boolean = false
  @Input() evoucher: iEvoucher
  @Output() dataChange = new EventEmitter<any>()

  sameItem$ = new BehaviorSubject<boolean>(false)

  constructor() {
  }

  ngOnInit() {
    this.sameItem$.next(this.evoucher.option.sameItem)
  }

  toggleSameItem() {
    this.evoucher.option.sameItem = !this.evoucher.option.sameItem
    this.sameItem$.next(this.evoucher.option.sameItem)
    this.dataChange.emit()
  }

  offerItemsChanged(event) {
    this.evoucher.option.offerItems = event.applyItems
    this.dataChange.emit()
  }

  requireItemsChanged(event) {
    this.evoucher.option.applyItemType = event.applyItemType
    this.evoucher.option.requireItems = event.applyItems
    this.dataChange.emit()
  }

  dataChanged() {
    this.dataChange.emit()
  }
}
