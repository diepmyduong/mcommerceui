
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { iPage } from 'app/services/chatbot/api/crud/page';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { iEvoucher, ManuallyOption, DiscountBillOption, DiscountItemOption, BuyADiscountBOption, BuyAOfferBOption, OfferItemOption, SamePriceOption, AccDecreaseOption } from 'app/services/chatbot/api/crud/evoucher';
import { cloneDeep, isEqual } from 'lodash'
import { iEvoucherCode } from 'app/services/chatbot/api/crud/evoucherCode';
import { iEvoucherLog } from 'app/services/chatbot/api/crud/evoucherLog';
import { iShop } from 'app/services/chatbot/api/crud/shop';
import { BehaviorSubject } from 'rxjs';
import { iEvoucherRemind } from 'app/services/chatbot/api/crud/evoucherRemind';
@Component({
  selector: 'app-evoucher-portal',
  templateUrl: './evoucher-portal.component.html',
  styleUrls: ['./evoucher-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EvoucherPortalComponent extends BasePortal implements OnInit {
  page: iPage
  pageId: any
  componentName = "EvoucherPortalComponent"
  portalName = "Evoucher"
  
  @Input() evoucherId: string
  evouchers: iEvoucher[]
  selectedEvoucher: iEvoucher
  evoucher: iEvoucher
  originalEvoucher: iEvoucher
  evoucherCodes: iEvoucherCode[]
  evoucherLogs: iEvoucherLog[]
  evoucherOptions: any = {}

  loadDone: boolean = false
  loading: boolean = false
  loadingItem: boolean = true
  loadingResult: boolean = false
  currentPage: number = 1
  total: number
  loadingMore: boolean = false
  limit: number = 50
  filter: string = ''

  tabIndex = 0

  targetTypes = [
    { code: 'global', display: 'Chung' },
    { code: 'group', display: 'Nhóm' },
    { code: 'manually', display: 'Thủ công' },
    { code: 'exchange_point', display: 'Đổi điểm' },
  ]

  randomCode: string
  formatNumber: number = 5
  formatNumbers = [0, 3, 4, 5, 6, 10, 12]
  formatType: string = '[A-Z0-9]'
  formatTypes = [
    { code: '', display: 'Mã khác' },
    { code: '\\d', display: 'Chỉ có số' },
    { code: '[a-z]', display: 'Chữ thường' },
    { code: '[A-Z]', display: 'Chữ hoa' },
    { code: '\\w', display: 'Chữ & số' },
    { code: '[A-Z0-9]', display: 'Chữ hoa & số' },
  ]
  evoucherTypes = [
    { code: 'discount_bill', display: 'Giảm giá đơn hàng' },
    { code: 'discount_item', display: 'Giảm giá sản phẩm' },
    { code: 'buy_a_discount_b', display: 'Mua A được giảm B' },
    { code: 'buy_a_offer_b', display: 'Mua A được tặng B' },
    { code: 'offer_item', display: 'Tặng sản phẩm' },
    { code: 'same_price', display: 'Đồng giá' },
    { code: 'acc_decrease', display: 'Chiết khấu theo giá' },
    { code: 'manually', display: 'Thủ công' },
  ]
  durationUnits = [
    { code: 'M', display: 'tháng' },
    { code: 'w', display: 'tuần' },
    { code: 'd', display: 'ngày' },
    { code: 'h', display: 'giờ' },
    { code: 'm', display: 'phút' },
  ]
  weekDays: any[] = [
    { code: 0, display: 'CN' },
    { code: 1, display: 'Thứ 2' },
    { code: 2, display: 'Thứ 3' },
    { code: 3, display: 'Thứ 4' },
    { code: 4, display: 'Thứ 5' },
    { code: 5, display: 'Thứ 6' },
    { code: 6, display: 'Thứ 7' },
  ]
  dayHours: any[] = []
  advancedExpireOpened: boolean = false
  doesApplyPlaces: boolean = false
  applyPlaces$ = new BehaviorSubject<any>({})
  shop: iShop

  evoucherReminds$ = new BehaviorSubject<iEvoucherRemind[]>(undefined)
  set evoucherReminds(val) { this.evoucherReminds$.next(val) }
  get evoucherReminds() { return this.evoucherReminds$.getValue() }
  loadingReminds$ = new BehaviorSubject<boolean>(false)
  evoucherDayOptions: any[] = []
  evoucherTimeOptions: any[] = []

  constructor(
    public changeRef: ChangeDetectorRef,
    private popoverService: PopoverService,
    private dialog: MatDialog,
    private router: Router
  ) {
    super()
    for (let i = 0; i < 15; i++) {
      this.evoucherDayOptions.push({ code: i, display: i + ' ngày' })
    }
    for (let i = 0; i < 24; i++) {
      this.dayHours.push({ code: i, display: i + 'h - ' + (i + 1) + 'h' })
      this.evoucherTimeOptions.push({ code: i, display: i + ' giờ' })
    }
  }

  async ngOnInit() {
    await this.reload()
    this.changeTab(0)
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
    this.onClosed.emit(true)
  }

  async reload(local = true) {
    try {
      this.loading = true
      this.detectChanges()
      let tasks = []
      
      tasks.push(this.chatbotApi.evoucher.getList({
        local, query: { fields: ["_id", "title"], limit: 0, order: { 'createdAt': 1 } }
      }).then(result => {
        this.evouchers = result
      }))
      tasks.push(this.chatbotApi.shop.getList({ local }).then(result => { 
        if (result && result.length) this.shop = result[0]
      }))

      await Promise.all(tasks)
    } catch (err) {
      // console.error(err)
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi lấy dữ liệu", err.message)
    } finally {
      this.loadDone = true
      this.loading = false
      this.detectChanges()
      if (this.evouchers) {
        let evoucher = this.evouchers.find(x => x._id == this.evoucherId)
        this.evoucherId = ''
        if (evoucher) this.openEvoucher(evoucher)
        // else this.openEvoucher(this.evouchers[0])
      }
    }
  }

  async loadEvoucher(local = true) {
    try {
      this.loadingItem = true
      this.detectChanges()
      let tasks = []
      
      tasks.push(this.chatbotApi.evoucher.getItem(this.selectedEvoucher._id, {
        local, query: { limit: 0, order: { 'createdAt': 1 } }
      }).then(result => {
        this.originalEvoucher = result
        this.evoucher = cloneDeep(result)
      }))

      await Promise.all(tasks)
      console.log(this.evoucher)
      if (this.shop) {
        let places
        if (!this.evoucher.applyPlaces || this.evoucher.applyPlaces.length == 0) {
          places = this.shop.places
          this.doesApplyPlaces = false
        } else {
          places = this.shop.places.filter(x => this.evoucher.applyPlaces.includes(x._id))
          this.doesApplyPlaces = true
        }
        this.applyPlaces$.next(places.reduce((obj, current) => {
          return {...obj, [current._id]: true}
        }, {}))
      }
      this.evoucherOptions[this.evoucher.type] = this.evoucher.option
      this.formatChanged('input')
      if (!this.evoucher.inputDate.length && !this.evoucher.inputHour.length) this.advancedExpireOpened = false
      else this.advancedExpireOpened = true
      this.inputTimeChanged()
    } catch (err) {
      // console.error(err)
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi lấy dữ liệu", err.message)
    } finally {
      this.loadingItem = false
      this.detectChanges()
    }
  }
  
  async addEvoucher(event, center = false) {
    let formOptions: iPopoverFormOptions = {
      parent: '.swiper-slide',
      target: center?'.btn-create':'.btn-blue',
      targetElement: event.target,
      width: '300px',
      confirm: 'Tạo khuyến mãi',
      horizontalAlign: 'right',
      verticalAlign: 'bottom',
      fields: [
        {
          placeholder: 'Tên',
          property: 'title',
          constraints: ['required']
        }
      ],
      onSubmit: async (data) => {
        if (!data.title) return
        try {
          this.loading = true
          this.detectChanges()
          let evoucher = await this.chatbotApi.evoucher.add({
            title: data.title,
            startDate: new Date(),
            endDate: new Date(),
            targetType: 'manually',
            type: 'manually',
            option: {
              message: 'Tin nhắn mẫu'
            },
            format: '[A-Z0-9]{5}',
            expireType: 'static',
            expireAmount: 10,
            expireUnit: 'd',
            qtyPerDay: 0,
            qtyPerMember: 0,
            qty: 0
          })
          this.openEvoucher(evoucher)
        } catch (err) {
          console.error(err)
          this.alert.handleError(err, "Lỗi khi tạo khuyến mãi");
          // this.alert.error('Không tạo được vòng xoay', err.message)
        } finally {
          this.loading = false
          this.detectChanges()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  editName(event, evoucher: iEvoucher) {
    let formOptions: iPopoverFormOptions = {
      parent: '.swiper-slide',
      target: '.portal-list-item',
      targetElement: event.target,
      width: '300px',
      confirm: 'Đổi tên khuyến mãi',
      horizontalAlign: 'right',
      verticalAlign: 'top',
      fields: [
        {
          placeholder: 'Tên khuyến mãi',
          property: 'title',
          constraints: ['required'],
          current: evoucher.title
        }
      ],
      onSubmit: async (data) => {
        if (!data.title) return
        try {
          this.loading = true
          this.detectChanges()
          await this.chatbotApi.evoucher.update(evoucher._id, {
            title: data.title
          })
          if (this.selectedEvoucher && this.selectedEvoucher._id == evoucher._id) {
            this.selectedEvoucher.title = data.title
          }
        } catch (err) {
          console.error(err)
          this.alert.handleError(err, "Lỗi khi đổi tên")
        } finally {
          this.loading = false
          this.detectChanges()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  async remove(evoucher: iEvoucher) {
    if (!await this.alert.warning('Xóa khuyến mãi', 'Bạn có muốn xóa khuyến mãi \"' + evoucher.title +'\"?')) return

    try {
      this.loading = true
      this.detectChanges()
      let id = evoucher._id
      await this.chatbotApi.evoucher.delete(evoucher._id)
      if (this.selectedEvoucher && id == this.selectedEvoucher._id) this.openEvoucher()
    } catch (err) {
      console.error(err)
      this.alert.handleError(err, "Lỗi khi xóa khuyến mãi");
      // this.alert.error('Không tạo được vòng xoay', err.message)
    } finally {
      this.loading = false
      this.detectChanges()
    }
  }

  async openEvoucher(evoucher = null) {
    if (this.selectedEvoucher != evoucher) {
      this.selectedEvoucher = evoucher
      if (this.selectedEvoucher) await this.loadEvoucher()
      else this.evoucher = null
      this.hideSave()
      this.changeTab(this.tabIndex)
    }
  }

  changeTab(index) {
    this.tabIndex = index
    if (index == 2) {
      this.loadResult()
    }
    if (index == 3) {
      this.loadReminds()
    }
    this.detectChanges()
  }

  async loadResult(local = true) {
    if (!this.selectedEvoucher) return
    try {
      this.loadingResult = true
      this.detectChanges()
      let tasks = []
      this.currentPage = 1
      tasks.push(this.chatbotApi.evoucherCode.getList({
        local, query: { 
          limit: this.limit, 
          page: this.currentPage,
          order: { 'createdAt': -1 }, 
          populates:["subscriber"], 
          filter: { eVoucher: this.selectedEvoucher._id }
        }
      }).then(result => {
        this.evoucherCodes = result
        this.total = this.chatbotApi.evoucherCode.pagination.totalItems
      }))

      await Promise.all(tasks)
      console.log(this.evoucherCodes)
    } catch (err) {
      console.error(err)
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi lấy dữ liệu", err.message)
    } finally {
      this.loadingResult = false
      this.detectChanges()
    }
  }

  async loadMoreResult() {
    try {
      this.loadingMore = true
      this.changeRef.detectChanges()
      this.currentPage += 1
      let tasks = []
      tasks.push(this.chatbotApi.evoucherCode.getList({
        local: false, query: { 
          limit: this.limit, 
          page: this.currentPage,
          order: { 'createdAt': -1 }, 
          populates:["subscriber"], 
          filter: { eVoucher: this.selectedEvoucher._id }
        }
      }).then(result => {
        this.evoucherCodes = [...this.evoucherCodes, ...result]
        console.log( 'res',this.evoucherCodes)
      }))
      await Promise.all(tasks)
  } catch (err) {
      this.alert.handleError(err, "Lỗi khi lấy thêm dữ liệu");
  } finally {
      this.loadingMore = false
      this.changeRef.detectChanges()
  }
  }

  targetTypeChanged() {
    if (this.evoucher.targetType != 'manually') {
      this.evoucher.format = 'CODE'
    } else {
      this.formatNumber = 5
      this.formatType = '[A-Z0-9]'
      this.formatChanged('select')
    }
    this.dataChanged()
  }

  expireTypeChanged() {
    this.evoucher.expireType=='static'?this.evoucher.expireType='dynamic':this.evoucher.expireType='static'
    if (this.evoucher.expireType == 'dynamic') {
      if (!this.evoucher.expireAmount) this.evoucher.expireAmount = 10
      if (!this.evoucher.expireUnit) this.evoucher.expireUnit = 'd'
    }
    this.dataChanged()
  }

  inputTimeChanged(item = null, mode = 'week') {
    if (item != null) {
      let evoucherProperty, arrayProperty
      if (mode == 'week') {
        evoucherProperty = 'inputDate'
        arrayProperty = 'weekDays'
      } else {
        evoucherProperty = 'inputHour'
        arrayProperty = 'dayHours'
      }
      let index = this.evoucher[evoucherProperty].findIndex(x => x == item)
      if (index >= 0) {
        this.evoucher[evoucherProperty].splice(index, 1)
        this[arrayProperty][this[arrayProperty].findIndex(x => x.code ==  item)].active = false
      } else {
        this.evoucher[evoucherProperty].push(item)
        this[arrayProperty][this[arrayProperty].findIndex(x => x.code ==  item)].active = true
      }
    } else {
      this.weekDays.forEach(x => {
        if (this.evoucher.inputDate.includes(x.code)) {
          x.active = true
        } else {
          x.active = false
        }
      })
      this.dayHours.forEach(x => {
        if (this.evoucher.inputHour.includes(x.code)) {
          x.active = true
        } else {
          x.active = false
        }
      })
    }
    this.dataChanged()
  }

  typeChanged() {
    if (!this.evoucherOptions[this.evoucher.type]) {
      let option
      switch (this.evoucher.type) {
        case 'manually':
          option = {
            message: ''
          } as ManuallyOption
          break
        case 'discount_bill': 
          option = {
            unit: 'percent',
            discount: 0,
            minAmountOfBill: 0,
            maxDiscount: 0,
            minQty: 0,
          } as DiscountBillOption
          break
        case 'discount_item':
          option = {
            unit: 'percent',
            discount: 0,
            minAmountOfBill: 0,
            maxDiscount: 0,
            minQty: 0,
            applyItemType: 'category',
            applyItems: [],
            inputDiscountForItem: null,
            discountOneItemForItem: 'min',
            inputDiscountFromItem: null,
            discountOneItemFromItem: 'min'
          } as DiscountItemOption
          break
        case 'buy_a_discount_b':
          option = {
            applyItemType: 'category',
            requireItems: [],
            applyItems: [],
            numberItemBuy: 1,
            numberItemFree: 1, // Số lượng món được giảm
            discount: 0, // Giá trị giảm theo %
            minAmountOfBill: 0, // Giảm khi đơn hàng phải hơn bao nhiêu
            maxDiscount: 0
          } as BuyADiscountBOption
          break
        case 'buy_a_offer_b':
          option = {
            applyItemType: 'category', // Giảm cho nhóm hoặc sản phẩm
            requireItems: [], // Danh sách nhóm cần mua
            numberItemBuy: 1, // Số lượng món cần mua
            numberItemFree: 1, // Số lượng món được giảm
            minAmountOfBill: 0, // Giảm khi đơn hàng phải hơn bao nhiêu
            sameItem: true
          } as BuyAOfferBOption
          break
        case 'offer_item':
          option = {
            offerItems: [], // Danh sách món được tặng
            minAmountOfBill: 0 // Giảm khi đơn hàng phải hơn bao nhiêu
          } as OfferItemOption
          break
        case 'same_price':
          option = {
            applyItemType: 'all', // Giảm cho nhóm hoặc sản phẩm
            applyItems: [],
            price: 0 // Đồng giá
          } as SamePriceOption
          break
        case 'acc_decrease':
          option = {
            discountAcc: 0, // Áp dụng giá giảm mỗi sản phẩm bao nhiêu
            discountAmountAcc: 0, // Thì giảm bao nhiêu
            optAccItemType: 'category', // Không áp dụng cho
            optAccItems: [], // Danh sách không áp dụng
            maxDiscount: 0, // Giảm không quá bao nhiêu 
          } as AccDecreaseOption
          break
        default: break
      }
      this.evoucherOptions[this.evoucher.type] = option
    }
    this.evoucher.option = this.evoucherOptions[this.evoucher.type]
    this.dataChanged()
  }

  formatChanged(source) {
    if (source != 'input') {
      if (this.formatNumber && this.formatType) {
        this.evoucher.format = this.formatType + '{' + this.formatNumber +'}'
      } else {
        return
      }
    } else {
      try {
        let first = this.evoucher.format.lastIndexOf('{')
        let last = this.evoucher.format.lastIndexOf('}')
        this.formatNumber = Number(this.evoucher.format.substring(first + 1, last))
        if (this.formatNumber == NaN) this.formatNumber = 0
        else if (!this.formatNumbers.includes(this.formatNumber)) this.formatNumber = 0
        let formatType = this.evoucher.format.substring(0, first)
        this.formatType = this.formatTypes.findIndex(x => x.code == formatType)>=0?this.formatType:''
      } catch (err) {
        this.formatNumber = 0
        this.formatType = ''
      }
    }
    this.dataChanged()
  }

  dataChanged() {
    if (this.showSave || this.isSaving) return
    
    this.showSaveButton()
    this.changeRef.detectChanges()
  }

  checkErrors() {
    switch (this.evoucher.type) {
      case 'manually':
        if (!this.evoucher.option.message) {
          this.alert.info('Thiếu chi tiết khuyến mãi', 'Yêu cầu nhập tin nhắn')
          return true
        }
        break
      case 'discount_item':
      case 'discount_bill': 
        if (!this.evoucher.option.discount) {
          this.alert.info('Thiếu chi tiết khuyến mãi', `Yêu cầu nhập ${this.evoucher.option.unit == 'percent'?'phần trăm':'giá trị'} giảm giá`)
          return true
        }
        if (this.evoucher.option.unit == 'percent' && (this.evoucher.option.discount < 0 || this.evoucher.option.discount > 100)) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Phần trăm giảm giá phải trong khoảng 0 đến 100')
          return true
        }
        if (this.evoucher.option.unit == 'amount' && this.evoucher.option.discount < 0) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Tiền giảm giá không được phép âm')
          return true
        }
        if (this.evoucher.option.minAmountOfBill == null) {
          this.alert.info('Thiếu chi tiết khuyến mãi', 'Yêu cầu nhập giá tiền tối thiểu')
          return true
        }
        if (this.evoucher.option.minAmountOfBill < 0) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Giá tiền tối thiểu không được phép âm')
          return true
        }
        if (this.evoucher.option.maxDiscount == null) {
          this.alert.info('Thiếu chi tiết khuyến mãi', 'Yêu cầu nhập giảm giá tối đa')
          return true
        }
        if (this.evoucher.option.maxDiscount < 0) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Giảm giá tối đa không được phép âm')
          return true
        }
        if (this.evoucher.type == 'discount_item') {
          if (!this.evoucher.option.inputDiscountForItem && !this.evoucher.option.inputDiscountFromItem) {
            this.alert.info('Thiếu chi tiết khuyến mãi', 'Yêu cầu nhập một trong hai trường giảm giá theo món')
            return true
          }
          if (!this.evoucher.option.applyItems.length) {
            this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Chưa chọn món trong danh sách được giảm giá')
            return true
          }
        }
        break
      case 'buy_a_discount_b':
        if (this.evoucher.option.discount < 0 || this.evoucher.option.discount > 100) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Phần trăm giảm giá phải trong khoảng 0 đến 100')
          return true
        }
        if (this.evoucher.option.minAmountOfBill == null) {
          this.alert.info('Thiếu chi tiết khuyến mãi', 'Yêu cầu nhập giá tiền tối thiểu')
          return true
        }
        if (this.evoucher.option.minAmountOfBill < 0) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Giá tiền tối thiểu không được phép âm')
          return true
        }
        if (!this.evoucher.option.numberItemBuy || !this.evoucher.option.numberItemFree) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Thiếu số lượng món yêu cầu mua và/hoặc số lượng món được giảm.')
          return true
        }
        if (!this.evoucher.option.applyItems.length) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Chưa chọn món trong danh sách được giảm giá')
          return true
        }
        if (!this.evoucher.option.requireItems.length) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Chưa chọn món trong danh sách yêu cầu')
          return true
        }
        break
      case 'buy_a_offer_b':
        if (this.evoucher.option.minAmountOfBill == null) {
          this.alert.info('Thiếu chi tiết khuyến mãi', 'Yêu cầu nhập giá tiền tối thiểu')
          return true
        }
        if (this.evoucher.option.minAmountOfBill < 0) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Giá tiền tối thiểu không được phép âm')
          return true
        }
        if (!this.evoucher.option.numberItemBuy || !this.evoucher.option.numberItemFree) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Thiếu số lượng món yêu cầu mua và/hoặc số lượng món được giảm.')
          return true
        }
        if (this.evoucher.option.offerType == 'max_item' && !this.evoucher.option.applyItems.length) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Chưa chọn món trong danh sách được tặng')
          return true
        }
        if (!this.evoucher.option.requireItems.length) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Chưa chọn món trong danh sách yêu cầu')
          return true
        }
        break
      case 'offer_item':
        if (!this.evoucher.option.offerItems.length) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Chưa chọn món trong danh sách được tặng')
          return true
        }
        if (this.evoucher.option.minAmountOfBill == null) {
          this.alert.info('Thiếu chi tiết khuyến mãi', 'Yêu cầu nhập giá tiền tối thiểu')
          return true
        }
        if (this.evoucher.option.minAmountOfBill < 0) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Giá tiền tối thiểu không được phép âm')
          return true
        }
        break
      case 'same_price':
        if (this.evoucher.option.applyItemType != 'all' && !this.evoucher.option.applyItems.length) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Chưa chọn món trong danh sách được đồng giá')
          return true
        }
        if (this.evoucher.option.price == null) {
          this.alert.info('Thiếu chi tiết khuyến mãi', 'Yêu cầu nhập giá tiền đồng giá')
          return true
        }
        if (this.evoucher.option.price < 0) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Giá tiền đồng giá không được phép âm')
          return true
        }
        break
      case 'acc_decrease':
        if (this.evoucher.option.discountAcc == null) {
          this.alert.info('Thiếu chi tiết khuyến mãi', 'Yêu cầu nhập giá giảm mỗi tổng tiền đơn hàng')
          return true
        }
        if (this.evoucher.option.discountAcc < 0) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Giá giảm mỗi tổng tiền đơn hàng không được phép âm')
          return true
        }
        if (this.evoucher.option.discountAmountAcc == null) {
          this.alert.info('Thiếu chi tiết khuyến mãi', 'Yêu cầu nhập giá trị được giảm')
          return true
        }
        if (this.evoucher.option.discountAmountAcc < 0) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Giá trị được giảm không được phép âm')
          return true
        }
        if (this.evoucher.option.maxDiscount == null) {
          this.alert.info('Thiếu chi tiết khuyến mãi', 'Yêu cầu nhập giảm giá tối đa')
          return true
        }
        if (this.evoucher.option.maxDiscount < 0) {
          this.alert.info('Chi tiết khuyến mãi không hợp lệ', 'Giảm giá tối đa không được phép âm')
          return true
        }
        break
      default: break
    }
  }

  async saveContent() {
    if (!this.selectedEvoucher) return

    if (this.checkErrors()) return
    let evoucherReminds = this.evoucherReminds?this.evoucherReminds.filter(x => x.hasChanged):[]
    if (evoucherReminds.filter(x => !x.story).length) {
      this.alert.info('Thiếu câu chuyện nhắc nhở', 'Các mục nhắc nhở không được để trống câu chuyện nhắc nhở.')
      return
    }

    try {
      this.showSaving()

      let tasks = []

      if (!isEqual(this.originalEvoucher, this.evoucher)) {
        tasks.push(this.chatbotApi.evoucher.update(this.evoucher._id, this.evoucher).then(res => {
          this.evoucher = res
        }))
      }
      
      if (evoucherReminds.length) {
        console.log(evoucherReminds)
        for (let i = 0; i < this.evoucherReminds.length; i++) {
          let evoucherRemind = this.evoucherReminds[i]
          if (!evoucherRemind.hasChanged) continue

          let { story, page, beforeDay, hour, eVoucher, ignoreUsed } = evoucherRemind
          let finalEvoucherRemind = { story, page, beforeDay, hour, eVoucher, ignoreUsed }
          if (evoucherRemind._id) {
            if (evoucherRemind.isDeleted) {
              tasks.push(this.chatbotApi.evoucherRemind.delete(evoucherRemind._id).then(res => {
              }))
            } else {
              tasks.push(this.chatbotApi.evoucherRemind.update(evoucherRemind._id, finalEvoucherRemind).then(res => {
                this.evoucherReminds[i] = { ...res }
              }))
            }
          } else {
            tasks.push(this.chatbotApi.evoucherRemind.add(finalEvoucherRemind).then(res => {
              this.evoucherReminds[i] = { ...res }
            }))
          }
        }
      }

      await Promise.all(tasks)
      if (evoucherReminds.length) this.evoucherReminds = [...this.evoucherReminds.filter(x => !x.isDeleted)]
      this.showSaveSuccess()
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lưu eVoucher");
      this.showSaveButton()
    }
  }

  async removeResult(res) {
    try {
      await this.chatbotApi.evoucherCode.delete(res._id)
      this.loadResult(false)
    } catch (err) {
      this.detectChanges()
    }
  }

  async exportCSV() {
    await this.chatbotApi.evoucher.exportCSV(this.selectedEvoucher._id, this.selectedEvoucher.title)
  }

  async removeAllResults() {
    // if (!await this.alert.warning('Xóa tất cả kết quả', 'Bạn có chắc chắn muốn xóa hết tất cả kết quả không')) return
    // try {
    //   this.loadingResult = true
    //   this.detectChanges()
    //   await this.chatbotApi.luckyWheel.removeAllResults(this.selectedWheel._id)
    //   this.wheelResults = []
    // } catch (err) {

    // } finally {
    //   this.loadingResult = false
    //   this.detectChanges()
    // }
  }

  async refreshResult() {
    try {
      this.loadingResult = true
      this.detectChanges()
      await this.loadResult(false)
    } catch (err) {
    } finally {
      this.loadingResult = false
      this.detectChanges()
    }
  }

  togglePlace(id) {
    let value = !this.applyPlaces$.getValue()[id]
    this.applyPlaces$.next({...this.applyPlaces$.getValue(), [id]: value})
    let applyPlaces = []
    for (let key of Object.keys(this.applyPlaces$.getValue())) {
      if (this.applyPlaces$.getValue()[key]) applyPlaces.push(key)
    }
    if (applyPlaces.length == this.shop.places.length) this.evoucher.applyPlaces = []  
    else this.evoucher.applyPlaces = applyPlaces
    this.dataChanged()
  }

  trackByFn(index, item) {
    return item._id
  }

  async loadReminds(local = true) {
    if (!this.selectedEvoucher) return
    try {
      this.loadingReminds$.next(true)
      let res = await this.chatbotApi.evoucherRemind.getList({
        local, query: { 
          limit: this.limit, 
          page: this.currentPage,
          order: { 'createdAt': -1 }, 
          filter: { eVoucher: this.selectedEvoucher._id }
        }
      })
      this.evoucherReminds = cloneDeep(res)
    } catch (err) {
      console.error(err)
      this.alert.handleError(err, 'Lấy danh sách nhắc nhở thất bại');
    } finally {
      this.loadingReminds$.next(false)
    }
  }

  remindDataChanged(evoucherRemind) {
    evoucherRemind.hasChanged = true
    this.dataChanged()
  }

  storyChanged(evoucherRemind, event) {
    evoucherRemind.story = event
    this.remindDataChanged(evoucherRemind)
  }

  toggleIgnoreUsed(evoucherRemind) {
    evoucherRemind.ignoreUsed = !evoucherRemind.ignoreUsed
    this.remindDataChanged(evoucherRemind)
  }

  count = 0
  async addRemind() {
    const { app } = this.chatbotApi.chatbotAuth
    let evoucherRemind = {
      story: '',
      page: (typeof(app.activePage)==='object') ? (app.activePage as iPage)._id : app.activePage as string,
      beforeDay: 1,
      hour: 12,
      eVoucher: this.selectedEvoucher._id,
      ignoreUsed: false,
      index: this.count++
    } as iEvoucherRemind
    this.evoucherReminds = [
      ...this.evoucherReminds,
      evoucherRemind
    ]
    this.remindDataChanged(evoucherRemind)
  }

  async removeRemind(evoucherRemind, index) {
    if (evoucherRemind._id) {
      evoucherRemind.isDeleted = true
      this.evoucherReminds = [...this.evoucherReminds]
      this.remindDataChanged(evoucherRemind)
    } else {
      this.evoucherReminds.splice(index, 1)
      this.evoucherReminds = [...this.evoucherReminds]
    }
  }
}
