import { ChangeDetectionStrategy, Component, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { iEvoucher } from 'app/services/chatbot/api/crud/evoucher';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';

@Component({
  selector: 'app-discount-bill-evoucher',
  templateUrl: './discount-bill-evoucher.component.html',
  styleUrls: ['./discount-bill-evoucher.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DiscountBillEvoucherComponent {
  @Input() disabled: boolean = false
  @Input() evoucher: iEvoucher
  @Output() dataChange = new EventEmitter<any>()

  @ViewChild('autoSize') autoSizeTextarea: CdkTextareaAutosize

  constructor() {
  }

  dataChanged() {
    this.dataChange.emit()
  }
}
