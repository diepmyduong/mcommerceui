import { ChangeDetectionStrategy, Component, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { iEvoucher } from 'app/services/chatbot/api/crud/evoucher';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-buy-a-discount-b-evoucher',
  templateUrl: './buy-a-discount-b-evoucher.component.html',
  styleUrls: ['./buy-a-discount-b-evoucher.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BuyADiscountBEvoucherComponent {
  @Input() disabled: boolean = false
  @Input() evoucher: iEvoucher
  @Output() dataChange = new EventEmitter<any>()

  constructor() {
  }

  ngOnInit() {
  }

  applyItemsChanged(event) {
    this.evoucher.option.applyItemType = event.applyItemType
    this.evoucher.option.applyItems = event.applyItems
    this.dataChange.emit()
  }

  requireItemsChanged(event) {
    this.evoucher.option.applyItemType = event.applyItemType
    this.evoucher.option.requireItems = event.applyItems
    this.dataChange.emit()
  }

  dataChanged() {
    this.dataChange.emit()
  }
}
