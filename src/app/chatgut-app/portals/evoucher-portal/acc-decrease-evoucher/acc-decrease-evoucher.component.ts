import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { iEvoucher } from 'app/services/chatbot/api/crud/evoucher';

@Component({
  selector: 'app-acc-decrease-evoucher',
  templateUrl: './acc-decrease-evoucher.component.html',
  styleUrls: ['./acc-decrease-evoucher.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccDecreaseEvoucherComponent {
  @Input() disabled: boolean = false
  @Input() evoucher: iEvoucher
  @Output() dataChange = new EventEmitter<any>()

  constructor() {
  }

  ngOnInit() {
  }

  applyItemsChanged(event) {
    this.evoucher.option.optAccItemType = event.applyItemType
    this.evoucher.option.optAccItems = event.applyItems
    this.dataChange.emit()
  }

  dataChanged() {
    this.dataChange.emit()
  }
}
