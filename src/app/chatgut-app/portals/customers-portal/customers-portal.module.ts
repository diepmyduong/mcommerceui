import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomersPortalComponent } from 'app/chatgut-app/portals/customers-portal/customers-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';

// Material
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatNativeDateModule, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { SelectionModule } from 'app/modals/selection/selection.module';
import { AppDateAdapter, APP_DATE_FORMATS } from 'app/shared/date-adapter';
import { JsonDialog } from 'app/modals/json/json.component';
import { NgJsonEditorModule } from 'ang-jsoneditor';
import { MatMenuModule } from '@angular/material';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgbDropdownModule } from "@ng-bootstrap/ng-bootstrap";
import { SendModule } from 'app/modals/send/send.module';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatButtonModule,
    NgxDatatableModule,
    FormsModule,
    ReactiveFormsModule,
    EditInfoModule,
    SelectionModule,
    NgJsonEditorModule,
    MatMenuModule,
    NgbDropdownModule,
    SendModule,
    NgMultiSelectDropDownModule,
  ],
  declarations: [CustomersPortalComponent, JsonDialog],
  entryComponents: [CustomersPortalComponent, JsonDialog],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: CustomersPortalComponent },
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
  ]
})
export class CustomersPortalModule { }
