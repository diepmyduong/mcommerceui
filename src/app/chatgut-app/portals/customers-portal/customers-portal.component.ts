import { isEqual, orderBy, forOwn, has, map, flatten, cloneDeep, isObject } from 'lodash-es'
import * as countryCode from 'app/shared/countries-code'
import { Component, OnInit, ViewChild, Output, EventEmitter, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { iEditInfoDialogData, EditInfoDialog } from 'app/modals/edit-info/edit-info.component';
import { MatDialog } from '@angular/material';
import { iSelectionData, SelectionDialog } from 'app/modals/selection/selection.component';
import { iGroup } from 'app/services/chatbot/api/crud/group';
import { iPage } from 'app/services/chatbot/api/crud/page';
import { iSubscriber } from 'app/services/chatbot/api/crud/subscriber';
import * as converter from 'json-2-csv'
import * as moment from 'moment'
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { JsonDialog } from 'app/modals/json/json.component';
import { DynamicComponentLoaderService } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { ChatbotAuthService } from 'app/services/chatbot';
import { iCrudPagination } from 'app/services/chatbot/api/crud';
import { SendDialog } from 'app/modals/send/send.component';

const GROUP_ALL: iGroup = {
  "name": 'Tất cả khách hàng',
  "isAll": true,
  "paramQuery": {},
  "_id": "0"
}

@Component({
  selector: 'app-customers-portal',
  templateUrl: './customers-portal.component.html',
  styleUrls: ['./customers-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomersPortalComponent extends BasePortal implements OnInit {

  storyId: string
  group: iGroup
  groups: iGroup[] = []
  page: iPage
  pageId: any
  componentName = "CustomerPortalComponent"
  subName = "Nhóm khách hàng"
  subscribers: iSubscriber[];
  @Output() onPortalSaved = new EventEmitter()
  @Output() onPortalClosed = new EventEmitter()
  @ViewChild(DatatableComponent) datatable: DatatableComponent;

  countriesObj = countryCode.isoCountriesVi
  allStatusObj = {
    subscribe: "Đăng kí",
    waiting: "Chưa tương tác",
    block: "Đã chặn",
    live_chat: "Live chat"
  }
  loadingSubscribers = true
  firstLoad: boolean = true
  canBeSaved = false
  searchString: string = ''
  previousSearch: string = ''

  limit: number
  offset: number = 0
  pagination: iCrudPagination
  rows = []
  advancedColumns = []
  selected = [];
  limitOptions: number[] = [10, 25, 50, 100, 200, 500, 1000]

  paramQuery: any = {}

  advancedFilters: any = null
  tempBasicFilters: any
  basicFilters = {
    status: { display: 'Trạng thái', data: [], selections: [
      { code: "subscribe", display: "Đăng kí" },
      { code: "block", display: "Đã chặn" },
      { code: "waiting", display: "Chưa tương tác" }
    ]},
    gender: { display: 'Giới tính', data: [], selections: [
      { code: "male", display: "Nam" },
      { code: "female", display: "Nữ" },
      { code: "other", display: "Khác" },
    ]},
    localies: { display: 'Quốc gia', data: [], selections: countryCode.countriesArray},
    // labels: { display: 'Nhãn', data: [], selections: []},
    tags: { display: 'Livechat tags', data: [], selections: []},
    referral: { display: 'Đường dẫn', data: [], selections: []},
    phone: { display: 'Số điện thoại', data: [], selections: [
      { code: "true", display: "Có số điện thoại" },
      { code: "false", display: "Không có số điện thoại" },
    ] },
    subscribe_date: { display: 'Ngày đăng ký', data: { start: null, end: null } },
    last_interaction: { display: 'Tin nhắn cuối', data: { start: null, end: null } }
  }

  otherFilters: {
    key: string
    value: string
    type: '#eq' | '#ne' | '#regex' | '#not' | '#exists' | '#non_exists' | '#gt' | '#lt' | '#gte' | '#lte'
  }[] = []

  otherFilterTypeObj = {
    '#eq': '=' ,
    '#ne': 'khác',
    '#regex': 'chứa chuỗi',
    // '#not': 'không chứa chuỗi',
    '#exists': 'có tồn tại',
    '#non_exists': 'không tồn tại',
    '#gt': '>',
    '#lt': '<',
    '#gte': '>=',
    '#lte': '<=',
  }

  filterSubtitle = {
    basic: '',
    advanced: '',
    other: ''
  }

  dropdownSettings = {
      singleSelection: false,
      idField: 'code',
      textField: 'display',
      selectAllText: 'Chọn tất cả',
      unSelectAllText: 'Bỏ chọn tất cả',
      itemsShowLimit: 3,
      allowSearchFilter: false
  }

  reloadNumber: number = 0
  userRule: string 
  labelsSynchronizing: boolean = false

  _sort: any = {}
  get sort() {
    if (!this._sort || !Object.keys(this._sort).length) return { "lastInteraction": -1 }
    return this._sort
  }
  set sort(val) { this._sort = val}
  
  constructor(
    public selectionDialog: MatDialog,
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef,
    private popoverService: PopoverService,
    private chatbotAuth: ChatbotAuthService,
    private dynamicLoader: DynamicComponentLoaderService
  ) {
    super()
    moment.locale('vi')

    this.userRule = this.chatbotAuth.user.rule
  }

  async ngOnInit() {
    let groupStorage = JSON.parse(sessionStorage.getItem('groupStorage'))
    const id = (this.chatbotApi.chatbotAuth.app.activePage as iPage)._id

    if (!this.group && groupStorage && groupStorage.id == id) {
      this.group = groupStorage.group
      this.pagination = {...groupStorage.pagination}
    } else {
      sessionStorage.setItem('groupStorage', null)
      this.pagination = {...this.chatbotApi.subscriber.pagination}
    }
    
    if (this.group) {
      this.setGroup(this.group)
    } else {
      this.setGroup(GROUP_ALL)
    }
  }

  async ngAfterViewInit() {
    this.dynamicLoader.getComponentFactory('SubscriberDetailPortalComponent')
  }

  async ngOnDestroy() {
    // this.onPortalClosed.emit();
    super.ngOnDestroy()
    let groupStorage = {
      id: (this.chatbotApi.chatbotAuth.app.activePage as iPage)._id,
      group: this.group,
      pagination: this.chatbotApi.subscriber.pagination
    }
    sessionStorage.setItem('groupStorage', JSON.stringify(groupStorage))
  }

  async reload(local: boolean = true) {

    if (isEqual(this.paramQuery, this.group.paramQuery) || (!this.group.paramQuery && Object.keys(this.paramQuery).length == 0)) this.hideSave()
    else this.showSaveButton()
    
    this.popoverService.closePopoverForm()

    try {
      let tasks = []

      this.loadingSubscribers = true
      this.datatable.messages.emptyMessage = "";
      this.rows = []
      this.detectChanges()

      if (this.firstLoad) {
        this.chatbotApi.group.getList({ local: false, query: { limit: 0, order: { createdAt: 1 } }}).then(result => {
          this.groups = [GROUP_ALL, ...result]
        })
        tasks.push(this.chatbotApi.referral.getList().then(result => {
          this.basicFilters.referral.selections = orderBy(map(result, x => 
            { return { code: x._id, display: x.name } }), ['display'],['asc']);
        }))
        tasks.push(this.chatbotApi.subscriberTag.getList({
          local: false, query: { limit: 0, fields: "name", order: { position: 1  } }
        }).then(res => {
          this.basicFilters.tags.selections = res.map(x => ({ code: x.name, display: x.name }))
        }))
        // tasks.push(this.chatbotApi.label.getAllLabels((this.chatbotApi.chatbotAuth.app.activePage as iPage)._id).then(result => {
        //   this.basicFilters.labels.selections = result.data.map(value => { return value.name });
        //   if (!this.basicFilters.labels.selections) {
        //     this.basicFilters.labels.selections = []
        //   }
        //   this.basicFilters.labels.selections = orderBy(this.basicFilters.labels.selections);
        // }).catch(err => {
        //   this.basicFilters.labels.selections = null
        // }))
      }

      this.reloadNumber++
      let newReloadNumber = this.reloadNumber
      let resultSubscribers 

      tasks.push(new Promise((resolve) => setTimeout(resolve)))

      tasks.push(this.chatbotApi.subscriber.filter({
        local, query: {
          fields: ["_id", "messengerProfile", "phone", "status", "createdAt", "updatedAt", "snippetUpdatedAt", 
          "uid", "otherInfo", "lastInteraction", "messageCount", "reactionCount", "commentCount", "commentPost"],
          limit: this.pagination.limit,
          page: this.offset + 1,
          order: this.sort
        }
      }, {...this.paramQuery, search: this.searchString}).then(result => {
        resultSubscribers = result
      }))

      await Promise.all(tasks);

      if (newReloadNumber != this.reloadNumber) {
        console.log('mismatch')
        return
      }
      this.previousSearch = this.searchString
      this.pagination = {...this.chatbotApi.subscriber.pagination}
      this.subscribers = orderBy(resultSubscribers, ['snippetUpdatedAt'], ['desc'])
      try {
        document.getElementById('groupDetailsTable').getElementsByClassName('datatable-body')
        .item(0).setAttribute("style", "height: " + (this.subscribers.length * 50 + 20) + "px !important");
      } catch (err) {
        
      }
      let data = []
      for (let subscriber of this.subscribers) {
        let row = {} as any;
        row = subscriber
        // row.subscriber = subscriber
        if (subscriber.messengerProfile) {
          row.avatar = subscriber.messengerProfile.profile_pic
          row.full_name = subscriber.messengerProfile.name?subscriber.messengerProfile.name:(subscriber.messengerProfile.first_name + " " + subscriber.messengerProfile.last_name)
          
          switch (subscriber.messengerProfile.gender) {
            case 'male': row.gender = "Nam"; break;
            case 'female': row.gender = "Nữ"; break;
            case 'other': row.gender = "Khác"; break;
            default: row.gender = "-"; break;
          }
          if (subscriber.messengerProfile.locale) {
            row.locale = subscriber.messengerProfile.locale
            row.localeFlag = 'flag flag-' + subscriber.messengerProfile.locale.substr(3, 5).toLowerCase()
            row.localeDisplay = this.countriesObj[subscriber.messengerProfile.locale.substr(3, 5)]
          }
        }
        // row.status = subscriber.status;
        // row.labels = []
        // if (subscriber.labels) row.labels = subscriber.labels;
        // row.phone = ""
        // if (subscriber.phone) {
        //   row.phone = subscriber.phone
        // }
        // row.createdAt = subscriber.createdAt
        if (!row.phone && row.otherInfo && row.otherInfo.phone) {
          row.phone = row.otherInfo.phone
        }
        let createdAtMoment = moment(subscriber.createdAt)
        if (createdAtMoment.year() == moment().year()) {
          row.createdAtFromNow = moment(subscriber.createdAt).format('DD MMM')
        } else {
          row.createdAtFromNow = moment(subscriber.createdAt).format('DD MMM YYYY')
        }
        if (subscriber.snippetUpdatedAt) {
          row.snippetUpdatedAtText = moment(subscriber.snippetUpdatedAt).fromNow()
        } else {
          row.snippetUpdatedAt = ''
          row.snippetUpdatedAtText = '-'
        }
        if (subscriber.lastInteraction) {
          row.lastInteractionText = moment(subscriber.lastInteraction).fromNow()
        } else {
          row.lastInteraction = ''
          row.lastInteractionText = '-'
        }

        row.otherInfoText = ''
        if (row.otherInfo) {
          // console.log(Object.keys(row.otherInfo))
          for (let key of Object.keys(row.otherInfo)) {
            row.otherInfoText += key + ', '
          }
          row.otherInfoText = row.otherInfoText.slice(0, row.otherInfoText.length - 2)
        } else {
          row.otherInfoText = '-'
        }
        
        // row.labelText = ''
        // if (row.labels.length) {
        //   for (let label of row.labels) {
        //     row.labelText += label + ', '
        //   }
        //   row.labelText = row.labelText.slice(0, row.labelText.length - 2)
        // } else {
        //   row.labelText = '-'
        // }

        data.push(row)
      }
      this.rows = [...data]

      this.loadingSubscribers = false
      this.datatable.messages.emptyMessage = "Không có dữ liệu";
      this.firstLoad = false
    } catch (err) {
      // console.error(err)
      // this.alert.error("Lỗi khi load dữ liệu", err.message)
      this.alert.handleError(err);
    } finally {
      this.detectChanges()
    }

    this.checkFilterSubtitle()
  }

  paramQueryToFilterData() {

    forOwn(this.basicFilters, (value, key) => {
      switch(key) {
        case 'subscribe_date': case 'last_interaction':
          value.data = { start: null, end: null }
          break;
        default:
          value.data = []
      }
    })
    this.advancedFilters = null
    this.otherFilters = []

    let subscribeChecked = false
    let interactionChecked = false

    forOwn(this.paramQuery, (value, key) => {
      let code = key
      switch (key) {
        case 'referralAccess': code = 'referral'; break;
        case 'isHavePhone': code = 'phone'; break;
        case 'otherParams': code = 'other'; break;
        case 'startSubscribeTime': code = 'subscribe_date'; break;
        case 'endSubscribeTime': code = 'subscribe_date'; break;
        case 'startLastInteraction': code = 'last_interaction'; break;
        case 'endLastInteraction': code = 'last_interaction'; break;
        default: break;
      }

      switch (code) {
        case 'subscribe_date': 
          if (!subscribeChecked) {
            if (this.paramQuery.startSubscribeTime) {
              this.basicFilters.subscribe_date.data.start = moment(this.paramQuery.startSubscribeTime).toDate()
            } else {
              this.basicFilters.subscribe_date.data.start = null
            }
            if (this.paramQuery.endSubscribeTime) {
              this.basicFilters.subscribe_date.data.end = moment(this.paramQuery.endSubscribeTime).toDate()
            } else {
              this.basicFilters.subscribe_date.data.end = null
            }
            subscribeChecked = true
          } else {
            return
          }
        break;
        case 'last_interaction': 
          if (!interactionChecked) {
            if (this.paramQuery.startLastInteraction) {
              this.basicFilters.last_interaction.data.start = moment(this.paramQuery.startLastInteraction).toDate()
            } else {
              this.basicFilters.last_interaction.data.start = null
            }
            if (this.paramQuery.endLastInteraction) {
              this.basicFilters.last_interaction.data.end = moment(this.paramQuery.endLastInteraction).toDate()
            } else {
              this.basicFilters.last_interaction.data.end = null
            }
            interactionChecked = true
          } else {
            return
          }
        break;
        case 'localies':
          let localies = []
          for (let l of value) {
            let iso_code = l.substring(3, 5)
            if (localies.findIndex(x => x == iso_code) == -1) {
              localies.push(iso_code)
            }
          }
          let localiesArray = []
          localies.forEach(local => {
            localiesArray.push(this.basicFilters.localies.selections.find(x => x.code == local))
          })
          this.basicFilters.localies.data = localiesArray;
        break;
        case 'phone':
          if (value) {
            this.basicFilters.phone.data = [this.basicFilters.phone.selections.find(x => x.code == 'true')]
          } else {
            this.basicFilters.phone.data = [this.basicFilters.phone.selections.find(x => x.code == 'false')]
          }
        break;
        case 'tags':
          this.basicFilters.tags.data = value?value.map(x => ({ code: x, display: x})):[]
          break;
        // case 'labels':
        //   this.basicFilters.labels.data = value
        //   break;
        case 'other': 
          this.advancedFilters = {}
          forOwn(value, (value2, key2) => {
            if (key2.startsWith('otherInfo.')) {
              let type
              let field = key2.replace('otherInfo.','')
              if (has(value2, '#exists') && value2['#exists'] == true) {
                type = '#exists'
              } else if (has(value2, '#exists') && value2['#exists'] == false) {
                type = '#non_exists'
              } else {
                type = Object.keys(value2)[0]
              }
              this.otherFilters.push({
                key: field,
                value: (type=='#exists'||type=='#non_exists')?'':value2[type],
                type: type
              })
            } else {
              this.advancedFilters[key2] = value2
            }
          })
          if (Object.keys(this.advancedFilters).length == 0) this.advancedFilters = null
        break;
        default: 
          if (this.basicFilters[code] && this.basicFilters[code].selections) {
            let array = []
            value.forEach(val => {
              array.push(this.basicFilters[code].selections.find(x => x.code == val))
            })
            this.basicFilters[code].data = array;
          }
        break;
      }
    })
    this.detectChanges()
  }

  filterDataToParamQuery() {
    this.paramQuery = {}
    console.log(this.basicFilters)
    forOwn(this.basicFilters, (value, key) => {
      switch (key) {
        case 'gender':
          if (this.basicFilters.gender.data.length) {
            this.paramQuery.gender = this.basicFilters.gender.data.map(data => { return data.code } )
          }
          break
        case 'phone':
          if (this.basicFilters.phone.data.length == 1) {
            this.paramQuery.isHavePhone = (this.basicFilters.phone.data[0].code=='true')
          }
          break
        case 'status':
          if (this.basicFilters.status.data.length) {
            this.paramQuery.status = this.basicFilters.status.data.map(data => { return data.code } )
          }
          break
        case 'localies':
            if (this.basicFilters.localies.data.length) {
              let localies = []
              for (let l of this.basicFilters.localies.data) {
                localies.push(countryCode.countriesArray.find(x => x.code == l.code).variations)
              }
              this.paramQuery.localies =  flatten(localies);
            }
          break
        case 'tags': 
          if (this.basicFilters.tags.data.length) {
            this.paramQuery.tags = this.basicFilters.tags.data.map(data => { return data.code } )
          }
          break;
        // case 'labels':
        //     if (this.basicFilters.labels.data.length) {
        //       this.paramQuery.labels =  this.basicFilters.labels.data
        //     }
        //   break
        case 'referral':
            if (this.basicFilters.referral.data.length) {
              this.paramQuery.referralAccess =  this.basicFilters.referral.data.map(data => { return data.code } )
            }
          break
        case 'subscribe_date':
          if (this.basicFilters.subscribe_date.data.start) {
            this.paramQuery.startSubscribeTime =  moment(this.basicFilters.subscribe_date.data.start).toDate();
          }
          if (this.basicFilters.subscribe_date.data.end) {
            this.paramQuery.endSubscribeTime =  moment(this.basicFilters.subscribe_date.data.end).endOf('day').toDate();
          }
          break
        case 'last_interaction':
          if (this.basicFilters.last_interaction.data.start) {
            this.paramQuery.startLastInteraction = moment(this.basicFilters.last_interaction.data.start).toDate();
          }
          if (this.basicFilters.last_interaction.data.end) {
            this.paramQuery.endLastInteraction =  moment(this.basicFilters.last_interaction.data.end).endOf('day').toDate();
          }
        break        
        default:
          break
      }

      if (this.advancedFilters) this.paramQuery.otherParams = cloneDeep(this.advancedFilters)

      this.otherFilters.forEach(filter => {
        if (!filter.key || (filter.type != '#exists' && filter.type != "#non_exists" && !filter.value)) return
        if (!this.paramQuery.otherParams) this.paramQuery.otherParams = {}
        if (!this.paramQuery.otherParams['otherInfo.' + filter.key]) this.paramQuery.otherParams['otherInfo.' + filter.key] = {}
        switch(filter.type) {
          case '#exists': 
            filter.value = ''
            this.paramQuery.otherParams['otherInfo.' + filter.key]['#exists'] = true
            break;
          case '#non_exists': 
            filter.value = ''
            this.paramQuery.otherParams['otherInfo.' + filter.key]['#exists'] = false 
            break;
          case '#regex':
            this.paramQuery.otherParams['otherInfo.' + filter.key][filter.type] = filter.value 
            break;
          case '#not':
            this.paramQuery.otherParams['otherInfo.' + filter.key][filter.type] = new RegExp(filter.value)
            break;
          default:
            let parsedValue = Number(filter.value)
            this.paramQuery.otherParams['otherInfo.' + filter.key][filter.type] = parsedValue?parsedValue:filter.value
            break;
        }
      })
    })

    this.checkFilterSubtitle()
    this.detectChanges()
  }

  async checkFilterSubtitle() {
    this.filterSubtitle.basic = ''
    forOwn(this.basicFilters, (value, key) => {
      switch(key) {
        case 'subscribe_date': case 'last_interaction':
          if (value.data.start || value.data.end) this.filterSubtitle.basic += value.display + ', '
        break;
        default:
          if (value.data && value.data.length) this.filterSubtitle.basic += value.display + ', '
        break;
      }
    })
    if (this.filterSubtitle.basic) this.filterSubtitle.basic = this.filterSubtitle.basic.substr(0, this.filterSubtitle.basic.length - 2)

    // this.filterSubtitle.other = ''
    // this.otherFilters.forEach(other => {
    //   if (other.key) this.filterSubtitle.other += other.key + ', '
    // })
    // if (this.filterSubtitle.other) this.filterSubtitle.other = this.filterSubtitle.other.substr(0, this.filterSubtitle.other.length - 2)

    this.filterSubtitle.advanced = this.advancedFilters?JSON.stringify(this.advancedFilters):''
    this.changeRef.detectChanges()
  }

  async editName(create = false, copy = false) {
    if (this.group.isAll) create = true
    let formOptions: iPopoverFormOptions = {
      parent: '.swiper-slide',
      target: '#customers-portal-name',
      width: 300,
      confirm: create?'Tạo nhóm mới':'Lưu tên nhóm',
      fields: [
        {
          placeholder: create?'Tên nhóm mới':'Tên nhóm',
          property: 'name',
          current: create?'':this.group.name,
          constraints: ['required']
        }
      ],
      onSubmit: (data) => {
        if (this.group.name == data.name || !data.name) return
        let fallback = this.group.name
        if (create) {
          let fallbackGroup = this.group
          const { app } = this.chatbotApi.chatbotAuth
          let newGroup = {
            "name": data.name,
            "page": isObject(app.activePage) ? (app.activePage as iPage)._id : app.activePage as string,
            "isRandom": false,
            // "paramQuery": JSON.stringify(this.paramQuery)
            // "paramQuery": this.getRenamedParamQuery()
            "paramQuery": copy?this.paramQuery:{}
          }
          this.setGroup(newGroup);
          this.chatbotApi.group.add({ reload: true, ...newGroup })
          .then(result => { 
            this.group = result; 
            this.groups.push(result);
            this.hideSave(); 
            this.detectChanges() })
          .catch(err => {
            console.error(err)
            this.setGroup(fallbackGroup)
            this.groups.pop()
            this.detectChanges()
          })
        } else {
          this.group.name = data.name
          this.portalName = data.name
          this.groups.find(x => x._id == this.group._id).name = data.name
          this.chatbotApi.group.update(this.group._id, { reload: true, name: data.name })
          .then(result => {
            this.detectChanges()
          })
          .catch(err => {
            console.error(err)
            this.group.name = fallback
            this.portalName = fallback
            this.groups.find(x => x._id == this.group._id).name = fallback
            this.detectChanges()
          })
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  setGroup(item) {
    this.group = item
    this.setPortalName(this.group.name)
    let paramQueryString = ''
    if (typeof(this.group.paramQuery)=='string')  {
      paramQueryString = this.group.paramQuery
      this.group.paramQuery = JSON.parse(paramQueryString)
      this.paramQuery = JSON.parse(paramQueryString)
    } else {
      this.paramQuery = cloneDeep(this.group.paramQuery)
    }
    if (!this.paramQuery) this.paramQuery = {}
    if (!this.pagination.limit || !this.limitOptions.includes(this.pagination.limit)) this.pagination.limit = 10
    this.offset = 0
    this.filterSubtitle.basic = ''
    this.filterSubtitle.advanced = ''
    this.selected = []
    this.paramQueryToFilterData()
    this.reload()
    this.detectChanges()
  }

  async deleteGroup() {
    if (!this.group.isAll) {
      if (!await this.alert.warning('Xóa nhóm', 'Bạn có chắc chắn muốn xóa nhóm này không')) return
      try {
        await this.chatbotApi.group.delete(this.group._id)
        this.groups.splice(this.groups.findIndex(x => x._id == this.group._id), 1)
        this.setGroup(this.groups[0])
      } catch (err) {
        this.alert.handleError(err, 'Lỗi khi xóa nhóm');
        // this.alert.error('Lỗi khi xóa nhóm', err.message)
      } finally {
        this.detectChanges()
      }
    }
  }

  async setPage(pageInfo) {
    this.offset = pageInfo.offset
    if (!this.firstLoad) await this.reload()
  }

  async limitChanged() {
    this.selected = []
    this.offset = 0
    if (!this.firstLoad) await this.reload()
  }

  filterItems() {
    this.selected = []
    this.filterDataToParamQuery();
    this.offset = 0
    this.reload()
    this.detectChanges()
  }

  async searchChanged() {
    if (this.previousSearch != this.searchString) {
      this.selected = []
      this.offset = 0
      this.reload()
    }
  }

  async addOtherFilter(event) {
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      targetElement: event.target,
      width: 650,
      confirm: 'Tạo bộ lọc',
      fields: [
        {
          type: 'filter',
          placeholder: '',
          property: 'filter',
          current: {
            key: '',
            value: '',
            type: '#regex'
          },
          constraints: ['required']
        }
      ],
      onSubmit: (data) => {
        if (data.filter) {
          this.otherFilters.push(data.filter)
          this.filterItems()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  async editFilter(index, event) {
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      targetElement: event.target,
      width: 650,
      confirm: 'Lưu',
      fields: [
        {
          type: 'filter',
          placeholder: '',
          property: 'filter',
          current: {...this.otherFilters[index]},
          constraints: ['required']
        }
      ],
      onSubmit: (data) => {
        if (data.filter) {
          this.otherFilters[index] = data.filter
          this.filterItems()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  async deleteFilter(index) {
    let isValid = true
    if (!this.otherFilters[index].key || 
      (this.otherFilters[index].type != '#exists' && this.otherFilters[index].type != "#non_exists" && !this.otherFilters[index].value)) isValid = false
    this.otherFilters.splice(index, 1)
    if (isValid) {
      this.filterItems()
    }
    this.detectChanges()
  }
  
  async updateGroup() {
    this.showSaving()
    this.detectChanges()
    try {
      const updateGroup = await this.chatbotApi.group.update(this.group._id, { 
        // paramQuery: this.getRenamedParamQuery() 
        // paramQuery: JSON.stringify(this.paramQuery)
        "paramQuery": this.paramQuery
      })
      this.group = updateGroup
      this.groups[this.groups.findIndex(x => x._id == this.group._id)] = this.group
      this.showSaveSuccess()
    } catch (err) {
      // console.error(err)
      // this.alert.error("Lỗi khi lưu nhóm", err.message)
      this.alert.handleError(err,"Lỗi khi lưu nhóm");
      this.showSaveButton()
    } finally {
      this.detectChanges()
    }
  }

  filterDropdownChanged(event) {
    if (event) {
      this.tempBasicFilters = null
      this.tempBasicFilters = cloneDeep(this.basicFilters)
    }
    this.detectChanges()
  }

  basicFilterItems() {
    this.basicFilters = cloneDeep(this.tempBasicFilters)
    this.filterItems()
  }
  
  openJsonDialog() {
      let dialogRef = this.dialog.open(JsonDialog, {
          width: '600px',
          data: this.advancedFilters
      })
      dialogRef.afterClosed().subscribe(result => {
          if (result && this.advancedFilters != result) {
            this.advancedFilters = result
            this.filterItems()
          }
      })
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    this.detectChanges()
  }

  async onActivate(event) {
    if (event.column && event.column.name == "Checkbox") return;
    if (event.type == "click") {
      const subscriber = event.row
      if (this.portalService.getLastPortalName() == "SubscriberDetailPortalComponent") {
        const component = this.portalService.container.portals[this.portalService.container.portals.length - 1] as any;
        await component.reload(false,subscriber._id);
        component.focus()
      } else {
        this.portalService.pushPortalAt(this.index + 1, "SubscriberDetailPortalComponent", {
          portalName: subscriber.name,
          subscriberId: subscriber._id
        })
      }
    }
    this.detectChanges()
  }

  async send() { 
    if (this.showSave) {
      this.alert.info('Không thể gửi', 'Xin lưu vào nhóm hoặc tạo nhóm mới trước khi gửi')
      return
    }

    this.dialog.open(SendDialog, {
      width: '700px',
      data: {
        targetId: this.selected.length?this.selected:this.group._id,
        targetName: this.group.name,
        targetMode: this.selected.length?'subscriber':'group'
      }
    })
  }

  async addLabels() {
    let data: iEditInfoDialogData = {
      title: "Thêm nhãn cho khách hàng",
      confirmButtonText: "Xác nhận",
      inputs: [{
        title: "Nhãn",
        property: "label",
        current: "",
        type: "text",
        required: true,
      }]
    };
    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    });

    const pageId = (this.chatbotApi.chatbotAuth.app.activePage as iPage)._id
    let subscriberIds = []
    this.selected.forEach(selection => {
      subscriberIds.push(selection._id)
    })

    dialogRef.afterClosed().toPromise().then(async result => {
      if (result && result.label) {
        try {
          await this.chatbotApi.label.addLabel(pageId, subscriberIds, result.label)
          this.alert.success("Thành công", "Thêm nhãn thành công")
        } catch (err) {
          this.alert.handleError(err,"Lỗi khi thêm nhãn");
          // this.alert.error("Lỗi khi thêm nhãn", err.message)
        } finally {
          this.detectChanges()
        }
      }
    })
  }

  async export() {
    if (this.canBeSaved) {
      this.alert.info('Không thể xuất file vì bộ lọc chưa được lưu', 'Cần lưu thay đổi nhóm hoặc tạo nhóm mới trước khi xuất')
      return
    }

    let data: iSelectionData = {
      title: 'Chọn phương thức xuất CSV',
      confirmButtonText: 'Xuất CSV',
      cancelButtonText: 'Quay lại',
      categories: [{
        name: 'Cơ bản',
        options: [
          { code: 'subscriber', icon: 'fas fa-check-square', display: 'Xuất các khách hàng đang được chọn ' + 
            (this.selected.length?('(' + this.selected.length + ' khách hàng)'):('(Chưa chọn khách hàng nào)')),
            disabled: this.selected.length?false:true },
          { code: 'group', icon: 'fas fa-users', display: 'Xuất tất cả mọi người trong nhóm (' + 
            this.pagination.totalItems + ' khách hàng)' }
        ]
      }]
    };
    let dialogRef = this.selectionDialog.open(SelectionDialog, {
      width: '600px',
      data: data
    });
    dialogRef.afterClosed().toPromise().then(async result => {
      if (result) {
        switch(result) {
          case 'subscriber':
            this.exportSelectedToCsv()
            break;
          case 'group':
            this.exportCsv();
            break;
        }
      }
    });
  }

  async exportCsv() {
    try{
      // await this.chatbotApi.group.exportCsv(this.group._id, this.group.name)
      this.chatbotApi.group.exportExcel(this.group._id, this.group.name);
    }catch (err){
      // console.log(err)
      this.alert.handleError(err, "Lỗi khi xuất file");
      // this.alert.error("Lỗi khi xuất file", err.message)
    }
  }

  async exportSelectedToCsv() {
    let data: any = []
    for (const select of this.selected) {
      data.push({
        uid: select.uid?select.uid:"",
        psid: select.messengerProfile.id?select.messengerProfile.id:"",
        name: select.full_name,
        gender: select.gender,
        locale: select.locale,
        labels: select.labels,
        phone: select.phone?select.phone:"",
        status: select.status?select.status:""
      })
    }
    converter.json2csvAsync(data).then(result => {
      var hiddenElement = document.createElement('a');
      let csv = "\uFEFF" + result
      hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
      hiddenElement.target = '_blank';
      hiddenElement.download = `data.csv`;
      hiddenElement.click();
    }).catch(err => {
      this.alert.handleError(err, 'Lỗi khi xuất file')
    })
  }

  // async syncLabels() {
  //   try {
  //     this.labelsSynchronizing = true
  //     this.detectChanges()
  //     await this.chatbotApi.subscriber.syncLabels()
  //     await this.reload(false)
  //   } catch (err) {
  //     this.alert.handleError(err);
  //     // this.alert.error("Lỗi", err.message)
  //   } finally {
  //     this.labelsSynchronizing = false
  //     this.detectChanges()
  //   }
  // }
  async checkIsStatistic(){
    console.log( 'group', this.group)
    if(this.group.isAll){
      await this.portalService.pushPortalAt(this.index + 1, "StatisticsResultPortalComponent", { 
        portalName: "Thống kê tất cả người dùng",
        mode: "subscriber",
      })
      return false
    }

    if(this.group.isStatistic) return true
    if(!await this.alert.question("Bật thống kê cho nhóm","Hiện tại nhóm chưa bật tính năng thống kê. Xác nhận bật tính năng thông kê cho nhóm")) return false
    try{
      await this.chatbotApi.group.update(this.group._id,{isStatistic: true})
      this.group.isStatistic = true
      await this.alert.success("Thành công","Bật tính năng thống kê thành công")
      return true
    }catch(err){
      console.log( 'err',err)
      this.alert.handleError(err,"Bật thống kê thất bại")
      return false
    }

  }
  async openGroupStatictis(){

    if(! await this.checkIsStatistic()) return

    await this.portalService.pushPortalAt(this.index + 1, "StatisticsResultPortalComponent", { 
      portalName: "Thống kê theo nhóm",
      mode: "group",
      data:{ groupId:this.group._id}
    })
  
  }

  async onSort(event) {
    const getType = value => (value == 'asc' ? 1 : -1);
    let prop = event.column.prop
    if (prop == 'full_name') prop = 'messengerProfile.name'
    else if (prop == 'gender') prop = 'messengerProfile.gender'
    else if (prop == 'locale') prop = 'messengerProfile.locale'

    this.onSelect({ selected: [] });
    this.sort = { [prop]: getType(event.newValue) };
    this.reload();
  }

  async refresh() {
    this.onSelect({ selected: [] });
    this.sort = {}
    this.reload(false);
  }
}
