
import { Component, OnInit, Input, ViewChild, NgZone, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { NgForm } from '@angular/forms'
import { MatSnackBar, MatDialog } from '@angular/material'
import { iApp, iUser } from 'app/services/chatbot/api/crud/app';
import { UserTransferOwnerDialog } from '../../../modals/user-transfer-owner/user-transfer-owner.component';
import { AppInviteCustomerDialog } from 'app/modals/app-invite-customer/app-invite-customer.component';
@Component({
  selector: 'app-user-access-portal',
  templateUrl: './user-access-portal.component.html',
  styleUrls: ['./user-access-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserAccessPortalComponent extends BasePortal implements OnInit {
  
  componentName = "UserAccessPortalComponent"
  app: iApp
  owner: iUser
  currentUser: iUser
  users: iUser[]

  userRoles:any[] = [
    {code:'admin',display:"Admin",link:"",loadDone:true},
    {code:'write',display:"Write",link:"",loadDone:true},
    {code:'read',display:"Read",link:"",loadDone:true},
    {code:'chatter',display:"Chat",link:"",loadDone:true},
  ]

  inviteLinkRole = 'admin'
  inviteEmailRole = 'admin'
  inviteLinks = { admin: '', write: '', read: '' }
  inviteLink: string
  inviteEmail: string
  generatingLink: boolean = false
  sendingEmail:boolean = false

  loadDone: boolean = false
  
  @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
  constructor(
    public changeRef: ChangeDetectorRef,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
  ) {
    super()
    this.setPortalName("Quyền truy cập")
  }

  async ngOnInit() {
    this.reload()

    if (this.chatbotApi.chatbotAuth.user.rule == 'admin') this.generateLink()
  }

  async ngAfterViewInit() {
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async inviteLinkRoleChanged() {
    this.generateLink()
  }

  copyId(){
    let input = document.getElementById('link-url') as HTMLInputElement
    input.value = this.inviteLink
    input.select();
    document.execCommand('copy',false);
    this.snackBar.open('✔️ Đã copy vào clipboard', '', {
      duration: 2000,
    });
    this.changeRef.detectChanges()
  }

  async generateLink() {
    this.inviteLink = ''
    try {
      this.generatingLink = true
      this.changeRef.detectChanges()
      
      if (!this.inviteLinks[this.inviteLinkRole]) {
        let res = await this.chatbotApi.app.generatorLink(this.inviteLinkRole)
        let inviteLink = res.inviteLink
        if (location.host != 'bot.mcom.app') inviteLink = inviteLink.replace('bot.mcom.app', location.host)
        this.inviteLinks[this.inviteLinkRole] = inviteLink
      } else {
        await new Promise(resolve => setTimeout(resolve, 500))
      }
      this.inviteLink = this.inviteLinks[this.inviteLinkRole]
    } catch(err) {
      // console.log( err)
      this.alert.handleError(err);
      // this.alert.error("Tạo đường dẫn thất bại", err.message)
    } finally {
      this.generatingLink = false
      this.changeRef.detectChanges()
    }
  }

  async sendEmail() {
    if (!this.inviteEmail) return
    let inviteData:any = {
      type: "email",
      rule: this.inviteEmailRole,
      email: this.inviteEmail
    }
    try {
      this.sendingEmail = true
      this.changeRef.detectChanges()
      await this.chatbotApi.app.invite(inviteData)
      this.inviteEmail = ''
      this.alert.success('Đã gửi thư mời', `Thư mời tham gia quản trị đã được gửi tới email ${inviteData.email}`)
    } catch(err) {
      this.alert.handleError(err, 'Không thể thêm người dùng này');
    } finally {
      this.sendingEmail = false
      this.changeRef.detectChanges()
    }
  }

  async reload(local = true) {
    try {
      this.loadDone = false
      this.changeRef.markForCheck()
      this.app = await this.chatbotApi.app.getItem(this.chatbotApi.chatbotAuth.app._id, { local }) as iApp

      this.users = [...this.app.users]
      let tasks = []
      for (let user of this.users) {
        tasks.push(this.chatbotApi.chatbotAuth.getUserInfo(user.uid).then(result => {
          this.users[this.users.findIndex(x => x.uid == user.uid)] = {...user, ...result}
        }))
      }
      await Promise.all(tasks)    
      
      this.owner = this.chatbotApi.chatbotAuth.app.users.find(u => u.uid === this.app.owner)
      this.currentUser = this.chatbotApi.chatbotAuth.app.users.find(u => u.uid === this.chatbotApi.chatbotAuth.firebaseUser.uid)

      console.log(this.users)
    } catch (err) {
      this.alert.handleError(err, 'Không thể lấy dữ liệu')
    } finally {
      this.loadDone = true
      this.changeRef.detectChanges()
    }
  }

  async changePermission(user, oldRole, role) {
    if (oldRole == role) return
    user.rule = role
    this.changeRef.detectChanges()
    try {
      await this.chatbotApi.app.changeUserRule({
        userId: user.uid,
        rule: role
      })
    } catch (err) {
      this.alert.handleError(err)
      user.rule = oldRole
    } finally {
      this.changeRef.detectChanges()
    }
  }

  async remove(user) {
    if (!await this.alert.warning('Xóa quyền truy cập app', 
    `Bạn có chắc chắn muốn xóa  ${user.displayName || user.name} khỏi app?`)) return
    try {
      await this.chatbotApi.app.deleteUser({ userId: user.uid })
      this.alert.success("Đã xoá người dùng", `Người dùng ${user.displayName || user.name} đã bị huỷ quyền truy cập`)
      this.reload(false)
    } catch(err) {
      this.alert.handleError(err, "Lỗi khi xóa");
      // this.alert.error("Không thành công", err.message)
    } finally {
      this.changeRef.detectChanges()
    }
  }

  async transfer() {    
    let dialogRef = this.dialog.open(UserTransferOwnerDialog, {
      width: '550px',
      data: {
        owner: this.owner,
        users: this.users,
        app: this.app
      }
    })
    
    dialogRef.afterClosed().toPromise().then(result => {
      if (result) {
        this.reload(false)
      }
    })
  }

  openInviteCustomerToApp() {
    let dialogRef = this.dialog.open(AppInviteCustomerDialog, {
      width: '500px',
      data: this.app
    })
    
    dialogRef.afterClosed().toPromise().then(result => {
      if (result) {
        this.reload()
      }
    })
  }
}
