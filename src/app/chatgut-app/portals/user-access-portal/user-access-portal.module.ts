import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserAccessPortalComponent } from 'app/chatgut-app/portals/user-access-portal/user-access-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatToolbarModule, MatIconModule, MatFormFieldModule, MatSelectModule, MatInputModule, MatTooltipModule, MatProgressSpinnerModule, MatSnackBarModule, MatDialogModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { UserTransferOwnerDialog } from '../../../modals/user-transfer-owner/user-transfer-owner.component';
import { AppInviteCustomerModule } from 'app/modals/app-invite-customer/app-invite-customer.module';
import { UserTransferOwnerDialogModule } from 'app/modals/user-transfer-owner/user-transfer-owner.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatSelectModule,
    MatIconModule,
    MatDialogModule,
    MatInputModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    UserTransferOwnerDialogModule,
    AppInviteCustomerModule
  ],
  declarations: [UserAccessPortalComponent],
  entryComponents: [UserAccessPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: UserAccessPortalComponent }
  ]
})
export class UserAccessPortalModule { }
