import { Component, Input, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { iStory } from 'app/services/chatbot/api/crud/story';
import { iTask, iBroadcast } from 'app/services/chatbot';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-story-manage-portal',
  templateUrl: './story-manage-portal.component.html',
  styleUrls: ['./story-manage-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StoryManagePortalComponent extends BasePortal {

  @Input() story: iStory
  tasks: iTask[] | iBroadcast[]
  loadDone: boolean = false
  loading: boolean = false
  broadcast: boolean = false

  processStates = {
    running: 'Đang gửi',
    schedule: 'Lên lịch',
    complete: 'Đã gửi',
    canceled: 'Đã hủy'
  }
  
  constructor(
    public changeRef: ChangeDetectorRef,
    private snackBar: MatSnackBar
  ) {
    super()
  }

  async ngOnInit() {
    this.reload()
  }

  async reload(local = true) {
    try {
      this.loading = true
      if (this.story.mode=='broadcast') this.broadcast = true
      if (this.broadcast) {
        this.tasks = await this.chatbotApi.broadcast.getList({ local, query: { limit: 0, filter: { story: this.story._id }, 
        order: { createdAt: -1 } } })
        let apiTasks = []
        for (let i = 0; i < this.tasks.length; i++) {
          let history = this.tasks[i]
          if ('SCHEDULED IN_PROGRESS'.includes(history.status)) {
            apiTasks.push(this.chatbotApi.broadcast.getItem(history._id).then(result => 
              this.tasks[i] = result
            ))
          }
        }
        await Promise.all(apiTasks)
        this.tasks.forEach(task => {
          task.reached = 0
          task.broadcasts.forEach(x => {
            task.reached += x.reached
          })

          // if (!reached || reached > task.reached) { reached = task.reached }
          task.value = 0
          if (task.status == 'ERROR') {
            task.processState = 'failed'
            task.value = 100
          }
          else if (task.status == 'IN_PROGRESS') {
            task.processState = 'running'
            task.value = 100
          }
          else if (task.status == 'SCHEDULED') {
            task.processState = 'scheduled'
            task.value = 0
          } else if (task.status == 'CANCELED') {
            task.processState = 'canceled'
            task.value = 100
          } else { 
            task.processState = 'complete'
            task.value = 100
          }
          task.subtitle = `<span>Số lượng thẻ: ${task.broadcasts.length}</span> <span>Đã gửi: ${task.reached}</span>`
          if (task.processState == 'running') {
            task.title = 'Đang gửi broadcast'
          } else if (task.processState == 'complete') {
            task.title = 'Gửi xong broadcast'
          } else if (task.processState == 'failed') {
            task.title = 'Lỗi khi gửi broadcast'
          } else if (task.processState == 'scheduled') {
            task.title = 'Đã lên lịch broadcast'
          } else if (task.processState == 'canceled') {
            task.title = 'Đã hủy broadcast'
          }

          // switch(history.status) {
          //   case 'FINISHED': history.statusText = 'Hoàn thành'; break;
          //   case 'IN_PROGRESS': history.statusText = 'Đang gửi'; break;
          //   case 'CANCELED': history.statusText = 'Đã hủy'; break;
          //   case 'ERROR': history.statusText = 'Bị lỗi'; break
          //   case 'SCHEDULED': history.statusText = 'Đã đặt lịch'; break;;
          // }
  
          // switch (history.type) {
          //   case 'label': history.typeText = 'Nhãn'; break;
          //   case 'all': history.typeText = 'Tất cả'; break;
          // }
        })
      } else {
        this.tasks = await this.chatbotApi.task.getList({ local, query: 
        { limit: 0, filter: { $and: [{ story: this.story._id }, {type: 'send_story' }] }, order: { createdAt: -1 } } })
        for (let task of this.tasks) {  
          task.subtitle = `<span ${task.completeProcess?`style="color: mediumseagreen"`:''}><i class="fas fa-check-circle"></i> ${task.completeProcess}</span> 
          <span ${task.failedProcess?`style="color: tomato"`:''}><i class="fas fa-exclamation-triangle"></i> ${task.failedProcess?'<b>' + task.failedProcess + '</b>':task.failedProcess}</span> 
          <span>Tổng: ${task.totalProcess}</span>
          <span>Đã xem: ${task.readCount}</span>`
          if (task.processState == 'running') {
            task.title = 'Đang gửi câu chuyện'
            if (task.payload && task.payload.lastFailedReason) task.error = task.payload.lastFailedReason
            task.value = (task.completeProcess + task.failedProcess) / task.totalProcess * 100
          } else if (task.processState == 'complete') {
            task.title = 'Gửi xong câu chuyện'
            if (task.payload && task.payload.lastFailedReason) task.error = task.payload.lastFailedReason
            task.value = 100
          } else if (task.processState == 'failed') {
            task.title = 'Gửi câu chuyện thất bại'
            if (task.payload && task.payload.lastFailedReason) task.error = task.payload.lastFailedReason
            task.value = 100
          } else if (task.processState == 'scheduled') {
            task.title = 'Đã lên lịch'
            if (task.payload && task.payload.lastFailedReason) task.error = task.payload.lastFailedReason
            task.value = 100
          } else if (task.processState == 'canceled') {
            task.title = 'Đã hủy'
            if (task.payload && task.payload.lastFailedReason) task.error = task.payload.lastFailedReason
            task.value = 100
          }
        }
      }

      

    } catch (err) {
      this.alert.handleError(err, 'Lỗi khi xem tác vụ')
    } finally {
      this.loadDone = true
      this.loading = false
      this.detectChanges()
    }
  }

  async cancelTask(task) {
    if (!await this.alert.warning('Ngừng tác vụ', 'Bạn có muốn hủy tác vụ này không?')) return

    try {
      if (this.broadcast) {
        await this.chatbotApi.broadcast.cancel(task._id)
      } else {
        await this.chatbotApi.task.cancel(task._id)
      }
      this.snackBar.open('✔️ Hủy tác vụ thành công', '', {
        duration: 2500
      })
      this.reload(false)
    } catch (err) {
      this.alert.handleError('Hủy tác vụ thất bại', 'Không thể hủy tác vụ')
    }
  }
}
