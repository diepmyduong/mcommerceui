import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { FormsModule } from '@angular/forms';
import { StoryManagePortalComponent } from './story-manage-portal.component';
import { MatProgressSpinnerModule, MatToolbarModule, MatButtonModule, MatTooltipModule, MatIconModule, MatSnackBarModule } from '@angular/material';
import { SafePipeModule } from 'app/shared/pipes/safe-pipe/safe-pipe.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatToolbarModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    SafePipeModule,
    MatSnackBarModule
  ],
  declarations: [StoryManagePortalComponent],
  entryComponents: [StoryManagePortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: StoryManagePortalComponent }
  ]
})
export class StoryManagePortalModule { }
