import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatisticsDetailsResultPortalComponent } from 'app/chatgut-app/portals/statistics-details-result-portal/statistics-details-result-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { MatToolbarModule, MatIconModule, MatProgressBarModule, MatProgressSpinnerModule, MatDividerModule, MatTooltipModule, MatButtonModule } from '@angular/material';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
  imports: [
    CommonModule,
    EditInfoModule,
    MatToolbarModule,
    MatIconModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatDividerModule,
    MatTooltipModule,
    MatButtonModule,
    NgxChartsModule
  ],
  declarations: [StatisticsDetailsResultPortalComponent],
  entryComponents: [StatisticsDetailsResultPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: StatisticsDetailsResultPortalComponent }
  ]
})
export class StatisticsDetailsResultPortalModule { }
