import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { iEditInfoDialogData, EditInfoDialog } from 'app/modals/edit-info/edit-info.component';
import { MatDialog } from '@angular/material';
import { AlertService } from 'app/services/alert.service';
import { iPage } from 'app/services/chatbot/api/crud/page';
@Component({
  selector: 'app-statistics-details-result-portal',
  templateUrl: './statistics-details-result-portal.component.html',
  styleUrls: ['./statistics-details-result-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatisticsDetailsResultPortalComponent extends BasePortal implements OnInit {
  componentName = "StatisticsDetailsResultPortalComponent"
  @Input() mode: 'story' | 'topic' | 'post' | 'referral'
  @Input() storyId: string;
  @Input() topicId: string;
  @Input() postId: string;
  @Input() referralId: string;
  @Input() name: string;
  subName: string
  storyData = []
  topicData = []
  referralData = []
  storyScheme = { domain: ["#81C784", "#FF8A65"] }
  topicScheme = { domain: ["#536DFE", "#FF5252"] }
  referralScheme = { domain: ["#FF8A65", "#FF80AB"] }
  totalEventCounts = 0
  loadDone = false;
  result: any
  hasFooter = true
  modeDisplay: string

  constructor(
    public dialog: MatDialog,
    public alert: AlertService,
    public changeRef: ChangeDetectorRef
  ) {
    super()
  }

  async ngOnInit() {
    setTimeout(() => { this.reload() }, 10)
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(refresh = false) {
    try{
      this.loadDone = false
      this.detectChanges()
      switch (this.mode) {
        case 'story': await this.onReloadStory(refresh); this.modeDisplay = 'Câu chuyện'; break;
        case 'topic': await this.onReloadTopic(refresh); this.modeDisplay = 'Chủ đề'; break;
        case 'post': await this.onReloadPost(refresh); this.modeDisplay = 'Bài viết'; break;
        case 'referral': await this.onReloadReferral(refresh); this.modeDisplay = 'Đường dẫn'; break;
      }
      this.subName = 'Thống kê ' + this.modeDisplay.toLowerCase()
    } catch(err){
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi lấy dữ liệu", err.message)
    } finally{
      this.loadDone = true
      this.detectChanges()
    }
    setTimeout(() => {
      if (this.mode == 'referral' && this.result.amount == 0) {
        document.getElementsByClassName("total-value")[0].innerHTML = "∞";
        // document.getElementsByClassName("item-value")[1].innerHTML = "∞"
      }
    })
  }

  async onReloadReferral(refresh = false) {
    this.result = await this.chatbotApi.statistic.summaryReferralDetail({ id: this.referralId }, refresh)
    if (this.result.amount !== 0) {
      this.referralData = [{
        name: 'Đã dùng',
        value: this.result.used
      }, {
        name: 'Còn lại',
        value: this.result.amount !== 0 ? this.result.amount - this.result.used : 0
      }]
    } else {
      this.referralData = [{
        name: 'Đã dùng',
        value: this.result.used
      }]
    }
    this.detectChanges()

  }

  async onReloadPost(refresh = false) {
    this.result = await this.chatbotApi.statistic.summaryInteractPostDetail({ id: this.postId }, refresh)
    this.detectChanges()
    console.log( {result:this.result})
  }

  async onReloadStory(refresh = false) {
    this.result = await this.chatbotApi.statistic.summaryStoryDetail({ id: this.storyId }, refresh)
    this.storyData = [
      { name: 'Thành công', value: this.result.sentSuccess.total },
      { name: 'Thất bại', value: this.result.sentFailed.total }
    ]
    this.detectChanges()
  }

  async onReloadTopic(refresh = false) {
    this.result = await this.chatbotApi.statistic.summaryTopicDetail({ id: this.topicId }, refresh)
    this.totalEventCounts = 0
    this.result.events.forEach(event => {
      this.totalEventCounts += event.eventCount
    })
    this.topicData = [
      { name: 'Đăng ký', value: this.result.totalSubscriber },
      { name: 'Đã hủy', value: this.result.totalUnsubscriber },
    ]
    this.detectChanges()
  }

  async openTopic() {
    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "TopicPortalComponent", {
      topicId: this.topicId,
      portalName: this.name
    })
  }

  async openStory() {
    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "StoryDetailPortalComponent", {
      storyId: this.storyId,
      portalName: this.name
    })
  }

  async openPost() {
    this.portalService.pushPortalAt(this.index + 1, "PostDetailPortalComponent", {
      interactPostId: this.postId,
      loadIndividualPost: true
    })
  }

  async openSubscriber(subscriberId) {

    if (this.portalService.getLastPortalName() == "SubscriberDetailPortalComponent") {
      const component = this.portalService.container.portals[this.portalService.container.portals.length - 1] as any;
      await component.reload(subscriberId);
      component.focus()
    } else {
      this.portalService.pushPortalAt(this.index + 1, "SubscriberDetailPortalComponent", { subscriberId })
    }
  }

  async openReferral() {
    this.portalService.pushPortalAt(this.index + 1, "StoryReferralPortalComponent", { refId: this.referralId })
  }

  async addLabels(subscribers: any[]) {
    let data: iEditInfoDialogData = {
      title: "Thêm nhãn cho khách hàng",
      confirmButtonText: "Xác nhận",
      inputs: [{
        title: "Nhãn",
        property: "label",
        current: "",
        type: "text",
        required: true,
      }]
    };
    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    });

    const pageId = (this.chatbotApi.chatbotAuth.app.activePage as iPage)._id;
    let subscriberIds = [];
    subscribers.forEach(sub => {
      subscriberIds.push(sub._id)
    })

    dialogRef.afterClosed().toPromise().then(async result => {
      if (result && result.label) {
        try {
          await this.chatbotApi.label.addLabel(pageId, subscriberIds, result.label)
          this.alert.success("Thành công", "Thêm nhãn thành công")
        } catch (err) {
          this.alert.handleError(err, "Lỗi khi thêm nhãn");
          // this.alert.error("Có lỗi xảy ra khi thêm nhãn", err.message)
        } finally {
          this.detectChanges()
        }
      }
    })
  }

  async refresh() {
    this.reload(true)
  }
}
