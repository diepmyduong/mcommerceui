import { Component, OnInit, Input, Output, EventEmitter, ViewChild, Host, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import { iReferral } from 'app/services/chatbot';
import { NgForm } from '@angular/forms';
import { StoryReferralPortalComponent } from 'app/chatgut-app/portals/story-referral-portal/story-referral-portal.component';

@Component({
  selector: 'story-referral-optional',
  templateUrl: './optional.component.html',
  styleUrls: ['./optional.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptionalComponent extends BaseComponent implements OnInit {

  @Input() referral: iReferral
  @Output() change = new EventEmitter<iReferral>()
  @ViewChild('myFrm', { read: NgForm }) myFrm: NgForm
  constructor(
    @Host() public referralPortal: StoryReferralPortalComponent,
    public changeRef: ChangeDetectorRef
  ) {
    super('OptionalComponent')
  }
  canEmmitChange: boolean = false;
  
  async ngOnInit() {
    this.subscriptions.push(this.myFrm.valueChanges.subscribe(this.formChange.bind(this)))
    setTimeout(() => {
      this.canEmmitChange = true
      this.referral.isOptionalValid = this.myFrm.valid
      this.changeRef.detectChanges()
    }, 1000)
    this.reload()
  }

  ngAfterViewInit() {
    this.referral.isOptionalValid = this.myFrm.valid
    this.changeRef.detectChanges()
  }

  async reload(local: boolean = true) {
  }

  formChange() {
    if (!this.canEmmitChange && !this.myFrm.dirty) return
    this.referral.isOptionalValid = this.myFrm.valid
    this.change.emit(this.referral)
  }
}