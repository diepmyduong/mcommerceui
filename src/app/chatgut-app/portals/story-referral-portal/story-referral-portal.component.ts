import { merge } from 'lodash-es'
import * as guid from 'guid'
import * as moment from 'moment'
import { Component, OnInit, Input, ViewChild, EventEmitter, Output, ElementRef, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { BasePortal } from "app/chatgut-app/portals/portal-container/base-portal";
import { NgForm } from "@angular/forms";
import { MatSnackBar, MatDialog } from "@angular/material";
import { ITime } from "app/shared/material-time/time-control";
import { iEditInfoDialogData, EditInfoDialog } from "app/modals/edit-info/edit-info.component";
import { ImagePopupDialog } from "app/modals/image-popup/image-popup.component";
import { iReferral } from 'app/services/chatbot/api/crud/referral';
declare var Jimp, AwesomeQRCode: any
@Component({
  selector: 'app-story-referral-portal',
  templateUrl: './story-referral-portal.component.html',
  styleUrls: ['./story-referral-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StoryReferralPortalComponent extends BasePortal implements OnInit {

  @Input() refId: string
  @Input() storyId: string
  @Input() mode: 'add' | 'detail' = 'detail'
  @Output() onRefCreated = new EventEmitter<iReferral>()
  @ViewChild('codeFrm', { read: NgForm }) codeFrm: NgForm
  @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
  @ViewChild("fileUploader") fileUploader: ElementRef;
  @ViewChild("swiper") swiperComp: any;
  constructor(
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef
  ) {
    super()
  }
  componentName = "StoryReferralPortalComponent"
  subName = 'Referral'
  refState: iReferral
  referral: iReferral = {
    name: '',
    limit: 0,
    amount: 0,
    link: ''
  }
  refRange: any = {}
  error: {
    date?: boolean,
    amount?: boolean,
    limit?: boolean
  } = {}
  createdRef: iReferral = undefined
  codeImg: string = ''
  creatingCode: boolean = false
  code: any = {
    id: guid.raw(),
    mode: '',
    src: "",
    imageSrc: "",
    mscode: '',
    mscodewithlogo: '',
    simple: '',
    logo: '',
    background: '',
    gif: '',
    files: null,
    option: {
      size: 500,
      margin: 0,
      dotScale: 1,
      whiteMargin: true,
      colorDark: "#000000",
      colorLight: "#ffffff",
      autoColor: false,
      maskedDots: false,
      backgroundDimming: 'rgba(0,0,0,0)',
      logoScale: 0.2,
      logoMargin: 6,
      logoCornerRadius: 500 * 0.2 / 2,
    }
  }
  codeTypes = [
    // { code: 'mscode', display: 'Messenger Code' },
    // { code: 'mscodewithlogo', display: 'Messenger Code & Hình ảnh' },
    { code: 'simple', display: 'QR Code' },
    { code: 'logo', display: 'QR Code & Hình ảnh' },
    { code: 'background', display: 'QR Code & Hình nền' },
    { code: 'gif', display: 'QR Code & Hình nền động' },
  ]
  selectedType: string = 'simple'
  isUploading: boolean = false
  swiperOptions = {
    pagination: {
      el: '.wifi-marketing-photos-pagination',
      type: 'bullets',
      clickable: true,
      dynamicBullets: true,
    },
    slidesPerView: 1,
    centeredSlides: true,
    navigation: {
      nextEl: '.wifi-marketing-photos-next',
      prevEl: '.wifi-marketing-photos-prev',
    },
    spaceBetween: 10,
    simulateTouch: false
  }
  // editDate:boolean = false
  // editCount:boolean = false
  private _this = this


  async ngOnInit() {
    await this.reload()
    
    setTimeout(()=>{
        this.subscriptions.formChanges = this.cardFrm.valueChanges.subscribe(change => {
          console.log( 'value',change)
          if (!this.isSaving) {
              this.showSaveButton()
              this.detectChanges()
          }
        })
    }, 200)
  }

  async ngAfterViewInit() {
  }
  
  async ngOnDestroy() {
    super.ngOnDestroy()
    if (this.subscriptions.formChanges)
      this.subscriptions.formChanges.unsubscribe()
  }

  updateRefState() {
    this.refState = merge({}, this.referral)
    this.detectChanges()
  }

  async reload(local: boolean = true) {
    try{
      if (this.mode === 'add') {
        this.referral = {
          type: 'story',
          option: {
            storyId: this.storyId
          },
          amount: 0,
          limit: 0
        }
        setTimeout(() => {
          this.setPortalName("New Referral")
          this.detectChanges()
        })
      } else {
        const referral = await this.chatbotApi.referral.getItem(this.refId, { local })
        this.setPortalName(referral.name)
        this.referral = merge({}, referral)
        this.updateRefState()
        const startDate = moment(this.referral.startDate).locale('en')
        const endDate = moment(this.referral.endDate).locale('en')
        this.refRange = {
          startDate: startDate.toDate(),
          startTime: {
            hour: startDate.get('hours'),
            minute: startDate.get('minute'),
            meriden: startDate.format('A')
          } as ITime,
          endDate: endDate.toDate(),
          endTime: {
            hour: endDate.get('hours'),
            minute: endDate.get('minute'),
            meriden: endDate.format('A')
          } as ITime,
        }
        this.detectChanges()
        // const mscode = await Jimp.read(this.referral.messengerCode);
        // if (this.referral.customQRCode && this.referral.customQRCode.logo) {
        //   await this.createMSCodeWithLogo(this.referral.customQRCode.logo)
        // }
        this.code.mode = 'simple'
        // this.code.src = this.referral.messengerCode
        // this.code.mscode = this.code.src
        this.codeImg = this.code.src
        if (!this.referral.wifiMKPhotos) this.referral.wifiMKPhotos = []
      }
    } catch (err){
      this.alert.handleError(err);
    } finally {
      this.detectChanges()
      this.createQRCode()
    }
  }

  getJimpBase64(jimp, mime: string = 'image/png') {
    return new Promise<string>((resolve, reject) => {
      jimp.getBase64(mime, (err, data) => {
        if (err) {
          reject(err)
          return
        }
        resolve(data)
      })
    })
  }

  getAwesomeQRData(setting: any) {
    return new Promise((resolve, reject) => {
      const awesomeQR = new AwesomeQRCode()
      if (setting.logoType === 'circle' || setting.logoCornerRadius > (setting.size * setting.logoScale / 2)) {
        setting.logoCornerRadius = setting.size * setting.logoScale / 2
      }
      setting.callback = (data) => {
        if (data) resolve(data)
        else reject()
      }
      awesomeQR.create(setting);
      this.detectChanges()
    })
  }

  getBufferFromSrc(src: string) {
    return new Promise(async (resolve, reject) => {
      const jimp = await Jimp.read(src)
      jimp.getBuffer('image/png', (err, buff) => {
        if (err) {
          reject(err)
          return
        } else {
          resolve(buff)
        }
      })
    })
  }

  getBlobFromURL(url: string) {
    return new Promise<Blob>((resolve, reject) => {
      var blob = null;
      var xhr = new XMLHttpRequest();
      xhr.open("GET", url);
      xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
      xhr.onload = () => {
        blob = xhr.response;//xhr.response is now a blob object
        resolve(blob)
      }
      xhr.send();
    })
  }

  async createMSCodeWithLogo(logoUrl: string) {
    const mscode = await Jimp.read(this.referral.messengerCode);
    const logo = await Jimp.read(logoUrl)
    const mask = await Jimp.read("assets/img/mscode-mask.png")
    logo.cover(500, 500)
    mscode.resize(500, 500)
    logo.mask(mask, 0, 0)
    mscode.composite(logo, 0, 0)
    this.code.mode = 'mscodewithlogo'
    this.code.src = await this.getJimpBase64(mscode)
    this.detectChanges()
  }

  async createLogoCode(src: string, option: any) {
    var image = new Image()
    image.crossOrigin = "Anonymous";
    this.showLoading()
    image.onload = async () => {
      try {
        const setting = merge({}, option)
        setting.text = this.referral.link
        setting.logoImage = image
        setting.backgroundImage = undefined
        setting.gifBackground = undefined
        setting.bindElement = this.code.id
        this.code.src = await this.getAwesomeQRData(setting)
        this.code.mode = "logo"
      } finally {
        this.hideLoading()
        this.detectChanges()
      }
    }
    image.src = await this.chatbotApi.deeplink.getImageBufferLink({ src })
    this.detectChanges()
  }

  async createBackgroundCode(src: string, option: any) {
    var image = new Image();
    image.crossOrigin = "Anonymous";
    this.showLoading()
    image.onload = async () => {
      try {
        const setting = merge({}, option)
        setting.text = this.referral.link
        setting.logoImage = undefined
        setting.gifBackground = undefined
        setting.backgroundImage = image
        this.code.src = await this.getAwesomeQRData(setting)
        this.code.mode = "background"
      } finally {
        this.hideLoading()
        this.detectChanges()
      }
    }
    image.src = await this.chatbotApi.deeplink.getImageBufferLink({ src })
    this.detectChanges()
  }

  async createGifCode(src: string, option: any) {
    const r = new FileReader();
    r.onload = async (e: any) => {
      // get the ArrayBuffer

      try {
        const setting = merge({}, option)
        setting.text = this.referral.link
        setting.logoImage = undefined
        setting.backgroundImage = undefined
        setting.gifBackground = e.target.result
        // const jimp = await Jimp.read(await this.getAwesomeQRData(setting))
        this.code.src = await this.getAwesomeQRData(setting)
        this.code.mode = "gif"
      } finally {
        this.hideLoading()
        this.detectChanges()
      }
    };
    // read as ArrayBuffer
    r.readAsArrayBuffer(await this.getBlobFromURL(await this.chatbotApi.deeplink.getImageBufferLink({ src })));
    this.detectChanges()
  }

  async onCreateMSCodeWithLogo() {
    if (!this.code.option) this.code.option = {}
    if (!this.code.option.logoUrl) this.code.option.logoUrl = await this.getURLDialog()
    if (this.code.option.logoUrl) {
      try {
        this.showLoading()
        const src = await this.chatbotApi.deeplink.getImageBufferLink({ src: this.code.option.logoUrl })
        await this.createMSCodeWithLogo(src)
      } catch (err) {
        this.alert.handleError(err, "Lỗi khi tạo QR Code");
        // this.alert.error('Không thể tạo QR Code', err.message)
      } finally {
        this.hideLoading()
        this.detectChanges()
      }
    } else {
      this.code.src = ''
    }
  }

  async onCreateMSCode() {
    this.code.mode = 'mscode'
    this.code.src = this.referral.messengerCode
    this.detectChanges()
  }

  async onCreateSimpleCode() {
    const setting = merge({}, this.code.option)
    setting.text = this.referral.link
    this.code.src = await this.getAwesomeQRData(setting)
    this.code.mode = "simple"
    this.detectChanges()
  }

  async onCreateLogoCode() {
    if (!this.code.option) this.code.option = {}
    if (!this.code.imageSrc) this.code.imageSrc = await this.getURLDialog()
    if (this.code.imageSrc) { await this.createLogoCode(this.code.imageSrc, this.code.option) }
    else {
      this.code.src = ''
    }
    this.detectChanges()
  }

  async onCreateBackgroundCode() {
    if (!this.code.option) this.code.option = {}
    if (!this.code.imageSrc) this.code.imageSrc = await this.getURLDialog()
    if (this.code.imageSrc) { await this.createBackgroundCode(this.code.imageSrc, this.code.option) }
    else {
      this.code.src = ''
    }
    this.detectChanges()
  }

  async onCreateGifCode(reloadTime: number = 0) {
    if (reloadTime === 3) {
      this.hideLoading()
      return;
    }
    if (!this.code.option) this.code.option = {}
    if (!this.code.imageSrc) this.code.imageSrc = await this.getURLDialog()
    if (this.code.imageSrc) await this.createGifCode(this.code.imageSrc, this.code.option)
    else {
      this.code.src = ''
    }
    this.detectChanges()

  }

  async getURLDialog() {
    let data: iEditInfoDialogData = {
      title: "Nhập link ảnh",
      confirmButtonText: "Xác nhận",
      inputs: [{
        title: "URL",
        property: "url",
        current: "",
        type: "text",
        required: true,
      }]
    };

    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    });

    return (await dialogRef.afterClosed().toPromise()).url
  }

  async onSetTime(mode: string, event: ITime) {

  }

  async createRef() {
    if (!this.validRef()) return

    try {
      this.showLoading()
      this.detectChanges()
      this.createdRef = await this.chatbotApi.referral.add(this.referral)
      this.close()

    } catch (err) {
      this.alert.handleError(err, "Lôi khi tạo mới");
      // this.alert.error('Không thể tạo mới', err.message)
    } finally {
      this.hideLoading()
      this.detectChanges()
    }
  }

  async saveContent() {
    if (!this.validRef()) return;
    try {
      this.showSaving()
      this.cardFrm.form.disable()
      this.detectChanges()
      await this.chatbotApi.referral.update(this.refId, this.referral)
      await this.updateRefState()
      this.cardFrm.form.enable()
      this.showSaveSuccess()
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lưu");
      // this.alert.error('Không thể cập nhật', err.message)
      this.cardFrm.form.enable()
      this.showSaveButton()
    } finally {
      this.detectChanges()
    }
  }

  validRef() {
    const startDate = moment(this.refRange.startDate).locale('en')
    startDate.hours(this.refRange.startTime.hour)
    startDate.minutes(this.refRange.startTime.minute)
    const endDate = moment(this.refRange.endDate).locale('en')
    endDate.hours(this.refRange.endTime.hour)
    endDate.minutes(this.refRange.endTime.minute)
    if (startDate > endDate) {
      this.error.date = true
      this.alert.info('Dữ liệu sai', 'Ngày bắt đầu phải nhỏ hơn hoặc bằng ngày kết thúc')
      return false
    }
    else this.error.date = false
    if (this.referral.amount < 0) {
      this.error.amount = true
      this.alert.info('Dữ liệu sai', 'Số lượng phải lớn hơn hoặc bằng 0')
      return false
    }
    else this.error.amount = false
    if (this.referral.limit < 0) {
      this.error.limit = true
      this.alert.info('Dữ liệu sai', 'Giới hạn phải lớn hơn hoặc bằng 0')
      return false
    }
    else this.error.limit = false
    this.referral.startDate = startDate.toDate()
    this.referral.endDate = endDate.toDate()
    const validChecks = ["isOptionalValid"]
    for (let valid of validChecks) {
      if (this.referral[valid] != undefined && !this.referral[valid]) {
        this.alert.info('Không hợp lệ', 'Các trường nhập vào có thể bị thiếu hoặc không hợp lệ')
        return false
      }
    }
    return true
  }

  async close() {
    if (this.mode === "add") {
      this.onRefCreated.emit(this.createdRef)
    }
    super.close()
  }

  openSnackBar(message: string, action?: string) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  async applyQRCode() {
    this.creatingCode = true
    this.codeFrm.form.disable()
    try {
      switch (this.code.mode) {
        case 'mscodewithlogo':
          await this.createMSCodeWithLogo(this.code.imageSrc)
          break
        case 'simple':
          await this.onCreateSimpleCode()
          break
        case 'logo':
          await this.createLogoCode(this.code.imageSrc, this.code.option)
          break
        case 'background':
          await this.createBackgroundCode(this.code.imageSrc, this.code.option)
          break
        case 'gif':
          await this.createGifCode(this.code.imageSrc, this.code.option)
          break
      }
    } finally {
      this.code[this.code.mode] = this.code.src
      this.codeImg = this.code.src
      this.creatingCode = false
      this.codeFrm.form.enable()
      this.detectChanges()
    }

  }

  async openSubscriber() {
    this.portalService.pushPortal("SubscribersPortalComponent")
  }

  async openStatistics() {
    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "StatisticsDetailsResultPortalComponent", {
      mode: 'referral',
      referralId: this.referral._id,
      portalName: 'Thống kê ',
      name: this.referral.name,
      hasFooter: false
    })
  }

  async onSelectionChanged(event) {
    this.code.mode = this.selectedType
    this.codeImg = this.code[this.selectedType]
    this.detectChanges()
  }

  async createQRCode() {
    this.applyQRCode()
  }

  async onChangeImageFile(event) {
    let files = this.fileUploader.nativeElement.files

    if (files.length == 0) return
    let file = files[0]
    if (file) {
      try {
        this.isUploading = true;
        this.detectChanges()
        let response = await this.chatbotApi.upload.uploadImage(file) as any
        this.code.imageSrc = response.link
      } catch (err) {
        // console.error(err.error.message);
        this.alert.handleError(err, "Lỗi khi tải ảnh lên");
        // this.alert.error("Lỗi khi tải ảnh lên", err.message)
      } finally {
        setTimeout(() => {
          this.isUploading = false;
          this.detectChanges()
        }, 300)
      }
    }
  }

  async openImage() {
    if (!this.code.src) return;

    let data = {
      image: this.code.src
    };

    let dialogRef = this.dialog.open(ImagePopupDialog, {
      maxHeight: '60vh',
      panelClass: 'image-popup',
      data: data
    });
  }

  async enableWifiMK(enable: boolean) {
    this.showLoading();
    try {
      this.detectChanges()
      await this.chatbotApi.referral.toggleWifiMK(this.referral._id, enable)
      await this.reload(false)
      if (enable && (!this.referral.wifiMKPhotos || this.referral.wifiMKPhotos.length == 0)) {
        this.referral.wifiMKPhotos = [{
          desktop: this.chatbotApi.cardBuilder.defaultImage,
          mobile: this.chatbotApi.cardBuilder.defaultImage
        }]
        this.showSaveButton()
      }
    } catch (err) {
      this.alert.handleError(err);
      // this.alert.error("Không thể bật wifi marketing", err.message)
      // console.error(err.error.message);
    } finally {
      this.hideLoading()
      this.detectChanges()
    }
  }

  async onImageSourceChanged(type: string, imageUrl: string, index: number) {
    if (type == 'mobile') {
      this.referral.wifiMKPhotos[index].mobile = imageUrl
    } else {
      this.referral.wifiMKPhotos[index].desktop = imageUrl
    }
    this.referral.wifiMKPhotos[index].mobile = this.referral.wifiMKPhotos[index].mobile || imageUrl
    this.referral.wifiMKPhotos[index].desktop = this.referral.wifiMKPhotos[index].desktop || imageUrl
    this.showSaveButton()
    this.detectChanges()
  }

  async addPhoto() {
    if (!this.referral.wifiMKPhotos) this.referral.wifiMKPhotos = []
    this.referral.wifiMKPhotos.push({
      desktop: this.chatbotApi.cardBuilder.defaultImage,
      mobile: this.chatbotApi.cardBuilder.defaultImage
    })
    this.showSaveButton()
    this.detectChanges()
  }

  async removePhoto() {
    if (this.referral.wifiMKPhotos.length == 1) {
      this.alert.info("Không thể xoá hình", "Yêu cầu ít nhất phải có 1 hình")
      return
    }
    const index = this.swiperComp.swiper.activeIndex;
    this.referral.wifiMKPhotos.splice(index, 1)
    this.showSaveButton()
    this.detectChanges()
  }
}
