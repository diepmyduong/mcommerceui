import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoryReferralPortalComponent } from 'app/chatgut-app/portals/story-referral-portal/story-referral-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { OptionalComponent } from 'app/chatgut-app/portals/story-referral-portal/optional/optional.component';
import { MatFormFieldModule, MatInputModule, MatToolbarModule, MatButtonModule, MatProgressSpinnerModule, MatIconModule, MatProgressBarModule, MatTooltipModule, MatDatepickerModule, MatNativeDateModule, MatDividerModule, MatSelectModule, MatCardModule, MatSnackBarModule, MatCheckboxModule } from '@angular/material';
import { MaterialTimeControlModule } from 'app/shared';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { FormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';
import { SwiperModule } from 'angular2-useful-swiper';
import { CardImageModule } from 'app/shared/cards/card-image/card-image.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatProgressBarModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MaterialTimeControlModule,
    MatDividerModule,
    MatSelectModule,
    MatCardModule,
    MatSnackBarModule,
    EditInfoModule,
    FormsModule,
    ClipboardModule,
    SwiperModule,
    CardImageModule,
    FlexLayoutModule,
    MatCheckboxModule
  ],
  declarations: [StoryReferralPortalComponent, OptionalComponent],
  entryComponents: [StoryReferralPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: StoryReferralPortalComponent }
  ]
})
export class StoryReferralPortalModule { }
