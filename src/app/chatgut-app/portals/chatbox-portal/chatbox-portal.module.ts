import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatboxPortalComponent } from 'app/chatgut-app/portals/chatbox-portal/chatbox-portal.component';
import { ChatboxMessageComponent } from 'app/chatgut-app/portals/chatbox-portal/chatbox-message/chatbox-message.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';

// Material
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { MatDividerModule } from '@angular/material/divider';
import { SwiperModule } from 'angular2-useful-swiper';
import { MatTooltipModule } from '@angular/material/tooltip';
import { PickerModule } from 'app/components/ngx-emoji-mart/picker/public_api';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { SendModule } from 'app/modals/send/send.module';
@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatProgressBarModule,
    MatDividerModule,
    FormsModule,
    MatTooltipModule,
    SwiperModule,
    PickerModule,
    EditInfoModule,
    SendModule,
  ],
  declarations: [ChatboxPortalComponent, ChatboxMessageComponent],
  entryComponents: [ChatboxPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ChatboxPortalComponent }
  ]
})
export class ChatboxPortalModule { }
