import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseComponent } from 'app/base.component';
import * as guid from 'guid'
import { iThreadMessage } from 'app/services/chatbot/api/crud/subscriber';
import { iPage } from 'app/services/chatbot/api/crud/page';
@Component({
  selector: 'app-chatbox-message',
  templateUrl: './chatbox-message.component.html',
  styleUrls: ['./chatbox-message.component.scss']
})
export class ChatboxMessageComponent extends BaseComponent implements OnInit {

  @Input() message: iThreadMessage
  @Input() sender: any
  @Input() type: any
  constructor(
    public changeRef: ChangeDetectorRef
  ) {
    super("ChatboxMessageComponent")
  }
  page: iPage
  id: string = guid.raw()
  MSG_TYPES = {
    TEXT: "text-message",
    IMAGE: "image-message",
    AUDIO: "audio-message",
    VIDEO: "video-message",
    GENERIC: "generic-message",
    LIST: "list-message",
    UNKNOW: "unknow-message"
  }
  messageType: string = this.MSG_TYPES.UNKNOW
  messageImage: string
  messageClass: string
  messageAudio: string
  messageVideo: string
  messageGeneric: any
  messageList: any
  swiperOptions = {
    navigation: {
      nextEl: `.element-button-next-${this.id}`,
      prevEl: `.element-button-prev-${this.id}`,
    }
  }

  tooltipPos: string
  messageAvatar: string
  async ngOnInit() {
    this.messageClass = this.message.from.id === this.sender.id ? 'myMessage' : 'fromThem'
    if (this.messageClass == 'myMessage') {
      this.tooltipPos = 'left'
    }
    else {
      this.tooltipPos = 'right'
    }
    this.reload()
  }
  

  async reload(local: boolean = true) {
    if (this.message.attachments) {
      if (/image/g.test(this.message.attachments.data[0].mime_type)) {
        this.messageType = this.MSG_TYPES.IMAGE
        this.messageImage = this.message.attachments.data[0].image_data.preview_url
      } else if (/audio/g.test(this.message.attachments.data[0].mime_type)) {
        this.messageType = this.MSG_TYPES.AUDIO
        this.messageAudio = this.message.attachments.data[0].file_url
      } else if (/video/g.test(this.message.attachments.data[0].mime_type)) {
        this.messageType = this.MSG_TYPES.VIDEO
        this.messageVideo = this.message.attachments.data[0].video_data.url
      } else {
        this.messageType = this.MSG_TYPES.UNKNOW
      }
    } else if (typeof this.message.message === 'string') {
      this.messageType = this.MSG_TYPES.TEXT
    } else if (this.message.message.attachment) {
      const { payload } = this.message.message.attachment
      if (payload) {
        switch(payload.template_type) {
          case 'generic':
            this.messageGeneric = payload
            this.messageType = this.MSG_TYPES.GENERIC
            break
          case 'list':
            this.messageType = this.MSG_TYPES.LIST
            this.messageList = payload
        }
      }
    } else {
      this.messageType = this.MSG_TYPES.UNKNOW
    }
    this.messageClass += ` ${this.messageType}`
  }

}
