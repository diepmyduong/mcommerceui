
import {debounceTime} from 'rxjs/operators';
declare const $: any;
import * as guid from 'guid'
import * as moment from 'moment'

import { Component, OnInit, Input, ViewChild, Output, EventEmitter, ElementRef, HostListener, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { MatDialog } from '@angular/material'
import { ENTER } from '@angular/cdk/keycodes';
import { BehaviorSubject } from 'rxjs';
import { iEditInfoDialogData, EditInfoDialog } from 'app/modals/edit-info/edit-info.component';
import { iSubscriber, iThreadInfo, iThreadMessage } from 'app/services/chatbot/api/crud/subscriber';
import { iSendData } from 'app/services/chatbot/api/send';
import { iPage } from 'app/services/chatbot/api/crud/page';
import { SendDialog } from 'app/modals/send/send.component';
import { get } from 'lodash-es'

@Component({
  selector: 'app-chatbox-portal',
  templateUrl: './chatbox-portal.component.html',
  styleUrls: ['./chatbox-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatboxPortalComponent extends BasePortal implements OnInit {

  componentName = "ChatboxPortalComponent"
  @Input() subscriberId: string
  @Input() uid: string
  @Output() onPortalClosed: EventEmitter<any> = new EventEmitter()
  @ViewChild('messagesContainer') private messagesContainer: ElementRef;
  @ViewChild('messageInputRef') private messageInputRef: ElementRef;
  constructor(
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef
  ) {
    super()
  }
  showSendingSpinner: boolean = false

  messageInput: string = ""
  subscriber: iSubscriber
  threadInfo: iThreadInfo
  cachedThreadData: any
  threadMessages: iThreadMessage[] = []
  sentMessages: iThreadMessage[] = []
  tempMessages: BehaviorSubject<iThreadMessage[]> = new BehaviorSubject<iThreadMessage[]>([])
  messagesPaging: any = {}
  lastScrollPosition: number = 0
  messagesContainerEl: any
  scrolledToTop = new EventEmitter<number>()
  newMessageCursor: string
  onSubscriberMessage = new EventEmitter<any>()
  displayEmoji = undefined;
  lockSendMessage = false;

  avatar = {
    subscriber: "",
    app: ""
  }
  @HostListener('document:click', ['$event'])
  public documentClick(event: any): void {
    if (event.srcElement.classList.contains('fa-smile') || event.srcElement.classList.contains('emoji-button')) {
      this.displayEmoji = true;
    } else {
      this.displayEmoji = false;
    }
    this.detectChanges()
  }

  async ngOnInit() {
    if(!this.uid){
      await this.alert.info("Không thể xem tin nhắn","Hãy tương tác với người dùng trước để xem tin nhắn với người dùng")
        this.close()
      
      return
    }
    this.avatar.app = (this.chatbotApi.chatbotAuth.app.activePage as any).meta.picture.data.url;
    this.reload()
    // this.subscriptions.onMessageFromUser = this.chatbotApi.socket.onMessageFromUser.subscribe(this.handleNewMessage.bind(this))
    // this.subscriptions.onMessageFromPage = this.chatbotApi.socket.onMessageFromPage.subscribe(this.handleNewMessage.bind(this))
    this.subscriptions.onThreadChange = this.chatbotApi.socket.onThreadChange.subscribe(this.handleNewMessage.bind(this))
    this.subscriptions.onSubscriberMessage = this.onSubscriberMessage.pipe(debounceTime(1500)).subscribe(payload => {
      this.loadNewMessages()
    })

    this.subscriptions.onTempMessagesChanges = this.tempMessages.subscribe(tempMessages => {
      if (tempMessages.length == 0 || this.lockSendMessage) return;

      //CHECK WAITING IF YES THEN LEAVE
      tempMessages.forEach(messages => {
        if (messages.waiting) {
          return;
        }
      })

      this.sendTempMessages();
    })
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
    this.onPortalClosed.emit();
    this.chatbotApi.subscriber.threadMessages[this.subscriberId] = this.threadMessages;
  }

  ngAfterViewInit() {
    this.messagesContainerEl = $(this.messagesContainer.nativeElement)
    this.messagesContainerEl.scroll(() => {
      const offset = this.messagesContainerEl.scrollTop()
      if (offset === 0 && !this.progressBar.show) this.scrolledToTop.next(offset)
    })
    this.subscriptions.onScrolledToTop = this.scrolledToTop.subscribe(offset => {
      this.loadMore()
    })
  }

  async reload(local: boolean = true) {
    this.showLoading()
    this.changeRef.detectChanges()
    try {
      const [subscriber, threadInfo] = await Promise.all([
        this.chatbotApi.subscriber.getItem(this.subscriberId, { local }),
        this.chatbotApi.subscriber.getThreadInfo(this.subscriberId, { local })
      ])
      const threadMessagesData = await this.chatbotApi.subscriber.getThreadMessages(this.subscriberId, { local, query: { limit: 20 } });
      this.subscriber = subscriber
      this.threadInfo = threadInfo
      this.avatar.subscriber = subscriber.messengerProfile.profile_pic
      this.cachedThreadData = threadMessagesData
      this.threadMessages = threadMessagesData.items.slice().reverse()
      for (let i = 0; i < this.threadMessages.length; i++) {
        this.checkFirstLast(i);
      }
      this.messagesPaging = (threadMessagesData as any).paging
      this.newMessageCursor = get(this.messagesPaging, 'cursors.before');
      this.loadNewMessages()
      this.subName = `Chat`
      setTimeout(() => {
        this.scrollToBottom()
        this.messagesContainer.nativeElement.classList.remove("initing");
        const currentHeight = this.messagesContainer.nativeElement.scrollHeight
        setTimeout(() => {
          if (this.messagesContainer.nativeElement.scrollHeight > currentHeight) {
            this.scrollToBottom()
          }
        }, 800)
      }, 200)
    }catch (err){
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi lấy giữ liệu",err.message)
    } finally {
      this.hideLoading()
      this.changeRef.detectChanges()
    }
  }

  async checkFirstLast(i) {
    let message = this.threadMessages[i];
    if (i == 0 || this.threadMessages[i - 1].from.id != message.from.id) {
      message.isFirst = true;
    } else {
      message.isFirst = false;
    }
    if (i == this.threadMessages.length - 1 || this.threadMessages[i + 1].from.id != message.from.id) {
      message.isLast = true;
      if (message.from.id == this.threadInfo.senders.data[1].id) {
        message.avatar = this.avatar.app;
      } else {
        message.avatar = this.avatar.subscriber;
      }
    } else {
      message.isLast = false;
    }
    this.changeRef.detectChanges()
  }

  async loadMore(local: boolean = true) {
    this.showLoading()
    this.changeRef.detectChanges()
    try {
      if(!get(this.messagesPaging,'cursors.after')) return;
      const threadMessagesData = await this.chatbotApi.subscriber.getThreadMessages(this.subscriberId, {
        local,
        query: {
          after: get(this.messagesPaging,'cursors.after'),
          limit: 50
        }
      }) as any
      const offset = this.messagesContainer.nativeElement.scrollHeight - this.messagesContainer.nativeElement.scrollTop
      const concatIndexBefore = threadMessagesData.items.length - 1;
      this.threadMessages = threadMessagesData.items.slice().reverse().concat(this.threadMessages)
      for (let i = 0; i < concatIndexBefore + 2; i++) {
        this.checkFirstLast(i);
      }
      this.messagesPaging = threadMessagesData.paging
      setTimeout(() => {
        this.scrollToBottom(offset)
      }, 500)
    } catch (err){
      this.alert.handleError(err, "Lỗi khi tải thêm dữ liệu");
      // this.alert.error("Lỗi khi tải thên nội dung", err.message)
    } finally {
      this.hideLoading()
      this.changeRef.detectChanges()
    }
  }

  handleNewMessage(payload: any) {
    const { page_id, thread_id, thread_key } = payload
    if(this.threadInfo.id === thread_id) {
      this.onSubscriberMessage.next(payload)
    }
    this.changeRef.detectChanges()
  }

  async loadNewMessages() {
    this.changeRef.detectChanges()
    const threadMessagesData = await this.chatbotApi.subscriber.getThreadMessages(this.subscriberId, {
      local: false,
      query: {
        before: this.newMessageCursor
      }
    }) as any
    const offset = this.messagesContainer.nativeElement.scrollHeight - this.messagesContainer.nativeElement.scrollTop
    if (threadMessagesData.items.length > 0) {
      const newMessages = threadMessagesData.items.reverse() as iThreadMessage[]
      let newMessagesLength = 0;
      newMessages.forEach(message => {
        if (message.from.id !== this.subscriberId) {
          newMessagesLength++;
        }
      })
      const concatIndexBefore = this.threadMessages.length - 1;
      this.threadMessages = this.threadMessages.concat(newMessages)
      for (let i = concatIndexBefore; i < this.threadMessages.length; i++) {
        this.checkFirstLast(i);
      }
      this.cachedThreadData.items = newMessages.concat(this.cachedThreadData.items)
      this.cachedThreadData.paging.cursors.before = threadMessagesData.paging.cursors.before
      this.sentMessages.splice(0, newMessagesLength)
      //this.tempMessages = []
      this.newMessageCursor = threadMessagesData.paging.cursors.before
    }
    if (offset < 1000) {
      setTimeout(() => {
        this.scrollToBottom()
        const currentHeight = this.messagesContainer.nativeElement.scrollHeight
        setTimeout(() => {
          if (this.messagesContainer.nativeElement.scrollHeight > currentHeight) {
            this.scrollToBottom()
          }
        }, 800)
      }, 200)
    }
    this.detectChanges()

  }

  scrollToBottom(offset: number = 0) {
    try {
      this.messagesContainer.nativeElement.scrollTop = this.messagesContainer.nativeElement.scrollHeight - offset
    } catch (err) {

    }
  }

  async pushToWaitingStack() {
    const tempMessage: iThreadMessage = {
      id: guid.raw(),
      message: this.messageInput,
      from: this.threadInfo.senders.data[1],
      created_time: moment().format(),
      avatar: this.avatar.app,
      isFirst: true,
      isLast: true,
      waiting: false
    }
    this.messageInput = ""
    const tempMessagesNew = this.tempMessages.getValue()
    tempMessagesNew.push(tempMessage)
    this.tempMessages.next(tempMessagesNew)
    this.detectChanges()
    setTimeout(() => {
      this.scrollToBottom()
    }, 200)
  }

  async sendTempMessages() {
    try{
      this.changeRef.detectChanges()
      let tempMessages = this.tempMessages.getValue()
      this.lockSendMessage = true
  
      // RETURN IF NO TEMP LEFT
  
      let stories = []
      tempMessages.forEach(messages => {
        stories.push({
          type: 'text',
          option: {
            text: messages.message
          }
        })
        messages.waiting = true;
      })
  
      const sendData: iSendData = {
        type: 'new_story',
        story: stories,
        sendBy: 'subscriber',
        sendTo: [this.subscriberId],
        nonTask: true
      }
      this.tempMessages.next(tempMessages)
  
      await this.chatbotApi.send.sendMessage(sendData)
      let newSentMessages = [];
      tempMessages = this.tempMessages.getValue()
      for (let i = 0; i < tempMessages.length; i++) {
        let message = tempMessages[i];
        if (message.waiting) {
          message.waiting = false
          newSentMessages.push(message)
          tempMessages.splice(i, 1)
          i--;
        }
      }
  
      this.lockSendMessage = false;
      this.tempMessages.next(tempMessages)
      this.sentMessages = this.sentMessages.concat(newSentMessages)
    } catch (err){
      this.alert.handleError(err, "Lỗi khi gửi template");
      // this.alert.error("Lỗi", err.message)
    } finally {
      this.detectChanges()
    }
    
  }

  onKeyDownTextMessage(event: KeyboardEvent) {
    if (event.shiftKey == false && event.keyCode === ENTER) {
      event.preventDefault();
      if (this.messageInput.trim().length > 0) {
        //this.sendTextMessage()
        this.pushToWaitingStack()
      }
    }
    this.detectChanges()
  }

  async emojiClicked(event) {
    this.messageInput += event.emoji.native;
    this.displayEmoji = false;
    this.detectChanges()
  }

  async openProfile(){
    window.open(
      'https://www.facebook.com/' + this.subscriber.uid,
      '_blank'
    );
  }

  async openSubscriber() {
    this.portalService.pushPortalAt(this.index + 1, "SubscriberDetailPortalComponent", {
      subscriberId: this.subscriber._id
    })
  }

  async addLabel() {
    let data: iEditInfoDialogData = {
      title: "Thêm nhãn cho khách hàng",
      confirmButtonText: "Xác nhận",
      inputs: [{
        title: "Nhãn",
        property: "label",
        current: "",
        type: "text",
        required: true,
      }]
    };
    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    });

    const pageId = (this.chatbotApi.chatbotAuth.app.activePage as iPage)._id;

    dialogRef.afterClosed().toPromise().then(async result => {
      if (result && result.label) {
        try {
          await this.chatbotApi.label.addLabel(pageId, [this.subscriberId], result.label)
          this.alert.success("Thành công", "Thêm nhãn thành công")
        } catch (err) {
          this.alert.handleError(err, "Lỗi khi thêm nhãn");
          // this.alert.error("Có lỗi xảy ra khi thêm nhãn",err.message)
        } finally {
          this.detectChanges()
        }
      }
    })
  }

  async sendStory() {
    // let sendData: iSendData = {
    //   type: "story",
    //   story: "",
    //   sendBy: "subscriber",
    //   sendTo: [this.subscriberId]
    // }

    // sendData.type = "story"
    // sendData.story = await this.getStory() as string
    // if (!sendData.story) return;

    // try {
    //   let result = await this.chatbotApi.send.sendMessage(sendData)
    // } catch (err) {
    //   this.alert.error("Không thể gửi", "Vui lòng thử lại sau")
    // }

    this.dialog.open(SendDialog, {
      width: '700px',
      data: {
        targetId: [this.subscriber],
        targetName: this.subscriber.messengerProfile.name?this.subscriber.name:(this.subscriber.messengerProfile.first_name + ' ' + this.subscriber.messengerProfile.last_name),
        targetMode: 'subscriber'
      }
    })

  }

  getStory() {
    return new Promise(async (resolve, reject) => {
      const componentRef = await this.portalService.pushPortalAt(this.index + 1, "StoriesPortalComponent", {
        select: true, selectedStory: "", multi: false
      })

      if (!componentRef)
        return;

      const component = componentRef.instance as any
      this.subscriptions.onStorySaved = component.onStorySaved.subscribe(storyId => {
          resolve(storyId);
          component.hideSave();
          component.close();
      })
      this.subscriptions.onStoryClosed = component.onStoryClosed.subscribe(storyId => {
        component.onStorySaved.unsubscribe();
        component.onStoryClosed.unsubscribe();
      })
    })

  }

}
