import { Component, OnInit, Input, Output, EventEmitter, ViewChild, Host, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseComponent } from 'app/base.component';

@Component({
  selector: 'statistic-option',
  templateUrl: './statistic-option.component.html',
  styleUrls: ['./statistic-option.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatisticOptionComponent extends BaseComponent implements OnInit {

  @Input() mode:string
  @Input() endDate:any
  @Input() startDate:any
  @Input() moreOptions:any
  @Output() onTypeDateSelect = new EventEmitter<any>()
  @Output() onStartDateChange = new EventEmitter<any>()
  @Output() onEndDateChange = new EventEmitter<any>()
  @Output() onMoreOptionsChange = new EventEmitter<any>()
  @Input() dateSelect:string 
  dateOptions: any[]=[
    { code:"7_day", display:"7 ngày trước",},
    { code:"30_day", display:"30 ngày trước",},
    { code:"current_month", display:"Tháng này",},
    { code:"previous_month", display:"Tháng trước",},
    { code:"custom", display:"Tùy chỉnh",},
  ]
  constructor(
    public changeRef: ChangeDetectorRef
  ) {
    super('StatisticOptionComponent')
  }
  canEmmitChange: boolean = false;
  
  async ngOnInit() {
    if(!this.dateSelect) this.dateSelect = "7_day"
    this.changeRef.detectChanges()
  }

  ngAfterViewInit() {
    
  }

  onDateOptionSelect(code){
    this.dateSelect = code; 
    this.onTypeDateSelect.emit(code)
    this.changeRef.detectChanges()
  }
  onOptionsChange(value,code){
    this.onMoreOptionsChange.emit({
      value,
      code
    })
  }
}