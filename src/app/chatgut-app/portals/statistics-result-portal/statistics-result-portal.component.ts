import * as moment from 'moment'
import { Component, OnInit, QueryList, ViewChildren, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Subscription, BehaviorSubject } from 'rxjs';
import * as d3 from "d3-shape";
import { DynamicComponentLoaderService } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { ITime } from 'app/shared/material-time/time-control';
import { iTask } from 'app/services/chatbot';
@Component({
  selector: 'app-statistics-result-portal',
  templateUrl: './statistics-result-portal.component.html',
  styleUrls: ['./statistics-result-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatisticsResultPortalComponent extends BasePortal implements OnInit {
  componentName = "StatisticsResultPortalComponent"
  @Input() data: any
  @Input() mode: any
  @ViewChildren(MatPaginator) paginationList: QueryList<MatPaginator>;
  @ViewChildren(MatSort) sortList: QueryList<MatSort>;
  constructor(
    private dynamicLoader: DynamicComponentLoaderService,
    public changeRef: ChangeDetectorRef
  ) {
    super()
    moment.locale('en')
  }
  curvebasis = d3.curveMonotoneX

  fromDate: moment.Moment
  toDate: moment.Moment

  startTime: ITime
  endTime: ITime
  
  filter: any
  filterString: string
  result: any = null
  dataSource: MatTableDataSource<any> = new MatTableDataSource()
  displayedColumns = []
  loadDone: boolean = false
  loadChartDone: any = {
    subscribeByDay:true,
    genderInteraction: true,
    subscriber_interact:true,
    story:true,
    storyInteraction:true,
    unique_subscriber_receive_story:true,
  }
  notFound: boolean = false
  isEmpty: boolean
  noTable: boolean = false
 
  moreOptions: any = {
    // genderInteraction:{
    //   type:"radio",
    //   value:'subscribe',
    //   options:[
    //     {code:'subscribe',display:'Đăng ký',value:true},
    //     {code:'block',display:'Đã chăn',value:false},
    //   ]
    // },
    // interaction:{
    //   type:"checkbox",
    //   options:[
    //     {code:'activity',display:'Hoạt động',value:true},
    //     {code:'message',display:'Tin nhắn',value:true},
    //   ]
    // }
  }
  
  interactionSubscriberData = []
  interactionSubscriberScheme = { domain: ["#80DEEA", "#EF5350"] }
  interactionMessageData = []
  interactionMessageScheme = { domain: ["#81C784", "#FF8A65"] }
  interactionNumberData = []
  interactionNumberScheme = { domain: ["#FFE082", "#80CBC4", "#CE93D8"] }

  subscriberAnalyticData = []
  subscriberAnalyticScheme = { domain: ["#536DFE", "#99f47a", "#FFD740", "#FF5252"] }
  subscriberGenderData = []
  subscriberGenderScheme = { domain: ["#90CAF9", "#FF5252", "#B2DFDB"] }
  subscriberSubscribingData = []
  subscriberSubscribingScheme = { domain: ["#4DD0E1", "#B39DDB", "#CFD8DC"] }
  subscriberBlockedData = []
  subscriberBlockedScheme = { domain: ["#FF8A65", "#FF80AB", "#FFECB3"] }
  genderSubcriberInteraction = []

  subscribingLineData = []
  subscribingLineScheme = { domain: ["#81C784"] }

  uniqueSubcriberReceiveStoryData = []
  uniqueSubcriberReceiveStoryScheme = { domain: ["#81C784"] }

  subscriberInteractionScheme = { domain: ["#ff6482", "#64b5f6"] }

  subscribeByDayData = []
  subscribeByDayScheme = { domain: ["#2590ce", "#64b5f6"] }

  groupData = []
  groupScheme = { domain: ["#2590ce", "#64b5f6"] }

  genderInteraction = []
  genderInteractionScheme = { domain: ["#2590ce", "#64b5f6"] }

  blockLineData = []
  blockLineScheme = { domain: ["#CCFF90"] }

  pageInteractionData = []
  pageInteractionScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  storyNumberData = []
  storyNumberScheme = { domain: ["#81C784"] }

  overviewNumberData = []
  overviewNumberScheme = { domain: ["#59ba5d", "#8a5ae0", "#FF6E40", "#A5D6A7", "#B388FF", "#FF6E40"] }
  overviewPieData = []
  overviewPieScheme = { domain: ["#A5D6A7", "#FF6E40", "#D50000", "#4A148C",  "#42A5F5"] }
  overviewChartData = []
  overviewChartScheme = { domain: [] }

  topicNumberData = []
  topicNumberScheme = { domain: ["#A5D6A7", "#B388FF", "#FF6E40"] }

  postNumberData = []
  postNumberScheme = { domain: ["#CCFF90"] }

  referralNumberData = []
  referralNumberScheme = { domain: ["#EA80FC"] }

  buttonTagNumberData = []
  buttonTagNumberScheme = { domain: ["#EEFF41"] }

  pagePostNumberData = []
  pagePostNumberScheme = { domain: ["#CCFF90"] }

  reactionNumberData = []
  reactionNumberScheme = { domain: ["#D50000", "#4A148C", "#6200EA", "#304FFE", "#42A5F5"] }

  subscriberInteractionLineData = []
  subscriberInteractionLineScheme = { domain: ["#D50000", "#4A148C","#304FFE"] }

  interactionStoryData = []
  interactionStoryScheme = { domain: ["#64b5f6"] }

  pagination: MatPaginator
  sort: MatSort
  paginationListSubscription: Subscription
  sortListSubscription: Subscription

  async ngOnInit() {
    this.setupFilter()
    console.log( 'test',this.filter[this.mode])
    this.startSummary()
  }  

  async setupFilter() {
    this.fromDate = moment().subtract(7,'days')
    this.toDate = moment()
    this.startTime = {
      hour: moment().startOf('day').get('hour'),
      minute: moment().startOf('day').get('minute'),
      format: 24
    } as ITime
    this.endTime = {
      hour: moment().endOf('day').get('hour'),
      minute: moment().endOf('day').get('minute'),
      format: 24
    } as ITime
    this.filter = {
      overview: {
        start_date: this.fromDate.toDate(),
        end_date: this.toDate.toDate(),
      },
      interaction: {
        start_date: this.fromDate.toDate(),
        end_date: this.toDate.toDate(),
        start_time: this.startTime,
        end_time: this.endTime,
        activity: true,
        message: true
      },
      group: {
        start_date: this.fromDate.toDate(),
        end_date: this.toDate.toDate(),
        start_time: this.startTime,
        end_time: this.endTime
      },
      subscriber: {
        analytic: true,
        activity: true,
        subscribeByDay:{
          start_date: this.fromDate.toDate(),
          end_date: this.toDate.toDate(),
          start_time: this.startTime,
          end_time: this.endTime,
        },
        genderInteraction:{
          start_date: this.fromDate.toDate(),
          end_date: this.toDate.toDate(),
          start_time: this.startTime,
          end_time: this.endTime,
          subscribe: true,
          block: false
        },
        subscriber_interact:{
          start_date: this.fromDate.toDate(),
          end_date: this.toDate.toDate(),
          start_time: this.startTime,
          end_time: this.endTime,
        }
      
      },
      story: {
        story:{
          start_date: this.fromDate.toDate(),
          end_date: this.toDate.toDate(),
          start_time: this.startTime,
          end_time: this.endTime,
        },       
        storyInteraction: {
          start_date: this.fromDate.toDate(),
          end_date: this.toDate.toDate(),
          start_time: this.startTime,
          end_time: this.endTime,
        },
        unique_subscriber_receive_story: {
          start_date: this.fromDate.toDate(),
          end_date: this.toDate.toDate(),
          start_time: this.startTime,
          end_time: this.endTime,
        },
      },
      topic: {
        start_date: this.fromDate.toDate(),
        end_date: this.toDate.toDate(),
        start_time: this.startTime,
        end_time: this.endTime,
      },
      post: {
        start_date: this.fromDate.toDate(),
        end_date: this.toDate.toDate(),
        start_time: this.startTime,
        end_time: this.endTime,
        subscriber: true
      },
      reactPost: {
      },
      referral: {
        start_date: this.fromDate.toDate(),
        end_date: this.toDate.toDate(),
        start_time: this.startTime,
        end_time: this.endTime,
        subscriber: true
      },
      button: {
        start_date: this.fromDate.toDate(),
        end_date: this.toDate.toDate(),
        start_time: this.startTime,
        end_time: this.endTime,
        subscriber: true
      },
      storySent: {
        start_date: this.fromDate.toDate(),
        end_date: this.toDate.toDate(),
        start_time: this.startTime,
        end_time: this.endTime,
      },
    }
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
    if (this.paginationListSubscription) this.paginationListSubscription.unsubscribe();
    if (this.sortListSubscription) this.sortListSubscription.unsubscribe();
    clearInterval(this.syncPostInterval)
  }

  async ngAfterViewInit() {
    this.paginationListSubscription = this.paginationList.changes.subscribe((comps: QueryList<MatPaginator>) => {
      this.pagination = comps.first;
      this.dataSource.paginator = this.pagination
    });

    this.sortListSubscription = this.sortList.changes.subscribe((comps: QueryList<MatSort>) => {
      this.sort = comps.first;
    });

    this.dynamicLoader.getComponentFactory('StatisticsDetailsResultPortalComponent')
    this.detectChanges()
  }

  async getStatisticResult(mode, display, filter) {
    this.setPortalName(display)
    this.mode = mode;
    this.filter = filter;

    this.startSummary();
  }

  async startSummary(refresh = false) {
    let fromDateTime, toDateTime;
    let activityCheck, messageCheck, genderCheck, analyticCheck, subscriberCheck;
    let params, response;

    this.loadDone = false;
    this.notFound = false;
    this.filterString = "";
    this.noTable = false;
    this.detectChanges()

    switch (this.mode) {
      case 'overview': {
        clearInterval(this.syncPostInterval)
        let tasks = []
        tasks.push(this.chatbotApi.app.statisticPosts().then(res => { this.result = res }))
        tasks.push(this.chatbotApi.task.getList({
          local: false, query: {
            limit: 1,
            order: {
              updatedAt: -1
            },
            filter: {
              app: this.chatbotApi.chatbotAuth.app._id,
              type: 'sync_posts',
            }
          }
        }).then(res => { 
          console.log(res[0])
          if (res && res[0] && res[0].processState == 'running') {
            this.syncPostTask = res[0]
            this.syncingPosts$.next(true)
            this.syncPostInterval = setInterval(async () => {
              await this.checkTask(this.syncPostTask._id)
            }, 2000)
          }
         }))
        await Promise.all(tasks)

        this.overviewNumberData = [
          { 
            name: "Bình luận / Bài viết bình luận", 
            value: this.result.comment || '[Không có]',
            value2: this.result.commentPost 
          },
          { 
            name: "Biểu cảm / Bài viết biểu cảm", 
            value: this.result.reaction || '[Không có]',
            value2: this.result.reactionPost 
          }, { 
            name: "Chia sẻ / Bài viết chia sẻ", 
            value: this.result.share || '[Không có]',
            value2: this.result.sharePost 
          }
        ]

        this.overviewPieData = [
          { name: "Hình ảnh", value: this.result.post_type.photos?this.result.post_type.photos:0 },
          { name: "Video", value: this.result.post_type.videos?this.result.post_type.videos:0 },
          { name: "Trạng thái", value: this.result.post_type.status?this.result.post_type.status:0 },
          { name: "Câu chuyện", value: this.result.post_type.events?this.result.post_type.events:0 },
          { name: "Chia sẻ câu chuyện", value: this.result.post_type.shared_story?this.result.post_type.shared_story:0 },
        ]
        await this.loadOverviewStatic()

        this.loadDone = true
        break
      }
      case 'group': {

        try {
          await this.loadGroupStatic()
        } catch (err) {
          // console.error(err)
          this.alert.handleError(err);  
          // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
        } finally {
          this.loadDone = true;
        }
        break;
      }
      case 'interaction':
        {
          fromDateTime = moment(this.filter.interaction.start_date)
          fromDateTime.hour(this.filter.interaction.start_time.hour)
          fromDateTime.minutes(this.filter.interaction.start_time.minute)

          toDateTime = moment(this.filter.interaction.end_date)
          toDateTime.hour(this.filter.interaction.end_time.hour)
          toDateTime.minutes(this.filter.interaction.end_time.minute)

          activityCheck = this.filter.interaction.activity
          messageCheck = this.filter.interaction.message

          params = {
            startTime: moment(fromDateTime.toDate()).utc().format(),
            endTime: moment(toDateTime.toDate()).utc().format(),
            isShowActivityDetail: activityCheck,
            isShowMessageDetail: messageCheck
          };

          if (analyticCheck == false) this.noTable = true

          try {
            response = await this.chatbotApi.statistic.summaryActivities(params, refresh)
            if (response) {
              this.result = response

              if (this.result.message) {
                let dataTable: any[] = [];
                this.result.message.forEach((item, index) => {
                  dataTable.push({
                    id: index + 1,
                    story: item.story,
                    name: item.name,
                    success: item.sentSuccess.total ? item.sentSuccess.total : 0,
                    fail: item.sentFailed.total ? item.sentFailed.total : 0,
                    read: item.storySentRead.total ? item.storySentRead.total : 0
                  })
                })
                this.dataSource.data = dataTable;
                this.dataSource.sort = this.sort;
              } else {
                this.noTable = true
              }
            } else {
              this.notFound = true
            }
          } catch (err) {
            // console.error(err)
            this.alert.handleError(err);
            // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
          } finally {
            this.displayedColumns = ['id', 'story', 'success', 'fail', 'read'];
            this.loadDone = true
          }

          this.interactionSubscriberData = [
            { name: "Đang đăng ký", value: this.result.totalOfSubscribing },
            { name: "Đã hủy", value: this.result.totalOfBlock }
          ]

          this.interactionMessageData = [
            { name: "Thành công", value: this.result.totalOfSuccessfulSent },
            { name: "Thất bại", value: this.result.totalOfFailedSent }
          ]

          this.interactionNumberData = [
            { name: "Lần gửi câu chuyện", value: this.result.totalDeliveried },
            { name: "Số tin nhắn đã đọc", value: this.result.totalOfRead },
            { name: "Lượt nút được bấm", value: this.result.totalOfClicked }
          ]
          break;
        }
      case 'subscriber':
        {

          activityCheck = this.filter.subscriber.activity
          analyticCheck = this.filter.subscriber.analytic

          params = {
            isShowActivityDetail: activityCheck,
            isShowAnalyticDetail: analyticCheck,
            isCheckGender: true
          };
          if (analyticCheck == false) this.noTable = true
          // get data for line chart

          try {
            this.loadSubscribeByDayStatic()
            this.loadGenderStatic()
            this.loadSubscriberInteract()

            response = await this.chatbotApi.statistic.summarySubscribers(params, refresh)
            if (response) {
              this.result = response

              if (this.result.subscriberAnalytics) {
                let dataTable: any[] = [];
                this.result.subscriberAnalytics.forEach((item, index) => {
                  dataTable.push({
                    id: index + 1,
                    subscriberId: item._id,
                    user: item.name,
                    success: item.storyReceiviesSuccess.total ? item.storyReceiviesSuccess.total : 0,
                    fail: item.storyReceiviesFailed.total ? item.storyReceiviesFailed.total : 0,
                    successfulStories: "",
                    failedStories: ""
                  })

                  try {
                    if (item.storyReceiviesSuccess.stories
                      && item.storyReceiviesSuccess.stories.length != 0) {
                      let str = ""
                      item.storyReceiviesSuccess.stories.forEach((item1, index1) => {
                        if (index1 == item.storyReceiviesSuccess.stories.length - 1) {
                          str += item1.name
                        } else {
                          str += item1.name + ", "
                        }
                      });
                      dataTable[index].successfulStories = str
                    }
                  } catch (err) {
                    // console.error(err)
                    this.alert.handleError(err);
                    // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
                  }

                  try {
                    if (item.storyReceiviesFailed.stories
                      && item.storyReceiviesFailed.stories.length != 0) {
                      let str = ""
                      item.storyReceiviesFailed.stories.forEach((item1, index1) => {
                        if (index1 == item.storyReceiviesFailed.stories.length - 1) {
                          str += item1.name
                        } else {
                          str += item1.name + ", "
                        }
                      });
                      dataTable[index].failedStories = str
                    }
                  } catch (err) {
                    // console.error(err)
                    this.alert.handleError(err);
                    // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
                  }
                })
                this.dataSource.data = dataTable;
                this.dataSource.sort = this.sort;
              } else {
                this.noTable = true
              }
            } else {
              this.notFound = true
            }
             
          } catch (err) {
            // console.error(err)
            this.alert.handleError(err);
            // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
          } finally {
            this.displayedColumns = ['id', 'user', 'success', 'fail', 'successfulStories', 'failedStories'];
            this.loadDone = true
          }

          this.subscriberAnalyticData = [
            { name: "Đăng ký", value: this.result.totalSubscribingSubscriber },
            { name: "Livechat", value: this.result.totalLiveChatSubscriber || 0 },
            { name: "Đang chờ", value: this.result.totalWaitingSubscriber },
            { name: "Bị chặn", value: this.result.totalBlockedSubscriber },
          ]

          if (this.result.gender) {
            this.subscriberGenderData = [
              { name: "Nam", value: this.result.gender.male ? this.result.gender.male : 0 },
              { name: "Nữ", value: this.result.gender.female ? this.result.gender.female : 0 },
              { name: "Khác", value: this.result.gender.other ? this.result.gender.other : 0 }
            ]
          } else {
            this.subscriberGenderData = []
          }

          // if (this.result.activities) {
          //   this.subscriberSubscribingData = [
          //     { name: "Nam", value: this.result.activities.subscribing.gender.male },
          //     { name: "Nữ", value: this.result.activities.subscribing.gender.female },
          //     { name: "Khác", value: this.result.activities.subscribing.gender.other }
          //   ]

          //   this.subscriberBlockedData = [
          //     { name: "Nam", value: this.result.activities.blocked.gender.male },
          //     { name: "Nữ", value: this.result.activities.blocked.gender.female },
          //     { name: "Khác", value: this.result.activities.blocked.gender.other }
          //   ]
          // } else {
          //   this.subscriberSubscribingData = []
          //   this.subscriberBlockedData = []
          // }
          break;
        }
      case 'story': {
        try{
          await this.loadStoryInteraction()
          this.loadReceiveStory()
          this.loadStory()
        } catch (err){
          console.log( err)
          this.alert.handleError(err);
          // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
        }finally{
          this.loadDone = true;
        }
        
        break;
      }
      case 'topic': {
        fromDateTime = moment(this.filter.topic.start_date)
        fromDateTime.hour(this.filter.topic.start_time.hour)
        fromDateTime.minutes(this.filter.topic.start_time.minute)

        toDateTime = moment(this.filter.topic.end_date)
        toDateTime.hour(this.filter.topic.end_time.hour)
        toDateTime.minutes(this.filter.topic.end_time.minute)

        params = {
          startTime: moment(fromDateTime.toDate()).utc().format(),
          endTime: moment(toDateTime.toDate()).utc().format()
        };

        try {
          response = await this.chatbotApi.statistic.summaryTopics(params, refresh)
          if (response) {
            this.result = response
            this.notFound = true;

            if (this.result.topics) {
              let dataTable: any[] = [];
              this.result.topics.forEach((item, index) => {
                dataTable.push({
                  id: index + 1,
                  name: item.name,
                  topicId: item._id,
                  totalEvents: item.events.length,
                  totalSubscriber: item.totalSubscriber,
                  totalUnsubscriber: item.totalUnsubscriber,
                })

                if (item.events) {
                  let content = ""
                  item.events.forEach((item, index, array) => {
                    content += item.name
                    if (index != array.length - 1) {
                      content += ", "
                    }
                  })
                  dataTable[index].events = content
                  dataTable[index].eventNum = item.events.length
                }
              })
              this.dataSource.data = dataTable;
              this.dataSource.sort = this.sort;
            } else {
              this.noTable = true
            }
          } else {
            this.notFound = true;
          }
        } catch (err) {
          // console.error(err)
          this.alert.handleError(err);
          // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
        } finally {
          this.displayedColumns = ['id', 'name', 'events', 'totalEvents', 'totalSubscriber', 'totalUnsubscriber'];
          this.loadDone = true;
        }

        this.topicNumberData = [
          { name: "Tổng số chủ đề", value: this.result.totalTopic },
          { name: "Đăng ký chủ đề", value: this.result.totalSubscriber },
          { name: "Hủy đăng ký chủ đề", value: this.result.totalUnsubscriber }
        ]
        break;
      }
      case 'post': {
        fromDateTime = moment(this.filter.post.start_date)
        fromDateTime.hour(this.filter.post.start_time.hour)
        fromDateTime.minutes(this.filter.post.start_time.minute)

        toDateTime = moment(this.filter.post.end_date)
        toDateTime.hour(this.filter.post.end_time.hour)
        toDateTime.minutes(this.filter.post.end_time.minute)

        subscriberCheck = this.filter.post.subscriber;

        params = {
          startTime: moment(fromDateTime.toDate()).utc().format(),
          endTime: moment(toDateTime.toDate()).utc().format(),
          subscriberDetail: subscriberCheck
        };

        try {
          response = await this.chatbotApi.statistic.summaryPosts(params, refresh)
          console.log('data', response)
          if (response) {
            this.result = response
            this.notFound = true;

            if (this.result.interactPosts) {
              let dataTable: any[] = [];
              this.result.interactPosts.forEach((item, index) => {
                dataTable.push({
                  id: index + 1,
                  postId: item._id,
                  name: item.name,
                  postLink: item.postLink,
                  createdAt: item.createdAt,
                  totalComment: item.totalComment,
                  subscriberLength: item.subscribers.length
                })
              })

              this.dataSource.data = dataTable;
              this.dataSource.sort = this.sort;
            } else {
              this.noTable = true
            }
          } else {
            this.notFound = true;
          }
        } catch (err) {
          // console.error(err)
          this.alert.handleError(err);  
          // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
        } finally {
          this.displayedColumns = ['id', 'name', 'createdAt', 'postLink', 'totalComment'];
          if (subscriberCheck) {
            this.displayedColumns.push('subscriberLength')
          }

          this.loadDone = true;
        }
        break;
      }
      case 'reactPost': {

        params = {
        };
        try {
          response = await this.chatbotApi.statistic.summaryReactPosts(params, refresh)
          if (response) {
            this.result = response
            this.notFound = true;

            if (this.result.posts) {
              let dataTable: any[] = [];
              this.result.posts.forEach((item, index) => {
                let message: String = item.message
                if (message && message.length > 100) {
                  message = message.slice(1, 100)
                  message += '..'
                }
                dataTable.push({
                  id: index + 1,
                  postId: item.id,
                  date: item.created_time,
                  thumb: item.picture || 'https://i.imgur.com/YvkA0IB.png',
                  name: message || "Không có",
                  postLink: item.link,
                  totalComment: item.statisticData.totalComment,
                  totalReaction: item.statisticData.totalReaction,
                  totalUser: item.statisticData.users.length
                })
              })

              this.dataSource.data = dataTable;
              this.dataSource.sort = this.sort;
              console.log( 'reactPost', this.dataSource)
            } else {
              this.noTable = true
            }
          } else {
            this.notFound = true;
          }
        } catch (err) {
          // console.error(err)
          this.alert.handleError(err);
          // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
        } finally {
          this.displayedColumns = ['date', 'thumb', 'name', 'postLink', 'totalComment', 'totalReaction', 'totalUser'];
          this.loadDone = true;
        }

        break;
      }
      case 'referral': {
        fromDateTime = moment(this.filter.referral.start_date)
        fromDateTime.hour(this.filter.referral.start_time.hour)
        fromDateTime.minutes(this.filter.referral.start_time.minute)

        toDateTime = moment(this.filter.referral.end_date)
        toDateTime.hour(this.filter.referral.end_time.hour)
        toDateTime.minutes(this.filter.referral.end_time.minute)

        subscriberCheck = this.filter.referral.subscriber;

        params = {
          startTime: moment(fromDateTime.toDate()).utc().format(),
          endTime: moment(toDateTime.toDate()).utc().format(),
          subscriberDetail: subscriberCheck
        };

        try {
          response = await this.chatbotApi.statistic.summaryReferral(params, refresh)
          if (response) {
            this.result = response
            this.notFound = true;

            if (this.result.referrals) {
              let dataTable: any[] = [];
              this.result.referrals.forEach((item, index) => {
                dataTable.push({
                  id: index + 1,
                  referralId: item._id,
                  name: item.name,
                  createdAt: item.createdAt,
                  type: (item.type == 'story' ? 'Câu chuyện' : (item.type == 'promotion' ? 'Khuyến mãi' : 'Innoway')),
                  totalSubscriberAccess: item.totalSubscriberAccess,
                  subscriberLength: item.subscribers.length,
                  used: item.used,
                  amount: item.amount
                })
              })
              this.dataSource.data = dataTable;
              this.dataSource.sort = this.sort;
            } else {
              this.noTable = true
            }
          } else {
            this.notFound = true;
          }
        } catch (err) {
          // console.error(err)
          this.alert.handleError(err);
          // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
        } finally {
          this.displayedColumns = ['id', 'name', 'createdAt', 'storyType', 'totalSubscriberAccess', 'used', 'amount'];
          this.loadDone = true;
        }

        this.referralNumberData = [{
          name: "Tổng số referral",
          value: this.result.totalReferral
        }]
        break;
      }
      case 'button': {

        fromDateTime = moment(this.filter.button.start_date)
        fromDateTime.hour(this.filter.button.start_time.hour)
        fromDateTime.minutes(this.filter.button.start_time.minute)

        toDateTime = moment(this.filter.button.end_date)
        toDateTime.hour(this.filter.button.end_time.hour)
        toDateTime.minutes(this.filter.button.end_time.minute)

        subscriberCheck = this.filter.button.subscriber;

        params = {
          startTime: moment(fromDateTime.toDate()).utc().format(),
          endTime: moment(toDateTime.toDate()).utc().format(),
          subscriberDetail: subscriberCheck
        };

        try {
          response = await this.chatbotApi.statistic.summaryButtonTags(params, refresh)
          if (response) {
            this.result = response
            this.notFound = true;

            if (this.result.buttonTags) {
              let dataTable: any[] = [];
              this.result.buttonTags.forEach((item, index) => {
                dataTable.push({
                  id: index + 1,
                  buttonTagId: item._id,
                  name: item.name,
                  createdAt: item.createdAt,
                  status: (item.status == 'active' ? 'Hoạt động' : 'Không hoạt động'),
                  totalSubscriberAccess: item.totalSubscriberAccess,
                  count: item.count
                })
              })

              this.dataSource.data = dataTable;
              this.dataSource.sort = this.sort;
            } else {
              this.noTable = true
            }
          } else {
            this.notFound = true;
          }
        } catch (err) {
          // console.error(err)
          this.alert.handleError(err);
          // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
        } finally {
          this.displayedColumns = ['id', 'name', 'status', 'createdAt', 'count', 'totalSubscriberAccess'];
          this.loadDone = true;
        }

        this.buttonTagNumberData = [{
          name: "Tổng số tag nút bấm",
          value: this.result.totalButtonTag
        }]
        break;
      }

      case 'page-post': {

        params = {
          postId: this.filter.postId
        }

        try {
          response = await this.chatbotApi.statistic.summaryPostDetail(params, refresh)
          if (response) {
            this.result = response
            this.notFound = true;

            let dataTable: any[] = [];
            this.result.users.forEach((item, index) => {
              let comments = ''
              item.comments.forEach((comment: any) => { comments = comments + "'" + comment.message + "',"; return })
              comments = comments.length !== 0 ? comments.slice(0, comments.length - 1) + "." : ""
              dataTable.push({
                id: index + 1,
                uid: item.uid,
                reaction: item.reaction,
                name: item.name,
                comment: comments,
                count: item.commentCount
              })
            })
            this.dataSource.data = dataTable;
            this.dataSource.sort = this.sort;
            console.log('data', this.result)
          } else {
            this.notFound = true;
          }
        } catch (err) {
          // console.error(err)
          this.alert.handleError(err);
          // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
        } finally {
          this.displayedColumns = ['id', 'uid', 'name', 'reaction', 'comment', 'count'];
          this.loadDone = true;
        }

        this.pagePostNumberData = [
          { name: "Reaction", value: response.totalReaction },
          { name: "Bình luận", value: response.totalComment }
        ]
        this.reactionNumberData = [
          { name: "LIKE", value: response.reaction.LIKE },
          { name: "LOVE", value: response.reaction.LOVE },
          { name: "HAHA", value: response.reaction.HAHA },
          { name: "WOW", value: response.reaction.WOW },
          { name: "SAD", value: response.reaction.SAD },
          { name: "ANGRY", value: response.reaction.ANGRY }
        ]
        break;
      }
      case 'subscriberInteraction': {

        fromDateTime = moment(this.filter.subscriberInteraction.start_date)
        fromDateTime.hour(this.filter.subscriberInteraction.start_time.hour)
        fromDateTime.minutes(this.filter.subscriberInteraction.start_time.minute)

        toDateTime = moment(this.filter.subscriberInteraction.end_date)
        toDateTime.hour(this.filter.subscriberInteraction.end_time.hour)
        toDateTime.minutes(this.filter.subscriberInteraction.end_time.minute)

        params = {
          startTime: moment(fromDateTime.toDate()).utc().format(),
          endTime: moment(toDateTime.toDate()).utc().format(),
        };

        try {
          const subscribe = await this.chatbotApi.statistic.summarySubscribe(params, refresh)
          const block = await this.chatbotApi.statistic.summaryBlock(params, refresh)
          this.result = response = subscribe
          this.pageInteractionData = [
            {
              name: "Đang đăng ký",
              series: subscribe.interactive
            },
            {
              name: "Đã chặn",
              series: block.interactive
            }
          ]
          console.log('res', this.pageInteractionData)
        } catch (err) {
          // console.error(err)
          this.alert.handleError(err);
          // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
        } finally {
          this.loadDone = true
        }
        break;
      } 
    }
    this.applyFilter("")
    this.detectChanges()
  }

  async refresh() {
    this.startSummary(true)
  }
  async loadReceiveStory(){

      this.loadChartDone.unique_subscriber_receive_story = false
      let fromDateTime, toDateTime;
      let params, response;
      fromDateTime = moment(this.filter[this.mode].unique_subscriber_receive_story.start_date)
      fromDateTime.hour(this.filter[this.mode].unique_subscriber_receive_story.start_time.hour)
      fromDateTime.minutes(this.filter[this.mode].unique_subscriber_receive_story.start_time.minute)

      toDateTime = moment(this.filter[this.mode].unique_subscriber_receive_story.end_date)
      toDateTime.hour(this.filter[this.mode].unique_subscriber_receive_story.end_time.hour)
      toDateTime.minutes(this.filter[this.mode].unique_subscriber_receive_story.end_time.minute)

      params = {
        startTime: moment(fromDateTime.toDate()).utc().format(),
        endTime: moment(toDateTime.toDate()).utc().format(),
      };

      try {
        this.detectChanges()
        response = await this.chatbotApi.statistic.uniqueSubscriberReceiveStory(params, true)
        if (response) {
          this.result = response
          this.notFound = true;

          this.uniqueSubcriberReceiveStoryData = [
            {
              name: "Người dùng nhận câu chuyện",
              series: response.interactive
            }
          ]

        } else {
          this.notFound = true;
        }
      } catch (err) {
        // console.error(err)
        this.alert.handleError(err);
        // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
      } finally {
        this.loadChartDone.unique_subscriber_receive_story = true
        this.detectChanges()
      }

  }
  async loadStoryInteraction(){
    this.loadChartDone.storyInteraction = false
    let fromDateTime, toDateTime;
    let params, response;
      fromDateTime = moment(this.filter[this.mode].storyInteraction.start_date)
      fromDateTime.hour(this.filter[this.mode].storyInteraction.start_time.hour)
      fromDateTime.minutes(this.filter[this.mode].storyInteraction.start_time.minute)

      toDateTime = moment(this.filter[this.mode].storyInteraction.end_date)
      toDateTime.hour(this.filter[this.mode].storyInteraction.end_time.hour)
      toDateTime.minutes(this.filter[this.mode].storyInteraction.end_time.minute)

      params = {
        startTime: moment(fromDateTime.toDate()).utc().format(),
        endTime: moment(toDateTime.toDate()).utc().format(),
      };

      try {
        this.detectChanges()
        response = await this.chatbotApi.statistic.storySent(params, true)
        if (response) {
          console.log(response)
          this.result = response
          this.noTable = true
        } else {
          this.notFound = true
        }
      } catch (err) {
        // console.error(err)
        this.alert.handleError(err);
        // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
      } finally {

        this.loadChartDone.storyInteraction = true
        this.detectChanges()
      }
      this.interactionStoryData = [
        {
          name: "Số tương tác",
          series: this.result.interactive
        }

      ]
      this.detectChanges()
      
  }

  async loadStory(){
    this.loadChartDone.story = false
    let fromDateTime, toDateTime;
    let params, response;
    fromDateTime = moment(this.filter[this.mode].story.start_date)
    fromDateTime.hour(this.filter[this.mode].story.start_time.hour)
    fromDateTime.minutes(this.filter[this.mode].story.start_time.minute)

    toDateTime = moment(this.filter[this.mode].story.end_date)
    toDateTime.hour(this.filter[this.mode].story.end_time.hour)
    toDateTime.minutes(this.filter[this.mode].story.end_time.minute)
    params = {
      startTime: moment(fromDateTime.toDate()).utc().format(),
      endTime: moment(toDateTime.toDate()).utc().format(),
  
    };

    try {
      this.detectChanges()
      response = await this.chatbotApi.statistic.countNumberMessage(params, true)
      // response = await this.chatbotApi.statistic.summaryStories(params, true)
      if (response) {
        this.result = response

        if (this.result) {
          let dataTable: any[] = [];
          this.result.forEach((item, index) => {
            dataTable.push({
              id: index + 1,
              storyId: item.story,
              story: item.name.join(', '),
              totalSent: item.totalProcess ? item.totalProcess : 0,
              sentSuccess: item.completeProcess ? item.completeProcess : 0,
              sentFailed: item.failedProcess ? item.failedProcess : 0,
              readCount: item.readCount ? item.readCount : 0,
              // clicked: item.clicked.total ? item.clicked.total : 0
            })
          })
          this.dataSource.data = dataTable;
          this.dataSource.sort = this.sort;
        } else {
          this.noTable = true
        }
      } else {
        this.notFound = true
      }
    } catch (err) {
      // console.error(err)
      this.alert.handleError(err);
      // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
    } finally {
      this.displayedColumns = ['id', 'story', 'totalSent', 'sentSuccess', 'sentFailed', 'readCount'];
      this.loadChartDone.story = true
      this.detectChanges()
    }

    this.storyNumberData = [{
      name: "Tổng số câu chuyện",
      value: this.result.amout
    }]
    this.detectChanges()
  }
  async loadSubscribeByDayStatic(){
    this.loadChartDone.subscribeByDay = false
    let fromDateTime, toDateTime;
    let params, response;
    fromDateTime = moment(this.filter.subscriber.subscribeByDay.start_date)
        fromDateTime.hour(this.filter.subscriber.subscribeByDay.start_time.hour)
        fromDateTime.minutes(this.filter.subscriber.subscribeByDay.start_time.minute)

        toDateTime = moment(this.filter.subscriber.subscribeByDay.end_date)
        toDateTime.hour(this.filter.subscriber.subscribeByDay.end_time.hour)
        toDateTime.minutes(this.filter.subscriber.subscribeByDay.end_time.minute)

        params = {
          startTime: moment(fromDateTime.toDate()).utc().format(),
          endTime: moment(toDateTime.toDate()).utc().format(),
        };

        try {
          this.detectChanges()
            response = await this.chatbotApi.statistic.summarySubscribeByDay(params, true)
            console.log( 'res',response)
            if (response) {
              this.result = response
              this.notFound = false;
    
              this.subscribeByDayData = [
                {
                  name: "Người đăng kí",
                  series: response.total
                }
              ]
    
            } else {
              this.notFound = true;
            }
        } catch (err) {
            // console.error(err)
          this.alert.handleError(err);
            // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
        } finally {
          this.loadChartDone.subscribeByDay = true
          this.detectChanges()
        }
  }
  async loadGenderStatic(){
    this.loadChartDone.genderInteraction = false
    let fromDateTime, toDateTime;
    let params, response;
    fromDateTime = moment(this.filter.subscriber.genderInteraction.start_date)
        fromDateTime.hour(this.filter.subscriber.genderInteraction.start_time.hour)
        fromDateTime.minutes(this.filter.subscriber.genderInteraction.start_time.minute)

        toDateTime = moment(this.filter.subscriber.genderInteraction.end_date)
        toDateTime.hour(this.filter.subscriber.genderInteraction.end_time.hour)
        toDateTime.minutes(this.filter.subscriber.genderInteraction.end_time.minute)

        params = {
          startTime: moment(fromDateTime.toDate()).utc().format(),
          endTime: moment(toDateTime.toDate()).utc().format(),
          isCheckGender: true 
        };

        try {
          this.detectChanges()
          if (this.filter.subscriber.genderInteraction.subscribe) {
            response = await this.chatbotApi.statistic.summarySubscribe(params, true)
          } else if (this.filter.subscriber.genderInteraction.block) {
            response = await this.chatbotApi.statistic.summaryBlock(params, true)

          }
          this.genderSubcriberInteraction = response.interactive


          this.result = response
        } catch (err) {
            // console.error(err)
          this.alert.handleError(err);
            // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
        } finally {
          this.loadChartDone.genderInteraction = true
          this.detectChanges()
        }
  }
  async loadSubscriberInteract(){
    this.loadChartDone.subscriber_interact = false
    let fromDateTime, toDateTime;
    let params, response;

      fromDateTime = moment(this.filter.subscriber.subscriber_interact.start_date)
      fromDateTime.hour(this.filter.subscriber.subscriber_interact.start_time.hour)
      fromDateTime.minutes(this.filter.subscriber.subscriber_interact.start_time.minute)

      toDateTime = moment(this.filter.subscriber.subscriber_interact.end_date)
      toDateTime.hour(this.filter.subscriber.subscriber_interact.end_time.hour)
      toDateTime.minutes(this.filter.subscriber.subscriber_interact.end_time.minute)

      params = {
        startTime: moment(fromDateTime.toDate()).utc().format(),
        endTime: moment(toDateTime.toDate()).utc().format(),
      };

      try {
        this.detectChanges()
        response = await this.chatbotApi.statistic.subscriberInteractives(params, true)
        if (response) {
          this.result = response
          this.notFound = true;

          this.subscriberInteractionLineData = [
            {
              name: "Lần bấm nút",
              series: response.button
            },
            {
              name: "Lần đọc tin nhắn",
              series: response.read
            },
            {
              name: "Người dùng duy nhất",
              series: response.interactive
            }
          ]

        } else {
          this.notFound = true;
        }
      } catch (err) {
        // console.error(err)
        this.alert.handleError(err);
        // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
      } finally {
        this.loadChartDone.subscriber_interact = true
        this.detectChanges()
      }

  }
  async loadOverviewStatic(){
    this.loadChartDone.overview = false
    let fromDateTime, toDateTime;
    let params, response;

      fromDateTime = moment(this.filter.overview.start_date)

      toDateTime = moment(this.filter.overview.end_date)

      params = {
        startDate: moment(fromDateTime.toDate()).startOf('day').utc().format(),
        endDate: moment(toDateTime.toDate()).endOf('day').utc().format(),
        code: 'ChartStatisticPostsReport'
      };

      try {
        this.detectChanges()
        response = await this.chatbotApi.report.chartStatisticPostsReport(params)
        if (response) {
          this.notFound = false;
          let chart = response.charts[0]
          this.overviewChartScheme = { domain: chart.colColors }
          this.overviewChartData = chart.data.map(x => ({
            name: x.xLabel,
            series: x.colValues.map((value, index) => ({
              name: chart.colLabels[index],
              value
            }))
          }))

        } else {
          this.notFound = true;
        }
      } catch (err) {
        // console.error(err)
        this.alert.handleError(err);
        // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
      } finally {
        this.loadChartDone.overview = true
        this.detectChanges()
      }
  }
  async loadGroupStatic(){
    this.loadChartDone.group = false
    let fromDateTime, toDateTime;
    let params, response;

      fromDateTime = moment(this.filter.group.start_date)
      fromDateTime.hour(this.filter.group.start_time.hour)
      fromDateTime.minutes(this.filter.group.start_time.minute)

      toDateTime = moment(this.filter.group.end_date)
      toDateTime.hour(this.filter.group.end_time.hour)
      toDateTime.minutes(this.filter.group.end_time.minute)

      params = {
        startTime: moment(fromDateTime.toDate()).utc().format(),
        endTime: moment(toDateTime.toDate()).utc().format(),
        groupId: this.data.groupId
      };

      try {
        this.detectChanges()
        response = await this.chatbotApi.statistic.summaryGroup(params, true)
        console.log('data group', response)
        if (response) {
          this.result = response
          this.notFound = false;
          this.groupData = [
            {
              name: "Người dùng",
              series: response
            }
          ]

        } else {
          this.notFound = true;
        }
      } catch (err) {
        // console.error(err)
        this.alert.handleError(err);
        // this.alert.error("Có lỗi xảy ra khi lấy thống kê", err.message)
      } finally {
        this.loadChartDone.group = true
        this.detectChanges()
      }
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    if (this.dataSource.filteredData.length == 0) this.isEmpty = true
    else this.isEmpty = false
    this.detectChanges()
  }

  async openTopicStatistics(topicId, topicName) {
    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "StatisticsDetailsResultPortalComponent", {
      mode: 'topic',
      topicId: topicId,
      portalName: 'Thống kê ',
      name: topicName
    })
  }

  async openStoryStatistics(storyId, storyName) {
    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "StatisticsDetailsResultPortalComponent", {
      mode: 'story',
      storyId: storyId,
      portalName: 'Thống kê ',
      name: storyName
    })
  }

  async openPostStatistics(postId, postName) {
    console.log( 'postId', postId)
    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "StatisticsDetailsResultPortalComponent", {
      mode: 'post',
      postId: postId,
      portalName: 'Thống kê ',
      name: postName
    })
  }

  async openReferralStatistics(referralId, referralName) {
    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "StatisticsDetailsResultPortalComponent", {
      mode: 'referral',
      referralId: referralId,
      portalName: 'Thống kê ',
      name: referralName
    })
  }

  async openSubscriber(subscriberId) {

    if (this.portalService.getLastPortalName() == "SubscriberDetailPortalComponent") {
      const component = this.portalService.container.portals[this.portalService.container.portals.length - 1];
      await component.reload(subscriberId);
      component.focus()
    } else {
      this.portalService.pushPortalAt(this.index + 1, "SubscriberDetailPortalComponent", {
        subscriberId: subscriberId
      })
    }
  }
  async openGuidePortal() {
    if (this.portalService.getLastPortalName() != 'GuidePortalComponent') {
      this.portalService.pushPortal('GuidePortalComponent', { slug: "" });
    }
  }
  async setFilterTime(instance: string, mode: string, mode2:string) {
    if(!mode2){
      this.filter[mode].typeDate = instance
      switch (instance) {
        case "7_day":
          this.filter[mode].start_date = moment().subtract(7, 'days').format()
          this.filter[mode].end_date = moment().format()
          break;
        case "30_day":
          this.filter[mode].start_date = moment().subtract(30, 'days').format()
          this.filter[mode].end_date = moment().format()  
          break;
        case "current_month":
          this.filter[mode].start_date = moment().startOf('month').format()
          this.filter[mode].end_date = moment().endOf('month').format()
          break;
        case "previous_month":
          this.filter[mode].start_date = moment().startOf('month').subtract(1, 'months').format()
          this.filter[mode].end_date = moment().startOf('month').subtract(1, 'days').format()
          break;
      }
    }else{
      this.filter[mode][mode2].typeDate = instance
      switch (instance) {
        case "7_day":
          this.filter[mode][mode2].start_date = moment().subtract(7, 'days').format()
          this.filter[mode][mode2].end_date = moment().format()
          break;
        case "30_day":
          this.filter[mode][mode2].start_date = moment().subtract(30, 'days').format()
          this.filter[mode][mode2].end_date = moment().format()  
          break;
        case "current_month":
          this.filter[mode][mode2].start_date = moment().startOf('month').format()
          this.filter[mode][mode2].end_date = moment().endOf('month').format()
          break;
        case "previous_month":
          this.filter[mode][mode2].start_date = moment().startOf('month').subtract(1, 'months').format()
          this.filter[mode][mode2].end_date = moment().startOf('month').subtract(1, 'days').format()
          break;
      }
    }
    
    switch(mode2){
      case 'subscribeByDay' : return this.loadSubscribeByDayStatic();
      case 'genderInteraction': return   this.loadGenderStatic();
      case 'subscriber_interact':   return this.loadSubscriberInteract()
      case 'story' : return this.loadStory()
      case 'storyInteraction' : return this.loadStoryInteraction()
      case 'unique_subscriber_receive_story': return this.loadReceiveStory()
      default: {
        if (this.mode == 'overview') return this.loadOverviewStatic()
        else return this.startSummary()
      }
    }
  }
  async openStatisticPostal(postId: string) {
    console.log( 'id', postId)
    if (this.portalService.container.getPortalNameAt(this.index + 1) != "StatisticsResultPortalComponent") {
      console.log("pushPortal")
      await this.portalService.pushPortalAt(this.index + 1, 'StatisticsResultPortalComponent', { portalName: "Thống kê" })
    }
    const component = this.portalService.container.portals[this.index + 1]
    component.getStatisticResult("page-post", "Thống kê tương tác", { postId })

  }
  onDateOptionSelect(code ,mode){
    if(code!="custom") {
      this.setFilterTime(code,this.mode,mode);
    } else{
      if(!mode){
        this.filter[this.mode].typeDate = 'custom'
      }else{
        this.filter[this.mode][mode].typeDate = 'custom'
      }
    }
    this.detectChanges()
  }
  onDateChange(date,typeDate, mode){
    if(typeDate == 'start'){
      if(mode){
        this.filter[this.mode][mode].start_date = date
      }else{
        this.filter[this.mode].start_date = date
      }
    }else{
      if(mode){
        this.filter[this.mode][mode].end_date = date
      }else{
        this.filter[this.mode].end_date = date
      }
    }
    switch(mode){
      case 'subscribeByDay' : return this.loadSubscribeByDayStatic();
      case 'genderInteraction': return   this.loadGenderStatic();
      case 'subscriber_interact':   return this.loadSubscriberInteract()
      case 'story' : return this.loadStory()
      case 'storyInteraction' : return this.loadStoryInteraction()
      case 'unique_subscriber_receive_story': return this.loadReceiveStory()
      case 'group': return this.loadGroupStatic()
      case 'overview': return this.loadOverviewStatic()
      default: return this.startSummary()
    }
  }

  onOptionsChange(event){
    console.log(event )
    if(this.moreOptions[this.mode].type =="radio"){
      this.moreOptions[this.mode].value = event.value
      this.moreOptions[this.mode].options.forEach(option=>{
        this.filter[this.mode][option.code] = false
      })
      this.filter[this.mode][event.value] = true
    }else{
      this.filter[this.mode][event.code] = event.value
    }
    console.log( 'after',this.filter[this.mode])
    this.detectChanges()
    this.startSummary()
  }

  syncPostTask: iTask
  syncPostInterval: any
  syncingPosts$ = new BehaviorSubject<boolean>(false)
  async syncPosts() {
    if (this.syncingPosts$.getValue()) return
    if (this.chatbotApi.chatbotAuth.user.rule != 'admin') {
      this.alert.info('Không đủ quyền đồng bộ', 'Chỉ có admin mới có thể thực hiện đồng bộ')
      return
    }
    try {
      this.syncingPosts$.next(true)
      let syncPostTask = await this.chatbotApi.post.syncPosts()
      this.syncPostInterval = setInterval(async () => {
        await this.checkTask(syncPostTask._id)
      }, 2000)
      
    } catch (err) {
      console.error(err)
      this.alert.handleError(err, 'Lỗi khi đồng bộ bài viết')
      this.syncingPosts$.next(false)
    }
  }

  async checkTask(id) {
    try {
      this.syncPostTask = await this.chatbotApi.task.getItem(id, { local: false })
      if (this.syncPostTask.processState != 'running') {
        clearInterval(this.syncPostInterval)
        this.syncingPosts$.next(false)
        if (this.syncPostTask.processState == 'complete') {
          this.alert.success('Đồng bộ thành công', 'Tổng quan Fanpage đã được đồng bộ thành công')
        } else {
          this.alert.error('Đồng bộ thất bại', this.syncPostTask.title)
        }
      }
    } catch (err) {
      clearInterval(this.syncPostInterval)
    }
  }
}
