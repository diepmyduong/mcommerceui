import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatisticsResultPortalComponent } from 'app/chatgut-app/portals/statistics-result-portal/statistics-result-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatToolbarModule, MatIconModule, MatProgressBarModule, MatProgressSpinnerModule, MatInputModule, MatTableModule, MatFormFieldModule, MatPaginatorModule, MatTooltipModule, MatButtonModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatMenuModule, MatCheckboxModule, MatRadioModule, MatExpansionModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { StatisticOptionComponent } from './statistic-option/statistic-option.component';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatButtonModule,
    FormsModule,
    NgxChartsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatMenuModule,
    MatCheckboxModule,
    MatRadioModule,
    MatExpansionModule
  ],
  declarations: [StatisticsResultPortalComponent,StatisticOptionComponent],
  entryComponents: [StatisticsResultPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: StatisticsResultPortalComponent }
  ]
})
export class StatisticsResultPortalModule { }
