import { Component, OnInit, Input, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { MatDialog } from '@angular/material';
import { iPage } from 'app/services/chatbot';
import { ComponentRef } from '@angular/core/src/render3';
import { DynamicComponentLoaderService } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';

const SETTINGS = [
  { code: 'StarterPortalComponent', display: "Bắt đầu", icon: "fas fa-bars" },
  { code: 'UserAccessPortalComponent', display: "Quyền truy cập", icon: "fas fa-sign-in-alt" },
  { code: 'PartnerPortalComponent', display: "Cộng tác", icon: "fas fa-handshake" },
  { code: 'TopicsPortalComponent', display: "Chủ đề", icon: "fas fa-calendar-week" },
  { code: 'WhiteListPortalComponent', display: "Tên miền", icon: "far fa-list-alt" },
  { code: "EnvironmentPortalComponent", display: "Biến môi trường", icon: "fas fa-code" },
  { code: 'NotifyPortalComponent', display: "Thông báo", icon: "fas fa-bell" },
  { code: 'MenuPortalComponent', display: 'Plugin mở rộng', icon: 'fas fa-plug', options: [
    { code: 'AiPortalComponent', display: "Trí tuệ nhân tạo", icon: "fab fa-android" },
    { code: "PostsPortalComponent", display: 'Bài đăng', icon: 'far fa-newspaper' },
    { code: 'UserGooglesheetPortalComponent', display: "Danh sách googlesheet", icon: "fas fa-users",  extra: { type: 'gsheetUser' } },
    { code: 'LiveChatSettingsPortalComponent', display: "Cài đặt Live Chat", icon: "fas fa-comment-dots" },
    { code: "QuickMessagesPortalComponent", display: "Tin nhắn nhanh", icon: 'fas fa-rocket' },
    { code: "ImageSearchPortalComponent", display: "Tìm kiếm ảnh", icon: 'far fa-images' },
    { code: "ChatExtensionPortalComponent", display: "Thanh tiện ích Messegner", icon: "fas fa-external-link-alt"},
  ] },
]

const STATISTICS = [
  { code: "StatisticsResultPortalComponent", display: "Tổng quan", icon: "fas fa-chart-bar", extra: { mode: "overview", portalName: "Tổng quan Fanpage" } },
  { code: "StatisticsResultPortalComponent", display: "Người dùng", icon: "fas fa-users", extra: { mode: "subscriber", portalName: "Thống kê người dùng" } },
  { code: "StatisticsResultPortalComponent", display: "Câu chuyện", icon: "fas fa-book", extra: { mode: "story", portalName: "Thống kê câu chuyện" } }, 
  { code: "StatisticsResultPortalComponent", display: "Chủ đề", icon: "fas fa-columns", extra: { mode: "topic", portalName: "Thống kê chủ đề" } }, 
  { code: "StatisticsResultPortalComponent", display: "Bài viết", icon: "fas fa-newspaper", extra: { mode: "post", portalName: "Thống kê bài viết" } }, 
  { code: "StatisticsResultPortalComponent", display: "Tương tác bài viết", icon: "far fa-laugh-beam", extra: { mode: "reactPost", portalName: "Thống kê tương tác bài viết" } }, 
  { code: "StatisticsResultPortalComponent", display: "Đường dẫn", icon: "fas fa-hand-point-right", extra: { mode: "referral", portalName: "Thống kê đường dẫn" } },
  { code: "StatisticsResultPortalComponent", display: "Tag nút bấm", icon: "fas fa-tag", extra: { mode: "button", portalName: "Thống kê tag nút bấm" } }
]

const MARKETING = [
  { code: "ShopPortalComponent", display: 'Shop', icon: 'fas fa-store'},
  { code: "HaravanPortalComponent", display: 'Haravan', svg: 'assets/img/haravan.svg'},
  { code: 'EvoucherPortalComponent', display: "Evoucher", icon: "fas fa-ticket-alt" },
  { code: "BookingPortalComponent", display: 'Đặt lịch', icon: 'fas fa-calendar-alt'},
  { code: 'WheelPortalComponent', display: "Vòng quay", icon: "fas fa-dharmachakra" },
]

const OVERVIEW = [
  { code: "QRCodePortalComponent", display: 'Mã QR', icon: 'fas fa-qrcode'},
  { code: 'DictionaryPortalComponent', display: "Từ khóa", icon: "fas fa-language" },
  // { code: "RefPortalComponent", display: 'Ref câu chuyện', icon: 'fas fa-bookmark'},
]

@Component({
  selector: 'app-menu-portal',
  templateUrl: './menu-portal.component.html',
  styleUrls: ['./menu-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuPortalComponent extends BasePortal implements OnInit {
  page: iPage
  pageId: any
  componentName = "MenuPortalComponent"

  @Input() type: 'SETTINGS' | 'STATISTICS' | 'MARKETING' | 'OVERVIEW'

  menu: any[]
  current: any
  subMenu: any = []

  constructor(
    public dialog: MatDialog,
    private dynamicLoader: DynamicComponentLoaderService,
    public changeRef: ChangeDetectorRef
  ) {
    super()
  }

  async ngOnInit() {
    switch (this.type) {
      case 'SETTINGS':
        this.setPortalName('Thiết lập')
        this.menu = SETTINGS
        break;
      case 'STATISTICS':
        this.setPortalName('Thống kê')
        this.menu = STATISTICS
        break;
      case 'MARKETING':
        this.setPortalName('Hỗ trợ bán hàng')
        this.menu = MARKETING
        break;
      case 'OVERVIEW':
        this.setPortalName('Tổng quan câu chuyện')
        this.menu = OVERVIEW
        break;
    }
    this.menu.forEach(x => {
      x.closed = false
      x.opened = false
    })
    this.detectChanges()
  }

  async ngAfterViewInit() {
    switch (this.type) {
      case 'STATISTICS':
      this.dynamicLoader.getComponentFactory('StatisticsResultPortalComponent')
    }
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async openMenu(item) {
    if (item.options) {
      if (item.opened) {
        this.menu.forEach(x => {
          x.closed = false
          x.opened = false
        })
        this.subMenu = []
      } else {
        this.menu.forEach(x => {
          x.closed = true
        })
        item.closed = false
        item.opened = true
        setTimeout(() => {
          this.subMenu = item.options
          this.detectChanges()
        }, 300)
      }
    } else {
      let compRef = await this.portalService.pushPortalAt(this.index + 1, item.code, item.extra?item.extra:{}) as ComponentRef<BasePortal>
      this.current = item
      this.subscriptions.onClosed = compRef.instance.onClosed.subscribe(() => {
        this.current = null
        this.subscriptions.onClosed.unsubscribe()
        this.detectChanges()
      })
      this.detectChanges()
    }
  }
}
