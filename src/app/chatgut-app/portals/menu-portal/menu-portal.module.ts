import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuPortalComponent } from 'app/chatgut-app/portals/menu-portal/menu-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MaterialModule } from 'app/shared';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [MenuPortalComponent],
  entryComponents: [MenuPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: MenuPortalComponent }
  ]
})
export class MenuPortalModule { }
