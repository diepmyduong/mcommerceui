import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { iReferral, iStory } from 'app/services/chatbot';
import * as moment from 'moment'
import { MatDialog } from '@angular/material';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { groupBy } from 'lodash-es'

@Component({
  selector: 'app-qrcode-portal',
  templateUrl: './qrcode-portal.component.html',
  styleUrls: ['./qrcode-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class QRCodePortalComponent extends BasePortal implements OnInit {

  componentName = "QRCodePortalComponent"

  loadDone$ = new BehaviorSubject<boolean>(false)
  loading$ = new BehaviorSubject<boolean>(false)

  referralGroups$ = new BehaviorSubject<{
    referrals: iReferral,
    story: iStory
  }[]>(undefined)
  get referralGroups() { return this.referralGroups$.getValue() }
  set referralGroups(val) { this.referralGroups$.next(val) }

  referrals$ = new BehaviorSubject<iReferral[]>(undefined)
  get referrals() { return this.referrals$.getValue() }
  set referrals(val) { this.referrals$.next(val) }

  stories: iStory[]

  openedReferral$ = new BehaviorSubject<string>('')
  openedStory$ = new BehaviorSubject<string>('')

  filter: string

  constructor (
    public dialog: MatDialog,
    public popoverService: PopoverService,
    private router: Router,
    public changeRef: ChangeDetectorRef
  ) {
    super()
    this.setPortalName('Danh sách mã QR')
  }

  async ngOnInit() {
    this.reload()
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(local: boolean = true) {
    this.loading$.next(true)
    this.detectChanges()
    try {
      let referrals
      let tasks = []
      tasks.push(this.chatbotApi.referral.getList({ local, query: { 
          order: { 'createdAt': 1 }, 
          limit: 0 
      } }).then(res => referrals = res))
      tasks.push(this.chatbotApi.story.getAll().then(res => {
        this.stories = res
      }))
      await Promise.all(tasks)
      this.referrals = referrals
      this.referralGroups = this.createReferralGroups(this.referrals)
      console.log(this.referralGroups)
    } catch (err) {
      this.alert.handleError(err);
    } finally {
      this.loadDone$.next(true)
      this.loading$.next(false)
    }
  }

  createReferralGroups(referrals) {
    let groupObj = groupBy(referrals, 'option.storyId')
    return Object.keys(groupObj).map(storyId => {      
      let story = this.stories.find(x => x._id == storyId)
      let refs = groupObj[storyId]
      refs.forEach(ref => {
        ref.storyName = story?story.name:'[Không có câu chuyện]'
      })
      return { story, referrals: refs }
    })
  }

  async openQRCode(referral: iReferral) {
    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "StoryReferralPortalComponent", {
      refId: referral._id,
      portalName: referral.name
    })
    this.openedReferral$.next(referral._id)

    if (!componentRef) return;
    let component = componentRef.instance as any
    
    component.onClosed.pipe(take(1)).subscribe(() => {
      this.openedReferral$.next('')
    })
  }

  async addQRCode(event, center = false, qrcode = null) {
    let formOptions: iPopoverFormOptions = {
      parent: '.swiper-slide',
      target: qrcode?'.portal-list-item':center?'.btn-create-qrcode':'.btn-blue',
      targetElement: event.target,
      width: '96%',
      confirm: qrcode?'Chỉnh sửa mã QR':'Tạo mã QR',
      horizontalAlign: 'center',
      verticalAlign: qrcode?'top':'bottom',
      fields: [
        {
          placeholder: 'Tên mã QR',
          property: 'name',
          constraints: ['required'],
          current: qrcode ? qrcode.name : ''
        }, {
          placeholder: 'Câu chuyện',
          property: 'storyId',
          type: 'story',
          constraints: ['required'],
          current: qrcode ? qrcode.option.storyId : ''
        }
      ],
      onSubmit: async (data) => {
        if (!data.name) return
        try {
          this.loading$.next(true)
          let { name, storyId } = data
          if (qrcode) {
            let newQrCode = await this.chatbotApi.referral.update(qrcode._id, {
              name, option: {
                storyId
              }
            })
            let index = this.referrals.findIndex(x => x._id == qrcode._id)
            if (index >= 0) {
              let index = this.referrals.findIndex(x => x._id == qrcode._id)
              let referrals = this.referrals
              referrals[index] = newQrCode
              this.referralGroups = this.createReferralGroups(this.referrals)
            }
          } else {
            const qrCode = await this.chatbotApi.referral.add({
              name,
              limit: 0,
              amount: 0,
              type: 'story',
              option: {
                storyId
              },
              startDate: moment().add(1, 'days').startOf('day').toDate(),
              endDate :moment().add(1, 'days').endOf('day').toDate(),
            })
            this.referrals = [...this.referrals, qrCode]
            this.referralGroups = this.createReferralGroups(this.referrals)
            if (!this.openedReferral$.getValue()) {
              this.openQRCode(qrCode)
            }
          }
        } catch (err) {
          this.alert.handleError(err, 'Lỗi khi tạo mã QR');
        } finally {
          this.loading$.next(false)
          setTimeout(() => {
            let el = document.getElementById(this.portalId)
            el.scroll({ top: el.scrollHeight, behavior: 'smooth' })
          })
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  async openStory(story) {
    if (!story) {
      this.alert.info('Không tìm thấy câu chuyện', 'Câu chuyện không có hoặc đã bị xoá.')
      return
    }
    let componentRef = await this.portalService.pushPortalAt(this.index + 1, "StoryDetailPortalComponent", { 
      storyId: story._id, 
      portalName: story.name
    })
    this.openedStory$.next(story._id)

    if (!componentRef) return;
    let component = componentRef.instance as any
    
    component.onClosed.pipe(take(1)).subscribe(() => {
      this.openedStory$.next('')
    })
  }

  async removeQRCode(qrcode: iReferral) {
    if (!await this.alert.warning('Xoá mã QR', "Bạn có muốn xoá mã QR này không?", 'Xóa')) return;
    try {
      this.loading$.next(true)
      await this.chatbotApi.referral.delete(qrcode._id);
      if (this.openedReferral$.getValue() == qrcode._id) {
        this.portalService.popPortal(this.index + 1);
      }
      let referrals = this.referrals
      let index = referrals.findIndex(x => x._id == qrcode._id)
      if (index >= 0) {
        referrals.splice(index, 1)
        this.referrals = [...referrals]
      }      
      this.referralGroups = this.createReferralGroups(this.referrals)
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi xóa mã QR");
    } finally {
      this.loading$.next(false)
    }
  }

  trackByFn(index, item) {
    return item._id
  }

}

