import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatExtensionPortalComponent } from 'app/chatgut-app/portals/chat-extension-portal/chat-extension-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDividerModule } from '@angular/material/divider';
import { SelectionModule } from 'app/modals/selection/selection.module';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule,
    MatButtonModule,
    SelectionModule,
    EditInfoModule
  ],
  declarations: [ChatExtensionPortalComponent],
  entryComponents: [ChatExtensionPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ChatExtensionPortalComponent }
  ]
})
export class ChatExtensionPortalModule { }
