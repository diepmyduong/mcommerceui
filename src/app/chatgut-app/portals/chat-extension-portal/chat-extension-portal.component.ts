import { Component, OnInit, Output, EventEmitter, ViewChild, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { NgForm } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { iSetting, iPage } from 'app/services/chatbot';
import { iEditInfoDialogData, EditInfoDialog } from 'app/modals/edit-info/edit-info.component';
import { SelectionDialog, iSelectionData } from 'app/modals/selection/selection.component';
import { environment } from '../../../../environments';

@Component({
  selector: 'app-chat-extension-portal',
  templateUrl: './chat-extension-portal.component.html',
  styleUrls: ['./chat-extension-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatExtensionPortalComponent extends BasePortal implements OnInit {
  @Output() onChange = new EventEmitter<any>()
  @Input() settingId: string
  @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
  constructor(
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef
  ) {
    super()
    this.setPortalName("Thanh tiện ích Messenger")
  }
  componentName = "ChatExtensionPortalComponent"
  setting: iSetting
  whilelistSetting: iSetting
  extensions = {
    custom: { icon: 'fas fa-globe', display: 'Website', code: 'custom' },
    google_map: { icon: 'fas fa-map', display: 'Mở Google Map', code: 'google_map' },
    order_innoway: { icon: 'fas fa-cart-plus', display: 'Đặt hàng', code: 'order_innoway' },
    basket_innoway: { icon: 'fas fa-shopping-basket', display: 'Giỏ hàng', code: 'basket_innoway' },
    profile_innoway: { icon: 'fas fa-user-circle', display: 'Thông tin cá nhân', code: 'profile_innoway' },
    extension_order: { icon: 'fas fa-store', display: 'Shop', code: 'extension_order' },
    extension_profile: { icon: 'fas fa-user', display: 'Trang cá nhân', code: 'extension_profile' }
  }
  extensionType = "custom"
  async ngOnInit() {
    await this.reload()
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(local: boolean = true) {
    this.showLoading()
    this.detectChanges()
    const { app } = this.chatbotApi.chatbotAuth
    const pageId = (typeof(app.activePage)==='object') ? (app.activePage as iPage)._id : app.activePage as string
    const settings = await this.chatbotApi.setting.getList({
      local, query: {
        filter: {
          type: { $in: ['home_url', 'whitelisted_domains'] },
          page: pageId
        }
      }
    })
    this.setting = settings.find(s => s.type === "home_url")
    this.whilelistSetting = settings.find(s => s.type === "whitelisted_domains")
    if (this.setting) {
      if (/\/innoway\/profile/g.test(this.setting.option.url)) {
        this.extensionType = 'profile_innoway'
      } else if (/view\/v1\/innoway\?path=category/g.test(this.setting.option.url)) {
        this.extensionType = 'order_innoway'
      } else if (/view\/v1\/innoway\?path=basket/g.test(this.setting.option.url)) {
        this.extensionType = 'basket_innoway'
      } else if (/api\/v1\/deeplink\/gmap/g.test(this.setting.option.url)) {
        this.extensionType = 'google_map'
      } else if (/bot-static\.mcom\.app/g.test(this.setting.option.url)) {
        if (this.setting.option.url.includes("order")) this.extensionType = "extension_order";
        else if (this.setting.option.url.includes("profile")) this.extensionType = "extension_profile";
      }
    }
    this.hideLoading()
    this.detectChanges()
  }

  async addExtension(edit: boolean = false) {
    const selectData = {
      title: 'Chọn Loại Extension',
      confirmButtonText: 'Chọn',
      cancelButtonText: 'Quay lại',
      categories: [{
        name: "Căn bản",
        options: [this.extensions.custom, this.extensions.google_map, this.extensions.extension_order, this.extensions.extension_profile]
      }]
    } as iSelectionData
    const innowayPartner = this.chatbotApi.chatbotAuth.app.partners.find(p => p.partner === "innoway")
    if (innowayPartner) {
      selectData.categories.push({
        name: "Innoway",
        options: [this.extensions.order_innoway, this.extensions.basket_innoway, this.extensions.profile_innoway]
      } as any)
    }
    const type = await this.dialog.open(SelectionDialog, {
      width: '600px',
      data: selectData
    }).afterClosed().toPromise()
    let homeUrl = undefined
    switch (type) {
      case 'custom':
        homeUrl = await this.onCreateCustomExtension(edit)
        break
      case 'google_map':
        homeUrl = await this.onCreateGmapExtension(edit)
        break
      case 'order_innoway':
        homeUrl = await this.chatbotApi.app.getInnowayDeepLink({ path: "category" })
        break
      case 'basket_innoway':
        homeUrl = await this.chatbotApi.app.getInnowayDeepLink({ path: "basket" })
        break
      case 'profile_innoway':
        homeUrl = await this.chatbotApi.app.getInnowayProfileLink()
        break
      case 'extension_order':
        homeUrl = `${environment.chatbot.staticHost}/order`
        break
      case 'extension_profile':
        homeUrl = `${environment.chatbot.staticHost}/profile`
        break
    }
    if (homeUrl) {
      this.extensionType = type
      if (edit && this.setting) {
        this.setting.option.url = homeUrl
        this.setting = await this.chatbotApi.setting.update(this.setting._id, this.setting, { reload: true })
      } else {
        this.setting = await this.chatbotApi.page.addSetting({
          type: "home_url",
          option: {
            "url": homeUrl,
            "webview_height_ratio": "tall",
            "webview_share_button": "show",
            "in_test": false
          }
        })
      }
    }
    this.detectChanges()
  }
  async getUrl(url: string = "") {
    const title = url == "" ? "Tạo tiện ích mở Website" : "Cập nhật tiện ích Website"
    const confirmText = url == "" ? "Tạo" : "Cập nhật"
    return await this.dialog.open(EditInfoDialog, {
      width: '600px',
      data: {
        title,
        confirmButtonText: confirmText,
        inputs: [{
          type: "text",
          required: true,
          isURL: true,
          title: "URL Website",
          property: "url",
          validation: (value) => {
            const url = new URL(value)
            if (url.protocol !== "https:") {
              return "Đường dẫn yêu cầu phải có SSL (https://)"
            }
          }
        }]
      } as iEditInfoDialogData
    }).afterClosed().toPromise()
  }
  getQueryString(field, url) {
    let reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    let string = reg.exec(url);
    return string ? string[1] : null;
  };
  async openMapDialog(location = "") {
    let data: iEditInfoDialogData = {
      title: "",
      confirmButtonText: "Xác nhận",
      inputs: [
        {
          title: "Tọa độ [Ví dụ: 10.785864,106.690374] (Vĩ độ, Kinh độ | latitude, longitude)",
          property: "location",
          current: (location) ? location : '10.785864, 106.690374',
          type: "text",
          required: true,
          isLocation: true,
        },
      ]
    };
    if (!location) {
      data.title = "Thêm nút bản đồ";
    } else {
      data.title = "Chỉnh sửa nút bản đồ";
    }
    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data
    });

    return await dialogRef.afterClosed().toPromise();
  }
  async onCreateCustomExtension(edit) {
    const result = await this.getUrl((edit && this.extensionType == 'custom') ? this.setting.option.url : "")
    if (result) {
      const url = new URL(result.url)
      if (this.whilelistSetting && this.whilelistSetting.option.findIndex(d => {
        const domain = new URL(d)
        return domain.host === url.host
      }) === -1) {
        const confirm = await this.alert.question("Chú ý", "Tên miền này chưa có trong danh sách trắng.\nBạn có muốn thêm vô danh sách trắng không?", "Có", "Không")
        if (confirm) {
          this.whilelistSetting.option.push(url.origin)
          this.whilelistSetting = await this.chatbotApi.setting.update(this.whilelistSetting._id, this.whilelistSetting, { reload: true })
        }
        this.detectChanges()
      }
      return url.href
    }
  }
  async onCreateGmapExtension(edit) {
    let location = undefined
    if (edit && this.extensionType == 'google_map') {
      const tlat = this.getQueryString('tlat', this.setting.option.url)
      const tlng = this.getQueryString('tlng', this.setting.option.url)
      location = `${tlat}, ${tlng}`
    }
    const result = await this.openMapDialog(location)
    if (result) {
      if (result.location != location) {
        const locationArray = result.location.split(',');
        const lat = locationArray[0].trim();
        const long = locationArray[1].trim();
        let url;
        return await this.chatbotApi.deeplink.getGoogleMapDeeplink({
          to: {
            latitude: lat,
            longitude: long,
          }
        })
        this.detectChanges()
      }
    }
  }
}
