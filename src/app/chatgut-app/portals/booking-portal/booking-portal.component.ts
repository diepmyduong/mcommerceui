import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { iShop } from 'app/services/chatbot/api/crud/shop';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { PopoverService } from 'app/shared/popover-form/popover.service';

@Component({
  selector: 'app-booking-portal',
  templateUrl: './booking-portal.component.html',
  styleUrls: ['./booking-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookingPortalComponent extends BasePortal implements OnInit {

  componentName = "BookingPortalComponent"
  
  loading: boolean = false
  loadDone: boolean = false
  hasShopChanged: boolean = false

  shop: iShop

  hasCustomWorkDates: boolean = false
  duration: number
  durations = [15, 30, 60]

  weekDays = ['Chủ nhật', 'Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ bảy']

  constructor(
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef,
    public popoverService: PopoverService
  ) {
    super()
    this.setPortalName('Đặt lịch')
  }

  async ngOnInit() {
    await this.reload()
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async reload(local = true) {
    this.loadDone = false
    this.detectChanges()
    try {
      let shops

      let tasks = []
      tasks.push(this.chatbotApi.shop.getList({ local }).then(result => shops = result))
      await Promise.all(tasks)

      if (shops && shops.length) {
        this.shop = shops[0]
        this.duration = 60 - this.shop.bkDurations[this.shop.bkDurations.length - 1]
        this.hasCustomWorkDates = this.shop.bkDateOfWeek?true:false
        console.log(this.shop)
      }
    } catch (err) {
      this.alert.handleError(err)
    } finally {
      this.loadDone = true
      this.detectChanges()
    }
  }
  
  async saveContent() {
    try {
      this.showSaving()
      this.detectChanges()
      let tasks = []
      let { bkDurations, bkAmount, forwardBookingStory, bkDateOfWeek } = this.shop
      if (!this.hasCustomWorkDates) bkDateOfWeek = null
      tasks.push(this.chatbotApi.shop.update(this.shop._id, { bkDurations, bkAmount, forwardBookingStory, bkDateOfWeek }))
      await Promise.all(tasks)
      this.showSaveSuccess()
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lưu thông tin đặt lịch");
      this.showSaveButton()
    } finally {
      this.detectChanges()
    }
  }

  async editShopName(event, edit = false) {
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      target: edit?'.shop-name':'.btn-create',
      targetElement: event.target,
      width: '96%',
      confirm: edit?'Đổi tên cửa hàng':'Tạo cửa hàng',
      horizontalAlign: edit?'left':'center',
      verticalAlign: edit?'top':'bottom',
      fields: [
        {
          placeholder: 'Tên cửa hàng',
          property: 'name',
          constraints: ['required'],
          current: edit?this.shop.name:''
        }
      ],
      onSubmit: async (data) => {
        if (!data.name) return
        try {
          if (edit) {
            this.shop.name = (await this.chatbotApi.shop.update(this.shop._id, { name: data.name })).name
          } else {
            this.loadDone = false
            this.detectChanges()
            this.shop = (await this.chatbotApi.shop.add({ name: data.name }))
            this.loadDone = true
          }
        } catch (err) {
          console.error(err)
          if (edit) {
            this.alert.handleError(err, "Lỗi khi đổi tên cửa hàng")
          } else {
            this.alert.handleError(err, "Lỗi khi tạo cửa hàng")
          }
        } finally {
          this.detectChanges()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }
  
  trackByFn(index, item) {
    return item._id; // or item.id
  }

  shopChanged() {
    this.showSaveButton()
    this.hasShopChanged = true
    this.detectChanges()
    console.log(this.shop)
  }

  storyChanged(event) {
    this.shop.forwardBookingStory = event
    this.shopChanged()
  }

  durationChanged(event) {
    let durations = []
    let duration = 0
    do {
      durations.push(duration)
      duration += event
    } while (duration < 60)
    this.shop.bkDurations = durations
    this.shopChanged()
  }

  toggleCustomWorkDates() {
    this.hasCustomWorkDates = !this.hasCustomWorkDates
    if (this.hasCustomWorkDates && !this.shop.bkDateOfWeek) {
      let dateOfWeek = {}
      for (let i = 0; i < this.weekDays.length; i++) {
        dateOfWeek[i] = { start: "09:00", end: "20:00", off: false }
      }
      this.shop.bkDateOfWeek = dateOfWeek
    }
    this.shopChanged()
  }

  toggleWeekDay(index) {
    this.shop.bkDateOfWeek[index].off = !this.shop.bkDateOfWeek[index].off
    this.shopChanged()
  }
}
