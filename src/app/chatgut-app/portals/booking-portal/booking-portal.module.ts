import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MaterialModule } from 'app/shared';
import { BookingPortalComponent } from './booking-portal.component';
import { MatTooltipModule, MatProgressSpinnerModule, MatTabsModule, MatButtonModule, MatSelectModule, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { CardImageModule } from 'app/shared/cards/card-image/card-image.module';
import { FormsModule } from '@angular/forms';
import { AccountingPipeModule } from 'app/shared/pipes/accounting-pipe/accounting-pipe.module';
import { AppDateAdapter, APP_DATE_FORMATS } from 'app/shared/date-adapter';
import { StorySelectionModule } from 'app/components/story-selection/story-selection.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatTabsModule,
    CardImageModule,
    AccountingPipeModule,
    MatButtonModule,
    MatSelectModule,
    StorySelectionModule,
    FormsModule,
  ],
  declarations: [BookingPortalComponent],
  entryComponents: [BookingPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: BookingPortalComponent },
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
  ]
})
export class BookingPortalModule { }
