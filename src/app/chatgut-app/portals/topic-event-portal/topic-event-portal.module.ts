import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopicEventPortalComponent } from 'app/chatgut-app/portals/topic-event-portal/topic-event-portal.component';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatToolbarModule, MatButtonModule, MatProgressSpinnerModule, MatIconModule, MatProgressBarModule, MatFormFieldModule, MatInputModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatCardModule, MatDividerModule, MatTooltipModule } from '@angular/material';
import { MaterialTimeControlModule } from 'app/shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDividerModule,
    MaterialTimeControlModule,
    MatSelectModule,
    MatTooltipModule,
    MatCardModule,
    TextFieldModule,
    FlexLayoutModule,
    FormsModule
  ],
  declarations: [TopicEventPortalComponent],
  entryComponents: [TopicEventPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: TopicEventPortalComponent }
  ]
})
export class TopicEventPortalModule { }
