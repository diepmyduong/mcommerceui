import * as moment from 'moment'
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { NgForm } from '@angular/forms';
import { ITime } from 'app/shared/material-time/time-control';
import { iTopicEvent, iTopicEventStatus } from 'app/services/chatbot/api/crud/topicEvent';
import { iStory } from 'app/services/chatbot/api/crud/story';
@Component({
  selector: 'app-topic-event-portal',
  templateUrl: './topic-event-portal.component.html',
  styleUrls: ['./topic-event-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicEventPortalComponent extends BasePortal implements OnInit {

  @Input() eventId: string
  @Input() topicId: string
  @Input() mode: 'add' | 'detail' = 'detail'
  @Output() onEventCreated = new EventEmitter<iTopicEvent>()
  @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
  constructor(
    public changeRef: ChangeDetectorRef
  ) {
    super()
  }
  componentName = "TopicEventPortalComponent"
  event: iTopicEvent
  eventStatus: iTopicEventStatus
  stories: iStory[]
  selectedStory: iStory
  eventScheduleData: any = {}
  loadDone: boolean = false
  subName = "Sự kiện"
  error: {
    date?: boolean
  } = {}
  createdEvent: iTopicEvent = undefined
  cronConfig = {
    options: {
      allowMonth: false,
      allowYear: false,
      allowMinute: false,
      allowHour: false,
      allowDay: true,
      allowWeek: true
    }
  }
  weekOptions = [
    { display: "Thứ hai", value: "1" },
    { display: "Thứ ba", value: "2" },
    { display: "Thứ tư", value: "3" },
    { display: "Thứ năm", value: "4" },
    { display: "Thứ sáu", value: "5" },
    { display: "Thứ bảy", value: "6" },
    { display: "Thứ chủ nhật", value: "0" }
  ]
  toggling: boolean = false

  async ngOnInit() {
    this.setPortalName(this.event.name)
    await this.reload()
    // if (this.eventStatus && !this.event.jobId) {
    //   this.cardFrm.form.disable()
    // }
    setTimeout(() => {
      this.subscriptions.formChanges = this.cardFrm.valueChanges.subscribe(change => {
        this.eventChanged()
        this.detectChanges()
      })
    }, 200)
  }

  async ngAfterViewInit() {
  }

  async eventChanged() {
    if (!this.isSaving && this.loadDone && !this.toggling) {
      this.showSaveButton();
      this.detectChanges()
    }
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
    if (this.subscriptions.formChanges) {
      this.subscriptions.formChanges.unsubscribe()
    }
  }

  async reload(local: boolean = true) {
    try {
      this.loadDone = false
      this.detectChanges()
      this.stories = await this.chatbotApi.story.getList({
        local,
        query: {
          limit: 0,
          fields: ["_id", "name"],
          order: { 'createdAt': 1 },
          filter: { mode: 'normal' }
        }
      })
      const [event, eventStatus] = await Promise.all([
        this.chatbotApi.topicEvent.getItem(this.eventId, { local }),
        this.chatbotApi.topicEvent.getStatus(this.eventId).catch(err => {
          return {
            failed: true,
            failReason: "Event Stopped"
          } as any
        })
      ])
      this.event = event
      this.eventStatus = eventStatus
      if (this.event.eventData.type == 'story') {
        this.selectedStory = this.stories.find(s => s._id === this.event.eventData.story)
      }
      const scheduledAt = moment(this.event.eventSchedule).locale('en')
      this.eventScheduleData = {
        scheduledDate: scheduledAt.toDate(),
        scheduledTime: {
          hour: scheduledAt.hour(),
          minute: scheduledAt.minute(),
          meriden: scheduledAt.format('A')
        } as ITime
      }
      this.eventScheduleData.endTime = {
        hour: moment().hour(),
        minute: moment().minute(),
        meriden: moment().format('A')
      } as ITime
      if (this.event.endDate) {
        const endAt = moment(this.event.endDate)
        this.eventScheduleData.endDate = endAt.toDate()
        this.eventScheduleData.endTime = {
          hour: endAt.hour(),
          minute: endAt.minute(),
          meriden: endAt.format('A')
        } as ITime
      }
      if (this.event.eventRepeatEvery) {
        const cron = this.event.eventRepeatEvery.split(' ')
        if (cron[4]) this.event.weeks = cron[4].split(',')
      }
    } catch (err) {
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi lấy dữ liệu", err.message)
    } finally {
      this.loadDone = true;
      this.detectChanges()
    }
  }

  async onSetTime(mode: string, event: ITime) {

  }

  async addEvent(formCtrl: NgForm) {
    if (!this.validEvent()) return
    try {
      this.showLoading()
      this.detectChanges()
      this.createdEvent = await this.chatbotApi.topic.addEvent(this.topicId, this.event)
      this.close()
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi tạo mới");
      // this.alert.error('Không thể tạo mới', err.message)
    } finally {
      this.hideLoading()
      this.detectChanges()
    }
  }

  async saveContent() {
    if (!this.validEvent()) return
    try {
      this.showSaving();
      this.cardFrm.form.disable()
      this.detectChanges()
      await this.chatbotApi.topicEvent.update(this.eventId, this.event)
      //await this.reload()
      this.cardFrm.form.enable()
      this.showSaveSuccess();
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi lưu");
      // this.alert.error('Không thể cập nhật', err.message)
      this.showSaveButton();
      this.detectChanges()
    }
  }

  validEvent() {
    const scheduledDate = moment(this.eventScheduleData.scheduledDate).locale('en')
    scheduledDate.hours(this.eventScheduleData.scheduledTime.hour)
    scheduledDate.minutes(this.eventScheduleData.scheduledTime.minute)
    this.event.eventSchedule = scheduledDate.toDate()
    if (this.eventScheduleData.endDate) {
      const endDate = moment(this.eventScheduleData.endDate).locale('en')
      endDate.hours(this.eventScheduleData.endTime.hour)
      endDate.minutes(this.eventScheduleData.endTime.minute)
      this.event.endDate = endDate.toDate()
    }
    this.detectChanges()
    return true
  }

  async close() {
    if (this.mode === "add") {
      this.onEventCreated.emit(this.createdEvent)
    }
    super.close()
    this.detectChanges()
  }

  async selectStory() {
    const storyId = await this.getStory(this.event.eventData.story)
    if (storyId != this.event.eventData.story) {
      this.selectedStory = this.stories.find(s => s._id === storyId)
      this.event.eventData.story = storyId
      if (!this.isSaving && this.loadDone && !this.toggling) {
        this.showSaveButton();
        this.detectChanges()
      }
    }
  }

  getStory(storyId?: string) {
    return new Promise(async (resolve, reject) => {
      const componentRef = await this.portalService.pushPortalAt(this.index + 1, "StoriesPortalComponent", {
        select: true, multi: false, selectedStories: storyId
      })
      if (!componentRef) {
        return;
      }
      const component = componentRef.instance as any
      this.subscriptions.onStorySaved = component.onStorySaved.subscribe(storyId => {
        resolve(storyId)
        component.hideSave();
        component.close();
      })
      this.subscriptions.onStoryClosed = component.onStoryClosed.subscribe(storyId => {
        component.onStorySaved.unsubscribe();
        component.onStoryClosed.unsubscribe();
      })
    })
  }

  async stopEvent() {
    this.toggling = true
    this.cardFrm.form.disable()
    this.detectChanges()
    await this.chatbotApi.topicEvent.stopEvent(this.eventId)
    await this.reload(false)
    this.cardFrm.form.enable()
    this.detectChanges()
    setTimeout(() => { 
      this.toggling = false
      this.detectChanges() }, 300);
  }

  async enableEvent() {
    this.toggling = true
    this.cardFrm.form.disable()
    this.detectChanges()
    await this.chatbotApi.topicEvent.continueEvent(this.eventId)
    await this.reload(false)
    this.cardFrm.form.enable()
    setTimeout(() => { this.toggling = false }, 300);
    this.detectChanges()
  }

  private _this = this
}
