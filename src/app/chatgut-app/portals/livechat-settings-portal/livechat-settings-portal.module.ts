import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatTooltipModule, MatProgressSpinnerModule, MatButtonModule, MatSelectModule, MatIconModule, MatToolbarModule, MatChipsModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { LiveChatSettingsPortalComponent } from './livechat-settings-portal.component';
import { MaterialModule } from 'app/shared';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatToolbarModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatButtonModule,
    MatSelectModule,
    MatSelectModule,
    MatChipsModule,
    DragDropModule
  ],
  declarations: [LiveChatSettingsPortalComponent],
  entryComponents: [LiveChatSettingsPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: LiveChatSettingsPortalComponent },
  ]
})
export class LiveChatSettingsPortalModule { }
