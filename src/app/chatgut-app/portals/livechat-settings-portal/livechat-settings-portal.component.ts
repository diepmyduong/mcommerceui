import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { MatDialog } from '@angular/material';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { BehaviorSubject, Subject } from 'rxjs';
import { skip, takeUntil } from 'rxjs/operators';
import { iSubscriberTag } from 'app/services/chatbot';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { cloneDeep } from 'lodash-es'
import { environment } from '@environments';

@Component({
  selector: 'app-livechat-settings-portal',
  templateUrl: './livechat-settings-portal.component.html',
  styleUrls: ['./livechat-settings-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LiveChatSettingsPortalComponent extends BasePortal implements OnInit {

  componentName = "LiveChatSettingsPortalComponent"

  tabs = [
    { code: 'tag', display: 'Tag' },
    { code: 'property', display: 'Thuộc tính' }
  ]

  colors = [
    { code: '#29C5FF', display: 'Xanh lơ' },
    { code: '#0287D0', display: 'Xanh dương' },
    { code: '#004790', display: 'Xanh biển' },
    { code: '#fa8072', display: 'Đỏ cá hồi' },
    { code: '#f24b3a', display: 'Đỏ tươi' },
    { code: '#B71C0C', display: 'Đỏ lựu' },
    { code: '#3EDC81', display: 'Xanh lá cây' },
    { code: '#30b848', display: 'Xanh lục' },
    { code: '#006C11', display: 'Xanh quân đội' },
    { code: '#F9BF3B', display: 'Cam cà rốt' },
    { code: '#F1892D', display: 'Cam quả cam' },
    { code: '#C86400', display: 'Cam bí ngô' },
    { code: '#AB69C6', display: 'Tím nho' },
    { code: '#7E349D', display: 'Tím hoàng gia' },
    { code: '#4E046D', display: 'Tím mực' },
    { code: '#FFA27B', display: 'Da người nhạt' },
    { code: '#E3724B', display: 'Da người trung' },
    { code: '#B3421B', display: 'Da người đậm' },
    { code: '#47EBE0', display: 'Ngọc trời' },
    { code: '#17BBB0', display: 'Ngọc bích' },
    { code: '#008B80', display: 'Ngọc lam' },
    { code: '#FF8CC8', display: 'Hồng phấn' },
    { code: '#EA4C88', display: 'Hồng tươi' },
    { code: '#BA1C58', display: 'Hồng nhung' },
    { code: '#EAB897', display: 'Nâu be' },
    { code: '#AE7C5B', display: 'Nâu trầm' },
    { code: '#8E5C3B', display: 'Nâu đất' },
    { code: '#ACBAC9', display: 'Màu bạc' },
    { code: '#8C9AA9', display: 'Màu sắt' },
    { code: '#3C4A59', display: 'Màu bão' },
  ]

  propertyTypes = [
    { code: 'text', display: 'Chữ' },
    { code: 'number', display: 'Số' },
    { code: 'boolean', display: 'Bật/Tắt' },
    { code: 'options', display: 'Lựa chọn' },
    { code: 'date', display: 'Ngày' },
    { code: 'tel', display: 'Số điện thoại' },
    { code: 'email', display: 'Email' },
  ]
  propertyTypesObj = {}

  destroy = new Subject<boolean>();
  currentTab = new BehaviorSubject<string>(undefined)
  loadDone = new BehaviorSubject<boolean>(false)
  subscriberTags = new BehaviorSubject<iSubscriberTag[]>(undefined)
  subscriberProperties = new BehaviorSubject<any[]>(undefined)
  subscriberPropertyId: string
  loading: boolean = false
  url: string

  @Output() onClosed = new EventEmitter<any>()

  constructor(
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef,
    public popoverService: PopoverService
  ) {
    super()
    this.setPortalName('Cài đặt Live Chat');
    this.propertyTypesObj = this.propertyTypes.reduce((obj, item) => { obj[item.code] = item.display; return obj }, {})
    this.url = `${environment.chatbot.livechatHost}?token=${this.chatbotApi.chatbotAuth.appToken}`
  }

  async ngOnInit() {
    // await this.reload()

    this.currentTab.pipe(
      takeUntil(this.destroy),
    ).subscribe(async tab => {
      if (this.subscriptions.subscriberProperties) this.subscriptions.subscriberProperties.unsubscribe()
      this.hideSave()
      switch(tab) {
        case 'tag':
          this.getSubscriberTags()
          break
        case 'property':
          await this.getSubscriberProperties()

          this.subscriptions.subscriberProperties = this.subscriberProperties.pipe(
            skip(1),
            takeUntil(this.destroy)
          ).subscribe(x => {
            this.showSaveButton()
          })
          break
      }
      return tab
    })
  }

  async ngAfterViewInit() {
    this.changeTab('tag')
  }

  async ngOnDestroy() {
    this.destroy.next(true)
    super.ngOnDestroy()
    this.onClosed.emit(true)
  }

  async getSubscriberTags(local = true) {
    try {
      this.loadDone.next(false)
      let subscriberTags = await this.chatbotApi.subscriberTag.getList({
        local, query: { order: { position: 1 } }
      })
      this.subscriberTags.next(subscriberTags)
      console.log(this.subscriberTags.getValue())
    } catch (err) {
      console.error(err)
      this.alert.handleError(err, 'Lỗi khi lấy tag người dùng')
    } finally {
      this.loadDone.next(true)
      this.changeRef.detectChanges()
    }
  }

  async getSubscriberProperties(local = true) {
    try {
      this.loadDone.next(false)
      let subscriberProperty = (await this.chatbotApi.subscriberProperty.getList({
        local
      }))[0]
      if (subscriberProperty) {
        this.subscriberPropertyId = subscriberProperty._id
        this.subscriberProperties.next(cloneDeep(subscriberProperty.properties))
      } else {
        this.subscriberProperties.next([])
      }
      console.log(this.subscriberProperties.getValue())
    } catch (err) {
      console.error(err)
      this.alert.handleError(err, 'Lỗi khi lấy thuộc tính người dùng')
    } finally {
      this.loadDone.next(true)
    }
  }
  
  async openTag(event, tag: iSubscriberTag = null) {
    let usedColors = this.subscriberTags.getValue().map(x => x.color)
    let availableColors = this.colors.filter(x => (tag && tag.color == x.code) || !usedColors.includes(x.code))
    if (!availableColors.length) {
      this.alert.info('Quá số lượng tag', 'Không cho phép tạo thêm tag, hãy xóa tag để tạo mới.')
      return
    }

    let formOptions: iPopoverFormOptions = {
      parent: '.portal',
      target: '.tag',
      targetElement: event.target,
      width: '94%',
      confirm: tag?'Chỉnh sửa tag':'Tạo tag',
      horizontalAlign: 'center',
      verticalAlign: 'center',
      fields: [
        {
          placeholder: 'Tên tag',
          property: 'name',
          col: 'col-6',
          constraints: ['required'],
          current: tag?tag.name:''
        }, {
          placeholder: 'Màu sắc',
          property: 'color',
          type: 'select',
          extra: 'color',
          col: 'col-6',
          constraints: ['required'],
          options: availableColors,
          current: tag?tag.color:availableColors[0].code
        }, {
          placeholder: 'Câu chuyện gắn tag',
          property: 'story',
          type: 'story',
          col: 'col-12',
          current: tag?tag.story:''
        }, {
          placeholder: 'Câu chuyện gỡ tag',
          property: 'untagStory',
          type: 'story',
          col: 'col-12',
          current: tag?tag.untagStory:''
        }
      ],
      onSubmit: async (data) => {
        if (!data.name) return
        this.loading = true
        this.detectChanges()
        let { name, color, story, untagStory } = data
        let tags = this.subscriberTags.getValue()
        let backupTag
        try {
          if (tag) {
            let index = tags.findIndex(x => x._id == tag._id)
            backupTag = {...tags[index] }
            tags[index] = {...tags[index], name, color, story, untagStory}
            this.subscriberTags.next([...tags])
            await this.chatbotApi.subscriberTag.update(tag._id, { name, color, story: story || null, untagStory: untagStory || null })          
          } else {
            backupTag = { name, color, position: tags.length, story: story || null, untagStory: untagStory || null }
            this.subscriberTags.next([...tags, backupTag])
            let result = await this.chatbotApi.subscriberTag.add(backupTag)
            backupTag._id = result._id
          }
        } catch (err) {
          console.error(err)
          this.alert.handleError(err, tag?'Chỉnh sửa tag thất bại':'Tạo tag thất bại');
          if (tag) {
            let index = tags.findIndex(x => x._id == backupTag._id)
            tags[index] = {...tags[index], ...backupTag}
          } else {
            let index = tags.findIndex(x => x === backupTag)
            tags.splice(index, 1)
          }
          this.subscriberTags.next([...tags])
        } finally {
          this.loading = false
          this.detectChanges()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  async deleteTag(tag) {
    if (!await this.alert.warning('Xóa tag', `Bạn có muốn xóa tag "${tag.name}"?`)) return

    this.loading = true
    this.detectChanges()
    let tags = this.subscriberTags.getValue()     
    let index = tags.findIndex(x => x._id == tag._id)
    let backupTag
    try {
      backupTag = {...tags[index]}
      tags.splice(index, 1)
      this.subscriberTags.next([...tags])
      await this.chatbotApi.subscriberTag.delete(tag._id)
    } catch (err) {
      console.error(err)
      tags.splice(index, 0, backupTag)
      this.subscriberTags.next([...tags])
      this.alert.handleError(err, 'Xóa tag thất bại');
    } finally {
      this.loading = false
      this.detectChanges()
    }
  }

  async dropTag(event: CdkDragDrop<any[]>) {
    let tags = this.subscriberTags.getValue()     
    moveItemInArray(tags, event.previousIndex, event.currentIndex)
    this.subscriberTags.next([...tags])
    let tasks = []
    for (let i = 0; i < tags.length; i++) {
      let tag = tags[i]
      if (i != tag.position) {
        tag.position = i
        tasks.push(this.chatbotApi.subscriberTag.update(tag._id, { position: i }))
      }
    }
    await Promise.all(tasks)
  }

  async changeTab(tab) {
    if (this.isSaving) return

    this.currentTab.next(tab)
    this.hideSave()
  }

  inputChanged(property = null) {
    this.showSaveButton()
    if (property) {
      if (property.type == 'tel') {
        property.key = 'DienThoai'
      } else if (property.type == 'email') {
        property.key = 'Email'
      }
      
    }
  }
  
  async addNewProperty() {
    let subscriberProperties = this.subscriberProperties.getValue()
    subscriberProperties.push({
      display: '',
      key: '',
      type: 'text'
    })
    this.subscriberProperties.next(subscriberProperties)
  }

  async deleteProperty(index) {
    let subscriberProperties = this.subscriberProperties.getValue()
    subscriberProperties.splice(index, 1)
    this.subscriberProperties.next(subscriberProperties)
  }

  async saveContent() {
    switch(this.currentTab.getValue()) {
      case 'property': {

        let subscriberProperties = this.subscriberProperties.getValue()
        for (let property of subscriberProperties) {
          if (!property.display || !property.key) {
            this.alert.info('Thiếu tên hiển thị hoặc biến giá trị', 'Yêu cầu nhập đầy đủ tên hiển thị và biến giá trị')
            return
          }
          if (property.key.trim().indexOf(' ') > -1) {
            this.alert.info('Dữ liệu sai format', 'Yêu cầu biến giá trị không được có khoảng trắng')
            return
          }
        }

        try {
          this.showSaving()
          if (this.subscriberPropertyId) {
            await this.chatbotApi.subscriberProperty.update(this.subscriberPropertyId, { properties: this.subscriberProperties.getValue() })
          } else {
            let result = await this.chatbotApi.subscriberProperty.add({ properties: this.subscriberProperties.getValue() })
            this.subscriberPropertyId = result._id
          }
        } catch (err) {
          console.error(err)
          this.alert.handleError(err, 'Lưu thuộc tính thất bại')
        } finally {
          this.showSaveSuccess()
        }
        break
      }
    } 
  }

  removeOption(property, index) {
    property.options.splice(index, 1)
    console.log(property, index)
    this.showSaveButton()
  }

  openOption(event, property, option = '') {
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      target: '.mat-chip',
      targetElement: event.target,
      width: '250px',
      confirm: option?'Đổi tên lựa chọn':'Thêm lựa chọn',
      horizontalAlign: 'center',
      verticalAlign: 'bottom',
      fields: [
        {
          placeholder: 'Lựa chọn',
          property: 'name',
          constraints: ['required'],
          current: option || ''
        }
      ],
      onSubmit: async (data) => {
        if (!data.name) return
        if (!option) {
          if (!property.options) property.options = []
          property.options.push(data.name)
        } else {
          let index = property.options.findIndex(x => x === option)
          if (index > -1 ) {
            property.options[index] = data.name
          }
        }
        this.showSaveButton()
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }
  
  trackByFn(index, item) {
    return item._id; // or item.id
  }

  moveUp(index) {
    let subscriberProperties = this.subscriberProperties.getValue()
    if (index == 0) return 

    moveItemInArray(subscriberProperties, index, index - 1)
    this.subscriberProperties.next(subscriberProperties)
  }

  moveDown(index) {
    let subscriberProperties = this.subscriberProperties.getValue()
    if (index == subscriberProperties.length - 1) return 

    moveItemInArray(subscriberProperties, index, index + 1)
    this.subscriberProperties.next(subscriberProperties)
  }
}