import * as moment from 'moment'
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal'
import { ITime } from 'app/shared/material-time/time-control';
import { iPage } from 'app/services/chatbot/api/crud/page';
import { DynamicComponentLoaderService } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';

@Component({
  selector: 'app-statistics-portal',
  templateUrl: './statistics-portal.component.html',
  styleUrls: ['./statistics-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatisticsPortalComponent extends BasePortal implements OnInit {
  page: iPage
  pageId: any
  componentName = "StatisticsPortalComponent"

  fromDate: moment.Moment
  toDate: moment.Moment

  startTime: ITime
  endTime: ITime

  statistics_filter: any
  settingsMenu: any[] = []

  mode: string = ""
  loadDone: boolean = false

  constructor(
    private dynamicLoader: DynamicComponentLoaderService,
    public changeRef: ChangeDetectorRef
  ) {
    super()
    this.setPortalName("Thống kê")

  }
  async ngOnInit() {
    // this.fromDate = moment(this.chatbotApi.chatbotAuth.app.createdAt)
    // this.toDate = moment()
    // this.startTime = {
    //   hour: moment().startOf('day').get('hour'),
    //   minute: moment().startOf('day').get('minute'),
    //   format: 24
    // } as ITime  
    // this.endTime = {
    //   hour: moment().endOf('day').get('hour'),
    //   minute: moment().endOf('day').get('minute'),
    //   format: 24
    // } as ITime

    // this.setupFilter()
    // this.setupMenu()
    setTimeout(async () => {
      this.reload()
    }, 10)
  }

  async ngOnDestroy() {
    super.ngOnDestroy()
  }

  async ngAfterViewInit() {    
    this.dynamicLoader.getComponentFactory('StatisticsResultPortalComponent')
  }

  async setupFilter() {
    this.statistics_filter = {
    
      interaction: {
        start_date: this.fromDate.toDate(),
        end_date: this.toDate.toDate(),
        start_time: this.startTime,
        end_time: this.endTime,
        activity: true,
        message: true
      },
      subscriber: {
        analytic: true,
        activity: true,
        subscribeByDay:{
          start_date: this.fromDate.toDate(),
          end_date: this.toDate.toDate(),
          start_time: this.startTime,
          end_time: this.endTime,
        },
        genderInteraction:{
          start_date: this.fromDate.toDate(),
          end_date: this.toDate.toDate(),
          start_time: this.startTime,
          end_time: this.endTime,
          subscribe: true,
          block: false
        },
        subscriber_interact:{
          start_date: this.fromDate.toDate(),
          end_date: this.toDate.toDate(),
          start_time: this.startTime,
          end_time: this.endTime,
        }
      
      },
      story: {
        story:{
          start_date: this.fromDate.toDate(),
          end_date: this.toDate.toDate(),
          start_time: this.startTime,
          end_time: this.endTime,
        },       
        storyInteraction: {
          start_date: this.fromDate.toDate(),
          end_date: this.toDate.toDate(),
          start_time: this.startTime,
          end_time: this.endTime,
        },
        unique_subscriber_receive_story: {
          start_date: this.fromDate.toDate(),
          end_date: this.toDate.toDate(),
          start_time: this.startTime,
          end_time: this.endTime,
        },
      },
      topic: {
        start_date: this.fromDate.toDate(),
        end_date: this.toDate.toDate(),
        start_time: this.startTime,
        end_time: this.endTime,
      },
      post: {
        start_date: this.fromDate.toDate(),
        end_date: this.toDate.toDate(),
        start_time: this.startTime,
        end_time: this.endTime,
        subscriber: true
      },
      reactPost: {
      },
      referral: {
        start_date: this.fromDate.toDate(),
        end_date: this.toDate.toDate(),
        start_time: this.startTime,
        end_time: this.endTime,
        subscriber: true
      },
      button: {
        start_date: this.fromDate.toDate(),
        end_date: this.toDate.toDate(),
        start_time: this.startTime,
        end_time: this.endTime,
        subscriber: true
      },
      storySent: {
        start_date: this.fromDate.toDate(),
        end_date: this.toDate.toDate(),
        start_time: this.startTime,
        end_time: this.endTime,
      },
      // subscriber_interact: {
      //   start_date: this.fromDate.toDate(),
      //   end_date: this.toDate.toDate(),
      //   start_time: this.startTime,
      //   end_time: this.endTime,
      // },
      
    }
  }

  async setupMenu() {
    this.settingsMenu = [
    //   {
    //   code: "subscriber_interact",
    //   display: "Tương tác người dùng",
    //   icon: "fas fa-users",

    // },
    // {
    //   code: "subscriberInteraction",
    //   display: "Lượng quan tâm",
    //   icon: "far fa-eye",
    // }, 
    // {
    //   code: "genderInteraction",
    //   display: "Tương tác theo giới tính",
    //   icon: "fas fa-address-book",
    // }, 
    // {
    //   code: "interaction",
    //   display: "Lượng tương tác",
    //   icon: "far fa-dot-circle",
    // }, 
    {
      code: "subscriber",
      display: "Người dùng",
      icon: "fas fa-users",
    },
      // {
      //   code: "storyInteraction",
      //   display: "Tương tác câu chuyện",
      //   icon: "fas fa-book-reader",
      // },
      // {
      //   code: "unique_subscriber_receive_story",
      //   display: "Tổng người nhận câu chuyện hàng ngày",
      //   icon: "fas fa-user",
      // },
     
      {
        code: "story",
        display: "Câu chuyện",
        icon: "fas fa-book",
      }, {
        code: "topic",
        display: "Chủ đề",
        icon: "fas fa-columns",
      }, {
        code: "post",
        display: "Bài viết",
        icon: "fas fa-newspaper",
      }, {
        code: "reactPost",
        display: "Tương tác bài viết",
        icon: "far fa-laugh-beam",
      }, {
        code: "referral",
        display: "Đường dẫn",
        icon: "fas fa-hand-point-right",
      },
      {
        code: "button",
        display: "Tag nút bấm",
        icon: "fas fa-tag",
      }
    ]
  }

  async reload(local: boolean = false) {
    this.fromDate = moment().subtract(7,'days')
    this.toDate = moment()
    this.startTime = {
      hour: moment().startOf('day').get('hour'),
      minute: moment().startOf('day').get('minute'),
      format: 24
    } as ITime
    this.endTime = {
      hour: moment().endOf('day').get('hour'),
      minute: moment().endOf('day').get('minute'),
      format: 24
    } as ITime

    let tasks = []
    tasks.push(this.setupFilter())
    tasks.push(this.setupMenu())
    await Promise.all(tasks)

    this.loadDone = true
    this.changeRef.detectChanges()
  }

  async openStatistics(code, display) {
    if (this.portalService.container.getPortalNameAt(this.index + 1) != "StatisticsResultPortalComponent") {
      await this.portalService.pushPortal('StatisticsResultPortalComponent', { 
        portalName: display,
        mode: code
      })
    }
    // const component = this.portalService.container.portals[this.index + 1]
    // component.getStatisticResult(code, display, this.statistics_filter)
  }

  async openSettings(code) {
    if (this.mode == code) {
      this.mode = ""
      return;
    } else {
      this.mode = code;
    }
  }
}
