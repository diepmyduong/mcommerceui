import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { StatisticsPortalComponent } from 'app/chatgut-app/portals/statistics-portal/statistics-portal.component';
import { MatToolbarModule, MatIconModule, MatProgressBarModule, MatListModule, MatLineModule, MatInputModule, MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatCheckboxModule, MatDividerModule, MatButtonModule, MatTooltip, MatRadioModule } from '@angular/material';
import { MaterialTimeControlModule } from 'app/shared';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatProgressBarModule,
    MatListModule,
    MatLineModule,
    MatInputModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MaterialTimeControlModule,
    MatCheckboxModule,
    MatDividerModule,
    MatButtonModule,
    FormsModule,
    MatRadioModule
  ],
  declarations: [StatisticsPortalComponent],
  entryComponents: [StatisticsPortalComponent],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: StatisticsPortalComponent}
  ]
})
export class StatisticsPortalModule { }
