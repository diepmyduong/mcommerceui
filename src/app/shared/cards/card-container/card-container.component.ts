import { merge } from 'lodash-es'
import * as guid from 'guid';
declare const $: any;
import { Component, OnInit, OnDestroy, ComponentFactoryResolver, ViewContainerRef, ViewChild, ElementRef, Input, ViewRef, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject } from 'rxjs'
import { iCard, ChatbotApiService } from 'app/services/chatbot'
import { BasePortal } from 'app/chatgut-app/portals/portal-container/base-portal';
import { PortalContainerComponent } from 'app/chatgut-app/portals/portal-container/portal-container.component';
import { DynamicComponentLoaderService } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { BaseCard } from 'app/shared/cards/base-card';
import { StoryDetailPortalComponent } from 'app/chatgut-app/portals/story-detail-portal/story-detail-portal.component';
import { moveItemInArray } from '@angular/cdk/drag-drop';
export interface iOnCardsChange {
  status: string,
  data: any
}
@Component({
  selector: 'app-card-container',
  templateUrl: './card-container.component.html',
  styleUrls: ['./card-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardContainerComponent implements OnInit, OnDestroy {

  @Input() haveTooltip: boolean = false
  @Input() container: PortalContainerComponent
  @Input() parentViewRef: ViewRef
  @Input() cards: iCard[]
  @Input() parentPortal: BasePortal
  @Output() change = new EventEmitter<iOnCardsChange>()
  @ViewChild("cardViewContainer", { read: ViewContainerRef }) cardViewContainer: ViewContainerRef
  @ViewChild("cardListRef", { read: ElementRef }) cardListRef: ElementRef
  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    public chatbotApi: ChatbotApiService,
    private dynamicComponentLoader: DynamicComponentLoaderService,
    public changeRef: ChangeDetectorRef
  ) {
  }
  id: string = guid.raw();
  cardComps: BaseCard[] = [];
  cardInited: BehaviorSubject<number> = new BehaviorSubject<number>(-1)
  deleteList: string[] = []

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  async pushCardComp(cardComp: any, params: any = {}, scroll: boolean = true, pos = -1) {
    let componentFactory;
    if (typeof cardComp == "string") {
      componentFactory = await this.dynamicComponentLoader.getComponentFactory(cardComp);
    } else {
      componentFactory = this.componentFactoryResolver.resolveComponentFactory(cardComp);
    }
    let componentRef = componentFactory.create(this.cardViewContainer.parentInjector)
    let newIndex
    if (pos == -1) {
      newIndex = this.cardComps.length
    } else {
      newIndex = pos
    }
    let viewRef = this.cardViewContainer.insert(componentRef.hostView, newIndex)
    let component: BaseCard = componentRef.instance as BaseCard
    component.viewRef = viewRef
    component.viewInited = this.cardInited
    component.cardContainerId = this.id
    merge(component, params)
    if (pos == -1) {
      component.index = newIndex
      this.cardComps.push(component)
    } else {
      this.cardComps.splice(pos, 0, component)
      this.updateIndex()
    }
    this.changeRef.detectChanges();

    setTimeout(() => {
      let template = document.getElementById(this.id + '-' + component.cardRefId)
      if (template) template.appendChild(componentRef.location.nativeElement)
    })
    if (scroll) {
      setTimeout(() => { document.getElementsByClassName(component.cardRefId).item(0).scrollIntoView({ block: 'center', behavior: 'smooth' }) }, 100)
    };
    return componentRef
  }

  scrollToTop(): void {
    try {
      $(this.cardListRef.nativeElement)
        .closest('.portal-content-block')
        .animate({
          scrollTop: 0
        }, 500)
    } catch (err) { }
  }

  scrollToBottom(): void {
    try {
      $(this.cardListRef.nativeElement)
        .closest('.portal-content-block')
        .animate({
          scrollTop: this.cardListRef.nativeElement.scrollHeight
        }, 500)
    } catch (err) { }
  }

  async removeCard(id, index) {
    if (id) {
      this.deleteList.push(id)
    }
    this.cardComps.splice(index, 1)
    this.cards.splice(index, 1)
    this.updateIndex()
    this.change.emit()
  }

  async saveAllCards() {

    let storyPortal: any = this.parentPortal;
    let cardIds = []
    const allTask = []
    const getIdTask = []

    //DELETE ALL CARDS IN DELETE LIST
    for (let id of this.deleteList) {
      if (id) {
        allTask.push(this.chatbotApi.card.delete(id))
      }
    }
    this.deleteList = [];

    for (let i = 0; i < this.cardComps.length; i++) {
      let cardCmp = this.cardComps[i];
      cardCmp.allowCanBeSaved = false;
      //ADD NEW CARD
      if (cardCmp.card && !cardCmp.card._id && cardCmp.operation == 'add') {
        getIdTask.push(this.chatbotApi.story.addCard(storyPortal.storyId, cardCmp.card).then(c => {
          cardCmp.card = c
          return c
        }))
        continue;
      }

      //SAVE CARD THAT CAN BE SAVED
      if (cardCmp.canBeSaved.getValue() || i == this.cardComps.length - 1) {
        allTask.push(cardCmp.saveContent())
        continue;
      }
    }
    //WAIT FOR ALL PROMISE
    await Promise.all(getIdTask)

    //GET ALL THE CARD ID FOR ITS CURRENT ORDER
    for (let cardCmp of this.cardComps) {
      cardIds.push(cardCmp.card._id)
      cardCmp.operation = ''
    }

    //REORDER CARDS
    this.cards = cardIds;
    allTask.push(this.chatbotApi.story.updateOrder(storyPortal.storyId, cardIds))
    await Promise.all(allTask);
    for (let i = 0; i < this.cardComps.length; i++) {
      this.cardComps[i].disableSaveDetection();
    }


  }

  // async duplicateCard(index) {
  //   const storyPortal: any = this.parentPortal;
  //   const cardComp = this.cardComps[index]
  //   storyPortal.addCard(cardComp.card.type, cardComp.card.option, true, index + 1,
  //     cardComp.card.condition, cardComp.card.isBreak)
  // }

  async drop(event) {
    console.log( 'event drop', event)
    if(event.item.data && event.item.data.isPopularCard){
      return (this.parentPortal as StoryDetailPortalComponent).addCard(event.item.data.code, {}, true, event.currentIndex)
    }
    if (event.previousContainer === event.container) {
      if (event.previousIndex == event.currentIndex) return
      this.moveCard(event.previousIndex, event.currentIndex)
    } else {
      if (!event.previousContainer || !event.container) return
      let previousCardComp = (event.previousContainer.data as BaseCard[])[event.previousIndex]
      let currentCardComp = (event.container.data as BaseCard[])[0]
      if (currentCardComp && !currentCardComp.draggable) return
      let card = previousCardComp.card;
      (this.parentPortal as StoryDetailPortalComponent).addCard(card.type, card.option, true, event.currentIndex, card.condition, card.isBreak)
    }
    this.change.emit()
  }

  async moveCard(previousIndex, currentIndex) {
    moveItemInArray(this.cards, previousIndex, currentIndex);
    moveItemInArray(this.cardComps, previousIndex, currentIndex);
    this.change.emit()
    this.updateIndex()
    setTimeout(() => { document.getElementsByClassName(this.cardComps[currentIndex].cardRefId).item(0).scrollIntoView({ block: 'center', behavior: 'smooth' }) }, 100)
  }

  trackByFn(index, item) {
    return item.cardRefId; // or item.id
  }

  updateIndex() {
    for (let i = 0; i < this.cardComps.length; i++) {
      let card = this.cardComps[i] as BaseCard
      card.index = i
      if (card.cardSidebar) {
        card.cardSidebar.first = (i==0)?true:false
        card.cardSidebar.last = (i==this.cardComps.length - 1)?true:false
        card.cardSidebar.ref.detectChanges()
      }
    }
  }

}
