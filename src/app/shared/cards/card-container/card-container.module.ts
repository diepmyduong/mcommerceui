import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardContainerComponent } from './card-container.component';
import { MatListModule } from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  imports: [
    CommonModule,
    MatListModule,
    DragDropModule,
  ],
  declarations: [CardContainerComponent],
  exports: [CardContainerComponent]
})
export class CardContainerModule { }
