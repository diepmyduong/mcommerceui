import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardAdvancedComponent } from 'app/shared/cards/card-advanced/card-advanced.component';
import { MatCheckboxModule, MatTooltipModule, MatFormFieldModule, MatSelectModule, MatButtonToggleModule, MatIconModule } from '@angular/material';
import { CodemirrorModule } from 'ng2-codemirror';
import { FormsModule } from '@angular/forms';
import { ButtonContainerModule } from 'app/shared/buttons/button-container/button-container.module';

@NgModule({
  imports: [
    CommonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatSelectModule,
    MatButtonToggleModule,
    CodemirrorModule,
    FormsModule,
    ButtonContainerModule,
    MatIconModule
  ],
  declarations: [CardAdvancedComponent],
  exports: [CardAdvancedComponent]
})
export class CardAdvancedModule { }
