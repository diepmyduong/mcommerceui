import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ViewChildren, QueryList, ChangeDetectionStrategy, ChangeDetectorRef, Pipe, PipeTransform } from '@angular/core';
import { CodemirrorComponent } from 'ng2-codemirror';
import { VALID_BUTTONS_2 } from 'app/shared/buttons/button-container/validButtons';
import { BaseCard } from 'app/shared/cards/base-card';
import { ButtonContainerComponent } from 'app/shared/buttons/button-container/button-container.component';
import { ButtonService } from 'app/services/button.service';
import { differenceBy, cloneDeep } from 'lodash-es';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { PopoverService } from 'app/shared/popover-form/popover.service';
@Component({
  selector: 'app-card-advanced',
  templateUrl: './card-advanced.component.html',
  styleUrls: ['./card-advanced.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardAdvancedComponent implements OnInit {

  @Input() disabled: boolean
  @Input() break: boolean
  @Input() condition: any
  @Input() container: BaseCard
  @Output() breakChange: EventEmitter<boolean> = new EventEmitter<boolean>()
  @Output() conditionChange: EventEmitter<string> = new EventEmitter<string>()
  @Output() closeClicked: EventEmitter<any> = new EventEmitter<any>()
  @ViewChild('editor') editor: CodemirrorComponent;
  @ViewChildren(ButtonContainerComponent) btnContainersRef: QueryList<ButtonContainerComponent>;
  constructor(
    private buttonSrv: ButtonService, 
    public changeRef: ChangeDetectorRef,
    private popoverService: PopoverService) { }
  tutorial = "Nếu điều kiện gửi trả về kết quả là true thì thẻ sẽ được gửi, ngược lại sẽ không gửi thể. Nếu dừng câu chuyên được đánh check thì sẽ dừng câu chuyện sau khi thẻ đã được gửi, nhưng nếu thẻ không được gửi do điều kiện trả về là false thì câu chuyện sẽ không bị dừng lại."

  codeMirrorOption = {
    mode: "javascript",
    theme: "material",
    lineWrapping: true,
  }
  mode: 'block' | 'code' =  'block'

  mainLogicOperator: string = 'and'
  operationArray: any[]
  operationObjectArray: any[]

  operatorObject = {
    '==': '=',
    '!=': 'Khác',
    '!': 'không có',
    '!!': 'tồn tại',
    '>': '>',
    '>=': '>=',
    '<': '<',
    '<=': '<='
  }

  blockCondition: any = {
    "and": [
    ]
  }
  codeCondition: string = ''

  quickReplyValidButtons = VALID_BUTTONS_2
  btnContainers: ButtonContainerComponent[] = [];
  debounce: any;

  ngOnInit() {    
    this.operationArray = this.blockCondition.and
    //check mode
    if (this.condition) {
      if (typeof(this.condition) == 'string') {
        this.mode = 'code'
        this.codeCondition = this.condition
        this.changeRef.detectChanges()
      } else {
        this.mode = 'block'
        if ((this.condition.and && this.condition.and.length) || (this.condition.or && this.condition.or.length)) {
          this.blockCondition = cloneDeep(this.condition)
          this.mainLogicOperator = Object.keys(this.condition)[0]
          this.operationArray = this.condition[this.mainLogicOperator]
        } else {
          this.operationArray = this.blockCondition[this.mainLogicOperator]
        }
      }
    } else {
      //default
      this.condition = this.blockCondition
    }
    this.createOperationObjectArray()
    this.changeRef.detectChanges()
  }

  ngAfterViewInit() {
    this.regisBtnContainers();
    this.btnContainersRef.changes.subscribe(this.regisBtnContainers.bind(this));
  }

  ngOnDestroy() {
    this.btnContainers.forEach(c => this.buttonSrv.clearContainer(c));
  }

  regisBtnContainers() {
    const containers = this.btnContainersRef.toArray();
    differenceBy(this.btnContainers, containers, 'containerId').forEach(c => this.buttonSrv.clearContainer(c));
    this.btnContainers = containers;
    this.btnContainers.forEach(c => this.buttonSrv.regisContainer(c));
    this.changeRef.detectChanges()
  }

  ngAfterContentInit() {
    setTimeout(() => {
      if (this.mode == 'code') {
        this.editor.instance.refresh();
        this.changeRef.detectChanges()
      }
    }, 500)
  }

  codeConditionChanged(event) {
    if (this.debounce) clearTimeout(this.debounce)
    this.debounce = setTimeout(() => {
      this.setCondition()
      this.conditionChange.emit(event)
    }, 250)
  }

  blockConditionChanged() {
    this.createBlockCondition()
    this.setCondition()
    this.conditionChange.emit(this.condition)
  }

  updateOperation(op, index) {
    let operation = this.operationArray[index]
    let keys = Object.keys(operation)
    for (let key of keys) {
      delete operation[key]
    }
    operation[op.operator] = []
    operation[op.operator].push(op.key)
    if (op.operator != '!' && op.operator != '!!') {
      operation[op.operator].push(op.value)
    }
    this.operationArray[index] = op
    this.createOperationObjectArray()
    this.changeRef.detectChanges()
  }

  createBlockCondition() {
    let blockCondition = {}
    let array = []
    for (let operation of this.operationArray) {
      let operator = Object.keys(operation)[0]
      if (operation[operator][0]) {
        array.push(operation)
      }
    }
    if (!this.operationArray.length) {
      blockCondition = {}
    } else {
      blockCondition[this.mainLogicOperator] = array
    }
    this.blockCondition = blockCondition
    this.changeRef.detectChanges()
  }

  switchMode(mode) {
    this.mode = mode
    if (this.mode == 'block') this.createBlockCondition()
    this.setCondition()
    this.conditionChange.emit(this.condition)
    this.changeRef.detectChanges()
  }

  setCondition() {
    if (this.mode == 'block') {
      this.condition = this.blockCondition
    } else {
      this.condition = this.codeCondition
    }
    this.changeRef.detectChanges()
  }

  // addCondition() {
  //   this.operationArray.push({ "==": [ "", "" ] })
  //   this.blockConditionChanged()
  // }

  removeCondition(index) {
    this.operationArray.splice(index, 1)
    this.createOperationObjectArray()
    this.blockConditionChanged()
  }
  
  async addCondition(event) {
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      target: '.btn-add',
      targetElement: event.target,
      width: 480,
      horizontalAlign: 'center',
      confirm: 'Tạo điều kiện',
      fields: [
        {
          type: 'condition',
          placeholder: '',
          property: 'condition',
          current: {
            key: '',
            value: '',
            type: '=='
          },
          constraints: ['required']
        }
      ],
      onSubmit: (data) => {
        if (data.condition) {
          let jsonObject = {}
          jsonObject[data.condition.type] = []
          jsonObject[data.condition.type].push(data.condition.key)
          jsonObject[data.condition.type].push(data.condition.value)
          // let jsonString = `{ "${data.condition.type}": ["${data.condition.key}","${data.condition.value}"] }`
          this.operationArray.push(jsonObject)
          this.createOperationObjectArray()
          this.blockConditionChanged()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  async editCondition(event, index) {
    let operation = this.operationArray[index]
    let type = Object.keys(operation)[0]
    let key =  operation[type][0]
    let value =  operation[type][1]
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      target: '.condition-block',
      targetElement: event.target,
      width: 480,
      horizontalAlign: 'center',
      confirm: 'Lưu điều kiện',
      fields: [
        {
          type: 'condition',
          placeholder: '',
          property: 'condition',
          current: {
            key: key,
            value: value,
            type: type
          },
          constraints: ['required']
        }
      ],
      onSubmit: (data) => {
        if (data.condition && (data.condition.type != type || data.condition.key != key || data.condition.value != value)) {
          let jsonString = `{ "${data.condition.type}": ["${data.condition.key}","${data.condition.value}"] }`
          let operation = JSON.parse(jsonString)
          this.operationArray[index] = operation
          this.createOperationObjectArray()
          this.blockConditionChanged()
        }
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  createOperationObjectArray() {
    let array = []
    for (let operation of this.operationArray) {
      let item = {} as any
      item.type = Object.keys(operation)[0]
      item.key = operation[item.type][0]
      item.value = operation[item.type][1]
      array.push(item)
    }
    this.operationObjectArray = array
  }
}
