import { Component, OnInit, Input, EventEmitter, Output, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-card-sidebar',
  templateUrl: './card-sidebar.component.html',
  styleUrls: ['./card-sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardSidebarComponent implements OnInit {

  @Input() disabled: boolean
  @Input() index: number
  @Output() insertBeforeClicked: EventEmitter<any> = new EventEmitter()
  @Output() configClicked: EventEmitter<any> = new EventEmitter()
  @Output() moveUpClicked: EventEmitter<any> = new EventEmitter()
  @Output() moveDownClicked: EventEmitter<any> = new EventEmitter()
  @Output() duplicateClicked: EventEmitter<any> = new EventEmitter()
  @Output() removeClicked: EventEmitter<any> = new EventEmitter()

  public first: boolean = false
  public last: boolean = false

  constructor( 
    public ref: ChangeDetectorRef
    ) {

  }
  
  ngOnInit() {
  }

  ngAfterContentInit() {
  }

  async configButtonClicked() {
    this.configClicked.emit()
  }

  async moveUpButtonClicked() {
    this.moveUpClicked.emit()
  }

  async moveDownButtonClicked() {
    this.moveDownClicked.emit()
  }

  async duplicateButtonClicked(sameStory) {
    this.duplicateClicked.emit(sameStory)
  }

  async removeButtonClicked() {
    this.removeClicked.emit()
  }

  async insertBeforeButtonClicked() {
    this.insertBeforeClicked.emit()
  }

}
