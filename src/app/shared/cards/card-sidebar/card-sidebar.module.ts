import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTooltipModule, MatButtonModule, MatMenuModule } from '@angular/material';
import { CardSidebarComponent } from './card-sidebar.component';

@NgModule({
  imports: [
    CommonModule,
    MatTooltipModule,
    MatButtonModule,
    MatMenuModule
  ],
  declarations: [CardSidebarComponent],
  exports: [CardSidebarComponent]
})
export class CardSidebarModule { }
