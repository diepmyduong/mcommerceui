import { NgModule } from '@angular/core';
import { CardHeaderModule } from './card-header/card-header.module';
import { CardAdvancedModule } from './card-advanced/card-advanced.module';
import { CardSidebarModule } from './card-sidebar/card-sidebar.module';
import { HightlightTextareaModule } from 'app/directives/hightlight-textarea/hightlight-textarea.module';

@NgModule({
    imports: [
        CardHeaderModule,
        CardAdvancedModule,
        CardSidebarModule,
        HightlightTextareaModule
    ],
    exports: [
        CardHeaderModule,
        CardAdvancedModule,
        CardSidebarModule,
        HightlightTextareaModule
    ]
})
export class CardComponentsModule { }
