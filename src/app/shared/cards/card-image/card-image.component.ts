import { Component, OnInit, Input, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service';
import { MatDialog } from '@angular/material/dialog';
import { AlertService } from 'app/services/alert.service';
import { ImagePopupDialog } from 'app/modals/image-popup/image-popup.component';

@Component({
  selector: 'app-card-image',
  templateUrl: './card-image.component.html',
  styleUrls: ['./card-image.component.scss']
})
export class CardImageComponent implements OnInit {

  @Input() imageSrc: string
  @Input() imageRatio: string = 'standalone'
  @Input() disabled: boolean = false
  @Input() hidden: boolean = false
  @Output() sourceChanged: EventEmitter<any> = new EventEmitter<any>()
  @Output() uploadingChanged: EventEmitter<boolean> = new EventEmitter<boolean>(false)

  isUploading: boolean = false
  state: string = 'loaded'
  defaultImage: string
  files: any

  @ViewChild('image') imageRef: any
  @ViewChild("fileUploader") fileUploader: ElementRef;
  constructor(
    public chatbotApi: ChatbotApiService,
    public dialog: MatDialog,
    public alert: AlertService
  ) {
    this.defaultImage = chatbotApi.cardBuilder.defaultImage
  }

  ngOnInit() {
  }

  ngAfterContentInit() {
    setTimeout(() => {
      this.imageRef.nativeElement.onload = () => {
        if (this.imageRef.nativeElement.src.indexOf('/assets/img/warning-image.svg') > -1) {
          this.state = 'warning'
        } else {
          if (this.imageRef.nativeElement.src.indexOf('/assets/img/error-image.svg') > -1) {
            this.state = 'error'
          } else {
            this.state = 'loaded'
          }
        }
      }
      this.imageRef.nativeElement.onerror = () => {
        if (/\{\{.*\}\}/g.test(this.imageSrc)) {
          this.imageRef.nativeElement.src = '/assets/img/warning-image.svg'
        } else {
          this.imageRef.nativeElement.src = '/assets/img/error-image.svg'
        }
      }
    })
  }

  async onChangeImageFile(event) {
    let files = this.fileUploader.nativeElement.files

    if (files.length == 0) return
    let file = files[0]
    if (file) {
      try {
        this.isUploading = true;
        this.uploadingChanged.emit(true);
        let response = await this.chatbotApi.upload.uploadImage(file) as any
        this.imageSrc = response.link
        this.sourceChanged.emit(this.imageSrc)
      } catch (err) {
        // console.error(err)
        this.alert.handleError(err, "Lỗi khi tải ảnh lên");
        // this.alert.error("Lỗi khi tải ảnh lên", err.message)
      } finally {
        setTimeout(() => {
          this.isUploading = false;
          this.uploadingChanged.emit(false);
        }, 300)
      }
    }
  }

  async openImage() {
    if (this.state != 'loaded') return;

    let data = {
      image: this.imageSrc
    };

    this.dialog.open(ImagePopupDialog, {
      maxHeight: '60vh',
      panelClass: 'image-popup',
      data: data
    });
  }

  async onSourceChanged(event) {
    this.sourceChanged.emit(event)
  }

  imageLoaded(event) {
    
  }
}
