import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardImageComponent } from 'app/shared/cards/card-image/card-image.component';
import { ImagePopupModule } from 'app/modals/image-popup/image-popup.module';
import { MatTooltipModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { LazyLoadImagesModule } from 'ngx-lazy-load-images';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatTooltipModule,
    LazyLoadImagesModule,
    ImagePopupModule
  ],
  declarations: [CardImageComponent],
  exports: [CardImageComponent]
})
export class CardImageModule { }
