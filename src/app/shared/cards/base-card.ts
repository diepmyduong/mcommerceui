
import {debounceTime} from 'rxjs/operators';
import * as Console from 'console-prefix'
import * as guid from 'guid'
import { get, forEach } from 'lodash-es'
import { Input, Output, EventEmitter, ViewRef, ViewChild, ChangeDetectorRef, Component, HostListener } from '@angular/core';
import { BehaviorSubject ,  Subscription } from 'rxjs'
import { NgForm } from '@angular/forms'
import { MatSlideToggle } from '@angular/material'
import { iCard, ChatbotApiService, iSetting } from 'app/services/chatbot'
import { AlertService } from 'app/services/alert.service';
import { PortalService } from 'app/services/portal.service';
import { ServicesInjector } from 'app/services/serviceInjector';
import { CardService } from 'app/services/card.service';
import { StoryDetailPortalComponent } from 'app/chatgut-app/portals/story-detail-portal/story-detail-portal.component';
import { CardSidebarComponent } from './card-sidebar/card-sidebar.component';
import { CardContainerComponent } from './card-container/card-container.component';
export class BaseCard {
    // @HostBinding('class.swiper-slide') swiperSlideClass = true
    @Input() card: iCard
    @Input() extra: any
    @Output() change: EventEmitter<any> = new EventEmitter()
    @ViewChild(CardSidebarComponent) cardSidebar: CardSidebarComponent
    @ViewChild('cardFrm', { read: NgForm }) cardFrm: NgForm
    constructor(
    ) {
        this.subscriptions = {};
        this.alert = ServicesInjector.get(AlertService);
        this.chatbotApi = ServicesInjector.get(ChatbotApiService);
        this.portalService = ServicesInjector.get(PortalService);
        this.cardService = ServicesInjector.get(CardService)
    }
    cardContainerId: string
    portalService: PortalService
    cardService: CardService
    viewInited: BehaviorSubject<number>
    subscriptions: { [key: string]: Subscription }
    viewRef: ViewRef
    operation: any
    cardName: string = ""
    chatbotApi: ChatbotApiService
    dirty: boolean = false
    cardRefId: string = guid.raw()
    isSaving = false
    isUploading = false
    storyId: string = ""
    canBeSaved: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)
    allowCanBeSaved: boolean = false
    disallowAdvancedQuickReplies: boolean = false
    protected alert: AlertService
    index: number
    draggable: boolean = true
    quickReplies: any[]

    codeMirrorOption = {
        mode: "javascript",
        theme: "material",
        lineWrapping: true,
        lineNumbers: true,
    }
    configOpened: boolean = false
    get container() { return get(this.cardContainerRef,'container') as CardContainerComponent }
    get cardContainerRef() { return this.cardService.containers[this.cardContainerId]; }
    get log() { return Console(`Card [${this.index}]`).log }
    get dragData() { return { type: 'card-order', cardIndex: this.index, card: this.card, portalIndex: this.portalIndex, storyId: this.storyId } }
    get guid() { return guid }   

    get _changeRef(): ChangeDetectorRef { return (<any>this).changeRef; }
    detectChanges() { this._changeRef?this._changeRef.detectChanges(): null; }

    @HostListener('mouseenter')
    mouseenter() {
        if (!this.cardSidebar) return
        let wrapper = document.getElementsByClassName('card-wrapper')
        for (let i = 0; i < wrapper.length; i++) {
            let item = wrapper.item(i)
            if (item.getElementsByClassName('story-card').item(0).classList.contains(this.cardRefId)) {
                item.classList.add('active')
            } else {
                item.classList.remove('active')
            }
        }
    }

    onQuickReplyButtonsChange(buttons: any[]) {
        this.card.option.quick_replies = buttons
        this.canBeSaved.next(true)
        this.container.change.emit();
        this.detectChanges()
    }

    async onBreakChanged(event) {
        this.card.isBreak = event;
        this.canBeSaved.next(true)
        this.container.change.emit();
        this.detectChanges()
    }

    async onConditionChanged(event) {
        this.card.condition = event
        this.canBeSaved.next(true)
        this.container.change.emit()
        this.detectChanges()
    }

    async runIntro() { }

    onUploading(event) {
        this.isUploading = event;
        this.detectChanges()
    }

    cardValid(): string {
        if (this.cardFrm.valid) {
            return ""
        } else {
            return `Thẻ thứ ${this.index + 1} (${this.cardName}) không hợp lệ.`
        }
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        forEach(this.subscriptions, (sub: Subscription, key: string) => {
            sub.unsubscribe()
        })
    }


    disableSaveDetection() {
        this.allowCanBeSaved = false;
        setTimeout(() => {
            this.allowCanBeSaved = true
        }, 2000)
    }

    ngAfterContentInit() {
        this.viewInited.next(this.index)
        if (this.card && (this.card.isBreak || (this.card.condition && ((this.card.condition.and && this.card.condition.and.length) || 
        (this.card.condition.or && this.card.condition.or.length) || (typeof(this.card.condition)=='string'))))) {
            this.configOpened = true;
        }
        this.disableSaveDetection();
        this.subscriptions.cardValueChanges = this.cardFrm.valueChanges.pipe(debounceTime(500)).subscribe(value => {
            if (this.allowCanBeSaved && !this.canBeSaved.getValue() && this.cardFrm.dirty) {
                this.canBeSaved.next(true)
                this.container.change.emit()
                this.detectChanges();
            }
        })
        this.subscriptions.canBeSave = this.canBeSaved.subscribe(canSave => {
            if (canSave && this.allowCanBeSaved) {
                this.container.change.emit();
            }
            this.detectChanges()
        })
        this.detectChanges();
    }

    async beforeSave() { return; }

    get portalIndex() {
        return this.portalService.container.warpper.indexOf(this.container.parentViewRef)
    }

    async saveContent() {
        // Disable Change
        this.isSaving = true
        this.detectChanges()
        // Update Card
        try {
            await this.beforeSave();
            const card = await this.chatbotApi.card.update(this.card._id, this.card, { reload: true })
            this.dirty = false;
            this.isSaving = false;
            this.canBeSaved.next(false);
            this.disableSaveDetection();
        } catch (err) {
            console.error(err)
            this.isSaving = false;
            this.canBeSaved.next(true)
            this.detectChanges()
            throw Error('save error')
        }
    }
    
    save() {}

    saveQuickReplies() {}

    resetForm(formCtrl: NgForm, card: iCard | iSetting): any {
        if (card.condition || card.isBreak) {
            return {
                condition: card.condition,
                isBreak: card.isBreak
            }
        } else {
            return {}
        }

    }

    async remove() {
        this.operation = "delete";
        await this.container.removeCard(this.card._id, this.index);
    }

    async duplicateCard(sameStory) {
        (this.container.parentPortal as StoryDetailPortalComponent).duplicateCard(this.index, sameStory)
        // this.container.duplicateCard(this.index)
    }

    async moveUp() {
        await this.container.moveCard(this.index, this.index - 1)
    }

    async moveDown() {
        await this.container.moveCard(this.index, this.index + 1)
    }

    async configToggle() {
        this.configOpened = !this.configOpened
        this.detectChanges()
    }

    async insertBefore() {
        const currentPortal = this.portalService.container.portals[this.portalIndex] as StoryDetailPortalComponent
        // console.log(this.index)
        // await currentPortal.openCreateCardDialog(this.index)
        currentPortal.openQuickCards(this.index, this.cardContainerId + '-' + this.cardRefId)
    }

    async changeNote(note) {
        if (this.card.note == note) return
        if (this.operation != 'add') { // if card is already saved
            let fallback = this.card.note
            this.card.note = note
            this.chatbotApi.card.update(this.card._id, {
                note
            }).then(result => {
    
            }).catch(err => {
                console.error(err)
                this.card.note = fallback
            })
        } else {
            this.card.note = note
        }
        this.detectChanges()
    }

    // async changeNote(event) {
    //     let original = this.card.note
    //     try {
    //         this.card.note = event
    //         this.chatbotApi.card.update(this.card._id, {
    //         note: event
    //         })
    //     } catch (err) {
    //         this.card.note = original
    //     }
    // }


}