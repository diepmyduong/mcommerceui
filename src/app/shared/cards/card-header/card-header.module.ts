import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardHeaderComponent } from 'app/shared/cards/card-header/card-header.component';
import { MatCardModule, MatIconModule, MatMenuModule, MatTooltipModule, MatDividerModule, MatButtonModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatIconModule,
    MatMenuModule,
    MatTooltipModule,
    MatDividerModule,
    MatButtonModule,
  ],
  declarations: [CardHeaderComponent],
  exports: [CardHeaderComponent]
})
export class CardHeaderModule { }
