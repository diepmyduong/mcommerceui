import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { ChatbotApiService } from 'app/services/chatbot';
import { PopoverService } from 'app/shared/popover-form/popover.service';

@Component({
  selector: 'app-card-header',
  templateUrl: './card-header.component.html',
  styleUrls: ['./card-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardHeaderComponent implements OnInit {

  @Input() icon: string
  @Input() title: string
  @Input() disabled: boolean
  @Input() note: string
  @Output() noteChanged: EventEmitter<any> = new EventEmitter()

  public first: boolean = false
  public last: boolean = false

  constructor( 
    private popoverService: PopoverService
    ) {

  }
  
  ngOnInit() {
  }

  ngAfterContentInit() {
  }

  
  
  async editNote(event) {
    
    let formOptions: iPopoverFormOptions = {
      parent: '.portal-content-block',
      targetElement: event.target,
      width: 300,
      horizontalAlign: 'right',
      fields: [
        {
          type: 'textarea',
          placeholder: 'Ghi chú',
          property: 'note',
          current: this.note,
          constraints: ['required']
        }
      ],
      onSubmit: (data) => {
        this.noteChanged.emit(data.note)
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

}
