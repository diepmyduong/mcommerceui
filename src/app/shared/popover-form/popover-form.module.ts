import { NgModule } from '@angular/core';
import { PopoverForm } from './popover-form.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { PopoverService } from './popover.service';
import { MatFormFieldModule, MatSelectModule, MatButtonToggleModule, MatProgressSpinnerModule } from '@angular/material';
import { NgJsonEditorModule } from 'ang-jsoneditor';
import { CardImageModule } from '../cards/card-image/card-image.module';
import { StorySelectionModule } from 'app/components/story-selection/story-selection.module';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    MatFormFieldModule,
    MatButtonToggleModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    CardImageModule,
    StorySelectionModule,
    NgJsonEditorModule
  ],
  declarations: [PopoverForm],
  exports: [PopoverForm],
  entryComponents: [PopoverForm],
  providers: [PopoverService,
      { provide: DYNAMIC_COMPONENT, useValue: PopoverForm }
    ]
})
export class PopoverFormModule {
}
