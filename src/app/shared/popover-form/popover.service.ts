import { Injectable, ApplicationRef, ViewContainerRef, ComponentRef } from '@angular/core';
import { PopoverForm, iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { Subscription } from 'rxjs';
import { DynamicComponentLoaderService } from '../../services/dynamic-component-loader/dynamic-component-loader.service';
import { AppComponent } from 'app/app.component';

@Injectable({
    providedIn: 'root',
})
export class PopoverService {

    formList: string[] = [
        "appName"
    ]
    popoverFormRef: ComponentRef<PopoverForm>
    popoverFormComp: PopoverForm
    clickSubscription: Subscription
    viewRef: ViewContainerRef
    clickHandler: EventListener
    constructor(
        private dynamicComponentLoader: DynamicComponentLoaderService,
        private appRef: ApplicationRef
    ) {
        this.viewRef = (this.appRef.components[0].instance as AppComponent).viewContainerRef
    }

    public async createPopoverForm(options: iPopoverFormOptions) {
        try {
            document.removeEventListener('click', this.clickHandler)
            // get form by id
            let popoverForm = document.getElementById('popover-form')
            // if no form or form ref found
            if (!this.popoverFormRef || !popoverForm) {
                if (this.popoverFormRef) { this.popoverFormRef.destroy(); }
                let componentFactory = await this.dynamicComponentLoader.getComponentFactory('PopoverForm');
                this.popoverFormRef = this.viewRef.createComponent(componentFactory) as ComponentRef<PopoverForm>
                this.popoverFormComp = this.popoverFormRef.instance
                popoverForm = document.getElementById('popover-form')
            }
            
            this.popoverFormComp.setFormOptions(options)
            
            let target: HTMLElement
            if (options.targetElement) {
                target = options.targetElement
                //retrace clicked element to the actual target
                if (options.target) {
                    while(!(target.classList.contains(options.target.substr(1)) || target.id == options.target.substr(1))) {
                        target = target.offsetParent as HTMLElement
                        if (!target) break
                    }
                }
            } else {
                target = document.getElementById(options.target.substr(1))
            }

            //calculate offset from the target to its parent
            let offsetTop = 0, offsetLeft = 0, offsetRight = 0
            let current = target
            while(!(current.classList.contains(options.parent.substr(1)) || current.id == options.parent.substr(1))) {
                offsetTop += current.offsetTop
                offsetLeft += current.offsetLeft
                current = current.offsetParent as HTMLElement
            }

            //find left, right, width of popover
            popoverForm.style.width = (typeof options.width)=='string'?(options.width as string):(options.width.toString() + 'px')
            let left = offsetLeft - target.clientLeft - 8
            let right = current.offsetWidth - (left + target.offsetWidth) - 24
            let width = (typeof options.width)=='number'?(options.width as number):popoverForm.offsetWidth

            if (!options.horizontalAlign) {
                //redetermine horizontal aligment if it overflows its parent
                if ((left + width) >= current.offsetWidth && (right + width) >= current.offsetWidth) {
                    options.horizontalAlign = 'center'
                } else if ((left + width) >= current.offsetWidth) {
                    options.horizontalAlign = 'right'
                } else if ((right + width) >= current.offsetWidth) {
                    options.horizontalAlign = 'left'
                }
            }
            //set left and right
            if (options.horizontalAlign == 'center') {
                popoverForm.style.right = 'auto'
                popoverForm.style.left = '50%'
            } else if (options.horizontalAlign == 'right') {
                popoverForm.style.left = 'auto'
                popoverForm.style.right = right.toString() + 'px'
            } else {
                popoverForm.style.right = 'auto'
                popoverForm.style.left = left.toString() + 'px'
            }
            
            //set top
            if (options.verticalAlign == 'center') {
                popoverForm.style.top = (offsetTop - target.clientTop - 8 - target.clientHeight / 2) + 'px'
                popoverForm.style.bottom = 'auto'
            } else if (options.verticalAlign == 'bottom') {
                // popoverForm.style.top = (offsetTop - target.clientTop - 36 - target.clientHeight) + 'px'
                popoverForm.style.top = 'auto'
                popoverForm.style.bottom = (current.clientHeight - (offsetTop + target.clientTop + target.clientHeight) + 12) + 'px'
            } else {
                popoverForm.style.top = (offsetTop - target.clientTop - 8) + 'px'
                popoverForm.style.bottom = 'auto'
            }

            //transform if center
            popoverForm.style.transform = ''
            if (options.horizontalAlign == 'center' || options.verticalAlign == 'center') {
                popoverForm.style.transformOrigin = 'center center'
                if (options.horizontalAlign == 'center') popoverForm.style.transform += 'translateX(-50%) '
                if (options.verticalAlign == 'center') popoverForm.style.transform += 'translateY(-50%) '
            }

            current.appendChild(popoverForm)

            if (this.clickSubscription) this.clickSubscription.unsubscribe()
            this.clickSubscription = this.popoverFormComp.click.subscribe(data => {
                if (data) {
                    options.onSubmit(data)
                }
                this.closePopoverForm()
                this.clickSubscription.unsubscribe()
            })

            setTimeout(() => {
                switch (options.fields[0].type) {
                    case 'textarea':
                        (popoverForm.getElementsByTagName('textarea').item(0) as HTMLTextAreaElement).focus()
                    break;
                    case 'product':
                        (popoverForm.getElementsByClassName('form-input').item(0) as HTMLInputElement).focus()
                    break;
                    default: 
                        (popoverForm.getElementsByTagName('input').item(0) as HTMLInputElement).focus()
                    break;
                }

                this.clickHandler = (event:any) => {
                    let cdk = document.getElementsByClassName('cdk-overlay-container').item(0)
                    if (cdk && cdk.contains(event.srcElement)) return
                    
                    if (!popoverForm.contains(event.srcElement) && 
                    !this.popoverFormComp.hasChanged &&
                    !this.popoverFormComp.active) {
                        this.closePopoverForm()
                    }
                }
                document.addEventListener('click', this.clickHandler)
            })
        } catch (err) {
            console.error(err)
            this.closePopoverForm()
        }
    }

    public closePopoverForm() {
        let form = document.getElementById('popover-form')
        if (form) {
            form.remove()
            this.popoverFormRef.destroy()
            if (this.clickSubscription) this.clickSubscription.unsubscribe()
            document.removeEventListener('click', this.clickHandler)
        }
    }

}