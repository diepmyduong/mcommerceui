import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Output, EventEmitter} from '@angular/core';
import { cloneDeep, isEqual } from 'lodash-es'
import { iSubscriber, ChatbotApiService } from 'app/services/chatbot';

@Component({
    selector: 'popover-form',
    styleUrls: ['./popover-form.component.scss'],
    templateUrl: './popover-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopoverForm implements OnInit {

    @Output() click: EventEmitter<any> = new EventEmitter<any>()

    formOptions: iPopoverFormOptions = {
        parent: 'body',
        width: 300,
        confirm: 'Lưu',
        cancel: 'Hủy',
        fields: []
    }

    originalData: any = {}
    data: any = {}
    active: boolean = false

    typeArray = {
        filter: [
            { code: '#eq', display: '=' },
            { code: '#ne', display: 'Khác' },
            { code: '#regex', display: 'Chứa chuỗi' },
            // { code: '#not', display: 'Không chứa chuỗi' },
            { code: '#exists', display: 'Có tồn tại' },
            { code: '#non_exists', display: 'Không tồn tại' },
            { code: '#gt', display: '>' },
            { code: '#lt', display: '<' },
            { code: '#gte', display: '>=' },
            { code: '#lte', display: '<=' },
        ],
        condition: [        
            { code: '==', display: '=' },
            { code: '!=', display: 'Khác' },
            { code: '!', display: 'Không có' },
            { code: '!!', display: 'Tồn tại' },
            { code: '>', display: '>' },
            { code: '>=', display: '>=' },
            { code: '<', display: '<' },
            { code: '<=', display: '<=' }
        ]
    }

    hasChanged: boolean = false
    hasError: boolean = false
    errors: any = {}

    searchText: string = ''

    constructor(
        private ref: ChangeDetectorRef,
        private chatbotApi: ChatbotApiService
    ) {
    }

    ngOnInit() {
    }

    public setFormOptions(options: iPopoverFormOptions) {
        this.formOptions = { ...this.formOptions, ...options }
        this.data = {}
        this.formOptions.fields.forEach(field => {
            if (!field.type) field.type = 'text'
            this.data[field.property] = (field.current !== null || field.current !== undefined)?field.current:''
            this.originalData = cloneDeep(this.data)
            if (field.type == 'subscribers') {
                this.loadSubscribers(field)
            }
        })
        this.ref.detectChanges()
    }

    onYesClick() {

        if (this.checkErrors()) return

        this.click.emit(this.data)
    }

    onNoClick() {
        this.click.emit()
    }

    selectionChanged(field) {
        switch (this.data[field.property].type) {
            case '#exists': case '#non_exists': case '!': case '!!':
                this.data[field.property].value = ''
                break

        }

        this.inputChanged()
    }

    onImageSourceChanged(event, property) {
        this.data[property].image = event
        this.ref.detectChanges()
    }

    inputChanged() {
        if (this.hasError) {
            this.checkErrors()
        }

        this.hasChanged = !isEqual(this.data, this.originalData)
        this.checkHasError()
        this.ref.detectChanges()
    }

    checkErrors() {
        // iterate the data fields
        for (let field of this.formOptions.fields) {
            // check if there is any constraint of them, if not skip
            if (!field.constraints || !field.constraints.length) continue
            switch (field.type) {
                case 'product':
                    if (!this.errors[field.property]) this.errors[field.property] = {}
                    if (!this.data[field.property].name) {
                        this.errors[field.property].name = "Bắt buộc"
                    } else {
                        this.errors[field.property].name = ""
                    }
                    // if (!this.data[field.property].price.length) {
                    //     this.errors[field.property].price = "Bắt buộc"
                    // } else {
                    //     this.errors[field.property].price = ""
                    // }

                    if (!this.errors[field.property].name && !this.errors[field.property].price) 
                        this.errors[field.property] = ""
                break
                case 'filter': case 'condition': {
                    if (!this.errors[field.property]) this.errors[field.property] = {}
                    if (!this.data[field.property].key) {
                        this.errors[field.property].key = "Bắt buộc"
                    } else {
                        this.errors[field.property].key = ""
                    }

                    if (!this.data[field.property].value) {
                        if ((field.type == 'filter' && this.data[field.property].type != '#exists' && this.data[field.property].type != '#non_exists') ||
                        (field.type == 'condition' && this.data[field.property].type != '!' && this.data[field.property].type != '!!')) {
                            this.errors[field.property].value = "Bắt buộc"
                        } else {
                            this.errors[field.property].value = ""
                        }
                    } else {
                        this.errors[field.property].value = ""
                    }       
                    
                    if (!this.errors[field.property].key && !this.errors[field.property].value) {
                        this.errors[field.property] = ""
                    }
                }
                break;
                case 'sequence_event':
                    if (!this.data[field.property].value) {
                        this.errors[field.property] = "Bắt buộc"
                    } else if (Number.isNaN(Number.parseFloat(this.data[field.property].value))) {
                        this.errors[field.property] = "Nhập số"
                    } else {                        
                        this.errors[field.property] = ''
                    }
                break;
                case 'subscribers':

                break
                default:
                    for (let cons of field.constraints) {
                        switch (cons) {
                            case 'required':
                                if (!this.data[field.property]) {
                                    this.errors[field.property] = "Bắt buộc"
                                    continue
                                } else {
                                    this.errors[field.property] = ''
                                }
                            break;
                            case 'ref':
                                if (this.data[field.property].includes(' ') || this.data[field.property].includes('_'))  {
                                    this.errors[field.property] = 'Không được chứa dấu cách hoặc dấu gạch dưới'
                                } else {
                                    this.errors[field.property] = ''
                                }
                            break
                            case 'hashtag':
                                if (!/\#\S+/g.test(this.data[field.property])) {
                                    this.errors[field.property] = 'Bắt đầu bằng # và theo sau bởi chữ và số'
                                } else {
                                    this.errors[field.property] = ''
                                }
                            break;
                            default: break
                        }
                    }
                    // this.errors[field.property] = ""
                break;
            }
        }

        return this.checkHasError()
    }

    checkHasError() {
        let flag = false
        for (let key in this.errors) {
            if (this.errors.hasOwnProperty(key) && this.errors[key]) {
                flag = true
                break
            }
        }

        this.hasError = flag
        return this.hasError
    }

    subscribers: iSubscriber[]
    searchedSubscribers: iSubscriber[] = []
    currentPage: number
    searchPage: number
    totalSubscribers: number
    totalSearchSubscribers: number = 0
    loadSubscribersDone: boolean = false
    loadingMore: boolean = false
    searching: boolean = false

    addSubscriber(field, sub) {
        let index = this.data[field.property].findIndex(x => x._id == sub._id)
        if (index == -1) {
            this.data[field.property].push(sub)
            this.hasChanged = true
            this.ref.detectChanges()
        }
    }

    removeSubscriber(field, sub) {
        let index = this.data[field.property].findIndex(x => x._id == sub._id)
        if (index >= 0) {
            this.data[field.property].splice(index, 1)
            this.hasChanged = true
            this.ref.detectChanges()
        }
        console.log(this.data[field.property])
    }

    async loadSubscribers(field, local = true) {
        try {
            this.currentPage = 1
            this.subscribers = await this.chatbotApi.subscriber.getList({ local, query: {
                fields: ["_id", "psid", "messengerProfile"],
                limit: 15,
                page: this.currentPage
                }
            })
            this.totalSubscribers = this.chatbotApi.subscriber.pagination.totalItems
        } catch (err) {
            console.error(err)
        } finally {
            this.loadSubscribersDone = true
            this.ref.detectChanges()
        }
    }

    async loadMore() {
        try {
            this.loadingMore = true
            this.ref.detectChanges()
            this.currentPage += 1
            let tasks = []
            tasks.push(this.chatbotApi.subscriber.getList({ local: true, query: {
                fields: ["_id", "psid", "messengerProfile"],
                limit: 15,
                page: this.currentPage
            } }).then(result => {
                this.subscribers = [...this.subscribers, ...result]
            }))
            await Promise.all(tasks)
        } catch (err) {
            console.error(err)
        } finally {
            this.loadingMore = false
            this.ref.detectChanges()
        }
    }

    async search() {
        try {
            this.searching = true
            this.searchedSubscribers = []
            this.totalSearchSubscribers = 0
            this.searchPage = 1
            this.ref.detectChanges()
            let tasks = []
            tasks.push(this.chatbotApi.subscriber.filter({
                local: true, query: {
                    fields: ["_id", "psid", "messengerProfile"],
                    limit: 15,
                    page: this.searchPage,
                    order: { "snippetUpdatedAtText": -1 }
                }
            }, {search: this.searchText}).then(result => {
                this.searchedSubscribers = result
                this.totalSearchSubscribers = this.chatbotApi.subscriber.pagination.totalItems
            }))
            await Promise.all(tasks)
        } catch (err) {
        } finally {
            this.searching = false
            this.ref.detectChanges()
        }
    }

    async searchMore() {
        try {
            this.loadingMore = true
            this.searchPage += 1
            this.ref.detectChanges()
            let tasks = []
            tasks.push(this.chatbotApi.subscriber.filter({
                local: true, query: {
                    fields: ["_id", "psid", "messengerProfile"],
                    limit: 15,
                    page: this.searchPage,
                    order: { "snippetUpdatedAtText": -1 }
                }
            }, {search: this.searchText}).then(result => {
                this.searchedSubscribers = [...this.searchedSubscribers, ...result]
            }))
            await Promise.all(tasks)
        } catch (err) {
        } finally {
            this.loadingMore = false
            this.ref.detectChanges()
        }
    }


    onChanged(event, property) {
        this.data[property] = event

        this.hasChanged = !isEqual(this.data, this.originalData)
        if (this.errors[property]) {
            this.checkErrors()
            this.checkHasError()
        }

        this.inputChanged()
    }
}

export interface iPopoverFormOptions {
    parent?: string
    target?: string
    targetElement?: HTMLElement
    width?: number | string
    title?: string
    confirm?: string
    cancel?: string
    fields?: iPopoverFormField[]
    horizontalAlign?: 'left' | 'right' | 'auto' | 'center'
    verticalAlign?: 'top' | 'bottom' | 'auto' | 'center'
    onSubmit?: (data: any) => any
}

export interface iPopoverFormField {
    type?: 'text' | 'textarea' | 'filter' | 'condition' | 'json' | 'sequence_event' | 'product' | 'subscribers' | 'select' | 'story' | 'checkbox'
    constraints?: ('required' | 'ref' | 'hashtag')[]
    col?: string
    extra?: any
    inputType?: string
    placeholder?: any
    property?: any
    current?: any
    error?: any
    options?: any[]
}