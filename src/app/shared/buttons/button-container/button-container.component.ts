import * as Console from 'console-prefix';
import * as guid from 'guid';
import  { has } from 'lodash-es'
import { Component, OnInit, ViewChild, ViewContainerRef, ElementRef, Input, Output, EventEmitter, ComponentFactoryResolver, ChangeDetectionStrategy, ChangeDetectorRef, ViewChildren } from '@angular/core';
import { ChatbotApiService, iPartner, iStory } from 'app/services/chatbot';
import { MatDialog } from '@angular/material/dialog';
import { AlertService } from 'app/services/alert.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { iSelectionData, SelectionDialog } from 'app/modals/selection/selection.component';
import { DynamicComponentLoaderService } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { IValidButtons, BUTTONS_LIST } from 'app/shared/buttons/button-container/validButtons';
import { CardContainerComponent } from 'app/shared/cards/card-container/card-container.component';
import { moveItemInArray } from '@angular/cdk/drag-drop';
import { ButtonFormService } from '../button-form/button-form.service';
import { ButtonComponent } from '../button-component/button-component.component';
import { environment } from '../../../../environments';
import { ButtonService } from 'app/services/button.service';

@Component({
  selector: 'app-button-container',
  templateUrl: './button-container.component.html',
  styleUrls: ['./button-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonContainerComponent implements OnInit {

  @ViewChild("buttonViewContainer", { read: ViewContainerRef }) buttonViewContainer: ViewContainerRef
  @Input() container: CardContainerComponent
  @Input() min: number = 0
  @Input() limit: number = 3
  @Input() validButtons: IValidButtons
  @Input() buttons: any[]
  @Input() menuIndex: number
  @Input() readOnly: boolean = false
  @Input() isQuickReplyContainer: boolean = false
  @Input() isMenuContainer: boolean = false
  _disabled: boolean
  @Input() set disabled(value: boolean) {
    this._disabled = value
    for (let button of this.buttonComps) {
      button.setDiasble(value)
    }
  }
  get disabled() {
    return this._disabled
  }
  @Input() fullWidth: boolean = false
  @Output() onButtonClick = new EventEmitter<any>()
  @Output() change = new EventEmitter<any[]>()
  constructor(
    public chatbotApi: ChatbotApiService,
    public selectionDialog: MatDialog,
    public dialog: MatDialog,
    public alert: AlertService,
    private dynamicComponentLoader: DynamicComponentLoaderService,
    public changeRef: ChangeDetectorRef,
    private buttonFormSrv: ButtonFormService,
    private buttonSrv: ButtonService
  ) {
    this.buttonComps = []
    this.buttonInited = new BehaviorSubject<number>(-1)
    this.subscriptions = []
    BUTTONS_LIST.forEach(b => { this.buttonTypesDisplay[b.code] = b.display; });
  }
  buttonComps: ButtonComponent[]
  buttonInited: BehaviorSubject<number>
  buttonTypesDisplay = {};
  subscriptions: Subscription[]
  containerId: string = guid.raw()
  innowayPartner: iPartner
  stories: iStory[]

  get log() { return Console('ButtonContainer').log }

  ngOnInit() {
    this.buttonSrv.regisContainer(this)
    if (!this.buttons) this.buttons = []
    this.setButtons(this.buttons)
    // this.innowayPartner = this.chatbotApi.chatbotAuth.app.partners.find(p => p.partner == 'innoway')
    // if (!this.innowayPartner && this.validButtons) {
    //   delete this.validButtons['order_innoway']
    //   delete this.validButtons['basket_innoway']
    //   delete this.validButtons['profile_innoway']
    //   delete this.validButtons['simple_checkout_innoway']
    // }
  }

  ngOnDestroy() {
    this.buttonSrv.clearContainer(this)
  }

  async setButtons(buttons) {
    this.buttons = buttons
    this.buttonComps = []
    this.buttonViewContainer.clear()
    if (this.buttons) {
      for (let button of this.buttons) {
        const { type, ...params } = button
        if (this.readOnly) button.readOnly = true
        if (this.validButtons[type]) {
          // await this.pushButtonComp(this.validButtons[type], {
          //   button, readOnly: this.readOnly
          // })
          await this.pushButton(button, this.readOnly)
        }
      }
    }
  }

  async pushButton(button, readOnly = false) {
    let componentFactory;
    componentFactory = await this.dynamicComponentLoader.getComponentFactory('ButtonComponent')
    let newIndex = this.buttonComps.length
    let componentRef = componentFactory.create(this.buttonViewContainer.parentInjector)
    this.buttonViewContainer.insert(componentRef.hostView, newIndex)
    let component: ButtonComponent = componentRef.instance
    component.index = newIndex
    component.viewInited = this.buttonInited
    component.containerId = this.containerId;
    component.init(button, this.isQuickReplyContainer?'quick_reply':(this.isMenuContainer?'menu':'card'), readOnly)
    this.buttonComps.push(component)
    this.updateIndex()
    this.changeRef.detectChanges();

    setTimeout(() => {
      let template = document.getElementById(this.containerId + '-' + component.buttonRefId)
      if (template) template.appendChild(componentRef.location.nativeElement)
    })

    return componentRef
  }

  popButtonComp(index: number) {
    if (this.buttonViewContainer.length == this.min) {
      this.alert.info('Chú ý', 'Số nút không được nhỏ hơn ' + this.min)
      return;
    }
    this.buttonComps.splice(index, 1)
    this.buttons.splice(index, 1)

    this.updateIndex()

    this.change.emit(this.buttons)
  }

  quick_reply_text = async (data) => {
    const button = {
      type: "quick_reply_text",
      content_type: "text",
      title: data.title,
      image_url: data.icon,
      buttonTag: data.tag,
      payload: JSON.stringify({
        type: 'story',
        data: data.story,
        additional: data.additional
      })
    }
    // await this.pushButtonComp(this.validButtons.quick_reply_text, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  web_url = async (data) => {
    const button = {
      type: "web_url",
      title: data.title,
      buttonTag: data.tag,
      url: data.url,
      messenger_extensions: data.messenger_extensions
    }
    // await this.pushButtonComp(this.validButtons.web_url, { button })
    await this.pushButton(button )
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  phone_number = async (data) => {
    const button = {
      type: "phone_number",
      title: data.title,
      buttonTag: data.tag,
      payload: data.phone
    }
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  element_share = async (data) => {
    const button = {
      type: "element_share",
      share_contents: {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: [
              {
                title: data.shareTitle,
                subtitle: data.shareSubtitle,
                image_url: data.shareImage,
                default_action: {
                  type: "web_url",
                  url: data.shareLink
                },
                buttons: [
                  {
                    type: "web_url",
                    url: data.shareLink,
                    title: data.shareButton
                  }
                ]
              }
            ]
          }
        }
      }
    }
    // await this.pushButtonComp(this.validButtons.element_share, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  postback =  async (data) => {    
    const button = {
      type: "postback",
      title: data.title,
      buttonTag: data.tag,
      payload: JSON.stringify({
        type: "story",
        data: data.story
      })
    }
    // await this.pushButtonComp(this.validButtons.postback, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  about_topic = async (data) => {
    const button = {
      type: "postback",
      title: data.title,
      buttonTag: data.tag,
      payload: JSON.stringify({
        type: "about_topic",
        additional: data.additional,
        data: {
          storyId: data.storyId,
          topicId: data.topicId
        }
      })
    }
    // await this.pushButtonComp(this.validButtons.about_topic, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  order_innoway = async (data) => {
    const url = await this.chatbotApi.app.getInnowayDeepLink({ path: "category" })
    const button = {
      type: "web_url",
      title: data.title,
      buttonTag: data.tag,
      url,
      webview_height_ratio: "full",
      messenger_extensions: true,
      webview_share_button: "hide"
    }
    // await this.pushButtonComp(this.validButtons.order_innoway, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  basket_innoway = async (data) => {
    const url = await this.chatbotApi.app.getInnowayDeepLink({ path: "basket" })
    const button = {
      type: "web_url",
      title: data.title,
      buttonTag: data.tag,
      url,
      webview_height_ratio: "full",
      messenger_extensions: true,
      webview_share_button: "hide"
    }
    // this.pushButtonComp(this.validButtons.basket_innoway, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  profile_innoway = async (data) => {
    const url = await this.chatbotApi.app.getInnowayProfileLink()
    const button = {
      type: "web_url",
      title: data.title,
      buttonTag: data.tag,
      url,
      webview_height_ratio: "full",
      messenger_extensions: true,
      webview_share_button: "hide"
    }
    // this.pushButtonComp(this.validButtons.basket_innoway, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  simple_checkout_innoway = async (data) => {
    const button = {
      type: "web_url",
      title: data.title,
      buttonTag: data.tag,
      url: await this.chatbotApi.deeplink.getSimpleCheckouLink({
        country: "VN",
        currency: "VND",
        sub_fee: 0,
        products: []
      }),
      webview_height_ratio: "full",
      messenger_extensions: true,
      webview_share_button: "hide"
    }
    // this.pushButtonComp(this.validButtons.simple_checkout_innoway, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  google_map = async (data) => {
    const url = await this.chatbotApi.deeplink.getGoogleMapDeeplink({
      to: {
        latitude: data.lat,
        longitude: data.lng
      }
    })
    const button = {
      type: "web_url",
      title: data.title,
      buttonTag: data.tag,
      url
    }
    // this.pushButtonComp(this.validButtons.google_map, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }
  
  quick_reply_email = async (data) => {
    const button = {
      type: "quick_reply_email",
      content_type: "user_email"
    }
    // this.pushButtonComp(this.validButtons.quick_reply_email, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  quick_reply_phone = async(data) => {
    const button = {
      type: "quick_reply_phone",
      content_type: "user_phone_number"
    }
    // this.pushButtonComp(this.validButtons.quick_reply_phone, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  quick_reply_location = async(data) => {
    const button = {
      type: "quick_reply_location",
      content_type: "location"
    }
    // this.pushButtonComp(this.validButtons.quick_reply_location, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)

  }

  quick_reply_activity = async (data) => {
    const button = {
      type: "quick_reply_activity",
      title: data.title,
      image_url: data.image_url?data.image_url:'',
      value: data.value
    }
    // let componentRef = this.pushButtonComp(this.validButtons.quick_reply_activity, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  live_chat = async (data) => {
    const button = {
      type: "postback",
      title: data.title,
      buttonTag: data.tag,
      payload: JSON.stringify({
        type: "live_chat",
        data: ""
      })
    }
    // this.pushButtonComp(this.validButtons.live_chat, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }
  
  activity = async (data) => {
    const button = {
      type: "postback",
      title: data.title,
      buttonTag: data.tag,
      payload: JSON.stringify({
        type: "activity",
        data: data.value
      })
    }
    // let componentRef = await this.pushButtonComp(this.validButtons.postback, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  deeplink = async (data) => {
    const button = {
      type: "deeplink",
      title: data.title,
      buttonTag: data.tag,
      androidUrl: data.androidUrl,
      androidStoreUrl: data.androidStoreUrl,
      iosStoreUrl: data.iosStoreUrl,
      iosUrl: data.iosUrl,
      webUrl: data.webUrl
    }
    // this.pushButtonComp(this.validButtons.deeplink, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  extension_order = async (data) => this.generateExtension(data, 'order')
  extension_profile = async (data) => this.generateExtension(data, 'profile')
  extension_booking = async (data) => this.generateExtension(data, 'booking')
  extension_history = async (data) => this.generateExtension(data, 'history')
  extension_reward = async (data) => this.generateExtension(data, 'reward?readonly=true')

  async generateExtension(data, path) {
    const button = {
      type: "web_url",
      title: data.title,
      buttonTag: data.tag,
      url: `${environment.chatbot.staticHost}/${path}`,
      messenger_extensions: true
    }
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  lucky_wheel = async (data) => {    
    const button = {
      type: "lucky_wheel",
      btnTitle: data.btnTitle,
      title: data.title,
      buttonTag: data.tag,
      multi: data.multi=='true'?true:false,
      luckyWheelId: data.wheel._id,
      giftId: data.giftId,
      successRatio: data.successRatio,
      storyId: data.storyId
    }
    // this.pushButtonComp(this.validButtons.lucky_wheel, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  nested = async (data) => {
    const button = {
      type: "nested",
      title: data.title,
      call_to_actions: [{
        "type": "web_url",
        "title": "Web URL",
        "url": "http://google.com"
      }]
    }
    // this.pushButtonComp(this.validButtons.nested, { button })
    await this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  onSubmit = (type) => {
    return this[type]
  }

  async addNewButton(event) {
    if (Object.keys(this.validButtons).length == 1) {
      let code = Object.keys(this.validButtons)[0]
      let buttonCode = 'quick_reply_text'
      this.buttonFormSrv.createNewButton({ event, buttonCode, onSubmit: this.onSubmit(buttonCode), removeTag: false })
    } else {
      const basicOptions = BUTTONS_LIST.filter(item => {
        return has(this.validButtons, item.code) && !/innoway/g.test(item.code)
      })
      const innowayOptions = BUTTONS_LIST.filter(item => {
        return has(this.validButtons, item.code) && /innoway/g.test(item.code)
      })
      const webviewButtons = ['lucky_wheel','extension_order', 'extension_profile', 'extension_booking', 'extension_reward', 'extension_history']
      let webviewOptions = []
      for (let i = 0; i < basicOptions.length; i++) {
        let option = basicOptions[i]
        if (webviewButtons.includes(option.code)) {
          webviewOptions.push(option)
          basicOptions.splice(i, 1)
          i--
        }
      }

      let buttonClipboardData
      try {
        // check clipboard
        let pasteData = sessionStorage.getItem('buttonData')
        if (pasteData) {
          let buttonObject = JSON.parse(pasteData)
          if (buttonObject.button && buttonObject.data && this.validButtons[buttonObject.data.type]) {
            buttonClipboardData = buttonObject.data
          }
        }
      } catch (err) {

      }

      let data: iSelectionData = {
        title: 'Thêm nút',
        confirmButtonText: 'Chọn loại nút',
        cancelButtonText: 'Quay lại',
        categories: [
          {
            name: 'Cơ bản',
            options: buttonClipboardData?[...basicOptions, {
              code: 'paste', display: "Paste dữ liệu", icon: 'fas fa-paste'
            }]:basicOptions,
          }
        ]
      }
      if (webviewOptions.length > 0) {
        data.categories.push({
          name: "Mở Webview",
          options: webviewOptions
        })
      }
      if (innowayOptions.length > 0) {
        data.categories.push({
          name: "Innoway",
          options: innowayOptions
        })
      }

      let dialogRef = this.selectionDialog.open(SelectionDialog, {
        width: '600px',
        data: data
      })
      dialogRef.afterClosed().toPromise().then(async result => {
        if (result) {
          let buttonCode = result;
          if (this.validButtons[buttonCode]) {  
            switch (buttonCode) {
              case 'quick_reply_email': case 'quick_reply_location': case 'quick_reply_phone':
                this[buttonCode]()
                break
              default:
                this.buttonFormSrv.createNewButton({ event, buttonCode, onSubmit: this.onSubmit(buttonCode), removeTag: this.isMenuContainer })
                break
            }
          } else if (buttonCode == 'paste') {
            this.pasteButtonData(buttonClipboardData)
          }
        }
      })

    }
  }

  async pasteButtonData(button) {
    this.pushButton(button)
    this.buttons.push(button)
    this.change.emit(this.buttons)
  }

  onButtonChange(indexChange: number) {
    const compChange = this.buttonComps[indexChange];
    compChange.detectChanges();
    this.buttons[indexChange] = compChange.button;
    this.change.emit(this.buttons);
  }

  async drop(event) {
    if (event.previousIndex == event.currentIndex) return

    this.moveButton(event.previousIndex, event.currentIndex)

    this.change.emit(this.buttons)
  }

  async moveButton(previousIndex, currentIndex) {
    moveItemInArray(this.buttons, previousIndex, currentIndex);
    moveItemInArray(this.buttonComps, previousIndex, currentIndex);
    this.updateIndex()
  }

  updateIndex() {
    for (let i = 0; i < this.buttonComps.length; i++) {
      this.buttonComps[i].index = i
    }
  }

  trackByFn(index, item) {
    return item.buttonRefId; // or item.id
  }
}
