import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonContainerComponent } from 'app/shared/buttons/button-container/button-container.component';
import { MatListModule, MatButtonModule, MatTooltipModule } from '@angular/material';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';
import { SelectionModule } from 'app/modals/selection/selection.module';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  imports: [
    CommonModule,
    MatListModule,
    MatButtonModule,
    MatTooltipModule,
    EditInfoModule,
    SelectionModule,
    DragDropModule
  ],
  declarations: [ButtonContainerComponent],
  exports: [ButtonContainerComponent]
})
export class ButtonContainerModule { }
