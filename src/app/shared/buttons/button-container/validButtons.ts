export interface IValidButtons {
    web_url?: any,
    postback?: any,
    phone_number?: any,
    nested?: any,
    quick_reply_text?: any,
    quick_reply_location?: any,
    // order_innoway?: any,
    // basket_innoway?: any,
    // profile_innoway?: any,
    // simple_checkout_innoway?: any,
    google_map?: any,
    about_topic?: any,
    quick_reply_topic?: any,
    quick_reply_activity?: any,
    quick_reply_email?: any,
    quick_reply_phone?: any,
    element_share?: any,
    live_chat?: any,
    deeplink?: any,
    activity?: any,
    unsubscribe_topic?: any,
    lucky_wheel?: any,
    extension_order?:any,
    extension_profile?: any,
    extension_booking?: any,
    extension_reward?: any
    extension_history?: any
}

// butotn activity card
export const VALID_BUTTONS_1: IValidButtons = {
    web_url: "UrlButtonComponent",
    postback: "PostbackButtonComponent",
    phone_number: "PhoneButtonComponent",
    // order_innoway: "UrlButtonComponent",
    // basket_innoway: "UrlButtonComponent",
    google_map: "UrlButtonComponent",    
    element_share: "ShareButtonComponent",
    // profile_innoway: "UrlButtonComponent",
    // simple_checkout_innoway: "UrlButtonComponent",
    live_chat: "PostbackButtonComponent",
    deeplink: "DeeplinkButtonComponent",
    activity: "PostbackButtonComponent",
    extension_order:"UrlButtonComponent",
    extension_profile:"UrlButtonComponent",
    extension_booking:"UrlButtonComponent",
    extension_reward:"UrlButtonComponent",
    extension_history:"UrlButtonComponent",
}

// quick reply card and card advanced
export const VALID_BUTTONS_2: IValidButtons = {
    quick_reply_text: "TextQuickReplyButtonComponent",
    // quick_reply_topic: "TextQuickReplyButtonComponent",
}

// everything else
export const VALID_BUTTONS_3: IValidButtons = {
    web_url: "UrlButtonComponent",
    postback: "PostbackButtonComponent",
    phone_number: "PhoneButtonComponent",
    // order_innoway: "UrlButtonComponent",
    // basket_innoway: "UrlButtonComponent",
    google_map: "UrlButtonComponent",
    element_share: "ShareButtonComponent",
    // profile_innoway: "UrlButtonComponent",
    // simple_checkout_innoway: "UrlButtonComponent",
    live_chat: "PostbackButtonComponent",
    deeplink: "DeeplinkButtonComponent",
    lucky_wheel: "WheelButtonComponent",
    extension_order:"UrlButtonComponent",
    extension_profile:"UrlButtonComponent",
    extension_booking:"UrlButtonComponent",
    extension_reward:"UrlButtonComponent",
    extension_history:"UrlButtonComponent",
}

//quick reply activity card
export const VALID_BUTTONS_4: IValidButtons = {
    quick_reply_text: "TextQuickReplyButtonComponent",
    quick_reply_activity: "QuickReplyActivityButtonComponent",
    quick_reply_email: "LocationQuickReplyButtonComponent",
    quick_reply_phone: "LocationQuickReplyButtonComponent",
    quick_reply_location: "LocationQuickReplyButtonComponent",
}

//menu card
export const VALID_BUTTONS_5: IValidButtons = {
    web_url: "UrlButtonComponent",
    postback: "PostbackButtonComponent",
    // order_innoway: "UrlButtonComponent",
    // basket_innoway: "UrlButtonComponent",
    google_map: "UrlButtonComponent",
    nested: "NestedButtonComponent",
    // profile_innoway: "UrlButtonComponent",
    // simple_checkout_innoway: "UrlButtonComponent",
    live_chat: "PostbackButtonComponent",
    extension_order:"UrlButtonComponent",
    extension_profile:"UrlButtonComponent",
    extension_booking:"UrlButtonComponent",
    extension_reward:"UrlButtonComponent",
    extension_history:"UrlButtonComponent",
    // deeplink: "DeeplinkButtonComponent",
}

//menu card last menu
export const VALID_BUTTONS_6: IValidButtons = {
    web_url: "UrlButtonComponent",
    postback: "PostbackButtonComponent",
    // order_innoway: "UrlButtonComponent",
    // basket_innoway: "UrlButtonComponent",
    google_map: "UrlButtonComponent",
    // profile_innoway: "UrlButtonComponent",
    // simple_checkout_innoway: "UrlButtonComponent",
    live_chat: "PostbackButtonComponent",
    extension_order:"UrlButtonComponent",
    extension_profile:"UrlButtonComponent",
    extension_booking:"UrlButtonComponent",
    extension_reward:"UrlButtonComponent",
    extension_history:"UrlButtonComponent",
    // deeplink: "DeeplinkButtonComponent",
}

export const BUTTONS_LIST = [
    { code: 'web_url', display: "Mở trang web", icon: 'fas fa-globe' },
    { code: 'phone_number', display: "Gọi điện thoại", icon: 'fas fa-phone' },
    { code: 'element_share', display: "Chia sẻ", icon: 'fas fa-share-alt' },
    { code: 'nested', display: "Menu mở rộng", icon: 'fas fa-bars' },
    { code: 'postback', display: "Câu chuyện", icon: 'fas fa-book' },
    { code: 'about_topic', display: "Chủ đề", icon: 'fas fa-calendar-alt' },
    { code: "unsubscribe_topic", display: "Huỷ theo dõi chủ đề", icon: 'fas fa-ban' },
    { code: 'order_innoway', display: "Đặt hàng", icon: 'fas fa-cart-plus' },
    { code: 'basket_innoway', display: "Giỏ hàng", icon: 'fas fa-shopping-basket' },
    { code: 'google_map', display: "Mở Google Map", icon: 'fas fa-map' },
    { code: 'quick_reply_text', display: "Trả lời câu chuyện", icon: 'fas fa-book' },
    { code: 'quick_reply_topic', display: "Trả lời chủ đề", icon: 'far fa-comment-alt' },
    { code: 'quick_reply_location', display: "Trả lời địa chỉ", icon: 'fas fa-map-marker' },
    { code: 'quick_reply_activity', display: "Trả lời nhanh", icon: 'fas fa-comments' },
    { code: 'quick_reply_email', display: "Trả lời email", icon: 'fas fa-at' },
    { code: 'quick_reply_phone', display: "Trả lời phone", icon: 'fas fa-phone' },
    { code: 'profile_innoway', display: "Thông tin cá nhân", icon: 'fas fa-user-circle' },
    { code: 'simple_checkout_innoway', display: "Thanh toán nhanh", icon: 'far fa-money-bill-alt' },
    { code: 'live_chat', display: "Chat trực tiếp", icon: 'fas fa-comments' },
    { code: 'deeplink', display: "Deeplink", icon: 'fas fa-link' },
    { code: 'activity', display: "Trả lời tương tác", icon: 'fas fa-comment' },
    { code: 'lucky_wheel', display: "Vòng quay", icon: 'fas fa-dharmachakra' },
    { code: 'extension_order', display: "Shop", icon: 'fas fa-store' },
    { code: 'extension_profile', display: "Trang Cá Nhân", icon: 'fas fa-user' },
    { code: 'extension_booking', display: "Đặt lịch", icon: 'far fa-clock' },
    { code: 'extension_reward', display: "Xem ưu đãi", icon: 'fas fa-gift' },
    { code: 'extension_history', display: "Lịch sử đơn hàng", icon: 'fas fa-receipt' },
]

export const BUTTONS_OBJECT = BUTTONS_LIST.reduce((obj, item) => { obj[item.code] = { display: item.display, icon: item.icon }; return obj }, {})