import * as guid from "guid";
import { get } from "lodash-es";
import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef
} from "@angular/core";
import { Subscription, BehaviorSubject } from "rxjs";
import { MatSnackBar, MatDialog } from "@angular/material";
import { PortalService } from "app/services/portal.service";
import { ButtonService, ButtonContainerRef } from "app/services/button.service";
import { ButtonContainerComponent } from "../button-container/button-container.component";
import { BUTTONS_OBJECT } from "../button-container/validButtons";
import { ButtonFormService } from "../button-form/button-form.service";
import { SimpleCheckoutDialog } from "app/modals/simple-checkout/simple-checkout.component";
import { ChatbotApiService, iStory } from "app/services/chatbot";
import { StoryDetailPortalComponent } from "app/chatgut-app/portals/story-detail-portal/story-detail-portal.component";
@Component({
  selector: "app-button-component",
  templateUrl: "./button-component.component.html",
  styleUrls: ["./button-component.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonComponent implements OnInit {
  @Input() type:
    | "web_url"
    | "phone_number"
    | "element_share"
    | "nested"
    | "postback"
    | "about_topic"
    | "lucky_wheel"
    | "unsubscribe_topic"
    | "order_innoway"
    | "basket_innoway"
    | "google_map"
    | "quick_reply_text"
    | "story"
    | "quick_reply_topic"
    | "quick_reply_location"
    | "quick_reply_activity"
    | "quick_reply_email"
    | "quick_reply_phone"
    | "profile_innoway"
    | "simple_checkout_innoway"
    | "live_chat"
    | "deeplink"
    | "activity"
    | "extension_order"
    | "extension_profile"
    | "extension_booking"
    | "extension_reward"
    | "extension_history"
  @Input() readOnly: boolean = false;
  @Input() button: any;
  @Output() onClick = new EventEmitter<any>();

  hasInitted: boolean = false;
  display: string;
  icon: string;
  title: string;
  disabled: boolean = false;
  viewInited: BehaviorSubject<number>;
  subscriptions: { [key: string]: Subscription } = {};
  index: number;
  buttonRefId: string = guid.raw();
  containerId: string = guid.raw();
  containerType: "card" | "quick_reply" | "menu";
  isQuickReplyButton: boolean = false;
  payload: any = {};
  description: string = "";
  isNextOpened: boolean = false;

  get dragData() {
    return {
      type: "button-order",
      buttonIndex: this.index,
      button: this.button
    };
  }
  get containerRef(): ButtonContainerRef {
    return this.buttonSrv.containers[this.containerId];
  }
  get container(): ButtonContainerComponent {
    console.log(this.containerRef)
    return get(this.containerRef, "container");
  }
  get portalIndex() {
    return this.portalService.container.swiperWrapper.indexOf(
      this.container.container.parentViewRef
    );
  }

  constructor(
    public changeRef: ChangeDetectorRef,
    private snackBar: MatSnackBar,
    private portalService: PortalService,
    private buttonSrv: ButtonService,
    private buttonForm: ButtonFormService,
    private dialog: MatDialog,
    private chatbotApi: ChatbotApiService
  ) {}

  ngOnInit() {}

  ngAfterContentInit() {
    setTimeout(() => {
      this.viewInited.next(this.index);
    });
  }

  ngOnDestroy() {
    for (let sub in this.subscriptions) {
      this.subscriptions[sub].unsubscribe();
    }
  }

  init(button, containerType, readOnly = false) {
    this.containerType = containerType;
    this.readOnly = readOnly;
    this.button = button;

    this.checkType();
    this.display = BUTTONS_OBJECT[this.type].display;
    this.icon = BUTTONS_OBJECT[this.type].icon;

    // add description
    try {
      switch (this.type) {
        case "web_url": {
          this.description = this.button.url;
          break;
        }
        case "phone_number": {
          this.description = this.button.payload;
          break;
        }
        case "postback":
        case "quick_reply_text": {
          this.getStoryDescription();
          break;
        }
        case "lucky_wheel": {
          let stories = (this.container.container
            .parentPortal as StoryDetailPortalComponent).stories as iStory[];
          if (stories && stories.length) {
            let storyId = this.button.storyId;
            if (storyId) {
              let index = stories.findIndex(x => x._id == storyId);
              if (index >= 0 && stories[index])
                this.description = stories[index].name;
            }
          }
          break;
        }
        case "activity": {
          this.description = this.payload.data;
          break;
        }
        case "quick_reply_activity": {
          this.description = this.button.value;
          break;
        }
        case "element_share": {
          this.description = this.element.title;
          break;
        }
        case "deeplink": {
          this.description = this.button.webUrl;
          break;
        }
      }
    } catch (err) {
      console.error("small error", err);
    }

    this.hasInitted = true;
    this.changeRef.detectChanges();
  }

  checkType() {
    this.type = this.button.type;
    if (this.button.payload) {
      try {
        let parsed = JSON.parse(this.button.payload);
        this.payload = parsed;
      } catch (err) {}
    }
    switch (this.type) {
      case "postback": {
        if (this.payload.type && this.payload.type != "story")
          this.type = this.payload.type;
        break;
      }
      case "web_url": {
        let url = this.button.url as string;
        if (/\/deeplink\/gmap/g.test(url)) {
          this.type = "google_map";
        } else if (/\/innoway\/profile/g.test(url)) {
          this.type = "profile_innoway";
        } else if (/view\/v1\/innoway\?path=category/g.test(url)) {
          this.type = "order_innoway";
        } else if (/view\/v1\/innoway\?path=basket/g.test(url)) {
          this.type = "basket_innoway";
        } else if (/view\/v1\/innoway\/simpleCheckout/g.test(url)) {
          this.type = "simple_checkout_innoway";
        } else if (/bot-static\.m-co\.me/g.test(url)) {
          if (url.includes("order")) this.type = "extension_order";
          else if (url.includes("profile")) this.type = "extension_profile";
          else if (url.includes("booking")) this.type = "extension_booking";
          else if (url.includes("reward")) this.type = "extension_reward";
          else if (url.includes("history")) this.type = "extension_history";
        }
        break;
      }
      case "element_share": {
        this.title = "Chia sẻ";
        break;
      }
      case "quick_reply_email": {
        this.title = "Lấy email";
        break;
      }
      case "quick_reply_location": {
        this.title = "Lấy toạ độ";
        break;
      }
      case "quick_reply_phone": {
        this.title = "Lấy điện thoại";
        break;
      }
      default:
        break;
    }
    return this.type;
  }

  async defaultAction(event) {
    if (this.type == "nested") {
      this.container.onButtonClick.emit(this);
    } else {
      let { button, onSubmit, onDelete, onCopy } = this;
      this.buttonForm.editButton({
        event,
        button,
        onSubmit,
        onDelete,
        onCopy,
        removeTag: this.containerType == "menu"
      });
    }
  }

  async edit() {
    if (this.type == "nested") {
      let { button, onSubmit, onDelete, onCopy } = this;
      this.buttonForm.editButton({
        event,
        button,
        onSubmit,
        onDelete,
        onCopy,
        removeTag: this.containerType == "menu"
      });
    } else {
      let story = await this.getStory(this.payload.data);
      if (story._id && story._id != this.payload.data) {
        this.payload.data = story._id;
        this.button.payload = JSON.stringify(this.payload);
        this.description = story.name;
        this.container.onButtonChange(this.index);
      }
      this.changeRef.detectChanges();
    }
  }

  onSubmit = data => {
    if (data) this[this.type](data);
  };

  quick_reply_location = data => {};
  quick_reply_email = data => {};
  quick_reply_phone = data => {};

  deeplink = data => {
    // if (data.title == this.button.title && data.tag == this.button.buttonTag
    // && data.webUrl == this.button.webUrl && data.androidUrl == this.button.androidUrl && data.iosUrl == this.button.iosUrl
    // && data.androidStoreUrl == this.button.androidStoreUrl && data.iosStoreUrl == this.button.iosStoreUrl) return

    this.button.title = data.title;
    this.button.buttonTag = data.tag;
    this.button.webUrl = data.webUrl;
    this.button.androidUrl = data.androidUrl;
    this.button.iosUrl = data.iosUrl;
    this.button.androidStoreUrl = data.androidStoreUrl;
    this.button.iosStoreUrl = data.iosStoreUrl;
    this.description = this.button.webUrl;
    this.container.onButtonChange(this.index);
  };

  nested = data => {
    // if (data.title == this.button.title) return

    this.button.title = data.title;
    this.container.onButtonChange(this.index);
  };

  phone_number = data => {
    // if (data.title == this.button.title && data.tag == this.button.buttonTag
    // && data.phone == this.button.payload) return

    this.button.title = data.title;
    this.button.buttonTag = data.tag;
    this.button.payload = data.phone;
    this.container.onButtonChange(this.index);

    this.description = this.button.payload;
  };

  postback = data => {
    // if (data.title == this.button.title && data.tag == this.button.buttonTag
    // && data.story == this.payload.data && data.additional == this.payload.additional) return

    this.button.title = data.title;
    this.button.buttonTag = data.tag;
    this.payload.data = data.story;
    this.payload.additional = data.additional;
    this.button.payload = JSON.stringify(this.payload);

    this.getStoryDescription();
    this.container.onButtonChange(this.index);
  };

  about_topic = data => {
    // if (data.title == this.button.title && data.tag == this.button.buttonTag && data.topicId == this.payload.data.topicId
    // && data.storyId == this.payload.data.storyId && data.additional == this.payload.additional) return

    this.button.title = data.title;
    this.button.buttonTag = data.tag;
    this.payload.data.storyId = data.storyId;
    this.payload.data.topicId = data.topicId;
    this.payload.additional = data.additional;
    this.button.payload = JSON.stringify(this.payload);
    this.container.onButtonChange(this.index);
  };

  live_chat = data => {
    // if (data.title == this.button.title && data.tag == this.button.buttonTag) return

    this.button.title = data.title;
    this.button.buttonTag = data.tag;
    this.container.onButtonChange(this.index);
  };

  activity = data => {
    // if (data.title == this.button.title && data.tag == this.button.buttonTag
    // && data.value == this.payload.data) return

    this.button.title = data.title;
    this.button.buttonTag = data.tag;
    this.payload.data = data.value;
    this.button.payload = JSON.stringify(this.payload);

    this.description = this.payload.data;
    this.container.onButtonChange(this.index);
  };

  quick_reply_activity = data => {
    // if (data.title == this.button.title && data.tag == this.button.buttonTag
    // && data.icon == this.button.image_url && data.value == this.button.value) return

    this.button.title = data.title;
    this.button.buttonTag = data.tag;
    this.button.image_url = data.icon;
    this.button.value = data.value;

    this.description = this.button.value;
    this.container.onButtonChange(this.index);
  };

  element_share = data => {
    // if (data.shareTitle == this.element.title && data.shareSubtitle == this.element.subtitle
    // && data.shareImage == this.element.image_url && data.shareButton == this.element.buttons[0].title
    // && data.shareLink == this.element.buttons[0].url) return

    this.element.title = data.shareTitle;
    this.element.subtitle = data.shareSubtitle;
    this.element.image_url = data.shareImage;
    this.element.default_action.url = data.shareLink;
    this.element.buttons[0].url = data.shareLink;
    this.element.buttons[0].title = data.shareButton;
    this.description = this.element.title;
    this.container.onButtonChange(this.index);
  };

  quick_reply_text = data => {
    // if (data.title == this.button.title && data.tag == this.button.buttonTag
    // && data.icon == this.button.image_url && data.additional == this.payload.additional
    // && data.story == this.payload.data) return

    this.button.title = data.title;
    this.button.buttonTag = data.tag;
    this.button.image_url = data.icon;
    this.payload.data = data.story;
    this.payload.additional = data.additional;
    this.button.payload = JSON.stringify(this.payload);

    this.getStoryDescription();

    this.container.onButtonChange(this.index);
  };

  web_url = data => {
    // if (data.title == this.button.title && data.tag == this.button.buttonTag
    // && data.url == this.button.url && data.messenger_extensions == this.button.messenger_extensions) return

    this.button.title = data.title;
    this.button.buttonTag = data.tag;
    this.button.url = data.url;
    this.button.messenger_extensions = data.messenger_extensions;
    this.description = this.button.url;
    this.container.onButtonChange(this.index);
  };

  extension_order = data => this.executeExtension
  extension_profile = data => this.executeExtension
  extension_booking = data => this.executeExtension
  extension_reward = data => this.executeExtension
  extension_history = data => this.executeExtension

  executeExtension = data => {
    this.button.title = data.title;
    this.button.buttonTag = data.tag;
    this.container.onButtonChange(this.index);
  }

  order_innoway = data => {
    // if (data.title == this.button.title && data.tag == this.button.buttonTag) return

    this.button.title = data.title;
    this.button.buttonTag = data.tag;
    this.container.onButtonChange(this.index);
  };

  basket_innoway = data => {
    // if (data.title == this.button.title && data.tag == this.button.buttonTag) return

    this.button.title = data.title;
    this.button.buttonTag = data.tag;
    this.container.onButtonChange(this.index);
  };

  profile_innoway = data => {
    // if (data.title == this.button.title && data.tag == this.button.buttonTag) return

    this.button.title = data.title;
    this.button.buttonTag = data.tag;
    this.container.onButtonChange(this.index);
  };

  simple_checkout_innoway = data => {
    this.editSimpleCheckoutButton();
  };

  google_map = data => {
    const tlat = this.getQueryString("tlat", this.button.url);
    const tlng = this.getQueryString("tlng", this.button.url);
    // if (data.title == this.button.title && data.tag == this.button.buttonTag
    // && data.lat == tlat && data.lng == tlng ) return

    this.button.title = data.title;
    this.button.buttonTag = data.tag;
    this.chatbotApi.deeplink
      .getGoogleMapDeeplink({
        to: {
          latitude: data.lat,
          longitude: data.lng
        }
      })
      .then(result => {
        this.button.url = result;
      });
  };

  lucky_wheel = data => {
    // if (data.title == this.button.title && data.tag == this.button.buttonTag && data.btnTitle == this.button.btnTitle
    // && data.multi == this.button.multi.toString() && data.turn == this.button.turn && data.wheel._id == this.button.luckyWheelId
    // && data.giftId == this.button.giftId && data.successRatio == this.button.successRatio && data.storyId == this.button.storyId) return

    this.button.title = data.title;
    this.button.buttonTag = data.tag;
    this.button.btnTitle = data.btnTitle;
    this.button.multi = data.multi == "true" ? true : false;
    this.button.turn = data.turn;
    this.button.luckyWheelId = data.wheel._id;
    this.button.giftId = data.giftId;
    this.button.successRatio = data.successRatio;
    this.button.storyId = data.storyId;

    let stories = (this.container.container
      .parentPortal as StoryDetailPortalComponent).stories as iStory[];
    if (stories && stories.length) {
      let storyId = this.button.storyId;
      if (storyId) {
        let index = stories.findIndex(x => x._id == storyId);
        if (index >= 0 && stories[index])
          this.description = stories[index].name;
      }
    }

    this.container.onButtonChange(this.index);
  };

  async copy() {
    let buttonObject = { button: true, data: this.button };
    let buttonData = JSON.stringify(buttonObject);
    sessionStorage.setItem("buttonData", buttonData);
    this.snackBar.open(
      "✔️ Đã copy dữ liệu nút. Chọn thêm nút để paste dữ liệu",
      "",
      {
        duration: 2500
      }
    );
  }

  get element() {
    return this.button.share_contents.attachment.payload.elements[0];
  }

  setDiasble(value) {
    this.disabled = value;
    this.changeRef.detectChanges();
  }

  getQueryString(field, url) {
    let reg = new RegExp("[?&]" + field + "=([^&#]*)", "i");
    let string = reg.exec(url);
    return string ? string[1] : null;
  }

  async editSimpleCheckoutButton() {
    const result = await this.dialog
      .open(SimpleCheckoutDialog, {
        width: "500px",
        data: this.button
      })
      .afterClosed()
      .toPromise();
    if (result) {
      this.button = result;
      this.container.onButtonChange(this.index);
    }
  }

  getStory(ids: string[]) {
    return new Promise<any>(async (resolve, reject) => {
      const componentRef = await this.portalService.pushPortalAt(
        this.portalIndex + 1,
        "StoriesPortalComponent",
        {
          select: true,
          multi: false,
          selectedStories: ids,
          returnIdOnly: false
        }
      );
      this.isNextOpened = true;
      this.changeRef.detectChanges();

      if (!componentRef) return;
      const component = componentRef.instance as any;

      if (this.subscriptions.savedSubscription)
        this.subscriptions.savedSubscription.unsubscribe();
      this.subscriptions.savedSubscription = component.onStorySaved.subscribe(
        selectedStory => {
          resolve(selectedStory);
          component.hideSave();
          component.close();
        }
      );
      if (this.subscriptions.closedSubscription)
        this.subscriptions.closedSubscription.unsubscribe();
      this.subscriptions.closedSubscription = component.onStoryClosed.subscribe(
        () => {
          this.isNextOpened = false;
          this.changeRef.detectChanges();
          this.subscriptions.savedSubscription.unsubscribe();
          this.subscriptions.closedSubscription.unsubscribe();
        }
      );
    });
  }

  getStoryDescription() {
    if (this.containerType != "menu") {
      let stories = (this.container.container
        .parentPortal as StoryDetailPortalComponent).stories as iStory[];
      if (stories && stories.length) {
        let storyId = this.payload.data;
        if (storyId) {
          let index = stories.findIndex(x => x._id == storyId);
          if (index >= 0 && stories[index])
            this.description = stories[index].name;
        }
      }
    }
  }

  detectChanges() {
    this.changeRef.detectChanges();
  }

  onDelete = () => {
    this.container.popButtonComp(this.index);
  };
  onCopy = () => {
    this.copy();
  };
}
