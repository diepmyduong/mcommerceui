import { NgModule } from '@angular/core';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatTooltipModule, MatSnackBarModule } from '@angular/material';
import { ButtonComponent } from './button-component.component';
import { CommonModule } from '@angular/common';
import { SimpleCheckoutModule } from 'app/modals/simple-checkout/simple-checkout.module';

@NgModule({
  imports: [
    CommonModule,
    MatTooltipModule,
    MatSnackBarModule,
    SimpleCheckoutModule
  ],
  declarations: [ButtonComponent],
  entryComponents: [ButtonComponent],
  exports: [],
  providers: [
    { provide: DYNAMIC_COMPONENT, useValue: ButtonComponent }
  ]
})
export class ButtonComponentModule { }
