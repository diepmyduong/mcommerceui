import { get, forEach } from 'lodash-es'
import * as Console from 'console-prefix'
import * as guid from 'guid'
import { Input, Output, EventEmitter, ViewRef, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject ,  Subscription } from 'rxjs'
import { PortalService } from 'app/services/portal.service';
import { ServicesInjector } from 'app/services/serviceInjector';
import { ButtonService, ButtonContainerRef } from 'app/services/button.service';
import { ButtonContainerComponent } from 'app/shared/buttons/button-container/button-container.component';
import { MatSnackBar } from '@angular/material';
export class BaseButton {
    @Input() readOnly: boolean = false
    @Input() button: any
    @Output() onClick = new EventEmitter<any>()
    constructor() { }
    containerId: string
    portalService: PortalService = ServicesInjector.get(PortalService);
    buttonSrv: ButtonService = ServicesInjector.get(ButtonService);
    disabled: boolean = false
    viewInited: BehaviorSubject<number>
    subscriptions: { [key: string]: Subscription } = {};
    index: number
    buttonComps: any[]
    get dragData() { return { type: 'button-order', buttonIndex: this.index, button: this.button } }
    get log() { return Console(`[Button-${this.index}]`).log; }
    get containerRef(): ButtonContainerRef { return this.buttonSrv.containers[this.containerId] }
    get container(): ButtonContainerComponent { return get(this.containerRef,'container'); }
    get portalIndex() { return this.portalService.container.swiperWrapper.indexOf(this.container.container.parentViewRef) }
    // get index() { return this.container?this.container.buttonViewContainer.indexOf(this.viewRef):-1;  }
    buttonRefId: string = guid.raw()

    get _changeRef(): ChangeDetectorRef { return (<any>this).changeRef; }
    detectChanges() { this._changeRef?this._changeRef.detectChanges(): null; }
    get _snackBar(): MatSnackBar { return (<any>this).snackBar; }

    ngAfterContentInit() { setTimeout(() => { this.viewInited.next(this.index) }) }
    ngOnDestroy() { forEach(this.subscriptions, (sub: Subscription) => { sub.unsubscribe() }) }
    close() { this.container.popButtonComp(this.index) }

    async defaultAction(button) {
        // this.onClick.emit(this.button)
        // this.container.onButtonClick.emit(this)
    }

    async copy() {
        let buttonObject = { button: true, data: this.button }
        let buttonData = JSON.stringify(buttonObject)
        sessionStorage.setItem('buttonData', buttonData)
        this._snackBar.open('✔️ Đã copy dữ liệu nút. Chọn thêm nút để paste dữ liệu', '', {
            duration: 2500,
        });
    }

    onDelete = () => { this.close() }
    onCopy = () => { this.copy() }
}