import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DYNAMIC_COMPONENT } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { MatFormFieldModule, MatSelectModule, MatButtonToggleModule } from '@angular/material';
import { NgJsonEditorModule } from 'ang-jsoneditor';
import { ButtonForm } from './button-form.component';
import { ButtonFormService } from './button-form.service';
import { HightlightTextareaModule } from 'app/directives/hightlight-textarea/hightlight-textarea.module';
import { StorySelectionModule } from 'app/components/story-selection/story-selection.module';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    MatFormFieldModule,
    MatButtonToggleModule,
    MatSelectModule,
    NgJsonEditorModule,
    StorySelectionModule,
    HightlightTextareaModule,
    TextFieldModule
  ],
  declarations: [ButtonForm],
  exports: [ButtonForm],
  entryComponents: [ButtonForm],
  providers: [ButtonFormService,
      { provide: DYNAMIC_COMPONENT, useValue: ButtonForm }
    ]
})
export class ButtonFormModule {
}
