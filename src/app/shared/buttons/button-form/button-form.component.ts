import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Output, EventEmitter, ViewChild} from '@angular/core';
import { cloneDeep, isEqual } from 'lodash-es'
import { PortalService } from 'app/services/portal.service';
import { Subscription } from 'rxjs';
import { StorySelectionComponent } from 'app/components/story-selection/story-selection.component';

@Component({
    selector: 'button-form',
    styleUrls: ['./button-form.component.scss'],
    templateUrl: './button-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonForm implements OnInit {

    @Output() change: EventEmitter<any> = new EventEmitter<any>()
    @Output() close: EventEmitter<any> = new EventEmitter<any>()
    @ViewChild(StorySelectionComponent) storySelection: StorySelectionComponent

    formOptions: iButtonFormOptions = { }

    originalData: any = {}
    data: any = {}
    active: boolean = false
    changeTimeout: any

    hasChanged: boolean = false
    hasError: boolean = false
    errors: any = {}

    closeSubscription: Subscription

    constructor(
        public ref: ChangeDetectorRef,
        private portalService: PortalService
    ) {
    }

    ngOnInit() {
    }

    public setFormOptions(options: iButtonFormOptions) {
        this.formOptions = { ...options }
        this.data = {}
        this.formOptions.fields.forEach(field => {
            if (!field.type) field.type = 'text'
            this.data[field.property] = field.current
            this.originalData = cloneDeep(this.data)

            if (field.setupOtherPropertyOptions) {
                let otherField = this.formOptions.fields.find(x => x.property == field.setupOtherPropertyOptions.property)
                otherField.options = [ { code: null, display: 'Không chọn'}, 
                    ...this.data[field.property]?this.data[field.property][field.setupOtherPropertyOptions.sourceValue]
                    .map(x => ({ code: x._id, display: x.name })):[] ]
            }

            if (field.disableOtherProperty) {
                for (let disableOtherProperty of field.disableOtherProperty) {
                    let otherField = this.formOptions.fields.find(x => x.property == disableOtherProperty.property)
                    if (!otherField.disabled && (disableOtherProperty.value && this.data[field.property] == disableOtherProperty.value) ||
                    disableOtherProperty.hasValue == !!this.data[field.property]) {
                        this.data[disableOtherProperty.property] = null
                        otherField.disabled = true
                    }
                }
            }
        })

        this.ref.detectChanges()
    }

    onYesClick() {
        if (this.checkErrors()) return
        this.change.emit(this.data)
        this.close.emit()
    }

    onNoClick() {
        if (this.formOptions.mode == 'edit' && this.checkErrors()) return 
        
        this.close.emit()
    }

    onDeleteClick() {
        this.formOptions.onDelete()
        this.close.emit()
    }

    onCopyClick() {
        this.formOptions.onCopy()
    }

    inputChanged(field: iButtonFormField) {
        this.hasChanged = !isEqual(this.data, this.originalData)

        if (field.setupOtherPropertyOptions) {
            let otherField = this.formOptions.fields.find(x => x.property == field.setupOtherPropertyOptions.property)
            otherField.options = [ { code: null, display: 'Không chọn'}, 
                ...this.data[field.property]?this.data[field.property][field.setupOtherPropertyOptions.sourceValue]
                .map(x => ({ code: x._id, display: x.name })):[] ]
            this.data[otherField.property] = null
        }
        
        if (field.disableOtherProperty) {
            for (let disableOtherProperty of field.disableOtherProperty) {
                if ((disableOtherProperty.value != undefined && this.data[field.property] == disableOtherProperty.value) ||
                disableOtherProperty.hasValue == !!this.data[field.property]) {
                    this.data[disableOtherProperty.property] = null
                    this.formOptions.fields.find(x => x.property == disableOtherProperty.property).disabled = true
                } else {
                    this.formOptions.fields.find(x => x.property == disableOtherProperty.property).disabled = false
                }
            }
        }

        if (this.errors[field.property]) {
            this.checkErrors()
            this.checkHasError()
        }

        this.editChanged()
    }

    editChanged() {
        if (this.formOptions.mode == 'edit') {
            if (this.changeTimeout) clearTimeout(this.changeTimeout)
            this.changeTimeout = setTimeout(() => {
                this.change.emit(this.data)
                clearTimeout(this.changeTimeout)
            }, 500)
        }
    }

    onChanged(event, property) {
        this.data[property] = event

        this.hasChanged = !isEqual(this.data, this.originalData)
        if (this.errors[property]) {
            this.checkErrors()
            this.checkHasError()
        }

        this.editChanged()
    }

    onPasteClick() {
        switch(this.formOptions.pasteData.type) {
            case 'quick_reply_text': {
                let payload = JSON.parse(this.formOptions.pasteData.payload)
                console.log(payload)
                this.data['title'] = this.formOptions.pasteData.title
                this.data['tag'] = this.formOptions.pasteData.buttonTag
                this.data['icon'] = this.formOptions.pasteData.image_url
                this.data['story'] = payload.data
                this.data['additional'] = payload.additional
                this.storySelection.changeId(payload.data)
                this.ref.detectChanges()
                break
            }
        }

        this.editChanged()
    }

    public checkErrors() {
        // iterate the data fields
        for (let field of this.formOptions.fields) {
            // check if there is any constraint of them, if not skip
            if (!field.constraints || !field.constraints.length) continue

            if (field.constraints.includes('required')) {
                if (!this.data[field.property]) {
                    this.errors[field.property] = 'Bắt buộc'
                } else {
                    this.errors[field.property] = ''
                }
            }
            
            if (field.constraints.includes('url')) {
                let regex = new RegExp(/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/g)
                if (!regex.test(this.data[field.property])) {
                    this.errors[field.property] = 'Link không hợp lệ'
                } else {
                    this.errors[field.property] = ''
                }
            }
            
            if (field.constraints.includes('phone')) {
                let regex = new RegExp(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/g)
                if (!regex.test(this.data[field.property])) {
                    this.errors[field.property] = 'Số điện thoại không hợp lệ'
                } else {
                    this.errors[field.property] = ''
                }
            }
        }

        return this.checkHasError()
    }

    checkHasError() {
        let flag = false
        for (let key in this.errors) {
            if (this.errors.hasOwnProperty(key) && this.errors[key]) {
                flag = true
                break
            }
        }

        this.hasError = flag
        return this.hasError
    }

    async openPortal(field) {
        let params = {}
        if (field.current && this.data[field.current.property]) {
            params[field.current.propertyName] = this.data[field.current.property][field.current.value]
        }
        const componentRef = await this.portalService.pushPortal(field.property, params)
        const component = componentRef.instance as any;
        this.active = true
        if (component.onClosed) {
            this.closeSubscription = component.onClosed.subscribe(() => {
                this.active = false
                this.closeSubscription.unsubscribe();
            })
        }
    }

}

export interface iButtonFormOptions {
    mode?: 'add' | 'edit'
    parent?: string
    target?: string
    targetElement?: HTMLElement
    width?: number | string
    title?: string
    icon?: string
    confirm?: string
    cancel?: string
    fields?: iButtonFormField[]
    horizontalAlign?: 'left' | 'right' | 'auto' | 'center'
    verticalAlign?: 'top' | 'bottom' | 'auto' | 'center'
    pasteData?: any
    onSubmit?: (data: any) => any
    onDelete?: () => any
    onCopy?: () => any
}

export interface iButtonFormField {
    type?: 'text' | 'icon' | 'story' | 'tag' | 'image' | 'topic' | 'select' | 'textarea'
    col?: number
    constraints?: ('required' | 'url' | 'phone')[]
    placeholder?: any
    property?: any
    current?: any
    error?: any
    additional?: any
    options?: { code: any, display: string }[]
    setupOtherPropertyOptions?: { property: string, sourceValue: string }
    disabled?: boolean
    disableOtherProperty?: { property: string, value?: string, hasValue?: boolean }[]
}