import {
  Injectable,
  ApplicationRef,
  ViewContainerRef,
  ComponentRef
} from "@angular/core";
import { Subscription } from "rxjs";
import { AppComponent } from "app/app.component";
import { iButtonFormOptions, ButtonForm } from "./button-form.component";
import { DynamicComponentLoaderService } from "app/services/dynamic-component-loader/dynamic-component-loader.service";
import { ChatbotApiService } from "app/services/chatbot";
import { BUTTONS_OBJECT } from "../button-container/validButtons";

@Injectable({
  providedIn: "root"
})
export class ButtonFormService {
  buttonFormRef: ComponentRef<ButtonForm>;
  buttonFormComp: ButtonForm;
  changeSubscription: Subscription;
  closeSubscription: Subscription;
  viewRef: ViewContainerRef;
  clickHandler: EventListener;
  constructor(
    private dynamicComponentLoader: DynamicComponentLoaderService,
    private appRef: ApplicationRef,
    private chatbotApi: ChatbotApiService
  ) {
    this.viewRef = (this.appRef.components[0]
      .instance as AppComponent).viewContainerRef;
  }

  public async createButtonForm(options: iButtonFormOptions) {
    try {
      document.removeEventListener("mousedown", this.clickHandler);
      // get form by id
      let buttonForm = document.getElementById("button-form");
      // if no form or form ref found
      if (!this.buttonFormRef || !buttonForm) {
        if (this.buttonFormRef) {
          this.buttonFormRef.destroy();
        }
        let componentFactory = await this.dynamicComponentLoader.getComponentFactory(
          "ButtonForm"
        );
        this.buttonFormRef = this.viewRef.createComponent(
          componentFactory
        ) as ComponentRef<ButtonForm>;
        this.buttonFormComp = this.buttonFormRef.instance;
        buttonForm = document.getElementById("button-form");
      }

      this.buttonFormComp.setFormOptions(options);

      let target: HTMLElement;
      if (options.targetElement) {
        target = options.targetElement;
        //retrace clicked element to the actual target
        if (options.target) {
          while (
            !(
              target.classList.contains(options.target.substr(1)) ||
              target.id == options.target.substr(1)
            )
          ) {
            target = target.offsetParent as HTMLElement;
            if (!target) break;
          }
        }
      } else {
        target = document.getElementById(options.target.substr(1));
      }

      //calculate offset from the target to its parent
      let offsetTop = 0,
        offsetLeft = 0,
        offsetRight = 0;
      let current = target;
      while (
        !(
          current.classList.contains(options.parent.substr(1)) ||
          current.id == options.parent.substr(1)
        )
      ) {
        offsetTop += current.offsetTop;
        offsetLeft += current.offsetLeft;
        current = current.offsetParent as HTMLElement;
      }
      let width = current.clientWidth * 0.96;
      buttonForm.style.width = width + "px";
      buttonForm.style.top = current.scrollTop + 20 + "px";
      buttonForm.style.bottom = "auto";
      buttonForm.style.left = (current.clientWidth - width) / 2 + "px";

      // buttonForm.style.right = 'auto'
      // buttonForm.style.left = '50%'
      // buttonForm.style.transform = 'translateX(-50%)'

      current.appendChild(buttonForm);

      if (this.changeSubscription) this.changeSubscription.unsubscribe();
      this.changeSubscription = this.buttonFormComp.change.subscribe(data => {
        if (data) {
          options.onSubmit(data);
        }
      });

      if (this.closeSubscription) this.closeSubscription.unsubscribe();
      this.closeSubscription = this.buttonFormComp.close.subscribe(() => {
        this.closeButtonForm();
      });

      setTimeout(() => {
        if (current.scrollTop > offsetTop - buttonForm.clientHeight / 2) {
          buttonForm.style.top = current.scrollTop + 20 + "px";
        } else if (
          offsetTop + buttonForm.clientHeight / 2 >
          current.scrollTop + current.clientHeight
        ) {
          buttonForm.style.top =
            current.scrollTop +
            current.clientHeight -
            buttonForm.clientHeight -
            20 +
            "px";
        } else {
          buttonForm.style.top = offsetTop - buttonForm.clientHeight / 2 + "px";
        }

        if (options.fields.length) {
          (buttonForm
            .getElementsByTagName("input")
            .item(0) as HTMLInputElement).focus();
        }

        this.clickHandler = (event: any) => {
          let cdk = document
            .getElementsByClassName("cdk-overlay-container")
            .item(0);
          if (cdk && cdk.contains(event.srcElement)) return;

          if (
            !buttonForm.contains(event.srcElement) &&
            !this.buttonFormComp.active
          ) {
            this.buttonFormComp.onNoClick();
          }
          // this.buttonFormComp.checkErrors()
          // if (!buttonForm.contains(event.srcElement) && !this.buttonFormComp.hasError
          // && !this.buttonFormComp.active) {
          //     this.closeButtonForm()
          // }
        };
        document.addEventListener("mousedown", this.clickHandler);
      });
      // setTimeout(() => {
      //     buttonForm.scrollIntoView({ behavior: 'smooth', block: 'center' })
      // }, 300)
    } catch (err) {
      console.error(err);
    }
  }

  async createNewButton({ event, buttonCode, onSubmit, removeTag }) {
    let options: iButtonFormOptions = {
      targetElement: event.target,
      onSubmit,
      title: BUTTONS_OBJECT[buttonCode].display,
      icon: BUTTONS_OBJECT[buttonCode].icon
    };
    options = { mode: "add", ...options, ...(await this[buttonCode]()) };
    if (removeTag && options.fields.length >= 2) {
      let index = options.fields.findIndex(x => x.property == "tag");
      if (index >= 0) {
        options.fields.splice(index, 1);
        options.fields[0].col = 12;
      }
    }
    await this.createButtonForm(options);
    return this.buttonFormComp.change;
  }

  async editButton({ event, button, onSubmit, onDelete, onCopy, removeTag }) {
    let buttonType = button.type;
    if (buttonType == "postback") {
      let payload = JSON.parse(button.payload);
      if (payload.type && payload.type != "story") buttonType = payload.type;
    } else if (buttonType == "web_url") {
      let url = button.url as string;
      if (/\/deeplink\/gmap/g.test(url)) {
        buttonType = "google_map";
      } else if (/\/innoway\/profile/g.test(url)) {
        buttonType = "profile_innoway";
      } else if (/view\/v1\/innoway\?path=category/g.test(url)) {
        buttonType = "order_innoway";
      } else if (/view\/v1\/innoway\?path=basket/g.test(url)) {
        buttonType = "basket_innoway";
      } else if (/view\/v1\/innoway\/simpleCheckout/g.test(url)) {
        buttonType = "simple_checkout_innoway";
      } else if (/bot-static\.mcom\.app/g.test(url)) {
        if (url.includes("order")) buttonType = "extension_order";
        else if (url.includes("profile")) buttonType = "extension_profile";
        else if (url.includes("booking")) buttonType = "extension_booking";
        else if (url.includes("reward")) buttonType = "extension_reward";
      }
    }

    let options: iButtonFormOptions = {
      targetElement: event.target,
      onSubmit,
      onDelete,
      onCopy,
      title: BUTTONS_OBJECT[buttonType].display,
      icon: BUTTONS_OBJECT[buttonType].icon
    };
    options = { mode: "edit", ...options, ...(await this[buttonType](button)) };
    if (removeTag) {
      let index = options.fields.findIndex(x => x.property == "tag");
      if (index >= 0) {
        options.fields.splice(index, 1);
        options.fields[0].col = 12;
      }
    }
    await this.createButtonForm(options);
    return this.buttonFormComp.change;
  }

  public closeButtonForm() {
    let form = document.getElementById("button-form");
    if (form) {
      form.classList.add("close-down");
      setTimeout(() => {
        form.remove();
        this.buttonFormRef.destroy();
        this.changeSubscription.unsubscribe();
        this.closeSubscription.unsubscribe();
        form.classList.remove("close-down");
        document.removeEventListener("click", this.clickHandler);
      }, 340);
    }
  }

  quick_reply_location = button => ({
    parent: ".portal-content-block",
    target: button ? ".quick-reply-button" : ".add-button-quick-reply",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: []
  });

  quick_reply_email = button => ({
    parent: ".portal-content-block",
    target: button ? ".quick-reply-button" : ".add-button-quick-reply",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: []
  });

  quick_reply_phone = button => ({
    parent: ".portal-content-block",
    target: button ? ".quick-reply-button" : ".add-button-quick-reply",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: []
  });

  quick_reply_activity = button => ({
    parent: ".portal-content-block",
    target: button ? ".quick-reply-button" : ".add-button-quick-reply",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: [
      {
        type: "text",
        placeholder: "Tên nút",
        constraints: ["required"],
        col: 8,
        property: "title",
        current: button ? button.title : ""
      },
      {
        type: "text",
        placeholder: "Tag nút",
        col: 4,
        property: "tag",
        current: button ? button.buttonTag : ""
      },
      {
        type: "text",
        placeholder: "Giá trị",
        col: 6,
        property: "value",
        current: button ? button.value : ""
      },
      {
        type: "icon",
        placeholder: "Icon",
        col: 6,
        property: "icon",
        current: button ? button.image_url : ""
      }
    ]
  });

  quick_reply_text = button => {
    let payload = button ? JSON.parse(button.payload) : null;

    let buttonClipboardData;
    if (!button) {
      try {
        let pasteData = sessionStorage.getItem("buttonData");
        if (pasteData) {
          let buttonObject = JSON.parse(pasteData);
          if (
            buttonObject.button &&
            buttonObject.data &&
            buttonObject.data.type == "quick_reply_text"
          ) {
            buttonClipboardData = buttonObject.data;
          }
        }
      } catch (err) {}
    }

    return {
      parent: ".portal-content-block",
      target: button ? ".quick-reply-button" : ".add-button-quick-reply",
      confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
      pasteData: buttonClipboardData ? buttonClipboardData : null,
      fields: [
        {
          type: "text",
          placeholder: "Tên nút",
          constraints: ["required"],
          col: 8,
          property: "title",
          current: button ? button.title : ""
        },
        {
          type: "text",
          placeholder: "Tag nút",
          col: 4,
          property: "tag",
          current: button ? button.buttonTag : ""
        },
        {
          type: "story",
          placeholder: "Câu chuyện",
          constraints: ["required"],
          col: 8,
          property: "story",
          current: button ? payload.data : ""
        },
        {
          type: "text",
          placeholder: "Giá trị",
          col: 4,
          property: "additional",
          current: button ? payload.additional : ""
        },
        {
          type: "icon",
          placeholder: "Icon",
          col: 12,
          property: "icon",
          current: button ? button.image_url : ""
        }
      ]
    };
  };

  web_url = button => ({
    parent: ".portal-content-block",
    target: button ? ".card-button" : ".add-button",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: [
      {
        type: "text",
        placeholder: "Tên nút",
        constraints: ["required"],
        col: 8,
        property: "title",
        current: button ? button.title : ""
      },
      {
        type: "text",
        placeholder: "Tag nút",
        col: 4,
        property: "tag",
        current: button ? button.buttonTag : ""
      },
      {
        type: "text",
        placeholder: "Link web",
        constraints: ["required"],
        col: 6,
        property: "url",
        current: button ? button.url : ""
      },
      {
        type: "checkbox",
        placeholder: "Mở bằng hộp thoại",
        col: 6,
        property: "messenger_extensions",
        current: button ? button.messenger_extensions : false
      }
    ]
  });

  phone_number = button => ({
    parent: ".portal-content-block",
    target: button ? ".card-button" : ".add-button",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: [
      {
        type: "text",
        placeholder: "Tên nút",
        constraints: ["required"],
        col: 8,
        property: "title",
        current: button ? button.title : ""
      },
      {
        type: "text",
        placeholder: "Tag nút",
        col: 4,
        property: "tag",
        current: button ? button.buttonTag : ""
      },
      {
        type: "text",
        placeholder: "Số điện thoại",
        constraints: ["phone"],
        col: 12,
        property: "phone",
        current: button ? button.payload : ""
      }
    ]
  });

  extension_order = button => ({
    parent: ".portal-content-block",
    target: button ? ".card-button" : ".add-button",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: [
      {
        type: "text",
        placeholder: "Tên nút",
        constraints: ["required"],
        col: 8,
        property: "title",
        current: button ? button.title : "Đặt hàng"
      },
      {
        type: "text",
        placeholder: "Tag nút",
        col: 4,
        property: "tag",
        current: button ? button.buttonTag : ""
      }
    ]
  });

  extension_profile = button => ({
    parent: ".portal-content-block",
    target: button ? ".card-button" : ".add-button",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: [
      {
        type: "text",
        placeholder: "Tên nút",
        constraints: ["required"],
        col: 8,
        property: "title",
        current: button ? button.title : "Trang cá nhân"
      },
      {
        type: "text",
        placeholder: "Tag nút",
        col: 4,
        property: "tag",
        current: button ? button.buttonTag : ""
      }
    ]
  });

  extension_booking = button => ({
    parent: ".portal-content-block",
    target: button ? ".card-button" : ".add-button",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: [
      {
        type: "text",
        placeholder: "Tên nút",
        constraints: ["required"],
        col: 8,
        property: "title",
        current: button ? button.title : "Đặt lịch"
      },
      {
        type: "text",
        placeholder: "Tag nút",
        col: 4,
        property: "tag",
        current: button ? button.buttonTag : ""
      }
    ]
  });

  extension_reward = button => ({
    parent: ".portal-content-block",
    target: button ? ".card-button" : ".add-button",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: [
      {
        type: "text",
        placeholder: "Tên nút",
        constraints: ["required"],
        col: 8,
        property: "title",
        current: button ? button.title : "Xem ưu đãi"
      },
      {
        type: "text",
        placeholder: "Tag nút",
        col: 4,
        property: "tag",
        current: button ? button.buttonTag : ""
      }
    ]
  });

  extension_history = button => ({
    parent: ".portal-content-block",
    target: button ? ".card-button" : ".add-button",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: [
      {
        type: "text",
        placeholder: "Tên nút",
        constraints: ["required"],
        col: 8,
        property: "title",
        current: button ? button.title : "Lịch sử đơn hàng"
      },
      {
        type: "text",
        placeholder: "Tag nút",
        col: 4,
        property: "tag",
        current: button ? button.buttonTag : ""
      }
    ]
  });

  element_share = button => {
    let element = button
      ? button.share_contents.attachment.payload.elements[0]
      : null;
    return {
      parent: ".portal-content-block",
      target: button ? ".card-button" : ".add-button",
      confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
      fields: [
        {
          type: "text",
          placeholder: "Tiêu đề chia sẻ",
          constraints: ["required"],
          col: 6,
          property: "shareTitle",
          current: button ? element.title : ""
        },
        {
          type: "textarea",
          placeholder: "Mô tả chia sẻ",
          col: 6,
          property: "shareSubtitle",
          current: button ? element.subtitle : ""
        },
        {
          type: "image",
          placeholder: "Link ảnh chia sẻ",
          constraints: ["required"],
          col: 6,
          property: "shareImage",
          current: button ? element.image_url : ""
        },
        {
          type: "text",
          placeholder: "Tên nút chia sẻ",
          constraints: ["required"],
          col: 6,
          property: "shareButton",
          current: button ? element.buttons[0].title : ""
        },
        {
          type: "text",
          placeholder: "Đường link chia sẻ",
          constraints: ["required"],
          col: 12,
          property: "shareLink",
          current: button ? element.buttons[0].url : ""
        }
      ]
    };
  };

  postback = button => {
    let payload = button ? JSON.parse(button.payload) : null;
    return {
      parent: ".portal-content-block",
      target: button ? ".card-button" : ".add-button",
      confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
      fields: [
        {
          type: "text",
          placeholder: "Tên nút",
          constraints: ["required"],
          col: 8,
          property: "title",
          current: button ? button.title : ""
        },
        {
          type: "text",
          placeholder: "Tag nút",
          col: 4,
          property: "tag",
          current: button ? button.buttonTag : ""
        },
        {
          type: "story",
          placeholder: "Câu chuyện",
          constraints: ["required"],
          col: 8,
          property: "story",
          current: button ? payload.data : ""
        },
        {
          type: "text",
          placeholder: "Giá trị",
          col: 4,
          property: "additional",
          current: button ? payload.additional : ""
        }
      ]
    };
  };

  about_topic = button => {
    let payload = button ? JSON.parse(button.payload) : null;
    return {
      parent: ".portal-content-block",
      target: button ? ".card-button" : ".add-button",
      confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
      fields: [
        {
          type: "text",
          placeholder: "Tên nút",
          constraints: ["required"],
          col: 8,
          property: "title",
          current: button ? button.title : ""
        },
        {
          type: "text",
          placeholder: "Tag nút",
          col: 4,
          property: "tag",
          current: button ? button.buttonTag : ""
        },
        {
          type: "topic",
          placeholder: "Chủ đề",
          constraints: ["required"],
          col: 8,
          property: "topicId",
          current: button ? payload.data.topicId : ""
        },
        {
          type: "text",
          placeholder: "Giá trị",
          col: 4,
          property: "additional",
          current: button ? payload.additional : ""
        },
        {
          type: "story",
          placeholder: "Chuyển câu chuyện",
          col: 12,
          property: "storyId",
          current: button ? payload.data.storyId : ""
        }
      ]
    };
  };

  order_innoway = button => ({
    parent: ".portal-content-block",
    target: button ? ".card-button" : ".add-button",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: [
      {
        type: "text",
        placeholder: "Tên nút",
        constraints: ["required"],
        col: 8,
        property: "title",
        current: button ? button.title : "Đặt hàng"
      },
      {
        type: "text",
        placeholder: "Tag nút",
        col: 4,
        property: "tag",
        current: button ? button.buttonTag : ""
      }
    ]
  });

  basket_innoway = button => ({
    parent: ".portal-content-block",
    target: button ? ".card-button" : ".add-button",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: [
      {
        type: "text",
        placeholder: "Tên nút",
        constraints: ["required"],
        col: 8,
        property: "title",
        current: button ? button.title : "Giỏ hàng"
      },
      {
        type: "text",
        placeholder: "Tag nút",
        col: 4,
        property: "tag",
        current: button ? button.buttonTag : ""
      }
    ]
  });

  profile_innoway = button => ({
    parent: ".portal-content-block",
    target: button ? ".card-button" : ".add-button",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: [
      {
        type: "text",
        placeholder: "Tên nút",
        constraints: ["required"],
        col: 8,
        property: "title",
        current: button ? button.title : "Thông tin người dùng"
      },
      {
        type: "text",
        placeholder: "Tag nút",
        col: 4,
        property: "tag",
        current: button ? button.buttonTag : ""
      }
    ]
  });

  simple_checkout_innoway = button => ({
    parent: ".portal-content-block",
    target: button ? ".card-button" : ".add-button",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: [
      {
        type: "text",
        placeholder: "Tên nút",
        constraints: ["required"],
        col: 8,
        property: "title",
        current: button ? button.title : "Mua ngay"
      },
      {
        type: "text",
        placeholder: "Tag nút",
        col: 4,
        property: "tag",
        current: button ? button.buttonTag : ""
      }
    ]
  });

  google_map = button => {
    let tlat, tlng;
    if (button) {
      tlat = this.getQueryString("tlat", button.url);
      tlng = this.getQueryString("tlng", button.url);
    }
    return {
      parent: ".portal-content-block",
      target: button ? ".card-button" : ".add-button",
      confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
      fields: [
        {
          type: "text",
          placeholder: "Tên nút",
          constraints: ["required"],
          col: 8,
          property: "title",
          current: button ? button.title : "Mở bản đồ"
        },
        {
          type: "text",
          placeholder: "Tag nút",
          col: 4,
          property: "tag",
          current: button ? button.buttonTag : ""
        },
        {
          type: "text",
          placeholder: "Vĩ độ (Latitude)",
          col: 6,
          property: "lat",
          current: button ? tlat : "10.785864"
        },
        {
          type: "text",
          placeholder: "Kinh độ (Longitude)",
          col: 6,
          property: "lng",
          current: button ? tlng : "106.690374"
        }
      ]
    };
  };

  live_chat = button => ({
    parent: ".portal-content-block",
    target: button ? ".card-button" : ".add-button",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: [
      {
        type: "text",
        placeholder: "Tên nút",
        constraints: ["required"],
        col: 8,
        property: "title",
        current: button ? button.title : "Chat với nhân viên"
      },
      {
        type: "text",
        placeholder: "Tag nút",
        col: 4,
        property: "tag",
        current: button ? button.buttonTag : ""
      }
    ]
  });

  activity = button => {
    let payload = button ? JSON.parse(button.payload) : null;
    return {
      parent: ".portal-content-block",
      target: button ? ".card-button" : ".add-button",
      confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
      fields: [
        {
          type: "text",
          placeholder: "Tên nút",
          constraints: ["required"],
          col: 8,
          property: "title",
          current: button ? button.title : ""
        },
        {
          type: "text",
          placeholder: "Tag nút",
          col: 4,
          property: "tag",
          current: button ? button.buttonTag : ""
        },
        {
          type: "text",
          placeholder: "Giá trị",
          col: 12,
          property: "value",
          current: button ? payload.data : ""
        }
      ]
    };
  };

  deeplink = button => ({
    parent: ".portal-content-block",
    target: button ? ".card-button" : ".add-button",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: [
      {
        type: "text",
        placeholder: "Tên nút",
        constraints: ["required"],
        col: 8,
        property: "title",
        current: button ? button.title : "Mở Messenger"
      },
      {
        type: "text",
        placeholder: "Tag nút",
        col: 4,
        property: "tag",
        current: button ? button.buttonTag : ""
      },
      {
        type: "text",
        placeholder: "Link web",
        col: 12,
        property: "webUrl",
        current: button ? button.webUrl : "https://m.me/bot.mcom"
      },
      {
        type: "text",
        placeholder: "Link Android",
        col: 6,
        property: "androidUrl",
        current: button ? button.androidUrl : "https://m.me/bot.mcom"
      },
      {
        type: "text",
        placeholder: "Link Store Android",
        col: 6,
        property: "androidStoreUrl",
        current: button
          ? button.androidStoreUrl
          : "https://play.google.com/store/apps/details?id=com.facebook.orca&hl=fr"
      },
      {
        type: "text",
        placeholder: "Link iOS",
        col: 6,
        property: "iosUrl",
        current: button ? button.iosUrl : "https://m.me/bot.mcom"
      },
      {
        type: "text",
        placeholder: "Link Store iOS",
        col: 6,
        property: "iosStoreUrl",
        current: button
          ? button.iosStoreUrl
          : "https://itunes.apple.com/us/app/messenger/id454638411?mt=8"
      }
    ]
  });

  lucky_wheel = async button => {
    let wheels = await this.getWheels();
    console.log(wheels);
    return {
      parent: ".portal-content-block",
      target: button ? ".card-button" : ".add-button",
      confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
      fields: [
        {
          type: "text",
          placeholder: "Tên nút",
          constraints: ["required"],
          col: 8,
          property: "title",
          current: button ? button.title : "Vòng quay"
        },
        {
          type: "text",
          placeholder: "Tag nút",
          col: 4,
          property: "tag",
          current: button ? button.buttonTag : ""
        },
        {
          type: "text",
          placeholder: "Tựa đề nút vòng quay",
          constraints: ["required"],
          col: 4,
          property: "btnTitle",
          current: button ? button.btnTitle : "Bắt đầu quay"
        },
        {
          type: "select",
          placeholder: "Loại hình quay",
          col: 5,
          property: "multi",
          current: button ? button.multi.toString() : "false",
          options: [
            { code: "true", display: "Quay nhiều lượt" },
            { code: "false", display: "Chỉ quay một lượt" }
          ],
          disableOtherProperty: [{ property: "turn", value: "false" }]
        },
        {
          type: "text",
          placeholder: "Lượt quay",
          col: 3,
          property: "turn",
          current: button ? button.turn : ""
        },
        {
          type: "select",
          placeholder: "Vòng quay",
          constraints: ["required"],
          col: 8,
          property: "wheel",
          current: button
            ? wheels.find(x => x._id == button.luckyWheelId)
            : null,
          options: [...wheels.map(x => ({ code: x, display: x.name }))],
          setupOtherPropertyOptions: {
            property: "giftId",
            sourceValue: "gifts"
          },
          disableOtherProperty: [
            { property: "giftId", hasValue: false },
            { property: "successRatio", hasValue: false }
          ]
        },
        {
          type: "open",
          placeholder: "Mở vòng quay",
          col: 4,
          property: "WheelPortalComponent",
          current: { property: "wheel", value: "_id", propertyName: "wheelId" }
        },
        {
          type: "select",
          placeholder: "Phần thưởng",
          col: 6,
          property: "giftId",
          current: button ? button.giftId : "",
          options: [],
          disableOtherProperty: [{ property: "successRatio", hasValue: true }]
        },
        {
          type: "text",
          placeholder: "Tỉ lệ thắng (%)",
          col: 6,
          property: "successRatio",
          current: button ? button.successRatio : "",
          disableOtherProperty: [{ property: "giftId", hasValue: true }]
        },
        {
          type: "story",
          placeholder: "Câu chuyện",
          col: 12,
          property: "storyId",
          current: button ? button.storyId : ""
        }
      ]
    };
  };

  nested = button => ({
    parent: ".portal-content-block",
    target: button ? ".card-button" : ".add-button",
    confirm: button ? "Lưu thay đổi" : "Tạo nút mới",
    fields: [
      {
        type: "text",
        placeholder: "Tên nút",
        constraints: ["required"],
        col: 12,
        property: "title",
        current: button ? button.title : "Mở rộng"
      }
    ]
  });

  getQueryString(field, url) {
    let reg = new RegExp("[?&]" + field + "=([^&#]*)", "i");
    let string = reg.exec(url);
    return string ? string[1] : null;
  }

  async getWheels() {
    return await this.chatbotApi.luckyWheel.getList({
      local: true,
      query: { limit: 0, order: { createdAt: 1 } }
    });
  }
}
