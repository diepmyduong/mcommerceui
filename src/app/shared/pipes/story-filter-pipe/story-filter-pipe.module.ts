import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoryFilterPipe } from './story-filter.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [StoryFilterPipe],
  exports: [StoryFilterPipe]
})
export class StoryFilterPipeModule { }
