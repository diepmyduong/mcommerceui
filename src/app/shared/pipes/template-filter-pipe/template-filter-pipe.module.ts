import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplateFilterPipe } from 'app/shared/pipes/template-filter-pipe/template-filter.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TemplateFilterPipe],
  exports: [TemplateFilterPipe]
})
export class TemplateFilterPipeModule { }
