import { Pipe, PipeTransform } from "@angular/core";
import { ChatbotAuthService } from "app/services/chatbot";
@Pipe({
    name: 'templateFilter',
    pure: false
  })
  export class TemplateFilterPipe implements PipeTransform {
    userId
    constructor(private auth: ChatbotAuthService) {
      this.userId = this.auth.firebaseUser.uid
    }
    
    transform(items: any[], type: any, categories: string[]): any {
      let filteredItems = []
      if (categories && categories.length) {
        items = items.filter(item => {
          let flag = false
          for (let item_cat of item.category) {
            if (categories.includes(item_cat)) {
              flag = true
              break
            }
          }
          return flag
        })
      }
      if (items && items.length) {
        switch (type) {
          case 'public':
            filteredItems = items.filter(item => {
              return item.type == 'public'
            })
          break;
          case 'private':
            filteredItems = items.filter(item => {
              return item.type == 'private' && item.owner == this.userId
            })
          break;
          case 'shared':
            filteredItems = items.filter(item => {
              return item.type == 'private' && item.owner != this.userId
            })
          break;
        }
      }
      if (filteredItems.length) return filteredItems
      else return [undefined]
    }
  }