import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NameFilterPipe } from 'app/shared/pipes/name-filter-pipe/name-filter.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [NameFilterPipe],
  exports: [NameFilterPipe]
})
export class NameFilterPipeModule { }
