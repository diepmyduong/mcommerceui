import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafePipe } from 'app/shared/pipes/safe-pipe/safe.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SafePipe],
  exports: [SafePipe]
})
export class SafePipeModule { }
