import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppFilterPipe } from './app-filter.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AppFilterPipe],
  exports: [AppFilterPipe]
})
export class AppFilterPipeModule { }
