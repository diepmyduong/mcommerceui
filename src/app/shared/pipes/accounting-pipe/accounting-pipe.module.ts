import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountingPipe } from './accounting.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AccountingPipe],
  exports: [AccountingPipe]
})
export class AccountingPipeModule { }
