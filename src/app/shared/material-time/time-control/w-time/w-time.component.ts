import { Component, Input, Output, OnInit, Inject, EventEmitter  } from '@angular/core';

import { CLOCK_TYPE, ITime } from 'app/shared/material-time/time-control/w-clock/w-clock.component';
import * as moment from 'moment'
@Component({
  selector: 'w-time',
  templateUrl: './w-time.component.html',
  styleUrls: ['./w-time.component.scss']
})
export class WTimeComponent implements OnInit {

  @Input() userTime: ITime;
  @Output() userTimeChange: EventEmitter<ITime> = new EventEmitter();

  @Input() revertLabel: string;
  @Input() submitLabel: string;
  @Input() cancelLabel: string;

  @Output() onRevert: EventEmitter<null> = new EventEmitter();
  @Output() onSubmit: EventEmitter<ITime> = new EventEmitter();
  @Output() onCancel: EventEmitter<null> = new EventEmitter();

  @Input() color: string;
  @Input() revertColor: string;
  @Input() cancelColor: string = 'warn'

  @Input() showCancelButton: boolean = false
  @Input() showRevertButton: boolean = true

  public VIEW_HOURS = CLOCK_TYPE.HOURS;
  public VIEW_MINUTES = CLOCK_TYPE.MINUTES;
  public currentView: CLOCK_TYPE = this.VIEW_HOURS;

  constructor() {}

  ngOnInit() {
    if (!this.userTime) {
      this.userTime = {
        hour: 6,
        minute: 0,
        meriden: 'PM'
      };
    }

    if (!this.revertLabel) {
      this.revertLabel = 'Reset'
    }

    if (!this.submitLabel) {
      this.submitLabel = 'Okay'
    }

    if(!this.cancelLabel) {
      this.cancelLabel = 'Cancel'
    }
  }

  public formatMinute(): string {
    if (this.userTime.minute < 10) {
      return '0' + String(this.userTime.minute);
    } else {
      return String(this.userTime.minute);
    }
  }

  public setCurrentView(type: CLOCK_TYPE) {
    this.currentView = type;
  }

  public setMeridien(m: 'PM' | 'AM') {
    this.userTime.meriden = m
  }

  public cancel() {
    this.onCancel.emit();
  }

  public revert() {
    this.onRevert.emit();
  }

  public submit() {
    this.onSubmit.emit(this.userTime);
  }

  public emituserTimeChange(event) {
    this.userTimeChange.emit(this.userTime);
  }

  get hour() {
    return this.userMoment.format('hh')
  }

  get userMoment() {
    return moment(`${this.userTime.hour}:${this.userTime.minute} ${this.userTime.meriden}`, 'HH:mm A').locale('en')
  }

}
