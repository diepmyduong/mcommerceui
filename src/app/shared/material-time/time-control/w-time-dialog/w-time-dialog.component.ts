
import { Component, Inject, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';

import { CLOCK_TYPE, ITime } from 'app/shared/material-time/time-control/w-clock/w-clock.component';
import * as moment from 'moment'

@Component({
    styleUrls: ['./w-time-dialog.component.scss'],
    templateUrl: './w-time-dialog.component.html'
})
export class WTimeDialogComponent {

    public userTime: ITime;
    private VIEW_HOURS = CLOCK_TYPE.HOURS;
    private VIEW_MINUTES = CLOCK_TYPE.MINUTES;
    private currentView: CLOCK_TYPE = this.VIEW_HOURS;
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: {time: ITime, color: string, submitLabel: string, revertLabel: string, cancelLabel: string},
        @Inject(MAT_DIALOG_DATA) public color: string,
        @Inject(MAT_DIALOG_DATA) public subsc: string,
        private dialogRef: MatDialogRef<WTimeDialogComponent>,
    ) {
        this.userTime = data.time;
        this.color = data.color;
    }

    public revert() {
        const toDate = moment().locale('en')
        this.userTime.hour = toDate.get('hour')
        this.userTime.minute = toDate.get('minute')
        this.userTime.meriden = toDate.format('A') as 'PM' | 'AM'
    }

    public submit() {
        this.dialogRef.close(this.userTime);
    }

    public cancel() {
        this.dialogRef.close(-1)
    }
}
