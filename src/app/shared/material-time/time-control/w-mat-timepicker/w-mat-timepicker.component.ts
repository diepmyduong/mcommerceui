
import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectorRef, ViewChild, SimpleChanges } from '@angular/core';
import { MatDialog, MatInput } from '@angular/material';

import { WTimeDialogComponent } from 'app/shared/material-time/time-control/w-time-dialog/w-time-dialog.component';
import { ITime } from 'app/shared/material-time/time-control/w-clock/w-clock.component';
import * as moment from 'moment'
import { BehaviorSubject } from 'rxjs';
@Component({
    selector: 'w-mat-timepicker',
    styleUrls: ['./w-mat-timepicker.component.scss'],
    templateUrl: './w-mat-timepicker.component.html'
})

export class WMatTimePickerComponent implements OnInit {

    @Input() userTime: ITime;

    @Output() userTimeChange: EventEmitter<ITime> = new EventEmitter();

    @Input() color: string;

    @Input() submitLabel: string
    @Input() revertLabel: string
    @Input() cancelLabel: string
    @Input() placeholder: string
    @Input() disabled: boolean = false

    @ViewChild("timeInput", { read: MatInput }) timeInput: MatInput

    constructor(private dialog: MatDialog, private ref: ChangeDetectorRef) { }

    ngOnInit() {

    }
    
    ngOnChanges(changes: SimpleChanges) {

        this.timeInput.value = this.getTime()
        // You can also use categoryId.previousValue and 
        // categoryId.firstChange for comparing old and new values
    
    }

    ngAfterViewInit() {
        //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
        //Add 'implements AfterViewInit' to the class.
        setTimeout(() => {
            if (!this.userTime) {
                const toDate = moment().locale('en')
                this.userTime = {
                    'hour': toDate.get('hour'),
                    'minute': toDate.get('minute'),
                    'meriden': toDate.format('A') as any
                }
                this.emituserTimeChange()
            }
            this.timeInput.value = this.getTime()
        }, 200)

    }

    getTime() {
        if (!this.userTime) {
            return '';
        } else {
            return moment(`${this.userTime.hour}:${this.userTime.minute}`, 'HH:mm').format('hh:mm A')
        }
    }


    public showPicker($event) {

        const dialogRef = this.dialog.open(WTimeDialogComponent, {
            data: {
                time: {
                    hour: this.userTime.hour,
                    minute: this.userTime.minute,
                    meriden: this.userTime.meriden
                },
                color: this.color,
                submitLabel: this.submitLabel,
                revertLabel: this.revertLabel,
                cancelLabel: this.cancelLabel
            }
        });

        dialogRef.afterClosed()
            .subscribe((result: ITime | -1) => {
                // result will be update userTime object or -1 or undefined (closed dialog w/o clicking cancel)
                if (result === undefined) {
                    return;
                } else if (result !== -1) {
                    if(result.meriden.toLowerCase() === "pm" && result.hour < 12) result.hour += 12
                    else if (result.meriden.toLowerCase() === "am" && result.hour >= 12) result.hour -= 12
                    this.userTime = result;
                    this.timeInput.value = this.getTime()
                    this.emituserTimeChange();
                }
            });
        return false;
    }

    private emituserTimeChange() {
        this.timeInput.value = this.getTime()
        this.userTimeChange.emit(this.userTime);
    }
}
