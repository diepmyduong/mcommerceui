export const primaryKeywords = ['id', 'first_name', 'last_name', 'gender', 'name', 'locale', 'timezone', 'profile_pic', '_referral', '_input', '_meta', '_gift','page_name','user_name','comment_text','uid'];
export const HightLightTextAreaSchemas = [
    { className: 'hl-remove', highlight: /^\$null$/g }, // remove value
    { className: 'hl-primary', highlight: new RegExp(`{{( +)?(${primaryKeywords.join('|')})( +)?}}`, 'g') }, // primary
    { className: 'hl-variable', highlight: /{{( +)?\$.*?}}/g }, // variable
    { className: 'hl-environment', highlight: /{{( +)?\env\..*?}}/g }, // environments
    { className: 'hl-other', highlight: /{{(.*?)}}/g }, // the rest
]
export const atKeywords = [...primaryKeywords,
    `$GioiTinh(gender)`,
    `$gioiTinh(gender)`,
    `moment().utcOffset('+07:00').format('DD/MM/YYYY HH:mm')`,
    `moment().utcOffset('+07:00').format('DD/MM/YYYY')`,
    `$random(0, 100)`,
    `$randexp(/[1-6]/)`,
    `$search(docs, keyword,'name', {'name':1})`,
    `$unixEncode(data)`,
    `$unixDecode(encode)`,
    `$time("DD-MM-YYYY")`
]
export const postcaseKeywords = ['page_name','user_name','comment_text','uid']