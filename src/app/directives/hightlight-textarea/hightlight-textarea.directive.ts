import { Directive, ElementRef, Input, Output, EventEmitter, HostListener } from '@angular/core';
import 'highlight-within-textarea';
import { HightLightTextAreaSchemas, atKeywords, postcaseKeywords } from './scheme';
import { ChatbotApiService, iApp } from 'app/services/chatbot';
import { CardService } from 'app/services/card.service';
import { Subscription } from 'rxjs';
declare var $: any;
@Directive({
  selector: '[hlTextArea]'
})
export class HightlightTextareaDirective {
  @Input() typeHlText:string = "default";
  @Input() ngModel;
  @Output() ngModelChange = new EventEmitter();
  @HostListener('document:keydown.enter', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    $(this.el.nativeElement).highlightWithinTextarea('update');
  }
  constructor(
    private el: ElementRef, 
    private chatbotApi: ChatbotApiService
  ) {
  }

  async ngOnInit() {    
  }

  ngAfterViewInit() {
    const $textarea = $(this.el.nativeElement);
    // console.log( 'textarea',$textarea)
    $textarea.highlightWithinTextarea({
      highlight: HightLightTextAreaSchemas
    })

    $textarea.focus(async () => {
      const { app } = this.chatbotApi.chatbotAuth as iApp
      let environment
      if (app.environment && typeof app.environment == "string") {
        environment = await this.chatbotApi.environment.getItem(app.environment, { local: true });
      } else {
        environment = await this.chatbotApi.environment.getItem(app.environment._id, { local: true });
      }
      let environmentKeys = []
      if (environment && environment.data) {
        for (let key of Object.keys(environment.data)) {
          if (environment.data.hasOwnProperty(key)) {
            environmentKeys.push('env.' + key)
          }
        }
      }

      let data 
      if(this.typeHlText == "post-case"){
        data = postcaseKeywords
      }
      if(this.typeHlText == "default"){
        data = [...atKeywords, ...environmentKeys]
      } 
      $textarea.atwho({
        at: "{{",
        data: data,
        insertTpl: "{{${name}}}",
        callbacks: {
          beforeInsert: (value, $li) => {            
            setTimeout(() => {
              this.ngModel = $textarea.val()
              this.ngModelChange.emit(this.ngModel)
            }, 250);
            return value;
          }
        }
      })
    })

    $textarea.blur(() => {
      $textarea.highlightWithinTextarea('update');
      $textarea.atwho('destroy')
    })

    if ($textarea.is('input')) {
      $textarea.prev('.hwt-backdrop').children('.hwt-content').css( "white-space", "pre" );
    }

    setTimeout(() => {
      $(this.el.nativeElement).highlightWithinTextarea('update');
    }, 250)
  }

  ngOnDestroy() {
    $(this.el.nativeElement).highlightWithinTextarea('destroy');
  }

}
