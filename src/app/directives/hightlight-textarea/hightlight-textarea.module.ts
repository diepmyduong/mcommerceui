import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HightlightTextareaDirective } from './hightlight-textarea.directive';
import 'highlight-within-textarea';
@NgModule({
  declarations: [HightlightTextareaDirective],
  imports: [
    CommonModule
  ],
  exports: [HightlightTextareaDirective]
})
export class HightlightTextareaModule { }
