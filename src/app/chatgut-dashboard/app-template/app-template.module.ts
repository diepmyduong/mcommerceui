import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppTemplateComponent } from './app-template.component';
import { ChatgutAppTemplateRouting } from './app-template.routing';
import { TemplateFilterPipeModule } from 'app/shared/pipes/template-filter-pipe/template-filter-pipe.module';
import { MatTooltipModule, MatSnackBarModule, MatProgressSpinnerModule, MatTabsModule, MatDialogModule } from '@angular/material';
import { TemplateListComponent } from './template-list/template-list.component';
import { TemplateDetailsComponent } from './template-details/template-details.component';
import { TemplateShareDialog } from './template-share/template-share.component';
import { TemplateInviteComponent } from './template-invite/template-invite.component';

@NgModule({
  providers: [],
  imports: [
    CommonModule,
    FormsModule,
    ChatgutAppTemplateRouting,
    TemplateFilterPipeModule,
    MatTooltipModule,
    MatSnackBarModule, 
    MatProgressSpinnerModule,
    MatDialogModule,
    MatTabsModule
  ],
  declarations: [AppTemplateComponent, TemplateDetailsComponent, TemplateListComponent, TemplateShareDialog, TemplateInviteComponent],
  entryComponents: [TemplateShareDialog]
})
export class AppTemplateModule { }
