import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from 'app/base.component';
import { ChatbotApiService } from 'app/services/chatbot';
import { AlertService } from 'app/services/alert.service';

@Component({
  selector: 'app-template-invite',
  templateUrl: './template-invite.component.html',
  styleUrls: ['./template-invite.component.scss']
})
export class TemplateInviteComponent {

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public chatbotApi: ChatbotApiService,
    public alert: AlertService
  ) { 
  }
  inviteToken: string

  async ngOnInit() {
    this.inviteToken = this.route.snapshot.queryParams["payload"]
    try {
      const payload = this.inviteToken.split("|")
      if(payload.length != 2) {
        this.toTemplateList()
        return
      }
      const templateId = payload[0]
      const shareToken = payload[1]
      let res = await this.chatbotApi.template.acceptShare(templateId, shareToken)
      if(res && res.error){
        // this.alert.error("Lỗi","Thêm mẫu cho người dùng thất bại")
        this.alert.handleError(res);
      }
      // console.log('accept share', res)
      this.router.navigate(["/template", templateId])
    } catch(err) {
      this.alert.handleError(err, "Lỗi khi tạo mẫu");
      // this.alert.error("Không thể mẫu cho người dùng", err.message)
      this.toTemplateList()
    }

  }

  toTemplateList() {
    this.router.navigate(["/template"])
  }

}
