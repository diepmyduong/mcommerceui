import { Component, Inject, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from "@angular/material";
import { iTemplate, ChatbotApiService } from "app/services/chatbot";
import { AlertService } from "app/services/alert.service";
import { BehaviorSubject } from "rxjs";
import { TemplateListComponent } from "../template-list/template-list.component";

@Component({
  selector: 'app-template-share',
  templateUrl: './template-share.component.html',
  styleUrls: ['./template-share.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TemplateShareDialog {
  
  template: iTemplate
  link: string
  email: string

  generatingLink: boolean
  sharingEmail: boolean

  publishing$ = new BehaviorSubject<boolean>(false)
  parent: TemplateListComponent

  constructor(
    public dialogRef: MatDialogRef<TemplateShareDialog>,
    @Inject(MAT_DIALOG_DATA) public data: iTemplate,
    private chatbotApi: ChatbotApiService,
    private changeRef: ChangeDetectorRef,
    private snackBar: MatSnackBar,
    private alert: AlertService) {
      this.template = data.template
      this.parent = data.parent
  }

  async ngOnInit() {
    try {
      this.link = (await this.chatbotApi.template.getShareLink(this.template._id)).inviteLink
      this.changeRef.detectChanges()
    } catch (err) { 
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi lấy dữ liệu template", err.message)
    }
  }

  async generateLink() {
    try {
      this.generatingLink = true
      
      let inviteLink = (await this.chatbotApi.template.getShareLink(this.template._id)).inviteLink
      if (location.host != 'bot.mcom.app') inviteLink = inviteLink.replace('bot.mcom.app', location.host)
      this.link = inviteLink
    } catch (err) {
      this.alert.handleError(err, 'Lỗi xảy ra khi tạo link')
    } finally {
      this.generatingLink = false
      this.changeRef.detectChanges()
    }
  }

  async shareViaEmail() {

    let form = document.getElementById('email-form') as HTMLFormElement
    if (!form.checkValidity()) return

    try {
      this.sharingEmail = true
      await this.chatbotApi.template.share({
        templateId: this.template._id,
        email: this.email
      })
      this.snackBar.open('✔️ Đã chia sẻ mẫu cho email', '', {
        duration: 2000,
      });
      setTimeout(async () => {
        let firebaseToken = await this.chatbotApi.chatbotAuth.firebaseToken
        let template = await this.chatbotApi.template.getItem(this.template._id, {
          headers: {
            'access_token': firebaseToken
        }})
        console.log(template)
        this.email = ''
        this.template.shareableUser = template.shareableUser
        this.chatbotApi.template.setItem(template, {
          headers: {
            'access_token': firebaseToken
        }})
        this.changeRef.detectChanges()
      }, 300)

    } catch (err) {
      this.snackBar.open('❌ Lỗi khi chia sẻ. Xin kiểm tra lại email.', '', {
        duration: 2000,
      });
    } finally {
      this.sharingEmail = false
      this.changeRef.detectChanges()
    }
  }

  async removeShare(user) {
    try {
      await this.chatbotApi.template.unshare({
        templateId: this.template._id,
        uid: user.uid
      })
      this.snackBar.open('✔️ Hủy chia sẻ thành công', '', {
        duration: 2000,
      });
      this.template.shareableUser.splice(this.template.shareableUser.findIndex(x => x.uid == user.uid), 1)
      this.changeRef.detectChanges()
    } catch (err) {
      this.snackBar.open('❌ Lỗi xảy ra khi hủy chia sẻ', '', {
        duration: 2000,
      });
    }
  }
  
  async publishTemplate() {
    try {
      this.publishing$.next(true)
      await this.chatbotApi.template.updateTemplate(this.template._id, {
        type: 'public'
      })
      this.alert.success('Chuyển thành mẫu chung thành công', 'Mẫu đã được chuyển đổi thành mẫu chung')
      this.parent.reload(false)
      this.dialogRef.close()
    } catch (err) {
      console.error(err)
      this.alert.error('Lỗi khi chuyển thành mẫu chung', err.message)
    } finally {
      this.publishing$.next(false)
    }
  }

  copyId(){
    let input = document.getElementById('link-url') as HTMLInputElement
    input.value = this.link
    input.select();
    document.execCommand('copy',false);
    this.snackBar.open('✔️ Đã copy vào clipboard', '', {
      duration: 2000,
    });
    this.changeRef.detectChanges()
  }
}