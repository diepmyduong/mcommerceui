import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { ChatbotApiService, iFanpage, iApp, iEnvironment, iTemplate } from "app/services/chatbot";
import { AlertService } from "app/services/alert.service";
import * as moment from 'moment'
import { MatSnackBar } from "@angular/material";

@Component({
  selector: 'app-template-details',
  templateUrl: './template-details.component.html',
  styleUrls: ['./template-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TemplateDetailsComponent {

  add: boolean
  details: boolean
  editable: boolean
  id: string
  readonly: boolean
  paramSubscription: Subscription
  user: any
  apps: iApp[] = []
  selectedApp: iApp
  template: iTemplate
  loadEnvironmentDone: boolean = false
  loadDone: boolean = false
  submitting: boolean = false
  
  name: string
  description: string
  image: string
  preview: string
  environment: iEnvironment
  environmentConfig: { key: string, value: string }[]

  nameError: boolean = false
  descError: boolean = false
  
  constructor(
    private activatedRoute: ActivatedRoute,
    private chatbotApi: ChatbotApiService,
    private alert: AlertService,
    private changeRef: ChangeDetectorRef,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    this.paramSubscription = this.activatedRoute.params.subscribe(params => {
      if (location.pathname.includes('/add')) {
        this.add = true
      } else {
        this.details = true
        this.id = params['id']
      }
    }); 
  }

  async ngOnDestroy() {
    if (this.paramSubscription) this.paramSubscription.unsubscribe()
  }

  async ngOnInit() {
    await this.reload()
  }

  async reload() {
    try {
      let tasks = []

      // get fanpages
      if (this.add) {     
        tasks.push(this.chatbotApi.app.getList({
          local: true,
          query: {
            order: {
              createdAt: -1
            },
            populates: ["activePage", "license"],
          }
        }).then(result => {
          this.apps = result
          this.apps.forEach(x => {
            x.disabled = x.whiteListed?false:this.isExpired(x.license.expireDate)
          })
          // console.log(this.apps)
        }))
      }

      if (this.details) {
        let firebaseToken = await this.chatbotApi.chatbotAuth.firebaseToken
        tasks.push(this.chatbotApi.template.getItem(this.id, {
          headers: {
            'access_token': firebaseToken
          }}).then(result => {
          this.template = result
          this.name = this.template.name
          this.description = this.template.description
          this.preview = this.template.preview
          this.image = this.template.image
          this.loadTemplateEnvironmentConfig(this.template)
          let userId = this.chatbotApi.chatbotAuth.firebaseUser.uid
          if (this.template.owner != userId) this.readonly = true
          // console.log(result)
        }))
      }

      await Promise.all(tasks)
    } catch (err) {
      // console.error(err)
      this.alert.handleError(err);
      // this.alert.error("Lỗi khi lấy dữ liệu", err.message)
    } finally {
      this.loadDone = true
      this.changeRef.detectChanges()
    }
  }

  selectApp(app) {
    if (app) {
      if (!this.name || (this.selectedApp && this.selectedApp.activePage['name'] == this.name)) {
        this.name = app.activePage.name
      }
      this.selectedApp = app
      this.loadAppEnvironmentConfig(app)
    } else {
      if (this.selectedApp && this.selectedApp.activePage['name'] == this.name) {
        this.name = ''
      }
      this.selectedApp = null
      this.environment = null
    }
    this.changeRef.detectChanges()
  }

  async loadTemplateEnvironmentConfig(template) {
    if (template.environmentConfig) {
      this.environmentConfig = Object.keys(template.environmentConfig).map(key => ({ key: key, value: template.environmentConfig[key]}))
    }
    this.loadEnvironmentDone = true
    this.changeRef.detectChanges()
    console.log(this.loadEnvironmentDone)
  }

  async loadAppEnvironmentConfig(app) {
    try {
      this.loadEnvironmentDone = false
      this.changeRef.detectChanges()
      if (app.environment && typeof app.environment == "string") {
        this.environment = await this.chatbotApi.environment.get(app.environment, app._id, app.accessToken);
      } else {
        this.environment = await this.chatbotApi.environment.get((app.environment as iEnvironment)._id, app._id, app.accessToken);
      }
      if (this.environment && this.environment.data) {
        this.environmentConfig = Object.keys(this.environment.data).map(key => ({ key: key, value: this.environment.data[key]}))
      } else {
        this.environmentConfig = []
      }
      // console.log(this.environment)
    } catch (err) {
      // console.error(err)
      this.alert.handleError(err, "Lỗi khi tải biến môi trường");
      // this.alert.error('Có lỗi xảy ra khi tải biến môi trường', err.message)
    } finally {
      this.loadEnvironmentDone = true
      this.changeRef.detectChanges()
    }
  }

  async createTemplate() {
    try {
      if (!this.selectedApp && this.add) {
        await this.alert.info('Bạn chưa chọn app', 'Chọn app để tạo mẫu')
        return
      }

      if (!this.name) {
        await this.alert.info('Bắt buộc nhập tên', 'Tên mẫu không được để trống')
        this.nameError = true
        return
      }
      if (!this.description) {
        await this.alert.info('Bắt buộc nhập mô tả', 'Mô tả không được để trống')
        this.descError = true
        return
      }

      if (!this.loadEnvironmentDone) {
        await this.alert.info('Chưa tải xong biến môi trường', 'Xin chờ tải xong danh sách biến môi trường của app')
        return
      }

      let environmentConfig
      if (this.environmentConfig) {
        environmentConfig = this.environmentConfig.reduce((result, item) => {
          result[item.key] = item.value
          return result
        }, {})
      } else {
        environmentConfig = {}
      }
      
      this.submitting = true
      if (this.add) {
        let temp = await this.chatbotApi.page.createTemplateApp(this.selectedApp.activePage['_id'], this.selectedApp._id, this.selectedApp.accessToken, 
        { name: this.name, description: this.description, image: this.image, author: '', preview: this.preview, environmentConfig })
        await this.alert.success("Thành công", `Mẫu ${this.name} đã được tạo`)
        this.router.navigate(['/template', temp._id])
      } else {
        this.template.name = this.name
        this.template.description = this.description
        this.template.image = this.image
        this.template.preview = this.preview
        this.template.environmentConfig = this.environmentConfig
        let firebaseToken = await this.chatbotApi.chatbotAuth.firebaseToken
        await this.chatbotApi.template.update(this.id, this.template, {
          headers: {
          'access_token': firebaseToken
        }})
        this.snackBar.open('✔️ Lưu thành công', '', {
          duration: 2000,
        });
      }
    } catch (err) {
      // console.error(err)
      if (this.add) {
        this.alert.handleError(err, "Lỗi khi tạo mẫu");
        // this.alert.error('Tạo mẫu thất bại', err.message)
      } else {
        this.alert.handleError(err);
        // this.alert.error('Lưu thay đổi mẫu thất bại', err.message)
      }
    } finally {
      this.submitting = false
      this.changeRef.detectChanges()
    }
  }

  isExpired(date) {
    if (!date) return false
    let expiredMoment = moment(date);
    let expiredMomentUTC = expiredMoment.utc()
    let nowUTC = moment.utc()
    let diffDuration = moment.duration(expiredMomentUTC.diff(nowUTC))
    let minDiff = diffDuration.asMinutes();
    if (minDiff > 0) {
      return false
    } else {
      return true
    }
  }
  

}