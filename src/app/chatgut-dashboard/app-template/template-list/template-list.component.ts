import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { ChatbotApiService, iTemplate, iTemplateCategory } from "app/services/chatbot";
import { AlertService } from "app/services/alert.service";
import { MatSnackBar, MatDialog } from "@angular/material";
import { TemplateShareDialog } from "../template-share/template-share.component";
import { BehaviorSubject } from "rxjs";

@Component({
  selector: 'app-template-list',
  templateUrl: './template-list.component.html',
  styleUrls: ['./template-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TemplateListComponent {
  
  types = [
    { code: 'private', display: 'Mẫu của tôi' }, 
    { code: 'shared', display: 'Mẫu được chia sẻ' },
    { code: 'public', display: 'Mẫu chung' }
  ]
  currentTypeIndex = 0

  templates: iTemplate[] = []
  categories: iTemplateCategory[] = []

  loadDone: boolean = false

  constructor(
    private chatbotApi: ChatbotApiService,
    private changeRef: ChangeDetectorRef,
    private alert: AlertService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {

  }

  async ngOnInit() {
    await this.reload()
  }

  async reload(local = true) {
    try {
      let firebaseToken = await this.chatbotApi.chatbotAuth.firebaseToken
      let tasks = []
      
      tasks.push(this.chatbotApi.template.getList({
        local,
        headers: {
          'access_token': firebaseToken
        }
      }).then(result => { this.templates = result }))

      tasks.push(this.chatbotApi.templateCategory.getList().then(result => {
        this.categories = [...result]
      }))

      await Promise.all(tasks)

      // console.log(this.templates, this.categories)
      for (let t of this.templates) {
        let cat = this.categories.find(x => t.category.includes(x._id))
        t.categoryName = cat?cat.name:''
      }

    } catch (error) {
      // console.log("error: ", error)
      this.alert.handleError(error);
      // this.alert.error("Lỗi khi lấy dữ liệu", error.message)
    } finally {
      this.loadDone = true;
      this.changeRef.detectChanges()
    }
  }

  async openShareDialog(template) {
    this.dialog.open(TemplateShareDialog, {
      width: '650px',
      data: {
        template,
        parent: this
      }
    })
  }

  async removeTemplate(temp) {
    if (!await this.alert.warning('Xóa mẫu', 'Bạn có muốn xóa mẫu này')) return

    try {
      await this.chatbotApi.template.deleteTemplate(temp._id)
      this.snackBar.open('Đã xoá mẫu ' + temp.name, '', {
        duration: 2000,
      });
      this.chatbotApi.template.setRequestReload()
      this.templates.splice(this.templates.findIndex(x => x._id == temp._id), 1)
    } catch (err) {
      // console.log(err)
      this.alert.handleError(err, "Lỗi khi xóa");
      // this.alert.error("Xóa template thất bại", err.message)
    } finally {
      this.changeRef.detectChanges()
    }
  }
  
  unpublishing$ = new BehaviorSubject<{}>({})
  async unpublishTemplate(id) {
    try {
      this.unpublishing$.next({...this.unpublishing$.getValue(), [id]: true})
      await this.chatbotApi.template.updateTemplate(id, {
        type: 'private'
      })
      this.alert.success('Chuyển thành mẫu thường thành công', 'Mẫu đã được chuyển đổi về mẫu cá nhân')
      this.reload(false)
    } catch (err) {
      console.error(err)
      this.alert.error('Lỗi khi chuyển thành mẫu chung', err.message)
    } finally {
      this.unpublishing$.next({...this.unpublishing$.getValue(), [id]: false})
    }
  }

}