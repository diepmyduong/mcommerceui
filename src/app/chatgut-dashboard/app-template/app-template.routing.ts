import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppTemplateComponent } from './app-template.component';
import { TemplateListComponent } from './template-list/template-list.component';
import { TemplateDetailsComponent } from './template-details/template-details.component';
import { TemplateInviteComponent } from './template-invite/template-invite.component';


const routes: Routes = [{
    path: "",
    component: AppTemplateComponent,
    children: [{
        path: "",
        component: TemplateListComponent
    }, {
        path: "list",
        redirectTo: ""
    }, {
        path: "acceptInviteLink",
        component: TemplateInviteComponent
    }, {
        path: "add",
        component: TemplateDetailsComponent
    }, {
        path: ":id",
        component: TemplateDetailsComponent
    }, {
        path: "**",
        redirectTo: ""
    }]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ChatgutAppTemplateRouting { }


