import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ChatbotApiService } from 'app/services/chatbot';
@Component({
  selector: 'app-app-template',
  templateUrl: './app-template.component.html',
  styleUrls: ['./app-template.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppTemplateComponent implements OnInit {
  constructor(
    private chatbotApi: ChatbotApiService
  ) {
  }

  async ngOnInit() {
    this.chatbotApi.setTitle('Templates')
  }
}
