import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from 'app/shared';
import { TemplateFilterPipeModule } from 'app/shared/pipes/template-filter-pipe/template-filter-pipe.module';
import { AppCreationComponent } from './app-creation.component';

@NgModule({
  providers: [],
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: "", component: AppCreationComponent }]),
    MaterialModule,
    FormsModule,
    TemplateFilterPipeModule
  ],
  declarations: [AppCreationComponent],
  entryComponents: []
})
export class AppCreationModule { }
