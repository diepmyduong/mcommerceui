import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import { ChatbotApiService, iTemplate, iFanpage, iApp, iPlugin, iTemplateCategory } from 'app/services/chatbot';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'app/services/alert.service';
import { MatDialog } from '@angular/material';
import { get, sortBy } from 'lodash-es'
import { DeviceDetectorService } from 'ngx-device-detector';
import { ChatgutDashboardService } from '../chatgut-dashboard.service';

@Component({
  selector: 'app-creation',
  templateUrl: './app-creation.component.html',
  styleUrls: ['./app-creation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppCreationComponent implements OnInit {

  @ViewChild("appNameInput") appNameInput: ElementRef
  constructor(
    public chatbotApi: ChatbotApiService,
    public route: ActivatedRoute,
    public router: Router,
    private alert: AlertService,
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef,
    private deviceDetector: DeviceDetectorService,
    private dashboardSrv: ChatgutDashboardService
  ) {
  }
  user: any;
  appInfo: any
  app: iApp
  hasAppPrior: boolean = false
  template_filter: string
  
  isShowConfig: boolean = false
  isUseSpreadsheet: boolean = false
  ssPlugin: iPlugin
  categories: iTemplateCategory[] = []
  selectedCategories: string[] = []

  appName: string
  fanpages: iFanpage[] = []
  selectedFanpage: iFanpage
  templates: iTemplate[] = []
  selectedTemplate: iTemplate
  email: string = ''

  templateSelected: boolean = false
  loadDone: boolean = false
  submitting: boolean = false

  types = [
    { code: 'public', display: 'Mẫu chung' }, 
    { code: 'private', display: 'Mẫu của tôi' }, 
    { code: 'shared', display: 'Mẫu được chia sẻ' }]
  currentTypeIndex = 0

  currentStep = 1

  async ngOnInit() {
    this.chatbotApi.setTitle('Create bot app')
    this.app = JSON.parse(this.route.snapshot.queryParamMap.get("app"))
    if (this.app) this.hasAppPrior = true
    await this.reload()
  }

  async reconnectFanpage() {
    await this.chatbotApi.chatbotAuth.getFacebookToken(true)
    this.reload(false)
  }

  async reload(local = true) {
    try {
      let firebaseToken = await this.chatbotApi.chatbotAuth.firebaseToken
      this.user = this.chatbotApi.chatbotAuth.firebaseUser;

      // this.chatbotApi.user.getIntros()

      if (!this.chatbotApi.chatbotAuth.facebookToken) {
        if (!await this.alert.question('Tài khoản chưa kết nối Facebook', 'Bạn có muốn kết nối Facebook không?', '', '', 
        () => this.chatbotApi.chatbotAuth.getFacebookToken())) {
          this.router.navigate([''])
          return 
        }
      }
      const userToken = this.chatbotApi.chatbotAuth.facebookToken

      let tasks = []

      tasks.push(this.chatbotApi.page.getFanpages(userToken, { local: false }).then(result => {
        this.fanpages = sortBy(result, ["is_webhooks_subscribed", "name_with_location_descriptor"])
      }))
      
      tasks.push(this.chatbotApi.template.getList({
        headers: {
          'access_token': firebaseToken
        }
      }).then(result => this.templates = result))

      tasks.push(this.chatbotApi.templateCategory.getList().then(result => {
        this.categories = [...result]
      }))

      await Promise.all(tasks)
      for (let t of this.templates) {
        let cat = this.categories.find(x => t.category.includes(x._id))
        t.categoryName = cat?cat.name:undefined
      }

      console.log(this.templates)

    } catch (error) {
      // console.log("error: ", error)
      this.alert.handleError(error);
      // this.alert.error("Lỗi khi lấy dữ liệu", error.message)
    } finally {
      this.loadDone = true;
      this.changeRef.detectChanges()
      // setTimeout(() => {
      //   (this.appNameInput.nativeElement as HTMLInputElement).focus()
      // })
    }
  }

  async selectFanpage(fanpage) {
    this.selectedFanpage = fanpage; 
    if (this.currentStep == 1) { 
      this.currentStep++
      this.toStep(2)
    }
    this.changeRef.detectChanges()
  }

  async selectTemplate(template) {
    this.selectedTemplate = template;
    this.templateSelected = true 
    if (this.currentStep == 2) { 
      this.currentStep++
      this.toStep(3)
    }
    this.changeRef.detectChanges()
  }

  async connectPage() {
    if (!this.loadDone) {
      return
    }

    this.appName = 'App ' + this.selectedFanpage.name

    // if (!this.appName) {
    //   await this.alert.info("Thiếu tên app", "Yêu cầu đặt tên cho app cần tạo")
    //   setTimeout(() => {
    //     (this.appNameInput.nativeElement as HTMLInputElement).focus()
    //   })
    //   return
    // }

    if (!this.selectedFanpage) {
      this.alert.info("Chưa chọn fanpage", "Yêu cầu chọn fanpage để kết nối với chatbot")
      return 
    }
    try {
      this.submitting = true
      this.changeRef.detectChanges()
      if (!this.chatbotApi.chatbotAuth.facebookToken) {
        await this.chatbotApi.chatbotAuth.getFacebookToken()
      }
      const userToken = this.chatbotApi.chatbotAuth.facebookToken
      const pageToken = this.selectedFanpage.access_token
      if (this.hasAppPrior) {    
        let page = await this.chatbotApi.app.subscribePage({
          appId: this.app._id, appToken: this.app.accessToken,
          userToken, pageToken
        })
        if (this.selectedTemplate) {
          await this.chatbotApi.template.importTemplate(this.app._id, this.app.accessToken, page._id, this.selectedTemplate._id)
        }
        this.app = await this.chatbotApi.app.getItem(this.app._id, { reload: true, local: false })
      } else {
        this.app = await this.chatbotApi.app.connect(this.appName, await this.chatbotApi.chatbotAuth.firebaseToken,
          userToken, pageToken, this.selectedTemplate ? this.selectedTemplate._id : '')
      }
      this.dashboardSrv.reloadApp(this.app)

      if (get(this.selectedTemplate,'option.isUsedSpreadsheetPlugin')) {
        this.chatbotApi.user.setSpreadsheetFirst(this.app._id)
        this.addSpreadsheetEmail()
      }
      this.submitting = false
      await this.alert.success("Thành công", this.hasAppPrior?"Kết nối Trang vào App thành công":"Tạo app thành công")

      this.chatbotApi.app.getList({
        local: false,
        query: {
          order: {
            createdAt: -1
          }
        }
      })
      // this.isShowConfig = true
      if (this.deviceDetector.isDesktop) {
        this.router.navigate(['apps', this.app._id])
      } else {
        this.router.navigate(['simple', this.app._id])
      }
    } catch (err) {
      console.error('subscribe error', err)
      if(err.error && err.error.message == "This page has been subscibed"){
        this.alert.error("Không thể đăng ký", "Trang này đã được đăng kí vui lòng chọn trang khác")
      }else{
        this.alert.handleError(err, "Lỗi khi đăng ký");
        // this.alert.error("Không thể đăng ký", err.message)
      }
      this.submitting = false
    } finally {
      this.changeRef.detectChanges()
    }
  }

  async addSpreadsheetEmail() {
    try {
      let plugin = await this.chatbotApi.plugin.getSpreadsheetPlugin(this.app._id, this.app.accessToken)
      this.chatbotApi.plugin.addUser(plugin._id, this.email, this.app._id, this.app.accessToken)
    } catch(err) {
      this.alert.handleError(err, "Lỗi khi thêm email");
      // this.alert.error("Thêm emal thất bại", err.message)
    }
  }

  async selectCategory(cat) {
    console.log('')
    let index = this.selectedCategories.findIndex(x => x == cat._id)
    if (index > -1) {
      this.selectedCategories.splice(index, 1)
      cat.active = false
    } else {
      this.selectedCategories.push(cat._id)
      cat.active = true
    }
    this.changeRef.detectChanges()
  }

  async toStep(index) {
    setTimeout(() => {
      switch (index) {
        case 2:
        try {
          document.getElementsByClassName('add-template').item(0).scrollIntoView({ block: 'start', behavior: 'smooth' })
        } catch (err) {
          console.error(err)
        }
        break;
        case 3:
        try {
          document.getElementsByClassName('app-container').item(0).scroll({ top: document.getElementsByClassName('app-container').item(0).scrollHeight, behavior: 'smooth' })
        } catch (err) {
          document.getElementsByClassName('app-container').item(0).scroll(0, document.getElementsByClassName('app-container').item(0).scrollHeight)
        }
        break;
      }
      this.changeRef.detectChanges()
    }, 100)
  }
}


