import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { iApp, ChatbotApiService } from "app/services/chatbot";
import { cloneDeep, orderBy } from 'lodash-es'
import * as moment from 'moment'

@Injectable({
  providedIn: 'root'
})
export class ChatgutDashboardService {
  isAppList$ = new BehaviorSubject<boolean>(false)
  loadingBots$ = new BehaviorSubject<boolean>(false)
  apps$ = new BehaviorSubject<iApp[]>(undefined)
  set apps(value) { this.apps$.next(value) }
  get apps() { return this.apps$.getValue() }
  appFilter$ = new BehaviorSubject<string>('')
  appType$ = new BehaviorSubject<string>('')
  userUid: string

  constructor(
    public chatbotApi: ChatbotApiService
  ) {
    moment.locale('vi')
    this.userUid = this.chatbotApi.chatbotAuth.firebaseUser.uid
  }

  async reloadApps(local: boolean = true) {
    this.loadingBots$.next(true)
    try {
      let apps = cloneDeep(await this.chatbotApi.app.getList({ local,  query: { order: {  createdAt: -1  } }}) ) as iApp[]
      apps.forEach(app => this.postProcessApp(app))
      this.apps = apps
      console.log(apps)
    } catch(err) {
      console.error(err)
    } finally {
      this.loadingBots$.next(false)
    }
  }

  async reloadApp(app) {
    try {
      let newApp = await this.chatbotApi.app.getItem(app._id, { local: false })
      this.postProcessApp(newApp)
      let index = this.apps.findIndex(x => x._id == newApp._id)
      if (index >= 0) this.apps[index] = newApp
      else this.apps.splice(0, 0, newApp)
    } catch (err) {
      let index = this.apps.findIndex(x => x._id == app._id)
      this.apps.splice(index, 1)
    } finally {
      this.apps = [...this.apps]
    }
  }

  postProcessApp(app: iApp) {
    this.checkOwner(app)
    this.setExpiration(app)
    this.setRule(app)
  }

  checkOwner(app: iApp) {
    app.isOwner = (this.userUid == app.owner)?true:false
  }
  
  setExpiration(app: iApp) {
    if (app.whiteListed) {
      app.expiredClass = "label-whitelist"
      app.expiredDuration = "vô thời hạn"
      app.expiredDate = "mãi mãi"
      return
    } 

    let expiredMoment = moment(app.license.expireDate);
    app.expiredDate = expiredMoment.format('HH:mm dddd DD-MM-YYYY');

    let expiredMomentUTC = expiredMoment.utc()
    let nowUTC = moment.utc()
    let diffDuration = moment.duration(expiredMomentUTC.diff(nowUTC))
    let minDiff = diffDuration.asMinutes();
    let dayDiff = Math.round(diffDuration.asDays());
    let monthDiff = Math.round(diffDuration.asMonths());
    let yearDiff = Math.round(diffDuration.asYears());
    if (app.license.status === "locking") {
      app.isLocked = true
    }
    if (minDiff > 0) {
      if (yearDiff >= 1) {
        app.expiredDuration = 'còn ' + yearDiff + ' năm'
      } else {
        if (monthDiff >= 1) {
          app.expiredDuration = 'còn ' + monthDiff + ' tháng'
        } else {
          if (dayDiff >= 1) {
            app.expiredDuration = 'còn ' + dayDiff + ' ngày'
          } else {
            app.expiredDuration = 'hết hạn hôm nay'
          }
        }
      }

      if (dayDiff > 15) {
        app.expiredClass = 'label-positive';
      } else if (dayDiff > 7) {
        app.expiredClass = 'label-neutral';
      } else {
        app.isExpiredSoon = true;
        app.expiredClass = 'label-danger';
      }

    } else {
      app.expiredDuration = 'đã hết hạn'
      app.isExpired = true;

      app.expiredClass = 'label-negative'
    }
  }

  setRule(app: iApp) {
    app.rule = app.users.find(x => x.uid == this.userUid).rule
  }
}