import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '@environments';
import { AlertService } from 'app/services/alert.service';
import { iApp } from 'app/services/chatbot/api/crud/app';
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service';
import { iPopoverFormOptions } from 'app/shared/popover-form/popover-form.component';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import * as moment from 'moment';
import { DeviceDetectorService } from 'ngx-device-detector';
import { BehaviorSubject } from 'rxjs';
@Component({
  selector: 'app-app-block',
  templateUrl: './app-block.component.html',
  styleUrls: ['./app-block.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppBlockComponent implements OnInit {
  showLoading$ = new BehaviorSubject<boolean>(false)
  url: string
  simpleApp: boolean = false

  @Input() mode: "instance" | "new" = "instance"
  @Input() app: iApp
  @Output() change = new EventEmitter<any>()
  constructor(
    public chatbotApi: ChatbotApiService,
    public router: Router,
    public route: ActivatedRoute,
    public dialog: MatDialog,
    public changeDetector: ChangeDetectorRef,
    public alert: AlertService,
    private deviceService: DeviceDetectorService,
    private popoverService: PopoverService,
    public changeRef: ChangeDetectorRef
  ) {
    moment.locale('vi')
    if (!this.deviceService.isDesktop()) {
      this.simpleApp = true
    }
  }

  async ngOnInit() {
    if (this.mode === "instance") {
      this.url = `${environment.chatbot.livechatHost}?token=${this.appToken}`
    }
  }

  async openApp() {
    if (this.app.isExpired) return;

    this.showLoading$.next(true)
    await this.router.navigate(['apps', this.app._id])
    this.showLoading$.next(false)
  }

  async subscribePage() {
    this.router.navigate(['/add'], { queryParams:{ app: JSON.stringify({ _id: this.app._id, accessToken: this.app.accessToken }) } })
  }

  async removeApp() {
    if (this.app.activePage) {
      this.alert.error('Không thể xóa app', 'Cần phải hủy đăng ký các trang trước khi xóa app')
      return;
    }

    if (!await this.alert.warning('Xóa app', 'Bạn có chắc chắn muốn xóa app không? Tất cả dữ liệu sẽ bị mất', 'Tôi muốn xoá app')) return;
    try {
      this.showLoading$.next(true)
      await this.chatbotApi.app.delete(this.app._id, {
        headers: {
          app_id: this.app._id,
          app_token: this.app.accessToken
        }
      });
      this.change.emit()
      this.chatbotApi.user.removeStates(this.app._id)
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi hủy đăng ký");
      // this.alert.error("Không thể hủy đăng ký", err.message)
    } finally {
      this.showLoading$.next(false)
    }
  }

  async unsubscribePage() {
    if (!await this.alert.warning('Huỷ đăng ký trang', 'Mọi dữ liệu sẽ bi xoá bỏ', 'Hủy đăng ký trang')) return;
    try {
      this.showLoading$.next(true)
      await this.chatbotApi.app.unsubscribePage({
        appId: this.app._id,
        appToken: this.app.accessToken
      })
      this.change.emit()
      if (this.chatbotApi.page.fanpages.getValue().length > 0) {
        await this.chatbotApi.page.getFanpages(this.chatbotApi.chatbotAuth.facebookToken, { local: false, reload: true })
      }
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi hủy đăng ký");
      // this.alert.error("Không thể hủy đăng ký", err.message)
    } finally {
      this.showLoading$.next(false)
    }
  }

  async refeshPageToken() {
    try {
      if (!this.chatbotApi.chatbotAuth.facebookToken) {
        await this.chatbotApi.chatbotAuth.getFacebookToken()
      }
      this.showLoading$.next(true)
      const userToken = this.chatbotApi.chatbotAuth.facebookToken
      await this.chatbotApi.app.refeshPageToken({
        appId: this.app._id, appToken: this.app.accessToken, userToken: userToken
      })
      this.change.emit()
    } catch (err) {
      this.alert.handleError(err, "Lỗi khi kết nối lại trang");
      // this.alert.error("Không thể kết nối lại Trang", err.message)
    } finally {
      this.showLoading$.next(false)
    }

  }

  async leaveApp() {
    if (!await this.alert.warning('Rời khỏi app', "Bạn có chắc chắn muốn thoát khỏi app này không?", 'Rời khỏi')) return;
    try {
      const userId = await this.chatbotApi.chatbotAuth.firebaseUser.uid
      await this.chatbotApi.app.leaveApp(this.app._id, { reload: true })
      this.change.emit()
      this.alert.success("Thành công", "Bạn đã rời khỏi app thành công")
    } catch (err) {
      this.alert.handleError(err);
      // this.alert.error("Có lỗi xảy ra", err.message)
    } finally {
      this.changeRef.detectChanges()
    }
  }

  async requestExpandExpire() {
    this.router.navigate(["user/cost"], { queryParams:{ appId : this.app._id } })
  }

  async editAppName(event) {
    let formOptions: iPopoverFormOptions = {
      parent: '#app-list',
      targetElement: event.target,
      width: 300,
      fields: [
        {
          placeholder: 'Tên app',
          property: 'name',
          current: this.app.name,
          constraints: ['required']
        }
      ],
      onSubmit: (data) => {
        if (this.app.name == data.name || !data.name) return
        let fallback = this.app.name
        this.app.name = data.name
        this.chatbotApi.chatbotAuth.app = this.app
        this.changeRef.detectChanges()
        this.chatbotApi.app.updateName(this.app._id, data.name).then().catch(err => {
          console.error(err)
          this.app.name = fallback
          this.changeRef.detectChanges()
        })
        this.change.emit()
      }
    }
    this.popoverService.createPopoverForm(formOptions)
  }

  get appToken() {
    return `A|${this.app._id}|${this.app.accessToken}`
  }
}
