import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ChatgutDashboardService } from '../chatgut-dashboard.service';
import { ChatbotApiService, iApp } from 'app/services/chatbot';

@Component({
  selector: 'app-list',
  templateUrl: './app-list.component.html',
  styleUrls: ['./app-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppListComponent implements OnInit {
  constructor(
    public dashboardSrv: ChatgutDashboardService,
    private chatbotApi: ChatbotApiService
  ) {    
    this.chatbotApi.setTitle('Dashboard')
  }
  
  ngOnInit() {

  }

  trackByFn(index, item) {
    return item._id
  }

  onAppChanged(app: iApp) {
    console.log(app)
    this.dashboardSrv.reloadApp(app)
    // this.dashboardSrv.appChanged()
  }
}


