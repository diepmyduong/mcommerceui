import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppListComponent } from './app-list.component';
import { MatProgressBarModule, MatTooltipModule, MatButtonModule, MatSelectModule } from '@angular/material';
import { AppFilterPipeModule } from 'app/shared/pipes/app-filter-pipe/app-filter-pipe.module';
import { AppListToolbarComponent } from './app-list-toolbar/app-list-toolbar.component';
import { FormsModule } from '@angular/forms';
import { AppBlockComponent } from './app-block/app-block.component';

@NgModule({
  providers: [],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: "", component: AppListComponent }]),
    MatProgressBarModule,
    MatTooltipModule,
    MatSelectModule,
    MatButtonModule,
    AppFilterPipeModule
  ],
  declarations: [AppBlockComponent, AppListComponent, AppListToolbarComponent],
  entryComponents: []
})
export class AppListModule { }
