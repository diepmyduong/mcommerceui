import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ChatgutDashboardService } from 'app/chatgut-dashboard/chatgut-dashboard.service';
import { Subject, BehaviorSubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { orderBy } from 'lodash-es'

@Component({
  selector: 'app-list-toolbar',
  templateUrl: './app-list-toolbar.component.html',
  styleUrls: ['./app-list-toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppListToolbarComponent implements OnInit {
  constructor(
    public dashboardSrv: ChatgutDashboardService
  ) {
    let sortMode = localStorage.getItem('app-sort-mode')
    if (sortMode && this.sortModes.find(x => x.code == sortMode)) this.sortMode$.next(sortMode)
  }

  appFilter: string = '';
  destroy$ = new Subject()

  appNum$ = new BehaviorSubject<number>(undefined)
  expiredAppNum$ = new BehaviorSubject<number>(undefined)
  disconnectedAppNum$ = new BehaviorSubject<number>(undefined)

  sortMode$ = new BehaviorSubject<string>('createdAt_desc')
  sortModes = [
    { code: 'createdAt_desc', display: 'Sắp xếp Mới đến cũ' },
    { code: 'createdAt_asc', display: 'Sắp xếp Cũ đến mới' },
    { code: 'updatedAt_desc', display: 'Sắp xếp Cập nhật mới nhất' },
  ]
  
  ngOnInit() {
    this.dashboardSrv.apps$.pipe(takeUntil(this.destroy$)).subscribe(apps => {
      this.appNum$.next(apps.length)
      this.expiredAppNum$.next(apps.filter(x => x.isExpired).length)
      this.disconnectedAppNum$.next(apps.filter(x => x.pageTokenExpired).length)
    })

    this.sortMode$.pipe(takeUntil(this.destroy$)).subscribe(mode => {
      localStorage.setItem('app-sort-mode', mode)
      switch (mode) {
        case 'createdAt_desc': {
          this.dashboardSrv.apps = orderBy(this.dashboardSrv.apps, ['createdAt'], ['desc'])
          break
        }
        case 'createdAt_asc': {
          this.dashboardSrv.apps = orderBy(this.dashboardSrv.apps, ['createdAt'], ['asc'])
          break
        }
        case 'updatedAt_desc': {
          this.dashboardSrv.apps = orderBy(this.dashboardSrv.apps, ['updatedAt'], ['desc'])
          break
        }
      }
    })
  }

  ngOnDestroy() {
    this.destroy$.next()
  }

  reload() {
    this.dashboardSrv.reloadApps(false)
  }

  async setFilterType(type) {
    this.dashboardSrv.appType$.next(type)
  }

}


