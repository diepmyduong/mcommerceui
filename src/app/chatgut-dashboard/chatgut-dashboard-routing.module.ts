import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatgutDashboardComponent } from 'app/chatgut-dashboard/chatgut-dashboard.component';
import { FirebaseAuthGuard } from 'app/services/chatbot'
const routes: Routes = [{
  path: "",
  redirectTo: "apps",
  pathMatch: "full"
},{
  path: "apps",
  component: ChatgutDashboardComponent,
  loadChildren: 'app/chatgut-dashboard/app-list/app-list.module#AppListModule',
  canActivate: [FirebaseAuthGuard],
  pathMatch: 'full'
},{
  path: "apps",
  canActivate: [FirebaseAuthGuard],
  loadChildren: 'app/chatgut-app/chatgut-app.module#ChatgutAppModule',
  pathMatch: 'prefix'
}, {
  path: "simple/:id",
  component: ChatgutDashboardComponent,
  canActivate: [FirebaseAuthGuard],
  loadChildren: 'app/chatgut-dashboard/simple-app/simple-app.module#SimpleAppModule'
}, {
  path: "add",
  component: ChatgutDashboardComponent,
  canActivate: [FirebaseAuthGuard],
  loadChildren: 'app/chatgut-dashboard/app-creation/app-creation.module#AppCreationModule'
},{
  path: "template",
  component: ChatgutDashboardComponent,
  canActivate: [FirebaseAuthGuard],
  loadChildren: 'app/chatgut-dashboard/app-template/app-template.module#AppTemplateModule'
},{
  path: "user",
  component: ChatgutDashboardComponent,
  canActivate: [FirebaseAuthGuard],
  loadChildren: 'app/chatgut-dashboard/app-user/app-user.module#AppUserModule'
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatgutDashboardRoutingModule { }
