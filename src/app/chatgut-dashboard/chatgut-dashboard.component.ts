import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, QueryList, ViewChild, ViewChildren, ViewContainerRef } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AppBlockComponent } from 'app/chatgut-dashboard/app-list/app-block/app-block.component';
import { EditInfoDialog, iEditInfoDialogData } from 'app/modals/edit-info/edit-info.component';
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service';
import { DynamicComponentLoaderService } from 'app/services/dynamic-component-loader/dynamic-component-loader.service';
import { PopoverService } from 'app/shared/popover-form/popover.service';
import { Subject } from 'rxjs';
import "rxjs/add/operator/map";
import "rxjs/add/operator/startWith";
import "rxjs/add/operator/takeWhile";
import { takeUntil } from 'rxjs/operators';
import { ChatgutDashboardService } from './chatgut-dashboard.service';
import { isEqual } from 'lodash-es'
@Component({
  selector: 'app-chatgut-dashboard',
  templateUrl: './chatgut-dashboard.component.html',
  styleUrls: ['./chatgut-dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatgutDashboardComponent implements OnInit {
  user: any;
  sidebarOpened: boolean = false
  appFilter: string
  destroy$ = new Subject()
  customer: any
  
  @ViewChildren('appTile') appTileList:QueryList<AppBlockComponent>
  @ViewChild("popoverContainer", { read: ViewContainerRef }) popoverContainer: ViewContainerRef
  constructor(
    public chatbotApi: ChatbotApiService,
    public router: Router,
    public dialog: MatDialog,
    public changeRef: ChangeDetectorRef,
    public popoverService: PopoverService,
    private dynamicLoader: DynamicComponentLoaderService,
    public dashboardSrv: ChatgutDashboardService,
    public route: ActivatedRoute,
  ) {
    const qs = this.route.snapshot.queryParams;
    if (qs) {
      if(window.opener && window.opener.infusionCallback) {
        window.opener.infusionCallback(qs, window)
        return
      }
    }
    this.router.events.pipe(
      takeUntil(this.destroy$)
    ).subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.url == '/apps') this.dashboardSrv.isAppList$.next(true)
        else this.dashboardSrv.isAppList$.next(false)
      }
    })
    this.dashboardSrv.appFilter$.next('')
  }

  async ngOnInit() {
    this.user = this.chatbotApi.chatbotAuth.firebaseUser;
    await this.dashboardSrv.reloadApps()
    this.dynamicLoader.getComponentFactory('ChatgutAppComponent')
    this.dynamicLoader.getComponentFactory('StoriesPortalComponent')
    this.checkInfo()
  }

  async ngOnDestroy() {
    this.destroy$.next()
  }

  async logout() {
    if (await this.chatbotApi.chatbotAuth.logout()) {
      this.router.navigate(['/pages/login'])
    }
  }
  async searchChanged(event) {
    this.dashboardSrv.appFilter$.next(event)
  }
  async checkInfo() {
    this.chatbotApi.customer.me().then(res => {
      this.customer = res
      if (!this.customer.fullName || !this.customer.phone) this.openCustomer()
    })
  }

  async openCustomer() {
    let newCustomer = false
    if (!this.customer) this.customer = await this.chatbotApi.customer.me()
    if (isEqual(this.customer, {})) newCustomer = true 
    let data: iEditInfoDialogData = {
      title: newCustomer?"Yêu cầu bổ sung thông tin tài khoản":"Cập nhật thông tin tài khoản",
      confirmButtonText: newCustomer?"Xác nhận thông tin tài khoản":"Cập nhật thông tin tài khoản",
      inputs: [
        {
          title: "Họ tên",
          property: "fullName",
          type: "text",
          required: true,
          current: this.customer.fullName
        }, {
          title: "Số điện thoại",
          property: "phone",
          type: "text",
          required: true,
          current: this.customer.phone
        }, {
          title: 'Email',
          property: 'email',
          type: 'text',
          current: this.customer.email ? this.customer.email : this.chatbotApi.chatbotAuth.firebaseUser.email
        }
      ]
    };
    let dialogRef = this.dialog.open(EditInfoDialog, {
      width: '500px',
      data: data,
      disableClose: (!this.customer.fullName || !this.customer.phone)?true:false
    });
    const result = await dialogRef.afterClosed().toPromise();
    if (result) {
      this.customer = await this.chatbotApi.customer.updateMe(result)
      this.chatbotApi.customer.clear()
    }
  }
}

