import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SimpleAppComponent } from './simple-app.component';
import { AppAuthGuard } from 'app/services/chatbot';
const routes: Routes = [{
    path: "",
    component: SimpleAppComponent,
    canActivate: [AppAuthGuard]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SimpleAppRoutingModule { }
