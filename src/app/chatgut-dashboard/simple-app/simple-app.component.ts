import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ChatbotApiService, iApp, iPage } from 'app/services/chatbot';
import { isObject } from 'lodash-es'
import { AlertService } from 'app/services/alert.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-simple-app',
  templateUrl: './simple-app.component.html',
  styleUrls: ['./simple-app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpleAppComponent implements OnInit {

  app: iApp
  page: iPage
  
  constructor(
    public chatbotApi: ChatbotApiService,
    private alert: AlertService,
    private snackBar: MatSnackBar,
    public changeRef: ChangeDetectorRef
  ) {
  }

  rules: string[] = ["admin", "write", "read"]
  userEmail: string
  userRule: string = "admin"
  inviteRule: string = "admin"
  inviteLink: string
  loadDone: boolean = false
  sendingInvite: boolean = false
  generatingLink: boolean = false

  async ngOnInit() {
    this.app = this.chatbotApi.chatbotAuth.app

    if (isObject(this.app.activePage)) {
      this.page = this.app.activePage as iPage
      this.chatbotApi.page.getFanpageInfo()
    } else {
      this.page = await this.chatbotApi.page.getItem(this.app.activePage as string,
        {
          local: true,
          reload: false,
        })
      await this.chatbotApi.page.getFanpageInfo()
    }
    this.changeRef.detectChanges()


    await this.setRule("admin")
    console.log(this.app)

  }

  async setRule(rule) {
    if (this.generatingLink) return
    this.inviteRule = rule
    this.generateLink()
    this.changeRef.detectChanges()
  }

  copyLink(){
    if (!this.inviteLink) return
    let input = document.getElementById('link-url') as HTMLInputElement
    input.value = this.inviteLink
    input.select();
    document.execCommand('copy',false);
    this.snackBar.open('✔️ Đã copy vào clipboard', '', {
      duration: 2000,
    });
    this.changeRef.detectChanges()
  }

  async generateLink() {
    try {
      this.generatingLink = true
      this.inviteLink = ''
      this.changeRef.detectChanges()

      let res = await this.chatbotApi.app.generatorLink(this.inviteRule)
      this.inviteLink = res.inviteLink
    } catch(err) {
      this.alert.handleError(err)
    } finally {
      this.generatingLink = false
      this.changeRef.detectChanges()
    }
  }

  async inviteUser() {

    if (!this.userEmail) {
      this.alert.info('Bắt buộc nhập email', 'Email người dùng phải được nhập.')
      return
    }

    let inviteData:any = {
      type: "email",
      rule: this.userRule,
      email: this.userEmail
    }
    try {
      this.sendingInvite = true
      this.changeRef.detectChanges()
      await this.chatbotApi.app.invite(inviteData)
      this.alert.success('Đã gửi thư mời', `Thư mời tham gia quản trị đã được gửi tới email ${inviteData.email}`)
      this.userEmail = ''
    } catch(err) {
      // console.error('err', err.error.message);
      this.alert.handleError(err, "Lỗi khi thêm người dùng");
      // this.alert.error('Lỗi khi thêm người dùng', err.message)
    } finally {
      this.sendingInvite = false
      this.changeRef.detectChanges()
    }
  }

  async reload(local = true) {
    try {

    } catch (error) {
    } finally {
      this.loadDone = true
      this.changeRef.detectChanges()
    }
  }
}


