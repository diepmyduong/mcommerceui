import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SimpleAppComponent } from "./simple-app.component";
import { SimpleAppRoutingModule } from "./simple-app.routing";
import { MatSnackBarModule } from "@angular/material";

@NgModule({
  providers: [],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    SimpleAppRoutingModule
  ],
  declarations: [SimpleAppComponent],
  entryComponents: []
})
export class SimpleAppModule { }
