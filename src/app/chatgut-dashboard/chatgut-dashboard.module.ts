import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChatgutDashboardRoutingModule } from 'app/chatgut-dashboard/chatgut-dashboard-routing.module';
import { ChatgutDashboardComponent } from 'app/chatgut-dashboard/chatgut-dashboard.component';
import { PopoverFormModule } from 'app/shared/popover-form/popover-form.module';
import { MatSidenavModule, MatTooltipModule, MatToolbarModule } from '@angular/material';
import { EditInfoModule } from 'app/modals/edit-info/edit-info.module';

@NgModule({
  imports: [
    CommonModule,
    ChatgutDashboardRoutingModule,
    MatSidenavModule,
    MatTooltipModule,
    MatToolbarModule,
    EditInfoModule,
    FormsModule,
    PopoverFormModule
  ],
  declarations: [ChatgutDashboardComponent],
  entryComponents: []
})
export class ChatgutDashboardModule { }
