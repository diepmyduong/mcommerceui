import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatProgressSpinnerModule, MatTooltipModule, MatTableModule, MatDialogModule, MatMenuModule } from '@angular/material';
import { UserAppListComponent } from './user-app-list.component';
import { AppInviteCustomerModule } from 'app/modals/app-invite-customer/app-invite-customer.module';
import { UserTransferOwnerDialogModule } from 'app/modals/user-transfer-owner/user-transfer-owner.module';

@NgModule({
  providers: [],
  imports: [
    CommonModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatTableModule,
    MatMenuModule,
    MatDialogModule,
    AppInviteCustomerModule,
    UserTransferOwnerDialogModule,
    RouterModule.forChild([{ path: '', component: UserAppListComponent}])
  ],
  declarations: [UserAppListComponent],
  entryComponents: []
})
export class UserAppListModule { }
