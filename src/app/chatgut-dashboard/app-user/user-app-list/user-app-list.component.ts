import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { ChatbotApiService, iApp, App } from "app/services/chatbot";
import { AlertService } from "app/services/alert.service";
import { MatDialog } from "@angular/material";
import * as moment from 'moment'
import { saveAs } from 'file-saver';
import { AppInviteCustomerDialog } from "app/modals/app-invite-customer/app-invite-customer.component";
import { UserTransferOwnerDialog } from "app/modals/user-transfer-owner/user-transfer-owner.component";

@Component({
  selector: 'user-app-list',
  templateUrl: './user-app-list.component.html',
  styleUrls: ['./user-app-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserAppListComponent {

  whitelisting: any = {}
  upgrading: any = {}
  loading: boolean = false
  loadDone: boolean = false
  loadingMore: boolean = false
  apps: iApp[]
  inviteLink: string
  inviting: boolean = false
  whiteListed: boolean = false
  displayedColumns: string[] = ['index', 'page', 'owner', 'totalOfSubscribers', 'expirationDate', 'action']
  followList = []
  followApp: boolean = false

  searchText: string 
  page = 1
  limit = 10
  total

  constructor(
    private chatbotApi: ChatbotApiService,
    private changeRef: ChangeDetectorRef,
    private alert: AlertService,
    private dialog: MatDialog
  ) {
    moment.locale('vi')
  }

  async ngOnInit() {
    this.chatbotApi.setTitle('App list')
    await this.getFollowList()
    await this.reload()
  }

  async getFollowList() {
    this.followList = (await this.chatbotApi.chatbotAuth.firebaseDatabase
    .ref(`users/${this.chatbotApi.chatbotAuth.firebaseUser.uid}/follow`).once('value')).val()
    if (!this.followList) this.followList = []
    console.log(this.followList)
  }

  async toggleFollow(app) {
    app.follow = !app.follow
    if (app.follow) {
      this.followList.push(app._id)
    } else {
      this.followList.splice(this.followList.findIndex(x => x == app._id), 1)
    }

    this.chatbotApi.chatbotAuth.firebaseDatabase
    .ref(`users/${this.chatbotApi.chatbotAuth.firebaseUser.uid}/follow`).set(this.followList)
  }

  async toggleFollowApp() {
    this.followApp = !this.followApp
    this.changeRef.detectChanges()
    this.reload()
  }

  async toggleWhitelist() {
    this.whiteListed = !this.whiteListed
    this.changeRef.detectChanges()
    this.reload()
  }

  createFilter() {
    let andFilter: any = {}
    if (this.followApp) andFilter['_id'] = { $in: this.followList }
    if (this.whiteListed) andFilter['whiteListed'] = this.whiteListed
    // if (this.searchText) {
    //   andFilter['$text'] = { $search: this.searchText }
    // }
    return andFilter
  }

  async reload(local = true) {
    try {
      this.page = 1
      this.loading = true
      this.changeRef.detectChanges()
      let adminToken = this.chatbotApi.chatbotAuth.systemUser.token
      let tasks = []
        // andFilter.$and.push({ search: { $text: this.searchText } })
        // andFilter.$or = []
        // andFilter.$or.push({
        //   { 'activePage.name': { $regex: this.searchText } }
        // }
    
      
      tasks.push(this.chatbotApi.systemUser.getApps({
        local,
        headers: {
          'x-api-key': adminToken
        },
        query: {
          limit: this.limit,
          populates: [{
            path: 'activePage',
            select: 'name meta'
          }, {
            path: 'license'
          }],
          search: this.searchText?this.searchText:undefined,
          filter: this.createFilter()
        }
      }).then(result => { 
        this.total = result.count;
        for (let row of result.rows) {
          if (typeof(row.owner=='string')) {
            if (row.owner) row.ownerObj = row.users.find(x => x.uid == row.owner)
            else row.ownerObj = ''
          }
          this.calculateExpiration(row)
          row.follow = this.followList.includes(row._id)
        }
        this.apps = result.rows
      }))

      await Promise.all(tasks)
      console.log(this.apps)

    } catch (error) {
      this.alert.handleError(error, 'Lỗi khi lấy danh sách app');
    } finally {
      this.loading = false
      this.loadDone = true
      this.changeRef.detectChanges()
    }
  }

  async loadMore(local = true) {
    if (this.loadingMore) return

    this.loadingMore = true
    this.changeRef.detectChanges()
    try {
      let adminToken = this.chatbotApi.chatbotAuth.systemUser.token
      let tasks = []

      tasks.push(this.chatbotApi.systemUser.getApps({
        local,
        headers: {
          'x-api-key': adminToken
        },
        query: {
          limit: this.limit,
          offset: this.apps.length,
          populates: [{
            path: 'activePage',
            select: 'name meta'
          }, {
            path: 'license'
          }],
          search: this.searchText?this.searchText:undefined,
          filter: this.createFilter()
        }
      }).then(result => { 
        for (let row of result.rows) {
          if (typeof(row.owner=='string')) {
            if (row.owner) row.ownerObj = row.users.find(x => x.uid == row.owner)
            else row.ownerObj = ''
          }
          this.calculateExpiration(row)
        }
        this.apps = [...this.apps, ...result.rows]
      }))

      await Promise.all(tasks)
    } catch (error) {
      this.alert.handleError(error, 'Lỗi khi tải thêm agency');
    } finally {
      this.loadingMore = false
      this.changeRef.detectChanges()
    }
  }

  timeout
  searchChanged() {
    if (this.timeout) clearTimeout(this.timeout)
    if (!this.searchText) {
      this.reload()
    } else {
      this.timeout = setTimeout(() => {
        this.reload()
      }, 300)
    }
  }

  calculateExpiration(app) {

    if (app.whiteListed) {
      app.expiredClass = "label-whitelist"
      app.expiredDuration = "vô thời hạn"
      app.expiredDate = "mãi mãi"
      this.changeRef.detectChanges()
      return
    } 

    let expiredMoment = moment(app.license.expireDate);
    app.expiredDate = expiredMoment.format('HH:mm dddd DD-MM-YYYY');
    let expiredMomentUTC = expiredMoment.utc()
    let nowUTC = moment.utc()
    let diffDuration = moment.duration(expiredMomentUTC.diff(nowUTC))
    let minDiff = diffDuration.asMinutes();
    let dayDiff = Math.round(diffDuration.asDays());
    let monthDiff = Math.round(diffDuration.asMonths());
    let yearDiff = Math.round(diffDuration.asYears());
    if (app.license.status === "locking") {
      app.isLocked = true
    } else {
      app.isLocked = false
    }
    if (minDiff > 0) {
      if (yearDiff >= 1) {
        app.expiredDuration = 'còn ' + yearDiff + ' năm'
      } else {
        if (monthDiff >= 1) {
          app.expiredDuration = 'còn ' + monthDiff + ' tháng'
        } else {
          if (dayDiff >= 1) {
            app.expiredDuration = 'còn ' + dayDiff + ' ngày'
          } else {
            app.expiredDuration = 'hết hạn hôm nay'
          }
        }
      }

      if (dayDiff > 15) {
        app.expiredClass = 'label-positive';
      } else if (dayDiff > 7) {
        app.expiredClass = 'label-neutral';
      } else {
        app.isExpiredSoon = true;
        app.expiredClass = 'label-danger';
      }

    } else {
      app.expiredDuration = 'đã hết hạn'
      app.isExpired = true;

      app.expiredClass = 'label-negative'
    }
    app.dayDiff = dayDiff
    this.changeRef.detectChanges()
  }

  async upgradeApp(id) {
    let app = this.apps.find(x => x._id == id)
    if (!await this.alert.question('Gia hạn app ' + app.name, 'App sẽ được sử dụng thêm 30 ngày kể từ hôm nay')) return
    try {
      this.upgrading[id] = true
      this.changeRef.detectChanges()
      let adminToken = this.chatbotApi.chatbotAuth.systemUser.token
      let result = await this.chatbotApi.systemUser.upgrade(app._id, adminToken)
      app.license = result
      this.calculateExpiration(app)
    } catch (err) {
      this.alert.handleError(err, 'Lỗi khi gia hạn app')
    } finally {
      this.upgrading[id] = false
      this.changeRef.detectChanges()
    }
  }

  async openInviteCustomer(app: iApp) {
    this.dialog.open(AppInviteCustomerDialog, {
      width: '500px',
      data: app
    })
  }

  async exportCSVCustomer(app: iApp) {    
    try {
      let data = await this.chatbotApi.app.exportExcelAllSubscribers(app._id)
      const blob = new Blob([data], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      });
      saveAs(blob, `DS Khách hàng ${app.name}.xlsx`);
    } catch (err) {
      console.error(err)
      this.alert.handleError(err, 'Lỗi xảy ra khi xuất dữ liệu')
    }
  }

  async transfer(app: iApp) {    
    let dialogRef = this.dialog.open(UserTransferOwnerDialog, {
      width: '550px',
      data: {
        owner: app.owner,
        users: app.users,
        app: app
      }
    })
    
    dialogRef.afterClosed().toPromise().then(result => {
      if (result) {
        this.reload(false)
      }
    })
  }

  async whitelistApp(id) {
    let app = this.apps.find(x => x._id == id)
    try {
      this.whitelisting[id] = true
      this.changeRef.detectChanges()
      let adminToken = this.chatbotApi.chatbotAuth.systemUser.token
      let result = await this.chatbotApi.systemUser.whiteList(id, adminToken)
      app.whiteListed = true
      this.calculateExpiration(app)
    } catch (err) {
      this.alert.handleError(err, 'Lỗi khi đưa app vào whitelist')
    } finally {
      this.whitelisting[id] = false
      this.changeRef.detectChanges()
    }
  }

  async unwhitelistApp(id) {
    let app = this.apps.find(x => x._id == id)
    try {
      this.whitelisting[id] = true
      this.changeRef.detectChanges()
      let adminToken = this.chatbotApi.chatbotAuth.systemUser.token
      let result = await this.chatbotApi.systemUser.unwhiteList(id, adminToken)
      app.whiteListed = false
      this.calculateExpiration(app)
    } catch (err) {
      this.alert.handleError(err, 'Lỗi khi đưa app vào whitelist')
    } finally {
      this.whitelisting[id] = false
      this.changeRef.detectChanges()
    }
  }

  formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
  }

  async exportApps() {
    let adminToken = this.chatbotApi.chatbotAuth.systemUser.token;
    let andFilter = { $and: [] }
    if (this.followApp) andFilter.$and.push({ _id: { $in: this.followList } })
    if (this.whiteListed) andFilter.$and.push({ whiteListed: this.whiteListed })
    if (this.searchText) andFilter.$and.push({
      $or: [
        { name: { $regex: this.searchText } },
        { 'activePage.name': { $regex: this.searchText } }
      ]
    })
    const downloadURL = await this.chatbotApi.systemUser.getExportAppsDownloadURL(adminToken, {
      populates: [{
        path: 'activePage',
        select: 'name meta'
      }, {
        path: 'license'
      }],
      filter: andFilter.$and.length?andFilter:undefined
    })
    window.open(downloadURL);
  }
}