import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppUserComponent } from './app-user.component';
import { UserInviteComponent } from './user-invite/user-invite.component';
import { SystemAdminGuard } from './user-guards/system-admin.guard';
import { SystemAgencyGuard } from './user-guards/system-agency.guard';

const routes: Routes = [{
    path: "",
    component: AppUserComponent,
    children: [{
        path: ""
    },{
    //     path: "cost",
    //     loadChildren: 'app/chatgut-dashboard/app-user/user-cost/user-cost.module#UserCostModule'
    // },{
    //     path: "history",
    //     loadChildren: 'app/chatgut-dashboard/app-user/user-history/user-history.module#UserHistoryModule'
    // },{
    //     path: "code",
    //     canActivate: [SystemAgencyGuard],
    //     loadChildren: 'app/chatgut-dashboard/app-user/user-code/user-code.module#UserCodeModule'
    // },{
    //     path: "agencies", 
    //     canActivate: [SystemAdminGuard],
    //     loadChildren: 'app/chatgut-dashboard/app-user/user-agency/user-agency.module#UserAgencyModule'
    // },{
        path: "app_list",
        canActivate: [SystemAdminGuard],
        loadChildren: 'app/chatgut-dashboard/app-user/user-app-list/user-app-list.module#UserAppListModule'
    },{
        path: "revenue",
        canActivate: [SystemAdminGuard],
        loadChildren: 'app/chatgut-dashboard/app-user/user-revenue/user-revenue.module#UserRevenueModule'
    },{
        path: "customers",
        canActivate: [SystemAdminGuard],
        loadChildren: 'app/chatgut-dashboard/app-user/user-customer/user-customer.module#UserCustomerModule'
    },{
        path: "acceptInviteLink",
        component: UserInviteComponent
    },{
        path: "**",
        redirectTo: "cost"
    }]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AppUserRouting { }


