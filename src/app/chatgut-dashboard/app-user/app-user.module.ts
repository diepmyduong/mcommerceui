import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TemplateFilterPipeModule } from 'app/shared/pipes/template-filter-pipe/template-filter-pipe.module';
import { MatTooltipModule, MatSnackBarModule, MatProgressSpinnerModule, MatTabsModule, MatDialogModule } from '@angular/material';
import { AppUserComponent } from './app-user.component';
import { AppUserRouting } from './app-user.routing';
import { UserInviteComponent } from './user-invite/user-invite.component';
import { SystemAdminGuard } from './user-guards/system-admin.guard';
import { SystemAgencyGuard } from './user-guards/system-agency.guard';

@NgModule({
  providers: [
    SystemAgencyGuard,
    SystemAdminGuard
  ],
  imports: [
    CommonModule,
    FormsModule,
    AppUserRouting,
    TemplateFilterPipeModule,
    MatTooltipModule,
    MatSnackBarModule, 
    MatProgressSpinnerModule,
    MatDialogModule,
    MatTabsModule
  ],
  declarations: [AppUserComponent, UserInviteComponent],
  entryComponents: [],
})
export class AppUserModule { }
