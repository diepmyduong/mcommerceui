import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { ChatbotApiService } from 'app/services/chatbot';
import { iSystemUser } from 'app/services/chatbot/api/crud/systemUser';

// const USER = [
//   { code: 'cost', display: 'Chi phí app', icon: 'fas fa-coins', rule: '' },
//   { code: 'history', display: 'Lịch sử thanh toán', icon: 'fas fa-history', rule: '' },
// ]

// const AGENCY = [
  // { code: 'code', display: 'Quản lý code', icon: 'fas fa-keyboard', rule: 'system_agency' },
  // { code: 'cost', display: 'Chi phí app', icon: 'fas fa-coins', rule: '' },
  // { code: 'history', display: 'Lịch sử thanh toán', icon: 'fas fa-history', rule: '' },
// ]

const ADMIN = [
  { code: 'app_list', display: 'Danh sách app', icon: 'fas fa-th-list', rule: 'system_admin' },
  { code: 'revenue', display: 'Doanh thu', icon: 'fas fa-funnel-dollar', rule: 'system_admin' },
  { code: 'customers', display: 'Khách hàng', icon: 'fas fa-user-friends', rule: 'system_admin' },
  // { code: 'agencies', display: 'Quản lý agency', icon: 'fas fa-briefcase', rule: 'system_admin' },
  // { code: 'code', display: 'Quản lý code', icon: 'fas fa-keyboard', rule: 'system_agency' },
  // { code: 'cost', display: 'Chi phí', icon: 'fas fa-coins', rule: '' },
  // { code: 'history', display: 'Lịch sử', icon: 'fas fa-history', rule: '' },
]

@Component({
  selector: 'app-user',
  templateUrl: './app-user.component.html',
  styleUrls: ['./app-user.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppUserComponent implements OnInit {
  constructor(
    public router: Router,
    public changeRef: ChangeDetectorRef,
    public chatbotApi: ChatbotApiService
  ) {
  }
  systemUserSubscription: Subscription
  routeSubscription: Subscription
  selectedIndex: number = 0
  tabs = []
  systemUser: iSystemUser

  async ngOnInit() {
    if (this.chatbotApi.chatbotAuth.hasCheckSystemUser.getValue()) {
      this.systemUser = this.chatbotApi.chatbotAuth.systemUser
      this.changeRef.detectChanges()
      this.initRouteCheck()
    } else {
      this.systemUserSubscription = this.chatbotApi.chatbotAuth.hasCheckSystemUser.subscribe(hasCheck => {
        if (hasCheck) {
          this.systemUser = this.chatbotApi.chatbotAuth.systemUser
          this.changeRef.detectChanges()
          this.initRouteCheck()
          this.systemUserSubscription.unsubscribe()
        }
      })
    }
  }

  initRouteCheck() {          
    this.checkRouteURL(this.router.url)
    this.routeSubscription = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.checkRouteURL(event.url)
      }
    })
    this.changeRef.detectChanges()
  }
  
  checkRouteURL(url) {
    if (!this.tabs.length) {
      if (this.systemUser) {
        switch (this.systemUser.rule) {
          case 'system_admin':
          this.tabs = ADMIN
          break
          // case 'system_agency':
          // this.tabs = AGENCY
          // break
        }
      } else {
        // this.tabs = USER
      }
    }
    if (url == '/user' || !this.tabs.length) {
      if (this.systemUser) {
        switch (this.systemUser.rule) {
          case 'system_admin':
          this.router.navigate(['/user/app_list'])
          break
          case 'system_agency':
          this.router.navigate(['/user/code'])
          break
        }
      } else {
        this.router.navigate(['/user/cost'])
      }
      return
    }
    let index = this.tabs.findIndex(x => url.indexOf(x.code)>-1)
    if (index) {
      this.selectedIndex = index
    } else {
      this.selectedIndex = 0
    }
    this.changeRef.detectChanges()
  }

  async ngOnDestroy() {
    if (this.routeSubscription) this.routeSubscription.unsubscribe()
    if (this.systemUserSubscription) this.systemUserSubscription.unsubscribe()
  }

  async selectTab($event) {
    this.selectedIndex = $event
    this.router.navigate(['/user', this.tabs[this.selectedIndex].code])
    this.changeRef.detectChanges()
  }
}
