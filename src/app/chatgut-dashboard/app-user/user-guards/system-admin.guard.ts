import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { Router } from '@angular/router'
@Injectable()
export class SystemAdminGuard implements CanActivate {
  constructor(
    public chatbotApi: ChatbotApiService,
    public router: Router
  ) {
  }

  canActivate(): Promise<boolean> {
    return this.chatbotApi.chatbotAuth.authenticated.then(async state => {
      if (state && this.chatbotApi.chatbotAuth.hasCheckSystemUser.getValue()) {
        if (this.chatbotApi.chatbotAuth.systemUser && this.chatbotApi.chatbotAuth.systemUser.rule === 'system_admin') {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    })
  }
}
