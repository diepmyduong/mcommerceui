import { animate, state, style, transition, trigger } from "@angular/animations";
import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from "@angular/core";
import { MatSnackBar } from "@angular/material";
import { AlertService } from "app/services/alert.service";
import { ChatbotApiService, iApp } from "app/services/chatbot";
import { iCustomer } from "app/services/chatbot/api/crud/customer";
import { BehaviorSubject, merge, Subject } from "rxjs";
import { distinctUntilChanged, takeUntil } from "rxjs/operators";
import * as moment from 'moment'

@Component({
  selector: 'user-customer',
  templateUrl: './user-customer.component.html',
  styleUrls: ['./user-customer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', opacity: 0, padding: 0})),
      state('expanded', style({height: '*', opacity: 1, padding: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class UserCustomerComponent {

  apps: iApp[]
  loadingApps: boolean = false
  loading: boolean = false
  loadDone: boolean = false
  loadingMore: boolean = false
  customers: iCustomer[]
  displayedColumns: string[] = ['index', 'fullName', 'email', 'phone', 'qtyApp', 'createdAt', 'status', 'action']
  expandedElement: any

  searchText: string
  limit = 10
  total

  fromDate$ = new BehaviorSubject<any>(null)
  toDate$ = new BehaviorSubject<any>(null)

  orderBy: string
  orderByList = [
    { code: 'createdAtDesc', display: 'mới đến cũ' },
    { code: 'createdAtAsc', display: 'cũ đến mới' },
    { code: 'appDesc', display: 'số app giảm dần' },
    { code: 'appAsc', display: 'số app tăng dần' },
  ]

  destroy$ = new Subject()

  constructor(
    private chatbotApi: ChatbotApiService,
    private changeRef: ChangeDetectorRef,
    private alert: AlertService,
    private snackBar: MatSnackBar
  ) {
    this.orderBy = this.orderByList[0].code
  }

  async ngOnInit() {
    this.chatbotApi.setTitle('Khách hàng')
    this.initDateSubscription()
    await this.reload()
  }

  async ngOnDestroy() {
    this.destroy$.next()
  }

  initDateSubscription() {
    merge(this.fromDate$, this.toDate$).pipe(
      takeUntil(this.destroy$),
      distinctUntilChanged(),
    ).subscribe(res => {
      this.reload()
    })
  }

  async reload(local = true) {
    let order
    switch (this.orderBy) {
      case 'createdAtDesc': order = { createdAt: -1 }; break;
      case 'createdAtAsc': order = { createdAt: 1 }; break;
      case 'appDesc': order = { qtyApp: -1 }; break;
      case 'appAsc': order = { qtyApp: 1 }; break;
    }
    let createdAt = {}
    let fromDate = this.fromDate$.getValue()
    let toDate = this.toDate$.getValue()
    if (fromDate) createdAt = { $gte: moment(fromDate).startOf('day') }
    if (toDate) createdAt = { ...createdAt, $lte: moment(toDate).endOf('day') }
    if (!fromDate && !toDate) createdAt = undefined
    try {
      this.loading = true
      this.changeRef.detectChanges()
      let adminToken = this.chatbotApi.chatbotAuth.systemUser.token
      let tasks = []
      
      tasks.push(this.chatbotApi.customer.getList({
        headers: {
          'x-api-key': adminToken
        },
        query: {
          order,
          limit: this.limit,
          filter: {
            ...this.searchText?{
              fullName: { $regex: this.searchText }
            }:{},
            createdAt
          },
        }
      }).then(result => { 
        this.total = this.chatbotApi.customer.pagination.totalItems
        this.customers = result
      }))

      await Promise.all(tasks)

    } catch (error) {
      this.alert.handleError(error, 'Lỗi khi lấy danh sách khách hàng');
    } finally {
      this.loading = false
      this.loadDone = true
      this.changeRef.detectChanges()
    }
  }

  async loadMore(local = true) {
    if (this.loadingMore) return

    this.loadingMore = true
    this.changeRef.detectChanges()
    try {
      let adminToken = this.chatbotApi.chatbotAuth.systemUser.token
      let tasks = []

      tasks.push(this.chatbotApi.customer.getList({
        headers: {
          'x-api-key': adminToken
        },
        query: {
          limit: this.limit,
          offset: this.customers.length
        }
      }).then(result => { 
        this.customers = [...this.customers, ...result]
      }))

      await Promise.all(tasks)
    } catch (error) {
      this.alert.handleError(error, 'Lỗi khi tải thêm khách hàng');
    } finally {
      this.loadingMore = false
      this.changeRef.detectChanges()
    }
  }
  
  async setStatus(status, customer: iCustomer) {
    let fallback = customer.status
    customer.status = status
    this.changeRef.markForCheck()
    try {
      let adminToken = this.chatbotApi.chatbotAuth.systemUser.token
      await this.chatbotApi.customer.update(customer._id, { status }, { headers: {
        'x-api-key': adminToken
      } })
    } catch (err) {
      this.alert.handleError(err, 'Lỗi khi chỉnh trạng thái');
      customer.status = fallback
    } finally {
      this.changeRef.detectChanges()
    }
  }

  async setExpandedElement(el) {
    let flag = this.expandedElement?true:false
    this.expandedElement = this.expandedElement === el ? null : el
    if (!this.expandedElement) {
      this.loadingApps = false
      this.apps = null
      this.changeRef.detectChanges()
      return
    }
    this.loadingApps = true
    this.changeRef.detectChanges()
    if (flag) await new Promise(r => setTimeout(r, 300))

    try {
      await this.getApps(el)
    } catch (err) {

    } finally {
      this.loadingApps = false
      this.changeRef.detectChanges()
    }
  }

  async getApps(customer: iCustomer, local = true) {
    
    let token = this.chatbotApi.chatbotAuth.systemUser.token
    let tasks = []
    console.log(customer)
    tasks.push(this.chatbotApi.systemUser.getApps({
      local,
      headers: {
        'x-api-key': token
      },
      query: {
        limit: 0,
        populates: [{
          path: 'activePage',
          select: 'name meta'
        }, {
          path: 'license'
        }],
        filter: { owner: customer.uid }
      }
    })
    .then(result => {
      let rows = result.rows
      for (let row of rows) {
        this.calculateExpiration(row)
      }
      this.apps = rows
      console.log('apps', this.apps)
    }))

    await Promise.all(tasks)
  }

  

  calculateExpiration(app) {

    if (app.whiteListed) {
      app.expiredClass = "label-whitelist"
      app.expiredDuration = "vô thời hạn"
      app.expiredDate = "mãi mãi"
      this.changeRef.detectChanges()
      return
    } 

    let expiredMoment = moment(app.license.expireDate);
    app.expiredDate = expiredMoment.format('HH:mm dddd DD-MM-YYYY');
    let expiredMomentUTC = expiredMoment.utc()
    let nowUTC = moment.utc()
    let diffDuration = moment.duration(expiredMomentUTC.diff(nowUTC))
    let minDiff = diffDuration.asMinutes();
    let dayDiff = Math.round(diffDuration.asDays());
    let monthDiff = Math.round(diffDuration.asMonths());
    let yearDiff = Math.round(diffDuration.asYears());
    if (app.license.status === "locking") {
      app.isLocked = true
    } else {
      app.isLocked = false
    }
    if (minDiff > 0) {
      if (yearDiff >= 1) {
        app.expiredDuration = 'còn ' + yearDiff + ' năm'
      } else {
        if (monthDiff >= 1) {
          app.expiredDuration = 'còn ' + monthDiff + ' tháng'
        } else {
          if (dayDiff >= 1) {
            app.expiredDuration = 'còn ' + dayDiff + ' ngày'
          } else {
            app.expiredDuration = 'hết hạn hôm nay'
          }
        }
      }

      if (dayDiff > 15) {
        app.expiredClass = 'label-positive';
      } else if (dayDiff > 7) {
        app.expiredClass = 'label-neutral';
      } else {
        app.isExpiredSoon = true;
        app.expiredClass = 'label-danger';
      }

    } else {
      app.expiredDuration = 'đã hết hạn'
      app.isExpired = true;

      app.expiredClass = 'label-negative'
    }
    app.dayDiff = dayDiff
    this.changeRef.detectChanges()
  }
}