import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChatbotApiService } from 'app/services/chatbot';
import { AlertService } from 'app/services/alert.service';

@Component({
  selector: 'app-user-invite',
  templateUrl: './user-invite.component.html',
  styleUrls: ['./user-invite.component.scss']
})
export class UserInviteComponent {

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public chatbotApi: ChatbotApiService,
    public alert: AlertService
  ) { 
  }
  inviteToken: string

  async ngOnInit() {
    this.inviteToken = this.route.snapshot.queryParams["payload"]
    try {
      let firebaseToken = await this.chatbotApi.chatbotAuth.firebaseToken
      this.chatbotApi.chatbotAuth.systemUser = await this.chatbotApi.systemUser.acceptAgencyInvitation(firebaseToken, this.inviteToken)
      this.chatbotApi.chatbotAuth.hasCheckSystemUser.next(true)
      this.router.navigate([""])
    } catch(err) {
      this.alert.handleError(err, 'Lỗi khi nhận lời mời agency');
      // this.alert.error("Không thể mẫu cho người dùng", err.message)
      this.router.navigate(["/apps"])
    }
  }
}
