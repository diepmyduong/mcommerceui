import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { ChatbotApiService, iTemplate, iTemplateCategory } from "app/services/chatbot";
import { AlertService } from "app/services/alert.service";
import { MatSnackBar, MatDialog } from "@angular/material";
import { iSystemUser } from "app/services/chatbot/api/crud/systemUser";
import { iLicenseCode } from "app/services/chatbot/api/crud/licenseCode";
import { trigger, state, style, transition, animate } from "@angular/animations";

@Component({
  selector: 'user-agency',
  templateUrl: './user-agency.component.html',
  styleUrls: ['./user-agency.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', opacity: 0, padding: 0})),
      state('expanded', style({height: '*', opacity: 1, padding: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class UserAgencyComponent {

  loadingCodes: boolean = false
  loading: boolean = false
  loadDone: boolean = false
  loadingMore: boolean = false
  agencies: iSystemUser[]
  inviteLink: string
  inviting: boolean = false
  displayedColumns: string[] = ['index', 'name', 'email', 'status', 'action']
  expandedElement: any
  codes: iLicenseCode[]

  searchText: string 
  limit = 10
  total

  constructor(
    private chatbotApi: ChatbotApiService,
    private changeRef: ChangeDetectorRef,
    private alert: AlertService,
    private snackBar: MatSnackBar
  ) {
  }

  async ngOnInit() {
    this.chatbotApi.setTitle('Agency')

    await this.reload()
  }

  async reload(local = true) {
    try {
      this.loading = true
      this.changeRef.detectChanges()
      let adminToken = this.chatbotApi.chatbotAuth.systemUser.token
      let tasks = []
      
      tasks.push(this.chatbotApi.systemUser.getList({
        headers: {
          'x-api-key': adminToken
        },
        query: {
          limit: this.limit,
          filter: {
            'rule': 'system_agency',
            ...this.searchText?{
              name: { $regex: this.searchText }
            }:{}
          },
        }
      }).then(result => { 
        this.total = this.chatbotApi.systemUser.pagination.totalItems
        this.agencies = result
      }))

      await Promise.all(tasks)
      console.log(this.agencies)

    } catch (error) {
      this.alert.handleError(error, 'Lỗi khi lấy danh sách agency');
    } finally {
      this.loading = false
      this.loadDone = true
      this.changeRef.detectChanges()
    }
  }

  async loadMore(local = true) {
    if (this.loadingMore) return

    this.loadingMore = true
    this.changeRef.detectChanges()
    try {
      let adminToken = this.chatbotApi.chatbotAuth.systemUser.token
      let tasks = []

      tasks.push(this.chatbotApi.systemUser.getList({
        headers: {
          'x-api-key': adminToken
        },
        query: {
          limit: this.limit,
          offset: this.agencies.length,
          filter: {
            'rule': 'system_agency'
          },
        }
      }).then(result => {
        this.agencies = [...this.agencies, ...result]
      }))

      await Promise.all(tasks)
    } catch (error) {
      this.alert.handleError(error, 'Lỗi khi tải thêm agency');
    } finally {
      this.loadingMore = false
      this.changeRef.detectChanges()
    }
  }
  
  async inviteAgency() {
    try {
      this.inviting = true
      let result = await this.chatbotApi.systemUser.getInviteAgencyToken(this.chatbotApi.chatbotAuth.systemUser.token)
      console.log(result)
      this.inviteLink = location.origin + '/user/acceptInviteLink?payload=' + result.inviteToken
    } catch (err) {
      this.alert.handleError(err, 'Lỗi khi lấy link mời agency');
    } finally {
      this.inviting = false
      this.changeRef.detectChanges()
    }
  }

  copyLink(){
    let input = document.getElementById('invite-link') as HTMLInputElement
    input.value = this.inviteLink
    input.select();
    document.execCommand('copy',false);
    this.snackBar.open('✔️ Đã copy vào clipboard', '', {
      duration: 2000,
    });
    this.changeRef.detectChanges()
  }

  async setStatus(status, agency: iSystemUser) {
    let fallback = agency.status
    agency.status = status
    this.changeRef.detectChanges()
    try {
      let adminToken = this.chatbotApi.chatbotAuth.systemUser.token
      await this.chatbotApi.systemUser.update(agency._id, { status }, { headers: {
        'x-api-key': adminToken
      } })
    } catch (err) {
      this.alert.handleError(err, 'Lỗi khi chỉnh trạng thái');
      agency.status = fallback
    } finally {
      this.changeRef.detectChanges()
    }
  }

  async setExpandedElement(el) {
    let flag = this.expandedElement?true:false
    this.expandedElement = this.expandedElement === el ? null : el
    if (!this.expandedElement) {
      this.loadingCodes = false
      this.codes = null
      this.changeRef.detectChanges()
      return
    }
    this.loadingCodes = true
    this.changeRef.detectChanges()
    if (flag) await new Promise(r => setTimeout(r, 300))

    try {
      await this.getCodes(el)
    } catch (err) {

    } finally {
      this.loadingCodes = false
      this.changeRef.detectChanges()
    }
  }

  async getCodes(agency: iSystemUser, local = true) {
    
    let token = this.chatbotApi.chatbotAuth.systemUser.token
    let tasks = []
    
    tasks.push(this.chatbotApi.licenseCode.getList({
      local, 
      query: {
        limit: 0,
        order: { createdAt: -1 }, 
        filter: { owner: agency._id }
      },
      headers: { 'x-api-key': token } })
    .then(result => {
      this.codes = result
      console.log('codes', this.codes)
    }))

    await Promise.all(tasks)
  }

}