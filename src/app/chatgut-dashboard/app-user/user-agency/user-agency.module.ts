import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UserAgencyComponent } from './user-agency.component';
import { RouterModule } from '@angular/router';
import { MatProgressSpinnerModule, MatTooltipModule, MatTableModule } from '@angular/material';

@NgModule({
  providers: [],
  imports: [
    CommonModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatTableModule,
    RouterModule.forChild([{ path: '', component: UserAgencyComponent}])
  ],
  declarations: [UserAgencyComponent],
  entryComponents: []
})
export class UserAgencyModule { }
