import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatProgressSpinnerModule, MatTooltipModule } from '@angular/material';
import { AccountingPipeModule } from 'app/shared/pipes/accounting-pipe/accounting-pipe.module';
import { UserHistoryComponent } from './user-history.component';

@NgModule({
  providers: [],
  imports: [
    CommonModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    RouterModule.forChild([{ path: '', component: UserHistoryComponent}]),
    AccountingPipeModule
  ],
  declarations: [UserHistoryComponent]
})
export class UserHistoryModule { }
