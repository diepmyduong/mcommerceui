import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChatbotApiService } from 'app/services/chatbot';
import { AlertService } from 'app/services/alert.service';
import { iBill } from 'app/services/chatbot/api/crud/bill';
import * as moment from 'moment'
import { Subscription } from 'rxjs';
import { environment } from '@environments';

@Component({
  selector: 'app-user-history',
  templateUrl: './user-history.component.html',
  styleUrls: ['./user-history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserHistoryComponent {

  selectedBill: iBill
  bills: iBill[]
  packages: any
  loadDone: boolean = false
  loadingMore: boolean = false

  total: number
  limit = 10
  page = 1

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public chatbotApi: ChatbotApiService,
    public alert: AlertService,
    public changeRef: ChangeDetectorRef
  ) { 
  }


  async ngOnInit() {
    this.chatbotApi.setTitle('Paid history')
    this.checkQueryParams()
    await this.reload()
  }

  async checkQueryParams() {
    let vnpayObj = this.route.snapshot.queryParams
    if (vnpayObj.vnp_ResponseCode) {
      if (vnpayObj.vnp_ResponseCode == '00') {
        this.alert.success('Thanh toán thành công', 'Cám ơn bạn đã thanh toán chi phí app')
        if (!environment.production) {
          this.chatbotApi.bill.vnpaySandboxCallback(vnpayObj)
        }
      } else {
        this.alert.error('Thanh toán không thành công', 'Xảy ra sự cố khi thanh toán')
      }
    }
  }

  async reload(local = true) {
    try {
      let firebaseToken = await this.chatbotApi.chatbotAuth.firebaseToken
      let tasks = []
      
      tasks.push(this.chatbotApi.dailyCost.getPackages().then(result => { this.packages = result }))
      tasks.push(this.chatbotApi.bill.getList({
        local, 
        query: {
          limit: this.limit,
          order: { paidAt: -1 }, 
          populates: [{
            path: "app",
            select: "name activePage",
            populate: [{
              path: "activePage",
              select: "name meta"
            }]
          }],
          filter: { status: 'paid' }
        },
        headers: {
          'access_token': firebaseToken
        }
      })
      .then(result => {
        this.bills = result 
        console.log(this.bills)
        this.total = this.chatbotApi.bill.pagination.totalItems
      }))

      await Promise.all(tasks)
      this.selectedBill = this.bills[0]

      for (let bill of this.bills) {
        let totalDays = bill.detail.map(x => x.qty).reduce((prev, current) => prev + current, 0)
        for (let cost of bill.detail) {
          cost.desc = this.packages[cost.package].desc
        }
        bill.totalDays = totalDays
      }

      console.log(this.bills)
    } catch (error) {
      this.alert.handleError(error, 'Lỗi khi lấy lịch sử thanh toán');
    } finally {
      this.loadDone = true
      this.changeRef.detectChanges()
    }
  }

  async loadMore(local = true) {
    if (this.loadingMore) return

    this.loadingMore = true
    this.page++
    this.changeRef.detectChanges()
    try {
      let firebaseToken = await this.chatbotApi.chatbotAuth.firebaseToken
      let tasks = []
      
      tasks.push(this.chatbotApi.bill.getList({
        local, 
        query: {
          limit: this.limit,
          page: this.page,
          order: { paidAt: -1 }, 
          populates: [{
            path: "app",
            select: "name activePage",
            populate: [{
              path: "activePage",
              select: "name meta"
            }]
          }],
          filter: { status: 'paid' }
        },
        headers: {
          'access_token': firebaseToken
        }
      })
      .then(result => {
        for (let bill of result) {
          let totalDays = bill.detail.map(x => x.qty).reduce((prev, current) => prev + current, 0)
          for (let cost of bill.detail) {
            cost.desc = this.packages[cost.package].desc
          }
          bill.totalDays = totalDays
        }

        this.bills = [...this.bills, ...result]
      }))
      await Promise.all(tasks)
    } catch (error) {
      this.alert.handleError(error, 'Lỗi khi tải thêm');
    } finally {
      this.loadingMore = false
      this.changeRef.detectChanges()
    }
  }

  selectBill(bill) {
    this.selectedBill = null
    this.changeRef.detectChanges()
    this.selectedBill = bill
    this.changeRef.detectChanges()
  }

  calculateExtra(app) {
    let totalDays = 0
    app.costs = app.costs.sort((a, b) => a.package<b.package?-1:1)
    app.costs.forEach(cost => {
      totalDays += cost.qty
    })
    app.totalDays = totalDays
  }

}
