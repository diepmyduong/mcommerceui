import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChatbotApiService } from 'app/services/chatbot';
import { AlertService } from 'app/services/alert.service';
import * as moment from 'moment'

@Component({
  selector: 'app-user-revenue',
  templateUrl: './user-revenue.component.html',
  styleUrls: ['./user-revenue.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserRevenueComponent {

  loading: boolean = false
  loadDone: boolean = false
  currentMonth: number
  currentYear: number
  monthList: any[]
  yearList: number[]
  dates: any[]
  stats: any

  total: number
  displayedColumns: string[] = ['index', 'date', 'amountWithoutSaleOff', 'amountOfSaleOff', 'amount'];

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public chatbotApi: ChatbotApiService,
    public alert: AlertService,
    public changeRef: ChangeDetectorRef
  ) { 
  }


  async ngOnInit() {
    this.chatbotApi.setTitle('App revenue')
    // this.checkQueryParams()
    await this.initDate()
    await this.reload()
  }

  async initDate() {
    let today = moment()
    //get current
    this.currentMonth = today.get('month')
    this.currentYear = today.get('year')

    this.monthList = []
    this.yearList = []
    for (let i = 0; i < 10; i++) {
      let year = this.currentYear - i
      this.yearList.push(year)
    }

    for (let month = 0; month < 12; month++) {
      this.monthList.push(month)
    }

    this.changeRef.detectChanges()
  }

  async reload(local = true) {
    try {
      this.loading = true
      this.dates = []
      this.stats = null
      this.changeRef.detectChanges()
      let adminToken = this.chatbotApi.chatbotAuth.systemUser.token
      let tasks = []
      
      let day = moment(`1/${this.currentMonth + 1}/${this.currentYear}`, 'DD/MM/YYYY')
      let from = day.startOf('month').toISOString()
      let to = day.endOf('month').toISOString()
      console.log(day, from, to)
      tasks.push(this.chatbotApi.bill.getStats(adminToken, { local, query: { from, to } })
      .then(result => {
        if (result && result.length) {
          this.stats = result[0]
          if (this.stats.dates) {
            this.dates = this.stats.dates
          } else {
            this.dates = []
          }
        } else {
          this.stats = null
          this.dates = []
        }
        console.log('here', result)
      }))

      await Promise.all(tasks)
      // this.selectedBill = this.bills[0]

      // for (let bill of this.bills) {
      //   let totalDays = bill.detail.map(x => x.qty).reduce((prev, current) => prev + current, 0)
      //   for (let cost of bill.detail) {
      //     cost.desc = this.packages[cost.package].desc
      //   }
      //   bill.totalDays = totalDays
      // }

    } catch (error) {
      this.alert.handleError(error, 'Lỗi khi lấy doanh thu');
    } finally {
      this.loading = false
      this.loadDone = true
      this.changeRef.detectChanges()
    }
  }

  // async sortChanged(event) {
  //   console.log('sort changed', event)
  // }

}
