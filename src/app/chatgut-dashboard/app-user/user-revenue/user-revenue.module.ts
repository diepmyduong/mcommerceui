import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatProgressSpinnerModule, MatTooltipModule, MatSelectModule, MatTab, MatTableModule } from '@angular/material';
import { AccountingPipeModule } from 'app/shared/pipes/accounting-pipe/accounting-pipe.module';
import { UserRevenueComponent } from './user-revenue.component';

@NgModule({
  providers: [],
  imports: [
    CommonModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatSelectModule,
    MatTableModule,
    RouterModule.forChild([{ path: '', component: UserRevenueComponent}]),
    AccountingPipeModule
  ],
  declarations: [UserRevenueComponent]
})
export class UserRevenueModule { }
