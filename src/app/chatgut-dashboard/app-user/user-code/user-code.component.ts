import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChatbotApiService, iApp } from 'app/services/chatbot';
import { AlertService } from 'app/services/alert.service';
import { iLicenseCode } from 'app/services/chatbot/api/crud/licenseCode';
import { MatDialog } from '@angular/material';
import { CodeDialog } from './code-dialog/code-dialog.component';
import { trigger, state, style, transition, animate } from "@angular/animations";

@Component({
  selector: 'app-user-code',
  templateUrl: './user-code.component.html',
  styleUrls: ['./user-code.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', opacity: 0, padding: 0})),
      state('expanded', style({height: '*', opacity: 1, padding: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class UserCodeComponent {

  codes: iLicenseCode[]
  loadingApps: boolean = false
  loadDone: boolean = false
  loading: boolean = false
  loadingMore: boolean = false
  expandedElement: any
  apps: iApp[]

  displayedColumns: string[] = []
  searchText: string
  page = 1
  limit = 10
  currentLimit
  total

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public chatbotApi: ChatbotApiService,
    public alert: AlertService,
    public changeRef: ChangeDetectorRef,
    public dialog: MatDialog
  ) { 
    this.currentLimit = this.limit
  }


  async ngOnInit() {
    this.chatbotApi.setTitle('Code')
    if (this.chatbotApi.chatbotAuth.systemUser.rule == 'system_admin') {
      this.displayedColumns = ['index', 'code', 'saleOf', 'owner', 'status', 'action']
    } else {
      this.displayedColumns = ['index', 'code', 'saleOf', 'status', 'action']
    }
    await this.reload()
  }

  async reload(local = true) {
    this.loading = true
    this.changeRef.detectChanges()
    try {
      let token = this.chatbotApi.chatbotAuth.systemUser.token
      let tasks = []
      
      tasks.push(this.chatbotApi.licenseCode.getList({
        local, 
        query: {
          limit: this.searchText?0:this.currentLimit,
          order: { createdAt: -1 }, 
          populates: ['owner'],
          filter: this.searchText?{
            $or: [
              { code: { $regex: this.searchText } },
              { 'owner.name': { $regex: this.searchText } }
            ]
            // code: { $regex: this.searchText }
          }:undefined
        },
        headers: { 'x-api-key': token } })
      .then(result => {
        this.codes = result
        if (!this.searchText) this.currentLimit = this.codes.length
      }))

      await Promise.all(tasks)
      this.total = this.chatbotApi.licenseCode.pagination.totalItems

      console.log(this.codes)
    } catch (error) {
      this.alert.handleError(error, 'Lỗi khi lấy mã code');
    } finally {
      this.loadDone = true
      this.loading = false
      this.changeRef.detectChanges()
    }
  }

  async loadMore(local = true) {
    if (this.loadingMore) return

    this.loadingMore = true
    this.page++
    this.changeRef.detectChanges()
    try {
      let token = this.chatbotApi.chatbotAuth.systemUser.token
      let tasks = []
      
      tasks.push(this.chatbotApi.licenseCode.getList({
        local, 
        query: {
          limit: this.limit,
          page: this.page,
          order: { createdAt: -1 }, 
          populates: ['owner']
        },
        headers: { 'x-api-key': token } })
      .then(result => {
        this.codes = [...this.codes, ...result]
        if (!this.searchText) this.currentLimit = this.codes.length
      }))
      await Promise.all(tasks)
    } catch (error) {
      this.alert.handleError(error, 'Lỗi khi lấy mã code');
    } finally {
      this.loadingMore = false
      this.changeRef.detectChanges()
    }
  }

  createCode() {
    let dialogRef = this.dialog.open(CodeDialog, {
      width: '450px',
      data: { refreshFunction: () => this.reload(false) }
    })
  }

  async setStatus(status, code: iLicenseCode) {
    let fallback = code.status
    code.status = status
    this.changeRef.detectChanges()
    try {
      let adminToken = this.chatbotApi.chatbotAuth.systemUser.token
      await this.chatbotApi.licenseCode.update(code._id, { status }, { headers: {
        'x-api-key': adminToken
      } })
    } catch (err) {
      this.alert.handleError(err, 'Lỗi khi chỉnh trạng thái');
      code.status = fallback
    } finally {
      this.changeRef.detectChanges()
    }
  }

  async setExpandedElement(el) {
    let flag = this.expandedElement?true:false
    this.expandedElement = this.expandedElement === el ? null : el
    if (!this.expandedElement) {
      this.loadingApps = false
      this.apps = null
      this.changeRef.detectChanges()
      return
    }
    this.loadingApps = true
    this.changeRef.detectChanges()
    if (flag) await new Promise(r => setTimeout(r, 300))

    try {
      await this.getApps(el)
    } catch (err) {

    } finally {
      this.loadingApps = false
      this.changeRef.detectChanges()
    }
  }

  async getApps(code: iLicenseCode, local = true) {
    
    let token = this.chatbotApi.chatbotAuth.systemUser.token
    let tasks = []
    
    tasks.push(this.chatbotApi.systemUser.getLicenseApps(token, {
      local, 
      query: {
        licenseCode: code.code
      },
      headers: { 'x-api-key': token } })
    .then(result => {
      this.apps = result.rows
      for (let app of this.apps) {
        app.ownerObj = app.users.find(x => x.uid == app.owner)
      }
      console.log('apps', this.apps)
    }))

    await Promise.all(tasks)
  }

}
