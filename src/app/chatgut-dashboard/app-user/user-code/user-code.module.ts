import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatProgressSpinnerModule, MatTooltipModule, MatTableModule, MatDialogModule, MatSelectModule } from '@angular/material';
import { AccountingPipeModule } from 'app/shared/pipes/accounting-pipe/accounting-pipe.module';
import { UserCodeComponent } from './user-code.component';
import { CodeDialog } from './code-dialog/code-dialog.component';

@NgModule({
  providers: [],
  imports: [
    CommonModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatSelectModule,
    MatTableModule,
    MatDialogModule,
    RouterModule.forChild([{ path: '', component: UserCodeComponent}]),
    AccountingPipeModule
  ],
  entryComponents: [CodeDialog],
  declarations: [UserCodeComponent, CodeDialog]
})
export class UserCodeModule { }
