import { Component, Inject, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from "@angular/material";
import { iTemplate, ChatbotApiService } from "app/services/chatbot";
import { AlertService } from "app/services/alert.service";

@Component({
  selector: 'app-code-dialog',
  templateUrl: './code-dialog.component.html',
  styleUrls: ['./code-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CodeDialog {

  code: string
  saleoff: number
  codeError: string
  saleOffArray = [0, 5, 10, 15, 20, 25, 30]
  refreshFunction

  creating: boolean = false
  success: boolean = false
  
  constructor(
    public dialogRef: MatDialogRef<CodeDialog>,
    @Inject(MAT_DIALOG_DATA) public data: iTemplate,
    private chatbotApi: ChatbotApiService,
    private changeRef: ChangeDetectorRef,
    private snackBar: MatSnackBar,
    private alert: AlertService) {
  }

  async ngOnInit() {
    this.saleoff = this.saleOffArray[4]
    this.refreshFunction = this.data.refreshFunction
  }

  async ngAfterViewInit() {
    document.getElementById('code-input').focus()
  }

  async inputChanged() {
    if (this.codeError) {
      this.checkCodeValid()
    }
  }

  async checkCodeValid() {
    if (!this.code) {
      this.codeError = 'Bắt buộc'
      return false
    }
    
    if (this.code.length < 6 || this.code.length > 32) {
      this.codeError = 'Từ 6 đến 32 kí tự'
      return false
    }

    if (!/^[^<>%$@!.,;'"/|=+\-\\_#()*&`~?: ]*$/g.test(this.code)) {
      this.codeError = 'Không dấu cách hay kí tự đặc biệt'
      return false
    }

    this.codeError = ''
    return true
  }

  async createCode() {
    if (!await this.checkCodeValid()) return
    if (this.codeError) return
    try {
      this.creating = true
      let token = this.chatbotApi.chatbotAuth.systemUser.token
      let result = await this.chatbotApi.licenseCode.create(token, this.code, this.saleoff)
      this.success = true
      this.refreshFunction()
    } catch (err) {
      this.snackBar.open('❌ Lỗi xảy ra khi tạo code', '', {
        duration: 2000,
      });
      if (err.error.type == "database_exception_query_fail") {
        this.codeError = 'Code này đã có người sở hữu'
      }
      console.log(err)
    } finally {
      this.creating = false
      this.changeRef.detectChanges()
    }
  }

  // async generateLink() {
  //   try {
  //     this.generatingLink = true
  //     this.link = (await this.chatbotApi.template.getShareLink(this.template._id)).inviteLink
  //   } catch (err) {
  //     this.snackBar.open('❌ Lỗi xảy ra khi tạo link', '', {
  //       duration: 2000,
  //     });
  //   } finally {
  //     this.generatingLink = false
  //     this.changeRef.detectChanges()
  //   }
  // }

  // async shareViaEmail() {

  //   let form = document.getElementById('email-form') as HTMLFormElement
  //   if (!form.checkValidity()) return

  //   try {
  //     this.sharingEmail = true
  //     await this.chatbotApi.template.share({
  //       templateId: this.template._id,
  //       email: this.email
  //     })
  //     this.snackBar.open('✔️ Đã chia sẻ mẫu cho email', '', {
  //       duration: 2000,
  //     });
  //     setTimeout(async () => {
  //       let firebaseToken = await this.chatbotApi.chatbotAuth.firebaseToken
  //       let template = await this.chatbotApi.template.getItem(this.template._id, {
  //         headers: {
  //           'access_token': firebaseToken
  //       }})
  //       console.log(template)
  //       this.email = ''
  //       this.template.shareableUser = template.shareableUser
  //       this.chatbotApi.template.setItem(template, {
  //         headers: {
  //           'access_token': firebaseToken
  //       }})
  //       this.changeRef.detectChanges()
  //     }, 300)

  //   } catch (err) {
  //     this.snackBar.open('❌ Lỗi khi chia sẻ. Xin kiểm tra lại email.', '', {
  //       duration: 2000,
  //     });
  //   } finally {
  //     this.sharingEmail = false
  //     this.changeRef.detectChanges()
  //   }
  // }

  // async removeShare(user) {
  //   try {
  //     await this.chatbotApi.template.unshare({
  //       templateId: this.template._id,
  //       uid: user.uid
  //     })
  //     this.snackBar.open('✔️ Hủy chia sẻ thành công', '', {
  //       duration: 2000,
  //     });
  //     this.template.shareableUser.splice(this.template.shareableUser.findIndex(x => x.uid == user.uid), 1)
  //     this.changeRef.detectChanges()
  //   } catch (err) {
  //     this.snackBar.open('❌ Lỗi xảy ra khi hủy chia sẻ', '', {
  //       duration: 2000,
  //     });
  //   }
  // }

  // copyId(){
  //   let input = document.getElementById('link-url') as HTMLInputElement
  //   input.value = this.link
  //   input.select();
  //   document.execCommand('copy',false);
  //   this.snackBar.open('✔️ Đã copy vào clipboard', '', {
  //     duration: 2000,
  //   });
  //   this.changeRef.detectChanges()
  // }
}