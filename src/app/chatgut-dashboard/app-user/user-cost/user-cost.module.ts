import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatProgressSpinnerModule, MatTooltipModule, MatSelectModule } from '@angular/material';
import { UserCostComponent } from './user-cost.component';
import { AccountingPipeModule } from 'app/shared/pipes/accounting-pipe/accounting-pipe.module';
import { AppFilterPipeModule } from 'app/shared/pipes/app-filter-pipe/app-filter-pipe.module';

@NgModule({
  providers: [],
  imports: [
    CommonModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatSelectModule,
    RouterModule.forChild([{ path: '', component: UserCostComponent}]),
    AccountingPipeModule,
    AppFilterPipeModule
  ],
  declarations: [UserCostComponent]
})
export class UserCostModule { }
