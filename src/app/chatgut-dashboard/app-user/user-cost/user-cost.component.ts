import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core"
import { ChatbotApiService, iApp } from "app/services/chatbot"
import { AlertService } from "app/services/alert.service"
import { MatSnackBar, MatDialog } from "@angular/material"
import { iDailyCost, iPackage } from "app/services/chatbot/api/crud/dailyCost"
import * as moment from 'moment'
import { iBill } from "app/services/chatbot/api/crud/bill";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'user-cost',
  templateUrl: './user-cost.component.html',
  styleUrls: ['./user-cost.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserCostComponent {

  loadDone: boolean = false
  dailyCosts: iDailyCost[]
  selectedApp: iDailyCost
  apps: iApp[]
  checkingOut: boolean = false
  packages: any
  searchText: string
  orderBy: string
  orderByList = [
    { code: 'expiredDate', display: 'ngày hết hạn' },
    { code: 'alphabet', display: 'bảng chữ cái' },
    { code: 'costAsc', display: 'giá tăng dần' },
    { code: 'costDesc', display: 'giá giảm dần' },
  ]

  assigning: boolean = false
  codeInput: string
  codeInputError: string
  defaultAppId : string = ""
  followApp: boolean = false
  followList = []
  constructor(
    private chatbotApi: ChatbotApiService,
    private changeRef: ChangeDetectorRef,
    private alert: AlertService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private route: ActivatedRoute
  ) {
    moment.locale('vi')
    this.orderBy = this.orderByList[0].code
  }

  async ngOnInit() {
    this.chatbotApi.setTitle('Expenses')
    this.defaultAppId = this.route.snapshot.queryParams['appId']
    await this.getFollowList()
    await this.reload()
  }

  async getFollowList() {
    this.followList = (await this.chatbotApi.chatbotAuth.firebaseDatabase
    .ref(`users/${this.chatbotApi.chatbotAuth.firebaseUser.uid}/follow`).once('value')).val()
    if (!this.followList) this.followList = []
    console.log(this.followList)
  }

  async reload(local = true) {
    try {
      this.loadDone = false
      this.changeRef.detectChanges()
      let firebaseToken = await this.chatbotApi.chatbotAuth.firebaseToken
      let tasks = []
      
      tasks.push(this.chatbotApi.dailyCost.getPackages().then(result => { this.packages = result }))

      if (this.followApp && this.chatbotApi.chatbotAuth.systemUser.rule == 'system_admin') {
        tasks.push(this.chatbotApi.systemUser.getApps({
          local,
          headers: {
            'x-api-key': this.chatbotApi.chatbotAuth.systemUser.token
          },
          query: {
            limit: 0,
            order: { createdAt: -1 },
            populates: [{
              path: 'activePage',
              select: 'name meta'
            }, {
              path: 'license'
            }],
            filter: { _id: { "$in": this.followList } } 
          }
        }).then(result => { 
          for (let row of result.rows) {
            if (typeof(row.owner=='string')) {
              if (row.owner) row.ownerObj = row.users.find(x => x.uid == row.owner)
              else row.ownerObj = ''
            }
            this.calculateExpiration(row)
          }
          this.apps = result.rows
        }))
        tasks.push(this.chatbotApi.dailyCost.overviewFollow(this.chatbotApi.chatbotAuth.systemUser.token, 
          this.followList).then(result => { this.dailyCosts = result }))
      } else {
        tasks.push(this.chatbotApi.app.getList({ local, query: { order: { createdAt: -1 } } })
        .then(result => {
          this.apps = result 
        }))
        tasks.push(this.chatbotApi.dailyCost.overview(firebaseToken).then(result => { this.dailyCosts = result }))
      }

      await Promise.all(tasks)

      for (let i = 0; i < this.dailyCosts.length; i++) {
        let app = this.apps.find(x => x._id == this.dailyCosts[i]._id)
        this.dailyCosts[i] = Object.assign({}, this.dailyCosts[i], app)
        this.calculateExpiration(this.dailyCosts[i])
        this.calculateExtra(this.dailyCosts[i])
      }

      this.sortApps()
      console.log(this.dailyCosts, 'default app', this.defaultAppId)
      if (this.defaultAppId) {
        let app = this.dailyCosts.find(x => x._id == this.defaultAppId)
        if (app) {
          this.selectDailyCost(app)
        } else {
          this.selectDailyCost(this.dailyCosts[0])
        }
      } else {
        this.selectDailyCost(this.dailyCosts[0])
      }
    
    } catch (error) {
      this.alert.handleError(error, 'Lỗi khi lấy danh sách chi phí app');
    } finally {
      this.loadDone = true
      this.changeRef.detectChanges()
    }
  }

  sortApps() {
    switch (this.orderBy) {
      case 'expiredDate': {
        this.dailyCosts = this.dailyCosts.sort((a, b) => (a as any).dayDiff < (b as any).dayDiff?-1:1)
        break
      }
      case 'alphabet': {
        this.dailyCosts = this.dailyCosts.sort((a, b) => a.name < b.name?-1:1)
        break
      }
      case 'costAsc': {
        this.dailyCosts = this.dailyCosts.sort((a, b) => a.amount < b.amount?-1:1)
        break
      }
      case 'costDesc': {
        this.dailyCosts = this.dailyCosts.sort((a, b) => a.amount > b.amount?-1:1)
        break
      }
    }
    this.changeRef.detectChanges()
  }

  toggleCosts() {
    this.followApp = !this.followApp
    this.reload()
  }

  async checkout(appId) {
    if (this.assigning) return

    try {
      this.checkingOut = true
      this.changeRef.detectChanges()
      let firebaseToken = await this.chatbotApi.chatbotAuth.firebaseToken
      let result: iBill = await this.chatbotApi.dailyCost.checkout(firebaseToken, appId)
      location.href = result.transaction.url
      console.log(result)
    } catch (err) {
      this.alert.handleError(err)
    } finally {
      setTimeout(() => {        
        this.checkingOut = false
        this.changeRef.detectChanges()
      }, 1000)
    }
  }

  async editCode() {
    this.selectedApp.license.licenseCode = null
    this.selectedApp.licenseCodeInfo = null
    this.changeRef.detectChanges()
  }
  
  async assigningCode() {
    if (this.checkingOut || !this.selectedApp || (!this.codeInput && !this.selectedApp.license.licenseCode)) return

    try {
      this.assigning = true
      this.changeRef.detectChanges()
      let result
      if (this.selectedApp.license.licenseCode) {
        result = await this.chatbotApi.app.assigningCode(this.selectedApp._id, this.selectedApp.accessToken, this.selectedApp.license.licenseCode)
      } else {
        result = await this.chatbotApi.app.assigningCode(this.selectedApp._id, this.selectedApp.accessToken, this.codeInput)
        this.selectedApp.license.licenseCode = result.code
      }
      this.selectedApp.licenseCodeInfo = result
      this.selectedApp.finalAmount = Math.round(this.selectedApp.amount * (100 - result.saleOf) / 100)
      console.log(result)
    } catch (err) {
      if (err.error.type == "app_exception_license_code_not_exist") {
        this.codeInputError = 'Mã không tồn tại'
      } else if (err.error.type == "billing_exception_license_code_canceled") {
        this.codeInputError = 'Mã đã dừng hoạt động'
      } else {
        this.alert.handleError(err, 'Mã sai hoặc không hoạt động')
      }
    } finally {
      this.assigning = false
      this.changeRef.detectChanges()
    }
  }

  selectDailyCost(app) {
    if (this.checkingOut || this.assigning) return

    this.codeInput = ''
    this.codeInputError = ''
    this.selectedApp = null
    this.changeRef.detectChanges()
    this.selectedApp = app
    this.assigningCode()
    this.changeRef.detectChanges()
  }

  calculateExtra(app) {
    let totalDays = 0
    app.costs = app.costs.sort((a, b) => a.package<b.package?-1:1)
    for (let cost of app.costs) {
      cost.desc = this.packages[cost.package].desc
      totalDays += cost.qty
    }
    app.totalDays = totalDays
  }

  calculateExpiration(app) {

    if (app.whiteListed) {
      app.expiredClass = "label-whitelist"
      app.expiredDuration = "vô thời hạn"
      app.expiredDate = "mãi mãi"
      app.dayDiff = 100000000
      this.changeRef.detectChanges()
      return
    } 

    let expiredMoment = moment(app.license.expireDate);
    app.expiredDate = expiredMoment.format('HH:mm dddd DD-MM-YYYY');
    let expiredMomentUTC = expiredMoment.utc()
    let nowUTC = moment.utc()
    let diffDuration = moment.duration(expiredMomentUTC.diff(nowUTC))
    let minDiff = diffDuration.asMinutes();
    let dayDiff = Math.round(diffDuration.asDays());
    let monthDiff = Math.round(diffDuration.asMonths());
    let yearDiff = Math.round(diffDuration.asYears());
    if (app.license.status === "locking") {
      app.isLocked = true
    }
    if (minDiff > 0) {
      if (yearDiff >= 1) {
        app.expiredDuration = 'còn ' + yearDiff + ' năm'
      } else {
        if (monthDiff >= 1) {
          app.expiredDuration = 'còn ' + monthDiff + ' tháng'
        } else {
          if (dayDiff >= 1) {
            app.expiredDuration = 'còn ' + dayDiff + ' ngày'
          } else {
            app.expiredDuration = 'hết hạn hôm nay'
          }
        }
      }

      if (dayDiff > 15) {
        app.expiredClass = 'label-positive';
      } else if (dayDiff > 7) {
        app.expiredClass = 'label-neutral';
      } else {
        app.isExpiredSoon = true;
        app.expiredClass = 'label-danger';
      }

    } else {
      app.expiredDuration = 'đã hết hạn'
      app.isExpired = true;

      app.expiredClass = 'label-negative'
    }
    app.dayDiff = dayDiff
    this.changeRef.detectChanges()
  }
}