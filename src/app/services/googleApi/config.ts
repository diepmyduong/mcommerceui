export const GAPI_CONFIG = {
    client_id: '663331927074-t5f46ede908v0l134hqd440tlg2l7g0j.apps.googleusercontent.com',
    scope: 'https://www.googleapis.com/auth/spreadsheets https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/userinfo.email',
    response_type: 'code id_token permission',
    access_type: "offline"
}