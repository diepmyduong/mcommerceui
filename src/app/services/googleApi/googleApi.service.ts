import { Injectable } from "@angular/core";

declare const gapi: any;
@Injectable()
export class GoogleApiService {
    constructor() {
        gapi.load('client:auth2', this.init());
    } 
    init(){

    }
    async authorize() {
        return new Promise((resolve, reject) => {
            gapi.auth2.authorize({
                client_id: "562548548771-rk4v3dehuidi4v0i79rn8d2kap2bgj00.apps.googleusercontent.com",
                scope: 'https://www.googleapis.com/auth/spreadsheets https://www.googleapis.com/auth/drive.file https://www.googleapis.com/auth/userinfo.email',
                response_type: 'code id_token permission',
                access_type: "offline",
                prompt:"consent",
            }, function (response) {
                if (response.error) {
                    reject(response.error);
                }
                resolve(response)
            });
        })
    }
}