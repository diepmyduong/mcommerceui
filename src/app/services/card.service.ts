import { Injectable } from '@angular/core';
import { CardContainerComponent } from 'app/shared/cards/card-container/card-container.component';

let cards = {}

export const STORY_CARD_COMPONENT_LIST = [
  "TextCardComponent",
  "ImageCardComponent",
  "AudioCardComponent",
  "VideoCardComponent",
  "GenericCardComponent",
  "ButtonsCardComponent",
  "ListCardComponent",
  "GenericCategoriesCardComponent",
  "GenericProductsCardComponent",
  "MediaVideoCardComponent",
  "MediaImageCardComponent",
  "ListCategoriesCardComponent",
  "ListProductsCardComponent",
  "InputActivityCardComponent",
  "QuickReplyActivityCardComponent",
  "RangeActivityCardComponent",
  "ApiActionCardComponent",
  "UpdateActionCardComponent",
  "DynamicGenericCardComponent",
  "QrActivityCardComponent",
  "JsonInputActionCardComponent",
  "MediaActivityCardComponent",
  "DynamicQuickReplyActivityCardComponent",
  "ParseHtmlActionCardComponent",
  "OgVideoCallCardComponent",
  "DynamicFormActivityCardComponent",
  "NotifyActionCardComponent",
  "ApiActionCardComponent",
  "ButtonsActivityCardComponent",
  "TypingOnCardComponent",
  "CompareImageActionCardComponent",
  "GoogleSpreadsheetActionCardComponent",
  "GoToStoryCardComponent",
  "QuickReplyCardComponent",
  "ActionSendStoryCardComponent",
  "TagCardComponent",
]

export const COMMON_BROADCAST_CARD_COMPONENT_LIST = [
  "ButtonsCardComponent",
  "ImageCardComponent",
  "GenericCardComponent",
  "AudioCardComponent",
  "VideoCardComponent",
]

export const COMMON_STORY_CARD_COMPONENT_LIST = [
  // "TextCardComponent",
  "ButtonsCardComponent",
  "ImageCardComponent",
  // "AudioCardComponent",
  // "VideoCardComponent",
  "GenericCardComponent",
  // "ListCardComponent",
  // "GenericCategoriesCardComponent",
  // "GenericProductsCardComponent",
  // "MediaVideoCardComponent",
  // "MediaImageCardComponent",
  // "ListCategoriesCardComponent",
  // "ListProductsCardComponent",
  // "InputActivityCardComponent",
  // "QuickReplyActivityCardComponent",
  // "RangeActivityCardComponent",
  // "ApiActionCardComponent",
  "UpdateActionCardComponent",
  // "DynamicGenericCardComponent",
  // "QrActivityCardComponent",
  // "JsonInputActionCardComponent",
  // "MediaActivityCardComponent",
  // "DynamicQuickReplyActivityCardComponent",
  // "ParseHtmlActionCardComponent",
  // "OgVideoCallCardComponent",
  // "DynamicFormActivityCardComponent",
  // "NotifyActionCardComponent",
  // "ApiActionCardComponent",
  // "ButtonsActivityCardComponent",
  // "TypingOnCardComponent",
  // "CompareImageActionCardComponent",
  // "GoogleSpreadsheetActionCardComponent",
  "GoToStoryCardComponent",
  // "QuickReplyCardComponent"
]
export const DEFAULT_POPULAR_CARDS = [
  { "code": "button", "component": "ButtonsCardComponent", "display": "Tin nhắn", "icon": "fas fa-font" },
  { "code": "image", "component": "ImageCardComponent", "display": "Hình ảnh", "icon": "far fa-image" },
  { "code": "generic", "component": "GenericCardComponent", "display": "Hình và nút", "icon": "fas fa-newspaper" },
  { "code": "action_update", "component": "UpdateActionCardComponent", "display": "Lưu dữ liệu", "icon": "fas fa-save" },
  { "code": "go_to_story", "component": "GoToStoryCardComponent", "display": "Chuyển câu chuyện", "icon": "fas fa-book" },
]
export const DEFAULT_POPULAR_CARDS_BROADCAST = [
  { "code": "button", "component": "ButtonsCardComponent", "display": "Tin nhắn", "icon": "fas fa-font" },
  { "code": "image", "component": "ImageCardComponent", "display": "Hình ảnh", "icon": "far fa-image" },
  { "code": "generic", "component": "GenericCardComponent", "display": "Hình và nút", "icon": "fas fa-newspaper" },
  { "code": "list", "component": "ListCardComponent", "display": "Mẫu danh sách", "icon": "far fa-list-alt" },
  { "code": "media_image", "component": "MediaImageCardComponent", "display": "Mẫu hình ảnh", "icon": "far fa-images" },
]
export const CARD_CODE_COMPONENT_LIST_BROADCAST = [
  { "code": "button", "component": "ButtonsCardComponent", "display": "Tin nhắn", "icon": "fas fa-font" },
  { "code": "image", "component": "ImageCardComponent", "display": "Hình ảnh", "icon": "far fa-image" },
  { "code": "generic", "component": "GenericCardComponent", "display": "Hình và nút", "icon": "fas fa-newspaper" },
  { "code": "audio", "component": "AudioCardComponent", "display": "Âm thanh", "icon": "fas fa-music" },
  { "code": "video", "component": "VideoCardComponent", "display": "Video", "icon": "fas fa-video" },
  { "code": "text", "component": "TextCardComponent" ,"display": "Tin nhắn", "icon": "fas fa-font"},
  { "code": "list", "component": "ListCardComponent", "display": "Mẫu danh sách", "icon": "far fa-list-alt" },
  { "code": "media_video", "component": "MediaVideoCardComponent", "display": "Mẫu video", "icon": "far fa-file-video" },
  { "code": "media_image", "component": "MediaImageCardComponent", "display": "Mẫu hình ảnh", "icon": "far fa-images" },
]
export const CARD_CODE_COMPONENT_LIST = [
  // { code: 'text', component: 'TextCardComponent' },
  // { code: 'image', component: 'ImageCardComponent' },
  // { code: 'audio', component: 'AudioCardComponent' },
  // { code: 'video', component: 'VideoCardComponent' },
  // { code: 'generic', component: 'GenericCardComponent' },
  // { code: 'button', component: 'ButtonsCardComponent' },
  // { code: 'list', component: 'ListCardComponent' },
  // { code: 'generic_categories', component: 'GenericCategoriesCardComponent' },
  // { code: 'generic_products', component: 'GenericProductsCardComponent' },
  // { code: 'media_video', component: 'MediaVideoCardComponent' },
  // { code: 'media_image', component: 'MediaImageCardComponent' }, 
  // { code: 'list_categories', component: 'ListCategoriesCardComponent' },
  // { code: 'list_products', component: 'ListProductsCardComponent' },
  // { code: 'activity_input', component: 'InputActivityCardComponent' },
  // { code: 'activity_quickreply', component: 'QuickReplyActivityCardComponent' },
  // { code: 'activity_range', component: 'RangeActivityCardComponent' },
  // { code: 'action_api', component: 'ApiActionCardComponent' },
  // { code: 'action_update', component: 'UpdateActionCardComponent' },
  // { code: 'generic_dynamic', component: 'DynamicGenericCardComponent' },
  // { code: 'activity_qrcode', component: 'QrActivityCardComponent' },
  // { code: 'action_json_input', component: 'JsonInputActionCardComponent' },
  // { code: 'activity_media', component: 'MediaActivityCardComponent' },
  // { code: 'activity_dynamic_quickreply', component: 'DynamicQuickReplyActivityCardComponent' },
  // { code: 'action_parse_html', component: 'ParseHtmlActionCardComponent' },
  // { code: 'og_video_call', component: 'OgVideoCallCardComponent' },
  // { code: 'activity_dynamic_form', component: 'DynamicFormActivityCardComponent' },
  // { code: 'action_notify', component: 'NotifyActionCardComponent' },
  // { code: 'action_innoway_api', component: 'ApiActionCardComponent' },
  // { code: 'activity_button', component: 'ButtonsActivityCardComponent' },
  // { code: 'typing_on', component: 'TypingOnCardComponent' },
  // { code: 'action_compare_image', component: 'CompareImageActionCardComponent' },
  // { code: 'action_google_spreadsheet', component: 'GoogleSpreadsheetActionCardComponent' },
  // { code: 'go_to_story', component: 'GoToStoryCardComponent' },
  // { code: 'action_add_label', component: 'AddLabelCardComponent' },
  // { code: 'action_sequence', component: 'SequenceCardComponent' },
  // { code: 'action_send_story', component: 'ActionSendStoryCardComponent' },
  ...CARD_CODE_COMPONENT_LIST_BROADCAST,
  { "code": 'activity_datepicker',"component": "DatepickerCardComponent", "display": 'Lấy dữ liệu ngày', "icon": 'far fa-calendar-check' },
  { "code": "generic_categories", "component": "GenericCategoriesCardComponent", "display": "Loại sản phẩm", "icon": "fas fa-cubes" },
  { "code": "generic_products", "component": "GenericProductsCardComponent", "display": "Sản phẩm", "icon": "fas fa-cube" },
  { "code": "list_categories", "component": "ListCategoriesCardComponent", "display": "Danh sách mục sản phẩm", "icon": "fas fa-th-list" },
  { "code": "list_products", "component": "ListProductsCardComponent", "display": "Danh sách sản phẩm", "icon": "fas fa-list" },
  { "code": "activity_input", "component": "InputActivityCardComponent", "display": "Nhập chuỗi", "icon": "fas fa-keyboard" },
  { "code": "activity_quickreply", "component": "QuickReplyActivityCardComponent", "display": "Trả lời nhanh", "icon": "fas fa-comment-alt" },
  { "code": "activity_range", "component": "RangeActivityCardComponent", "display": "Nhập số", "icon": "fas fa-sort-numeric-down" },
  { "code": "action_api", "component": "ApiActionCardComponent", "display": "Gọi API", "icon": "fas fa-bolt" },
  { "code": "action_update", "component": "UpdateActionCardComponent", "display": "Lưu dữ liệu", "icon": "fas fa-save" },
  { "code": "generic_dynamic", "component": "DynamicGenericCardComponent", "display": "Menu động", "icon": "fas fa-file-code" },
  { "code": "activity_qrcode", "component": "QrActivityCardComponent", "display": "Quét QR code", "icon": "fas fa-qrcode" },
  { "code": "action_json_input", "component": "JsonInputActionCardComponent", "display": "Dữ liệu JSON", "icon": "fab fa-node-js" },
  { "code": "activity_media", "component": "MediaActivityCardComponent", "display": "Gửi File", "icon": "fas fa-paperclip" },
  { "code": "activity_dynamic_quickreply", "component": "DynamicQuickReplyActivityCardComponent", "display": "Gợi ý động", "icon": "fas fa-comments" },
  { "code": "action_parse_html", "component": "ParseHtmlActionCardComponent", "display": "Dữ liệu Website", "icon": "fas fa-globe" },
  { "code": "og_video_call", "component": "OgVideoCallCardComponent" },
  { "code": "activity_dynamic_form", "component": "DynamicFormActivityCardComponent", "display": "Biểu mẫu", "icon": "fas fa-clipboard" },
  { "code": "action_notify", "component": "NotifyActionCardComponent", "display": "Thông báo", "icon": "fas fa-bell" },
  { "code": "action_innoway_api", "component": "ApiActionCardComponent", "display": "Gọi API", "icon": "fas fa-bolt" },
  { "code": "activity_button", "component": "ButtonsActivityCardComponent", "display": "Tương tác nút bấm", "icon": "fab fa-stack-exchange" },
  { "code": "typing_on", "component": "TypingOnCardComponent", "display": "Thẻ chờ", "icon": "far fa-keyboard" },
  { "code": "action_compare_image", "component": "CompareImageActionCardComponent", "display": "So sánh ảnh", "icon": "fas fa-exchange-alt" },
  { "code": "action_google_spreadsheet", "component": "GoogleSpreadsheetActionCardComponent", "display": "Google spreadsheet", "icon": "fas fa-table" },
  { "code": "go_to_story", "component": "GoToStoryCardComponent", "display": "Chuyển câu chuyện", "icon": "fas fa-book" },
  { "code": "action_add_label", "component": "AddLabelCardComponent", "display": "Thêm nhãn", "icon": "fas fa-tag" },
  { "code": "action_sequence", "component": "SequenceCardComponent", "display": "Gắn/hủy luồng", "icon": "fas fa-stream" },
  { "code": "action_send_story", "component": "ActionSendStoryCardComponent", "display": "Gửi câu chuyện", "icon": "fas fa-paper-plane" },
  { "code": "infusion_add_contact","component":"InfusionAddContactCardComponent", "display": 'Thêm contact Infusion', "icon": 'far fa-plus-square'},
  { "code": "infusion_update_contact","component":"InfusionAddContactCardComponent", "display": 'Cập nhật contact Infusion', "icon": 'fas fa-wrench' ,  extra: { type: 'update' } },
  { "code": "infusion_achive_goal","component":"InfusionAchiveGoalCardComponent", "display": 'Thêm luồng Infusion', "icon": 'fas fa-project-diagram'},
  { "code": "action_e_voucher","component":"EvoucherCardComponent", "display": 'Khuyến mãi', "icon": 'fas fa-ticket-alt'},
  { "code": "action_subscriber_tag","component":"TagCardComponent", "display": 'Gắn/gỡ tag', "icon": 'fas fa-tags'},
  { "code": "action_member_point","component":"MemberPointCardComponent", "display": 'Tích điểm', "icon": 'fas fa-dice'},
]


@Injectable()
export class CardService {

  constructor() { }
  containers: { [x: string]: CardContainerRef } = {};

  regisContainer(container: CardContainerComponent) {
    if (!this.containers[container.id]) this.containers[container.id] = new CardContainerRef(container.id, container);
    return this.containers[container.id];
  }

  regisCard(card: any) {
    const component = new card()
    if (component.componentName) {
      cards[component.componentName] = card
    }
  }

  clearContainer(container: CardContainerComponent) {
    delete this.containers[container.id]
  }

}

export class CardContainerRef {
  constructor(public id: string, public container: CardContainerComponent) { }
  pushCardComp(name: string, params?: any, scroll?: boolean, pos?: number) {
    return this.container.pushCardComp(cards[name] || name, params, scroll, pos)
  }
}
