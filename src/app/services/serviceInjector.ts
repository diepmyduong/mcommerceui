import { Injector } from "@angular/core";

export let ServiceInjector = {
    set: function(injector: Injector) {
        ServicesInjector = injector;
    },
    get: function() {
        return ServicesInjector;
    }
}

export let ServicesInjector: Injector;