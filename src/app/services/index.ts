import { ServicesModule } from 'app/services/services.module'
import * as ChatBotService from 'app/services/chatbot'
export {
    ServicesModule,
    ChatBotService
}