import { Injectable } from '@angular/core';
import { AlertDialog } from 'app/modals/alert/alert.component';
import { MatDialog } from '@angular/material';
@Injectable()
export class AlertService {

    constructor(private dialog: MatDialog) { }

    private async openDialog(data): Promise<any> {
        if (!data.okText) {
            data.okText = "OK"
        }

        if (!data.confirmText) {
            data.confirmText = "Tiếp tục"
        }

        if (!data.cancelText) {
            data.cancelText = "Quay lại"
        }

        let dialogRef = this.dialog.open(AlertDialog, {
            width: '560px',
            data: data
        });

        return await dialogRef.afterClosed().toPromise()
    }

    async success(title, subtitle, okText = ''): Promise<any> {
        let data = { type: 'success', title, subtitle, okText }
        return await this.openDialog(data)
    }

    async info(title, subtitle, okText = ''): Promise<any> {
        let data = { type: 'info', title, subtitle, okText }
        return await this.openDialog(data)
    }

    async error(title, subtitle, okText = ''): Promise<any> {
        let data = { type: 'error', title, subtitle, okText }
        return await this.openDialog(data)
    }

    async warning(title, subtitle, confirmText = '', cancelText = '', confirmFn = null): Promise<any> {
        let data = { type: 'warning', title, subtitle, confirmText, cancelText, confirmFn}
        return await this.openDialog(data)
    }

    async question(title, subtitle, confirmText = '', cancelText = '', confirmFn = null): Promise<any> {
        let data = { type: 'question', title, subtitle, confirmText, cancelText, confirmFn}
        return await this.openDialog(data)
    }

    handleError(res: any, title = 'Có lỗi') {
        if ((res.message && res.message.indexOf('Attempt to use a destroyed view: detectChanges') >= 0) || (
            res.error && res.error.message.indexOf('Attempt to use a destroyed view: detectChanges') >= 0
        )) {
            console.warn('ViewDestroyedError')
            return
        } else if (res.error && res.error.message) {
            console.error(res)
            return this.error(title, res.error.message);
        } else {
            console.error(res)
            return this.error(title, res.message);
        }
    }

}