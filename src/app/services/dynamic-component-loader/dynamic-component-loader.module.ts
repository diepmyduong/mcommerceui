import { NgModule, NgModuleFactoryLoader, SystemJsNgModuleLoader, ModuleWithProviders, InjectionToken } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicComponentManifest } from 'app/services/dynamic-component-loader/dynamic-component-loader.manifest';
import { ROUTES } from '@angular/router';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [{ provide: NgModuleFactoryLoader, useClass: SystemJsNgModuleLoader }]
})
export class DynamicComponentLoaderModule {
  static forRoot(manifests: DynamicComponentManifest[]): ModuleWithProviders {
    return {
      ngModule: DynamicComponentLoaderModule,
      providers: [
        // provider for Angular CLI to analyze
        { provide: ROUTES, useValue: manifests, multi: true },
        { provide: DYNAMIC_COMPONENT_MANIFESTS, useValue: manifests },
      ],
    };
  }
}

export const DYNAMIC_COMPONENT_MANIFESTS = new InjectionToken<any>(`DYNAMIC_COMPONENT_MANIFESTS`);
