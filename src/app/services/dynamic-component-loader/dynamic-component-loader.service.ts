import { Injectable, Inject, NgModuleFactoryLoader, Injector, ComponentFactory, InjectionToken } from '@angular/core';
import { DYNAMIC_COMPONENT_MANIFESTS } from 'app/services/dynamic-component-loader/dynamic-component-loader.module';
import { DynamicComponentManifest } from 'app/services/dynamic-component-loader/dynamic-component-loader.manifest';

@Injectable()
export class DynamicComponentLoaderService {

  constructor(
    @Inject(DYNAMIC_COMPONENT_MANIFESTS) private manifests: DynamicComponentManifest[],
    private loader: NgModuleFactoryLoader,
    private injector: Injector
  ) { 
  }

  async getComponentFactory<T>(componentId: string, injector?: Injector): Promise<ComponentFactory<T>> {
    const manifest = this.manifests
      .find(m => m.componentId === componentId);

    const p = this.loader.load(manifest.loadChildren)
      .then(ngModuleFactory => {
        const moduleRef = ngModuleFactory.create(injector || this.injector);

        // Read from the moduleRef injector and locate the dynamic component type
        const dynamicComponentType = moduleRef.injector.get(DYNAMIC_COMPONENT);
        // Resolve this component factory
        return moduleRef.componentFactoryResolver.resolveComponentFactory<T>(dynamicComponentType);
      });

    return p;
  }

}

export const DYNAMIC_COMPONENT = new InjectionToken<any>('DYNAMIC_COMPONENT');