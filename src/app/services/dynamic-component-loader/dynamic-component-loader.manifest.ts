export interface DynamicComponentManifest {
    code?: string
    componentId: string;
    path: string;
    loadChildren: string;
}