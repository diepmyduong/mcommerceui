import { Injectable } from '@angular/core';
import { ButtonContainerComponent } from 'app/shared/buttons/button-container/button-container.component';

let buttons = {}

export const STORY_BUTTON_COMPONENT_LIST = [
  "UrlButtonComponent",
  "PostbackButtonComponent",
  "PhoneButtonComponent",
  "ShareButtonComponent",
  "DeeplinkButtonComponent"
]

@Injectable()
export class ButtonService {

  constructor() { }
  containers: { [x: string]: ButtonContainerRef } = {};

  regisContainer(container: ButtonContainerComponent) {
    if (!this.containers[container.containerId]) this.containers[container.containerId] = new ButtonContainerRef(container.containerId, container);
    return this.containers[container.containerId];
  }

  regisButton(button: any) {
    const component = new button()
    if (component.componentName) {
      buttons[component.componentName] = button
    }
  }

  clearContainer(container: ButtonContainerComponent) {
    delete this.containers[container.containerId]
  }
}

export class ButtonContainerRef {
  constructor(public id: string, public container: ButtonContainerComponent) { }
  pushCardComp(name: string, params?: any, scroll?: boolean) {
    return this.container.pushButton(buttons[name])
  }
}