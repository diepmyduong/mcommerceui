import { NgModule, Optional, SkipSelf, Injector } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChatbotConfigService } from 'app/services/chatbot/chatbot-config.service';
import { ChatbotAuthService } from 'app/services/chatbot/chatbot-auth.service';
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service';
import { FirebaseAuthGuard } from 'app/services/chatbot/firebase-auth.guard';
import { AppAuthGuard } from 'app/services/chatbot/app-auth.guard';
import { FacebookService } from 'ngx-facebook'
import { AlertService } from 'app/services/alert.service';
import { PortalService } from 'app/services/portal.service';
import { ServiceInjector } from 'app/services/serviceInjector';
import { CardService } from 'app/services/card.service';
import { ButtonService } from 'app/services/button.service';
import { GoogleApiService } from 'app/services/googleApi/googleApi.service';
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    FacebookService,
    ChatbotConfigService,
    ChatbotAuthService,
    ChatbotApiService,
    FirebaseAuthGuard,
    AppAuthGuard,
    AlertService,
    PortalService,
    CardService,
    ButtonService,
    GoogleApiService
  ]
})
export class ServicesModule { 
  constructor(
    @Optional() @SkipSelf() parentModule: ServicesModule,
    private injector: Injector
  ) {
    if(parentModule) throw new Error('ServicesModule is already loaded. Import only in AppModule');
    ServiceInjector.set(injector)
  }
}