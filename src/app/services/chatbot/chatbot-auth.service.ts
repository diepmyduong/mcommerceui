import { Injectable } from "@angular/core";
import { ChatbotConfigService } from "app/services/chatbot/chatbot-config.service";
import { BehaviorSubject } from "rxjs";
import { iApp, iUser } from "app/services/chatbot/api/crud/app";
import * as request from "request-promise";
import * as firebase from "firebase/app";
import "firebase/auth";
import { environment } from "@environments";
import * as Console from "console-prefix";
import { FacebookService, InitParams } from "ngx-facebook";
import { GoogleApiService } from "app/services/googleApi/googleApi.service";
import { iSystemUser } from "./api/crud/systemUser";
@Injectable()
export class ChatbotAuthService {
  constructor(
    public chatbotConfig: ChatbotConfigService,
    public fb: FacebookService,
    public googleApiSrv: GoogleApiService
  ) {
    // firebase.auth().useDeviceLanguage()
    this.firebaseApp = firebase.initializeApp(environment.chatbot.firebase);
    this.facebookProvider = new firebase.auth.FacebookAuthProvider();
    // this.fbLoginScope = 'business_management,email,manage_pages,pages_messaging,public_profile,publish_pages,user_friends,pages_messaging_phone_number,pages_messaging_subscriptions,read_page_mailboxes'
    this.fbLoginScope = [
      "email",
      "pages_manage_ads",
      "pages_manage_engagement",
      "pages_manage_metadata",
      "pages_manage_posts",
      "pages_messaging",
      "pages_read_engagement",
      "pages_read_user_content",
      // "pages_user_gender",
      // "pages_user_locale",
      // "pages_user_timezone",
      // "default",
    ].join(",");

    this.facebookProvider.addScope(this.fbLoginScope);
    this.facebookProvider.setCustomParameters({ display: "popup" });
    this.googleProvider = new firebase.auth.GoogleAuthProvider();
    this.googleProvider.addScope("https://www.googleapis.com/auth/userinfo.email");
    this.googleProvider.addScope("https://www.googleapis.com/auth/userinfo.profile");
    this.googleProvider.addScope("https://www.googleapis.com/auth/user.birthday.read");
    this.googleProvider.addScope("https://www.googleapis.com/auth/user.phonenumbers.read");
    this.firebaseApp.auth().onAuthStateChanged((user) => {
      this.log("onAuthStateChanged", user);
      if (user) {
        if (!this.manuallyLogin) {
          this.firebaseUser = user;
          user.getIdToken().then((token) => {
            this.onAuthStateChange.next(true);
          });
        }
      } else {
        this.onAuthStateChange.next(false);
      }
    });

    // FB API
    this.initFBService().then((res) => {
      this.log("Facebook SDK Inited", res);
    });
    fb.getLoginStatus().then((status) => {
      this.log("fb login status", status);
      this.fbLoginStatus = status;
      this.facebookToken = status.authResponse.accessToken;
    });
  }

  get log() {
    return Console("[chatbotAuth]").log;
  }
  onAuthStateChange = new BehaviorSubject<Boolean>(undefined);
  app: iApp = {};
  user: iUser;
  isOwner: boolean = false;
  adminToken: string;
  firebaseApp: firebase.app.App;
  facebookProvider: firebase.auth.FacebookAuthProvider;
  googleProvider: firebase.auth.GoogleAuthProvider;
  firebaseUser: firebase.User;
  // firebaseToken: string
  facebookToken: string;
  googleToken: string;
  manuallyLogin: boolean = false;
  fbLoginStatus: any;
  fbLoginScope: string;
  fbInitParams: InitParams = {
    appId: environment.facebook.appId,
    xfbml: true,
    version: environment.facebook.version,
    cookie: true,
  };
  systemUser: iSystemUser;
  hasCheckSystemUser: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  get firebaseDatabase() {
    return this.firebaseApp.database();
  }

  get firebaseToken() {
    return this.firebaseUser.getIdToken();
  }

  get appToken() {
    return `A|${this.app._id}|${this.app.accessToken}`;
  }

  async exec(option: any) {
    if (!option) throw new Error("option undefined in exec");
    try {
      let { uri, ...anohter } = option;
      return await request(uri, anohter);
    } catch (resError) {
      console.error("Lỗi", "Innoway Auth ERROR");
      throw resError;
    }
  }

  initFBService() {
    return this.fb.init(this.fbInitParams);
  }

  async setStatus(status: string) {
    const uid = this.firebaseUser.uid;
    const appId = this.app._id;
    const ref = this.firebaseDatabase.ref(`callCenter/${appId}/${uid}`);
    await ref.set({ status });
    ref.onDisconnect().set({ status: "offline" });
    return;
  }

  async getAppState() {
    await this.getApps();
  }

  async getApps() {
    if (this.adminToken) {
      const option = {
        method: "GET",
        uri: this.chatbotConfig.apiUrl("app"),
        headers: {
          //headers
          access_token: await this.firebaseToken,
        },
        json: true, // Automatically parses the JSON string in the response,
      };
      try {
        let res = await this.exec(option);
        if (res.results && res.results.objects && res.results.objects.count > 0) {
          this.app = res.results.objects.rows[0];
        }
        this.onAuthStateChange.next(true);
        return res.results.objects.rows;
      } catch (err) {
        this.onAuthStateChange.next(false);
        return null;
      }
    } else {
      this.onAuthStateChange.next(false);
      return null;
    }
  }

  async getFacebookToken(force = false) {
    if (!this.fbLoginStatus || this.fbLoginStatus.status != "connected" || force) {
      this.fbLoginStatus = await this.fb.login({
        scope: this.fbLoginScope,
        return_scopes: true,
        enable_profile_selector: true,
      });
    }
    this.facebookToken = this.fbLoginStatus.authResponse.accessToken;
    return this.facebookToken;
  }

  async loginFacebook() {
    this.manuallyLogin = true;
    const result = await this.firebaseApp.auth().signInWithPopup(this.facebookProvider);
    this.facebookToken = (<any>result.credential).accessToken;
    this.firebaseUser = result.user;
    this.onAuthStateChange.next(true);
    return result;
  }

  async loginEmailAndPassword(email: string, password: string) {
    this.manuallyLogin = true;
    const result = await this.firebaseApp.auth().signInWithEmailAndPassword(email, password);
    this.firebaseUser = result as any;
    this.onAuthStateChange.next(true);
    return this.firebaseUser;
  }

  async loginGoogle() {
    this.manuallyLogin = true;
    const result = await this.firebaseApp.auth().signInWithPopup(this.googleProvider);
    this.googleToken = (<any>result.credential).accessToken;
    this.firebaseUser = result.user;
    this.onAuthStateChange.next(true);
    return result;
  }

  async logout() {
    await this.firebaseApp.auth().signOut();
    try {
      this.fb
        .logout()
        .then((result) => {
          console.log("res", result);
        })
        .catch((err) => {
          console.log("error", err);
        });
      location.reload();
      return true;
    } catch (err) {
      console.error(err);
    }
  }

  get authenticated(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (this.onAuthStateChange.getValue() != undefined) {
        this.log("authenticated", this.onAuthStateChange.getValue());
        resolve(this.onAuthStateChange.getValue() as boolean);
      } else {
        let subscription = this.onAuthStateChange.subscribe((state) => {
          this.log("onAuthStateChange", state);
          if (subscription) {
            resolve(state as boolean);
            subscription.unsubscribe();
          }
        });
      }
    });
  }

  async getUsers() {
    const users = await this.firebaseApp.database().ref("users").once("value");
    return users.val() as iUserRecords;
  }

  async getUserInfo(userId: string) {
    const user = await this.firebaseApp.database().ref(`users/${userId}`).once("value");
    return user.val() as iUserRecord;
  }

  async findUsers(email: string) {
    const users = await this.firebaseApp
      .database()
      .ref(`users`)
      .orderByChild("email")
      .startAt(email)
      .endAt(email + "\uf8ff")
      .limitToFirst(5)
      .once("value");
    return users.val() as iUserRecords;
  }

  async sendVerifyEmail() {
    return await this.firebaseUser.sendEmailVerification({
      url: this.chatbotConfig.config.uiHost,
    });
  }
}

export interface iUserRecord {
  uid: string;
  email: string;
  displayName: string;
  photoURL: string;
  phoneNumber: string;
  [key: string]: any;
}

export interface iUserRecords {
  [uid: string]: iUserRecord;
}
