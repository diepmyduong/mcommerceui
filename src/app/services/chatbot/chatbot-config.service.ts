import { Injectable } from '@angular/core';
import { environment } from '@environments'
@Injectable()
export class ChatbotConfigService {

  constructor() {
    this.config = {
      host: environment.chatbot.host,
      version: environment.chatbot.version,
      uiHost: environment.chatbot.uiHost
    }
  }

  config: {
    host: string
    version: string
    uiHost: string
  }

  defaultAppPicture: string = 'https://i.imgur.com/rRsjehj.png'
  defaultUserAvatar: string = 'https://i.imgur.com/NN9xQ5Q.png'
  pasteData: string

  apiUrl(path: string = "") {
    return `${this.config.host}/api/${this.config.version}/${path}`
  }
  viewUrl(path: string = "") {
    return `${this.config.host}/view/${this.config.version}/${path}`
  }

}
