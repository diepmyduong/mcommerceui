import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute, Router } from '@angular/router';
import * as Console from 'console-prefix'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service';
import { iApp } from 'app/services/chatbot/api/crud/app';

@Injectable()
export class AppAuthGuard implements CanActivate {
  get log() { return Console('[AppAuth Guard]').log }
  constructor(
    public chatbotApi: ChatbotApiService,
    public router: Router,
    public route: ActivatedRoute
  ) {

  }
  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean> {
      try {
        const appId = next.params.id
        const app: iApp = await this.chatbotApi.app.getItem(appId, { local: true })
        if (app.activePage) {
          this.chatbotApi.chatbotAuth.app = app
          this.chatbotApi.chatbotAuth.user = app.users.find(u => u.uid === this.chatbotApi.chatbotAuth.firebaseUser.uid)
          console.log( 'user',this.chatbotApi.chatbotAuth.user)
          this.chatbotApi.chatbotAuth.isOwner = app.owner == this.chatbotApi.chatbotAuth.user.uid
          return true
        } else {
          this.router.navigate(['apps'])
          return false
        }
      } catch (err) {
        console.error(err)
        this.router.navigate(['/apps'])
      }

  }
}
