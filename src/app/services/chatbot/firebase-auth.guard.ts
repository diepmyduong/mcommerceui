import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { Router } from '@angular/router'
import * as Console from 'console-prefix'
@Injectable()
export class FirebaseAuthGuard implements CanActivate {
  constructor(
    public chatbotApi: ChatbotApiService,
    public router: Router
  ) {

  }
  get log() { return Console(`[Firebase auth]`).log }
  canActivate(
    next: ActivatedRouteSnapshot,
    stateSnapshot: RouterStateSnapshot): Promise<boolean> {
    return this.chatbotApi.chatbotAuth.authenticated.then(async state => {
      if (state == false) {
        sessionStorage.setItem('relogin_router_state_url', JSON.stringify({
          path: stateSnapshot.url.substring(0, stateSnapshot.url.indexOf("?")),
          queryParams: next.queryParams
        }))
        this.router.navigate(['/pages/login'])
      } else {
        if (state == true && !this.chatbotApi.chatbotAuth.hasCheckSystemUser.getValue()) {
          try {
            let firebaseToken = await this.chatbotApi.chatbotAuth.firebaseToken
            this.chatbotApi.chatbotAuth.systemUser = await this.chatbotApi.systemUser.login(firebaseToken)
          } catch (err) {
            this.chatbotApi.chatbotAuth.systemUser = null
          } finally {
            this.chatbotApi.chatbotAuth.hasCheckSystemUser.next(true)
          }
        }
      }
      return state
    })
  }
}
