import { Injectable } from '@angular/core';
import { ChatbotAuthService } from 'app/services/chatbot/chatbot-auth.service'
import { ChatbotConfigService } from 'app/services/chatbot/chatbot-config.service'
import { App } from 'app/services/chatbot/api/crud/app'
import { Page } from 'app/services/chatbot/api/crud/page'
import { Setting } from 'app/services/chatbot/api/crud/setting'
import { Story } from 'app/services/chatbot/api/crud/story'
import { Card } from 'app/services/chatbot/api/crud/card'
import { Subscriber } from 'app/services/chatbot/api/crud/subscriber'
import { Referral } from 'app/services/chatbot/api/crud/referral'
import { Dictionary } from 'app/services/chatbot/api/crud/dictionnary'
import { Group, iGroup } from 'app/services/chatbot/api/crud/group'
import { Send } from 'app/services/chatbot/api/send'
import { AI } from 'app/services/chatbot/api/crud/ai'
import { DeepLink } from 'app/services/chatbot/api/deeplink'
import { CardBuilderService } from 'app/services/chatbot/card-builder.service';
import { Topic } from 'app/services/chatbot/api/crud/topic'
import { TopicEvent } from 'app/services/chatbot/api/crud/topicEvent'
import { TopicSubscriber } from 'app/services/chatbot/api/crud/topicSubscriber'
import { Socket } from 'app/services/chatbot/api/socket';
import { Label } from 'app/services/chatbot/api/crud/label';
import { AdminNote } from 'app/services/chatbot/api/crud/admin-note'
import { InteractPost } from 'app/services/chatbot/api/crud/interactPost';
import { Upload } from "app/services/chatbot/api/crud/upload";
import { Statistic } from "app/services/chatbot/api/crud/statistic";
import { Task } from 'app/services/chatbot/api/crud/task';
import { Template } from 'app/services/chatbot/api/crud/template';
import { AlertService } from 'app/services/alert.service';
import { User } from 'app/services/chatbot/api/user';
import { ButtonTag } from 'app/services/chatbot/api/crud/buttonTag'
import { Environment } from 'app/services/chatbot/api/crud/environment';
import { ApiKey } from 'app/services/chatbot/api/crud/apiKey';
import { NotifyUser } from 'app/services/chatbot/api/crud/notifyUser';
import { QuickMessage } from 'app/services/chatbot/api/crud/quickMessage'
import { Plugin } from 'app/services/chatbot/api/crud/plugin'
import { TemplateCategory } from 'app/services/chatbot/api/crud/templateCategory';
import { Clarifai } from 'app/services/chatbot/api/crud/clarifai'
import { ClarifaiImage } from 'app/services/chatbot/api/crud/clarifaiImage'
import { Bill } from 'app/services/chatbot/api/crud/bill'
import { Billing } from 'app/services/chatbot/api/billing'
import { Broadcast } from './api/crud/broadcast';
import { GsheetUser } from './api/crud/gsheetUser';
import { StoryGroup } from './api/crud/storyGroup';
import { LuckyWheel } from './api/crud/luckyWheel';
import { LuckyWheelReceive } from './api/crud/luckyWheelReceive';
import { SystemUser } from './api/crud/systemUser';
import { DailyCost } from './api/crud/dailyCost';
import { LicenseCode } from './api/crud/licenseCode';
import { Infusion } from './api/crud/infusion';
import { Product } from './api/crud/product';
import { ProductCategory } from './api/crud/productCategory';
import { Shop } from './api/crud/shop';
import { Order } from './api/crud/order';
import { Membership } from './api/crud/membership'
import { MembershipPromotion } from './api/crud/membershipPromotion'
import { MembershipPromotionReceive } from './api/crud/membershipPromotionReceive'
import { Evoucher } from './api/crud/evoucher';
import { EvoucherCode } from './api/crud/evoucherCode';
import { EvoucherLog } from './api/crud/evoucherLog';
import { SubscriberTag } from './api/crud/subscriberTag';
import { SubscriberProperty } from './api/crud/subscriberProperty';
import { Customer } from './api/crud/customer';
import { Haravan } from './api/crud/haravan';
import { HaravanProduct } from './api/crud/haravanProduct';
import { HaravanCategory } from './api/crud/haravanCategory';
import { TopicQuickMessage } from './api/crud/topicQuickMessage';
import { Report } from './api/crud/report';
import { EvoucherRemind } from './api/crud/evoucherRemind';
import { Post } from './api/crud/post';
@Injectable()
export class ChatbotApiService {

  constructor(
    public chatbotConfig: ChatbotConfigService,
    public chatbotAuth: ChatbotAuthService,
    public alert: AlertService
  ) {
    chatbotAuth.onAuthStateChange.subscribe(status => {
      if (status) { this.connectSocket(); }
    })
  }

  setTitle(title) {
    document.title = title + ' - MCOM Chatbot'
  }

  clear() {
    Object.keys(this).forEach(k => {
      if (this[k].clear) {
        this[k].clear()
      }
    })
  }

  async connectSocket() {
    let connected = false
    const connectInterval = setInterval(() => {
      this.socket.getConnectionToken().then(token => {
        connected = true;
        clearInterval(connectInterval)
        this.socket.login(token)
      }).catch(err => {
      })
    }, 5000)
  }

  cardBuilder = new CardBuilderService(this)
  app = new App(this)
  page = new Page(this)
  setting = new Setting(this)
  story = new Story(this)
  storyGroup = new StoryGroup(this)
  card = new Card(this)
  subscriber = new Subscriber(this)
  systemUser = new SystemUser(this)
  dailyCost = new DailyCost(this)
  referral = new Referral(this)
  dictionary = new Dictionary(this)
  send = new Send(this)
  ai = new AI(this)
  deeplink = new DeepLink(this)
  topic = new Topic(this)
  topicEvent = new TopicEvent(this)
  topicSubscriber = new TopicSubscriber(this)
  socket = new Socket(this, this.alert)
  group = new Group(this)
  label = new Label(this)
  licenseCode = new LicenseCode(this)
  luckyWheel = new LuckyWheel(this)
  luckyWheelReceive = new LuckyWheelReceive(this)
  adminNote = new AdminNote(this)
  interactPost = new InteractPost(this)
  upload = new Upload(this)
  statistic = new Statistic(this)
  task = new Task(this)
  template = new Template(this)
  user = new User(this)
  buttonTag = new ButtonTag(this)
  environment = new Environment(this)
  evoucher = new Evoucher(this)
  evoucherCode = new EvoucherCode(this)
  evoucherRemind = new EvoucherRemind(this)
  evoucherLog = new EvoucherLog(this)
  apiKey = new ApiKey(this)
  notifyUser = new NotifyUser(this)
  quickMessage = new QuickMessage(this)
  topicQuickMessage = new TopicQuickMessage(this)
  post = new Post(this)
  plugin = new Plugin(this)
  subscriberTag = new SubscriberTag(this)
  subscriberProperty = new SubscriberProperty(this)
  templateCategory = new TemplateCategory(this)
  clarifai = new Clarifai(this)
  clarifaiImage = new ClarifaiImage(this)
  bill = new Bill(this)
  billing = new Billing(this)
  broadcast = new Broadcast(this)
  gsheetUser = new GsheetUser(this)
  infusion = new Infusion(this)
  product = new Product(this)
  productCategory = new ProductCategory(this)
  shop = new Shop(this)
  haravan = new Haravan(this)
  haravanProduct = new HaravanProduct(this)
  haravanCategory = new HaravanCategory(this)
  order = new Order(this)
  membership = new Membership(this)
  membershipPromotion = new MembershipPromotion(this)
  membershipPromotionReceive = new MembershipPromotionReceive(this)
  customer = new Customer(this)
  report = new Report(this)
}
