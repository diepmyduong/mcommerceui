import { BaseAPI } from 'app/services/chatbot/api/base'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iCard } from 'app/services/chatbot/api/crud/card'

export interface iSendData {
    type: "story" | "new_story"
    story: string | iCard[]
    sendBy: "phone" | "subscriber" | "group" | "all" | "label_id"
    sendTo: any,
    nonTask?: boolean,
    exceptReceived?: boolean,
    schedule?:Date
    scheduleAt?: Date
    context?: any
    maxSubscriber?: number
    random?: boolean
}

export class Send extends BaseAPI {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, "send")
    }

    async sendMessage(data: iSendData) {
        if (!data) throw new Error('data undefined in sendMessage')
        let setting = {
            method: 'POST',
            uri: this.apiUrl(),
            headers: { //headers
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
            },
            body: data,
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        if(res.results && res.results.object) {
            return res.results.object
        } else {
            return res
        }
    }
    async sendBroadcast(data: iSendData) {
      if (!data) throw new Error('data undefined in sendMessage')
      let setting = {
          method: 'POST',
          uri: this.apiUrl('broadcast'),
          headers: { //headers
              "content-type": "application/json",
              'x-api-key': this.api.chatbotAuth.appToken,
          },
          body: data,
          json: true // Automatically parses the JSON string in the response
      }
      let res = await this.exec(setting)
      if(res.results && res.results.object) {
          return res.results.object
      } else {
          return res
      }
  }
}