import { BaseAPI } from "app/services/chatbot/api/base";
import { ChatbotApiService } from "app/services/chatbot/chatbot-api.service"
import * as SocketClient from 'socketcluster-client'
import { environment } from "@environments";
import { EventEmitter } from "@angular/core";
import { AlertService } from "app/services/alert.service";

export class Socket extends BaseAPI {
    constructor(
        public api: ChatbotApiService,
        public alert: AlertService
    ) {
        super(api, "socket")
    }
    CHANNEL_EVENTS = {
        ON_MESSAGE: 'on_message',
        ON_SUBSCRIBER_UPDATE: 'on_subscriber_update',
        ON_PAGE_EXPIRED: 'on_page_expired',
        ON_REGIS_TESTER: 'on_regis_tester',
        ON_TASK_CHANGE: 'on_task_change',
        ON_PAGE_ECHO: 'on_page_echo',
        ON_CHECK_EXPIRED_APP: 'on_check_expired_app',
        ON_THREAD_CHANGE: 'on_thread_change',
        ON_CALL_REQUEST: 'on_call_request',
        ON_REGIS_NOTIFY: 'on_regis_notify',
        ON_REGIS_NOTIFY_DASH: 'on_regis-notify',
    }
    channels: ISocketChannel[] = []
    socket: any
    socketEvents: string[] = []
    connectionOption = {
        path: '/socketcluster/',
        port: environment.socket.port,
        hostname: environment.socket.host,
        autoConnect: true,
        secure: environment.socket.secure,
        rejectUnauthorized: false,
        connectTimeout: 10000, //milliseconds
        ackTimeout: 10000, //milliseconds
        channelPrefix: null,
        disconnectOnUnload: true,
        multiplex: true,
        autoReconnectOptions: {
            initialDelay: 10000, //milliseconds
            randomness: 10000, //milliseconds
            multiplier: 1.5, //decimal
            maxDelay: 60000 //milliseconds
        },
        authEngine: null,
        codecEngine: null,
        subscriptionRetryOptions: {},
    }
    onMessageFromUser = new EventEmitter<any>()
    onMessageFromPage = new EventEmitter<any>()
    onThreadChange = new EventEmitter<any>()
    onTaskChange = new EventEmitter<any>()
    onIncomingCall = new EventEmitter<any>()
    onRegisNotification = new EventEmitter<any>()
    clear() {
        // if (this.socket) {
        //     this.channels.forEach(c => {
        //         this.socket.destroyChannel(c.id)
        //     })
        //     this.channels = []
        //     this.log('deauthenticate connection')
        //     this.socket.deauthenticate()
        //     this.log('disconnect socket')
        //     this.socket.disconnect()
        //     this.socket = undefined
        // }
    }
    async getConnectionToken() {
        let setting = {
            method: 'GET',
            uri: this.apiUrl('connect'),
            headers: { //headers
                "content-type": "application/json",
                'access_token': await this.api.chatbotAuth.firebaseToken
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row.connectionToken
    }
    async login(token: string) {
        if (this.socket) {
            // this.log('destroy socket event', this.socketEvents)
            // this.socketEvents.forEach(event => {
            //     this.socket.off(event)
            // })
            // this.socketEvents = []
            this.log('destroy socket channel', this.channels)
            this.channels.forEach(c => {
                try {
                    c.instance.unwatch()
                    this.socket.destroyChannel(c.id)
                } catch(err) {
                    console.error(err)
                }
            })
            this.channels = []
            this.log('deauthenticate connection')
            this.socket.deauthenticate()
            this.log('disconnect socket')
            this.socket.disconnect()
            this.socket = undefined
        }
        this.socket = SocketClient.connect(this.connectionOption)
        this.socket.on('connect', (status) => this.onSocketConnect.bind(this)(status, token))
        this.socket.on('disconnect', (status) => this.onSocketDisconnect.bind(this)(status, token))

    }
    async onSocketDisconnect(status: ISocketConnectStatus, token: string) { }
    async onSocketConnect(status: ISocketConnectStatus, token: string) {
        const { authToken, id, isAuthenticated, pingTimeout } = status
        this.socket.emit('login', token, (err: any, payload: ISocketLoginCbPayload) => {
            if (err) {
                console.error('Socket Login Fail', err)
            } else {
                this.channels = []
                payload.subs.forEach(channelId => {
                    const topic = channelId.split('.')[1]
                    const channel = {
                        id: channelId,
                        name: topic,
                        instance: this.socket.subscribe(channelId, { waitForAuth: true })
                    }
                    this.channels.push(channel)
                    channel.instance.watch((message: ISocketChannelCbMessage) => this.onSubscribeChannel(message))
                })
            }
        })
    }

    async onSubscribeChannel(message: ISocketChannelCbMessage) {
        switch (message.type) {
            case this.CHANNEL_EVENTS.ON_REGIS_TESTER:
                this.alert.success('Đăng ký tester thành công', 'Bạn đã đăng ký làm tester thành công')
                break;
            case this.CHANNEL_EVENTS.ON_MESSAGE:
                if (this.api.chatbotAuth.app._id === message.appId) {
                    this.onMessageFromUser.next(message.payload)
                }
                break;
            case this.CHANNEL_EVENTS.ON_PAGE_ECHO:
                if (this.api.chatbotAuth.app._id === message.appId) {
                    this.onMessageFromPage.next(message.payload)
                }
                break;
            case this.CHANNEL_EVENTS.ON_PAGE_EXPIRED:
                break;
            case this.CHANNEL_EVENTS.ON_SUBSCRIBER_UPDATE:
                break;
            case this.CHANNEL_EVENTS.ON_TASK_CHANGE:
                const { appId, payload } = message
                if (this.api.chatbotAuth.app._id === appId) {
                    this.onTaskChange.emit(payload)
                }
                break;
            case this.CHANNEL_EVENTS.ON_THREAD_CHANGE:
                if (this.api.chatbotAuth.app._id === message.appId) {
                    this.onThreadChange.next(message.payload)
                }
                break;
            case this.CHANNEL_EVENTS.ON_CALL_REQUEST:
                this.onIncomingCall.next(message.payload)
                const accept = await this.alert.question('Cuộc gọi video đến', `${message.payload.client.messengerProfile.name} đang gọi bạn.\nBạn có chấp nhận không?`, 'Nhận cuộc gọi', 'Không')
                if (accept) {
                    var callWindow = window.open(message.payload.answer, "Video Call", "status=1, height=200, width=200, toolbar=0,resizable=0")
                    this.api.chatbotAuth.setStatus(message.payload.client._id)
                    const interval = setInterval(() => {
                        if (callWindow.closed) {
                            this.api.chatbotAuth.setStatus("online");
                            clearInterval(interval)
                        }
                    }, 5000)
                } else {
                    await this.api.chatbotAuth.setStatus("cancel." + message.payload.client._id)
                    this.api.chatbotAuth.setStatus("online")
                }
                break;
            case this.CHANNEL_EVENTS.ON_REGIS_NOTIFY: case this.CHANNEL_EVENTS.ON_REGIS_NOTIFY_DASH:
                await this.alert.success('Đăng ký nhận thông báo thành công', 'Bạn sẽ được nhận thông báo từ MCOM Notification')
                this.onRegisNotification.next(true)
                break
            default:
                console.log(message)
            break
        }
    }
}

export interface ISocketConnectStatus {
    authToken: string
    id: string
    isAuthenticated: boolean
    pingTimeout: number
}

export interface ISocketLoginCbPayload {
    subs: string[]
}

export interface ISocketChannel {
    id: string,
    name: string,
    instance: any
}

export interface ISocketChannelCbMessage {
    appId: string,
    type: string,
    payload: any
}