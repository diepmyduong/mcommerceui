import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'

export interface iEvoucherRemind extends iCrud {
    ignoreUsed?: boolean
    status?: string
    eVoucher?: string
    story?: string
    beforeDay?: number
    hour?: number
    page?: string
}

export class EvoucherRemind extends CrudAPI<iEvoucherRemind> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'eVoucherRemind')
    }
}