import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iApp } from 'app/services/chatbot/api/crud/app'
import { iSetting } from 'app/services/chatbot/api/crud/setting'
import { iStory } from 'app/services/chatbot/api/crud/story'
import { iAI } from 'app/services/chatbot/api/crud/ai'
import { BehaviorSubject } from 'rxjs'
import { iTopic, iTemplate, iInteractPost } from 'app/services/chatbot';
import { isObject, get, merge } from 'lodash-es'
export interface iPage extends iCrud {
    accessToken?: string
    app?: string | iApp
    pageId: string
    settings?: string[] | iSetting[]
    stories?: string[]
    status?: "active" | "deactive"
    tokenResetExpires?: number
    ai?: string | iAI
    aiState?: "active" | "deactive"
    meta?: any
    _id?: string
}

export interface iGetIntentsOption {
    local?: boolean
    reload?: boolean
    reloadLimit?: number
}

export interface iIntent {
    actions?: string[],
    contextIn?: any[],
    contextOut?: any[],
    events?: any[],
    fallbackIntent?: boolean,
    id: string,
    name: string,
    parameters?: any[],
    priority?: number
}

export interface iFanpage {
    access_token: string
    name: string
    id: string
    picture: { data: { url: string } }
    is_webhooks_subscribed: boolean
    [x: string]: any
}

export interface iGetAlbumsOption {
    local?: boolean
    reload?: boolean
    reloadLimit?: number
}

export interface iAlbum {
    cover_photo: {
        id: string,
        picture: string,
        name: string
    },
    id: string,
    name: string,
    photo_count: number
}

export interface iPhoto {
    id?: string,
    link?: string,
    images?: {
        height: number,
        source: string,
        width: number
    }[],
    preview?: {
        height: number,
        source: string,
        width: number
    }
}

export interface iVideo {
    content_category: string,
    created_time: Date,
    embeddable: boolean,
    id: string,
    picture: string,
    embed_html: string,
    from: {
        name: string,
        id: string
    }
    preview?: any,
    link?: string
}

export interface iGetPostsOption {
    local?: boolean,
    reload?: boolean,
    query?: {
        fields?: string,
        limit?: number,
        after?: string,
        before?: string,
        [key: string]: any
    }
}

export interface iFbPost {
    id: string,
    description?: string,
    full_picture?: string,
    picture?: string,
    caption?: string,
    message?: string,
    name?: string,
    story?: string,
    admin_creator?: {
        picture: {
            data: {
                url: string
            }
        },
        name: string
    },
    created_time: Date,
    link: string,
    isInteracted: boolean,
    interactPost?: iInteractPost
}

export class Page extends CrudAPI<iPage> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'page')
        this.options.query = {
            populates: ["settings", "ai"]
        }

    }
    intentOption: iGetIntentsOption = { local: true, reload: true, reloadLimit: 0 }
    intentsCache: {
        [hash: string]: {
            items: iIntent[]
            expired: Date
        }
    } = {}
    activeIntentHashRequest: string
    activeIntentRequest: any = {}
    albumsOption: iGetAlbumsOption = { local: true, reload: true, reloadLimit: 0 }
    albumsCache: {
        [hash: string]: {
            items: any[]
            expired: Date
        }
    } = {}
    activeAlbumsHashRequest: string
    activeAlbumsRequest: any = {}

    intents = new BehaviorSubject<iIntent[]>([])
    fanpages = new BehaviorSubject<iFanpage[]>([])
    albums = new BehaviorSubject<iAlbum[]>([])
    photos = new BehaviorSubject<iPhoto[]>([])
    videos = new BehaviorSubject<iVideo[]>([])

    enabledChatPluginPage = new BehaviorSubject<string>(undefined)

    get pageId() {
        return isObject(this.api.chatbotAuth.app.activePage) ? (this.api.chatbotAuth.app.activePage as iPage)._id : this.api.chatbotAuth.app.activePage as string
    }

    async enableChatPlugin(pageId: string, enable: boolean = true, option?: {
        minimized?: boolean,
        ref?: string
    }) {
        const div = document.createElement('div')
        div.className = 'fb-customerchat'
        const enabledChatPluginPage = this.enabledChatPluginPage.getValue()
        if (enable && (!enabledChatPluginPage || enabledChatPluginPage !== pageId)) {
            div.setAttribute('page_id', pageId)
            if (option && option.minimized) div.setAttribute('minimized', option.minimized.toString())
            if (option && option.ref) div.setAttribute('ref', option.ref.toString())
            this.enabledChatPluginPage.next(pageId)
            document.body.appendChild(div)
            return this.api.chatbotAuth.initFBService()
        } else if (!enable) {
            this.enabledChatPluginPage.next(undefined)
            document.body.appendChild(div)
            return this.api.chatbotAuth.initFBService()
        }
    }

    async getIntents(option?: iGetIntentsOption): Promise<iIntent[]> {
        option = merge({}, this.intentOption, option)
        let pageId = this.pageId
        let setting = {
            uri: this.apiUrl(`${pageId}/intents`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true // Automatically parses the JSON string in the response
        }
        const hashedRequest = this.hash(setting)
        this.activeIntentHashRequest = hashedRequest
        this.activeIntentRequest = setting
        if (option.local && this.intentsCache[hashedRequest] && this.intentsCache[hashedRequest].expired > new Date()) {
            let items = this.intentsCache[hashedRequest].items
            if (items.length > option.reloadLimit) { // If local empty, request to server
                return items
            }
        }
        let res = await this.exec(setting)
        let rows = res.results.object || []
        if (option.reload) {
            this.intentsCache[hashedRequest] = {
                items: rows,
                expired: this.moment().add(5, 'minutes').toDate()
            }
            this.intents.next(rows)
        }
        return rows;
    }

    async addStory(story: iStory) {
        let pageId = this.pageId
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${pageId}/stories`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: story
        }
        let res = await this.exec(setting)
        let row = res.results.object as iStory;
        // let items = this.api.story.items.getValue()
        // items.push(row)
        // this.api.story.items.next(items)
        return row
    }

    async addSetting(settingData: iSetting) {
        let pageId = this.pageId
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${pageId}/settings`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: settingData
        }
        let res = await this.exec(setting)
        let row = res.results.object as iSetting;
        let items = this.api.setting.items.getValue()
        items.push(row)
        this.api.setting.items.next(items)
        return row
    }

    async activeSetting(settingId: string) {
        let pageId = this.pageId
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`${pageId}/settings/${settingId}`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object as iSetting;
        return row
    }
    async deActiveSetting(settingId: string) {
        let pageId = this.pageId
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl(`${pageId}/settings/${settingId}`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object as iSetting;
        return row
    }
    async activePage() {
        let pageId = this.pageId
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`${pageId}/active`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object as any;
        this.getItem(pageId, { local: false, reload: true })
        return row
    }
    async deactivePage() {
        let pageId = this.pageId
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl(`${pageId}/active`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object as any;
        this.getItem(pageId, { local: false, reload: true })
        return row
    }
    async getFanpages(userToken: string, option: iGetIntentsOption = {
        local: true, reload: true
    }) {
        let setting = {
            uri: this.apiUrl(`getFanpages`),
            headers: { //headers
                
                'access_token': userToken
            },
            json: true // Automatically parses the JSON string in the response
        }
        if (option.local) {
            let fanpages = this.fanpages.getValue()
            if (fanpages.length > 0) {
                return fanpages
            }
        }
        let res = await this.exec(setting)
        let rows = res.results.object.data || []
        if (option.reload) this.fanpages.next(rows)
        return rows;
    }
    async setAi(params: {
        type: "default" | "custom",
        name?: string,
        description?: string,
        devToken?: string,
        clientToken?: string,
        version?: string,
        aiId?: string
    }) {
        let pageId = this.pageId
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${pageId}/setAi`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            body: params,
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        let page = await this.getItem(pageId, { local: false, reload: true })
        return page.ai as iAI
    }
    async changeAi(ai: any) {
        let pageId = this.pageId
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`${pageId}/changeAi`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            body: ai,
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        this.getItem(pageId, { local: false, reload: true })
        return row
    }
    async setAIState(state: "active" | "deactive") {
        let pageId = this.pageId
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`${pageId}/aiState`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true,
            body: { state }
        }
        let res = await this.exec(setting)
        let row = res.results.object as iPage
        this.getItem(pageId, { local: false, reload: true })
        return row
    }
    async getPageMailBox() {
        let pageId = this.pageId
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${pageId}/mailbox`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async getMessengerCode() {
        let pageId = this.pageId
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${pageId}/mscode`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async updateMessengerCode(image: string) {
        let pageId = this.pageId
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${pageId}/mscode`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            body: image,
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async exportInfo() {
        let pageId = this.pageId
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${pageId}/export`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async importInfo(data: any) {
        let pageId = this.pageId
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${pageId}/export`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            body: data,
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        this.getItem(pageId, { local: false, reload: true })
        return row
    }
    async getFanpageAlbums(option?: iGetAlbumsOption) {
        option = merge({}, this.albumsOption, option)
        let pageId = this.pageId
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${pageId}/albums`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true,
        }
        const hashedRequest = this.hash(setting)
        this.activeAlbumsHashRequest = hashedRequest
        this.activeAlbumsRequest = setting
        if (option.local && this.albumsCache[hashedRequest] && this.albumsCache[hashedRequest].expired > new Date()) {
            let items = this.albumsCache[hashedRequest].items
            if (items.length > option.reloadLimit) { // If local empty, request to server
                return items
            }
        }
        let res = await this.exec(setting)
        let rows = res.results.object.albums.data || []
        if (option.reload) {
            this.albumsCache[hashedRequest] = {
                items: rows,
                expired: this.moment().add(5, 'minutes').toDate()
            }
            this.albums.next(rows)
        }
        return rows;
    }
    async getFanpageAlbumPhotos(albumId: string, option?: iGetAlbumsOption) {
        option = merge({}, this.albumsOption, option)
        let pageId = this.pageId
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${pageId}/albumPhotos`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true,
            body: {
                albumId: albumId
            }
        }
        const hashedRequest = this.hash(setting)
        this.activeAlbumsHashRequest = hashedRequest
        this.activeAlbumsRequest = setting
        if (option.local && this.albumsCache[hashedRequest] && this.albumsCache[hashedRequest].expired > new Date()) {
            let items = this.albumsCache[hashedRequest].items
            if (items.length > option.reloadLimit) { // If local empty, request to server
                return items
            }
        }
        let res = await this.exec(setting)
        let rows = get(res,'results.object.photos.data') || []
        if (option.reload) {
            this.albumsCache[hashedRequest] = {
                items: rows,
                expired: this.moment().add(5, 'minutes').toDate()
            }
            this.photos.next(rows)
        }
        return (rows as iPhoto[])
    }
    async getFanpagePhotoSource(photoId: string) {
        let pageId = this.pageId
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${pageId}/photoSource`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true,
            body: {
                photoId: photoId
            }
        }
        let res = await this.exec(setting)
        let row = get(res,'results.object.photos.data');
        return row
    }
    async getFanpageVideos(option?: iGetAlbumsOption) {
        option = merge({}, this.albumsOption, option)
        let pageId = this.pageId
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${pageId}/videos`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true,
        }
        const hashedRequest = this.hash(setting)
        this.activeAlbumsHashRequest = hashedRequest
        this.activeAlbumsRequest = setting
        if (option.local && this.albumsCache[hashedRequest] && this.albumsCache[hashedRequest].expired > new Date()) {
            let items = this.albumsCache[hashedRequest].items
            if (items.length > option.reloadLimit) { // If local empty, request to server
                return items
            }
        }
        let res = await this.exec(setting)
        let rows = res.results.object.data as iVideo[] || []
        if (option.reload) {
            this.albumsCache[hashedRequest] = {
                items: rows,
                expired: this.moment().add(5, 'minutes').toDate()
            }
            this.videos.next(rows)
        }
        return rows;
    }
    async setGetStartedStories(storyIds: string[]) {
        let pageId = this.pageId
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`${pageId}/setGetStartedStories`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true,
            body: { storyIds }
        }
        let res = await this.exec(setting)
        return res.results.object
    }
    async addTopic(topic: iTopic) {
        let pageId = this.pageId
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${pageId}/topics`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true,
            body: topic
        }
        let res = await this.exec(setting)
        let row = res.results.object as iTopic;
        let items = this.api.topic.items.getValue()
        items.push(row)
        this.api.topic.items.next(items)
        return row
    }

    async getPosts(option?: iGetPostsOption) {
        option = merge({}, this.albumsOption, option)
        let pageId = this.pageId
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${pageId}/posts`),
            qs: option.query,
            headers: {
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true
        }
        this.log('request setting', setting)
        const hashedRequest = this.hash(setting)
        this.activeRequestHash = hashedRequest
        this.activeRequestSetting = setting
        if (option.local && this.requestCache[hashedRequest] && this.requestCache[hashedRequest].expired > new Date()) {
            return this.requestCache[hashedRequest]
        }
        let res = await this.exec(setting)
        let rows = res.results.object.data || []
        if (option.reload) {
            this.requestCache[hashedRequest] = {
                items: rows as iFbPost[],
                expired: this.moment().add(5, 'minutes').toDate(),
                paging: res.results.object.paging
            }
        }
        return {
            items: rows as iFbPost[],
            paging: res.results.object.paging
        }
    }
    
    async createTemplate(params: {
        name: string,
        description: string,
        image?: string,
        preview?: string,
        author?: string,
        environmentConfig?: any
    }) {
        let pageId = this.pageId
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${pageId}/template`),
            headers: {
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
            },
            body: params,
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object as iTemplate;
        this.api.template.getList({ local: false })
        return row
    }

    async activateSettings(settingId) {
        let pageId = this.pageId
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`${pageId}/settings/${settingId}`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object as iSetting;
        let items = this.api.setting.items.getValue()
        items.push(row)
        this.api.setting.items.next(items)
        return row
    }

    async deactivateSettings(settingId) {
        let pageId = this.pageId
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl(`${pageId}/settings/${settingId}`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object as iSetting;
        let items = this.api.setting.items.getValue()
        items.push(row)
        this.api.setting.items.next(items)
        return row
    }
    async addQuickMessage(message: string, storyId: string) {
        let pageId = this.pageId
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${pageId}/quickMessages`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true,
            body: { 
                message: message,
                type: "story",
                option: {
                    storyId: storyId
                }
             }
        }
        let res = await this.exec(setting)
        return res.results.object
    }
    async getFanpageInfo() {
        let pageId = this.pageId
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${pageId}/info`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true,
        }
        let res = await this.exec(setting)
        return res.results.object
    }
    async getSubscriber(after:string) {

      let pageId = this.pageId
      let setting = {
          method: 'GET',
          uri: this.apiUrl(`${pageId}/mailbox`),
          qs: {
            after: after
          },
          headers: { //headers
              
              'x-api-key': this.api.chatbotAuth.appToken,
               
          },
          json: true,
      }
      let res = await this.exec(setting)
      return res.results.object
  }
  async getLabels(options?: crudOptions) {
    let defaultOptions = {
      reload: true, //Auto update items list each request, 'false' to get result only.
      local: true, //Get local data instant request server.
      reloadLimit: 1,
      query: {}
  }
  options = merge({}, defaultOptions, options)
    let pageId = this.pageId
    let setting = {
        method: 'GET',
        uri: this.apiUrl(`${pageId}/customLabels`),
        qs: this._paserQuery(options.query),
        headers: { //headers
            
            'x-api-key': this.api.chatbotAuth.appToken,
             
        },
        json: true,
    }
    let res = await this.exec(setting)
    return res.results.object
  }
  async getLabel(label_id) {
    let pageId = this.pageId
    let setting = {
        method: 'GET',
        uri: this.apiUrl(`${pageId}/labels/${label_id}`),
        headers: { //headers
            
            'x-api-key': this.api.chatbotAuth.appToken
        },
        json: true,
    }
    let res = await this.exec(setting)
    let row = res.results?res.results.object:res.results
    return row
  }
  async createTemplateApp(pageId,app_id,app_token,params: {
    name: string,
    description: string,
    image?: string,
    preview?: string,
    author?: string,
    environmentConfig?: any
  }) {
      let setting = {
          method: 'POST',
          uri: this.apiUrl(`${pageId}/template`),
          headers: {
              
              "content-type": "application/json",
              'app_id': app_id,
              'app_token': app_token,
          },
          body: params,
          json: true
      }
      let res = await this.exec(setting)
      let row = res.results.object as iTemplate;
     // this.api.template.getList({ local: false })
      return row
  }
}