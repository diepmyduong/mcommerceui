import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'

export interface iLicenseCode extends iCrud {
    code?: string
    saleOf?: number
}

export class LicenseCode extends CrudAPI<iLicenseCode> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'licenseCode')
        this.options.query = {
        }
    }

    async create(token, code, saleoff) {        
        let setting = {
            method: 'POST',
            uri: this.apiUrl(``),
            headers: {
                
                "content-type": "application/json",
                'x-api-key': token,
            },
            body: {
                code, saleOf: saleoff
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
}