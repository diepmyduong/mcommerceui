import { CrudAPI, crudOptions, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import * as hash from 'object-hash'
import * as moment from 'moment'
import { cloneDeep } from 'lodash-es'

export interface iSystemUser extends iCrud {
    email?: string
    name?: string
    rule?: 'system_admin' | 'system_agency'
    status?: string
    token?: string
}

export class SystemUser extends CrudAPI<iSystemUser> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'systemUser')
    }

    async login(token) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`login`),
            headers: {
                
                "content-type": "application/json",
                'access_token': token,
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async getInviteAgencyToken(adminToken) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`inviteAgency`),
            headers: {
                
                "content-type": "application/json",
                'x-api-key': adminToken,
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async acceptAgencyInvitation(firebaseToken, inviteToken) {        
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`acceptAgencyInvite`),
            headers: {
                
                "content-type": "application/json",
                'access_token': firebaseToken
            },
            body: { inviteToken },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async upgrade(appId, adminToken) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`license/upgrade`),
            headers: {
                
                "content-type": "application/json",
                'x-api-key': adminToken,
            },
            body: { appId, packageLicense: 'basic' },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async getApps(options: crudOptions) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`app`),
            headers: {
                
                "content-type": "application/json",
                ...options.headers
            },
            qs: this._paserQuery(options.query),
            json: true
        }
        
        const hashedQuery = hash(options.query)
        this.activeHashQuery = hashedQuery
        this.activeQuery = options.query
        if (options.local
            && this.hashCache[hashedQuery] && this.hashCache[hashedQuery].expired > new Date()
            && this.hashCache[hashedQuery].isGetList) {
            let items = this.hashCache[hashedQuery].items
            return items
        }
        let res = await this.exec(setting)
        let row = res.results.object
        this.hashCache[hashedQuery] = {
            pagination: this.pagination,
            items: cloneDeep(row),
            expired: moment().add(5, 'minutes').toDate(),
            isGetList: true
        }
        return row
    }

    async getExportAppsDownloadURL(token: string, crudQuery: any) {
        const url = new URL(this.apiUrl(`app/export.csv`));
        url.searchParams.set('x-api-key', token);
        const query = this._paserQuery(crudQuery);
        for(let k of Object.keys(query)) {
            if (query[k]) url.searchParams.set(k, query[k]);
        }
        return url.href;
    }

    async getLicenseApps(adminToken, options: crudOptions) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`appUseLicenseCode`),
            headers: {
                
                "content-type": "application/json",
                'x-api-key': adminToken,
            },
            qs: this._paserQuery(options.query),
            json: true
        }
        
        const hashedQuery = hash(options.query)
        this.activeHashQuery = hashedQuery
        this.activeQuery = options.query
        if (options.local
            && this.hashCache[hashedQuery] && this.hashCache[hashedQuery].expired > new Date()
            && this.hashCache[hashedQuery].isGetList) {
            let items = this.hashCache[hashedQuery].items
            return items
        }
        let res = await this.exec(setting)
        let row = res.results.object
        this.hashCache[hashedQuery] = {
            pagination: this.pagination,
            items: cloneDeep(row),
            expired: moment().add(5, 'minutes').toDate(),
            isGetList: true
        }
        return row
    }

    async whiteList(appId, adminToken) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`whitelistApps`),
            headers: {
                
                "content-type": "application/json",
                'x-api-key': adminToken,
            },
            body: { apps: [appId] },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async unwhiteList(appId, adminToken) {
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl(`whitelistApps`),
            headers: {
                
                "content-type": "application/json",
                'x-api-key': adminToken,
            },
            body: { apps: [appId] },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
}