import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'

import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { iTopic } from 'app/services/chatbot/api/crud/topic';
import { iSubscriber } from 'app/services/chatbot';

export interface iTopicSubscriber extends iCrud {
    topic?: string | iTopic
    subscriber?: string | iSubscriber
    page?: string | iPage
}

export class TopicSubscriber extends CrudAPI<iTopicSubscriber> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'topicSubscriber')
    }
}