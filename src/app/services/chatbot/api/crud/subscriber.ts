import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { merge } from 'lodash-es'
import * as hash from 'object-hash'
import * as moment from 'moment'

export interface iSubscriber extends iCrud {
    messengerProfile?: {
        first_name: string
        gender: string,
        id: string,
        last_name: string,
        locale: string,
        timezone: number,
        profile_pic: string
        lastUpdate: any
        name: string
    },
    innowayId?: string
    innowayProfile?: any
    page?: string | iPage
    phone?: string
    uid?: string,
    createdAt?: any,
    updatedAt?: any,
    otherInfo?: any,
}

export interface iGetThreadInfoOption {
    local?: boolean,
    reload?: boolean,
    query?: {
        fields?: string,
        limit?: number,
        after?: string,
        before?: string,
        [key: string]: any
    }
}

export interface iGetThreadMessagesOption extends iGetThreadInfoOption { }

export interface iThreadInfo {
    message_count: number,
    participants: {
        data: iThreadSenderInfo[]
    },
    is_subscribed: boolean,
    unread_count: number,
    can_reply: boolean,
    link: string,
    id: string,
    updated_time: Date,
    snippet: string,
    former_participants: {
        data: any[]
    },
    senders: {
        data: iThreadSenderInfo[]
    },
    pagePicture: string,
    subscriberPicture: string,
}
export interface iThreadMessage {
    message?: any,
    from?: iThreadSenderInfo,
    to?: {
        data: iThreadSenderInfo[]
    },
    created_time?: string,
    id?: string,
    tags?: {
        data: { name: string }[]
    },
    attachments?: any,
    avatar?: string,
    isFirst?: boolean,
    isLast?:boolean
    waiting?:boolean
}
export interface iThreadSenderInfo {
    name: string,
    email: string,
    id: string
}

export class Subscriber extends CrudAPI<iSubscriber> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'subscriber')
        this.threadInfoOption = this.threadMessagesOption = {
            local: true, reload: true, query: {
                limit: 20
            }
        }
    }
    threadInfoOption: any
    threadMessagesOption: any
    threadMessages: any = {}

    async getThreadInfo(subscriberId: string, option?: iGetThreadInfoOption) {
        option = merge({}, this.threadInfoOption, option)
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${subscriberId}/threadInfo`),
            qs: option.query,
            headers: {
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
            },
            json: true
        }
        const hashedRequest = this.hash(setting)
        this.activeRequestHash = hashedRequest
        this.activeRequestSetting = setting
        if (option.local && this.requestCache[hashedRequest] && this.requestCache[hashedRequest].expired > new Date()) {
            return this.requestCache[hashedRequest].threadInfo
        }
        let res = await this.exec(setting)
        let threadInfo = res.results.object as iThreadInfo
        if (option.reload) {
            this.requestCache[hashedRequest] = {
                items: [],
                expired: this.moment().add(5, 'minutes').toDate(),
                threadInfo
            }
        }
        return threadInfo
    }
    async getThreadMessages(subscriberId: string, option?: iGetThreadMessagesOption) {
        option = merge({}, this.threadInfoOption, option)
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${subscriberId}/threadMessages`),
            qs: option.query,
            headers: {
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true
        }
        const hashedRequest = this.hash(setting)
        this.activeRequestHash = hashedRequest
        this.activeRequestSetting = setting
        // this.log("activeRequestSetting", this.activeRequestSetting)
        // this.log('request cache', this.requestCache)
        // this.log('hashedRequest', hashedRequest)
        // if (option.local && this.requestCache[hashedRequest] && this.requestCache[hashedRequest].expired > new Date()) {
        //     // return this.threadMessages[subscriberId];
        //     return this.requestCache[hashedRequest]
        // }
        let res = await this.exec(setting)
        let rows = res.results.object.data || []
        if (option.reload) {
            this.requestCache[hashedRequest] = {
                items: rows as iThreadMessage[],
                expired: this.moment().add(5, 'minutes').toDate(),
                paging: res.results.object.paging
            }
        }
        return {
            items: rows as iThreadMessage[],
            paging: res.results.object.paging
        }
    }

    async syncLabels() {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`syncLabels`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results?res.results.object:[]
        return row
    }

    async query(options: crudOptions, filter) {
        options = merge({}, this.options, options)
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`query`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
            },
            json: true,
            qs: this._paserQuery(options.query),
            body: filter
        }
        const hashedQuery = hash(setting)
        this.activeHashQuery = hashedQuery
        this.activeQuery = options.query
        if (options.local && this.localAppId == this.api.chatbotAuth.app._id
            && this.hashCache[hashedQuery] && this.hashCache[hashedQuery].expired > new Date()
            && this.hashCache[hashedQuery].isGetList) {
            let items = this.hashCache[hashedQuery].items
            this.pagination = this.hashCache[hashedQuery].pagination
            return items
        }
        if (this.localAppId != this.api.chatbotAuth.app._id)
            this.localAppId = this.api.chatbotAuth.app._id
        let res = await this.exec(setting)
        let { results, pagination } = res
        let rows = results.objects.rows as iSubscriber[]
        if (options.reload) {
            this.pagination = pagination
            this.pagination.totalItems = results.objects.count || 0
            this.hashCache[hashedQuery] = {
                pagination: this.pagination,
                items: rows,
                expired: moment().add(5, 'minutes').toDate(),
                isGetList: true
            }
            this.items.next(rows)
        }
        return rows;
    }

    async filter(options: crudOptions, paramQuery: any) {
        options = merge({}, this.options, options)
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`filter`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
            },
            json: true,
            qs: this._paserQuery(options.query),
            body: paramQuery
        }
        const hashedQuery = hash(setting)
        this.activeHashQuery = hashedQuery
        this.activeQuery = options.query
        if (options.local && this.localAppId == this.api.chatbotAuth.app._id
            && this.hashCache[hashedQuery] && this.hashCache[hashedQuery].expired > new Date()
            && this.hashCache[hashedQuery].isGetList) {
            let items = this.hashCache[hashedQuery].items
            this.pagination = this.hashCache[hashedQuery].pagination
            return items
        }
        if (this.localAppId != this.api.chatbotAuth.app._id)
            this.localAppId = this.api.chatbotAuth.app._id
        let res = await this.exec(setting)
        let { results, pagination } = res
        let rows = results.objects.rows as iSubscriber[]
        if (options.reload) {
            this.pagination = pagination
            this.pagination.totalItems = results.objects.count || 0
            this.hashCache[hashedQuery] = {
                pagination: this.pagination,
                items: rows,
                expired: moment().add(5, 'minutes').toDate(),
                isGetList: true
            }
            this.items.next(rows)
        }
        return rows;
    }

}