import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iSubscriber } from './subscriber';
import { iLuckyWheelGift } from './luckyWheel';
import { iPage } from 'app/services/chatbot';

export interface iMembership extends iCrud {
    phone: string
    page: string | iPage
    subscriber?: string | iSubscriber
    score: number
}

export class Membership extends CrudAPI<iMembership> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'membership')
        this.options.query = {
        }
    }

    async import(file: any) {
        try {
            const options = {
                uri: this.apiUrl('import'),
                method: "POST",
                headers: {
                    
                    'x-api-key': this.api.chatbotAuth.appToken
                },
                formData: {
                    file: {
                        value: file,
                        options: {
                            contentType: 'text/csv'
                        }
                    }
                },
                json: true // Automatically parses the JSON string in the response
            };
            const res = await this.exec(options);
            return res
        } catch (err) {
            throw (err)
        }
    }
    
}