import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { iStory } from 'app/services/chatbot/api/crud/story'

export interface iCard extends iCrud {
    option?: any
    page?: string | iPage
    type?: "action" | "text" | "image" | "audio" | "video" | "file" | "generic" | "button" | "list" | "receipt" | "generic_categories" | "generic_products" | "generic_promotions" | "generic_promotion" | "innoway_receip" | "generic_dynamic" | "list_products" | "list_categories" | "list_promotions" | "media_video" | "media_image" | "activity_input" | "activity_quickreply" | "activity_button" | "activity_range" | "activity_qrcode" | "activity_media" | "activity_dynamic_quickreply" | "activity_dynamic_form" | "action_email" | "action_api" | "action_innoway" | "action_update" | "action_json_input" | "action_parse_html" | "action_notify" | "action_innoway_api" | "action_compare_image" | "typing_on" | "action_compare_image" | "action_google_spreadsheet" | "og_video_call" | "go_to_story"|"action_add_label"|"action_send_story"|"action_sequence",
    story?: string | iStory,
    condition?: any,
    isBreak?: boolean,
    note?: string
}

export class Card extends CrudAPI<iCard> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'card')
    }

    async duplicate(cardId: string, options: crudOptions = {
        reload: true
    }) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${cardId}/duplicate`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object as iCard
        if (options.reload) {
            let items = this.items.getValue()
            items.push(row)
            this.items.next(items)
        }
        return row
    }

    async build(cardId: string) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${cardId}/build`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        return res.results.object
    }
    
    async copy(app_id, app_token , data: { destinationStoryId: string, cardId: string }) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`copy`),
            headers: { //headers
                
                "content-type": "application/json",
                'app_id': app_id,
                'app_token': app_token,
            },
            body: data,
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object as any
        return row
    }

    popularCards: any = {}
    async getPopular() {
        let appId = this.api.chatbotAuth.app._id
        if (this.popularCards[appId]) return this.popularCards[appId]
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`popular`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object as any
        this.popularCards[appId] = row
        return row
    }
}