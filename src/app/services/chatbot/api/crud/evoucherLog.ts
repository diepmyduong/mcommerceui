import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iEvoucher } from './evoucher';
import { iEvoucherCode } from './evoucherCode';
import { iPage } from './page';
import { iSubscriber } from './subscriber';

export interface iEvoucherLog extends iCrud {
    activeDate?: string
    eVoucher?: any | iEvoucher,
    eVoucherCode?: string | iEvoucherCode,
    page?: string | iPage,
    subscriber?: string | iSubscriber,
}

export class EvoucherLog extends CrudAPI<iEvoucherLog> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'eVoucherLog')
        this.options.query = {
        }
    }
}