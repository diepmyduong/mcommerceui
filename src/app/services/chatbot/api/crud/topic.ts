import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { iTopicEvent } from 'app/services/chatbot/api/crud/topicEvent';
import { iGroup } from '../..';

export interface iTopic extends iCrud {
    name?: string
    intro?: string
    image?: string
    groups?: string[] | iGroup[]
    page?: string | iPage
}

export class Topic extends CrudAPI<iTopic> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'topic')
    }
    async addEvent(topicId: string, event: iTopicEvent) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${topicId}/events`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: event
        }
        let res = await this.exec(setting)
        let row = res.results.object as iTopicEvent
        return row
    }
    async addGroupSubscriberToTopic(topicId: string, groupId: string[]) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${topicId}/group`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            body: { groupId },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async addSubscribersToTopic(topicId: string, subscriberIds: string[]) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${topicId}/subscribers`),
            headers: { //headers 
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            body: { subscriberIds },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async deleteGroupSubscribersToTopic(topicId: string, groupId: string[]) {
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl(`${topicId}/group`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            body: { groupId },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async deleteSubscribersToTopic(topicId: string, subscriberIds: string[]) {
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl(`${topicId}/subscribers`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            body: { subscriberIds },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async getSubscribersOfTopic(topicId: string) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${topicId}/subscribers`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
}