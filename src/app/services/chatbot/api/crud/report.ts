import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service';

export interface iReport extends iCrud {
    
}

export class Report extends CrudAPI<iReport> {
    constructor(
        public api: ChatbotApiService
    ){
        super(api,'report')
    }
    
    async chartStatisticPostsReport(params) {
        let setting = {
        method: 'GET',
        uri: this.apiUrl(`getData`),
        headers: { //headers            
            'x-api-key': this.api.chatbotAuth.appToken,
        },
        json: true,
        qs: params
        }
        let res: any = await this.exec(setting);
        let rows = res.results.object
        return rows;
    }
}