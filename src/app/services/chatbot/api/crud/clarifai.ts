import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { iStory } from 'app/services/chatbot/api/crud/story'
import { iApp } from 'app/services/chatbot';

export interface iClarifai extends iCrud {
    name: string,
    apiKey: string,
    status: "active" | "deactive"
}

export class Clarifai extends CrudAPI<iClarifai> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'clarifai')
    }

   
}