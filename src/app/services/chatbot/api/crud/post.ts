import { CrudAPI, iCrud } from '../crud'
import { ChatbotApiService } from '../../chatbot-api.service';

export interface iPost extends iCrud {
}

export class Post extends CrudAPI<iPost> {
    constructor(
        public api: ChatbotApiService
    ){
        super(api,'post')
    }

    
    async syncPosts(){
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`syncPosts`),
            headers: {
                'x-api-key': this.api.chatbotAuth.appToken,
            },
            json: true
        }
        let res = await this.exec(setting)
        return res.results.object
    }
}