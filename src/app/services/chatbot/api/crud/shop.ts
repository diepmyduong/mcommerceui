import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from './page';
import axios, { AxiosRequestConfig } from 'axios'

export interface iShop extends iCrud {
  page?: iPage
  name?: string
  image?: string
  shipFeePerKilometer?: number
  status?: 'active' | 'deactive'
  forwardStory?: string
  forwardGSheet?: boolean; // Có Ghi Dữ liêu order qua google sheet không ?
  gSheetUser?: string; // Nếu có chọn tài khoản google. => Tương tụ card GGSheet
  gSheetId?: string; // Id của sheet muốn ghi vào => Cho phép tạo mới hoặc mơ sheet
  orderSheetName?: string; // Tên sheet ghi nhận dữ liệu Đơn hàng
  bookingSheetName?: string; // Tên sheet ghi nhận booking
  bkAmount?: number
  bkDurations?: number[]
  forwardBookingStory?: string
  bkDateOfWeek?: any
  places?: {
    _id?: string
    name?: string
    image?: string
    fullAddress?: string
    long?: number
    lat?: number
    province?: string
    district?: string
  }[]
}

export class Shop extends CrudAPI<iShop> {
    constructor(
      public api: ChatbotApiService
    ){
      super(api,'shop')
    }

    async exportExcel() {
      let setting: AxiosRequestConfig = {
        method: 'GET',
        url: this.apiUrl(`exportExcel`),
        headers: { 
          'x-api-key': this.api.chatbotAuth.appToken,
        },
        responseType: 'blob'
      }
      let res = (await axios(setting)).data
      return res
    }

    async importExcel(file) {
      let formData = new FormData()
      formData.append('data', file)
      let setting: AxiosRequestConfig = {
        method: 'POST',
        url: this.apiUrl(`importExcel`),
        headers: { 
          'x-api-key': this.api.chatbotAuth.appToken,
          'Content-Type': 'multipart/form-data'
        },
        data: formData
      }
      let res = (await axios(setting)).data
      return res
    }
}