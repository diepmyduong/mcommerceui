import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { BehaviorSubject } from 'rxjs';
import { iEnvironment } from 'app/services/chatbot/api/crud/environment';
import * as QueryString from 'query-string';
import { merge, remove } from 'lodash-es'
import * as moment from 'moment'
import * as hash from 'object-hash'
import axios, { AxiosRequestConfig } from 'axios'
export interface iPartner {
    partner: "innoway"
    partnerId: string,
    partnerToken?: string,
    partnerSecret?: string,
    meta?: any
}

export interface iUser {
    uid: string,
    rule: "admin" | "read" | "write"
}

export interface iApp extends iCrud {
    activePage?: string | iPage,
    activeZaloOA?: any
    accessToken?: string,
    license?: any,
    environment?: string | iEnvironment,
    expireDate?: Date,
    owner?: string,
    users?: iUser[],
    name?: string,
    pageTokenExpired?: boolean,
    partners?: iPartner[],
    whiteListed?: boolean,
    settings?: {
        email?: {
            emailHost: string,
            emailPort: string,
            emailUser: string,
            emailPass: string
        },
        saveQuickOrderGoogleSheet?: {
            uri: string
        },
        quickOrderResponse?: {
            handleChangePhone?: string,
            handleChangePhoneError?: string,
            handleLocationDontSupport?: string,
            handleChangeLocation?: string,
            handleRemoveProductSuccess?: string,
            handleOrderSuccess?: string,
            handleOrderError?: string,
            confirmDeliveryInfo?: string,
            handleChangeProductAmount?: string,
            handleViewBasketItemsEmpty?: string
        }
    }
}

export interface iIWCategory {
    id?: string,
    brand_id?: string,
    description?: string,
    thirdparty_id?: string,
    image?: string,
    name?: string,
    parent_id?: string,
    status?: number,
    short_description?: string,
    created_at?: Date,
    updated_at?: Date
}

export class App extends CrudAPI<iApp> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'app')
        this.options.query = {
            limit: 0,
            populates: [{
                path: "activePage",
                select: "status name meta pageId"
            }, "license"]
        }
    }
    innowayCategories = new BehaviorSubject<iIWCategory[]>([])

    async updateOwner(id, userId) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${id}/updateOwner`),
            headers: { //headers
                'x-api-key': this.api.chatbotAuth.systemUser.token
            },
            json: true,
            body: {
                userId
            }
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async inviteToApp(id, customerId, rule) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${id}/inviteToApp`),
            headers: { //headers
                'x-api-key': this.api.chatbotAuth.systemUser.token
            },
            json: true,
            body: {
                customerId, rule
            }
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async exportExcelAllSubscribers(id) {
        let setting: AxiosRequestConfig = {
          method: 'GET',
          url: this.apiUrl(`${id}/exportExcelAllSubscribers`),
          headers: { 
            'x-api-key': this.api.chatbotAuth.systemUser.token
          },
          responseType: 'blob'
        }
        let res = (await axios(setting)).data
        return res
    }

    async getSecret() {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${this.api.chatbotAuth.app._id}/secret`),
            headers: { //headers
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row.secret
    }

    async getList(options?: crudOptions) {
        let access_token
        if (options.headers && options.headers.access_token) {
            access_token = options.headers.access_token
        } else {
            access_token = await this.api.chatbotAuth.firebaseToken
        }
        options = merge({}, this.options, options)
        let setting = {
            uri: this.apiUrl(),
            qs: this._paserQuery(options.query),
            headers: { //headers
                'access_token': access_token
            },
            json: true // Automatically parses the JSON string in the response
        }
        const hashedQuery = hash(options.query)
        if (options.local && this.hashCache[hashedQuery] && this.hashCache[hashedQuery].expired > new Date()
        && this.hashCache[hashedQuery].isGetList) {
            let items = this.hashCache[hashedQuery].items
            this.pagination = this.hashCache[hashedQuery].pagination
            return items
        }
        let res = await this.exec(setting)
        let { results, pagination } = res
        let rows = results.objects.rows as iApp[]
        if (options.reload) {
            this.pagination = pagination
            this.pagination.totalItems = results.objects.count || 0
            this.hashCache[hashedQuery] = {
                pagination: this.pagination,
                items: rows,
                expired: moment().add(5, 'minutes').toDate(),
                isGetList: true
            }
            this.items.next(rows)
        }
        return rows;
    }

    async getItem(id: string, options?: crudOptions) {
        if (!id) throw new Error("id undefined in getItem")
        options = merge({}, this.options, options)
        let setting = {
            method: 'GET',
            uri: this.apiUrl(id),
            qs: this._paserQuery(options.query),
            headers: { //headers
                'access_token': await this.api.chatbotAuth.firebaseToken
            },
            json: true // Automatically parses the JSON string in the response
        }
        if (options.local) {
            let items = this.items.getValue()
            let item = items.find(x => x._id == id)
            if (item) return item
        }
        let res = await this.exec(setting)
        let row = res.results.object as iApp
        if (options.reload) {
            let items = this.items.getValue()
            let index = items.findIndex(x => x._id == id)
            if (index > -1) {
                items[index] = row
            } else {
                items.push(row)
            }
            this.items.next(items)
        }
        return row
    }

    async add(data: iApp, options?: crudOptions) {
        if (!data) throw new Error('data undefined in add')
        options = merge({}, this.options, options)
        let setting = {
            method: 'POST',
            uri: this.apiUrl(),
            qs: this._paserQuery(options.query),
            headers: { //headers
                
                "content-type": "application/json",
                'access_token': await this.api.chatbotAuth.firebaseToken
            },
            body: data,
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object
        if (options.reload) {
            let items = this.items.getValue()
            items.unshift(row)
            this.items.next(items)
        }
        return row
    }

    async subscribePage(params: {
        appId: string, appToken: string,
        userToken: string, pageToken: string,
        // aiId: string
    }) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl('subscribePage'),
            headers: { //headers
                
                'app_id': params.appId,
                'app_token': params.appToken
            },
            body: {
                pageToken: params.pageToken,
                userToken: params.userToken,
                // ai: {
                //     type: "default",
                //     aiId: params.aiId
                // }
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object
        //this.getItem(params.appId, { reload: true, local: false })
        return row
    }

    async unsubscribePage(params: {
        appId: string, appToken: string
    }) {
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl('subscribePage'),
            headers: { //headers
                
                'app_id': params.appId,
                'app_token': params.appToken
            },
            json: true // Automatically parses the JSON string in the response
        }
        try {
            let res = await this.exec(setting)
            let row = res.results.object
            this.getItem(params.appId, { reload: true, local: false })
            return row
        } catch (err) {
            this.getItem(params.appId, { reload: true, local: false })
            throw err
        }
    }

    async refeshPageToken(params: {
        appId: string, appToken: string, userToken: string
    }) {
        const { appId, appToken, userToken } = params
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`${appId}/refeshPageToken`),
            headers: {
                
                'app_id': appId,
                'app_token': appToken
            },
            body: { userToken },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        this.getItem(params.appId, { reload: true, local: false })
        return row
    }

    async invite(params: {
        type: "email" | "phone" | "uid",
        email?: string,
        phone?: string,
        uid?: string,
        rule: string
    }) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${this.api.chatbotAuth.app._id}/invite`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
                'access_token': await this.api.chatbotAuth.firebaseToken
            },
            json: true,
            body: params
        }
        let res = await this.exec(setting)
        let row = res.results.object
        await this.getItem(this.api.chatbotAuth.app._id, { reload: true, local: false })
        return row
    }
    async generatorLink(
      rule: string
  ) {
      let setting = {
          method: 'GET',
          uri: this.apiUrl(`${this.api.chatbotAuth.app._id}/invite`),
          qs: { rule },
          headers: { //headers
              
              'x-api-key': this.api.chatbotAuth.appToken,
              'access_token': await this.api.chatbotAuth.firebaseToken
          },
          json: true,
      }
      let res = await this.exec(setting)
      let row = res.results.object
      return row
  }
    async acceptInvite(params: {
        appId: string,
        appToken: string
    }) {
        const { appId, appToken } = params
        let setting = {
            method: "POST",
            uri: this.apiUrl(`${appId}/acceptInvite`),
            headers: { //headers
                
                'app_id': appId,
                'app_token': appToken,
                'access_token': await this.api.chatbotAuth.firebaseToken
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async acceptInviteLink(params: {
      appId: string,
      appToken: string
  }) {
      const { appId, appToken } = params
      let setting = {
          method: "GET",
          uri: this.apiUrl(`${appId}/acceptInviteLink`),
          headers: { //headers
              
              'app_id': appId,
              'app_token': appToken,
              'access_token': await this.api.chatbotAuth.firebaseToken
          },
          json: true,
      }
      let res = await this.exec(setting)
      let row = res.results.object
      return row
  }
    async getInnowayProducts(option?: crudOptions) {
        option = merge({}, this.options, option)
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${this.api.chatbotAuth.app._id}/innoway/product`),
            headers: {
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true
        }
        const hashedRequest = this.hash(setting)
        this.activeRequestHash = hashedRequest
        this.activeRequestSetting = setting
        if (option.local && this.requestCache[hashedRequest] && this.requestCache[hashedRequest].expired > new Date()) {
            return this.requestCache[hashedRequest]
        }
        let res = await this.exec(setting)
        let rows = res.results.object || []
        if (option.reload) {
            this.requestCache[hashedRequest] = {
                items: rows,
                expired: this.moment().add(5, 'minutes').toDate()
            }
        }
        return {
            items: rows,
            paging: res.results.object.paging
        }
    }

    async getInnowayCategories(options?: crudOptions) {
        options = merge({}, this.options, options)
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${this.api.chatbotAuth.app._id}/innoway/category`),
            headers: {
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true
        }
        if (options.local) {
            let items = this.innowayCategories.getValue()
            if (items.length > 1) { // If local empty, request to server
                return items
            }
        }
        let res = await this.exec(setting)
        let rows = res.results.object.rows
        if (options.reload) {
            this.innowayCategories.next(rows)
        }
        return rows
    }

    async getInnowayProfileLink() {
        return `${this.api.chatbotConfig.config.host}/view/v1/innoway/profile`
    }

    async getInnowayDeepLink(params: {
        path: string,
        data?: any
    }) {
        return `${this.api.chatbotConfig.config.host}/view/v1/innoway?${QueryString.stringify(params)}`
    }

    async changeUserRule(params: {
        userId: string,
        rule: "admin" | "write" | "read"
    }) {
        const { userId, rule } = params
        const setting = {
            method: "POST",
            uri: this.apiUrl(`${this.api.chatbotAuth.app._id}/changeUserRule`),
            headers: {
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: {
                userUid: userId,
                rule
            }
        }
        let res = await this.exec(setting)
        let row = res.results.object
        await this.getItem(this.api.chatbotAuth.app._id, { reload: true, local: false })
        return row
    }

    async deleteUser(params: {
        userId: string
    }) {
        const { userId } = params
        const setting = {
            method: "DELETE",
            uri: this.apiUrl(`${this.api.chatbotAuth.app._id}/deleteUser`),
            headers: {
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: {
                userUid: userId
            }
        }
        let res = await this.exec(setting)
        let row = res.results.object
        await this.getItem(this.api.chatbotAuth.app._id, { reload: true, local: false })
        return row
    }
    async leaveApp(appId: string, options?: crudOptions) {
        const setting = {
            method: "DELETE",
            uri: this.apiUrl(`${appId}/leave`),
            headers: {
                
                'access_token': await this.api.chatbotAuth.firebaseToken
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        if (options.reload) {
            let items = this.hashCache[this.activeHashQuery] ? this.hashCache[this.activeHashQuery].items : this.items.getValue()
            const removed = remove(items, function (item) {
                return item._id == appId;
            })
            if (removed.length > 0) {
                if (this.hashCache[this.activeHashQuery]) {
                    this.hashCache[this.activeHashQuery].items = items
                    this.hashCache[this.activeHashQuery].expired = moment().add(5, 'minutes').toDate()
                }
                this.items.next(items)
            } else {
                await this.getList({ local: false, query: this.activeQuery })
            }
            if (this.hashCache[this.activeHashQuery]) {
                this.hashCache = {
                    [this.activeHashQuery]: this.hashCache[this.activeHashQuery]
                }
            }

        }
        return true
    }
    async addApiKey() {
        const setting = {
            method: "POST",
            uri: this.apiUrl(`${this.api.chatbotAuth.app._id}/apiKey`),
            headers: {
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async requestExpandExpired(appId: string) {
        const setting = {
            method: "GET",
            uri: this.apiUrl(`${appId}/requestExpandExpired`),
            headers: {
                'User-Agent': 'Request-Promise'
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async connect(appName, accessToken, userToken, pageToken, templateId) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl('connect'),
            headers: { //headers
                
                'access_token': accessToken
            },
            body: {
                pageToken: pageToken,
                userToken: userToken,
                name: appName,
                templateId: templateId
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object
        //let items = this.items.getValue()
        //items.unshift(row)
        // this.items.next(items)
        return row
    }
    async connectGSheet(params: {
        code: string
    }) {
        const { code } = params
        const setting = {
            method: "POST",
            uri: this.apiUrl(`${this.api.chatbotAuth.app._id}/gsheet`),
            headers: {
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: { code, googleApp: 'MCOM_2' }
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async connectInfusion(params: {
      code: string
  }) {
      const { code } = params
      const setting = {
          method: "POST",
          uri: this.apiUrl(`infusion`),
          headers: {
              
              'x-api-key': this.api.chatbotAuth.appToken
          },
          json: true,
          body: { code }
      }
      let res = await this.exec(setting)
      let row = res.results.object
      return row
  }
    async createSpreadsheet(params: {
        name: string,
        GsheetUserId: string
    }) {
        const { name, GsheetUserId } = params
        const setting = {
            method: "POST",
            uri: this.apiUrl(`${this.api.chatbotAuth.app._id}/gsheet/create`),
            headers: {
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: { name, GsheetUserId }
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async resetTester() {
        const setting = {
            method: "GET",
            uri: this.apiUrl(`${this.api.chatbotAuth.app._id}/resetTester`),
            headers: {
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async updateName(id, name) {
        const setting = {
            method: "PUT",
            uri: this.apiUrl(`${id}/name`),
            headers: {
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: { name }
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async assigningCode(appId, appToken, code) {
        const setting = {
            method: "POST",
            uri: this.apiUrl(`assignLicenseCode`),
            headers: {
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: { code }
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async transferOwner(appId, appToken, uid) {
        const setting = {
            method: "POST",
            uri: this.apiUrl(`${appId}/transfer`),
            headers: {
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: { userId: uid }
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async importExcelQuickMessage(file) {        
      let formData = new FormData()
      formData.append('data', file)
      let setting: AxiosRequestConfig = {
        method: 'POST',
        url: this.apiUrl(`importExcelQuickMessage`),
        headers: { 
          'x-api-key': this.api.chatbotAuth.appToken,
          'Content-Type': 'multipart/form-data'
        },
        data: formData
      }
      let res = (await axios(setting)).data
      return res
    }

    async statisticPosts() {
        const setting = {
            method: "GET",
            uri: this.apiUrl(`statisticPosts`),
            headers: {
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }    

    async statisticStoryGroup(storyGroupIds) {
        const setting = {
            method: "POST",
            uri: this.apiUrl(`statisticStoryGroup`),
            headers: {
                'x-api-key': this.api.chatbotAuth.appToken
            },
            body: {
                storyGroupIds
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async subscribersInStoryGroup(storyGroupId) {
        const setting = {
            method: "POST",
            uri: this.apiUrl(`subscribersInStoryGroup`),
            headers: {
                'x-api-key': this.api.chatbotAuth.appToken
            },
            qs: this._paserQuery({
                fields: ['_id', 'messengerProfile.name', 'messengerProfile.profile_pic'],
                limit: 0
            }),
            body: {
                storyGroupId
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.objects.rows
        return row
    }

    async removeSubscribersInStoryGroup(storyGroupId, subscribers) {
        const setting = {
            method: "POST",
            uri: this.apiUrl(`removeSubscribersInStoryGroup`),
            headers: {
                'x-api-key': this.api.chatbotAuth.appToken
            },
            body: {
                storyGroupId, subscribers
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
}