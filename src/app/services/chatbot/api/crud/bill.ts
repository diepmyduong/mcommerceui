import { CrudAPI, iCrud, crudOptions, crudQuery } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import * as hash from 'object-hash'
import { cloneDeep } from 'lodash-es'
import * as moment from 'moment'
// export interface iBill extends iCrud {
//     code: string
//     app: string | iApp
//     user: string
//     totalPrice: number
//     type: "license" | "feature"
//     option: {
//         feature?: {
//             name: string
//             price: number
//             discount?: number
//         }[],
//         license?: {
//             _id: string
//             package: string
//             price: number
//             discount?: number
//         }
//     },
//     transactionId: string
//     isPaid: boolean
// }



export interface iBill extends iCrud {
    amount: number
    app: string
    code: string
    detail: {
        package: string
        price: number
        qty: number
        amount: number
        desc: string
    }[]
    payer: {
        uid: string 
        name: string 
        email: string
        picture: string
    }
    paymentMethod: string
    status: string
    transaction: {
        trans: {
            amount: number
            channel: string
            option: {
                description: string
                channelId: string
            }
            status: string
        }
        url: string
        vnPayTrans: {
            status: string
            _id: string
            billingChannel: string
            transaction: string
            data: {
                vnp_Version: string
                vnp_Command: string
                vnp_Locale: string
                vnp_CurrCode: string
                vnp_TmnCode: string
                vnp_Amount: number
                vnp_CreateDate: string
                vnp_IpAddr: string
                vnp_OrderInfo: string
                vnp_OrderType: string
                vnp_ReturnUrl: string
                vnp_SecureHash: string
                vnp_SecureHashType: string
                vnp_TxnRef: string
            }
        }
    }
}


export class Bill extends CrudAPI<iBill> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'bill')
    }

    async getBills(appId: string, appToken: string, query?: crudQuery) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(``),
            headers: { //headers
                
                'app_id': appId,
                'app_token': appToken
            },
            json: true,
            qs: this._paserQuery(query)
        }
        let res = await this.exec(setting)
        const rows = res.results.objects.rows
        return rows
    }

    async vnpaySandboxCallback(vnpayObj) {
        let setting = {
            method: 'GET',
            uri: 'https://billsb.mcom.app/api/v1/webhook/vnPay',
            headers: { //headers
                
            },
            json: true,
            qs: this._paserQuery(vnpayObj)
        }
        let res = await this.exec(setting)
    }

    async getStats(adminToken, options?: crudOptions) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`stats`),
            headers: { //headers
                
                'x-api-key': adminToken,
            },
            json: true,
            qs: this._paserQuery(options.query)
        }
        
        const hashedQuery = hash(options.query)
        this.activeHashQuery = hashedQuery
        this.activeQuery = options.query
        if (options.local
            && this.hashCache[hashedQuery] && this.hashCache[hashedQuery].expired > new Date()
            && this.hashCache[hashedQuery].isGetList) {
            let items = this.hashCache[hashedQuery].items
            return items
        }
        let res = await this.exec(setting)
        let row = res.results.object
        this.hashCache[hashedQuery] = {
            pagination: this.pagination,
            items: cloneDeep(row),
            expired: moment().add(5, 'minutes').toDate(),
            isGetList: true
        }
        return row
    }
}