import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'

export interface iGroup extends iCrud {
    paramQuery?: any,
    name?: string,
    isAll?: boolean
}

export class Group extends CrudAPI<iGroup> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'group')
        this.options.query = {
        }
    }

    async getGroupSubscribers(groupId: string) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${groupId}/subscribers`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async exportCsv(groupId: string, groupName: string) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${groupId}/subscribers/csv`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
        }
        let res = await this.exec(setting)
        var hiddenElement = document.createElement('a');
        res.results.object = "\uFEFF" + res.results.objects
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(res.results.object);
        hiddenElement.target = '_blank';
        hiddenElement.download = `${groupName}.csv`;
        hiddenElement.click();
        hiddenElement.remove()
    }

    async exportExcel(groupId: string, groupName: string) {
        const url = this.apiUrl(`${groupId}/subscribers/excel?x-api-key=${this.api.chatbotAuth.appToken}`);
        window.open(url, '_blank');
    }
}