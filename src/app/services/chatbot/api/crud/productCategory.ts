import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from './page';
import { iProduct } from './product';
import { isObject } from 'lodash-es'

export interface iProductCategory extends iCrud {
  name?:string,
  page?: string,
  position?: number,
  listProductId?: string[]
  products?: iProduct[]
}

export class ProductCategory extends CrudAPI<iProductCategory> {
    constructor(
        public api: ChatbotApiService
    ){
        super(api,'productCategory')
    }
    get pageId() {
      return isObject(this.api.chatbotAuth.app.activePage) ? (this.api.chatbotAuth.app.activePage as iPage)._id : this.api.chatbotAuth.app.activePage as string
    }
    async addCategory(name: string, position: number) {
      let pageId = this.pageId
      let setting = {
          method: 'POST',
          uri: this.apiUrl(),
          headers: { //headers
            'x-api-key': this.api.chatbotAuth.appToken,
          },
          json: true,
          body: {
            name,  position , page: pageId
          }
      }
      let res = await this.exec(setting)
      let row = res.results.object ;
      return row
  }
}