import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'

import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { iStory } from 'app/services/chatbot';

export interface iDictionary extends iCrud {
    key?: string,
    page?: string | iPage,
    story?: string | iStory
}

export class Dictionary extends CrudAPI<iDictionary> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'dictionary')
    }
    async deleteStoryFromKey(dicId: string, storyId: string) {
        if (!dicId) throw new Error('dicId undefined in remove story from key')
        if (!storyId) throw new Error('storyId undefined in remove story from key')
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl(`${dicId}/story`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            },
            body: { storyId },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object as iDictionary;
        return row
    }
}