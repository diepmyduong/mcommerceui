import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { merge } from 'lodash-es'

export interface iButtonTag extends iCrud {
    name?: string
    count?: number
    page?: string | iPage
}

export class ButtonTag extends CrudAPI<iButtonTag> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'buttonTag')
    }

    async add(data: iButtonTag, options?: crudOptions): Promise<iButtonTag> {
        if (!data) throw new Error('data undefined in add')
        options = merge({}, this.options, options)
        let setting = {
            method: 'POST',
            uri: this.apiUrl(),
            qs: this._paserQuery(options.query),
            headers: merge({}, { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            }, options.headers),
            body: data,
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object as iButtonTag;
        if (options.reload) {
            let items = this.hashCache[this.activeHashQuery] ? this.hashCache[this.activeHashQuery].items : this.items.getValue()
            if (this.pagination.limit == 0 || items.length < this.pagination.limit) {
                if(items.find(i => i._id == row._id)) {
                    return row
                }
                items.push(row)
                this.items.next(items)
                if (this.hashCache[this.activeHashQuery]) {
                    this.hashCache[this.activeHashQuery].items = items
                    this.hashCache[this.activeHashQuery].expired = this.moment().add(5, 'minutes').toDate()
                }
            }
            if (this.hashCache[this.activeHashQuery]) {
                this.hashCache = {
                    [this.activeHashQuery]: this.hashCache[this.activeHashQuery]
                }
            }
        }
        return row
    }
}