import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from '../..';

export type DiscountBillOption = {
    unit: 'amount' | 'percent' // Giảm giá tiền hoặc phần trăm
    discount: number, // Giá trị giảm
    minAmountOfBill: number // Giảm khi đơn hàng phải hơn bao nhiêu
    maxDiscount: number, // Giảm không quá bao nhiêu 
    useForCombo: boolean // Sử dụng cho combo
    minQty: number // Số lượng mua tối thiểu
}
export type DiscountItemOption = DiscountBillOption & {
    applyItemType: 'category' | 'product' // Giảm cho nhóm hoặc sản phẩm
    applyItems: string[], // Danh sách nhóm hoặc sản phẩm
    inputDiscountForItem: number, // Giảm giá bao nhiêu món 
    discountOneItemForItem: 'min' | 'max', // Theo giá cao nhất hoặc thấp nhất
    inputDiscountFromItem: number, // Giảm giá từ món thứ
    discountOneItemFromItem: 'min' | 'max' // Theo giá cao nhất hoặc thấp nhất
    minQty: number // Số lượng mua tối thiểu
}
export type BuyADiscountBOption = {
    applyItemType: 'category' | 'product' // Giảm cho nhóm hoặc sản phẩm
    requireItems: string[], // Danh sách nhóm cần mua
    applyItems: string[], // Danh sách nhóm được giảm
    numberItemBuy: number, // Số lượng món cần mua
    numberItemFree: number, // Số lượng món được giảm
    discount: number, // Giá trị giảm theo %
    minAmountOfBill: number // Giảm khi đơn hàng phải hơn bao nhiêu
    maxDiscount: number
}
export type BuyAOfferBOption = {
    applyItemType: 'category' | 'product' // Giảm cho nhóm hoặc sản phẩm
    requireItems: string[], // Danh sách nhóm cần mua
    offerItems: { id: string, qty: number }[], // Danh sách món được tặng
    numberItemBuy: number, // Số lượng món cần mua
    numberItemFree: number, // Số lượng món được giảm
    minAmountOfBill: number // Giảm khi đơn hàng phải hơn bao nhiêu
    sameItem: boolean
}
export type OfferItemOption = {
    offerItems: { id: string, qty: number }[], // Danh sách món được tặng
    minAmountOfBill: number // Giảm khi đơn hàng phải hơn bao nhiêu
}
export type SamePriceOption = {
    applyItemType: 'all' | 'category' | 'product' // Giảm cho nhóm hoặc sản phẩm
    price: number // Đồng giá
}
export type AccDecreaseOption = {
    discountAcc: number // Áp dụng giá giảm mỗi sản phẩm bao nhiêu
    discountAmountAcc: number // Thì giảm bao nhiêu
    optAccItemType: 'category' | 'product' // Không áp dụng cho
    optAccItems: string[], // Danh sách không áp dụng
    maxDiscount: number, // Giảm không quá bao nhiêu 
}
export type ManuallyOption = {
    message: string
}

export interface iEvoucher extends iCrud {
    page?: string | iPage
    title?: string
    startDate?: Date
    endDate?: Date
    targetType?: "global" | "group" | "manually" | "exchange_point",
    targetGroup?: string,
    type?: 'discount_bill' | 'discount_item' | 'buy_a_discount_b' | 'buy_a_offer_b' | 'offer_item' | 'same_price' | 'acc_decrease' | 'manually'
    applyPlaces?: string[],
    option?: any | DiscountBillOption | DiscountItemOption | BuyADiscountBOption | BuyAOfferBOption | OfferItemOption | SamePriceOption | AccDecreaseOption | ManuallyOption
    
    code?: string
    format?: string
    point?: number

    expireType?: 'static' | 'dynamic'
    expireAmount?: number,
    expireUnit?: any,
    inputDate?: number[], // Ngày trong tuần
    inputHour?: number[] // Giờ trong tuần
    onlyCoupon?: boolean, // Không áp dụng cùng ưu đãi khác
    qtyPerDay?: number, // Số lần áp dụng mã mỗi ngày
    qtyPerMember?: number, // Số lần áp dụng cho một người dùng
    qty?: number // Số lần áp dụng của chương trình
    used?: number // Số lượng đã sử dụng
    usedByDate?: { [date: string]: number }
}

export class Evoucher extends CrudAPI<iEvoucher> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'eVoucher')
        this.options.query = {
        }
    }

    async exportCSV(id, name) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${id}/export`),
            headers: { //headers                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        var hiddenElement = document.createElement('a');
        res.results.object = "\uFEFF" + res.results.object
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(res.results.object);
        hiddenElement.target = '_blank';
        hiddenElement.download = `Danh sách kết quả Evoucher ${name}.csv`;
        hiddenElement.click();
        hiddenElement.remove()
        return res
    }
}