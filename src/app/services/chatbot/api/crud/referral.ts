import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { merge } from 'lodash-es'
export interface iReferral extends iCrud {
    type?: "story" | "promotion"
    option?: any
    amount?: number
    limit?: number
    startDate?: Date
    endDate?: Date
    messengerCode?: string
    name?: string
    used?: number
    link?: string
    page?: string | iPage
    customQRCode?: {
        logo?: string,
        backgroudImage?: string
    },
    expiredResponse?: string,
    primatureResponse?: string,
    overLimitResponse?: string,
    overAmountResponse?: string,
    enableWifiMK?: boolean,
    wifiMKCount?: number,
    wifiBitlink?: string,
    wifiMKPhotos?: any[],
    wifiMKButtonText?: string,
    bitlink?: string
}

export class Referral extends CrudAPI<iReferral> {
    constructor(
        public api: ChatbotApiService
    ){
        super(api,'referral')
    }

    async add(data: iReferral, options?: crudOptions): Promise<iReferral> {
        if (!data) throw new Error('data undefined in add')
        options = merge({}, this.options, options)
        let setting = {
            method: 'POST',
            uri: this.apiUrl(),
            qs: this._paserQuery(options.query),
            headers: merge({}, { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            }, options.headers),
            body: data,
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object.result as iReferral;
        if (options.reload) {
            let items = this.items.getValue()
            if(items.length < this.pagination.limit) {
                this.items.next(items)
                this.hashCache[this.activeHashQuery].items = items
                this.hashCache[this.activeHashQuery].expired = this.moment().add(5, 'minutes').toDate()
                items.push(row)
                
            }
            this.hashCache = {
                [this.activeHashQuery]: this.hashCache[this.activeHashQuery]
            }
        }
        return row
    }
    async toggleWifiMK(refId: string, enable: boolean, options?: crudOptions) {
        options = merge({}, this.options, options)
        const setting = {
            method: 'PUT',
            uri: this.apiUrl(`${refId}/wifiMK`),
            headers: merge({}, { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            }, options.headers),
            body: { enable },
            json: true
        };
        let res = await this.exec(setting);
        return res
    }

}