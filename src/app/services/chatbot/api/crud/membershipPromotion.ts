import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iSubscriber } from './subscriber';
import { iLuckyWheelGift } from './luckyWheel';
import { iPage } from 'app/services/chatbot';

export interface iMembershipPromotion extends iCrud {
    page: string | iPage
    startAt: Date
    endAt: Date
    type: "price" | "percent"
    code: string
    score: number
    expiredAt: any
    expiredType: "exact" | "about"
    amount: number
}

export class MembershipPromotion extends CrudAPI<iMembershipPromotion> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'membershipPromotion')
        this.options.query = {
        }
    }

}