import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iApp } from 'app/services/chatbot/api/crud/app';
import { iSubscriber } from 'app/services/chatbot/api/crud/subscriber';

export interface iNotifyUser extends iCrud {
    app?: iApp | string,
    uid?: string,
    psid?: string,
    options?: {
        isReceiveOrderFromUser?: boolean,
        isReceiveByNotifyCard?: boolean,
        isReceiveMcomPromotion?: boolean,
        isReceiveMcomNotify?: boolean,
        isReceiveUserTurnLivechat?: boolean
    },
    subscriber?: string | iSubscriber
}

export class NotifyUser extends CrudAPI<iNotifyUser> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'notifyUser')
    }
    async setting(data) {
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`setting`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: data
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
}