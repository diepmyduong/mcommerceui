import { CrudAPI, iCrud, crudOptions, crudQuery } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { iApp } from 'app/services/chatbot';
import { iHaravan } from './haravan';

export interface iHaravanCategory extends iCrud {
    page?: string | iPage;
    shop?: string | iHaravan;
    name?: string;
}

export class HaravanCategory extends CrudAPI<iHaravanCategory> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'haravanCategory')
    }

    
}