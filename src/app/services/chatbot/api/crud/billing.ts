import { CrudAPI, iCrud, crudOptions, crudQuery } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { iApp } from 'app/services/chatbot';

export interface iBilling extends iCrud {
    code: string
    app: string | iApp
    user: string
    totalPrice: number
    type: "license" | "feature"
    option: {
        feature?: {
            name: string
            price: number
            discount?: number
        }[],
        license?: {
            _id: string
            package: string
            price: number
            discount?: number
        }
    },
    transactionId: string
    isPaid: boolean
}

export class Billing extends CrudAPI<iBilling> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'billing')
    }

    
}