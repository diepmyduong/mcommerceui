import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { iStory } from 'app/services/chatbot/api/crud/story'

export interface iTemplateCategory extends iCrud {
    name: string
}

export class TemplateCategory extends CrudAPI<iTemplateCategory> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'templateCategory')
    }
}