import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'

export interface iGsheetUser extends iCrud {
    profile?: {
        id?: string,
        link?: string,
        picture?: string,
    }
}

export class GsheetUser extends CrudAPI<iGsheetUser> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'gsheetUser')
    }
    async getUser() {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.objects.rows
        return row
    }
    async revoke(id: string) {
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl(`${id}/revoke`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async reconnectGSheet(id: string, params: {
        code: string
    }) {
        const { code } = params
        const setting = {
            method: "POST",
            uri: this.apiUrl(`${id}/reconnect`),
            headers: {
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: { code }
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async createSpreadsheet(id: string, params: {
        name,
        sheets
    }) {
        const { name, sheets } = params
        const setting = {
            method: "POST",
            uri: this.apiUrl(`${id}/create`),
            headers: {
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: { name, sheets }
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
}