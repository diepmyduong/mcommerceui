import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service';

export interface iQuickMessage extends iCrud {
    page?: string 
    type?: "story"
    option?: any
    message?: string
}

export class QuickMessage extends CrudAPI<iQuickMessage> {
    constructor(
        public api: ChatbotApiService
    ){
        super(api,'quickMessage')
    }
}