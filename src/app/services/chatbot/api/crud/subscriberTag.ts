import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { iTopicEvent } from 'app/services/chatbot/api/crud/topicEvent';
import { iGroup } from '../..';

export interface iSubscriberTag extends iCrud {
    page?: string | iPage;
    name?: string;
    description?: string;
    count?: number;
    story?: string;
    color?: string;
    position?: number
}

export class SubscriberTag extends CrudAPI<iSubscriberTag> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'subscriberTag')
    }
}