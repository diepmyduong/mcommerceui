import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { merge } from 'lodash-es'
export interface iTemplate extends iCrud {
    name?: string
    description?: string
    type?: "private" | "public"
    shareableUser?: any[]
    image?: string
    source?: string
    owner?: string
    preview?: string
    used: string
    estimate: {
        isSell: boolean,
        currency: "VND" | "USD",
        price: string
    }
    tag: string[]
    author?: string
    option: any
    category: string
}

export class Template extends CrudAPI<iTemplate> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'template')
    }

    async getTemplateList() {
        let firebaseToken = await this.api.chatbotAuth.firebaseToken
        let setting = {
            method: 'GET',
            uri: this.apiUrl(),
            headers: { //headers
                
                'access_token': firebaseToken
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results ? res.results.objects.rows : res.results
        return row
    }

    async importTemplate(appId, appToken, pageId, templateId) {
        let setting = {
            method: 'POST',
            uri: this.crossModuleUrl(`page/${pageId}/importTemplate`),
            headers: { //headers
                
                'app_id': appId,
                'app_token': appToken
            },
            body: { templateId },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results ? res.results.objects : res.results
        return row
    }

    async getList(options?: crudOptions) {
        options = merge({
            headers: {
                access_token: await this.api.chatbotAuth.firebaseToken
            }
        }, options)
        return await super.getList(options)
    }

    async share(params: {
        templateId: string,
        email: string
    }) {
        const { templateId, email } = params
        const option = {
            method: "PUT",
            uri: this.apiUrl(`${templateId}/share`),
            headers: {
                
                "content-type": "application/json",
                access_token: await this.api.chatbotAuth.firebaseToken
            },
            body: { email },
            json: true
        }
        return await this.exec(option)
    }

    async unshare(params: {
        templateId: string,
        uid: string
    }) {
        const { templateId, uid } = params
        const option = {
            method: "DELETE",
            uri: this.apiUrl(`${templateId}/share`),
            headers: {
                
                "content-type": "application/json",
                access_token: await this.api.chatbotAuth.firebaseToken
            },
            body: { uid },
            json: true
        }
        return await this.exec(option)
    }
    async search(params: {
        query: string
    }) {
        const { query } = params
        const option = {
            method: "GET",
            uri: this.apiUrl(`search`),
            headers: {
                
                "content-type": "application/json",
                access_token: await this.api.chatbotAuth.firebaseToken
            },
            body: { query },
            json: true
        }
        return await this.exec(option)
    }
    
    async updateTemplate(templateId, data) {
        let firebaseToken = await this.api.chatbotAuth.firebaseToken
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`${templateId}`),
            headers: { //headers
                
                'access_token': firebaseToken
            },
            json: true,
            body: data
        }
        let res = await this.exec(setting)
        let row = res.results
        return row
    }
    async deleteTemplate(templateId) {
        let firebaseToken = await this.api.chatbotAuth.firebaseToken
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl(`${templateId}`),
            headers: { //headers
                
                'access_token': firebaseToken
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results
        return row
    }

    async getShareLink(templateId) {
        let firebaseToken = await this.api.chatbotAuth.firebaseToken
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${templateId}/shareLink`),
            headers: { //headers
                
                'access_token': firebaseToken
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async acceptShare(templateId, shareToken) {
        let firebaseToken = await this.api.chatbotAuth.firebaseToken
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${templateId}/acceptShare`),
            headers: { //headers
                
                'access_token': firebaseToken,
                'share_token': shareToken
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
}