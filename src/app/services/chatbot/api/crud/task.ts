import { CrudAPI, iCrud,  } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iApp } from 'app/services/chatbot/api/crud/app';

export interface iTask extends iCrud {
    type?: "send_story" | "broadcast" | "test"
    app?: string | iApp,
    title?: string,
    content?: string,
    payload?: any,
    processState?: 'complete' | 'running' | 'failed' | 'scheduled'
    completeProcess?: number,
    totalProcess?: number,
    failedProcess?: number
}

export class Task extends CrudAPI<iTask> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'task')
    }

    async cancel(id) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${id}/cancel`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
}