import { CrudAPI, iCrud, crudOptions, crudQuery } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { iApp } from 'app/services/chatbot';

export interface iHaravan extends iCrud {
    domain?: string
    username?: string
    password?: string
    productLastUpdate?: Date
    status?: string
    page?: string
}

export class Haravan extends CrudAPI<iHaravan> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'haravanShop')
    }

    async triggerProduct() {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`triggerProduct`),
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results?res.results.object:res.results
        return row
    }
}