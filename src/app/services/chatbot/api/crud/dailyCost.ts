import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'

export interface iDailyCost extends iCrud{
    amount: number // total
    costs: iCost[]
}

export interface iCost {
    amount: number // total of each package
    package: number 
    price: number // price of the package
    qty: number // quantity
}

export interface iPackage {
    limit: number
    package: string
    price: number
    type: string
    desc: string
}

export class DailyCost extends CrudAPI<iDailyCost> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'dailyCost')
    }

    async getPackages() {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`packages`),
            headers: {
                
                "content-type": "application/json",
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async overview(firebaseToken) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`overview`),
            headers: {
                
                "content-type": "application/json",
                'access_token': firebaseToken,
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async overviewFollow(apiKey, appIds) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`overview`),
            headers: {
                
                "content-type": "application/json",
                'x-api-key': apiKey
            },
            json: true,
            body: { appIds }
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async history(firebaseToken) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`history`),
            headers: {
                
                "content-type": "application/json",
                'access_token': firebaseToken,
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.objects
        return row
    }

    async checkout(firebaseToken, app, paymentMethod = 'vnpay') {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`checkout`),
            headers: {
                
                "content-type": "application/json",
                'access_token': firebaseToken,
            },
            body: {
                app, paymentMethod
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async getInviteAgencyToken(adminToken) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`inviteAgency`),
            headers: {
                
                "content-type": "application/json",
                'x-api-key': adminToken,
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async acceptAgencyInvitation(firebaseToken, inviteToken) {
        
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`acceptAgencyInvite`),
            headers: {
                
                "content-type": "application/json",
                'access_token': firebaseToken
            },
            body: { inviteToken },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
}