import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import axios, { AxiosRequestConfig } from 'axios'

export interface iLuckyWheel extends iCrud {
    name?: string
    config?: {
        backgroundColor?: string
        backgroundImage?: string
        buttonColor?: string
        bannerImage?: string
        wheelImage?: string
        pinImage?: string
        btnTitle?: string
        title?: string
        turn?: number
    },
    successRatio?: number
    failRatio?: number
    gifts?: iLuckyWheelGift[]
}

export interface iLuckyWheelGift {
    _id?: string
    name?: string
    desc?: string
    code?: string
    image?: string
    amount?: number
    used?: number
    type?: "gift" | "miss"
}

export class LuckyWheel extends CrudAPI<iLuckyWheel> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'luckyWheel')
        this.options.query = {
        }
    }

    async copy(app_id, app_token, wheelId) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${wheelId}/copy`),
            headers: { //headers
                
                "content-type": "application/json",
                'app_id': app_id,
                'app_token': app_token,
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    
    async resetGiftUsage(id, giftId) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${id}/reset`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            },
            body: { giftId },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        return res
    }

    async removeAllResults(id) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${id}/receive/reset`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        return res
    }

    async exportCSV(id, name) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${id}/receive/csv`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        var hiddenElement = document.createElement('a');
        res.results.object = "\uFEFF" + res.results.object
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(res.results.object);
        hiddenElement.target = '_blank';
        hiddenElement.download = `${name}.csv`;
        hiddenElement.click();
        hiddenElement.remove()
        return res
    }

    async getImportTemplate() {
        let setting: AxiosRequestConfig = {
          method: 'GET',
          url: this.apiUrl(`getImportTemplate`),
          headers: { 
            'x-api-key': this.api.chatbotAuth.appToken,
          },
          responseType: 'blob'
        }
        let res = (await axios(setting)).data
        return res
    }

    async importTemplate(file) {
        let formData = new FormData()
        formData.append('data', file)
        let setting: AxiosRequestConfig = {
          method: 'POST',
          url: this.apiUrl(`importTemplate`),
          headers: { 
            'x-api-key': this.api.chatbotAuth.appToken,
            'Content-Type': 'multipart/form-data'
          },
          data: formData
        }
        let res = (await axios(setting)).data
        return res
    }
}