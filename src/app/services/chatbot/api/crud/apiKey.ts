import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iApp } from 'app/services/chatbot';

export interface iApiKey extends iCrud {
    used?: number
    accessToken?: string
    app?: string | iApp
}

export class ApiKey extends CrudAPI<iApiKey> {
    constructor(
        public api: ChatbotApiService
    ){
        super(api,'apiKey')
    }
    async revoke(apiKeyId: string) {
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`${apiKeyId}/revoke`),
            headers: { //headers
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row 
    }
}