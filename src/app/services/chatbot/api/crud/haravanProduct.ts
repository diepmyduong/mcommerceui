import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud';
import { iPage } from 'app/services/chatbot/api/crud/page';
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service';
import { iHaravan } from './haravan';
import { iHaravanCategory } from './haravanCategory';

export interface iHaravanProduct extends iCrud {
    page?: string | iPage;
    shop?: string | iHaravan;
    name?: string;
    description?: string;
    price?: number;
    saleOffPrice?: number;
    image?: string;
    category?: string | iHaravanCategory;
    toppings?: any[]
}

export class HaravanProduct extends CrudAPI<iHaravanProduct> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'haravanProduct')
    }

    
}