import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'

export interface iCustomer extends iCrud { 
    uid?: string
    fullName?: string,
    phone?: string,
    email?: string,
    status?: 'active' | 'deactive'
}

export class Customer extends CrudAPI<iCustomer> {
    constructor(
        public api: ChatbotApiService
    ){
        super(api,'customer')
    }

    async me() {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`me`),
            headers: {
                access_token: await this.api.chatbotAuth.firebaseToken
            },
            json: true
        }
        let res = await this.exec(setting)
        return res.results.object
    }

    async updateMe({ fullName, email, phone }) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`me`),
            headers: { access_token: await this.api.chatbotAuth.firebaseToken },
            json: true,
            body: { fullName, email, phone }
        }
        let res = await this.exec(setting)
        return res.results.object
    }
}