import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'

import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'

export interface iAdminNote extends iCrud {
}

export class AdminNote extends CrudAPI<iAdminNote> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'admin_note')
        this.options.query = {
        }
    }

    async getAdminNotes(subscriber_id) {
        let setting = {
            method: 'GET',
            uri: this.crossModuleUrl(`subscriber/${subscriber_id}/adminNotes`),
            headers: { //headers
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async deleteAdminNote(subscriber_id, note_id) {
        const data = {
            "adminNoteId": note_id
        }
        let setting = {
            method: 'DELETE',
            uri: this.crossModuleUrl(`subscriber/${subscriber_id}/adminNotes`),
            headers: { //headers
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: data
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async addAdminNote(subscriber_id, note_text) {
        const data = {
            "body": note_text
        }
        let setting = {
            method: 'POST',
            uri: this.crossModuleUrl(`subscriber/${subscriber_id}/adminNotes`),
            headers: { //headers
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: data
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }


}