import { CrudAPI, iCrud } from '../crud'
import { ChatbotApiService } from '../../chatbot-api.service';

export interface iPlugin extends iCrud {
    page?: string 
    type: "spreadsheet"
    option: any
}

export class Plugin extends CrudAPI<iPlugin> {
    constructor(
        public api: ChatbotApiService
    ){
        super(api,'plugin')
    }
    async createPlugin(data){
        let setting = {
            method: 'POST',
            uri: this.apiUrl(``),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
            },
            json: true,
            body: data   
        }
        let res = await this.exec(setting)
        return res.results.object
    }
    async importSpreadsheetTemplate(id: string, data){
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${id}/spreadsheet/import`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,            
            },
            json: true,
            body: data   
        }
        let res = await this.exec(setting)
        return res.results.object
    }
    async addUser(id: string, email, appId?: string, appToken?: string){
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${id}/spreadsheet/addUser`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken,
            },
            json: true,
            body: { email }   
        }
        if(appId && appToken) {
            setting.headers = {
                
                'app_id': appId,
                'app_token':appToken,
            } as any
        }
        let res = await this.exec(setting)
        return res.results.object
    }
    async getSpreadsheetPlugin(appId: string, appToken: string){
        let setting = {
            method: 'GET',
            uri: this.apiUrl(``),
            headers: { //headers
                
                'app_id':appId,
                'app_token':appToken
            },
            json: true,
            qs: { type: "spreadsheet" }   
        }
        let res = await this.exec(setting)
        const rows = res.results.objects.rows
        return rows[0]
    }
}