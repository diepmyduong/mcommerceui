import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'

import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iApp } from 'app/services/chatbot';

export interface iEnvironment extends iCrud {
    app?: string | iApp,
    data?: { [x: string]: any }
}

export class Environment extends CrudAPI<iEnvironment> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'environment')
    }
    async updateEnvironment(environmentId: string, appId: string, appToken: string, data: any) {
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`${environmentId}`),
            headers: { //headers
                
                'app_id':appId,
                'app_token':appToken
            },
            body: {
                data: data
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async get(environmentId: string, appId: string, appToken: string) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${environmentId}`),
            headers: { //headers
                
                'app_id':appId,
                'app_token':appToken
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
}