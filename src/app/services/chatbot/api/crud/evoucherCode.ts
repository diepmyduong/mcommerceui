import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from './page';
import { iSubscriber } from './subscriber';
import { iEvoucher } from './evoucher';

export interface iEvoucherCode extends iCrud {
    subscriber?: string | iSubscriber
    code?: string
    eVoucher: string | iEvoucher
    expiredAt?: Date
    page?: string | iPage
    status?: "unused" | "used"
}

export class EvoucherCode extends CrudAPI<iEvoucherCode> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'eVoucherCode')
        this.options.query = {
        }
    }
}