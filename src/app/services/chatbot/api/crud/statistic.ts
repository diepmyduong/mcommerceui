import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'

export interface iStatistic extends iCrud {

}

export class Statistic extends CrudAPI<iStatistic> {
  constructor(
    public api: ChatbotApiService
  ) {
    super(api, 'statistic')
  }

  async summaryActivities(params: {
    startTime: Date,
    endTime: Date,
    isShowActivityDetail: boolean,
    isShowMessageDetail: boolean,
    isCheckGender: boolean
  }, refresh = false) {
    let { startTime, endTime, isShowActivityDetail, isShowMessageDetail, isCheckGender } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`activities`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken
      },
      json: true,
      qs: { startTime, endTime, isShowActivityDetail, isShowMessageDetail, isCheckGender, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }

  async summarySubscribers(params: {
    isCheckGender: boolean,
    isCheckName: boolean,
    isShowAnalyticDetail: boolean,
    isShowActivityDetail: boolean
  }, refresh = false) {
    let { isCheckGender, isCheckName, isShowAnalyticDetail, isShowActivityDetail } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`subscribers`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { isCheckGender, isCheckName: false, isShowAnalyticDetail: false, isShowActivityDetail: false, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }

  async summary(params: {
    isStoryDetail: boolean,
    isTopicDetail: boolean,
    isSubscriberDetail: boolean
  }, refresh = false) {
    let { isStoryDetail, isTopicDetail, isSubscriberDetail } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`summary`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { isStoryDetail, isTopicDetail, isSubscriberDetail, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }

  async summaryTopics(params: {
    startTime: Date,
    endTime: Date,
    isShowActivityDetail: boolean,
    isShowMessageDetail: boolean,
    isCheckGender: boolean
  }, refresh = false) {
    let { startTime, endTime, isShowActivityDetail, isShowMessageDetail, isCheckGender } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`topics`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, isShowActivityDetail, isShowMessageDetail, isCheckGender, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }

  async summaryStories(params: {
    startTime: Date,
    endTime: Date
  }, refresh = false) {
    let { startTime, endTime } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`stories`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }
  async countNumberMessage(params: {
    startTime?: Date,
    endTime?: Date
  }, refresh = false) {
    let { startTime, endTime } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`countNumberMessage`),
      headers: { //headers
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }
  async summaryStory(params: {
    storyId: string,
    startTime: Date,
    endTime: Date
  }, refresh = false) {
    let { storyId, startTime, endTime } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`story`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { storyId, startTime, endTime, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }

  async summaryPosts(params: {
    startTime: Date,
    endTime: Date,
    subscriberDetail: boolean
  }, refresh = false) {
    let { startTime, endTime, subscriberDetail } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`interactPosts`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, subscriberDetail, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }
  async summaryReactPosts(params: {
    statisticDetail?: Boolean,
    limit?: number
  }, refresh = false) {
    let { statisticDetail = true, limit = -1 } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`posts`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { statisticDetail, limit, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }
  async summaryReferral(params: {
    startTime: Date,
    endTime: Date,
    subscriberDetail: boolean
  }, refresh = false) {
    let { startTime, endTime, subscriberDetail } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`referrals`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, subscriberDetail, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }

  async summaryButtonTags(params: {
    startTime: Date,
    endTime: Date,
    subscriberDetail: boolean
  }, refresh = false) {
    let { startTime, endTime, subscriberDetail } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`buttonTags`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, subscriberDetail, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }

  async summaryStoryDetail(params: {
    id: string,
    startTime?: Date,
    endTime?: Date
  }, refresh = false) {
    let { startTime, endTime, id } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`stories/${id}`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }

  async summaryTopicDetail(params: {
    id: string,
    startTime?: Date,
    endTime?: Date
  }, refresh = false) {
    let { startTime, endTime, id } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`topics/${id}`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }

  async summaryInteractPostDetail(params: {
    id: string,
    startTime?: Date,
    endTime?: Date
  }, refresh = false) {
    let { startTime, endTime, id } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`interactPosts/${id}`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, subscriberDetail: true, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }

  async summaryReferralDetail(params: {
    id: string,
    startTime?: Date,
    endTime?: Date
  }, refresh = false) {
    let { startTime, endTime, id } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`referrals/${id}`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, subscriberDetail: true, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }
  async summaryPostDetail(params: {
    postId: string
  }, refresh = false) {
    let { postId } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`post`),
      headers: { //headers
        
        "content-type": "application/json",

        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { postId, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }
  async summaryPostDetailCsvData(params: {
    postId: string
  }, refresh = false) {
    let { postId } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`post/csv`),
      headers: { //headers
        
        "content-type": "application/json",

        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { postId, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }
  async summaryStoryInteractives(params: {
    startTime?: Date,
    endTime?: Date
  }, refresh = false) {
    let { startTime, endTime } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`storyInteractives`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }
  async summarySubscribe(params: {
    startTime?: Date,
    endTime?: Date,
    isCheckGender?: boolean
  }, refresh = false) {
    let { startTime, endTime, isCheckGender } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`subscribe`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, isCheckGender, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }
  async summarySubscribeByDay(params: {
    startTime?: Date,
    endTime?: Date
  }, refresh = false) {
    let { startTime, endTime} = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`subscribeByDay`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }
  async summaryBlock(params: {
    startTime?: Date,
    endTime?: Date,
    isCheckgender?: boolean
  }, refresh = false) {
    let { startTime, endTime, isCheckgender = false } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`block`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, isCheckgender, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }
  async storySent(params: {
    startTime?: Date,
    endTime?: Date
  }, refresh = false) {
    let { startTime, endTime } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`storySent`),
      headers: { //headers
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }
  async subscriberInteractives(params: {
    startTime?: Date,
    endTime?: Date
  }, refresh = false) {
    let { startTime, endTime } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`subscriberInteractives`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }
  async uniqueSubscriberReceiveStory(params: {
    startTime?: Date,
    endTime?: Date
  }, refresh = false) {
    let { startTime, endTime } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`uniqueSubscriberReceiveStory`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }
  async summaryGroup(params: {
    startTime: Date,
    endTime: Date,
    groupId: string
  }, refresh = false) {
    let { startTime, endTime, groupId } = params
    let setting = {
      method: 'GET',
      uri: this.apiUrl(`group/${groupId}`),
      headers: { //headers
        
        "content-type": "application/json",
        'x-api-key': this.api.chatbotAuth.appToken,

      },
      json: true,
      qs: { startTime, endTime, isRefresh: refresh ? true : undefined }
    }
    let res: any = await this.exec(setting);
    let rows = res.results.object
    return rows;
  }
}
