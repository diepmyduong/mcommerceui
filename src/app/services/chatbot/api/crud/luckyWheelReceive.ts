import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iSubscriber } from './subscriber';
import { iLuckyWheelGift } from './luckyWheel';

export interface iLuckyWheelReceive extends iCrud {
    buttonId: string
    gift: iLuckyWheelGift
    giftIndex: number
    giftNumber: number
    luckyWheel: string
    page: string
    status: string
    subscriber: iSubscriber
}

export class LuckyWheelReceive extends CrudAPI<iLuckyWheelReceive> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'luckyWheelReceive')
        this.options.query = {
        }
    }

    async deleteSubscribers(items) {
        try {
            let setting = {
                method: 'DELETE',
                uri: this.apiUrl(),
                qs: this._paserQuery({ items }),
                headers: { //headers
                    
                    "content-type": "application/json",
                    'x-api-key': this.api.chatbotAuth.appToken
                },
                json: true // Automatically parses the JSON string in the response
            }
            let res = await this.exec(setting)
            return res
        } catch (err) {
            throw(err)
        }
    }
}