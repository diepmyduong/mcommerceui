import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'

export interface iSetting extends iCrud {
    isDefault?: boolean
    option?: any
    type?: "persistent_menu" | "greeting" | "get_started" | "whitelisted_domains" | "home_url" | "ice_breakers"
    [key: string]: any
}

export class Setting extends CrudAPI<iSetting> {
    constructor(
        public api: ChatbotApiService
    ){
        super(api,'setting')
    }
}