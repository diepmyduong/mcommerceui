import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iShop } from './shop';
import { iPage } from '../..';
import { iProduct } from './product';
import { iSubscriber } from './subscriber';

export interface iOrder extends iCrud {
    page?: string | iPage
    shop?: string | iShop
    code?: string
    items?: {
        product: string | iProduct,
        name: string
        description: string
        image: string
        qty: number
        amount: number
        toppings?: any[]
        toppingDesc?: string
    }[]
    qty?: number
    amount?: number
    amountOfSaleOff?: number
    subTotal?: number
    address?: string
    from?: any
    distance?: number
    shipFee?: number
    subscriber?: string | iSubscriber
    customerInfo?: {
        name: string
        phone: string
    },
    isConfirm?: boolean
}

export class Order extends CrudAPI<iOrder> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'order')
    }
}