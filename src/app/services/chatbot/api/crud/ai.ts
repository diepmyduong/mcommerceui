import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'

import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'

export interface iAI extends iCrud {
    type?: "default" | "custom",
    page?: string | iPage,
    name?: string,
    description?: string
}

export class AI extends CrudAPI<iAI> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'ai')
    }

    async changeToken(aiId: string, token: {
        devToken: string,
        clientToken: string
    }) {
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`${aiId}/token`),
            headers: { //headers
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            },
            body: token,
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
}