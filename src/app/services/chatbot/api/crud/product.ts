import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'

export interface iProduct extends iCrud {
  page?:string,
  name?:string,
  description?:string,
  price?:number,
  image?:string,
  category?:string,
  toppings?: {
    _id?: string,
    groupName?: string,
    require?: boolean,
    options?:
    {
      _id?: string,
      display?: string,
      price?: number
    }[]
  }[]
}

export class Product extends CrudAPI<iProduct> {
    constructor(
        public api: ChatbotApiService
    ){
        super(api,'product')
    }
    async addProduct(params) {
      let setting = {
          method: 'POST',
          uri: this.apiUrl(),
          headers: { //headers
              
              'x-api-key': this.api.chatbotAuth.appToken,
               
          },
          json: true,
          body: params
      }
      let res = await this.exec(setting)
      let row = res.results.object ;
      return row
  }
}