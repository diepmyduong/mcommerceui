import { ChatbotApiService } from "app/services/chatbot/chatbot-api.service";
import { iCrud, CrudAPI } from "app/services/chatbot/api/crud";
import { iPage, iFbPost } from "app/services/chatbot";

export interface iInteractPost extends iCrud {
  page?: string | iPage;
  name?: string;
  description?: string;
  postId?: string;
  postData?: iFbPost;
  limit?: number;
  amount?: number;
  startDate?: Date;
  endDate?: Date;
  used?: number;
  cases?: iInteractPostCase[];
}

export interface iInteractPostCase {
  isResponseStory?: boolean;
  responseStory?: string;
  isResponseComment?: boolean;
  responseComment?: iInteractPostComment;
  isResponsePrivateReplies?: boolean;
  responsePrivateReplies?: string;
  privateRepliesConfig?: iInteractPostPrivateReplyConfig[];
  isCheckReaction?: boolean;
  reactionRequires?: ["LIKE" | "LOVE" | "HAHA" | "WOW" | "ANGRY"];
  isSendOnlyParentComment?: boolean;
  isHideUserComment?: boolean;
  priority?: number;
  value?: {
    regex?: string;
    intentId?: string;
    from?: number;
    to?: number;
    text?: string;
    intent?: any;
  };
  type?:
    | "text"
    | "number"
    | "range"
    | "regex"
    | "fail"
    | "intent"
    | "overLimit"
    | "overAmount"
    | "premature"
    | "expired"
    | "tagLimit";
  [key: string]: any;
}

export interface iInteractPostComment {
  message?: string;
  attachment_id?: string;
  attachment_url?: string;
  source?: string;
}

export interface iInteractPostPrivateReplyConfig {
  key: {
    regex?: string;
    intentId?: string;
    from?: number;
    to?: number;
    text?: string;
  };
  value: string;
  type?: "text" | "number" | "range" | "regex" | "fail" | "intent";
  [key: string]: any;
}

export class InteractPost extends CrudAPI<iInteractPost> {
  constructor(public api: ChatbotApiService) {
    super(api, "interactPost");
  }

  async copy(app_id, app_token, interactPostId) {
    let setting = {
      method: "POST",
      uri: this.apiUrl(`${interactPostId}/copy`),
      headers: {
        //headers

        "content-type": "application/json",
        app_id: app_id,
        app_token: app_token,
      },
      json: true,
    };
    let res = await this.exec(setting);
    let row = res.results.object as iInteractPost;
    return row;
  }

  async getCommentReplies(id: string, local: boolean = true) {
    let setting = {
      method: "GET",
      uri: this.apiUrl(`${id}/commentReplies`),
      headers: {
        "content-type": "application/json",
        "x-api-key": this.api.chatbotAuth.appToken,
      },
      json: true,
    };
    const hashedRequest = this.hash(setting);
    this.activeRequestHash = hashedRequest;
    this.activeRequestSetting = setting;
    if (local && this.requestCache[hashedRequest] && this.requestCache[hashedRequest].expired > new Date()) {
      return this.requestCache[hashedRequest].items;
    }
    let res = await this.exec(setting);
    let rows = res.results.object.rows;
    this.requestCache[hashedRequest] = {
      items: rows,
      expired: this.moment().add(5, "minutes").toDate(),
    };
    return rows;
  }
}
