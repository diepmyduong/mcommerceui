import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'

export interface iLabel extends iCrud {
}

export class Label extends CrudAPI<iLabel> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'label')
        this.options.query = {
        }
    }

    async getLabelListFromSubscriber(subscriber_id) {
        let setting = {
            method: 'GET',
            uri: this.crossModuleUrl(`subscriber/${subscriber_id}/facebookLabels`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
        }
        let res = await this.exec(setting)
        let row = res.results?res.results.object:res.results
        return row
    }

    async getAllLabels(page_id) {
        let setting = {
            method: 'GET',
            uri: this.crossModuleUrl(`page/${page_id}/labels`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results?res.results.object:res.results
        return row
    }

    async removeLabel(page_id, subscriber_ids, label_text) {
        const data = {
            "name": label_text,
            "subscriberIds": subscriber_ids
        }
        let setting = {
            method: 'DELETE',
            uri: this.crossModuleUrl(`page/${page_id}/labels/deleteSubscribers`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: data
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

    async addLabel(page_id, subscriber_ids, label_text) {
        const data = {
            "name": label_text,
            "subscriberIds": subscriber_ids
        }
        let setting = {
            method: 'POST',
            uri: this.crossModuleUrl(`page/${page_id}/labels/addSubscribers`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: data
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

}