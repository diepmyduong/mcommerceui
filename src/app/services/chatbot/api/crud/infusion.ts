import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'


export interface iInfusion extends iCrud {
  
}

export class Infusion extends CrudAPI<iInfusion> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'infusion')
    }
    // async getUser() {
    //     let setting = {
    //         method: 'GET',
    //         uri: this.apiUrl(),
    //         headers: { //headers
    //             
    //             'x-api-key': this.api.chatbotAuth.appToken
    //         },
    //         json: true,
    //     }
    //     let res = await this.exec(setting)
    //     let row = res.results.objects.rows
    //     return row
    // }
    async disconnect(id: string) {
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl(`${id}/disconnect`),
            headers: { //headers
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async reconnect(id: string, params: {
        code: string
    }) {
        const { code } = params
        const setting = {
            method: "POST",
            uri: this.apiUrl(`${id}/reconnect`),
            headers: {
                
                'x-api-key': this.api.chatbotAuth.appToken
            },
            json: true,
            body: { code }
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }

}