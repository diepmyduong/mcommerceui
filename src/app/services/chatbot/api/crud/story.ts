import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { iReferral } from 'app/services/chatbot/api/crud/referral'
import { iCard } from 'app/services/chatbot/api/crud/card'
import { iDictionary } from 'app/services/chatbot/api/crud/dictionnary';
import { merge, assign } from 'lodash-es'

export interface iStory extends iCrud {
    cards?: iCard[] | string[]
    intent?: string
    name?: string
    page?: string | iPage
    type?: "custom" | "intent"
    mode?: 'normal' | 'broadcast' | 'sequence'
    quickReplies?: any[],
    useRef?: boolean
    ref?: string
}

export class Story extends CrudAPI<iStory> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'story')
    }

    async getAll(mode = 'normal') {
        return await this.getList({
            local: true,
            query: {
              limit: 0,
              fields: ["_id", "name"],
              order: { 'createdAt': 1 },
              filter: { mode }
            }
        })
    }

    async copy(app_id, app_token , data: { name: string, storyId: string, storyGroupId: string }) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`copy`),
            headers: { //headers                
                "content-type": "application/json",
                'app_id': app_id,
                'app_token': app_token,
            },
            body: data,
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object as iStory
        return row
    }
    async duplicate(storyId: string, data: { name: string }, options: crudOptions = {
        reload: true
    }) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${storyId}/duplicate`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
            },
            body: data,
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object as iStory
        if (options.reload) {
            let items = this.items.getValue()
            items.push(row)
            this.items.next(items)
        }
        return row
    }
    async updateOrder(storyId: string, cardList: string[], options: crudOptions = {
        reload: true
    }) {
        if (!cardList) throw new Error('card undefined in add')
        options = assign({}, this.options, options)
        let body = { cards: cardList }
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`${storyId}/cards`),
            qs: this._paserQuery(options.query),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
            },
            body: body,
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object as iCard;
        let cards = this.api.card.items.getValue()
        cards.push(row)
        this.api.card.items.next(cards)
        // if (options.reload) {
        //     let items = this.items.getValue()
        //     const storyIndex = findIndex(items, { _id: storyId })
        //     items[storyIndex] = await this.getItem(storyId, { local: false, reload: true })
        //     this.items.next(items)
        // }
        return row
    }

    async addCard(storyId: string, card: iCard, options: crudOptions = {
        reload: true
    }) {
        if (!card) throw new Error('card undefined in add')
        options = assign({}, this.options, options)
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${storyId}/cards`),
            qs: this._paserQuery(options.query),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            body: card,
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object as iCard;
        let cards = this.api.card.items.getValue()
        cards.push(row)
        this.api.card.items.next(cards)
        // if (options.reload) {
        //     let items = this.items.getValue()
        //     const storyIndex = findIndex(items, { _id: storyId })
        //     items[storyIndex] = await this.getItem(storyId, { local: false, reload: true })
        //     this.items.next(items)
        // }
        return row
    }
    async deleteCards(storyId: string, cardIds: string[], options: crudOptions = {
        reload: true
    }) {
        if (!cardIds) throw new Error('Body must have card id array')
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl(`${storyId}/cards`),
            qs: this._paserQuery(options.query),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            body: { cardIds: cardIds },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object
        let cards = this.api.card.items.getValue()
        for (const card of cardIds) {
            let cardIndex = cards.findIndex(x => x._id == card)
            if (cardIndex > -1) {
                cards.splice(cardIndex, 1)
            }
        }
        this.api.card.items.next(cards)
        // if (options.reload) {
        //     let items = this.items.getValue()
        //     const storyIndex = findIndex(items, { _id: storyId })
        //     items[storyIndex] = await this.getItem(storyId, { local: false, reload: true })
        //     this.items.next(items)
        // }
        return row
    }
    async addReferral(storyId: string, referral: iReferral, options: crudOptions = {
        reload: true
    }) {
        if (!referral) throw new Error('Body require referral')
        options = assign({}, this.options, options)
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${storyId}/referral`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            body: referral,
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.result.object as iReferral
        let referrals = this.api.referral.items.getValue()
        referrals.push(row)
        this.api.referral.items.next(referrals)
        // if (options.reload) {
        //     let items = this.items.getValue()
        //     const storyIndex = findIndex(items, { _id: storyId })
        //     items[storyIndex] = await this.getItem(storyId, { local: false, reload: true })
        //     this.items.next(items)
        // }
        return row
    }
    async addkey(storyId: string, key: string, options: crudOptions = {
        reload: true
    }) {
        if (!key) throw new Error('key undefined in add')
        options = assign({}, this.options, options)
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${storyId}/keys`),
            qs: this._paserQuery(options.query),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            body: {
                key
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object as iDictionary;
        let dics = this.api.dictionary.items.getValue()
        dics.push(row)
        this.api.dictionary.items.next(dics)
        // if (options.reload) {
        //     let items = this.items.getValue()
        //     const storyIndex = findIndex(items, { _id: storyId })
        //     items[storyIndex] = await this.getItem(storyId, { local: false, reload: true })
        //     this.items.next(items)
        // }
        return row
    }
    async setIntentStories(params: {
        storyIds: string[],
        intent: string
    }) {
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`setIntentStories`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
            },
            body: params,
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        return res.results.object
    }
    
    async test(storyId: string) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${storyId}/testStory`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        return res.results
    }

    async updateQuickReplies(storyId: string, quickReplies: any[]) {
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`${storyId}`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,                 
            },
            body: {
                quickReplies: quickReplies
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        return res.results
    }

    async updateName(id: string, name: string, options?: crudOptions) {        
        if (!id) throw new Error('id undefined in edit')
        if (!name) throw new Error('data undefined in edit')
        let data = {
            $set: {
                name: name
            }
        }
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(id),
            qs: this._paserQuery(options.query),
            headers: merge({}, { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            }, options.headers),
            body: data,
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    async getListOtherApp(app_id, app_token) {
      let setting = {
        method: 'GET',
        uri: this.apiUrl(),
        headers: { //headers
            
            'app_id': app_id,
            'app_token': app_token,
        },
        json: true,
      }
      let res = await this.exec(setting)
      let row = res.results.objects.rows
      return row
  }
}