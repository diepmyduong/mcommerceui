import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'

export interface iSubscriberProperty extends iCrud {
    properties?: {
        display: string,
        key: string,
        type: string
    }[]
}

export class SubscriberProperty extends CrudAPI<iSubscriberProperty> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'subscriberProperty')
    }
}