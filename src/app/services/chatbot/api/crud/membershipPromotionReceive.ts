import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iSubscriber } from './subscriber';
import { iLuckyWheelGift } from './luckyWheel';
import { iPage } from 'app/services/chatbot';
import { iMembership } from 'app/services/chatbot/api/crud/membership';
import { iMembershipPromotion } from 'app/services/chatbot/api/crud/membershipPromotion';

export interface iMembershipPromotionReceive extends iCrud {
    page: string | iPage
    subscriber: string | iSubscriber
    member: string | iMembership
    promotion: string | iMembershipPromotion
    code: string
    expiredAt: Date
    isReceived: boolean
}

export class MembershipPromotionReceive extends CrudAPI<iMembershipPromotionReceive> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'membershipPromotionReceive')
        this.options.query = {
        }
    }

}