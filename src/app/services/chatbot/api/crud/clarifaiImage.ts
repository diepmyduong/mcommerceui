import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { iStory } from 'app/services/chatbot/api/crud/story'
import { iApp } from 'app/services/chatbot';

export interface iClarifaiImage extends iCrud {
    app: string | iApp
    story: string | iStory
    images: {
        url: string,
        clarifaiId: string,
        metadata: any
    }[]
}

export class ClarifaiImage extends CrudAPI<iClarifaiImage> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'clarifaiImage')
    }

    async upload(params: {
        clarifaiImageId: string,
        url: string,
        metadata: any
    }){
        const { clarifaiImageId, url, metadata } = params
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${clarifaiImageId}/image`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            },
            body: {
                url, metadata
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        return res.results.object
    }
    async updateImage(params: {
        clarifaiImageId: string,
        imageId: string,
        metadata: any
    }){
        const { clarifaiImageId, imageId, metadata } = params
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`${clarifaiImageId}/image`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            },
            body: {
                imageId , metadata
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        return res.results.object
    }

    async deleteImage(params: {
        clarifaiImageId: string,
        imageId: string
    }){
        const { clarifaiImageId, imageId } = params
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl(`${clarifaiImageId}/image`),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            },
            body: {
                imageId
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        return res.results.object
    }


}