import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'

import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iPage } from 'app/services/chatbot/api/crud/page'
import { iTopic } from 'app/services/chatbot/api/crud/topic';

export interface iTopicEventData {
    type: 'story' | 'new_story'
    story: any
}
export interface iTopicEvent extends iCrud {
    name?: string
    description?: string
    page?: string | iPage
    topic?: string | iTopic
    eventData?: iTopicEventData
    eventSchedule?: string | Date
    eventRepeatEvery?: string
    jobId?: string
    endDate?: Date
}

export interface iTopicEventStatus {
    scheduled: boolean,
    completed: boolean,
    failed: boolean,
    queued: boolean,
    repeating: boolean,
    running: boolean,
    failCount: number,
    failedAt: Date,
    failReason: any,
    lastFinishedAt: Date,
    lastRunAt: Date,
    lockedAt: Date,
    nextRunAt: Date
}

export class TopicEvent extends CrudAPI<iTopicEvent> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'topicEvent')
    }

    async getStatus(eventId: string) {
        let setting = {
            method: 'GET',
            uri: this.apiUrl(`${eventId}/status`),
            headers: { //headers
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object as iTopicEventStatus
        return row
    }

    async stopEvent(eventId: string) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${eventId}/stop`),
            headers: { //headers
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object as iTopicEvent
        return row
    }

    async continueEvent(eventId: string) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${eventId}/continue`),
            headers: { //headers
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
                 
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object as iTopicEvent
        return row
    }
}