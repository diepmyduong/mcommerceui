import { CrudAPI, iCrud } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service';

export interface iTopicQuickMessage extends iCrud {
   
}

export class TopicQuickMessage extends CrudAPI<iTopicQuickMessage> {
    constructor(
        public api: ChatbotApiService
    ){
        super(api,'topicQuickMessage')
    }
}