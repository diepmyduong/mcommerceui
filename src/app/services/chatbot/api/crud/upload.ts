import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iCrud, CrudAPI } from "app/services/chatbot/api/crud";
import Axios from "axios";

export interface iUpload extends iCrud {

}

export class Upload extends CrudAPI<iUpload> {
  constructor(
    public api: ChatbotApiService
  ) {
    super(api, "upload")
  }
  async uploadImage(file) {
    return new Promise((resolve, reject)=>{
      try {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', "https://api.imgur.com/3/image", true);
        xhr.setRequestHeader("Authorization", "Client-ID d4de8224fa0042f");
        xhr.setRequestHeader("Content-Type", "multipart/form-data");
        xhr.onreadystatechange = function () {
            if (xhr.readyState==4 && xhr.status==200) {
              
              let responseObject = JSON.parse(xhr.response)
              resolve(responseObject.data)
            }
        }
        xhr.onerror = function() {
          reject(new Error('Lỗi API'))
        }
        xhr.send(file);
      } catch (err) {
        reject(err)
      }
    });
  }
  async uploadImageData(dataURL) {
    const res = await Axios.post('https://api.imgur.com/3/image',{
      image: dataURL
    }, {
      headers: {
        Authorization: "Client-ID d4de8224fa0042f",
      }
    });
    return res.data;
  }
}
