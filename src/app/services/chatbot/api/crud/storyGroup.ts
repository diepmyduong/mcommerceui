import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { iCard } from 'app/services/chatbot/api/crud/card'
import { iStory } from './story';
import { merge, assign } from 'lodash-es'

export interface iSequenceEvent {
    period:  "minutes" | "hours" | "days",
    value: number
}

export interface iStoryGroup extends iCrud {
    name?: string
    page?: string
    stories?: (iStory | string)[]
    type?: 'normal' | 'sequence' | 'broadcast'
    sequenceEvents?: iSequenceEvent[]
}

export class StoryGroup extends CrudAPI<iStoryGroup> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'storyGroup')
    }

    async copy(app_id, app_token, groupId) {
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${groupId}/copy`),
            headers: { //headers
                
                "content-type": "application/json",
                'app_id': app_id,
                'app_token': app_token,
            },
            json: true
        }
        let res = await this.exec(setting)
        let row = res.results.object as iStoryGroup
        return row
    }
    
    async updateOrder(storyId: string, cardList: string[], options: crudOptions = {
        reload: true
    }) {
        if (!cardList) throw new Error('card undefined in add')
        options = assign({}, this.options, options)
        let body = { cards: cardList }
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(`${storyId}/cards`),
            qs: this._paserQuery(options.query),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
            },
            body: body,
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object as iCard;
        let cards = this.api.card.items.getValue()
        cards.push(row)
        this.api.card.items.next(cards)
        // if (options.reload) {
        //     let items = this.items.getValue()
        //     const storyIndex = findIndex(items, { _id: storyId })
        //     items[storyIndex] = await this.getItem(storyId, { local: false, reload: true })
        //     this.items.next(items)
        // }
        return row
    }

    async addCard(storyId: string, card: iCard, options: crudOptions = {
        reload: true
    }) {
        if (!card) throw new Error('card undefined in add')
        options = assign({}, this.options, options)
        let setting = {
            method: 'POST',
            uri: this.apiUrl(`${storyId}/cards`),
            qs: this._paserQuery(options.query),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
            },
            body: card,
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object as iCard;
        let cards = this.api.card.items.getValue()
        cards.push(row)
        this.api.card.items.next(cards)
        return row
    }

    async deleteCards(storyId: string, cardIds: string[], options: crudOptions = {
        reload: true
    }) {
        if (!cardIds) throw new Error('Body must have card id array')
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl(`${storyId}/cards`),
            qs: this._paserQuery(options.query),
            headers: { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken,
            },
            body: { cardIds: cardIds },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object
        let cards = this.api.card.items.getValue()
        for (const card of cardIds) {
            let cardIndex = cards.findIndex(x => x._id == card)
            if (cardIndex > -1) {
                cards.splice(cardIndex, 1)
            }
        }
        this.api.card.items.next(cards)
        // if (options.reload) {
        //     let items = this.items.getValue()
        //     const storyIndex = findIndex(items, { _id: storyId })
        //     items[storyIndex] = await this.getItem(storyId, { local: false, reload: true })
        //     this.items.next(items)
        // }
        return row
    }

    async updateName(id: string, name: string, options?: crudOptions) {        
        if (!id) throw new Error('id undefined in edit')
        if (!name) throw new Error('data undefined in edit')
        let data = {
            $set: {
                name: name
            }
        }
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(id),
            qs: this._paserQuery(options.query),
            headers: merge({}, { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            }, options.headers),
            body: data,
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
}