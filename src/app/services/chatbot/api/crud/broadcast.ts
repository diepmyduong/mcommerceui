import { CrudAPI, iCrud, crudOptions } from 'app/services/chatbot/api/crud'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { merge, remove } from 'lodash-es'
export interface iBroadcast extends iCrud {
    status?: 'ERROR' | 'SCHEDULED' | 'IN_PROGRESS' | 'FINISHED' | 'CANCELED'
    page?:string,
    broadcastId?:string,
    type?:string,
    estimateId?:string,
    reach?:number,
    labelId?:string,
    schedule?:Date,
    story?:string,
    broadcasts: any[]
}

export class Broadcast extends CrudAPI<iBroadcast> {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, 'broadcast')
        this.options.query = {
        }
    }
    async deleteSchedule(id: string, options?: crudOptions) {
        if (!id) throw new Error('id undefined in delete')
        options = merge({}, this.options, options)
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl(`${id}/cancel`),
            qs: this._paserQuery(options.query),
            headers: merge({}, { //headers
                
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            }, options.headers),
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        if (options.reload) {
            let items = this.hashCache[this.activeHashQuery] ? this.hashCache[this.activeHashQuery].items : this.items.getValue()
            const removed = remove(items, function (item) {
                return item._id == id;
            })
            if (removed.length > 0) {
                if (this.hashCache[this.activeHashQuery]) {
                    this.hashCache[this.activeHashQuery].items = items
                    this.hashCache[this.activeHashQuery].expired = this.moment().add(5, 'minutes').toDate()
                }
                this.items.next(items)
            } else {
                await this.getList({ local: false, query: this.activeQuery })
            }
            if (this.hashCache[this.activeHashQuery]) {
                this.hashCache = {
                    [this.activeHashQuery]: this.hashCache[this.activeHashQuery]
                }
            }
    }
    return true
  }
  async cancel(id) {
      let setting = {
          method: 'DELETE',
          uri: this.apiUrl(`${id}/cancel`),
          headers: { //headers
              
              "content-type": "application/json",
              'x-api-key': this.api.chatbotAuth.appToken
          },
          json: true
      }
      let res = await this.exec(setting)
      let row = res.results.object
      return row
  }
}