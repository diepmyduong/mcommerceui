import { BaseAPI } from 'app/services/chatbot/api/base'
import { BehaviorSubject } from 'rxjs'
import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import { merge, remove } from 'lodash-es'
import * as hash from 'object-hash'
import * as moment from 'moment'
export interface crudOptions {
    reload?: boolean
    local?: boolean
    query?: crudQuery
    headers?: any
}

export interface crudQuery {
    filter?: any
    fields?: any
    order?: any
    items?: any[]
    limit?: number
    page?: number
    offset?: number
    populates?: any[]
    [x: string]: any
}

export interface iCrud {
    _id?: string
    createAt?: Date
    status?: string
    updateAt?: Date
    [key: string]: any
}

export interface iCrudPagination {
    current_page?: number
    limit?: number
    next_page?: number
    prev_page?: number
    totalItems?: number
}

export class CrudAPI<T> extends BaseAPI {
    constructor(
        public api: ChatbotApiService,
        public moduleName: string
    ) {
        super(api, moduleName)
        this.options = {
            reload: true, //Auto update items list each request, 'false' to get result only.
            local: true, //Get local data instant request server.
            query: {}
        }
        this.items = new BehaviorSubject<T[]>([])
        this.pagination = {
            current_page: 1,
            limit: 0,
            next_page: 2,
            prev_page: 0,
            totalItems: 0
        }
        this.hashCache = {}
    }
    options: crudOptions
    items: BehaviorSubject<T[]>
    pagination: iCrudPagination
    localAppId: string
    hashCache: {
        [hash: string]: {
            pagination: iCrudPagination
            items: T[]
            expired: Date,
            isGetList: boolean
        }
    }
    activeHashQuery: string
    activeQuery: crudQuery = {}
    requestCache: {
        [hash: string]: {
            items: any[]
            expired: Date,
            [key: string]: any
        }
    } = {}
    activeRequestHash: string
    activeRequestSetting: any
    requestReload: boolean = false

    clear() {
        this.hashCache = {}
        this.activeHashQuery = ''
        this.activeQuery = {}
        this.activeRequestHash = ''
        this.activeRequestSetting = null
        this.requestCache = {}
    }

    get hash() { return hash }
    get moment() { return moment }

    setRequestReload() { this.requestReload = true }

    async getList(options?: crudOptions): Promise<T[]> {
        options = merge({}, this.options, options)
        let setting = {
            uri: this.apiUrl(),
            qs: this._paserQuery(options.query),
            headers: merge({}, { //headers
                'x-api-key': this.api.chatbotAuth.appToken
            }, options.headers),
            json: true // Automatically parses the JSON string in the response
        }
        const hashedQuery = hash(options.query)
        this.activeHashQuery = hashedQuery
        this.activeQuery = options.query
        if (options.local && this.localAppId == this.api.chatbotAuth.app._id
            && this.hashCache[hashedQuery] && this.hashCache[hashedQuery].expired > new Date()
            && this.hashCache[hashedQuery].isGetList && !this.requestReload) {
            let items = this.hashCache[hashedQuery].items
            this.pagination = this.hashCache[hashedQuery].pagination
            return items
        }
        if (this.localAppId != this.api.chatbotAuth.app._id)
            this.localAppId = this.api.chatbotAuth.app._id
        let res = await this.exec(setting)
        let { results, pagination } = res
        let rows = results.objects.rows as T[]
        if (options.reload) {
            this.pagination = pagination
            this.pagination.totalItems = results.objects.count || 0
            this.hashCache[hashedQuery] = {
                pagination: this.pagination,
                items: rows,
                expired: moment().add(5, 'minutes').toDate(),
                isGetList: true
            }
            this.items.next(rows)
        }
        return rows;
    }

    async setItems(items: any[], options?:crudOptions) {
        options = merge({}, this.options, options)
        const hashedQuery = hash(options.query)
        if (!this.hashCache[hashedQuery]) this.hashCache[hashedQuery] = {
            pagination: {},
            items: [],
            expired: moment().add(5, 'minutes').toDate(),
            isGetList: false,
        }
        this.hashCache[hashedQuery].items = items
    }

    async setItem(item: any, options?: crudOptions) {
        options = merge({}, this.options, options)
        const hashedQuery = hash(options.query)
        if (!this.hashCache[hashedQuery]) this.hashCache[hashedQuery] = {
            pagination: {},
            items: [],
            expired: moment().add(5, 'minutes').toDate(),
            isGetList: false,
        }
        let index = this.hashCache[hashedQuery].items.findIndex(x => (x as any)._id == item._id)
        if (index > -1) {
            this.hashCache[hashedQuery].items[index] = item
        } else {
            this.hashCache[hashedQuery].items.push(item)
        }
    }

    async getItem(id: string, options?: crudOptions): Promise<T> {
        if (!id) throw new Error("id undefined in getItem")
        options = merge({}, this.options, options)
        let setting = {
            method: 'GET',
            uri: this.apiUrl(id),
            qs: this._paserQuery(options.query),
            headers: merge({}, { //headers
                'x-api-key': this.api.chatbotAuth.appToken
            }, options.headers),
            json: true // Automatically parses the JSON string in the response
        }
        const hashedQuery = hash(options.query)
        if (options.local && this.hashCache[hashedQuery] && this.hashCache[hashedQuery].expired > new Date()) {
            let items = this.hashCache[hashedQuery].items as T[]
            let item = items.find(x => (x as any)._id == id)
            if (item) return item
        }
        let res = await this.exec(setting)
        let row = res.results.object as T
        if (options.reload) {
            if (hashedQuery === this.activeHashQuery) {
                let items = this.hashCache[this.activeHashQuery] ? this.hashCache[this.activeHashQuery].items : this.items.getValue()
                let index = items.findIndex(x => (x as any)._id == id)
                if (index > -1) {
                    items[index] = row
                } else {
                    items.push(row)
                }
                if (this.hashCache[this.activeHashQuery]) {
                    this.hashCache[this.activeHashQuery].items = items
                    this.hashCache[this.activeHashQuery].expired = moment().add(5, 'minutes').toDate()
                    this.hashCache = {
                        [this.activeHashQuery]: this.hashCache[this.activeHashQuery]
                    }
                }

                this.items.next(items)
            } else {
                if (!this.hashCache[hashedQuery]) this.hashCache[hashedQuery] = {
                    pagination: {},
                    items: [],
                    expired: moment().add(5, 'minutes').toDate(),
                    isGetList: false,
                }
                let index = this.hashCache[hashedQuery].items.findIndex(x => (x as any)._id == id)
                if (index > -1) {
                    this.hashCache[hashedQuery].items[index] = row
                } else {
                    this.hashCache[hashedQuery].items.push(row)
                }
            }

        }
        return row
    }

    async add(data: T, options?: crudOptions): Promise<T> {
        if (!data) throw new Error('data undefined in add')
        options = merge({}, this.options, options)
        let setting = {
            method: 'POST',
            uri: this.apiUrl(),
            qs: this._paserQuery(options.query),
            headers: merge({}, { //headers
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            }, options.headers),
            body: data,
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object as T;
        if (options.reload) {
            let items = this.hashCache[this.activeHashQuery] ? this.hashCache[this.activeHashQuery].items : this.items.getValue()
            if (this.pagination.limit == 0 || items.length < this.pagination.limit) {
                this.items.next(items)
                if (this.hashCache[this.activeHashQuery]) {
                    this.hashCache[this.activeHashQuery].items = items
                    this.hashCache[this.activeHashQuery].expired = moment().add(5, 'minutes').toDate()
                }
                items.push(row)

            }
            if (this.hashCache[this.activeHashQuery]) {
                this.hashCache = {
                    [this.activeHashQuery]: this.hashCache[this.activeHashQuery]
                }
            }
        }
        return row
    }

    async update(id: string, data: T, options?: crudOptions): Promise<T> {
        if (!id) throw new Error('id undefined in edit')
        if (!data) throw new Error('data undefined in edit')
        options = merge({}, this.options, options)
        let setting = {
            method: 'PUT',
            uri: this.apiUrl(id),
            qs: this._paserQuery(options.query),
            headers: merge({}, { //headers
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            }, options.headers),
            body: data,
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object as T
        if (options.reload) {
            let items = this.hashCache[this.activeHashQuery] ? this.hashCache[this.activeHashQuery].items : this.items.getValue()
            let index = items.findIndex(x => (x as any)._id == id)
            if (index > -1) {
                items[index] = row
                if (this.hashCache[this.activeHashQuery]) {
                    this.hashCache[this.activeHashQuery].items = items
                    this.hashCache[this.activeHashQuery].expired = moment().add(5, 'minutes').toDate()
                }
                this.items.next(items)
            }
            if (this.hashCache[this.activeHashQuery]) {
                this.hashCache = {
                    [this.activeHashQuery]: this.hashCache[this.activeHashQuery]
                }
            }

        }
        return row
    }

    async delete(id: string, options?: crudOptions, data?: any) {
        if (!id) throw new Error('id undefined in delete')
        options = merge({}, this.options, options)
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl(id),
            qs: this._paserQuery(options.query),
            headers: merge({}, { //headers
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            }, options.headers),
            body: data,
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        if (options.reload) {
            let items = this.hashCache[this.activeHashQuery] ? this.hashCache[this.activeHashQuery].items : this.items.getValue()
            const removed = remove(items, function (item) {
                return item._id == id;
            })
            if (removed.length > 0) {
                if (this.hashCache[this.activeHashQuery]) {
                    this.hashCache[this.activeHashQuery].items = items
                    this.hashCache[this.activeHashQuery].expired = moment().add(5, 'minutes').toDate()
                }
                this.items.next(items)
            } else {
                await this.getList({ local: false, query: this.activeQuery })
            }
            if (this.hashCache[this.activeHashQuery]) {
                this.hashCache = {
                    [this.activeHashQuery]: this.hashCache[this.activeHashQuery]
                }
            }

        }
        return true
    }

    async deleteAll(ids: string[], options?: crudOptions) {
        if (!ids) throw new Error('ids undefined in deleteAll')
        let setting = {
            method: 'DELETE',
            uri: this.apiUrl(),
            qs: this._paserQuery(merge({}, {
                items: ids
            }, options.query)),
            headers: merge({}, { //headers
                "content-type": "application/json",
                'x-api-key': this.api.chatbotAuth.appToken
            }, options.headers),
            json: true // Automatically parses the JSON string in the response
        }
        let res: any = await this.exec(setting)
        if (options.reload) {
            let items = this.hashCache[this.activeHashQuery] ? this.hashCache[this.activeHashQuery].items : this.items.getValue()
            const removed = remove(items, function (item) {
                return ids.indexOf(item._id) !== -1
            });
            if (removed.length > 0) {
                if (this.hashCache[this.activeHashQuery]) {
                    this.hashCache[this.activeHashQuery].items = items
                    this.hashCache[this.activeHashQuery].expired = moment().add(5, 'minutes').toDate()
                }
                this.items.next(items)
            } else {
                await this.getList({ local: false, query: this.activeQuery })
            }
            if (this.hashCache[this.activeHashQuery]) {
                this.hashCache = {
                    [this.activeHashQuery]: this.hashCache[this.activeHashQuery]
                }
            }

        }
        return true
    }

    protected _paserQuery(query: crudQuery = {}) {
        let parsedQuery = merge({}, query)
        if (query.filter) {
            parsedQuery.filter = JSON.stringify(query.filter);
        }
        if (query.order) {
            parsedQuery.order = JSON.stringify(query.order);
        }
        if (query.scopes) {
            parsedQuery.scopes = JSON.stringify(query.scopes);
        }
        if (query.fields) {
            parsedQuery.fields = JSON.stringify(query.fields);
        }
        if (query.items) {
            parsedQuery.items = JSON.stringify(query.items);
        }
        if (query.populates) {
            parsedQuery.populates = JSON.stringify(query.populates)
        }
        return parsedQuery;
    }
}