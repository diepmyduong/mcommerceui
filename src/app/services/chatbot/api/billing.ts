import { ChatbotApiService } from "app/services/chatbot";
import { BaseAPI } from "app/services/chatbot/api/base";
import { iCrudPagination, crudOptions, crudQuery} from './crud'
import { BehaviorSubject } from "rxjs";
import { merge } from 'lodash-es'

export class Billing extends BaseAPI {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, "billing")
        this.options = {
            reload: true, //Auto update items list each request, 'false' to get result only.
            local: true, //Get local data instant request server.
            query: {}
        }
        this.items = new BehaviorSubject<any[]>([])
        this.pagination = {
            current_page: 1,
            limit: 0,
            next_page: 2,
            prev_page: 0,
            totalItems: 0
        }
        this.hashCache = {}
    }
    options: crudOptions
    items: BehaviorSubject<any[]>
    pagination: iCrudPagination
    localAppId: string
    hashCache: {
        [hash: string]: {
            pagination: iCrudPagination
            items: any[]
            expired: Date,
            isGetList: boolean
        }
    }
    activeHashQuery: string
    activeQuery: crudQuery = {}
    requestCache: {
        [hash: string]: {
            items: any[]
            expired: Date,
            [key: string]: any
        }
    } = {}
    activeRequestHash: string
    activeRequestSetting: any
    async getList(options?: crudOptions) {
        options = merge({}, this.options, options)
        let setting = {
            uri: this.apiUrl(),
            qs: this._paserQuery(options.query),
            headers: { //headers
                'access_token': await this.api.chatbotAuth.firebaseToken
            },
            json: true // Automatically parses the JSON string in the response
        }
        if (options.local) {
            let items = this.items.getValue()
            if (items.length > 1) { // If local empty, request to server
                return items
            }
        }
        let res = await this.exec(setting)
        let { results, pagination } = res
        let rows = results.objects.rows as any[]
        if (options.reload) {
            this.pagination = pagination
            this.pagination.totalItems = results.objects.count || 0
            this.items.next(rows)
        }
        return rows;
    }
    async getHistory(options?: crudOptions) {
        options = merge({}, this.options, options)
        let setting = {
            uri: this.apiUrl("history"),
            qs: this._paserQuery(options.query),
            headers: { //headers
                'access_token': await this.api.chatbotAuth.firebaseToken
            },
            json: true // Automatically parses the JSON string in the response
        }
        if (options.local) {
            let items = this.items.getValue()
            if (items.length > 1) { // If local empty, request to server
                return items
            }
        }
        let res = await this.exec(setting)
        let { results, pagination } = res
        let rows = results.objects.rows as any[]
        // if (options.reload) {
        //     this.pagination = pagination
        //     this.pagination.totalItems = results.objects.count || 0
        //     this.items.next(rows)
        // }
        return rows;
    }
    async payForBill(billId: string, channel: "zalopay" | "vnpay"){
        let setting = {
            uri: this.apiUrl("payForBill"),
            method: "POST",
            headers: { //headers
                'access_token': await this.api.chatbotAuth.firebaseToken
            },
            body: { 
                billId: billId,
                channel: channel
            },
            json: true // Automatically parses the JSON string in the response
        }
        let res = await this.exec(setting)
        let row = res.results.object
        return row
    }
    
    protected _paserQuery(query: crudQuery = {}) {
        let parsedQuery = merge({}, query)
        if (query.filter) {
            parsedQuery.filter = JSON.stringify(query.filter);
        }
        if (query.order) {
            parsedQuery.order = JSON.stringify(query.order);
        }
        if (query.scopes) {
            parsedQuery.scopes = JSON.stringify(query.scopes);
        }
        if (query.fields) {
            parsedQuery.fields = JSON.stringify(query.fields);
        }
        if (query.items) {
            parsedQuery.items = JSON.stringify(query.items);
        }
        if (query.populates) {
            parsedQuery.populates = JSON.stringify(query.populates)
        }
        return parsedQuery;
    }
}