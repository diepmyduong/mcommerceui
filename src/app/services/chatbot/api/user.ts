import { BaseAPI } from "app/services/chatbot/api/base";
import { ChatbotApiService } from "app/services/chatbot/chatbot-api.service"
import * as firebase from 'firebase/app'
import 'firebase/database'

export class User extends BaseAPI {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, "user")
    }
    intros: any
    states: {
        isSpreadsheetTemplate: any
        spreadSheet: any
        globalVariables: any
    }

    async getIntros() {
        if(!this.intros) {
            const db = this.api.chatbotAuth.firebaseDatabase
            const introsSnap = await db.ref(`intros/${this.api.chatbotAuth.firebaseUser.uid}`).once('value') as firebase.database.DataSnapshot
            this.intros = introsSnap.val() || {}
        }
        return this.intros
    }

    async disableIntro(introKey: string) {
        this.intros[introKey] = true
        const db = this.api.chatbotAuth.firebaseDatabase
        return await db.ref(`intros/${this.api.chatbotAuth.firebaseUser.uid}/${introKey}`).set(true)
    }

    async isIntroDisable(introKey: string) {
        const intros = await this.getIntros()
        return !!intros[introKey]
    }

    async getStates(appId: string) {
        const db = this.api.chatbotAuth.firebaseDatabase
        const statesSnap = await db.ref(`states/${appId}`).once('value') as firebase.database.DataSnapshot
        this.states = statesSnap.val() || {
            isSpreadsheetTemplate: false,
            spreadSheet: false,
            globalVariables: false
        }
        return this.states
    }

    async removeStates(appId: string) {        
        const db = this.api.chatbotAuth.firebaseDatabase
        return await db.ref(`states/${appId}`).remove()
    }

    async disableState(appId: string, state: string) {
        this.states[state] = true
        const db = this.api.chatbotAuth.firebaseDatabase
        return await db.ref(`states/${appId}/${state}`).set(true)
    }

    async isStateDisable(appId: string, state: string) {
        const states = await this.getStates(appId)
        return !!states[state]
    }

    async setSpreadsheetFirst(appId: string) {
        const db = this.api.chatbotAuth.firebaseDatabase
        return await db.ref(`states/${appId}/isSpreadsheetTemplate`).set(true)
    }
}