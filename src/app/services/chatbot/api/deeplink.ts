import { ChatbotApiService } from "app/services/chatbot";
import { BaseAPI } from "app/services/chatbot/api/base";
declare var Buffer: any;
export class DeepLink extends BaseAPI {
    constructor(
        public api: ChatbotApiService
    ) {
        super(api, "deeplink")
    }

    async getGoogleMapDeeplink(params: {
        to: {
            latitude: string,
            longitude: string
        }
    }) {
        const { to } = params
        const query = {
            tlat: to.latitude,
            tlng: to.longitude
        }
        return this.apiUrl(`gmap?tlat=${to.latitude}&tlng=${to.longitude}`)
    }

    async getImageBufferLink(params: {
        src: string
    }) {
        const { src } = params
        return this.apiUrl(`image?base=${(new Buffer(src)).toString('base64')}`)
    }
    async getSimpleCheckouLink(payload) {
        return this.api.chatbotConfig.viewUrl('innoway/simpleCheckout?p='+JSON.stringify(payload))
    }
    async getVoiceBufferLink(params: { say: string }) {
        return this.apiUrl(`voice?base=${(new Buffer(params.say)).toString('base64')}`)
    }
}