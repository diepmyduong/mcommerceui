import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service'
import * as request from 'request-promise'
import * as Console from 'console-prefix'

export class BaseAPI {
    constructor(
        public api: ChatbotApiService,
        public moduleName: string 
    ){
    }

    get log() {
        return Console(`[API ${this.moduleName}]`).log
    }

    apiUrl(path:string = ""){
        return this.api.chatbotConfig.apiUrl(`${this.moduleName}/${path}`)
    }

    crossModuleUrl(path:string = ""){
        return this.api.chatbotConfig.apiUrl(`${path}`)
    }

    //Call API
    protected async exec(option){
        if(!option) throw new Error("option undefined in exec")
        try {
            return await request(option)
        }catch(resError){
            console.error("Lỗi", "API ERROR")
            throw resError;
        }
    }
}