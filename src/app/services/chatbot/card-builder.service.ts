import { ChatbotApiService } from 'app/services/chatbot/chatbot-api.service';
import { iPhoto } from 'app/services/chatbot/api/crud/page';
export class CardBuilderService {

  defaultImage = "https://i.imgur.com/qgxSCqD.png";

  constructor(
    public chatbotApi: ChatbotApiService
  ) { 
  }

  text = () => ({
    type: 'text',
    option: {
      text: "Tin nhắn mẫu"
    }
  })

  image = () => ({
    type: 'image',
    option: {
      attachment: {
        type: 'image',
        payload: {
          url: this.defaultImage
        }
      }
    }
  })

  audio = () => ({
    type: 'audio',
    option: {
      attachment: {
        type: 'audio',
        payload: {
          url: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3"
        }
      }
    }
  })

  video = () => ({
    type: 'video',
    option: {
      attachment: {
        type: 'video',
        payload: {
          url: "http://techslides.com/demos/sample-videos/small.mp4"
        }
      }
    }
  })

  button = () => ({
    type: "button",
    option: {
      attachment: {
        type: "template",
        payload: {
          template_type: "button",
          text: "Tin nhắn mẫu",
          buttons: [
          //   {
          //   type: "web_url",
          //   title: "Google Search",
          //   url: "https://google.com",
          //   messenger_extensions: false
          // }
          ]
        }
      }
    }
  })

  media_image = () => ({
    type: "media_image",
    option: {
      preview: '',
      attachment: {
        type: "template",
        payload: {
          template_type: "media",
          elements: [{
            media_type: "image",
            url: '',
            buttons: [{
              type: "web_url",
              title: "Nút Web URL",
              url: "https://google.com",
              messenger_extensions: false
            }],
          }]
        }
      }
    }
  })

  media_video = () => ({
    type: "media_video",
    option: {
      preview: '',
      attachment: {
        type: "template",
        payload: {
          template_type: "media",
          elements: [{
            media_type: "video",
            url: "",
            buttons: [{
              type: "web_url",
              title: "Nút Web URL",
              url: "https://google.com",
              messenger_extensions: false
            }]
          }]
        }
      }
    }
  })

  generic = () => ({
    type: "generic",
    option: {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          image_aspect_ratio: "horizontal",
          elements: [{
            title: "Tiêu đề",
            subtitle: "Mô tả ngắn",
            image_url: this.defaultImage,

            buttons: [{
              type: "web_url",
              title: "Nút Web URL",
              url: "https://google.com",
              messenger_extensions: false
            }]
          }]
        }
      }
    }
  })

  list = () => ({
    type: "list",
    option: {
      attachment: {
        type: "template",

        payload: {
          template_type: "list",
          top_element_style: "large",
          elements: [{
            title: "Tiêu đề",
            subtitle: "Mô tả ngắn",
            image_url: this.defaultImage,
          },
          {
            title: "Tiêu đề",
            subtitle: "Mô tả ngắn",
            image_url: this.defaultImage,
          }]
        }
      }
    }
  })

  generic_categories = () => ({
    type: "generic_categories",
    option: {
      query: {
        limit: 5,
        page: 1,
        filter: { name: "" }
      }
    }
  })

  generic_products = () => ({
    type: "generic_products",
    option: {
      query: {
        limit: 5,
        page: 1,
        filter: {
          name: ""
        }
      }
    }
  })

  list_categories = () => ({
    type: "list_categories",
    option: {
      query: {
        limit: 4,
        page: 1,
        filter: { name: "" }
      }
    }
  })

  list_products = () => ({
    type: "list_products",
    option: {
      query: {
        limit: 4,
        page: 1,
        filter: {
          name: ""
        }
      }
    }
  })

  activity_input = () => ({
    type: 'activity_input',
    option: {
      text: "Vui lòng nhập email",
      key: "email"
    }
  })

  activity_quickreply = () => ({
    type: 'activity_quickreply',
    option: {
      text: "Câu hỏi mẫu",
      key: "response",
      quick_replies: [{
        type: "quick_reply_activity",
        title: "Câu A",
        value: "a"
      }]
    }
  })

  activity_range = () => ({
    type: 'activity_range',
    option: {
      text: "Nhập Số lượng",
      key: "number",
      from: 1,
      to: 10,
      step: 1
    }
  })

  action_api = () => ({
    type: "action_api",
    option: {
      request: {
        method: "GET",
      },
      isCallback: false
    }
  })

  action_update = () => ({
    type: "action_update",
    option: {
      fields: []
    }
  })

  generic_dynamic = () => ({
    type: "generic_dynamic",
    option: {
      data: "{{items}}",
      mapping: {
        title: "$title",
        subtitle: "$subtitle",
        image: "$imageUrl",
        buttons: [{
          type: "web_url",
          title: "$btnTitle",
          url: "$urlLink",
          messenger_extensions: false
        }]
      },
      offset: 0,
      limit: 5
    }
  })

  activity_qrcode = () => ({
    type: 'activity_qrcode',
    option: {
      text: "Hãy mở camera và chụp lấy hình ảnh chứa QR code",
      key: "qr"
    }
  })

  action_json_input = () => ({
    type: "action_json_input",
    option: {
      key: "json",
      json: {}
    }
  })

  activity_media = () => ({
    type: "activity_media",
    option: {
      text: "Hãy gửi hình ảnh",
      key: "file",
    }
  })

  activity_dynamic_quickreply = () => ({
    type: "activity_dynamic_quickreply",
    option: {
      text: "Câu hỏi mẫu",
      key: "response",
      data: "{{items}}",
      mapping: {
        title: "<<title>>",
        value: "<<value>>",
        image: "<<imageUrl>>"
      },
      offset: 0,
      limit: 10,
      type: "activity"
    }
  })

  action_parse_html = () => ({
    type: "action_parse_html",
    option: {
      key: "links",
      filter: ["a[href]", {
        text: "$doc.text()",
        href: "$doc.attr('href')"
      }],
      url: "http://your-website.com"
    }
  })

  og_video_call = () => ({
    type: "og_video_call",
    option: {
      title: "Gọi Video",
      image: "https://cdn1.iconfinder.com/data/icons/social-media-2056/128/1-05-512.png",
      description: "Hãy nhấp vào đây đẻ thực hiện cuộc gọi"
    }
  })

  activity_dynamic_form = () => ({
    type: "activity_dynamic_form", 
    option: {
      "key": "form",
      "message": "Vui lòng chọn nút phía dưới và nhập đầy đủ thông tin.",
      "button": "Nhập thông tin",
      "title": "Mẫu Thông Tin",
      "allowSkip": false,
      "submitButton": "Gửi",
      "schema": {
        "simple": true,
        "schema": {
          "name": "Họ và tên",
          "gender":"$select:Nam,Nữ,Khác: Giới tính",
          "email": "$email:Email",
          "phone": "$tel:Điện thoại",
          // "province": "$province: Tỉnh/Thành",
          // "district": "$district: Quận/Huyện",
          // "ward": "$ward: Phường/Xã",
          "address": "Địa chỉ nhà"
        }
      }
    }
  })

  action_notify = () => ({
    type: 'action_notify',
    option: {
      text: "{{name}} đang tương tác!"
    }
  })

  action_innoway_api = () => ({
    type: 'action_innoway_api',
    option: {
      request: {
        method: 'GET',
        uri: 'api/v1/product_category',
        qs: {
          fields: `["$all"]`,
          limit: 5
        }
      },
      isCallback: true,
      key: 'categories',
      sourceKey: 'results.objects.rows'
    }
  })

  barcode = async () => ({
    type: 'image',
    option: {
      attachment: {
        type: 'image',
        payload: {
          url: await this.chatbotApi.deeplink.getImageBufferLink({
            src: `https://mobiledemand-barcode.azurewebsites.net/barcode/image?content=1234567890&size=350&symbology=QR_CODE&format=png&text=true` 
          })
        }
      }
    }
  })

  voice_response = async () => ({
    type: 'audio',
    option: {
      attachment: {
        type: 'audio',
        payload: {
          url: await this.chatbotApi.deeplink.getVoiceBufferLink({
            say: "Xin chào, Tôi là Bot Mcom"
          })
        }
      }
    }
  })

  activity_button = () => ({
    type: "activity_button",
    option: {
      key: "response",
      attachment: {
        type: "template",
        payload: {
          template_type: "button",
          text: "Chọn câu trả lời",
          buttons: [{
            type: "postback",
            title: "Câu A",
            payload: JSON.stringify({
              type: "activity",
              data: "A"
            })
          }]
        }
      }
    }
  })

  typing_on = () => ({
    type: "typing_on",
    option: {
      duration: 5000,
      type: "typing_on"
    }
  })

  action_compare_image = () => ({
    type: "action_compare_image",
    option: {
      image: "{{file}}",
      key: "match"
    }
  })

  action_google_spreadsheet = () => ({
    type: "action_google_spreadsheet",
    option: {
      fields: ["{{name}}","{{id}}","{{gender}}"],
      ssId: "",
      sheet: "chatbot",
      method:  "POST",
      gSheetUser: undefined
    }
  })

  go_to_story = () => ({
    type: "go_to_story",
    option: {
      story: [],
      type: "go_to_story"
    }
  })

  action_add_label = () => ({
    type: "action_add_label",
    option: {
      name:""
    }
  })

  action_sequence = () => ({
    type: "action_sequence",
    option: {
      mode: 'subscribe',
      storyGroupId: '',
      isForce: false
    }
  })
  action_send_story = () => ({
    type: "action_send_story",
    option : {
      psids: [], 
      storyId: "", 
      context: {}    
    }
  })

  infusion_add_contact = () => ({
    type: "infusion_add_contact",
    option: {
      infusionId: "",
      data: {
        family_name: "",
        given_name: "",
        email_addresses: [
          {
            email: "",
            field: "EMAIL1"
          }
        ],
        phone_numbers: [
          {
            extension: "",
            field: "PHONE1",
            number: "",
            type: "Mobile"
          }
        ],
      },
      key: ""
    }
  })
  infusion_update_contact = () => ({
    type: "infusion_update_contact",
    option: {
      infusionId: "",
      data: {
        family_name: "",
        given_name: "",
        email_addresses: [
          {
            email: "",
            field: "EMAIL1"
          }
        ],
        phone_numbers: [
          {
            extension: "",
            field: "PHONE1",
            number: "",
            type: "Mobile"
          }
        ],
      },
      contactId: ""
    }
  })
  infusion_achive_goal = () => ({
    type: "infusion_achive_goal",
    option: {
      infusionId: "",
      data: {
        integration: "",
        callName: "",
        contactId: ""
      }
    }
  })
  activity_datepicker = () => ({
    type: "activity_datepicker", 
    option: {
      "key": "datepicker",
      "message": "Vui lòng chọn nút phía dưới và nhập đầy đủ thông tin.",
      "button": "Nhập thông tin",
      "title": "Mẫu Thông Tin",
      "allowSkip": false,
      "submitButton": "Xác nhận",
      "format":"HH:mm DD-MM-YYYY",
      "schema": {
        "simple": true,
        "schema": {}
      }
    }
  })
  action_e_voucher = () => ({
    type: "action_e_voucher",
    "option": {
      "mode": "generate_code",
      "key": "code",
      "eVoucherId": ""
    }
  })
  action_subscriber_tag = () => ({
    type: "action_subscriber_tag",
    "option": {
      "mode": "tag",
      "tag": ""
    }
  })
  action_member_point = () => ({
    type: "action_member_point",
    "option": {
      "mode": "inc",
      "key": "",
      "point": 0,
    }
  })
}
