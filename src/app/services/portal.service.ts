import { Injectable } from '@angular/core';
import { PortalContainerComponent } from 'app/chatgut-app/portals/portal-container/portal-container.component';

@Injectable()
export class PortalService {

  constructor() { }
  container: PortalContainerComponent;
  portals = {}

  regisContainer(container: PortalContainerComponent) {
    this.container = container;
  }

  regisPortal(portal: any) {
    const component = new portal()
    if(component.componentName) {
      this.portals[component.componentName] = portal
    }
  }

  clear() { this.container.clear(); }

  pushPortal(name: string, params?: any) {
    return this.container.pushPortal(this.portals[name] || name, params);
  }

  pushPortalAt(index: number, name: string, params?: any) {
    let isPush
    if (<MouseEvent>event) {
      isPush = (<MouseEvent>event).ctrlKey || (<MouseEvent>event).metaKey;
    } else {
      isPush = false
    }
    if (isPush) {
      return this.container.pushPortal(this.portals[name] || name, params);
    } else {
      return this.container.pushPortalAt(index, this.portals[name] || name, params);
    }
  }

  popPortal(index: number) {
    return this.container.popPortal(index)
  }

  getLastPortalName() {
    return this.container.getLastPortalName();
  }

  getActivePortal() {
    return this.container.portals.find(p => p.portalId === this.container.activePortal);
  }

  isActive(name: string) {
    return this.getActivePortal.name == name;
  }

}
