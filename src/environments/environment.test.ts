export const environment = {
  production: true,
  chatbot: {
    host: "https://testbot-mcommerce.herokuapp.com",
    version: "v1",
    uiHost: "https://testbot.mcom.app",
    firebase: {
      apiKey: "AIzaSyBQ0lvxhKP33q9u1r5H_eBTpUzyI9hcsGw",
      authDomain: "mcommercetest-3d6ac.firebaseapp.com",
      databaseURL: "https://mcommercetest-3d6ac.firebaseio.com",
      projectId: "mcommercetest-3d6ac",
      storageBucket: "mcommercetest-3d6ac.appspot.com",
      messagingSenderId: "409324113138",
    },
    notifyPageId: "714956311961788",
    documentHost: "https://mcomdoc.herokuapp.com",
    staticHost: "https://bot-static.mcom.app",
  },
  socket: {
    host: "socket.mcom.app",
    port: 443,
    secure: true,
  },
  facebook: {
    appId: "1725534497512112",
    version: "v3.1",
  },
};
