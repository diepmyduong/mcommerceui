// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// export const environment = {
//   production: false,
//   chatbot: {
//     host: "http://localhost:5000",
//     version: "v1",
//     uiHost: "http://localhost:4200",
//     firebase: {
//       apiKey: "AIzaSyCaIxMarog1k6pHiID5LkSjFCYj3U3NNsQ",
//       authDomain: "mcommerce-dashboard.firebaseapp.com",
//       databaseURL: "https://mcommerce-dashboard.firebaseio.com",
//       projectId: "mcommerce-dashboard",
//       storageBucket: "mcommerce-dashboard.appspot.com",
//       messagingSenderId: "501335675666"
//     }
//   }
// };
export const environment = {
  production: false,
  chatbot: {
    // host: "https://a8eb4f6e50f3.ngrok.io",
    livechatHost: "https://live.botpro.vn",
    host: "https://bot-server.mcom.app",
    // host: "https://mcom.local:4200",
    version: "v1",
    uiHost: "https://bot-server.mcom.app",
    firebase: {
      apiKey: "AIzaSyCaIxMarog1k6pHiID5LkSjFCYj3U3NNsQ",
      authDomain: "mcommerce-dashboard.firebaseapp.com",
      databaseURL: "https://mcommerce-dashboard.firebaseio.com",
      projectId: "mcommerce-dashboard",
      storageBucket: "mcommerce-dashboard.appspot.com",
      messagingSenderId: "501335675666",
    },
    orderHost: "https://order.mcom.app",
    notifyPageId: "714956311961788",
    documentHost: "https://mcomdoc.herokuapp.com",
    staticHost: "https://bot-static.mcom.app",
  },
  socket: {
    host: "socket.mcom.app",
    port: 443,
    secure: true,
  },
  facebook: {
    appId: "143366482876596",
    version: "v3.3",
  },
};
