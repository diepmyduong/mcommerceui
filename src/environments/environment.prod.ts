export const environment = {
  production: true,
  chatbot: {
    livechatHost: "https://live.botpro.vn",
    host: "https://bot-server.mcom.app",
    version: "v1",
    uiHost: "https://bot.mcom.app",
    firebase: {
      apiKey: "AIzaSyCaIxMarog1k6pHiID5LkSjFCYj3U3NNsQ",
      authDomain: "mcommerce-dashboard.firebaseapp.com",
      databaseURL: "https://mcommerce-dashboard.firebaseio.com",
      projectId: "mcommerce-dashboard",
      storageBucket: "mcommerce-dashboard.appspot.com",
      messagingSenderId: "501335675666",
    },
    orderHost: "https://order.mcom.app",
    notifyPageId: "714956311961788",
    documentHost: "https://mcomdoc.herokuapp.com",
    staticHost: "https://bot-static.mcom.app",
  },
  socket: {
    host: "socket.mcom.app",
    port: 443,
    secure: true,
  },
  facebook: {
    appId: "143366482876596",
    version: "v3.1",
  },
};
