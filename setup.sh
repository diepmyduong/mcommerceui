#!bin/bash

node patch.js
npm install -g node-gyp
brew install pkg-config cairo libpng jpeg giflib
export PKG_CONFIG_PATH="${PKG_CONFIG_PATH}:/usr/local/opt/libffi/lib/pkgconfig"
npm i node-sass