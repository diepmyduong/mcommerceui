const fs = require('fs');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ProgressPlugin = require('webpack/lib/ProgressPlugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const postcssUrl = require('postcss-url');
const cssnano = require('cssnano');
const customProperties = require('postcss-custom-properties');

const { NoEmitOnErrorsPlugin, SourceMapDevToolPlugin, NamedModulesPlugin } = require('webpack');
const { NamedLazyChunksWebpackPlugin, BaseHrefWebpackPlugin } = require('@angular/cli/plugins/webpack');
const { CommonsChunkPlugin } = require('webpack').optimize;
const { AngularCompilerPlugin } = require('@ngtools/webpack');

const nodeModules = path.join(process.cwd(), 'node_modules');
const realNodeModules = fs.realpathSync(nodeModules);
const genDirNodeModules = path.join(process.cwd(), 'src', '$$_gendir', 'node_modules');
const entryPoints = ["inline","polyfills","sw-register","styles","vendor","main"];
const minimizeCss = false;
const baseHref = "";
const deployUrl = "";
const postcssPlugins = function () {
        // safe settings based on: https://github.com/ben-eb/cssnano/issues/358#issuecomment-283696193
        const importantCommentRe = /@preserve|@license|[@#]\s*source(?:Mapping)?URL|^!/i;
        const minimizeOptions = {
            autoprefixer: false,
            safe: true,
            mergeLonghand: false,
            discardComments: { remove: (comment) => !importantCommentRe.test(comment) }
        };
        return [
            postcssUrl({
                url: (URL) => {
                    // Only convert root relative URLs, which CSS-Loader won't process into require().
                    if (!URL.startsWith('/') || URL.startsWith('//')) {
                        return URL;
                    }
                    if (deployUrl.match(/:\/\//)) {
                        // If deployUrl contains a scheme, ignore baseHref use deployUrl as is.
                        return `${deployUrl.replace(/\/$/, '')}${URL}`;
                    }
                    else if (baseHref.match(/:\/\//)) {
                        // If baseHref contains a scheme, include it as is.
                        return baseHref.replace(/\/$/, '') +
                            `/${deployUrl}/${URL}`.replace(/\/\/+/g, '/');
                    }
                    else {
                        // Join together base-href, deploy-url and the original URL.
                        // Also dedupe multiple slashes into single ones.
                        return `/${baseHref}/${deployUrl}/${URL}`.replace(/\/\/+/g, '/');
                    }
                }
            }),
            autoprefixer(),
            customProperties({ preserve: true })
        ].concat(minimizeCss ? [cssnano(minimizeOptions)] : []);
    };




module.exports = {
  "resolve": {
    "extensions": [
      ".ts",
      ".js"
    ],
    "modules": [
      "./node_modules",
      "./node_modules"
    ],
    "symlinks": true,
    "alias": {
      "rxjs/AsyncSubject": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/AsyncSubject.js",
      "rxjs/BehaviorSubject": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/BehaviorSubject.js",
      "rxjs/InnerSubscriber": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/InnerSubscriber.js",
      "rxjs/Notification": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/Notification.js",
      "rxjs/Observable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/Observable.js",
      "rxjs/Observer": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/Observer.js",
      "rxjs/Operator": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/Operator.js",
      "rxjs/OuterSubscriber": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/OuterSubscriber.js",
      "rxjs/ReplaySubject": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/ReplaySubject.js",
      "rxjs/Rx": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/Rx.js",
      "rxjs/Scheduler": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/Scheduler.js",
      "rxjs/Subject": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/Subject.js",
      "rxjs/SubjectSubscription": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/SubjectSubscription.js",
      "rxjs/Subscriber": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/Subscriber.js",
      "rxjs/Subscription": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/Subscription.js",
      "rxjs/add/observable/bindCallback": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/bindCallback.js",
      "rxjs/add/observable/bindNodeCallback": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/bindNodeCallback.js",
      "rxjs/add/observable/combineLatest": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/combineLatest.js",
      "rxjs/add/observable/concat": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/concat.js",
      "rxjs/add/observable/defer": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/defer.js",
      "rxjs/add/observable/dom/ajax": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/dom/ajax.js",
      "rxjs/add/observable/dom/webSocket": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/dom/webSocket.js",
      "rxjs/add/observable/empty": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/empty.js",
      "rxjs/add/observable/forkJoin": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/forkJoin.js",
      "rxjs/add/observable/from": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/from.js",
      "rxjs/add/observable/fromEvent": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/fromEvent.js",
      "rxjs/add/observable/fromEventPattern": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/fromEventPattern.js",
      "rxjs/add/observable/fromPromise": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/fromPromise.js",
      "rxjs/add/observable/generate": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/generate.js",
      "rxjs/add/observable/if": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/if.js",
      "rxjs/add/observable/interval": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/interval.js",
      "rxjs/add/observable/merge": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/merge.js",
      "rxjs/add/observable/never": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/never.js",
      "rxjs/add/observable/of": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/of.js",
      "rxjs/add/observable/onErrorResumeNext": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/onErrorResumeNext.js",
      "rxjs/add/observable/pairs": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/pairs.js",
      "rxjs/add/observable/race": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/race.js",
      "rxjs/add/observable/range": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/range.js",
      "rxjs/add/observable/throw": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/throw.js",
      "rxjs/add/observable/timer": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/timer.js",
      "rxjs/add/observable/using": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/using.js",
      "rxjs/add/observable/zip": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/observable/zip.js",
      "rxjs/add/operator/audit": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/audit.js",
      "rxjs/add/operator/auditTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/auditTime.js",
      "rxjs/add/operator/buffer": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/buffer.js",
      "rxjs/add/operator/bufferCount": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/bufferCount.js",
      "rxjs/add/operator/bufferTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/bufferTime.js",
      "rxjs/add/operator/bufferToggle": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/bufferToggle.js",
      "rxjs/add/operator/bufferWhen": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/bufferWhen.js",
      "rxjs/add/operator/catch": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/catch.js",
      "rxjs/add/operator/combineAll": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/combineAll.js",
      "rxjs/add/operator/combineLatest": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/combineLatest.js",
      "rxjs/add/operator/concat": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/concat.js",
      "rxjs/add/operator/concatAll": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/concatAll.js",
      "rxjs/add/operator/concatMap": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/concatMap.js",
      "rxjs/add/operator/concatMapTo": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/concatMapTo.js",
      "rxjs/add/operator/count": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/count.js",
      "rxjs/add/operator/debounce": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/debounce.js",
      "rxjs/add/operator/debounceTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/debounceTime.js",
      "rxjs/add/operator/defaultIfEmpty": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/defaultIfEmpty.js",
      "rxjs/add/operator/delay": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/delay.js",
      "rxjs/add/operator/delayWhen": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/delayWhen.js",
      "rxjs/add/operator/dematerialize": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/dematerialize.js",
      "rxjs/add/operator/distinct": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/distinct.js",
      "rxjs/add/operator/distinctUntilChanged": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/distinctUntilChanged.js",
      "rxjs/add/operator/distinctUntilKeyChanged": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/distinctUntilKeyChanged.js",
      "rxjs/add/operator/do": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/do.js",
      "rxjs/add/operator/elementAt": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/elementAt.js",
      "rxjs/add/operator/every": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/every.js",
      "rxjs/add/operator/exhaust": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/exhaust.js",
      "rxjs/add/operator/exhaustMap": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/exhaustMap.js",
      "rxjs/add/operator/expand": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/expand.js",
      "rxjs/add/operator/filter": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/filter.js",
      "rxjs/add/operator/finally": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/finally.js",
      "rxjs/add/operator/find": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/find.js",
      "rxjs/add/operator/findIndex": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/findIndex.js",
      "rxjs/add/operator/first": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/first.js",
      "rxjs/add/operator/groupBy": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/groupBy.js",
      "rxjs/add/operator/ignoreElements": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/ignoreElements.js",
      "rxjs/add/operator/isEmpty": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/isEmpty.js",
      "rxjs/add/operator/last": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/last.js",
      "rxjs/add/operator/let": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/let.js",
      "rxjs/add/operator/map": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/map.js",
      "rxjs/add/operator/mapTo": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/mapTo.js",
      "rxjs/add/operator/materialize": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/materialize.js",
      "rxjs/add/operator/max": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/max.js",
      "rxjs/add/operator/merge": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/merge.js",
      "rxjs/add/operator/mergeAll": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/mergeAll.js",
      "rxjs/add/operator/mergeMap": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/mergeMap.js",
      "rxjs/add/operator/mergeMapTo": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/mergeMapTo.js",
      "rxjs/add/operator/mergeScan": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/mergeScan.js",
      "rxjs/add/operator/min": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/min.js",
      "rxjs/add/operator/multicast": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/multicast.js",
      "rxjs/add/operator/observeOn": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/observeOn.js",
      "rxjs/add/operator/onErrorResumeNext": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/onErrorResumeNext.js",
      "rxjs/add/operator/pairwise": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/pairwise.js",
      "rxjs/add/operator/partition": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/partition.js",
      "rxjs/add/operator/pluck": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/pluck.js",
      "rxjs/add/operator/publish": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/publish.js",
      "rxjs/add/operator/publishBehavior": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/publishBehavior.js",
      "rxjs/add/operator/publishLast": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/publishLast.js",
      "rxjs/add/operator/publishReplay": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/publishReplay.js",
      "rxjs/add/operator/race": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/race.js",
      "rxjs/add/operator/reduce": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/reduce.js",
      "rxjs/add/operator/repeat": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/repeat.js",
      "rxjs/add/operator/repeatWhen": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/repeatWhen.js",
      "rxjs/add/operator/retry": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/retry.js",
      "rxjs/add/operator/retryWhen": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/retryWhen.js",
      "rxjs/add/operator/sample": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/sample.js",
      "rxjs/add/operator/sampleTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/sampleTime.js",
      "rxjs/add/operator/scan": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/scan.js",
      "rxjs/add/operator/sequenceEqual": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/sequenceEqual.js",
      "rxjs/add/operator/share": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/share.js",
      "rxjs/add/operator/shareReplay": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/shareReplay.js",
      "rxjs/add/operator/single": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/single.js",
      "rxjs/add/operator/skip": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/skip.js",
      "rxjs/add/operator/skipLast": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/skipLast.js",
      "rxjs/add/operator/skipUntil": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/skipUntil.js",
      "rxjs/add/operator/skipWhile": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/skipWhile.js",
      "rxjs/add/operator/startWith": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/startWith.js",
      "rxjs/add/operator/subscribeOn": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/subscribeOn.js",
      "rxjs/add/operator/switch": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/switch.js",
      "rxjs/add/operator/switchMap": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/switchMap.js",
      "rxjs/add/operator/switchMapTo": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/switchMapTo.js",
      "rxjs/add/operator/take": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/take.js",
      "rxjs/add/operator/takeLast": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/takeLast.js",
      "rxjs/add/operator/takeUntil": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/takeUntil.js",
      "rxjs/add/operator/takeWhile": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/takeWhile.js",
      "rxjs/add/operator/throttle": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/throttle.js",
      "rxjs/add/operator/throttleTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/throttleTime.js",
      "rxjs/add/operator/timeInterval": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/timeInterval.js",
      "rxjs/add/operator/timeout": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/timeout.js",
      "rxjs/add/operator/timeoutWith": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/timeoutWith.js",
      "rxjs/add/operator/timestamp": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/timestamp.js",
      "rxjs/add/operator/toArray": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/toArray.js",
      "rxjs/add/operator/toPromise": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/toPromise.js",
      "rxjs/add/operator/window": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/window.js",
      "rxjs/add/operator/windowCount": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/windowCount.js",
      "rxjs/add/operator/windowTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/windowTime.js",
      "rxjs/add/operator/windowToggle": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/windowToggle.js",
      "rxjs/add/operator/windowWhen": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/windowWhen.js",
      "rxjs/add/operator/withLatestFrom": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/withLatestFrom.js",
      "rxjs/add/operator/zip": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/zip.js",
      "rxjs/add/operator/zipAll": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/add/operator/zipAll.js",
      "rxjs/interfaces": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/interfaces.js",
      "rxjs/observable/ArrayLikeObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/ArrayLikeObservable.js",
      "rxjs/observable/ArrayObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/ArrayObservable.js",
      "rxjs/observable/BoundCallbackObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/BoundCallbackObservable.js",
      "rxjs/observable/BoundNodeCallbackObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/BoundNodeCallbackObservable.js",
      "rxjs/observable/ConnectableObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/ConnectableObservable.js",
      "rxjs/observable/DeferObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/DeferObservable.js",
      "rxjs/observable/EmptyObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/EmptyObservable.js",
      "rxjs/observable/ErrorObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/ErrorObservable.js",
      "rxjs/observable/ForkJoinObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/ForkJoinObservable.js",
      "rxjs/observable/FromEventObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/FromEventObservable.js",
      "rxjs/observable/FromEventPatternObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/FromEventPatternObservable.js",
      "rxjs/observable/FromObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/FromObservable.js",
      "rxjs/observable/GenerateObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/GenerateObservable.js",
      "rxjs/observable/IfObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/IfObservable.js",
      "rxjs/observable/IntervalObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/IntervalObservable.js",
      "rxjs/observable/IteratorObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/IteratorObservable.js",
      "rxjs/observable/NeverObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/NeverObservable.js",
      "rxjs/observable/PairsObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/PairsObservable.js",
      "rxjs/observable/PromiseObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/PromiseObservable.js",
      "rxjs/observable/RangeObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/RangeObservable.js",
      "rxjs/observable/ScalarObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/ScalarObservable.js",
      "rxjs/observable/SubscribeOnObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/SubscribeOnObservable.js",
      "rxjs/observable/TimerObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/TimerObservable.js",
      "rxjs/observable/UsingObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/UsingObservable.js",
      "rxjs/observable/bindCallback": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/bindCallback.js",
      "rxjs/observable/bindNodeCallback": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/bindNodeCallback.js",
      "rxjs/observable/combineLatest": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/combineLatest.js",
      "rxjs/observable/concat": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/concat.js",
      "rxjs/observable/defer": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/defer.js",
      "rxjs/observable/dom/AjaxObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/dom/AjaxObservable.js",
      "rxjs/observable/dom/WebSocketSubject": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/dom/WebSocketSubject.js",
      "rxjs/observable/dom/ajax": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/dom/ajax.js",
      "rxjs/observable/dom/webSocket": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/dom/webSocket.js",
      "rxjs/observable/empty": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/empty.js",
      "rxjs/observable/forkJoin": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/forkJoin.js",
      "rxjs/observable/from": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/from.js",
      "rxjs/observable/fromEvent": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/fromEvent.js",
      "rxjs/observable/fromEventPattern": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/fromEventPattern.js",
      "rxjs/observable/fromPromise": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/fromPromise.js",
      "rxjs/observable/generate": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/generate.js",
      "rxjs/observable/if": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/if.js",
      "rxjs/observable/interval": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/interval.js",
      "rxjs/observable/merge": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/merge.js",
      "rxjs/observable/never": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/never.js",
      "rxjs/observable/of": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/of.js",
      "rxjs/observable/onErrorResumeNext": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/onErrorResumeNext.js",
      "rxjs/observable/pairs": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/pairs.js",
      "rxjs/observable/race": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/race.js",
      "rxjs/observable/range": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/range.js",
      "rxjs/observable/throw": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/throw.js",
      "rxjs/observable/timer": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/timer.js",
      "rxjs/observable/using": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/using.js",
      "rxjs/observable/zip": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/observable/zip.js",
      "rxjs/operator/audit": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/audit.js",
      "rxjs/operator/auditTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/auditTime.js",
      "rxjs/operator/buffer": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/buffer.js",
      "rxjs/operator/bufferCount": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/bufferCount.js",
      "rxjs/operator/bufferTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/bufferTime.js",
      "rxjs/operator/bufferToggle": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/bufferToggle.js",
      "rxjs/operator/bufferWhen": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/bufferWhen.js",
      "rxjs/operator/catch": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/catch.js",
      "rxjs/operator/combineAll": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/combineAll.js",
      "rxjs/operator/combineLatest": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/combineLatest.js",
      "rxjs/operator/concat": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/concat.js",
      "rxjs/operator/concatAll": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/concatAll.js",
      "rxjs/operator/concatMap": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/concatMap.js",
      "rxjs/operator/concatMapTo": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/concatMapTo.js",
      "rxjs/operator/count": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/count.js",
      "rxjs/operator/debounce": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/debounce.js",
      "rxjs/operator/debounceTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/debounceTime.js",
      "rxjs/operator/defaultIfEmpty": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/defaultIfEmpty.js",
      "rxjs/operator/delay": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/delay.js",
      "rxjs/operator/delayWhen": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/delayWhen.js",
      "rxjs/operator/dematerialize": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/dematerialize.js",
      "rxjs/operator/distinct": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/distinct.js",
      "rxjs/operator/distinctUntilChanged": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/distinctUntilChanged.js",
      "rxjs/operator/distinctUntilKeyChanged": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/distinctUntilKeyChanged.js",
      "rxjs/operator/do": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/do.js",
      "rxjs/operator/elementAt": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/elementAt.js",
      "rxjs/operator/every": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/every.js",
      "rxjs/operator/exhaust": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/exhaust.js",
      "rxjs/operator/exhaustMap": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/exhaustMap.js",
      "rxjs/operator/expand": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/expand.js",
      "rxjs/operator/filter": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/filter.js",
      "rxjs/operator/finally": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/finally.js",
      "rxjs/operator/find": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/find.js",
      "rxjs/operator/findIndex": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/findIndex.js",
      "rxjs/operator/first": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/first.js",
      "rxjs/operator/groupBy": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/groupBy.js",
      "rxjs/operator/ignoreElements": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/ignoreElements.js",
      "rxjs/operator/isEmpty": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/isEmpty.js",
      "rxjs/operator/last": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/last.js",
      "rxjs/operator/let": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/let.js",
      "rxjs/operator/map": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/map.js",
      "rxjs/operator/mapTo": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/mapTo.js",
      "rxjs/operator/materialize": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/materialize.js",
      "rxjs/operator/max": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/max.js",
      "rxjs/operator/merge": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/merge.js",
      "rxjs/operator/mergeAll": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/mergeAll.js",
      "rxjs/operator/mergeMap": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/mergeMap.js",
      "rxjs/operator/mergeMapTo": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/mergeMapTo.js",
      "rxjs/operator/mergeScan": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/mergeScan.js",
      "rxjs/operator/min": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/min.js",
      "rxjs/operator/multicast": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/multicast.js",
      "rxjs/operator/observeOn": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/observeOn.js",
      "rxjs/operator/onErrorResumeNext": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/onErrorResumeNext.js",
      "rxjs/operator/pairwise": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/pairwise.js",
      "rxjs/operator/partition": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/partition.js",
      "rxjs/operator/pluck": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/pluck.js",
      "rxjs/operator/publish": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/publish.js",
      "rxjs/operator/publishBehavior": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/publishBehavior.js",
      "rxjs/operator/publishLast": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/publishLast.js",
      "rxjs/operator/publishReplay": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/publishReplay.js",
      "rxjs/operator/race": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/race.js",
      "rxjs/operator/reduce": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/reduce.js",
      "rxjs/operator/repeat": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/repeat.js",
      "rxjs/operator/repeatWhen": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/repeatWhen.js",
      "rxjs/operator/retry": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/retry.js",
      "rxjs/operator/retryWhen": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/retryWhen.js",
      "rxjs/operator/sample": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/sample.js",
      "rxjs/operator/sampleTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/sampleTime.js",
      "rxjs/operator/scan": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/scan.js",
      "rxjs/operator/sequenceEqual": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/sequenceEqual.js",
      "rxjs/operator/share": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/share.js",
      "rxjs/operator/shareReplay": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/shareReplay.js",
      "rxjs/operator/single": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/single.js",
      "rxjs/operator/skip": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/skip.js",
      "rxjs/operator/skipLast": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/skipLast.js",
      "rxjs/operator/skipUntil": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/skipUntil.js",
      "rxjs/operator/skipWhile": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/skipWhile.js",
      "rxjs/operator/startWith": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/startWith.js",
      "rxjs/operator/subscribeOn": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/subscribeOn.js",
      "rxjs/operator/switch": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/switch.js",
      "rxjs/operator/switchMap": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/switchMap.js",
      "rxjs/operator/switchMapTo": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/switchMapTo.js",
      "rxjs/operator/take": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/take.js",
      "rxjs/operator/takeLast": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/takeLast.js",
      "rxjs/operator/takeUntil": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/takeUntil.js",
      "rxjs/operator/takeWhile": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/takeWhile.js",
      "rxjs/operator/throttle": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/throttle.js",
      "rxjs/operator/throttleTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/throttleTime.js",
      "rxjs/operator/timeInterval": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/timeInterval.js",
      "rxjs/operator/timeout": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/timeout.js",
      "rxjs/operator/timeoutWith": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/timeoutWith.js",
      "rxjs/operator/timestamp": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/timestamp.js",
      "rxjs/operator/toArray": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/toArray.js",
      "rxjs/operator/toPromise": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/toPromise.js",
      "rxjs/operator/window": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/window.js",
      "rxjs/operator/windowCount": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/windowCount.js",
      "rxjs/operator/windowTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/windowTime.js",
      "rxjs/operator/windowToggle": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/windowToggle.js",
      "rxjs/operator/windowWhen": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/windowWhen.js",
      "rxjs/operator/withLatestFrom": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/withLatestFrom.js",
      "rxjs/operator/zip": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/zip.js",
      "rxjs/operator/zipAll": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operator/zipAll.js",
      "rxjs/operators/audit": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/audit.js",
      "rxjs/operators/auditTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/auditTime.js",
      "rxjs/operators/buffer": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/buffer.js",
      "rxjs/operators/bufferCount": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/bufferCount.js",
      "rxjs/operators/bufferTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/bufferTime.js",
      "rxjs/operators/bufferToggle": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/bufferToggle.js",
      "rxjs/operators/bufferWhen": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/bufferWhen.js",
      "rxjs/operators/catchError": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/catchError.js",
      "rxjs/operators/combineAll": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/combineAll.js",
      "rxjs/operators/combineLatest": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/combineLatest.js",
      "rxjs/operators/concat": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/concat.js",
      "rxjs/operators/concatAll": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/concatAll.js",
      "rxjs/operators/concatMap": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/concatMap.js",
      "rxjs/operators/concatMapTo": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/concatMapTo.js",
      "rxjs/operators/count": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/count.js",
      "rxjs/operators/debounce": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/debounce.js",
      "rxjs/operators/debounceTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/debounceTime.js",
      "rxjs/operators/defaultIfEmpty": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/defaultIfEmpty.js",
      "rxjs/operators/delay": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/delay.js",
      "rxjs/operators/delayWhen": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/delayWhen.js",
      "rxjs/operators/dematerialize": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/dematerialize.js",
      "rxjs/operators/distinct": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/distinct.js",
      "rxjs/operators/distinctUntilChanged": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/distinctUntilChanged.js",
      "rxjs/operators/distinctUntilKeyChanged": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/distinctUntilKeyChanged.js",
      "rxjs/operators/elementAt": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/elementAt.js",
      "rxjs/operators/every": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/every.js",
      "rxjs/operators/exhaust": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/exhaust.js",
      "rxjs/operators/exhaustMap": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/exhaustMap.js",
      "rxjs/operators/expand": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/expand.js",
      "rxjs/operators/filter": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/filter.js",
      "rxjs/operators/finalize": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/finalize.js",
      "rxjs/operators/find": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/find.js",
      "rxjs/operators/findIndex": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/findIndex.js",
      "rxjs/operators/first": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/first.js",
      "rxjs/operators/groupBy": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/groupBy.js",
      "rxjs/operators/ignoreElements": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/ignoreElements.js",
      "rxjs/operators/index": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/index.js",
      "rxjs/operators/isEmpty": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/isEmpty.js",
      "rxjs/operators/last": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/last.js",
      "rxjs/operators/map": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/map.js",
      "rxjs/operators/mapTo": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/mapTo.js",
      "rxjs/operators/materialize": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/materialize.js",
      "rxjs/operators/max": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/max.js",
      "rxjs/operators/merge": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/merge.js",
      "rxjs/operators/mergeAll": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/mergeAll.js",
      "rxjs/operators/mergeMap": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/mergeMap.js",
      "rxjs/operators/mergeMapTo": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/mergeMapTo.js",
      "rxjs/operators/mergeScan": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/mergeScan.js",
      "rxjs/operators/min": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/min.js",
      "rxjs/operators/multicast": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/multicast.js",
      "rxjs/operators/observeOn": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/observeOn.js",
      "rxjs/operators/onErrorResumeNext": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/onErrorResumeNext.js",
      "rxjs/operators/pairwise": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/pairwise.js",
      "rxjs/operators/partition": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/partition.js",
      "rxjs/operators/pluck": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/pluck.js",
      "rxjs/operators/publish": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/publish.js",
      "rxjs/operators/publishBehavior": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/publishBehavior.js",
      "rxjs/operators/publishLast": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/publishLast.js",
      "rxjs/operators/publishReplay": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/publishReplay.js",
      "rxjs/operators/race": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/race.js",
      "rxjs/operators/reduce": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/reduce.js",
      "rxjs/operators/refCount": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/refCount.js",
      "rxjs/operators/repeat": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/repeat.js",
      "rxjs/operators/repeatWhen": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/repeatWhen.js",
      "rxjs/operators/retry": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/retry.js",
      "rxjs/operators/retryWhen": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/retryWhen.js",
      "rxjs/operators/sample": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/sample.js",
      "rxjs/operators/sampleTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/sampleTime.js",
      "rxjs/operators/scan": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/scan.js",
      "rxjs/operators/sequenceEqual": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/sequenceEqual.js",
      "rxjs/operators/share": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/share.js",
      "rxjs/operators/shareReplay": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/shareReplay.js",
      "rxjs/operators/single": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/single.js",
      "rxjs/operators/skip": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/skip.js",
      "rxjs/operators/skipLast": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/skipLast.js",
      "rxjs/operators/skipUntil": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/skipUntil.js",
      "rxjs/operators/skipWhile": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/skipWhile.js",
      "rxjs/operators/startWith": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/startWith.js",
      "rxjs/operators/subscribeOn": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/subscribeOn.js",
      "rxjs/operators/switchAll": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/switchAll.js",
      "rxjs/operators/switchMap": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/switchMap.js",
      "rxjs/operators/switchMapTo": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/switchMapTo.js",
      "rxjs/operators/take": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/take.js",
      "rxjs/operators/takeLast": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/takeLast.js",
      "rxjs/operators/takeUntil": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/takeUntil.js",
      "rxjs/operators/takeWhile": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/takeWhile.js",
      "rxjs/operators/tap": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/tap.js",
      "rxjs/operators/throttle": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/throttle.js",
      "rxjs/operators/throttleTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/throttleTime.js",
      "rxjs/operators/timeInterval": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/timeInterval.js",
      "rxjs/operators/timeout": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/timeout.js",
      "rxjs/operators/timeoutWith": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/timeoutWith.js",
      "rxjs/operators/timestamp": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/timestamp.js",
      "rxjs/operators/toArray": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/toArray.js",
      "rxjs/operators/window": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/window.js",
      "rxjs/operators/windowCount": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/windowCount.js",
      "rxjs/operators/windowTime": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/windowTime.js",
      "rxjs/operators/windowToggle": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/windowToggle.js",
      "rxjs/operators/windowWhen": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/windowWhen.js",
      "rxjs/operators/withLatestFrom": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/withLatestFrom.js",
      "rxjs/operators/zip": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/zip.js",
      "rxjs/operators/zipAll": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/zipAll.js",
      "rxjs/scheduler/Action": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/scheduler/Action.js",
      "rxjs/scheduler/AnimationFrameAction": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/scheduler/AnimationFrameAction.js",
      "rxjs/scheduler/AnimationFrameScheduler": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/scheduler/AnimationFrameScheduler.js",
      "rxjs/scheduler/AsapAction": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/scheduler/AsapAction.js",
      "rxjs/scheduler/AsapScheduler": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/scheduler/AsapScheduler.js",
      "rxjs/scheduler/AsyncAction": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/scheduler/AsyncAction.js",
      "rxjs/scheduler/AsyncScheduler": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/scheduler/AsyncScheduler.js",
      "rxjs/scheduler/QueueAction": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/scheduler/QueueAction.js",
      "rxjs/scheduler/QueueScheduler": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/scheduler/QueueScheduler.js",
      "rxjs/scheduler/VirtualTimeScheduler": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/scheduler/VirtualTimeScheduler.js",
      "rxjs/scheduler/animationFrame": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/scheduler/animationFrame.js",
      "rxjs/scheduler/asap": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/scheduler/asap.js",
      "rxjs/scheduler/async": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/scheduler/async.js",
      "rxjs/scheduler/queue": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/scheduler/queue.js",
      "rxjs/symbol/iterator": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/symbol/iterator.js",
      "rxjs/symbol/observable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/symbol/observable.js",
      "rxjs/symbol/rxSubscriber": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/symbol/rxSubscriber.js",
      "rxjs/testing/ColdObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/testing/ColdObservable.js",
      "rxjs/testing/HotObservable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/testing/HotObservable.js",
      "rxjs/testing/SubscriptionLog": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/testing/SubscriptionLog.js",
      "rxjs/testing/SubscriptionLoggable": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/testing/SubscriptionLoggable.js",
      "rxjs/testing/TestMessage": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/testing/TestMessage.js",
      "rxjs/testing/TestScheduler": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/testing/TestScheduler.js",
      "rxjs/util/AnimationFrame": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/AnimationFrame.js",
      "rxjs/util/ArgumentOutOfRangeError": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/ArgumentOutOfRangeError.js",
      "rxjs/util/EmptyError": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/EmptyError.js",
      "rxjs/util/FastMap": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/FastMap.js",
      "rxjs/util/Immediate": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/Immediate.js",
      "rxjs/util/Map": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/Map.js",
      "rxjs/util/MapPolyfill": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/MapPolyfill.js",
      "rxjs/util/ObjectUnsubscribedError": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/ObjectUnsubscribedError.js",
      "rxjs/util/Set": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/Set.js",
      "rxjs/util/TimeoutError": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/TimeoutError.js",
      "rxjs/util/UnsubscriptionError": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/UnsubscriptionError.js",
      "rxjs/util/applyMixins": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/applyMixins.js",
      "rxjs/util/assign": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/assign.js",
      "rxjs/util/errorObject": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/errorObject.js",
      "rxjs/util/identity": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/identity.js",
      "rxjs/util/isArray": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/isArray.js",
      "rxjs/util/isArrayLike": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/isArrayLike.js",
      "rxjs/util/isDate": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/isDate.js",
      "rxjs/util/isFunction": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/isFunction.js",
      "rxjs/util/isNumeric": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/isNumeric.js",
      "rxjs/util/isObject": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/isObject.js",
      "rxjs/util/isPromise": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/isPromise.js",
      "rxjs/util/isScheduler": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/isScheduler.js",
      "rxjs/util/noop": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/noop.js",
      "rxjs/util/not": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/not.js",
      "rxjs/util/pipe": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/pipe.js",
      "rxjs/util/root": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/root.js",
      "rxjs/util/subscribeToResult": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/subscribeToResult.js",
      "rxjs/util/toSubscriber": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/toSubscriber.js",
      "rxjs/util/tryCatch": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/util/tryCatch.js",
      "rxjs/operators": "/Users/user/Desktop/MyFiles/Innoway/mcommerce/node_modules/rxjs/_esm5/operators/index.js"
    },
    "mainFields": [
      "browser",
      "module",
      "main"
    ]
  },
  "resolveLoader": {
    "modules": [
      "./node_modules",
      "./node_modules"
    ]
  },
  "entry": {
    "main": [
      "./src/main.ts"
    ],
    "polyfills": [
      "./src/polyfills.ts"
    ],
    "styles": [
      "./src/styles.scss"
    ]
  },
  "output": {
    "path": path.join(process.cwd(), "dist"),
    "filename": "[name].bundle.js",
    "chunkFilename": "[id].chunk.js",
    "crossOriginLoading": false
  },
  "module": {
    "rules": [
      {
        "test": /\.html$/,
        "loader": "raw-loader"
      },
      {
        "test": /\.(eot|svg|cur)$/,
        "loader": "file-loader",
        "options": {
          "name": "[name].[hash:20].[ext]",
          "limit": 10000
        }
      },
      {
        "test": /\.(jpg|png|webp|gif|otf|ttf|woff|woff2|ani)$/,
        "loader": "url-loader",
        "options": {
          "name": "[name].[hash:20].[ext]",
          "limit": 10000
        }
      },
      {
        "exclude": [
          path.join(process.cwd(), "src/styles.scss")
        ],
        "test": /\.css$/,
        "use": [
          "exports-loader?module.exports.toString()",
          {
            "loader": "css-loader",
            "options": {
              "sourceMap": false,
              "importLoaders": 1
            }
          },
          {
            "loader": "postcss-loader",
            "options": {
              "ident": "postcss",
              "plugins": postcssPlugins
            }
          }
        ]
      },
      {
        "exclude": [
          path.join(process.cwd(), "src/styles.scss")
        ],
        "test": /\.scss$|\.sass$/,
        "use": [
          "exports-loader?module.exports.toString()",
          {
            "loader": "css-loader",
            "options": {
              "sourceMap": false,
              "importLoaders": 1
            }
          },
          {
            "loader": "postcss-loader",
            "options": {
              "ident": "postcss",
              "plugins": postcssPlugins
            }
          },
          {
            "loader": "sass-loader",
            "options": {
              "sourceMap": false,
              "precision": 8,
              "includePaths": []
            }
          }
        ]
      },
      {
        "exclude": [
          path.join(process.cwd(), "src/styles.scss")
        ],
        "test": /\.less$/,
        "use": [
          "exports-loader?module.exports.toString()",
          {
            "loader": "css-loader",
            "options": {
              "sourceMap": false,
              "importLoaders": 1
            }
          },
          {
            "loader": "postcss-loader",
            "options": {
              "ident": "postcss",
              "plugins": postcssPlugins
            }
          },
          {
            "loader": "less-loader",
            "options": {
              "sourceMap": false
            }
          }
        ]
      },
      {
        "exclude": [
          path.join(process.cwd(), "src/styles.scss")
        ],
        "test": /\.styl$/,
        "use": [
          "exports-loader?module.exports.toString()",
          {
            "loader": "css-loader",
            "options": {
              "sourceMap": false,
              "importLoaders": 1
            }
          },
          {
            "loader": "postcss-loader",
            "options": {
              "ident": "postcss",
              "plugins": postcssPlugins
            }
          },
          {
            "loader": "stylus-loader",
            "options": {
              "sourceMap": false,
              "paths": []
            }
          }
        ]
      },
      {
        "include": [
          path.join(process.cwd(), "src/styles.scss")
        ],
        "test": /\.css$/,
        "use": [
          "style-loader",
          {
            "loader": "css-loader",
            "options": {
              "sourceMap": false,
              "importLoaders": 1
            }
          },
          {
            "loader": "postcss-loader",
            "options": {
              "ident": "postcss",
              "plugins": postcssPlugins
            }
          }
        ]
      },
      {
        "include": [
          path.join(process.cwd(), "src/styles.scss")
        ],
        "test": /\.scss$|\.sass$/,
        "use": [
          "style-loader",
          {
            "loader": "css-loader",
            "options": {
              "sourceMap": false,
              "importLoaders": 1
            }
          },
          {
            "loader": "postcss-loader",
            "options": {
              "ident": "postcss",
              "plugins": postcssPlugins
            }
          },
          {
            "loader": "sass-loader",
            "options": {
              "sourceMap": false,
              "precision": 8,
              "includePaths": []
            }
          }
        ]
      },
      {
        "include": [
          path.join(process.cwd(), "src/styles.scss")
        ],
        "test": /\.less$/,
        "use": [
          "style-loader",
          {
            "loader": "css-loader",
            "options": {
              "sourceMap": false,
              "importLoaders": 1
            }
          },
          {
            "loader": "postcss-loader",
            "options": {
              "ident": "postcss",
              "plugins": postcssPlugins
            }
          },
          {
            "loader": "less-loader",
            "options": {
              "sourceMap": false
            }
          }
        ]
      },
      {
        "include": [
          path.join(process.cwd(), "src/styles.scss")
        ],
        "test": /\.styl$/,
        "use": [
          "style-loader",
          {
            "loader": "css-loader",
            "options": {
              "sourceMap": false,
              "importLoaders": 1
            }
          },
          {
            "loader": "postcss-loader",
            "options": {
              "ident": "postcss",
              "plugins": postcssPlugins
            }
          },
          {
            "loader": "stylus-loader",
            "options": {
              "sourceMap": false,
              "paths": []
            }
          }
        ]
      },
      {
        "test": /\.ts$/,
        "loader": "@ngtools/webpack"
      }
    ]
  },
  "plugins": [
    new NoEmitOnErrorsPlugin(),
    new CopyWebpackPlugin([
      {
        "context": "src",
        "to": "",
        "from": {
          "glob": "assets/**/*",
          "dot": true
        }
      },
      {
        "context": "src",
        "to": "",
        "from": {
          "glob": "favicon.ico",
          "dot": true
        }
      }
    ], {
      "ignore": [
        ".gitkeep"
      ],
      "debug": "warning"
    }),
    new ProgressPlugin(),
    new NamedLazyChunksWebpackPlugin(),
    new HtmlWebpackPlugin({
      "template": "./src/index.html",
      "filename": "./index.html",
      "hash": false,
      "inject": true,
      "compile": true,
      "favicon": false,
      "minify": false,
      "cache": true,
      "showErrors": true,
      "chunks": "all",
      "excludeChunks": [],
      "title": "Webpack App",
      "xhtml": true,
      "chunksSortMode": function sort(left, right) {
        let leftIndex = entryPoints.indexOf(left.names[0]);
        let rightindex = entryPoints.indexOf(right.names[0]);
        if (leftIndex > rightindex) {
            return 1;
        }
        else if (leftIndex < rightindex) {
            return -1;
        }
        else {
            return 0;
        }
    }
    }),
    new BaseHrefWebpackPlugin({}),
    new CommonsChunkPlugin({
      "name": [
        "inline"
      ],
      "minChunks": null
    }),
    new CommonsChunkPlugin({
      "name": [
        "vendor"
      ],
      "minChunks": (module) => {
                return module.resource
                    && (module.resource.startsWith(nodeModules)
                        || module.resource.startsWith(genDirNodeModules)
                        || module.resource.startsWith(realNodeModules));
            },
      "chunks": [
        "main"
      ]
    }),
    new SourceMapDevToolPlugin({
      "filename": "[file].map[query]",
      "moduleFilenameTemplate": "[resource-path]",
      "fallbackModuleFilenameTemplate": "[resource-path]?[hash]",
      "sourceRoot": "webpack:///"
    }),
    new CommonsChunkPlugin({
      "name": [
        "main"
      ],
      "minChunks": 2,
      "async": "common"
    }),
    new NamedModulesPlugin({}),
    new AngularCompilerPlugin({
      "mainPath": "main.ts",
      "platform": 0,
      "hostReplacementPaths": {
        "environments/environment.ts": "environments/environment.ts"
      },
      "sourceMap": true,
      "tsConfigPath": "src/tsconfig.app.json",
      "skipCodeGeneration": true,
      "compilerOptions": {}
    })
  ],
  "node": {
    "fs": "empty",
    "global": true,
    "crypto": "empty",
    "tls": "empty",
    "net": "empty",
    "process": true,
    "module": false,
    "clearImmediate": false,
    "setImmediate": false
  },
  "devServer": {
    "historyApiFallback": true
  }
};
